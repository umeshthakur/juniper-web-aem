package com.juniper.aem.helpers;

import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;

import lombok.Getter;

@Getter
@Model(adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TagHelper {
    protected final Logger Logger = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;
    
    @AemObject
    private Page currentPage;

    @Inject
    private String tagId;

    private String tagTitle = "";

    @PostConstruct
    protected void init() {
        if (resourceResolver != null) {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            Page homepage = currentPage.getAbsoluteParent(3);
            
            if (StringUtils.isNotBlank(tagId) && tagManager != null) {
                Tag tag = tagManager.resolve(tagId);
                if (tag != null) {
                    
                    if(homepage.getLanguage().getLanguage() != null && 
                       homepage.getLanguage().getCountry() != null && 
                       !homepage.getLanguage().getLanguage().equalsIgnoreCase("") &&
                       !homepage.getLanguage().getCountry().equalsIgnoreCase("")) {
                        
                        Locale lc = new Locale(homepage.getLanguage().getLanguage(), homepage.getLanguage().getCountry());
                        tagTitle = tag.getTitle(lc);
                    } else {
                        tagTitle = tag.getTitle();
                    }                    
                }
            }
        }
    }
}