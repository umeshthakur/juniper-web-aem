package com.juniper.aem.helpers;

import com.adobe.granite.ui.components.rendercondition.RenderCondition;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;

import javax.jcr.RepositoryException;
import java.util.Iterator;
import java.util.List;


public final class GroupRenderCondition implements RenderCondition {
    private final Authorizable authorizable;
    private final List<String> allowedGroups;

    public GroupRenderCondition(Authorizable authorizable, List<String> allowedGroups) {
        this.authorizable = authorizable;
        this.allowedGroups = allowedGroups;
    }

        @Override
        public boolean check() {
        try {
            Iterator<Group> groupIt = authorizable.memberOf();
            while (groupIt.hasNext()) {
                Group group = groupIt.next();
                if (allowedGroups.contains(group.getPrincipal().getName())) {
                    return true;
                }
            }
        } catch (RepositoryException e) {
            return false; // In case of RepositoryException return false.
        }
        return false;
    }
}