package com.juniper.aem.helpers;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = SlingHttpServletRequest.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageHelper {
    protected final Logger Logger = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private String fileReference;

    private String altText = "";
    
    private String imageRenditionPath = "";

    @PostConstruct
    protected void init() {
        if (resourceResolver != null && fileReference != null) {
            Resource resource = resourceResolver.getResource(fileReference);
            if(resource != null) {
                
                Asset asset = resource.adaptTo(Asset.class);
                if (asset != null) {
                    altText = asset.getMetadataValue(DamConstants.DC_TITLE);
                    this.imageRenditionPath=LinkUtil.addWebRendition(resourceResolver, this.fileReference);
                }
            }
        }
    }
}