package com.juniper.aem.servlets;


import com.juniper.aem.services.EmailService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(immediate = true,
        service = Servlet.class,
        property = {
                Constants.SERVICE_DESCRIPTION + "= Email Test Servlet.",
                "sling.servlet.methods=" + HttpConstants.METHOD_GET,
                "sling.servlet.paths=" + "/bin/juniper/emailtest",
                "sling.servlet.extensions=" + "html"
        })
public class EmailTestServlet extends SlingAllMethodsServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(EmailTestServlet.class);

    //@Reference
    // replace your email service here private EmailService emailService;
    private EmailService emailService;

    @Reference
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;

    }


    @Override
    public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        LOG.info("Executing get of EmailTestServlet");

        String templatePath = "/etc/notification/email/default/emailTemplate.txt";

        //Set the dynamic variables of your email template
        Map<String, String> emailParams = new HashMap<String, String>();
        emailParams.put("body", "hello there");

        //  Customize the sender email address - if required
        emailParams.put("senderEmailAddress", "rjgutha@juniper.net");
        emailParams.put("senderName", "Ranjith Gutha");
        emailParams.put("ccrecipient", "sarshana@juniper.net");

        // Array of email recipients
        String[] recipients = {"rjgutha@juniper.net"};

        // emailService.sendEmail(..) returns a list of all the recipients that could not be sent the email
        // An empty list indicates 100% success
        List<String> failureList = emailService.sendEmail(templatePath, emailParams, recipients);

        if (failureList.isEmpty()) {
            LOG.info("Email sent successfully to the recipients");
        } else {
            LOG.info("Failed - Sending Email Failed");
        }

        response.setHeader("Content-Type", "text/html");
        response.getWriter().print("<h1>Sling Email Service Called</h1>");
        response.getWriter().close();

    }
}
