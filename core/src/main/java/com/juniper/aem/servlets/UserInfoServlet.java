package com.juniper.aem.servlets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.juniper.aem.pojos.UserProfile;
import com.juniper.aem.utils.Constants;
import org.apache.http.HttpStatus;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.oak.spi.security.user.UserConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=get",
                "sling.servlet.paths=" + "/bin/juniper/user-info"
        })
@ServiceDescription("User Information Servlet")
public class UserInfoServlet extends SlingSafeMethodsServlet {
    private static final Logger LOG = LoggerFactory.getLogger(UserInfoServlet.class);
    private transient Gson gson;

    @Override
    public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException {
        response.setContentType(Constants.MEDIA_TYPE_APPLICATION_JSON);
        response.setCharacterEncoding(Constants.CHARSET_UTF_8);

        ResourceResolver resourceResolver = request.getResourceResolver();
        JSONObject jsonResponse = new JSONObject();
        try {
            User user = resourceResolver.adaptTo(User.class);
            boolean isLoggedIn = user != null && !user.getID().equals(UserConstants.DEFAULT_ANONYMOUS_ID);
            if (isLoggedIn) {
                UserProfile userProfile = new UserProfile(user);
                jsonResponse.put("userProfile", getGson().toJson(userProfile));
            }
            jsonResponse.put("isLoggedIn", isLoggedIn);
        } catch (RepositoryException | JSONException exception) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.sendError(HttpStatus.SC_INTERNAL_SERVER_ERROR, Constants.ERROR_MSG_UNKNOWN);
            LOG.error(Constants.ERROR_MSG_UNKNOWN, exception);
        }

        response.getWriter().write(jsonResponse.toString());
        response.getWriter().flush();
    }

    private Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder().create();
        }
        return gson;
    }
}
