package com.juniper.aem.servlets;


import com.juniper.aem.services.GlobalSearchService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=get",
                "sling.servlet.extensions=json",
                "sling.servlet.paths=" + "/bin/juniper/global-search"
        })
@ServiceDescription("Global Search Servlet")
public class GlobalSearchExternalServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = -4803951146574459499L;

    @Reference
    private GlobalSearchService globalSearchService;

    @Override
    public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException {
        GlobalSearchServletUtil.externalSearch(request, response, globalSearchService);
    }
}
