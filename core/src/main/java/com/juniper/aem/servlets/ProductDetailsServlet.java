package com.juniper.aem.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Servlet;

import com.juniper.aem.models.ProductModel;
import com.juniper.aem.services.ProductService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component(immediate=true,
    service=Servlet.class,
    property={
        Constants.SERVICE_DESCRIPTION + "=Property Overview Servlet.",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.resourceTypes=" + "/apps/juniper/components/structure/page",
        "sling.servlet.selectors=" + "product-overview",
        "sling.servlet.extensions=" + "html"
            })
public class ProductDetailsServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(ProductDetailsServlet.class);

    private ProductService productService;
    @Reference
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
    	try {
            if (request != null) {
                Resource resource = request.getResource();
                ValueMap valueMap = resource.getValueMap();
                ProductModel productModel = productService.getProduct(valueMap.get("productDataPath", ""));
                if (productModel != null) {
                    response.setContentType("application/json");
                    response.setCharacterEncoding("UTF-8");
                    PrintWriter out = null;
                    try {
                        out = response.getWriter();
                        Gson gson = new GsonBuilder().setPrettyPrinting().create();
                        out.print(gson.toJson(productModel));
                    } catch (IOException e) {
                        log.error("Error in writing response: ", e);
                    } finally {
                        out.close();
                    }
                }
            }
    	} catch (Exception exception) {
    		log.error("Error in writing response: ", exception);
    	}
    }
}
