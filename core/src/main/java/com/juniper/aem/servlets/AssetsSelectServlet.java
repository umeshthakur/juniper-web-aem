package com.juniper.aem.servlets;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.commerce.common.ValueMapDecorator;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.juniper.aem.models.ProductModel;
import com.juniper.aem.services.ProductService;

@Component(immediate = true, service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "= Product Assets Dropdown",
        "sling.servlet.resourceTypes=" + "/apps/dropDownListing"
})
public class AssetsSelectServlet extends SlingSafeMethodsServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(AssetsSelectServlet.class);

    private ProductService productService;

    @Reference
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Reference
    private QueryBuilder builder;

    private Session session;

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {

        try {
            log.debug("In doGet() of AssetsSelectServlet");
            List<Resource> ResourceList = new ArrayList<>();
            String pagePath = request.getRequestPathInfo().getSuffix().replaceAll("/jcr:content/.*$", "");
            log.debug("Page path is: " + pagePath);
            ResourceResolver resourceResolver = request.getResourceResolver();
            session=resourceResolver.adaptTo(Session.class);

            Resource pageResource = resourceResolver.getResource(pagePath);
            if (pageResource != null) {
                log.debug("Page resource is not null... getting current page");
                String productPath = "";
                Page currentPage = pageResource.adaptTo(Page.class);
                if (currentPage != null) {
                    log.debug("Current page is not null... getting page properties");
                    productPath = currentPage.getProperties().get("productDataPath", String.class);
                    log.debug("Product path is: " + productPath);
                    ProductModel productModel = productService.getProduct(productPath);
                    if (productModel != null && productModel.getTags() != null) {
                        log.debug("Product model is not null... getting tags and doing query things");
                        Arrays.stream(productModel.getTags()).forEach(tag -> log.debug("tagName: " + tag));

                        Map<String, String> map = new HashMap<>();

                        map.put("path", "/content/dam");
                        map.put("type", "dam:Asset");
                        map.put("property", "jcr:content/metadata/cq:tags");
                        map.put("property.or", "true");
                        map.put("p.limit", "-1");

                        String tags[] = productModel.getTags();

                        for(int i = 0; i < tags.length; i++)
                        {
                            map.put("property." + i + 1 + "_value", tags[i]);
                        }

                        Query query = builder.createQuery(PredicateGroup.create(map), session);
                        log.debug("query" + query.getPredicates().toString());
                        SearchResult result = query.getResult();
                        ValueMap vm = new ValueMapDecorator(new HashMap<>());
                        //populate empty option in dropdown
                        vm.put("value","");
                        vm.put("text","");
                        ResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));

                        // iterating over the results
                        for (Hit hit : result.getHits()) {
                            String path = hit.getPath();
                            log.debug("path"+path);
                            // Create a result element
                            Resource asset = resourceResolver.getResource(path);
                            String name=asset.getName();
                            if(name.contains(".pdf") || name.contains(".docx") || name.contains(".doc")) {
                                vm = new ValueMapDecorator(new HashMap<>());
                                //populate the map
                                vm.put("value", path);
                                vm.put("text", name);
                                ResourceList.add(new ValueMapResource(resourceResolver, new ResourceMetadata(), "nt:unstructured", vm));
                            }
                        }

                        DataSource ds = new SimpleDataSource(ResourceList.iterator());
                        request.setAttribute(DataSource.class.getName(), ds);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error in Get Drop Down Values", e);
        }
    }
}
