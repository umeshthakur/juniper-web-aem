package com.juniper.aem.servlets;


import com.google.gson.JsonObject;
import com.juniper.aem.pojos.SearchParameters;
import com.juniper.aem.services.GlobalSearchService;
import com.juniper.aem.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.oak.spi.security.user.UserConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GlobalSearchServletUtil {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalSearchServletUtil.class);

    private GlobalSearchServletUtil() {
    }

    public static void internalSearch(final SlingHttpServletRequest request, final SlingHttpServletResponse response, final GlobalSearchService globalSearchService) throws IOException {
        doGet(request, response, globalSearchService, false);
    }

    public static void externalSearch(final SlingHttpServletRequest request, final SlingHttpServletResponse response, final GlobalSearchService globalSearchService) throws IOException {
        doGet(request, response, globalSearchService, true);
    }

    private static void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response, final GlobalSearchService globalSearchService, final Boolean isExternal)
            throws IOException {
        response.setContentType(Constants.MEDIA_TYPE_APPLICATION_JSON);
        response.setCharacterEncoding(Constants.CHARSET_UTF_8);
        JsonObject jsonResponse = new JsonObject();
        String query = request.getParameter("query");
        String language = request.getParameter("language");
        String format = request.getParameter("format");
        String product = request.getParameter("product");
        String section = request.getParameter("section");
        String page = request.getParameter("page");
        int pageIndex = 0;
        if (StringUtils.isNotBlank(page)) {
            pageIndex = Integer.parseInt(page);
        }
        try {
            if (StringUtils.isNotBlank(query)) {
                ResourceResolver resourceResolver = request.getResourceResolver();
                User user = resourceResolver.adaptTo(User.class);
                Boolean isLoggedIn = user != null && !user.getID().equals(UserConstants.DEFAULT_ANONYMOUS_ID);
                String userToken = "";
                
                if(isLoggedIn) {                    
                    userToken = globalSearchService.getSecureTokenForUser(user, request);                                        
                    LOG.info("token received : {} " , userToken);                                      
                }
                
                if (isExternal) {
                    jsonResponse = globalSearchService.getCoveoRawResponse(query, pageIndex, isLoggedIn);
                } else {
                    Resource currentResource = request.getResource();
                    SearchParameters searchParameters = new SearchParameters(query, language, format, product, section, pageIndex);
                    jsonResponse = globalSearchService.getResults(searchParameters, isLoggedIn, currentResource);
                }
            } else {
                response.setStatus(HttpStatus.SC_BAD_REQUEST);
                response.sendError(HttpStatus.SC_BAD_REQUEST, "Query value is invalid.");
                LOG.warn("Servlet has been called with empty parameters");
            }
        } catch (Exception exception) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.sendError(HttpStatus.SC_INTERNAL_SERVER_ERROR, Constants.ERROR_MSG_UNKNOWN);
            LOG.error(Constants.ERROR_MSG_UNKNOWN, exception);
        }
        response.getWriter().write(jsonResponse.toString());
        response.getWriter().flush();
    }
}
