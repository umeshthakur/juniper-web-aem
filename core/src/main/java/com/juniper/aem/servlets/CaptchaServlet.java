package com.juniper.aem.servlets;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.json.JSONException;
import org.json.JSONObject;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.juniper.aem.services.RecaptchaService;

@Component(immediate = true, service = Servlet.class, property = { "sling.servlet.methods=" + HttpConstants.METHOD_POST,
        "sling.servlet.resourceTypes=" + "/apps/juniper/components/structure/page",
        "sling.servlet.paths=" + "/bin/juniper/captcha", "sling.servlet.extensions=" + "html" })
public class CaptchaServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    private final static String USER_AGENT = "Mozilla/5.0";
    private static final Logger LOG = LoggerFactory.getLogger(CaptchaServlet.class);

    @Reference
    private RecaptchaService recaptchaService;

    @Override
    public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Content-Type", "application/json");
        response.getWriter().print("{'test':'test'}");
        response.getWriter().close();
    }

    @Override
    public void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        JSONObject json = new JSONObject();
        String gResponse = "";
        String captchaValid = "false";

        Map<String, String> formFields = new HashMap<String, String>();

        String jspResponse = "";

        // save form data
        try {
            Enumeration paramNames = request.getParameterNames();
            while (paramNames.hasMoreElements()) {
                String paramName = (String) paramNames.nextElement();
                formFields.put(paramName, request.getParameter(paramName));
            }

        } catch (Exception e) {
            LOG.info("Error in reading form values ", e);
        }

        // recaptcha validation
        if (StringUtils.isNotBlank(formFields.get("g-recaptcha-response"))) {
        
            gResponse = formFields.get("g-recaptcha-response");

        } else if (StringUtils.isNotBlank(formFields.get("g-recaptcha-custom"))) {
            gResponse = formFields.get("g-recaptcha-custom");
        }
        if (!gResponse.isEmpty()) {

            captchaValid = this.verifyGlobal(gResponse, request);

            if (captchaValid.equalsIgnoreCase("false")) {
                jspResponse = "invalid-captcha";
            }
        } else {
            jspResponse = "invalid-captcha";
        }
        if (captchaValid.equalsIgnoreCase("true")) {

            jspResponse = "captcha valid";
        }
        try {
            json.put("response", jspResponse);
        } catch (JSONException e) {

            LOG.info("Error in upading JSON ", e);
        }
        PrintWriter out11 = response.getWriter();
        out11.println(json.toString());
        out11.close();

        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jspResponse);
    }

    public String verifyGlobal(String gRecaptchaResponse, SlingHttpServletRequest request) throws IOException {

        if (gRecaptchaResponse == null || "".equals(gRecaptchaResponse)) {
            return "false";
        }
        try {
            URL obj = new URL(recaptchaService.getRecaptchaUrl());
            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            // add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setConnectTimeout(20000);

            String postParams = "secret=".concat(recaptchaService.getRecaptchaSecretKey()).concat("&response=")
                    .concat(gRecaptchaResponse);

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(postParams);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(response.toString()).getAsJsonObject();
            String gSuccess = jsonObject.get("success").getAsString();

            return gSuccess;

        } catch (Exception e) {
            LOG.error("Exception in CaptchaServlet > verifyGlobal" + e);
            return "false";
        }
    }

}