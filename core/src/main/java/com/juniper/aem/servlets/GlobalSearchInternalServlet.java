package com.juniper.aem.servlets;


import com.juniper.aem.services.GlobalSearchService;
import com.juniper.aem.utils.Constants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.propertytypes.ServiceDescription;

import javax.servlet.Servlet;
import java.io.IOException;

@Component(service = Servlet.class,
        property = {
                "sling.servlet.methods=GET",
                "sling.servlet.extensions=json",
                "sling.servlet.selectors=results",
                "sling.servlet.resourceTypes=" + Constants.GLOBAL_SEARCH_TYPE
        })
@ServiceDescription("Global Search Internal Servlet")
public class GlobalSearchInternalServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = -4803951146574459499L;

    @Reference
    private GlobalSearchService globalSearchService;

    @Override
    public void doGet(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws IOException {
        GlobalSearchServletUtil.internalSearch(request, response, globalSearchService);
    }
}
