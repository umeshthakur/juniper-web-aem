package com.juniper.aem.servlets;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


@Component(immediate = true,
        service = Servlet.class,
        property = {
                Constants.SERVICE_DESCRIPTION + "= Feedback Survey Servlet",
                "sling.servlet.methods=" + HttpConstants.METHOD_POST,
                "sling.servlet.paths=" + "/bin/juniper/feedbacksurvey",
                "sling.servlet.extensions=" + "html"
        })

public class FeedbackSurveyServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(FeedbackSurveyServlet.class);

    @Override
    public void doPost(final SlingHttpServletRequest request, final SlingHttpServletResponse response)
            throws ServletException, IOException {

        HttpClient client = new DefaultHttpClient();
        HttpPost postrequest = new HttpPost("https://dataimportapi.allegiancetech.com/api/ImportRequest");
        postrequest.addHeader("Content-Type", "application/json;charset=UTF-8");
        postrequest.addHeader("API-version", "3.0");
        postrequest.addHeader("Authentication-Token", request.getParameter("TOKEN"));
        HttpResponse postresponse = null;

        String name = request.getParameter("name");
        String address = request.getParameter("address");
        String Q1 = request.getParameter("feedback_topic");
        String Q2 = request.getParameter("got_info");
        String Q2_Comments = request.getParameter("expected_data");
        String Q3_Comments = request.getParameter("problems");
        String Q4_URL = StringEscapeUtils.escapeHtml(request.getParameter("url"));
        String Email = StringEscapeUtils.escapeHtml(request.getParameter("C_EmailAddress"));
        String hidden_url = StringEscapeUtils.escapeHtml(request.getParameter("hidden_url"));
        String language = StringEscapeUtils.escapeHtml(request.getParameter("lang"));
        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);
        String CompletedDate = df.format(new Date());

        LOG.debug("Q1:" + Q1 + ":Q2:" + Q2 + ":Q2_Comments:" + Q2_Comments + ":Q3_Comments:" + Q3_Comments
                + ":Q4_URL:" + Q4_URL + ":Email:" + Email + ":hidden_url:" + hidden_url + ":CompletedDate:"
                + CompletedDate);
        try {

            HttpEntity entity_http = new StringEntity("{\r\n" + "  \"surveyCode\": \""
                    + "DEP8PW" + "\",\r\n" + "  \"sendAlerts\": "
                    + "false" + ",\r\n" + "  \"name\": \"" + "Feedback Footer Survey Import"
                    + "\",\r\n" + "  \"notificationEmails\": \"" + "rjgutha@juniper.net"
                    + "\",\r\n" + "  \"Respondents\": [\r\n" + "    {\r\n" + "       \"CompletedDate\": \""
                    + CompletedDate + "\",\r\n" + "      \"Responses\": {\r\n" + "        \"Q1\": \"" + Q1
                    + "\",\r\n" + "        \"Q2\": \"" + Q2 + "\",\r\n" + "        \"Q2_Comments\": \""
                    + Q2_Comments + "\",\r\n" + "        \"Q3_Comments\": \"" + Q3_Comments + "\",\r\n"
                    + "        \"Q4_URL\": \"" + Q4_URL + "\",\r\n" + "        \"Name\": \"Feedback Page\",\r\n"
                    + "        \"Email\": \"" + Email + "\",\r\n" + "        \"Q4_URL_HIDE\": \"" + hidden_url
                    + "\",\r\n" + "		 \"Language\": \"" + language + "\"\r\n" + "            }\r\n"
                    + "        }\r\n" + "    ]\r\n" + "}\r\n" + "", "UTF-8");
            LOG.debug("Http Entity:" + entity_http.toString());
            postrequest.setEntity(entity_http);
            postresponse = client.execute(postrequest);
            if (postresponse.getStatusLine().getStatusCode() == 202) {
                response.sendRedirect(request.getParameter("sucess_page"));
            } else {
                response.sendRedirect(request.getParameter("failure_page"));
            }

        } catch (ClientProtocolException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        } catch (RuntimeException ex) {
            postrequest.abort();
            ex.printStackTrace();
            throw ex;
        } finally {
            try {
                client.getConnectionManager().shutdown();

            } catch (Exception e) {

                e.printStackTrace();

            }
        }

    }


}
