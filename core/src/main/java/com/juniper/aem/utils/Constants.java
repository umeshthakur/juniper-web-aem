package com.juniper.aem.utils;

public final class Constants {
    /**
     * Page Properties
     */
    public static final String PROP_ADDITIONAL_TITLE = "additionalTitle";
    public static final String PROP_CANONICAL_URL = "canonicalUrl";
    public static final String PROP_PAGE_TITLE = "pageTitle";
    /**
     * SEO Constants
     */
    public static final String PROP_OG_LOCALE = "ogLocale";
    public static final String PROP_OG_TITLE = "ogTitle";
    public static final String PROP_OG_DESCRIPTION = "ogDescription";
    public static final String PROP_OG_IMAGE = "ogImage";

    public static final String PROP_TWITTER_CARD = "twitterCard";
    public static final String PROP_TWITTER_TITLE = "twitterTitle";
    public static final String PROP_TWITTER_DESCRIPTION = "twitterDescription";
    public static final String PROP_TWITTER_IMAGE = "twitterImage";
    public static final String PROP_TWITTER_IMAGE_ALT_TEXT = "twitterImageAltText";

    public static final String PROP_ADDITIONAL_META_DATA = "additionalMetadata";
    public static final String PROP_KEYWORDS = "keywords";

    /**
     * Miscellaneous
     */
    public static final String URL_EXTENSION = ".html";

    public static final String JUNIPER_TAGS_NAMESPACE = "juniper:";

    public static final String PRODUCT_PAGE = "juniper/components/structure/product-detail";

    public static final String SOLUTION_PAGE = "juniper/components/structure/product-detail";
    
    public static final String SOLUTION_TEMPLATE = "/conf/juniper/settings/wcm/templates/solution-page";
    
    public static final String PRODUCT_TEMPLATE = "/conf/juniper/settings/wcm/templates/product-detail";
    
    public static final String PRODUCT_FAMILY_LEVEL1_TEMPLATE = "/conf/juniper/settings/wcm/templates/product-family-lvl-1";
    
    public static final String PRODUCT_FAMILY_LEVEL2_TEMPLATE = "/conf/juniper/settings/wcm/templates/product-family-lvl-2";
    
    public static final String CONTENT_PAGE_TEMPLATE = "/conf/juniper/settings/wcm/templates/content-page";

    public static final String CASE_STUDY_PAGE = "juniper/components/structure/case-study";
    
    public static final String CQ_TEMPLATE = "cq:template";

    public static final String HOME_PAGE = "homepage";

    public static final String PRODUCT_DATA_PATH = "productDataPath";

    public static final String GLOBAL_SEARCH_TYPE = "juniper/components/global-search";

    /**
     * Analytics
     */

    public static final String CATEGORY = "category";

    public static final String SUBCATEGORY = "subcategory";

    public static final String CARD_IMAGE_PATH = "cardImagePath";
    
    public static final String IMAGE_PATH = "imagePath";
    
    public static final String IMAGE = "image";
    
    public static final String SUMMARY = "summary";
    
    public static final String FILE_REFERENCE = "fileReference";
    
    public static final String VIDEO_THUMBNAIL_IMAGE_PATH = "videothumbnailimagepath";

    /** HTTP constants. **/
    public static final String MEDIA_TYPE_APPLICATION_JSON = "application/json";
    public static final String MEDIA_TYPE_APPLICATION_XML = "application/xml";
    public static final String CHARSET_UTF_8 = "utf-8";

    /** Error messages. **/
    public static final String ERROR_MSG_UNKNOWN = "Unknown error";
    
    /** Recaptcha. **/
    public static final String RECAPTCHA_URL = "https://www.google.com/recaptcha/api/siteverify";
    
    public static final String RECAPTCHA_SECRET_KEY = "6LdSLUcUAAAAANjmeiz3qBkeQI2v51kNtXl43BZS";
    
    public static final String DEFAULT_LANG_MASTER_PAGE_PATH = "/content/juniper/language-masters";

    public static final String UTILS_PAGE_PATH = "/content/juniper/utils";
    
    public static final String DEFAULT_LANG_PAGE_PATH = "/content/juniper/us/en";

    /** Juniper InMoment Feedback Survey **/

    public static final String INMOMENT_API_URL = "https://dataimportapi.allegiancetech.com/api/Authenticate";

    public static final String INMOMENT_USER_NAME = "EXT-Teamsite-Developers@juniper.net";

    public static final String INMOMENT_COMPANY_NAME = "junipernetworks.allegiancetech.com";

    public static final String INMOMENT_PASSWORD = "Maritzcx123!";


    private Constants() {
    }
}
