package com.juniper.aem.utils;

import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import java.io.IOException;

/**
 * A helper interface that allows you to implement an executable using a {@link ResourceResolver}.
 *
 * @param <T> type that executable will return
 * @see JcrUtil#withResourceResolver(ResourceResolverFactory, ExecutableWithResourceResolver) for usage
 */
public interface ExecutableWithResourceResolver<T> {

  /**
   * An API to implement that needs a {@link ResourceResolver}.
   *
   * @param resourceResolver The Sling Resource Resolver
   * @return Result of execution
   * @throws PersistenceException in case of errors.
   */
  T execute(ResourceResolver resourceResolver) throws IOException;
}
