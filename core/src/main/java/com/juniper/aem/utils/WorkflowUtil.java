package com.juniper.aem.utils;

import java.util.Collections;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.commons.lang3.StringUtils;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowSession;
import com.day.cq.replication.Agent;
import com.day.cq.replication.AgentFilter;
import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.ReplicationOptions;
import com.day.cq.replication.Replicator;

public class WorkflowUtil {
    protected final static Logger log = LoggerFactory.getLogger(WorkflowUtil.class);

    public static boolean activateContent(Replicator replicator, String path, WorkflowSession wfsession,
            String replicationAgentName) {
        boolean replicationStatus = false;
        Session session = wfsession.adaptTo(Session.class);
        ReplicationOptions options = new ReplicationOptions();
        options.setSuppressVersions(true);
        options.setSynchronous(true);
        options.setSuppressStatusUpdate(false);

        options.setFilter(new AgentFilter() {
            public boolean isIncluded(final Agent agent) {
                return StringUtils.startsWithIgnoreCase(agent.getId(), replicationAgentName);
            }
        });

        try {
            replicator.replicate(session, ReplicationActionType.ACTIVATE, path, options);
            replicationStatus = replicator.getReplicationStatus(session, path).isDelivered();
        } catch (ReplicationException e) {
            log.error("ReplicationException:", e);
        }
        return replicationStatus;
    }
    public static boolean deActivateContent(Replicator replicator, String path, WorkflowSession wfsession,
            String replicationAgentName) {
        boolean replicationStatus = false;
        Session session = wfsession.adaptTo(Session.class);
        ReplicationOptions options = new ReplicationOptions();
        options.setSuppressVersions(true);
        options.setSynchronous(true);
        options.setSuppressStatusUpdate(false);

        options.setFilter(new AgentFilter() {
            public boolean isIncluded(final Agent agent) {
                return StringUtils.startsWithIgnoreCase(agent.getId(), replicationAgentName);
            }
        });

        try {
            replicator.replicate(session, ReplicationActionType.DEACTIVATE, path, options);
            replicationStatus = replicator.getReplicationStatus(session, path).isDelivered();
        } catch (ReplicationException e) {
            log.error("ReplicationException:", e);
        }
        return replicationStatus;
    }

    public static String getEmail(User member) {

        String senderEmailAddress = "";
        String UserId;
        try {
            UserId = member.getID();

            Value Value[] = member.getProperty("./profile/email");
            if (Value != null && Value.length > 0) {
                senderEmailAddress = Value[0].toString();
            }
            if (senderEmailAddress.isEmpty()) {
                senderEmailAddress = UserId + "@juniper.net";
            }
        } catch (RepositoryException e1) {
            log.error("Error in reading Email" + e1);
        }
        return senderEmailAddress;
    }

    public static ResourceResolver getResourceResolver(ResourceResolverFactory resourceResolverFactory, Session session)
            throws LoginException {
        return resourceResolverFactory.getResourceResolver(
                Collections.<String, Object>singletonMap(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session));

    }

}
