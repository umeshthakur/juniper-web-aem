package com.juniper.aem.utils;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.google.gson.JsonObject;

public class Utils {

	private final static String ACS_IMAGE_TRANSFORM_EXTENSION = "transform";

	public static String formatLink(String link) {
		link = StringUtils.trimToNull(link);

		if (StringUtils.isNotBlank(link)) {
			if (link.startsWith("/content") && !link.startsWith("/content/dam") && !link.endsWith(".html")) {
				link += ".html";
			}
		} else {
			link = "#";
		}

		return link;
	}

	public static boolean isInternalLink(String link) {
		link = StringUtils.trimToNull(link);

		return (StringUtils.isNotBlank(link) && link.startsWith("/content/juniper"));
	}

	public static String getId(String prefix, Resource resource) {
		return prefix + "-" + String.valueOf(Math.abs(resource.getPath().hashCode() - 1));
	}

	/**
	 * Returns the locale of the Page
	 *
	 * @param page
	 * @return The current locale of the page
	 */
	public static Locale getLocale (Page page) {
		return page.getLanguage(false);
	}

	public static Resource getOrCreateResource(String resourceName, Resource parentResource, ResourceResolver resourceResolver) throws PersistenceException {
		Resource resource = null;
		if (resourceResolver != null && parentResource != null) {
			resource = resourceResolver.getResource(parentResource.getPath() + "/" + resourceName);
			if (resource == null) {
				Map<String, Object> props = new HashMap<>();
				props.put(JcrConstants.JCR_PRIMARYTYPE, "oak:Unstructured");
				resource = resourceResolver.create(parentResource, resourceName, props);
			}
		}
		return resource;
	}

	/**
	 * Return the transformed image url using ACS Commons Named Image Transform feature
	 * https://adobe-consulting-services.github.io/acs-aem-commons/features/named-image-transform/index.html
	 */
	public static String getNamedTransformedImagePath(String imagePath, String transformName) {
		if(StringUtils.isNotBlank(imagePath)) {
			if (StringUtils.isNotBlank(transformName)) {
				String extension = StringUtils.substringAfterLast(imagePath, ".");
				if (StringUtils.isNotBlank(extension)) {
					return imagePath + "." + ACS_IMAGE_TRANSFORM_EXTENSION + "/" + transformName + "/" + "image." + extension;
				}
			}
			return imagePath;
		}
		return StringUtils.EMPTY;
	}

	/**
     * Convert ResourceBundle into a Map object.
     *
     * @param resource a resource bundle to convert.
     * @return Map a map version of the resource bundle.
     */
    public static Map<String, String> convertResourceBundleToMap(ResourceBundle resource) {
        Map<String, String> map = new HashMap<>();
        Enumeration<String> keys = resource.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            map.put(key, resource.getString(key));
        }
        return map;
	}

	/**
     * Convert ResourceBundle into a Json object.
     *
     * @param resource a resource bundle to convert.
     * @return JsonObject of the resource bundle.
     */
    public static JsonObject convertResourceBundleToJsonObject(ResourceBundle resource) {
        JsonObject json = new JsonObject();
        Set<String> keys = resource.keySet();
        for (String key : keys) {
			json.addProperty(key, resource.getString(key));
        }
        return json;
	}
    
    
    public static String getCountry(Page siteRoot, Page homePage) {
        String country = "";
        if (siteRoot != null) {
            Iterator<Page> childPages = siteRoot.listChildren();
            while (childPages.hasNext()) {
                Page langPage = childPages.next();
                country = langPage.getLanguage().getCountry();
                if (StringUtils.isBlank(country)) {
                    country = homePage.getLanguage().getCountry();
                } 
                if(StringUtils.isNotEmpty(country)) {
                    return country.toLowerCase();
                }
            }
        }
        return country;
    }

}
