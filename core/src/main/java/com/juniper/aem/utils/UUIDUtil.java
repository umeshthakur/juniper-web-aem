package com.juniper.aem.utils;

import lombok.Getter;

import java.util.Random;

@Getter
public class UUIDUtil {

    private UUIDUtil() {
    }

    public static String getUniqueId() {
        Random r = new Random();
        return String.valueOf(r.nextInt(Integer.MAX_VALUE));
    }

}