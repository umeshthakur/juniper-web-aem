package com.juniper.aem.utils;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Session;

import org.apache.jackrabbit.api.JackrabbitSession;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * A Simple utility class containing helper methods to work with the JCR.
 */
@Slf4j
public final class JcrUtil {

    private static final String JUNIPER_AUTHENTICATION_SERVICE = "juniper-service";

    /**
     * Private Constructor.
     */
    private JcrUtil() {
        throw new IllegalStateException("Cannot call the constructor");
    }


    /**
     * Provides a simple utility to execute any code that needs a Sling Resource Resolver This API closes the resource
     * resolver at the end of the execution. Thus, if you are returning anything, it should be an object that does not
     * rely on the ResourceResolver object to be still live
     *
     * @param <T> type that will be returned
     * @param resolverFactory The Resource Resolver Factory
     * @param executableWithResourceResolver The code executable
     * @return Result of execution
     */
    public static <T> T withResourceResolver(ResourceResolverFactory resolverFactory,
                                             ExecutableWithResourceResolver<T> executableWithResourceResolver) {
        ResourceResolver resolver = null;
        try {
            final Map<String, Object> authInfo = new HashMap<>();
            authInfo.put(ResourceResolverFactory.SUBSERVICE, JUNIPER_AUTHENTICATION_SERVICE);
            resolver = resolverFactory.getServiceResourceResolver(authInfo);
            return executableWithResourceResolver.<T>execute(resolver);
        } catch (Exception e) {
            throw new IllegalStateException("Exception when executing with the resourceResolver.", e);
        } finally {
            if (resolver != null && resolver.isLive()) {
                resolver.close();
            }
        }
    }

    /**
     * This method returns JackrabbitSession instance
     * @param jcrSession the JCR Session
     * @return JackrabbitSession only
     */
    public static JackrabbitSession getJackrabbitSession(Session jcrSession) {
        if ( jcrSession instanceof JackrabbitSession)  {
            return (JackrabbitSession) jcrSession;
        } else {
            return null;
        }
    }
}
