package com.juniper.aem.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;

import com.day.cq.wcm.api.Page;

public class LinkUtil {

    /**
     * If the specified URL points to a CQ Page, add the <code>.html</code> suffix to the URL.
     *
     * @param resourceResolver The ResourceResolver to resolve the URL.
     * @param url The URL to test if it links to a CQ Page.
     * @return the url, possibly updated to have <code>.html</code> appended to it.
     */
    public static String addHTMLIfPage(ResourceResolver resourceResolver, String url) {
        if (StringUtils.isEmpty(url) || resourceResolver == null) {
            return url;
        }

        Resource pageResource = resourceResolver.getResource(url);
        Page page = (pageResource != null && !ResourceUtil.isNonExistingResource(pageResource)) ?
                pageResource.adaptTo(Page.class) : null;

        if (page == null) {
            return url;
        }
        return resourceResolver.map(url) + ".html";
    }
    
    public static String addWebRendition(ResourceResolver resourceResolver, String url) {

        if (StringUtils.isEmpty(url) || resourceResolver == null) {
            return url;
        }
        
        String rendition = url;
        if(url.contains(".jpg") || url.contains(".jpeg")) {
            rendition += "/jcr:content/renditions/cq5dam.web.1280.1280.jpeg";
        } else if(url.contains(".png")) {  
          rendition += "/jcr:content/renditions/cq5dam.web.1280.1280.png";
        }  
        return rendition;
    }
}