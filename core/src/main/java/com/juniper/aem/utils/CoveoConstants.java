package com.juniper.aem.utils;

public final class CoveoConstants {
    /**
     * API query parameters.
     */
    public static final String PARAM_QUERY = "q";
    public static final String PARAM_ADVANCED_QUERY = "aq";
    public static final String PARAM_RESULTS_START = "firstResult";
    public static final String PARAM_RESULTS_LIMIT = "numberOfResults";
    public static final String PARAM_LANGUAGE_FILTER = "@language";
    public static final String PARAM_FORMAT_FILTER = "@filetype";
    public static final String PARAM_SITE_SECTION_FILTER = "@section";
    public static final String PARAM_PRODUCT_FILTER = "@productfilter";
    public static final String PARAM_COMPARATOR = "=";
    public static final String PARAM_COMPARATOR_EXACT = "==";
    public static final String PARAM_COVEO_PIPELINE = "pipeline";
    

    /**
     * Response properties.
     */
    public static final String PROP_TOTAL_COUNT = "totalCount";
    public static final String PROP_PAGES_COUNT = "pagesCount";
    public static final String PROP_PAGE_NUMBER = "pageNumber";
    public static final String PROP_RESULTS = "results";
    public static final String PROP_RESULT_TITLE = "title";
    public static final String PROP_RESULT_URL = "uri";
    public static final String PROP_RESULT_EXCERPT = "excerpt";

    /**
     * Error messages.
     */
    public static final String ERROR_MSG_UNAVAILABLE = "Coveo service is unavailable";
    public static final String ERROR_MSG_UNKNOWN = "Internal Server error on Coveo.";
    public static final String ERROR_MSG_FORBIDDEN = "User is not allowed to do the requested action.";
    public static final String ERROR_MSG__MALFORMED_RESPONSE = "Malformed Coveo response.";

    private CoveoConstants() {
    }
}
