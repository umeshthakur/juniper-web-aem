package com.juniper.aem.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Session;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Getter
@Setter
@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExplainerCardsDataModel {
    private static final Logger LOG = LoggerFactory.getLogger(ExplainerCardsDataModel.class);

    @Inject
    private String imagePath;
    
    @Inject
    private String altText;
    
    @Inject
    private String title;
    
    @Inject
    private String titleLink;
    
    @Inject
    private String titleTarget;
    
    @Inject
    private String description;
    
    @Inject
    private String ctaText;
    
    @Inject
    private String ctaLink;
    
    @Inject
    private String ctaTarget;
  
    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        
        if(this.titleLink != null) {
        titleLink = LinkUtil.addHTMLIfPage(resourceResolver,titleLink);
        }
        
        if(this.ctaLink != null) {
            ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, this.ctaLink);
        }
    }
}