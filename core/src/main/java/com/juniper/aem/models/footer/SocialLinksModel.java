package com.juniper.aem.models.footer;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialLinksModel {
	
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @ValueMapValue
    private String socialLinkType;
    
    @ValueMapValue
    private String socialPagePath;

    @ValueMapValue
    private String openSocialInNewWindow;
    
    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
	protected void init() {
    	openSocialInNewWindow = LinkUtil.addHTMLIfPage(resourceResolver, openSocialInNewWindow);
	}
}