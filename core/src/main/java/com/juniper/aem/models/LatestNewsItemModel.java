package com.juniper.aem.models;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LatestNewsItemModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String eyebrowText;

    @Inject
    private String titleText;

    @Inject
    private String introduction;

    @Inject
    private String imagePath;

    @Inject
    private String imageAltText;

    @Inject
    private String link;

    @Inject
    private String playIconColor;

    @Inject
    private String videoPath;

    @Inject
    private String openInNewTab;

    @SlingObject
    private ResourceResolver resourceResolver;

    private String ctaLink;

    @PostConstruct
    protected final void init() {
        ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, link);

//        if(this.imagePath != null) {
//            imagePath = LinkUtil.addWebRendition(resourceResolver, imagePath);
//        }
    }

    public boolean getIsVideo() {
        boolean isVideo = false;
        if(videoPath != null && !videoPath.isEmpty())
            isVideo = true;
        return isVideo;
    }

}
