package com.juniper.aem.models.form;

import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormTextFamilyModel {

    @Inject
    private String eyebrowDescriptor;

    @Inject
    private String headerTextTitle;

    @Inject
    private String intro;

    @Inject
    private String fileReference;

    @Inject
    private String altText;

    @Inject
    private String showBackgroundCircle;
    
    @Inject
    private String showBackgroundSquare;
    
    @Inject
    private String imageLink;

    @Inject
    private String imageLinkTarget;
    
    @Inject
    private String ctaText;
    
    @Inject
    private String ctaColor;
    
    @Inject
    private String ctaLink;
    
    @Inject
    private String target;
    

}
