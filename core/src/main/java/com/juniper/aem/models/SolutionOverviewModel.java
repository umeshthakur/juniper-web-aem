package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SolutionOverviewModel {
	
	@ValueMapValue
    private String buttonLink;
	
	@ValueMapValue
    private String button2Link;
	
	@ValueMapValue
    private String headingLink;
	
	private String finalButtonLink;
	private String finalButton2Link;
	private String finalHeadingLink;


	@Inject
	@Source("sling-object")
	private ResourceResolver resourceResolver;
	
	@PostConstruct
    protected void init() {	
		
		finalButtonLink = LinkUtil.addHTMLIfPage(resourceResolver, buttonLink);
		finalButton2Link = LinkUtil.addHTMLIfPage(resourceResolver, button2Link);
		finalHeadingLink = LinkUtil.addHTMLIfPage(resourceResolver, headingLink);
    }
	

}
