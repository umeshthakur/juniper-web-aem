package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResouceCenterModel {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
    @Inject
    private List<ResourceCenterSubCategoryModel>  practicalresources;
    
    @Inject
    private List<ResourceCenterSubCategoryModel>  trainingandcommunity;
    
    @Inject
    private List<ResourceCenterSubCategoryModel>  bginformation;
    
    @Inject
    private List<ResourceCenterSubCategoryModel>  support;
    
    @Inject
    private List<ResourceCenterSubCategoryModel>  learnmore;
    
    @Inject
    private List<ResourceCenterContainerModels>  datasheet;

	@PostConstruct
	protected void init() {
	}
	
	public List<ResourceCenterSubCategoryModel> getPracticalresources() {
		return practicalresources;
	}

	public List<ResourceCenterSubCategoryModel> getTrainingandcommunity() {
		return trainingandcommunity;
	}

	public List<ResourceCenterSubCategoryModel> getBginformation() {
		return bginformation;
	}

	public List<ResourceCenterSubCategoryModel> getSupport() {
		return support;
	}

	public List<ResourceCenterSubCategoryModel> getLearnmore() {
		return learnmore;
	}

	public List<ResourceCenterContainerModels> getDatasheet() {
		return datasheet;
	}
	


	
}