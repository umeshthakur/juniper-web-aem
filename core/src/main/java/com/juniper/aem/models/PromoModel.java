package com.juniper.aem.models;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentVariation;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import org.apache.sling.api.resource.Resource;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.Optional;

@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PromoModel {
    public static final String MODEL_TITLE = "Promo";

    @Inject @Named("fragmentPath")
    private String fragmentPath;

    @Inject @Named("variationName")
    private String variation;


    @Inject
    ResourceResolver resourceResolver;

    private Optional<ContentFragment> contentFragment;

    @PostConstruct
    public void init() {
        Resource fragmentResource = resourceResolver.getResource(fragmentPath);
        if(fragmentResource != null)
            contentFragment = Optional.ofNullable(fragmentResource.adaptTo(ContentFragment.class));
        else
            contentFragment = Optional.ofNullable(null);

    }

    public String getModel() {
        return contentFragment
            .map(ContentFragment::getTemplate)
            .map(FragmentTemplate::getTitle)
            .orElse(StringUtils.EMPTY);
    }

    public String getTitle() {
        return contentFragment
            .map(cf -> cf.getElement("title"))
            .map(ContentElement::getContent)
            .orElse(StringUtils.EMPTY);
    }

    public String getDescription() {
        return contentFragment
            .map(cf -> cf.getElement("description"))
            .map(ContentElement::getContent)
            .orElse(StringUtils.EMPTY);
    }

    public String getIconClass() {
        if(isVariant()) {
            return contentFragment
                .map(cv -> cv.getElement("icon-class").getVariation(variation))
                .map(ContentVariation::getContent)
                .orElse(StringUtils.EMPTY);
        }
        else {
            return contentFragment
                .map(cf -> cf.getElement("icon_class"))
                .map(ContentElement::getContent)
                .orElse(StringUtils.EMPTY);

        }
    }

    public String getHeading() {
        if(isVariant()) {
            return contentFragment
                .map(cv -> cv.getElement("heading").getVariation(variation))
                .map(ContentVariation::getContent)
                .orElse(StringUtils.EMPTY);
        }
        else {
            return contentFragment
                .map(cf -> cf.getElement("heading"))
                .map(ContentElement::getContent)
                .orElse(StringUtils.EMPTY);

        }
    }

    public String getBody() {
        if(isVariant()) {
            return contentFragment
                .map(cv -> cv.getElement("body").getVariation(variation))
                .map(ContentVariation::getContent)
                .orElse(StringUtils.EMPTY);
        }
        else {
            return contentFragment
                .map(cf -> cf.getElement("body"))
                .map(ContentElement::getContent)
                .orElse(StringUtils.EMPTY);

        }
    }

    protected boolean isVariant() {
        return (variation != null && !variation.isEmpty() && !"master".equalsIgnoreCase(variation));
    }

}
