package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RelatedInformationModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(RelatedInformationModel.class);
	
	@Inject
	@Via("valueMap")
	private String heading;
	
	@Inject
	@Via("valueMap")
	private String description;
	
	@Inject
	@Via("valueMap")
	private String theme;
	
	
	@ChildResource
    private List<RelatedInformationDataModel> pagePathContainer;
	

}
