package com.juniper.aem.models;

import java.util.List;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ColumnsModel {
    
    @Inject
    private String rating;
    
    @Inject
    private String description;

    @Inject
    private List<CtaModel>  cta; 
    
    @Inject
    private String videoPath;
    
    @Inject
    private String videoAltText;
    
    @Inject
    private String playIconColor;
   

}