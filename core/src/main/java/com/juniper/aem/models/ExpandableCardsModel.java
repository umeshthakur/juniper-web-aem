package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExpandableCardsModel {

    @Inject
    private String eyebrow;

    @Inject
    private String title;

    @Inject
    private String imagePath;

    @Inject
    private String altText;

    @Inject
    private String name;

    @Inject
    private String longDescription;

    @Inject
    private List<SocialMediaLinksModel> socialmediaLinks;

}