package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.day.cq.wcm.api.Page;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = { SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SecondaryNavModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;

    @AemObject
    private Page currentPage;

    //private Level1PageModel level1PageModel;
    
    private SecondaryNavPageModel secondaryNavPageModel;

    @PostConstruct
    protected final void init() {
        if (currentPage.getProperties().containsKey("showSecondaryNav")
                && currentPage.getParent().getProperties().containsKey("showSecondaryNav")
                && currentPage.getParent().getParent().getProperties().containsKey("showSecondaryNav")) {
                addSecondarynavPages(currentPage.getParent().getParent());
        } else if (currentPage.getProperties().containsKey("showSecondaryNav")
                && currentPage.getParent().getProperties().containsKey("showSecondaryNav")) {
                addSecondarynavPages(currentPage.getParent());
        } else if (currentPage.getProperties().containsKey("showSecondaryNav")) {
                addSecondarynavPages(currentPage);
        }
        
    }

    private void addSecondarynavPages(Page page) {
        // Level 1
        secondaryNavPageModel = page.getContentResource().adaptTo(SecondaryNavPageModel.class);
        if(secondaryNavPageModel != null && page.getProperties().containsKey("showSecondaryNav")) {
            // Level 2
            Iterator<Page> level2Page = page.listChildren();
            List<SecondaryNavPageModel> levelTwoPageList = new ArrayList<SecondaryNavPageModel>();
            while (level2Page.hasNext()) {
                Page page2 = (Page) level2Page.next();
                if(page2.getProperties().containsKey("showSecondaryNav")) {
                    SecondaryNavPageModel level2PageModel = page2.getContentResource().adaptTo(SecondaryNavPageModel.class);
                    
                    // Level 3 start
                    Iterator<Page> level3Page = page2.listChildren();
                    List<SecondaryNavPageModel> levelThreePageList = new ArrayList<SecondaryNavPageModel>();
                    while (level3Page.hasNext()) {
                        Page page3 = (Page) level3Page.next();
                        if(page3.getProperties().containsKey("showSecondaryNav")) {
                            SecondaryNavPageModel level3PageModel = page3.getContentResource().adaptTo(SecondaryNavPageModel.class);
                            levelThreePageList.add(level3PageModel);
                        }
                    }
                    // Level 3 end
                    if (levelThreePageList.size() > 0) {
                        level2PageModel.setLevelThreePageList(levelThreePageList);
                    }
                    levelTwoPageList.add(level2PageModel);
                }
            }
            // level 2 end
            if(levelTwoPageList.size() > 0) {
                //level1PageModel.setLevelTwoPageList(levelTwoPageList);
                secondaryNavPageModel.setLevelTwoPageList(levelTwoPageList);
            }
        }
    }
}