package com.juniper.aem.models;

import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

@Getter
@Setter
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class, ValueMap.class},
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductSpecsModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductSpecsModel.class);

    @Inject @Named("productSpecs")
    private String productSpecs;

    @Inject @Named("productSpecsValue")
    private String productSpecsValue;

    @PostConstruct
    protected void init() {

    }
}
