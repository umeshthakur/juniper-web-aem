package com.juniper.aem.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LatestNewsModel {
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String eyebrowText;

    @Inject
    private String titleText;   

    @Inject
    private String themeColor;
    
    @ChildResource
    private LatestNewsItemModel featured;
    
    @ChildResource
    private List<LatestNewsItemModel> items;
    
    @SlingObject
    private ResourceResolver resourceResolver;

}
