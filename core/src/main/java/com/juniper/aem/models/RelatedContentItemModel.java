package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RelatedContentItemModel {

    @Inject
    private String cardEyebrow;

    @Inject
    private String cardTitle;

    @Inject
    private String cardDescription;

    @Inject
    private String cardCtaText;

    @Inject
    private String cardCtaLink;

    @Inject
    private String cardTarget;

    @Inject
    private String fragmentPath;

    @Inject
    private String assetEyebrow;

    @Inject
    private String assetTarget;

    @Inject
    private String assetCtaLink;

    @Inject
    private String[] assetTags;

    @SlingObject
    private ResourceResolver resourceResolver;


    @PostConstruct
    protected void init() {

        cardCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, cardCtaLink);
        assetCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, assetCtaLink);


    }

}