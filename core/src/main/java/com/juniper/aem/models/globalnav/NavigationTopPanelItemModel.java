package com.juniper.aem.models.globalnav;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.juniper.aem.utils.LinkUtil;

import java.util.List;

import javax.annotation.PostConstruct;

@Getter
@Slf4j
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavigationTopPanelItemModel {

    @ValueMapValue
    private String howToBuyCtaLabel;
    @ValueMapValue
    private String howToBuyCtaLink;
    @ValueMapValue
    private String ctaStyle;
    @ValueMapValue
    private String contactUsCtaLabel;
    @ValueMapValue
    private String contactUsCtaLink;
    @ValueMapValue
    private String loginCtaLabel;
    @ValueMapValue
    private String loginCtaLink;
    @ValueMapValue
    private String currentLocaleLabel;

    // Account menu
    @ValueMapValue
    private String accountSubtitle;
    @ValueMapValue
    private String logoutLabel;
    @ValueMapValue
    private String partnerCenterCtaLabel;
    @ValueMapValue
    private String partnerCenterCtaLink;
    @ValueMapValue
    private String accountInformationCtaLabel;
    @ValueMapValue
    private String accountInformationCtaLink;
    @ValueMapValue
    private String newProductCtaLabel;
    @ValueMapValue
    private String newProductCtaLink;
    @ValueMapValue
    private String createAccountCtaLabel;
    @ValueMapValue
    private String createAccountCtaLink;

    @ChildResource
    private List<Resource> countriesList;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @PostConstruct
    protected void init() {
        howToBuyCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, howToBuyCtaLink);
        contactUsCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, contactUsCtaLink);
        loginCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, loginCtaLink);
        partnerCenterCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, partnerCenterCtaLink);
        accountInformationCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, accountInformationCtaLink);
        newProductCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, newProductCtaLink);
        createAccountCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, createAccountCtaLink);
    }

}