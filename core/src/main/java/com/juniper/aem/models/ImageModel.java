package com.juniper.aem.models;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ImageModel {

    @Inject
    private String fileReference;

    @Inject
    private String imagePath;

    @Inject
    private Boolean originalSize;

    @Inject
    private String description;

    @ValueMapValue
    @Default(values = "left")
    private String descriptionAlignment;

    @Inject
    private String altText;

    @Inject
    private String imageLink;

    @Inject
    private String imageLinkTarget;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        imageLink = LinkUtil.addHTMLIfPage(resourceResolver, imageLink);
        if(this.imagePath !=null)
        {   
        this.imagePath=LinkUtil.addWebRendition(resourceResolver, this.imagePath);
        }
    }

}