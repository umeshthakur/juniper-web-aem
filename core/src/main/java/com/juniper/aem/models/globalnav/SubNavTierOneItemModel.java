package com.juniper.aem.models.globalnav;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubNavTierOneItemModel {

    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String itemLabel;

    @Inject
    private Boolean hideLabel;

    @Inject
    private String itemDescriptor;

    @Inject
    private String itemUrl;

    @ChildResource
    private List<SubNavTierTwoItemModel> column2;

    @ChildResource
    private SubNavRecommendedContentModel recommendedContent;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        itemUrl = LinkUtil.addHTMLIfPage(resourceResolver, itemUrl);
    }
}