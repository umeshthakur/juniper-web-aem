package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FlexibleCTAModel {
	
	private static Logger log = LoggerFactory.getLogger(FlexibleCTAModel.class);
	
	@ValueMapValue
    private String ctaLink;
	
	@ValueMapValue
    private String ctaColor;
	
	@ValueMapValue
    private String ctaLabel;
	
	private String finalCtaLink;
	
	private String isLinkOrButton = "";
	
	private String linkhtml = "";

	@Inject
	@Source("sling-object")
	private ResourceResolver resourceResolver;
	
	@PostConstruct
    protected void init() {	
		
		finalCtaLink = LinkUtil.addHTMLIfPage(resourceResolver, ctaLink);
		
		if(ctaColor == null) {
			ctaColor = "";
		}
		
		if(ctaColor.contains("link")) {
			isLinkOrButton = "link";
		} else {
			isLinkOrButton = "button";
		}
		
		
		switch (ctaColor) {
		  case "link":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-arrow default\">";
			  break;
		  case "link-download":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-arrow down\">";
			   break;
		  case "link-inpage":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-arrow inpage\">";
			  break;
		  case "link-ext":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-arrow ext\">";
			  break;
		  case "link-pdf":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-pdf\">";
			  break;
		  case "link-ppt":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-slides\">";
			  break;
		  case "link-audio":
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-audio\">";
			  break;
		  case "link-video":					
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-video\">";
			  break;	
		  case "link-offsite":					
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-offsite\">";
			  break;
		  case "link-down-up":					
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-link-up-download\">";
			  break;
		  default:
			  linkhtml = "<button class=\"juniper-btn juniper-btn--link\">" + ctaLabel + "</button>\n  <span class=\"juniper-icon icon-arrow default\">";
			 
		}		
		
    }

}
