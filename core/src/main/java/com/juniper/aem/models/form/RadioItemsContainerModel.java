package com.juniper.aem.models.form;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RadioItemsContainerModel {

    private static final Logger LOG = LoggerFactory.getLogger(FormSelectOptionsContainerModel.class);

    @Inject
    @Via("valueMap")
    private String id;

    @Inject
    @Via("valueMap")
    private String value;

}
