
package com.juniper.aem.models;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.wcm.api.Page;
import com.juniper.aem.models.utils.BreadcrumbHelper;
import com.juniper.aem.utils.Constants;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Model(adaptables = {SlingHttpServletRequest.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AnalyticsModel {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private SlingHttpServletRequest request;

    @SlingObject
    private Resource resource;

    @Inject
    private InheritanceValueMap pageProperties;

    private String category;

    private String subcategory;

    private String referrerUrl;

    private String requestUrl;

    @AemObject
    private Page currentPage;

    private List<Page> breadcrumbPagelist = new ArrayList<Page>();

    @PostConstruct
    protected final void init() {
        referrerUrl = request.getHeader("referer");
        requestUrl = String.valueOf(request.getRequestURL());
        if (pageProperties != null) {
            category = pageProperties.getInherited(Constants.CATEGORY, "");
            subcategory = pageProperties.getInherited(Constants.SUBCATEGORY, "");
        }
        else {
            log.warn("Could not load page properties");
        }

        BreadcrumbHelper helper = new BreadcrumbHelper(resource, currentPage);
        breadcrumbPagelist = helper.findBreadcrumbs();
    }

    public String getBreadcrumbJS() {
        final StringBuffer breadcrumbs = new StringBuffer();
        final Boolean[] isFirst = {Boolean.TRUE};

        breadcrumbs.append("[");
        breadcrumbPagelist.forEach((crumb) -> {
            if(isFirst[0] == Boolean.TRUE)
                breadcrumbs.append("\"");
            else
                breadcrumbs.append(", \"");
            breadcrumbs.append(crumb.getName());
            breadcrumbs.append("\"");
            isFirst[0] = Boolean.FALSE;
        });
        breadcrumbs.append("]");
        return breadcrumbs.toString();
    }

}
