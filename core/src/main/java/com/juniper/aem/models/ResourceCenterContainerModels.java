package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.wcm.api.Page;
import com.juniper.aem.utils.Utils;

import lombok.Setter;

@Setter

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResourceCenterContainerModels {

	private static Logger log = LoggerFactory.getLogger(ResourceCenterContainerModels.class);

	@Inject
	ResourceResolver resourceResolver;

	@Inject
	Resource resource;

	@Inject
	private String label;

	@Inject
	private String link;

	@Inject
	private String assets;

	@Inject
	private String target;


	public String getLabel() {
		return label;
	}
	public String getLink() {
		return link;
	}

	public String getAssets() {
		return assets;
	}

	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}

	@PostConstruct
	protected void init() {
		try {

			if(assets!=null && !assets.isEmpty())
			{
				String assetPath=this.getAssets();
				this.setLink(assetPath);

			}
			String linkURL=this.getLink();
			if(this.getLink() !=null)
			{
				Resource resource=resourceResolver.getResource(linkURL);
				if (this.getLabel()==null && resource !=null) {
					if(linkURL.startsWith("/content/dam"))
					{
						Asset asset = resource.adaptTo(Asset.class);
						if(asset !=null && asset.getMetadataValue("dc:title") !=null)
						{
							this.setLabel(asset.getMetadataValue("dc:title").toString());	
						}
						else
						{
							this.setLabel(asset.getName().substring(0,asset.getName().lastIndexOf(".")));
						}
					}
					if(linkURL.startsWith("/content/juniper"))
					{
						Page page = resource.adaptTo(Page.class);
						if(page !=null && !page.getTitle().isEmpty())
						{
							this.setLabel(page.getTitle().toString());	
						}
						else
						{
							this.setLabel(page.getName());
						}

					}
				}
			}
			if(!resource.getParent().getName().equalsIgnoreCase("datasheet") && linkURL !=null && linkURL.contains("."))
			{	
				String fileType=linkURL.substring(linkURL.lastIndexOf("."));
				if(this.getLabel() !=null && fileType.equalsIgnoreCase(".pdf"))
				{
					this.setLabel(this.getLabel().concat(" (PDF)"));
				}

			}
			this.setLink(Utils.formatLink(this.getLink()));
		}
		catch(Exception e)
		{
			log.debug(e.getMessage());
		}

	}
}
