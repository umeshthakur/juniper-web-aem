package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StackedStatementsModel {

    @ValueMapValue
    private String eyebrow;

    @ValueMapValue
    private String header;

    @ValueMapValue
    private String headerLink;

    @ValueMapValue
    @Default(values = "_self")
    private String headerLinkTarget;

    @ValueMapValue
    private String description;

    @ValueMapValue
    private String ctaLabel;

    @ValueMapValue
    private String ctaLink;

    @ValueMapValue
    @Default(values = "_self")
    private String ctaLinkTarget;

    @ChildResource
    private List<Resource> statements;

    @PostConstruct
    protected final void init() {
        if (statements == null) {
            statements = new ArrayList<>();
        }
    }
}
