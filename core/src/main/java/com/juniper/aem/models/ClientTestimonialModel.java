package com.juniper.aem.models;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Setter
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ClientTestimonialModel {
    protected final Logger Log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String backgroundColor;

    @Inject
    private String imageOrVideo;

    @Inject
    private String imagePath;

    @Inject
    private String imageAltText;
    
    @Inject
    private String videoPath;
    
    @Inject
    private String videoAltText;
    
    @Inject
    private String videoURL;
    
    @Inject
    private String link;
    
    @Inject
    private String linkTarget;
    
    @Inject
    private String testimony;
    
    @Inject
    private String clientName;
    
    @Inject
    private String clientRole;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @PostConstruct
    protected void init() {
        link = LinkUtil.addHTMLIfPage(resourceResolver, this.link);
    }
}