package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import com.juniper.aem.utils.LinkUtil;

import javax.annotation.PostConstruct;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StackedParagraphsPropertiesModel {

    @ValueMapValue
    private String eyebrow;

    @ValueMapValue
    private String headline;

    @ValueMapValue
    private String text;

    @ValueMapValue
    private String ctaLabel;

    @ValueMapValue
    private String ctaLink;
    
    @ValueMapValue
    private String ctaLinkTarget;
    
    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {

        if(this.ctaLink != null) {
            ctaLink = LinkUtil.addHTMLIfPage(resourceResolver,ctaLink);
        }
    }
}
