package com.juniper.aem.models;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Model(adaptables = {Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PagePropertiesModel {
    private static final Logger LOG = LoggerFactory.getLogger(PagePropertiesModel.class);

    private String pagePath;

    private ValueMap pageProperties;

   	@PostConstruct
    protected void init() {
    }
}
