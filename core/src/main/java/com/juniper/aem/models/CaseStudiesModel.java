package com.juniper.aem.models;

import com.juniper.aem.services.ResourceResolverService;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Optional;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class CaseStudiesModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    @Named("caseStudies")
    private List<Resource> caseStudies;

    private List<CaseStudyModel> caseStudiesList = new ArrayList<>();

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private ResourceResolverService resourceResolverService;

    private Session session;

    public List<CaseStudyModel> getCaseStudiesList() {
        return caseStudiesList;
    }

    public void setCaseStudiesList(List<CaseStudyModel> caseStudiesList) {
        this.caseStudiesList = caseStudiesList;
    }


    @PostConstruct
    protected void init() {
        log.debug("In init of CaseStudiesModel");
        if (!(caseStudies == null) && !caseStudies.isEmpty()) {
            for (Resource resource : caseStudies) {
                CaseStudyModel model = resource.adaptTo(CaseStudyModel.class);
                log.debug("Created CaseStudyModel for Company {}", model.getCompany());
                caseStudiesList.add(model);
            }
            log.debug("Found {} caseStudies", caseStudies.size());

        } else {
            log.debug("caseStudies is empty");
        }
    }
}
