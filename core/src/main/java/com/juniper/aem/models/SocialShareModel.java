package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = {
        SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, cache = true)
public class SocialShareModel {

    private String label;
    
    private String sharePagePath;
    
    private String sharePageURL;

    private List<SocialMediaLinksModel> socialMediaLinksModelList = new ArrayList<SocialMediaLinksModel>();

    @SlingObject
    private Resource resource;
    
    @SlingObject
    private PageManager pageManager;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @AemObject
    private Page currentPage;

    @PostConstruct
    protected final void init() {
        
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page sharePage = pageManager.getContainingPage(resource.getPath());
        sharePagePath = sharePage.getPath().toString().replace("/content/juniper", "");
        sharePageURL = sharePagePath + ".html";
        
        Page homePage = currentPage.getAbsoluteParent(3);

        label = homePage.getProperties().get("label", "");

        if (homePage.getContentResource().getChild("items") != null) {
            Iterator<Resource> children = homePage.getContentResource().getChild("items").listChildren();
            while (children.hasNext()) {
                SocialMediaLinksModel socialMediaLinksModel = (children.next()).adaptTo(SocialMediaLinksModel.class);
                socialMediaLinksModelList.add(socialMediaLinksModel);
            }
        }
    }
}