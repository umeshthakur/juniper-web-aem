package com.juniper.aem.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableRowModel {

    @Inject
    private String columnData;

    @Inject
    private String verticalAlign;

    @Inject
    private String labelBackgroundColor;

    @Inject
    private String textColor;

    @Inject
    private String textAlignment;

    @Inject
    private String mergeRows;

    @Inject
    private String mergeColumns;

    @Inject
    private String groupLabel;

    @Inject
    private String columnMerge;

    @Inject
    private String rowMerge;
    
    @Inject
    private String fileReference;
    
    @Inject
    private String altText;

    @Inject
    private String columnDataType;
    
}