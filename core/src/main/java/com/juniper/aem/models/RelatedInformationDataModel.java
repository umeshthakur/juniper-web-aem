package com.juniper.aem.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RelatedInformationDataModel {

    private static final Logger LOG = LoggerFactory.getLogger(RelatedInformationDataModel.class);
    @Inject
    ResourceResolver resourceResolver;

    @Inject
    @Via("valueMap")
    private String pagePath;

    @Inject
    @Via("valueMap")
    private String showImage;
    
    @Inject
    @Via("valueMap")
    private String imageType;


    @Self
    Resource resource;

    @Inject
    private ProductService productService;

    List<ProductModel> productModelList = new ArrayList<>();

    List<PagePropertiesModel> pagePropertiesModelList = new ArrayList<>();
    
    @PostConstruct
    protected void init() {

        PageManager pm = resource.getResourceResolver().adaptTo(PageManager.class);
        //NPE Check
        if (pm.getContainingPage(pagePath) != null) {
            Page containingPage = pm.getContainingPage(pagePath);
            ValueMap pageProperties = containingPage.getProperties();
            PagePropertiesModel propertiesModel = new PagePropertiesModel();
            propertiesModel.setPageProperties(containingPage.getProperties());
            pagePropertiesModelList.add(propertiesModel);


            if (pageProperties != null && pageProperties.get("productDataPath") != null) {

                String productDataPath = pageProperties.get("productDataPath").toString();
                if (productDataPath != null) {

                    ProductModel productModel = productService.getProduct(productDataPath);

                    if (productModel != null) {
                        productModelList.add(productModel);
                    }
                }
            }

        }

    }


}
