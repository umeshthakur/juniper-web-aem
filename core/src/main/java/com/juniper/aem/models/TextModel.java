package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @Inject
    private String layoutType;
    
    @Inject
    private String splitTextType;
    
    @ChildResource
    private TextFamilyModel header;
    
    @ChildResource
    private TextFamilyModel twdaif;
    
    @ChildResource
    private TextFamilyModel twsi;

    @ChildResource
    private TextFamilyModel twdasi;

    @ChildResource
    private TextFamilyModel bt;
    
    @ChildResource
    private TextFamilyModel twili;
    
    @ChildResource
    private TextFamilyModel twil;

    @ChildResource
    private TextFamilyModel twmii;
    
    @ChildResource
    private TextFamilyModel twib;    
    
    @ChildResource
    private TextFamilyModel twibl;
    
    @ChildResource
    private TextFamilyModel text;  

    @ChildResource
    private TextFamilyModel stwi; 
    
    @ChildResource
    private TextFamilyModel stwoi; 

    @ChildResource
    private TextFamilyModel stawi; 
    
    @ChildResource
    private TextFamilyModel et; 
    
    @ChildResource
    private TextFamilyModel stfim1; 
    
    @ChildResource
    private TextFamilyModel stfim2; 
    
    @ChildResource
    private TextFamilyModel etwc;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @PostConstruct
    protected final void init() {
    }
}
