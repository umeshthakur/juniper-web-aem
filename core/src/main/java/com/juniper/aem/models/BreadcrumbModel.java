package com.juniper.aem.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class BreadcrumbModel {
    protected final Logger Log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String startLevel;

    @SlingObject
    private Resource resource;

    @SlingObject
    private PageManager pageManager;

    @SlingObject
    private ResourceResolver resourceResolver;

    private List<Page> pageList = new ArrayList<>();
    
    @PostConstruct
    protected void init() {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page page = pageManager.getContainingPage(resource.getPath());

        int pageDepth = page.getDepth();
        if (StringUtils.isBlank(startLevel) || !StringUtils.isNumeric(startLevel)) {
            startLevel = "4";
        }
        int startLevelInt = Integer.parseInt(startLevel);
        if (pageDepth > 2) {
            pageDepth -= 2;
        }
        for (int i = startLevelInt; i <= pageDepth; i++) {
            pageList.add(page.getAbsoluteParent(i));
        }
    }
}
