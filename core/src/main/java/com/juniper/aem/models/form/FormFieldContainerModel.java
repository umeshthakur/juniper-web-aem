package com.juniper.aem.models.form;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormFieldContainerModel {
	
	private static final Logger LOG = LoggerFactory.getLogger(FormFieldContainerModel.class);
	
	@Inject
	@Via("valueMap")
	private String theme;
	
	@Inject
	@Via("valueMap")
	private String eloquaURL;
	
	@Inject
	@Via("valueMap")
	private String formName;
	
	
	@ChildResource
    private List<FormFieldElementAttributeModel> formFieldElementAttributeModel;
	

}
