package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ExpandableDirectoryModel {
    
    @Inject
    private String heading;
    
    @Inject
    private String backgroundColor;
    
    @Inject
    private List<ExpandableCardsModel> cards;
}