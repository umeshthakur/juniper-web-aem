package com.juniper.aem.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableContainerModel {

    @Inject
    private String footnote;

    @Inject
    private String header;

    @Inject
    private String description;

    @Inject
    private String themeColor;

    @Inject
    private String alternateColumnColor;

    @Inject
    private String oddEven;

    @Inject
    private String backgroundColor;

    @ChildResource
    private List<TableModel> table;

}
