package com.juniper.aem.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.jcr.Session;
import java.util.*;
import java.util.Locale;

@Getter
@Setter
@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FilterModel {
    private static final Logger LOG = LoggerFactory.getLogger(FilterModel.class);

    @Inject
    @Named("cq:tags")
    private String[] tags;

    @Inject
    @Named("filterTitle")
    private String filterTitle;

    @Inject
    @Named("clearAll")
    private String clearAll;

    @Inject
    @Named("theme")
    private String theme;

    @SlingObject
    private Resource resource;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private ProductService productService;

    @Inject
    private Session session;

    @Inject
    private QueryBuilder builder;

    private Map<String, TreeMap<String, FilterOptionsModel>> filterMap = new TreeMap<String, TreeMap<String, FilterOptionsModel>>();

    @PostConstruct
    protected void init() {
        final TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page current = pageManager.getContainingPage(resource.getPath());
        // LOG.info("My Page ::" + sharePage.getAbsoluteParent(3));
        Page homepage = current.getAbsoluteParent(3);
        Locale lc = null;
        
        if(homepage.getLanguage().getLanguage() != null &&
                homepage.getLanguage().getCountry() != null &&
                !homepage.getLanguage().getLanguage().equalsIgnoreCase("") &&
                !homepage.getLanguage().getCountry().equalsIgnoreCase("")) {
            
            lc = new Locale(homepage.getLanguage().getLanguage(), homepage.getLanguage().getCountry());            
        } 
        
        if (pageManager != null) {
            String languageRoot = pageManager.getContainingPage(resource).getAbsoluteParent(3).getPath();
            if (tags != null) {
                for (String tagTypes : tags) {
                    Tag tag = tagManager.resolve(tagTypes);
                    // If tag does not exist, then continue with the next one in the list.
                    if (tag == null) {
                        continue;
                    }
                    Iterator<Tag> it = tag.listChildren();
                    String[] resultsTags = null;
                    Resource resultsComponent = resource.getParent();

                    if (resultsComponent != null) {

                        if(resultsComponent.getResourceType().contains("resultscards")) {

                            resultsTags = resultsComponent.getValueMap().get("productTags", String[].class);

                            TreeMap<String, FilterOptionsModel> productMap = new TreeMap<>();

                            Map<String, String> map = new HashMap<>();
                            map.put("path", languageRoot);
                            map.put("type", "cq:Page");
                            map.put("1_property", "jcr:content/cq:tags");
                            
                            String tagTitle = tag.getTitle();
                            if(lc != null) {
                                tagTitle = tag.getTitle(lc);
                            }
                            
                            while (it.hasNext()) {
                                Tag productTag = it.next();
                                map.put("1_property.value", productTag.getTagID());
                                map.put("orderby.sort", "asc");
                                map.put("p.limit", "-1");

                                if(resultsTags != null) {
                                    map.put("2_property", "jcr:content/cq:tags");
                                    for (int i = 0; i < resultsTags.length; i++) {
                                        map.put("2_property." + i + "_value", resultsTags[i]);
                                    }
                                }
                                map.put("3_property", "jcr:content/productDataPath");
                                map.put("3_property.operation", "exists");

                                Query query = builder.createQuery(PredicateGroup.create(map), session);

                                SearchResult result = query.getResult();
                                long count = 0;
                                if (result != null) {
                                    count = result.getTotalMatches();
                                }

                                FilterOptionsModel filterOptionsModel = new FilterOptionsModel();
                                filterOptionsModel.setCount(count);
                                
                                String prodTagTitle = productTag.getTitle();
                                if(lc != null) {
                                    prodTagTitle = productTag.getTitle(lc);
                                }
                                filterOptionsModel.setTagTitle(prodTagTitle);
                                String tagId = productTag.getName();
                                String parentTagId = productTag.getParent().getName();
                                filterOptionsModel.setTagId(parentTagId.concat("/").concat(tagId));

                                productMap.put(prodTagTitle, filterOptionsModel);
                            }
                            filterMap.put(tagTitle, productMap);
                        }
                        else {
                            resultsTags = resultsComponent.getValueMap().get("caseStudyTags", String[].class);
                            TreeMap<String, FilterOptionsModel> caseStudyMap = new TreeMap<>();
                            Map<String, String> map = new HashMap<>();
                            map.put("path", languageRoot);
                            map.put("type", "cq:Page");
                            map.put("1_property", "jcr:content/cq:tags");
                            
                            String tagTitle = tag.getTitle();
                            if(lc != null) {
                                tagTitle = tag.getTitle(lc);
                            }
                            
                            while (it.hasNext()) {
                                Tag caseStudyTag = it.next();
                                map.put("1_property.value", caseStudyTag.getTagID());
                                map.put("orderby.sort", "asc");
                                map.put("p.limit", "-1");

                                if(resultsTags != null) {
                                    map.put("2_property", "jcr:content/cq:tags");
                                    for (int i = 0; i < resultsTags.length; i++) {
                                        map.put("2_property." + i + "_value", resultsTags[i]);
                                    }
                                }
                                map.put("3_property","jcr:content/cq:template" );
                                map.put("3_property.value", "/conf/juniper/settings/wcm/templates/case-study");

                                Query query = builder.createQuery(PredicateGroup.create(map), session);

                                SearchResult result = query.getResult();
                                
                                long count = 0;
                                if (result != null) {
                                    count = result.getTotalMatches();
                                }
                                
                                String caseTagTitle = caseStudyTag.getTitle();
                                if(lc != null) {
                                    caseTagTitle = caseStudyTag.getTitle(lc);
                                }
                                
                                FilterOptionsModel filterOptionsModel = new FilterOptionsModel();
                                filterOptionsModel.setCount(count);
                                filterOptionsModel.setTagTitle(caseTagTitle);
                                String tagId = caseStudyTag.getName();
                                String parentTagId = caseStudyTag.getParent().getName();
                                filterOptionsModel.setTagId(parentTagId.concat("/").concat(tagId));

                                caseStudyMap.put(caseTagTitle, filterOptionsModel);
                            }
                            filterMap.put(tagTitle, caseStudyMap);
                        }
                    }  
                }
            }
        }
    }
}