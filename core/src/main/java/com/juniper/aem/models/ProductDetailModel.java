package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {Resource.class, SlingHttpServletRequest.class},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductDetailModel {
    protected final Logger Logger = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;

    @SlingObject
    private PageManager pageManager;

    @Inject @Via("resource")
    String pagePath;

    @Inject
    private ProductService productService;
    
    private ProductModel productModel;
    
    @PostConstruct
    protected void init() {
        try {
			PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
			Page productPage = pageManager.getContainingPage(resource.getPath());
	    	
	    	if(productPage != null && productPage.getProperties().get("productDataPath") != null) {
		    	String productPath = productPage.getProperties().get("productDataPath").toString();
		    	productModel = productService.getProduct(productPath);
	    	}
        } catch (Exception e) {
        	Logger.error("Can't extract product information from the resource", e);
        }
    }
}