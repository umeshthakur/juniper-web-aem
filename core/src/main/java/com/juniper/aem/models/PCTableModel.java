package com.juniper.aem.models;

import java.util.List;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PCTableModel {
    @Inject
    private String sectionHeader;

    @Inject
    private List<RowsModel>  rows;    

}
