package com.juniper.aem.models;


import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductsDataContainerModel {
    private static Logger log = LoggerFactory.getLogger(ProductsDataContainerModel.class);
    @Inject
    private String productDataPath;
    @Inject
    private String ctaText;
    @Inject
    private String ctaLink;
    @Inject
    private String target;

    @PostConstruct
    protected void init() {
    }
}
