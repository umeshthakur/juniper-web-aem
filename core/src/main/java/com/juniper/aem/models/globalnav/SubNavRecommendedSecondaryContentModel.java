package com.juniper.aem.models.globalnav;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubNavRecommendedSecondaryContentModel {

    @Inject
    private String title;

    @Inject
    private String description;

    @Inject
    private String link;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        link = LinkUtil.addHTMLIfPage(resourceResolver, link);
    }

}
