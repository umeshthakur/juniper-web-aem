package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class, ValueMap.class},
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductBenefitsModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductBenefitsModel.class);
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @Inject @Via("valueMap")
    private String title;

    @Inject @Named("shortDescription")
    private String shortDescription;
    
    @Inject @Named("longDescription")
    private String longDescription;
    
    @Inject @Named("imagePath")
    private String imagePath;

    @Inject @Named("imageLink")
    private String imageLink;
    
    @Inject @Named("altText")
    private String altText;

    @PostConstruct
    protected void init() {
        if(this.imagePath !=null)
        {
            this.imagePath=LinkUtil.addWebRendition(resourceResolver, this.imagePath);
        }
    }
}