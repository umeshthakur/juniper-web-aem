package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DataCenterDesignModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String imagePath;

    @Inject
    private String altText;

    @PostConstruct 
    protected void init() {
    }

}