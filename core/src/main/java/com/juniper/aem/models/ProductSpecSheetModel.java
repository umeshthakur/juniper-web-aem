package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class ProductSpecSheetModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String productDataPath;
    
    @Inject
    private String backButtonLink;
    
    @Inject
    private String backButtonLabel;
    
    @Inject
    private String fileReference;
    
    @Inject
    private String specLabel;
    
    @Inject
    private String heading;
    
    @Inject
    private String imageAltText;

    @Inject
    private ProductService productService;

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;
    
    ProductModel productModel;

    @PostConstruct
    protected void init() {
        try {
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (this.productDataPath != null) {
                productModel = productService.getProduct(this.productDataPath);
                }
            else
            {
                if (pageManager.getContainingPage(resource).getParent().getProperties().get("productDataPath") != null) {
                    productModel = productService.getProduct(pageManager.getContainingPage(resource).getParent().getProperties().get("productDataPath").toString());
                    }
                
            }
            
            backButtonLink = LinkUtil.addHTMLIfPage(resourceResolver, backButtonLink);
            } catch (Exception e) {
            log.error("Can't extract product information from the resource", e);

        }
    }
}