package com.juniper.aem.models.globalnav;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavSubTypeModel {

	@ValueMapValue
    private String navLabel;
    
    @ValueMapValue
    private String subNavType;

    @ChildResource
    private NavVersionModel v1;
    
    @ChildResource
    private NavVersionModel v2;
    
}