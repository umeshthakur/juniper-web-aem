package com.juniper.aem.models.globalnav;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.models.ProductModel;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.Constants;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavVersionModel {

    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;

    @ValueMapValue
    private String product1PagePath;

    @ValueMapValue
    private String product2PagePath;

    @ChildResource
    private List<SubNavItemModel> v1subnav;

    @ChildResource
    private V2SubNavItemModel v2subnav;

    @Inject
    private ProductService productService;

    private List<ProductModel> recommendedPageList = new ArrayList<ProductModel>();

    @PostConstruct
    protected void init() {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        if (product1PagePath != null) {
            Page page = pageManager.getContainingPage(product1PagePath);
            ValueMap valueMap = page.getProperties();
            if (Constants.SOLUTION_TEMPLATE.equals(valueMap.get(Constants.CQ_TEMPLATE, String.class))) {
                ProductModel pm = new ProductModel();
                pm.setTitle(valueMap.get("cardTitle", String.class));
                pm.setImagePath(valueMap.get("cardImagePath", String.class));
                pm.setShortDescription(valueMap.get("cardDescription", String.class));
                pm.setPagePath(product1PagePath);
                recommendedPageList.add(pm);
            } else if (Constants.PRODUCT_TEMPLATE.equals(valueMap.get(Constants.CQ_TEMPLATE, String.class))) {
                ProductModel pm = getProductModel(product1PagePath);
                if (pm != null) {
                    recommendedPageList.add(pm);
                }
            }
        }

        if (product2PagePath != null) {
            Page page = pageManager.getContainingPage(product2PagePath);
            ValueMap valueMap = page.getProperties();
            if (Constants.SOLUTION_TEMPLATE.equals(valueMap.get(Constants.CQ_TEMPLATE, String.class))) {
                ProductModel pm = new ProductModel();
                pm.setTitle(valueMap.get("cardTitle", String.class));
                pm.setImagePath(valueMap.get("cardImagePath", String.class));
                pm.setShortDescription(valueMap.get("cardDescription", String.class));
                pm.setPagePath(product1PagePath);
                recommendedPageList.add(pm);
            } else if (Constants.PRODUCT_TEMPLATE.equals(valueMap.get(Constants.CQ_TEMPLATE, String.class))) {
                ProductModel pm = getProductModel(product2PagePath);
                if (pm != null) {
                    recommendedPageList.add(pm);
                }
            }

        }
    }

    private ProductModel getProductModel(String productPagePath) {
        Resource resource = resourceResolver.getResource(productPagePath);
        if (resource != null
                && resource.getChild(JcrConstants.JCR_CONTENT).getValueMap().containsKey(Constants.PRODUCT_DATA_PATH)) {
            ProductModel productModel = productService.getProduct(resource.getChild(JcrConstants.JCR_CONTENT)
                    .getValueMap().get(Constants.PRODUCT_DATA_PATH).toString());
            if (productModel != null) {
                productModel
                        .setShortDescription(productModel.getShortDescription().replace("<p>", "").replace("</p>", ""));
                productModel.setPagePath(productPagePath);
                return productModel;
            }
        }
        return null;
    }
}