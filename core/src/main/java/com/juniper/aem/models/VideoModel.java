package com.juniper.aem.models;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.time.Duration;
import java.util.List;

@Getter
@Setter
@Model(adaptables = {Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, cache = true)
public class VideoModel {
    @Inject
    private String name;

    @Inject
    private String description;

    @Inject
    private List<ThumbnailModel> urls;

    @Inject
    private String uploadedDate;

    @Inject
    private String duration;

    @Inject
    private String embedUrl;


    @PostConstruct
    protected void init() {
        if (StringUtils.isNotBlank(duration)) {
            String[] duration_format = duration.split(":");

            int seconds = 0;
            int elements = duration_format.length;

            switch (elements) {
                case 1:
                    seconds = Integer.parseInt(duration_format[0]);
                    break;
                case 2:
                    seconds = Integer.parseInt(duration_format[1]) + (60 * Integer.parseInt(duration_format[0]));
                    break;
                case 3:
                    seconds = Integer.parseInt(duration_format[2]) + (60 * Integer.parseInt(duration_format[1]))
                            + (3600 * Integer.parseInt(duration_format[0]));
                    break;
            }
            this.setDuration(Duration.ofSeconds(seconds).toString());
        }
    }
}