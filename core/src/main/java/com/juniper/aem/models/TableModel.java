package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TableModel {

    @Inject
    private String variantType;
    
    @Inject @Named("variant1")
    private Resource variant1RowProps;
    
    @Inject @Named("variant2")
    private Resource variant2RowProps;
    
    @Inject @Named("variant3")
    private Resource variant3RowProps;
    
    @Inject @Named("variant4")
    private Resource variant4RowProps;
    
    @Inject @Named("variant5")
    private Resource variant5RowProps;
    
    @Inject @Named("variant6")
    private Resource variant6RowProps;
    
    @Inject @Named("variant7")
    private Resource variant7RowProps;

    @ChildResource
    private List<TableRowModel> variant1;

    @ChildResource
    private List<TableRowModel> variant2;

    @ChildResource
    private List<TableRowModel> variant3;

    @ChildResource
    private List<TableRowModel> variant4;

    @ChildResource
    private List<TableRowModel> variant5;

    @ChildResource
    private List<TableRowModel> variant6;
    
    @ChildResource
    private List<TableRowModel> variant7;

    @ChildResource
    private TableRowContainerModel columns;

    private boolean isSingleColumn = false;

    @PostConstruct
    protected final void init() {
        if (variant2 != null && variant2.size() == 1) {
            isSingleColumn = true;
        }
    }
}