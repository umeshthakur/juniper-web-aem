package com.juniper.aem.models;

import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import lombok.Getter;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductComparisonMatrixModel {
    
    @Inject
    private List<ImageModel>  tableHeaders;

    @Inject
    private List<PCTableModel>  tableContent;
}