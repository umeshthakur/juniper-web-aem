package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class, ValueMap.class},
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductViewDetailsModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductViewDetailsModel.class);

    @Inject @Via("valueMap")
    private String label;

    @Inject @Named("pagePath")
    private String pagePath;

    @PostConstruct
    protected void init() {
    }
}
