package com.juniper.aem.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DemandOverlayItemModel {
    @Inject
    private String image;

    @Inject
    private String icon;

    @Inject
    private String title;

    @Inject
    private String text;

    @Inject
    private String assetPath;
}