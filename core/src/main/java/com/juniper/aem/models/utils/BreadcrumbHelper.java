package com.juniper.aem.models.utils;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;
import static org.apache.sling.query.SlingQuery.$;
import org.apache.sling.query.SlingQuery;

import java.util.ArrayList;
import java.util.List;

public class BreadcrumbHelper {
    private Resource resource;
    private Page currentPage;

    public BreadcrumbHelper(Resource resource, Page currentPage) {
        this.resource = resource;
        this.currentPage = currentPage;
    }

    public List<Page> findBreadcrumbs() {
        List<Page> breadcrumbPagelist = new ArrayList<Page>();

        SlingQuery slingQuery = $(resource).find("juniper/components/breadcrumb");
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0 && (list.get(0).getValueMap()).get("startLevel") != null) {
                int startLevel = Integer.parseInt((list.get(0).getValueMap()).get("startLevel").toString());
                int currentLevel = currentPage.getDepth();
                while (startLevel < currentLevel) {
                    Page page = currentPage.getAbsoluteParent(startLevel);
                    if (page != null && page.getContentResource() != null) {
                        breadcrumbPagelist.add(page);
                    }
                    startLevel++;
                }
            }
        }
        return breadcrumbPagelist;
    }

}
