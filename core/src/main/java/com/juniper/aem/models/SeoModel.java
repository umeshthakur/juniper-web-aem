package com.juniper.aem.models;

import static org.apache.sling.query.SlingQuery.$;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.query.SlingQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.day.cq.commons.Externalizer;
import com.day.cq.commons.inherit.InheritanceValueMap;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.models.utils.BreadcrumbHelper;
import com.juniper.aem.pojos.SeoImage;
import com.juniper.aem.services.CaseStudyService;
import com.juniper.aem.services.ImageNamesService;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.Constants;
import com.juniper.aem.utils.Utils;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {
        SlingHttpServletRequest.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL, cache = true)
public class SeoModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;

    @Inject
    private QueryBuilder builder;
    
    @OSGiService
    private Externalizer externalizer;

    @Inject
    @Via("resource")
    private String canonicalUrl;

    private String pageTitle;

    private String description;

    private String keywords;

    private String ogLocale;

    private String ogTitle;

    private String ogDescription;

    private String ogSiteUrl;

    private String ogImage;

    private String twitterCard;

    private String twitterTitle;

    private String twitterDescription;

    private String twitterImage;

    private String twitterImageAltText;

    private String additionalMetadata;

    private String country;

    private String searchUrl;

    private Session session;

    @Inject
    private ProductService productService;

    @Inject
    private ImageNamesService imageNamesService;

    @Inject
    private CaseStudyService caseStudyService;

    @AemObject
    private Page currentPage;

    private HashMap<String, String> languageMap = new HashMap<>();

    @Inject
    private InheritanceValueMap pageProperties;

    private List<Page> breadcrumbPagelist = new ArrayList<Page>();

    private List<PageModel> breadcrumbSchemalist = new ArrayList<PageModel>();

    private List<String> imagesList = new ArrayList<String>();

    private List<VideoModel> videoModelList = new ArrayList<VideoModel>();

    private List<FaqModel> faqModelList = new ArrayList<FaqModel>();

    private List<SeoImage> seoImageList = new ArrayList<SeoImage>();

    private String homepagePath;
    
    private String unformattedUrl;
    
    private String pagePath;
    
    private String thumbnailUrlPath;
    
    private String publishUrl="";
    
    

    @PostConstruct
    protected final void init() {
        
        publishUrl=externalizer.externalLink(resourceResolver, "publish", "");
        publishUrl=publishUrl.substring(0, publishUrl.lastIndexOf("/"));
        
        Page homepage = currentPage.getAbsoluteParent(3);

        if (pageProperties != null && homepage != null) {
            homepagePath = homepage.getPath();

            pageTitle = pageProperties.getInherited(Constants.PROP_PAGE_TITLE, "");

            if (!Constants.HOME_PAGE.equals(currentPage.getTemplate().getName())) {
                String additonalTitle = pageProperties.getInherited(Constants.PROP_ADDITIONAL_TITLE, "");
                if (StringUtils.isNotEmpty(additonalTitle)) {
                    pageTitle = pageTitle.concat(additonalTitle);
                }
            }

            // Canonical Url
            if (pageProperties.containsKey(Constants.PROP_CANONICAL_URL)) {
                unformattedUrl = pageProperties.getInherited(Constants.PROP_CANONICAL_URL, "");
            } else {
                unformattedUrl = currentPage.getPath();
            }
            canonicalUrl = externalizer.publishLink(resourceResolver, resourceResolver.map(unformattedUrl)).concat(Constants.URL_EXTENSION);

            additionalMetadata = pageProperties.getInherited(Constants.PROP_ADDITIONAL_META_DATA, "");

            description = pageProperties.getInherited(JcrConstants.JCR_DESCRIPTION, "");

            ogLocale = homepage.getLanguage().getLanguage();

            country = homepage.getLanguage().getCountry();

            ogTitle = pageProperties.get(Constants.PROP_OG_TITLE, pageTitle);

            ogDescription = pageProperties.get(Constants.PROP_OG_DESCRIPTION, description);

            ogImage = pageProperties.get(Constants.PROP_OG_IMAGE, "");

            twitterCard = pageProperties.get(Constants.PROP_TWITTER_CARD, Constants.SUMMARY);

            twitterTitle = pageProperties.get(Constants.PROP_TWITTER_TITLE, pageTitle);

            twitterDescription = pageProperties.get(Constants.PROP_TWITTER_DESCRIPTION, description);

            twitterImage = pageProperties.get(Constants.PROP_TWITTER_IMAGE, "");

            twitterImageAltText = pageProperties.get(Constants.PROP_TWITTER_IMAGE_ALT_TEXT, "");

            keywords = pageProperties.getInherited(Constants.PROP_KEYWORDS, "");

            Page siteRootPage = currentPage.getAbsoluteParent(1);

            if (siteRootPage != null) {

                Iterator<Page> countryPages = siteRootPage.listChildren();
                String currentLangPagePath = homepage.getPath();
                while (countryPages.hasNext()) {
                    Page countryPage = countryPages.next();
                    if (!countryPage.getPath().contains(Constants.DEFAULT_LANG_MASTER_PAGE_PATH) && !countryPage.getPath().contains(Constants.UTILS_PAGE_PATH)) {
                        Iterator<Page> langPages = countryPage.listChildren();
                        while (langPages.hasNext()) {
                            Page langPage = langPages.next();
                            String language = langPage.getLanguage().getLanguage();
                            String country = langPage.getLanguage().getCountry();
                            String languageLocaleCode = language.toLowerCase();
                            if (StringUtils.isNotBlank(country)) {
                                languageLocaleCode = languageLocaleCode.concat("-").concat(country.toLowerCase());
                            }
                            String currentLocalizedPagePath = currentPage.getPath().replace(currentLangPagePath, langPage.getPath());
                            Resource localizedResource = resourceResolver.getResource(currentLocalizedPagePath);

                            if (localizedResource != null) {
                                if (currentLocalizedPagePath.contains(Constants.DEFAULT_LANG_PAGE_PATH)) {
                                    languageMap.put("x-default", externalizer.publishLink(resourceResolver,resourceResolver.map(currentLocalizedPagePath)) + Constants.URL_EXTENSION);
                                }
                                languageMap.put(languageLocaleCode, externalizer.publishLink(resourceResolver,resourceResolver.map(currentLocalizedPagePath)) + Constants.URL_EXTENSION);
                            }
                        }
                    }
                }
            }

            if (Constants.PRODUCT_TEMPLATE.equals(pageProperties.get(Constants.CQ_TEMPLATE))) {
                if (StringUtils.isNotEmpty(pageProperties.get(Constants.PRODUCT_DATA_PATH, ""))) {
                    ProductModel productModel = productService
                            .getProduct(pageProperties.get(Constants.PRODUCT_DATA_PATH, String.class));
                    if (productModel != null) {
                        if (StringUtils.isEmpty(ogImage)) {
                            ogImage = productModel.getImagePath() != null ? productModel.getImagePath() : "";
                        }

                        if (StringUtils.isEmpty(twitterImage)) {
                            twitterImage = productModel.getImagePath() != null ? productModel.getImagePath() : "";
                            twitterImageAltText = productModel.getAltText() != null ? productModel.getAltText() : "";
                        }

                        List<ProductAssetsModel> productAssetModelList = productModel.getAssets();
                        for (ProductAssetsModel productAssetsModel : productAssetModelList) {
                            imagesList.add(productAssetsModel.getFileReference());
                        }
                    }
                }
            }else if (Constants.PRODUCT_FAMILY_LEVEL1_TEMPLATE.equals(pageProperties.get(Constants.CQ_TEMPLATE)) 
            		|| Constants.PRODUCT_FAMILY_LEVEL2_TEMPLATE.equals(pageProperties.get(Constants.CQ_TEMPLATE))) {
            	if (resource != null) {
            		Resource rootResource = resource.getChild("root");
            		if (rootResource != null && rootResource.hasChildren()) {
            			Iterator<Resource> childResources = rootResource.listChildren();
            			while (childResources.hasNext()) {
            				Resource childResource = childResources.next();
            				if(childResource.getResourceType().equalsIgnoreCase("juniper/components/productfamilyoverview")){
            					if (childResource.getValueMap().containsKey("fileReference")) {
            					ValueMap valueMap = childResource.getValueMap();
	            					if (valueMap != null && valueMap.containsKey("fileReference")) {
	            						if (StringUtils.isEmpty(twitterImage)) {
	            							twitterImage = valueMap.get("fileReference").toString();
	            							twitterImageAltText = valueMap.containsKey("imageAltText") ? valueMap.get("imageAltText").toString() : "" ;
	            						}
	            						if (StringUtils.isEmpty(ogImage)) {
	            							ogImage = valueMap.get("fileReference").toString();
	            						}
	            					 }
	            					break;
	            				}
            				}
            			}	
            		}
            	}
            }else if (Constants.CONTENT_PAGE_TEMPLATE.equals(pageProperties.get(Constants.CQ_TEMPLATE))) {
            	if (homepage.getProperties().get("headerNavFragment") != null) {
                    String headerNavFragment = homepage.getProperties().get("headerNavFragment").toString();
                    Resource res = resourceResolver.getResource(headerNavFragment + "/jcr:content/root/navigation");
                    if (res != null) {
                        ValueMap valueMap = res.getValueMap();
                        if (res.getValueMap().containsKey("logoImagePath")) {
                        	if (StringUtils.isEmpty(twitterImage)) {
    							twitterImage = valueMap.get("logoImagePath").toString();
    							twitterImageAltText = valueMap.containsKey("logoImageAltText") ? valueMap.get("logoImageAltText").toString() : "";
    						}
    						if (StringUtils.isEmpty(ogImage)) {
    							ogImage = valueMap.get("logoImagePath").toString();
    						}                        	
                        }
                    }
                }            	
            }else if (Constants.SOLUTION_TEMPLATE.equals(pageProperties.get(Constants.CQ_TEMPLATE))) {
                ogImage = pageProperties.get("cardImagePath", "");
                twitterImage = pageProperties.get("cardImagePath", "");
                twitterImageAltText = pageProperties.get("altText", "");
            } else if (Constants.CASE_STUDY_PAGE
                    .equals(pageProperties.get(JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY, ""))) {
                if (resource != null) {
                    Resource rootResource = resource.getChild("root");
                    if (rootResource != null && rootResource.hasChildren()) {
                        Iterator<Resource> childResources = rootResource.listChildren();
                        while (childResources.hasNext()) {
                            Resource childResource = childResources.next();
                            if (childResource.getResourceType()
                                    .equalsIgnoreCase("juniper/components/casestudyoverview")) {
                                ValueMap valueMap = childResource.getValueMap();
                                if (valueMap != null && valueMap.containsKey("fragmentPath")) {
                                    Resource fragmentResource = resourceResolver
                                            .getResource(valueMap.get("fragmentPath").toString());
                                    if (fragmentResource != null) {
                                        Optional<ContentFragment> contentFragment = Optional
                                                .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                        if (contentFragment.isPresent()) {
                                            if (StringUtils.isEmpty(ogImage)) {
                                                ogImage = getAsString(contentFragment, "logo");
                                            }
                                            if (StringUtils.isEmpty(twitterImage)) {
                                                twitterImage = getAsString(contentFragment, "logo");
                                                twitterImageAltText = getAsString(contentFragment, "logo_alt_text");
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }

            BreadcrumbHelper helper = new BreadcrumbHelper(resource, currentPage);
            breadcrumbPagelist = helper.findBreadcrumbs();
            SlingQuery slingQuery;
            // Image schema

            // Get images from cardslevel1
            slingQuery = $(resource).find("juniper/components/cardslevel1");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resources : list) {
                        if(resources.getChild("pagePathContainer") != null) {
                            Iterator<Resource> iterator = resources.getChild("pagePathContainer").listChildren();
                            while (iterator.hasNext()) {
                                Resource cardLevel1Resource = iterator.next();
                                if (cardLevel1Resource != null && cardLevel1Resource.getValueMap().containsKey("pagePath")) {
                                    cardLevel1Resource = resourceResolver
                                            .getResource(cardLevel1Resource.getValueMap().get("pagePath").toString());
                                    if (cardLevel1Resource != null) {
                                        Page cardPage = cardLevel1Resource.adaptTo(Page.class);
                                        if (cardPage != null && cardPage.getProperties().containsKey("productDataPath")) {
                                            if(cardPage.getProperties().get("productDataPath") != null) {
                                                ProductModel pm = productService
                                                        .getProduct(cardPage.getProperties().get("productDataPath").toString());
                                                if (pm != null) {
                                                    seoImageList.add(new SeoImage(pm.getImagePath(), pm.getAltText()));
                                                }
                                            }
                                        } else if (cardPage.getProperties().containsKey("cardImagePath")) {
                                            seoImageList.add(new SeoImage(
                                                    cardPage.getProperties().get("cardImagePath").toString(), ""));
                                        }
                                    }
                                }
                            }                            
                        }
                    }
                }
            }

            slingQuery = $(resource).find("juniper/components/casestudyoverview");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resource : list) {
                        if (resource.getValueMap().containsKey("fragmentPath")) {
                            Resource fragmentResource = resourceResolver
                                    .getResource(resource.getValueMap().get("fragmentPath").toString());
                            if (fragmentResource != null) {
                                Optional<ContentFragment> contentFragment = Optional
                                        .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                if (contentFragment.isPresent()) {
                                    String image = getAsString(contentFragment, "image");
                                    seoImageList.add(new SeoImage(image, ""));
                                    String logo = getAsString(contentFragment, "logo");
                                    seoImageList.add(new SeoImage(logo, ""));
                                }
                            }
                        }
                    }
                }
            }

            slingQuery = $(resource).find("juniper/components/case-study-homepage");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resource : list) {
                        if (resource.getValueMap().containsKey("fragmentPath")) {
                            Resource fragmentResource = resourceResolver
                                    .getResource(resource.getValueMap().get("fragmentPath").toString());
                            if (fragmentResource != null) {
                                Optional<ContentFragment> contentFragment = Optional
                                        .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                if (contentFragment.isPresent()) {
                                    String image = getAsString(contentFragment, "image");
                                    seoImageList.add(new SeoImage(image, ""));
                                    String logo = getAsString(contentFragment, "logo");
                                    seoImageList.add(new SeoImage(logo, ""));
                                }
                            }
                        }
                    }
                }
            }

            slingQuery = $(resource).find("juniper/components/casestudy");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resource : list) {
                        if (resource.getValueMap().containsKey("fragmentPath")) {
                            Resource fragmentResource = resourceResolver
                                    .getResource(resource.getValueMap().get("fragmentPath").toString());
                            if (fragmentResource != null) {
                                Optional<ContentFragment> contentFragment = Optional
                                        .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                if (contentFragment.isPresent()) {
                                    String image = getAsString(contentFragment, "image");
                                    seoImageList.add(new SeoImage(image, ""));
                                    String logo = getAsString(contentFragment, "logo");
                                    seoImageList.add(new SeoImage(logo, ""));
                                }
                            }
                        }
                        
                        if(resource.getChild("caseStudies") != null) {
                            Iterator<Resource> iterator = resource.getChild("caseStudies").listChildren();
                            while (iterator.hasNext()) {
                                Resource caseStudyResource = iterator.next();
                                if (caseStudyResource.getValueMap().containsKey("fragmentPath")) {
                                    Resource fragmentResource = resourceResolver
                                            .getResource(caseStudyResource.getValueMap().get("fragmentPath").toString());
                                    if (fragmentResource != null) {
                                        Optional<ContentFragment> contentFragment = Optional
                                                .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                        if (contentFragment.isPresent()) {
                                            String image = getAsString(contentFragment, "image");
                                            seoImageList.add(new SeoImage(image, ""));
                                            String logo = getAsString(contentFragment, "logo");
                                            seoImageList.add(new SeoImage(logo, ""));
                                        }
                                    }
                                }
                            }                            
                        }
                    }
                }
            }

//            slingQuery = $(resource).find("juniper/components/casestudycards");
//            if (slingQuery != null) {
//                List<Resource> list = slingQuery.asList();
//                if (list != null && list.size() > 0) {
//                    for (Resource resource : list) {
//                        addCaseStudyCards(resource.getValueMap().get("fragmentPath").toString());
//                    }
//                }
//            }
            

            // Get images from hero carousel
            addImageFromComponentMultiField("hero-carousel", "slides", "imagePath", "imageAltText");

            // What we offer
            addImageFromComponentMultiField("whatweoffer", "solutions", "imagePath", "imageAltText");

            // Expandabledirectory
            addImageFromComponentMultiField("expandabledirectory", "cards", "imagePath", "altText");

            // explainerwithimages
            addImageFromComponentMultiField("explainerwithimages", "cards", "imagePath", "altText");

            // productcomparisonmatrix
            addImageFromComponentMultiField("productcomparisonmatrix", "tableHeaders", "fileReference", "altText");

            // formtextfamily
            addImageFromTextFamily("form/formtextfamily", "formimage", "fileReference", "altText");

            // textfamily - Text with descriptor and Half Image
            addImageFromTextFamily("text", "twdaif", "fileReference", "altText");

            // textfamily - Text with descriptor and small Image
            addImageFromTextFamily("text", "twdasi", "fileReference", "altText");

            // textfamily - Text with Small Image
            addImageFromTextFamily("text", "twsi", "fileReference", "altText");

            // textfamily - Text with Small Image
            addImageFromTextFamily("text", "twili", "fileReference", "altText");

            // textfamily - Text with Small Image
            addImageFromTextFamily("text", "stawi", "fileReference", "altText");

            // texfamily - Text with multiple images/icons
            addImageFromTextFamilyMultiple("text", "twmii", "images", "fileReference", "altText");

            // texfamily - Text with multiple images/icons
            addImageFromTextFamilyMultiple("text", "twibl", "twiblimages", "fileReference", "altText");

            // texfamily - Text with multiple images/icons
            addImageFromTextFamilyMultiple("text", "et", "etListItems", "fileReference", "altText");

            // Client Testimonial
            addImageFromComponent("clienttestimonial", "imagePath", "imageAltText");

            // stacked-statements-with-image
            addImageFromComponent("stacked-statements-with-image", "imagePath", "imageAltText");

            // solutionoverviewsticky
            addImageFromComponent("solutionoverviewsticky", "fileReference", "imageAltText");

            // latestnews
            addImageFromComponent("latestnews", "imagePath", "imageAltText");

            // image
            addImageFromComponent("image", "imagePath", "altText");

            // productdetailoverviewvideo
            addImageFromComponent("productdetailoverviewvideo", "fileReference", "alttext");

            // productfamilyoverview
            addImageFromComponent("productfamilyoverview", "fileReference", "imageAltText");

            // resourcecentervideo
            addImageFromComponent("resourcecentervideo", "videothumbnailimagePath", "");

            // productcomparison
            addImageFromPage("productcomparison", "product1DataPath");

            addImageFromPage("productcomparison", "product2DataPath");

            // productoverview
            addImageFromProductOverview("productoverview", "productDataPath");

            // relatedcontent
            addImageFromRelatedContent("relatedcontent", "relatedCards", "", "");

            // carouselItem
            addImageFromCarouselComponent("imagePath", "altText");

            // relatedProducts
            addImageFromRelatedProduct();

            // footer
            if (homepage.getProperties().get("footerNavFragment") != null) {
                String headerNavFragment = homepage.getProperties().get("footerNavFragment").toString();
                Resource res = resourceResolver.getResource(headerNavFragment + "/jcr:content/root/container/footer");
                if (res != null && res.getValueMap().containsKey("logoPath")) {
                    seoImageList.add(new SeoImage(res.getValueMap().get("logoPath").toString(), ""));
                }
            }

            // content-header
            slingQuery = $(resource).find("juniper/components/content-header");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resources : list) {
                        Resource featuredResource = resources.getChild("chwi");
                        if (featuredResource != null) {
                            if (featuredResource.getValueMap().containsKey("fileReference")) {
                                seoImageList.add(
                                        new SeoImage(featuredResource.getValueMap().get("fileReference").toString(),
                                                featuredResource.getValueMap().containsKey("imageAltText")
                                                        ? featuredResource.getValueMap().get("imageAltText").toString()
                                                        : ""));
                            }
                        }

                        featuredResource = resources.getChild("clhwi");
                        if (featuredResource != null) {
                            if (featuredResource.getValueMap().containsKey("fileReference")) {
                                seoImageList.add(
                                        new SeoImage(featuredResource.getValueMap().get("fileReference").toString(),
                                                featuredResource.getValueMap().containsKey("imageAltText")
                                                        ? featuredResource.getValueMap().get("imageAltText").toString()
                                                        : ""));
                            }
                        }
                    }
                }
            }
           
            // breadcrumb - SEO
            slingQuery = $(resource).find("juniper/components/breadcrumb");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    ValueMap vm = list.get(0).getValueMap();
                    // Set of default startLevel if not set on component
                    int startLevel = 4;
                    if (vm.get("startLevel") != null) {
                        startLevel = Integer.parseInt((list.get(0).getValueMap()).get("startLevel").toString());
                    }
                    int currentLevel = currentPage.getDepth();
                    while (startLevel < currentLevel) {
                        Page page = currentPage.getAbsoluteParent(startLevel);
                        if (page != null && page.getContentResource() != null
                                && !(page.getPath().equals(currentPage.getPath()))) {
                        	PageModel pageModel = new PageModel();
                        	pageModel.setTitle(page.getTitle());
                        	pagePath = resourceResolver.map(page.getPath());
                        	pagePath = externalizer.publishLink(resourceResolver, pagePath);
                        	pageModel.setPath(pagePath);
                        	breadcrumbSchemalist.add(pageModel);
                        }
                        startLevel++;
                    }
                }
            }
            // videoschema
            addVideoSchema();

            // FAQ Schema
            slingQuery = $(resource).find("juniper/components/faq");
            if (slingQuery != null) {
                List<Resource> list = slingQuery.asList();
                if (list != null && list.size() > 0) {
                    for (Resource resources : list) {
                        if (resources.getChild("faqs") != null && resources.getChild("faqs").hasChildren()) {
                            Iterator<Resource> iterator = resources.getChild("faqs").listChildren();
                            while (iterator.hasNext()) {
                                FaqModel FaqModel = (iterator.next()).adaptTo(FaqModel.class);
                                faqModelList.add(FaqModel);
                            }
                        }
                    }
                }
            }

            // Website Schema
            if (homepage.getProperties().get("headerNavFragment") != null) {
                String headerNavFragment = homepage.getProperties().get("headerNavFragment").toString();
                Resource res = resourceResolver.getResource(headerNavFragment + "/jcr:content/root/navigation");
                if (res != null) {
                    ValueMap valueMap = res.getValueMap();
                    if (valueMap.get("searchResultsPath") != null) {
                        searchUrl =  externalizer.publishLink(resourceResolver,resourceResolver.map(valueMap.get("searchResultsPath").toString()));
                    }
                    if (res.getValueMap().containsKey("logoImagePath")) {
                        seoImageList.add(new SeoImage(res.getValueMap().get("logoImagePath").toString(),
                                res.getValueMap().containsKey("logoImageAltText")
                                        ? res.getValueMap().get("logoImageAltText").toString()
                                        : ""));
                    }
                }
            }

        }
        if (!StringUtils.isEmpty(ogImage)) {
            ogImage =  externalizer.publishLink(resourceResolver,ogImage);
        }
        if (!StringUtils.isEmpty(twitterImage)) {
            twitterImage =  externalizer.publishLink(resourceResolver,twitterImage);
        }
        if(seoImageList.size()>0)
        {
            for(SeoImage sem:seoImageList)
            {
                if(!StringUtils.isEmpty(sem.getImagePath()))
                {
                   try {
                    sem.setImagePath(externalizer.publishLink(resourceResolver,sem.getImagePath()));
                   }catch(Exception e)
                   {
                       log.error("Exception in Externalizing seo image URLs", e);
                   }
                }
            }
        }
    }

    private String getAsString(Optional<ContentFragment> contentFragment, String name) {
        return contentFragment.map(cf -> cf.getElement(name)).map(ContentElement::getContent).orElse(StringUtils.EMPTY);
    }
        
    private void addVideoSchema() {
    	session = resourceResolver.adaptTo(Session.class);
        try {
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (pageManager != null) {
                session = resourceResolver.adaptTo(Session.class);
                Map<String, String> map = new HashMap<String, String>();
                map.put("path", currentPage.getPath() + "/" + JcrConstants.JCR_CONTENT);
                map.put("nodename", "seoVideoDetails");
                map.put("orderby.sort", "asc");
                map.put("p.limit", "-1");
                Query query = builder.createQuery(PredicateGroup.create(map), session);
                SearchResult result = query.getResult();
                for (Hit hit : result.getHits()) {
                    VideoModel videoModel = hit.getResource().adaptTo(VideoModel.class);
                    if (videoModel != null) {
	                    if (videoModel.getUrls() != null) {
		                    for(ThumbnailModel x : videoModel.getUrls()) {
		                    	thumbnailUrlPath = externalizer.publishLink(resourceResolver, x.getThumbnailUrl());
		                    	x.setThumbnailUrl(thumbnailUrlPath);
		                    }
	                   }
	                   if (StringUtils.isNotEmpty(videoModel.getName())) {
	                    videoModelList.add(videoModel);
	                   }
                    }
                }
            }
        } catch (Exception e) {
            log.error("Exception in addVideoSchema", e);
        }
    }

//    private void addCaseStudyCards(String fragmentPath) {
//        List<CaseStudyModel> caseStudyModelList = new ArrayList<CaseStudyModel>();
//        try {
//            session = resourceResolver.adaptTo(Session.class);
//            Map<String, String> map = new HashMap<String, String>();
//            log.info("fragment path :: " + fragmentPath);
//            if (fragmentPath != null) {
//                map.put("path", fragmentPath);
//            } else {
//                map.put("path", "/content/dam/juniper/case-studies");
//            }
//            map.put("type", "dam:Asset");
//            map.put("boolproperty", "jcr:content/contentFragment");
//            map.put("boolproperty.value", "true");
//            map.put("orderby", "@jcr:content/data/master/cardRank");
//            map.put("orderby.sort", "asc");
//            map.put("p.limit", "-1");
//            Query query = builder.createQuery(PredicateGroup.create(map), session);
//            SearchResult result = query.getResult();
//            CaseStudyModel caseStudyModel;
//            for (Hit hit : result.getHits()) {
//                String path = hit.getPath();
//                caseStudyModel = caseStudyService.getCaseStudy(path);
//                if (caseStudyModel != null) {
//                    if (StringUtils.isNotEmpty(caseStudyModel.getImage()) && !"#".equals(caseStudyModel.getImage())) {
//                        seoImageList.add(new SeoImage(caseStudyModel.getImage(), caseStudyModel.getImageAltText()));
//                        seoImageList.add(new SeoImage(caseStudyModel.getLogo(), caseStudyModel.getLogoAltText()));
//                    }
//                }
//            }
//        } catch (Exception e) {
//            log.error("Can't extract content fragment information from the resource", e);
//
//        }
//    }

    private void addImageFromPage(String componentName, String propertyName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    if (resource.getValueMap().containsKey(propertyName)) {
                        Resource res = resourceResolver
                                .getResource(resource.getValueMap().get(propertyName).toString());
                        if (res != null) {
                            Page page = res.adaptTo(Page.class);
                            if(page != null && page.getProperties().get("productDataPath") != null) {
                                ProductModel pm = productService
                                        .getProduct(page.getProperties().get("productDataPath").toString());
                                if (pm != null) {
                                    seoImageList.add(new SeoImage(pm.getImagePath(), pm.getAltText()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void addImageFromProductOverview(String componentName, String propertyName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    if (resource.getParent().getParent().getValueMap().containsKey(propertyName)) {
                        ProductModel pm = productService.getProduct(
                                resource.getParent().getParent().getValueMap().get(propertyName).toString());
                        if (pm != null) {
                            if (pm.getBenefits() != null && pm.getBenefits().size() > 0) {
                                for (ProductBenefitsModel pbm : pm.getBenefits()) {
                                    seoImageList.add(new SeoImage(pbm.getImagePath(), pbm.getAltText()));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void addImageFromCarouselComponent(String pathPropName, String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/carouselimagevideoitem");
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    resource = resource.getChild("image");
                    if (resource != null && resource.getValueMap().containsKey(pathPropName)) {
                        seoImageList.add(new SeoImage(resource.getValueMap().get(pathPropName).toString(),
                                resource.getValueMap().containsKey(altTextPropName)
                                        ? resource.getValueMap().get(altTextPropName).toString()
                                        : ""));
                    }
                }
            }
        }
    }

    private void addImageFromComponent(String componentName, String pathPropName, String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    if (resource.getValueMap().containsKey(pathPropName)) {
                        seoImageList.add(new SeoImage(resource.getValueMap().get(pathPropName).toString(),
                                resource.getValueMap().containsKey(altTextPropName)
                                        ? resource.getValueMap().get(altTextPropName).toString()
                                        : ""));
                    }
                }
            }
        }
    }

    private void addImageFromComponentMultiField(String componentName, String nodeName, String pathPropName,
            String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resources : list) {
                    if (resource != null && resource.getChild(nodeName) != null) {
                        Iterator<Resource> iterator = resources.getChild(nodeName).listChildren();
                        while (iterator.hasNext()) {
                            Resource res = iterator.next();
                            if (res.getValueMap().containsKey(pathPropName)) {
                                seoImageList.add(new SeoImage(res.getValueMap().get(pathPropName).toString(),
                                        res.getValueMap().containsKey(altTextPropName)
                                                ? res.getValueMap().get(altTextPropName).toString()
                                                : ""));
                            }
                        }
                    }
                }
            }
        }
    }

    private void addImageFromRelatedContent(String componentName, String nodeName, String pathPropName,
            String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resources : list) {
                    if(resources.getChild(nodeName) != null) {
                        Iterator<Resource> iterator = resources.getChild(nodeName).listChildren();
                        while (iterator.hasNext()) {
                            Resource res = iterator.next();
                            if (res.getValueMap().containsKey("layoutType")
                                    && "dynamic".equalsIgnoreCase(res.getValueMap().get("layoutType").toString())) {
                                res = res.getChild("dynamic");
                                if (res.getValueMap().containsKey("fragmentPath")) {
                                    Resource fragmentResource = resourceResolver
                                            .getResource(res.getValueMap().get("fragmentPath").toString());
                                    if (fragmentResource != null) {
                                        Optional<ContentFragment> contentFragment = Optional
                                                .ofNullable(fragmentResource.adaptTo(ContentFragment.class));
                                        if (contentFragment.isPresent()) {
                                            String image = getAsString(contentFragment, "logo");
                                            String altText = getAsString(contentFragment, "logo_alt_text");
                                            seoImageList.add(new SeoImage(image, altText));
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                }
            }
        }
    }

    private void addImageFromTextFamily(String componentName, String nodeName, String pathPropName,
            String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    resource = resource.getChild(nodeName);
                    if (resource != null && resource.getValueMap().containsKey(pathPropName)) {
                        seoImageList.add(new SeoImage(resource.getValueMap().get(pathPropName).toString(),
                                resource.getValueMap().containsKey(altTextPropName)
                                        ? resource.getValueMap().get(altTextPropName).toString()
                                        : ""));
                    }
                }
            }
        }
    }

    private void addImageFromTextFamilyMultiple(String componentName, String nodeName, String childNodeName,
            String pathPropName, String altTextPropName) {
        SlingQuery slingQuery = $(resource).find("juniper/components/" + componentName);
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resource : list) {
                    resource = resource.getChild(nodeName);
                    if (resource != null) {
                        Resource childResource = resource.getChild(childNodeName);
                        if (childResource != null && childResource.hasChildren()) {
                            Iterator<Resource> iterator = resource.getChild(childNodeName).listChildren();
                            while (iterator.hasNext()) {
                                Resource res = iterator.next();
                                if (res != null && res.getValueMap().containsKey(pathPropName)) {
                                    seoImageList.add(new SeoImage(res.getValueMap().get(pathPropName).toString(),
                                            res.getValueMap().containsKey(altTextPropName)
                                                    ? res.getValueMap().get(altTextPropName).toString()
                                                    : ""));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void addImageFromRelatedProduct() {
        SlingQuery slingQuery = $(resource).find("juniper/components/relatedproducts");
        if (slingQuery != null) {
            List<Resource> list = slingQuery.asList();
            if (list != null && list.size() > 0) {
                for (Resource resources : list) {
                    if (resources.getChild("pagePathContainer") != null) {
                        Iterator<Resource> iterator = resources.getChild("pagePathContainer").listChildren();
                        while (iterator.hasNext()) {
                            Resource relatedProductsResource = iterator.next();
                            if (relatedProductsResource.getValueMap().containsKey("pagePath")) {
                                Resource res = resourceResolver
                                        .getResource(relatedProductsResource.getValueMap().get("pagePath").toString());
                                if (res != null) {
                                    Page page = res.adaptTo(Page.class);
                                    if (page != null && page.getProperties().containsKey("productDataPath")) {
                                        if(page.getProperties().get("productDataPath") != null) {
                                            ProductModel pm = productService
                                                    .getProduct(page.getProperties().get("productDataPath").toString());
                                            if (pm != null) {
                                                seoImageList.add(new SeoImage(pm.getImagePath(), pm.getAltText()));
                                            }
                                        }

                                    } else if (page.getProperties().containsKey("cardImagePath")) {
                                        seoImageList.add(
                                                new SeoImage(page.getProperties().get("cardImagePath").toString(), ""));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}