package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SitemapPageModel {
    protected final Logger Log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String pageTitle;

    @Inject
    @Named("jcr:title")
    private String title;
    
    @Inject
    @Via("valueMap")
    protected String pagePath;

    private String pageLink;
    
    private String name;

    @SlingObject
    private Resource resource;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        pageLink = LinkUtil.addHTMLIfPage(resourceResolver, resource.getParent().getPath());
        name = resource.getParent().getName();
    }
}
