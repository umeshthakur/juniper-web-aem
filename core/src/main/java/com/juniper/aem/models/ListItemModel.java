package com.juniper.aem.models;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ListItemModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String primaryText;

    @Inject
    private String secondaryText;

    @Inject
    private String title;
    
    @Inject
    private String symbol;

    @Inject
    private String titleLink;

    @Inject
    private String titleLinkTarget;

    @Inject
    private String intro;

    @Inject
    private String fullIntro;

    @Inject
    private String ctaText;
    
    @Inject
    private String ctaStyle;

    @Inject
    private String ctaLink;

    @Inject
    private String target;

    @Inject
    private String fileReference;

    @Inject
    private String altText;

    @Inject
    private String subTitle;
    
    @Inject
    private String horizontalLine;

    @Inject
    private String imageLink;

    @Inject
    private String imageLinkTarget;

    private String leftcolumneyebrow;

    private String rightcolumneyebrow;

    @Inject
    Resource resource;

    @ChildResource
    private List<TextFamilyExpandableTextModelItems> leftcolumn;

    @ChildResource
    private List<TextFamilyExpandableTextModelItems> rightcolumn;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {

        Node node = resource.adaptTo(Node.class);
        try {

            leftcolumneyebrow = node.getNode("leftcolumn").getProperty("eyebrow").getString();
            rightcolumneyebrow = node.getNode("rightcolumn").getProperty("eyebrow").getString();

        } catch (RepositoryException e) {

            e.printStackTrace();
        }

        titleLink = LinkUtil.addHTMLIfPage(resourceResolver, titleLink);
        imageLink = LinkUtil.addHTMLIfPage(resourceResolver, imageLink);
        ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, ctaLink);
    }

}