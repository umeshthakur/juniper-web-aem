package com.juniper.aem.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContentModel {

    @Inject
    private String layoutType;

    @ChildResource
    private ContentHeaderModel chwt;

    @ChildResource
    private ContentHeaderModel chwi;

    @ChildResource
    private ContentHeaderModel clhwi;

}
