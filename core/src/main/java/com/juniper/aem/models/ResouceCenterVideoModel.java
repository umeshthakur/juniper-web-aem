package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.Utils;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResouceCenterVideoModel {

	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	@Inject
	private List<ResourceCenterSubCategoryModel>  categories; 

	@Inject
	private String resourcecenterheading;

	@Inject
	private String introductionText;

	@Inject
	private String ctaText;

	@Inject
	private String ctaStyle;

	@Inject
	private String ctaLink;

	@Inject
	private String ctatarget;

	@Inject
	private String videoTitle;

	@Inject
	private String videoLink;

	@Inject
	private String bgcircleColor; 


	@Inject
	private String videothumbnailimagePath;

	@Inject
	private String description;

	@PostConstruct
	protected void init() {
		String ctalink=this.getCtaLink();
		this.setCtaLink(Utils.formatLink(ctalink));
	}
}
