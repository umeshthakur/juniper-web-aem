package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.juniper.aem.services.CaseStudyService;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RelatedContentDataModel {

    private static final Logger LOG = LoggerFactory.getLogger(RelatedContentDataModel.class);

    @Inject
    private String layoutType;

    @Inject
    private RelatedContentItemModel dynamic;

    @Inject
    @Named("static")
    private RelatedContentItemModel staticcontent;

    @Inject
    private CaseStudyService caseStudyService;

    private CaseStudyModel caseStudyModel;

    @PostConstruct
    protected void init() {
        try {

            if ("dynamic".equalsIgnoreCase(this.layoutType)) {
                caseStudyModel = caseStudyService.getCaseStudy(this.dynamic.getFragmentPath());
            }

        } catch (NullPointerException e) {
            LOG.error("Could Not get CaseStudy Model", e);
        }

    }

}