package com.juniper.aem.models.globalnav;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class V2SubNavItemModel {

    @ChildResource
    private List<V2SubNavTierOneItemModel> column1;

    @ChildResource
    private List<V2SubNavTierOneItemModel> column2;
    
    @ChildResource
    private List<V2SubNavTierOneItemModel> column3;
    
}