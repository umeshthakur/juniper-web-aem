package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductModel.class);
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @Inject
    @Named("cq:tags")
    private String[] tags;

    @Inject
    @Via("valueMap")
    private String[] productCategory;

    @Inject
    @Via("valueMap")
    private String[] secondaryProductCategory;

    @Inject
    @Named("fullDescription")
    private String description;

    @Inject
    @Via("valueMap")
    private String shortDescription;

    @Inject
    @Named("productNameLong")
    private String title;

    @Inject
    @Via("valueMap")
    private String productNameShort;

    @Inject
    @Via("valueMap")
    private String[] productFamily;

    @Inject
    @Via("valueMap")
    private String[] featureTags;

    @Inject
    @Via("valueMap")
    private String imagePath;

    @Inject
    @Via("valueMap")
    private String eyebrow;

    @Inject
    @Via("valueMap")
    private String altText;

    @Inject
    @Via("valueMap")
    private String primaryDataSheet;

    private String pagePath;

    private String[] filterTags;

    @ChildResource
    private List<ProductFeaturesModel> features;

    @ChildResource
    private List<ProductBenefitsModel> benefits;

    @ChildResource
    private List<ProductSpecsModel> specs;
    
    private  LinkedHashMap<String, String> specsMap;

    @ChildResource
    private List<ProductPromosModel> promos;

    @ChildResource
    private List<ProductAssetsModel> assets;

    @ChildResource
    private List<ProductViewDetailsModel> viewdetails;

    private List<String> tagTitles = new ArrayList<>();

    @PostConstruct
    protected void init() {
        if (imagePath != null) {
            this.imagePath=LinkUtil.addWebRendition(resourceResolver, imagePath);
            ProductAssetsModel productAssetsModel = new ProductAssetsModel();
            productAssetsModel.setFileReference(imagePath);
            productAssetsModel.setAltText(altText);
            if (assets == null) {
                assets = new ArrayList<>();
            }
            assets.add(0, productAssetsModel);
        }
        if(specs !=null)
        {
        specsMap = specs.stream()
                .collect(Collectors.toMap(ProductSpecsModel::getProductSpecs,ProductSpecsModel::getProductSpecsValue,(oldValue,newValue)->oldValue,LinkedHashMap::new));
        }
    }
}