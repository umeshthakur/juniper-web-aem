package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import static org.apache.sling.query.SlingQuery.$;
import org.apache.sling.query.SlingQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.CaseStudyService;
import com.juniper.aem.services.ResourceResolverService;

import lombok.Getter;
import lombok.Setter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class CaseStudyCardsModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());


    @Inject
    private String fragmentPath;

    @Inject
    private String[] caseStudyTags;

    @Inject
    private String theme;

    @Inject
    private QueryBuilder builder;

    private Session session;

    @Inject
    private ResourceResolverService resourceResolverService;

    @Inject
    private CaseStudyService caseStudyService;

    private CaseStudyModel caseStudyModel;

    private List<CaseStudyModel> caseStudyModelList;

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;

    @PostConstruct
    protected void init() { 
        caseStudyModelList= new ArrayList<CaseStudyModel>();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);

            if (pageManager != null && tagManager!=null ) {
                String languageRoot = pageManager.getContainingPage(resource).getAbsoluteParent(3).getPath();
                Map<String, String> map = new HashMap<>();

                map.put("path", languageRoot);
                map.put("type", "cq:Page");
                map.put("1_property", "jcr:content/cq:tags");
                map.put("1_property.or", "true");

                for (int i = 0; i < this.caseStudyTags.length; i++) {
                    map.put("1_property." + i + 1 + "_value", caseStudyTags[i]);
                }

                map.put("2_property", "jcr:content/cq:template");
                map.put("2_property.value", "/conf/juniper/settings/wcm/templates/case-study");

                map.put("orderby","@jcr:content/cardRank");
                map.put("orderby.sort", "asc");
                map.put("p.limit", "-1");

                Query query = builder.createQuery(PredicateGroup.create(map), session);
                log.info("Query ----->>");
                log.info(query.getPredicates().toString());
                SearchResult result = query.getResult();

                for (Hit hit : result.getHits()) {   

                    Resource caseStudyResource = hit.getResource();
                    SlingQuery slingQuery = $(caseStudyResource).find("juniper/components/casestudyoverview");
                    Resource overviewResource = null;

                    if (slingQuery != null) {
                        List<Resource> list = slingQuery.asList();

                        if (list != null && list.size() > 0) {
                            overviewResource = list.get(0);
                        } 
                        if(overviewResource!=null ) {

                            if(overviewResource.getValueMap().get("fragmentPath") != null) {
                                String path = overviewResource.getValueMap().get("fragmentPath").toString();
                                caseStudyModel = caseStudyService.getCaseStudy(path);
                                caseStudyModel.setHomepage(pageManager.getContainingPage(resource).getAbsoluteParent(3));
                                
                                if(caseStudyModel !=null)
                                {
                                    
                                    caseStudyModel.setCaseStudyPagePath(caseStudyResource.getPath());
                                    
                                    Page caseStudyPagePath = pageManager.getContainingPage(caseStudyResource.getPath());
                                    Tag[] filterTags = tagManager.getTags(caseStudyPagePath.getContentResource());

                                    List<String> caseStudyFilterTag = new ArrayList<String>();
                                    List<String> caseStudyFilterName = new ArrayList<String>();
                                    List<String> caseStudyFilterCategory = new ArrayList<String>();

                                    for (int i = 0; i < filterTags.length; i++) {
                                        
                                        String filterTag = filterTags[i].toString().split("juniper/")[1];
                                        caseStudyFilterTag.add(filterTag);
                                        
                                        String filterTagCategory=  filterTag.split("/")[0];
                                        String filterTagName =filterTag.split("/")[1];

                                        caseStudyFilterName.add(filterTagName);
                                        caseStudyFilterCategory.add(filterTagCategory);
                                    }

                                    caseStudyModel.setCaseStudyFilterTags(caseStudyFilterTag);
                                    caseStudyModel.setCaseStudyFilterName(caseStudyFilterName);
                                    caseStudyModel.setCaseStudyFilterCategory(caseStudyFilterCategory);
                                    
                                    log.info("Taga-->>" + caseStudyPagePath.getName().toString());
                                    log.info(caseStudyFilterTag.toString());
                                    log.info(caseStudyFilterName.toString());
                                    
                                    
                                    caseStudyModelList.add(caseStudyModel);
                                }
                            }
                        }
                    }


                }
            }
        } catch (Exception e) {
            log.error("Can't find content fragment information for the resource", e);

        }
    }
}