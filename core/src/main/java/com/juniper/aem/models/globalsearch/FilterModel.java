package com.juniper.aem.models.globalsearch;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FilterModel {

    @ValueMapValue
    private String name;

    @ValueMapValue
    private String value;

    @ValueMapValue
    private String code;

    @ValueMapValue
    private String isSecure;

    @ChildResource
    private List<FilterModel> items;

}
