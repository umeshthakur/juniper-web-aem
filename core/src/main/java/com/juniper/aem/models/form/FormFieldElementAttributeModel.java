package com.juniper.aem.models.form;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.FeedbackSurveyService;
import com.juniper.aem.services.ResourceResolverService;
import lombok.Getter;
import lombok.Setter;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import java.io.IOException;
import java.util.List;

@Getter
@Setter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormFieldElementAttributeModel {

    private static final Logger LOG = LoggerFactory.getLogger(FormFieldElementAttributeModel.class);

    @Inject
    @Via("valueMap")
    private String heading;

    @Inject
    @Via("valueMap")
    private String name;

    @Inject
    @Via("valueMap")
    private String label;

    @Inject
    @Via("valueMap")
    private String id;

    @Inject
    @Via("valueMap")
    private String value;

    @Inject
    @Via("valueMap")
    private String isRequired;

    @Inject
    @Via("valueMap")
    private String overridingErrorMessage;

    @Inject
    @Via("valueMap")
    private String newWindow;

    @Inject
    @Via("valueMap")
    private String privacytext;

    @Inject
    @Via("valueMap")
    private String optinoutlabel;

    @Inject
    @Via("valueMap")
    private String optinname;

    @Inject
    @Via("valueMap")
    private String optinid;

    @Inject
    @Via("valueMap")
    private String emailErrorMessage;

    @Inject
    @Via("valueMap")
    private String captchaServiceURL;
    
    @Inject
    @Via("valueMap")
    private String captchaErrorMessage;

    @Inject
    @Via("valueMap")
    private String htmlcode;

    private String errorMessage;

    @ChildResource
    private List<FormSelectOptionsContainerModel> formSelectOptionsContainer;

    @ChildResource
    private List<RadioItemsContainerModel> radioItemsContainer;

    @ChildResource
    private List<CheckboxItemsContainerModel> checkboxItemsContainer;

    @Self
    Resource resource;

    @Inject
    private ResourceResolverService resourceResolverService;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Reference
    @Inject
    private FeedbackSurveyService feedbackSurveyService;


    @PostConstruct
    protected void init() throws ItemNotFoundException, RepositoryException {
        Node node = resource.adaptTo(Node.class);
        if (node != null && node.getParent().getParent().hasProperty("errorMessage")) {
            errorMessage = node.getParent().getParent().getProperty("errorMessage").getString();
        }
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        Page pageName = pageManager.getContainingPage(resource.getPath());
        ValueMap valueMap = resource.adaptTo(ValueMap.class);
        String feildName = valueMap.get("name", String.class);
        if (feildName != null && feildName.equalsIgnoreCase("TOKEN") && pageName.getPath().contains("feedback")) {
            HttpClient client = new DefaultHttpClient();
            HttpPost request = new HttpPost(feedbackSurveyService.getSurveyUrl());
            request.addHeader("Content-Type", "application/json;charset=UTF-8");
            request.addHeader("API-version", "3.0");
            HttpResponse response;
            String token = "";
            try {
                StringEntity entity = new StringEntity("{\r\n" + "  \"username\": \""
                        + feedbackSurveyService.getSurveyuserName() + "\",\r\n" + "  \"password\": \""
                        + feedbackSurveyService.getSurveypassword() + "\",\r\n" + "  \"companyName\": \""
                        + feedbackSurveyService.getSurveyCompanyName() + "\"\r\n" + "  \r\n" + "}\r\n" + "");
                request.setEntity(entity);
                response = client.execute(request);
                int responseStatusCode = response.getStatusLine().getStatusCode();
                LOG.debug("response code:" + responseStatusCode);
                token = EntityUtils.toString(response.getEntity()).trim();
                token = token.substring(1, token.length() - 1);
                String responseBody = response.toString();
                this.setValue(token);
            } catch (ClientProtocolException e) {
                LOG.error("Client Protocal Exception" + e);
            } catch (IOException e) {
                LOG.error("Cannot Read Token" + e);
            } catch (RuntimeException ex) {
                request.abort();
                LOG.error("Runtime Exception" + ex);
                throw ex;
            } finally {
                try {
                    client.getConnectionManager().shutdown();
                } catch (Exception e) {
                    LOG.error("Cannot Close Connection" + e);
                }
            }
        }
    }

}
