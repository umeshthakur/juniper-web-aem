package com.juniper.aem.models;

import java.util.List;
import javax.inject.Inject;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RowsModel {
    
    @Inject
    private String feature;
    
    @Inject
    private String isExpandable;

    @Inject
    private List<ColumnsModel>  columns;
    

}