package com.juniper.aem.models;

import java.util.*;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentVariation;
import com.adobe.cq.dam.cfm.FragmentTemplate;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.LinkUtil;
import com.juniper.aem.utils.Utils;

import lombok.Getter;
import lombok.Setter;

@Setter
@Model(adaptables = Resource.class,
defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class CaseStudyModel {
    private static final Logger LOG = LoggerFactory.getLogger(CaseStudyModel.class);

    public static final String MODEL_TITLE = "Case Study";
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject 
    @Named("fragmentPath")
    private String fragmentPath;

    @Inject 
    @Named("variationName")
    private String variation;

    @Inject
    @Named("cq:tags")
    private String[] CqTags;

    @Inject
    ResourceResolver resourceResolver;

    @Inject
    private ProductService productService;

    private Optional<ContentFragment> contentFragment;

    @Getter
    private List<ProductModel> productModelList = new ArrayList<ProductModel>();

    @Getter
    @Setter
    private String caseStudyPagePath;
    
    @Getter
    @Setter
    private List<String> caseStudyFilterTags;

    @Getter
    @Setter
    private List<String> caseStudyFilterName;
    
    @Getter
    @Setter
    private  List<String> caseStudyFilterCategory;

    @AemObject
    private PageManager pageManager;
    
    @Getter
    @Setter
    private Page homepage;

    @SlingObject
    private Resource resource;

    @PostConstruct
    public void init() {
        Resource fragmentResource = resourceResolver.getResource(fragmentPath);
        if(fragmentResource != null)
        {
            contentFragment = Optional.ofNullable(fragmentResource.adaptTo(ContentFragment.class));
        }
        else
        {
            contentFragment = Optional.ofNullable(null);
        }
        String[] productPages = this.getProductPages();
        for (java.lang.String productPagePath : productPages) {
            addToModelList(productPagePath.trim());
        }
                       
    }

    public String getModel() {
        return contentFragment
                .map(ContentFragment::getTemplate)
                .map(FragmentTemplate::getTitle)
                .orElse(StringUtils.EMPTY);
    }

    public String getCompany() {
        return getAsString("company");
    }

    public String getLogo() {
        return LinkUtil.addWebRendition(resourceResolver,getAsString("logo"));
    }

    public String getLogoAltText() {
        return getAsString("logo_alt_text");
    }

    public String getDescription() {
        return getAsString("description");
    }

    public String getSize() {
        return getAsString("size");
    }

    public String getImage() {
        return LinkUtil.addWebRendition(resourceResolver,getAsString("image"));
    }

    public String getImageAltText() {
        return getAsString("image_alt_text");
    }

    public String getVideoURL() {
        return getAsString("videoURL");
    }

    public String getVideoAltText() {
        return getAsString("video_alt_text");
    }

    public String getTitle() {
        return getAsString("title");
    }
    public String getQuoteAuthorInfo() {
        return getAsString("quote_author_info");
    }

    public String getQuoteText() {
        return getAsString("quote_text");
    }

    public String getTitleLink() {
        return Utils.formatLink(getAsString("title_link"));
    }

    public String getIntroduction() {
        return getAsString("introduction");
    }

    public String getIntroductionLong() {
        return getAsString("introduction_long");
    }

    public String getCustomerSuccess() {
        return getAsString("customer_success");
    }

    public String getChallengeTitle() {
        return getAsString("challenge_title");
    }

    public String getChallenge() {
        return getAsString("challenge");
    }

    public String getSolutionTitle() {
        return getAsString("solution_title");
    }

    public String getSolution() {
        return getAsString("solution");
    }

    // Page link is used for pulling from content fragment
    public String getPageLink() {
        return  Utils.formatLink(getAsString("pageLink"));
    }

    public String getOutcomeTitle() {
        return getAsString("outcome_title");
    }

    public String getOutcome() {
        return getAsString("outcome");
    }

    public String[] getIndustries() {

        return resolveTags(getAsStringArray("industries"));

    }

    public String[] getSolutions() {

        return resolveTags(getAsStringArray("solutions"));
    }

    public String[] getProducts() {

        return resolveTags(getAsStringArray("products"));
    }

    public String[] getRegions() {

        return resolveTags(getAsStringArray("regions"));
    }

    public String[] getTags() {
        Stream<String> stream = Stream.of();
        stream = Stream.concat(stream, Arrays.stream(getIndustries()));
        stream = Stream.concat(stream, Arrays.stream(getSolutions()));
        stream = Stream.concat(stream, Arrays.stream(getProducts()));
        stream = Stream.concat(stream, Arrays.stream(getRegions()));
        return resolveTags(stream.toArray(String[]::new));
    }

    public String[] getProductPages() {
        return getAsStringArray("product-pages");
    }

    private String getAsString(String name) {
        if(isVariant()) {
            return contentFragment
                    .map(cv -> cv.getElement(name).getVariation(variation))
                    .map(ContentVariation::getContent)
                    .orElse(StringUtils.EMPTY);
        }
        else {
            return contentFragment
                    .map(cf -> cf.getElement(name))
                    .map(ContentElement::getContent)
                    .orElse(StringUtils.EMPTY);

        }
    }

    private String[] getAsStringArray(String name) {
        String element = getAsString(name);
        LOG.info("For {} we found these tags {}", name, element);
        return element.split(",|\\R");
    }

    protected boolean isVariant() {
        return (variation != null && !variation.isEmpty() && !"master".equalsIgnoreCase(variation));
    }


    protected String[] resolveTags(String[] tagNames) {
        ArrayList<String> titles = new ArrayList<>();
        
        if(homepage == null) {
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            Page current = pageManager.getContainingPage(resource.getPath());
            homepage = current.getAbsoluteParent(3);
        }
        
        Locale lc = null;
        
        if(homepage.getLanguage().getLanguage() != null &&
                homepage.getLanguage().getCountry() != null &&
                !homepage.getLanguage().getLanguage().equalsIgnoreCase("") &&
                !homepage.getLanguage().getCountry().equalsIgnoreCase("")) {
            
            lc = new Locale(homepage.getLanguage().getLanguage(), homepage.getLanguage().getCountry());            
        } 
        
        try {
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if(tagManager != null) {
                if(tagNames != null) {
                    log.info("Looking up Titles for tag names <{}>", (Object) tagNames);
                    int len = tagNames.length;
                    //for (java.lang.String tagName : tagNames) {
                    for (int i = 0; i < tagNames.length; i++) {
                        log.info("Resolving tag name {}", tagNames[i]);
                        Tag tag = tagManager.resolve(tagNames[i]);
                        if (tag != null) {

                            String tagTitle = tag.getTitle();
                            log.info("Tag found adding title {}", tagTitle);

                            if(lc != null) {
                                titles.add(tag.getTitle(lc));
                            } else {
                                titles.add(tag.getTitle());
                            }
                        }
                    }
                    //log.info("Returning {} titles <{}>", titles.size(), (Object) titles);
                }
                else {
                    log.info("No tag names found. Returning empty array.");
                }
            }
            else {
                log.warn("Tag Manager is null. Returning empty array.");
            }
        }catch(Exception e)
        {
            log.error("error in resolve Tags", e);
        }
        return titles.toArray(new String[0]);
    }
    protected void addToModelList(String pagePath) {
        Page productPage=pageManager.getPage(pagePath);
        if(productPage!=null)
        {
            ValueMap productPageProperties = productPage.getProperties();
            if (productPageProperties != null && productPageProperties.containsKey("productDataPath")) {
                String productDataPath = productPageProperties.get("productDataPath",String.class);
                if (productDataPath != null) {
                    ProductModel productModel = productService.getProduct(productDataPath);
                    if (productModel != null) {
                        productModel.setPagePath(Utils.formatLink(pagePath));
                        productModelList.add(productModel);
                    }
                }
            }
        }
    }
}
