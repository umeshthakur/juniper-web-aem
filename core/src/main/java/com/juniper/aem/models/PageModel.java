package com.juniper.aem.models;

import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Inject;


@Getter
@Setter
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class PageModel {
    protected final Logger Log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String title;
    
    @Inject
    private String path;

}
