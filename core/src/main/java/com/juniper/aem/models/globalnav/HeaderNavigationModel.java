package com.juniper.aem.models.globalnav;

import lombok.Getter;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeaderNavigationModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @ChildResource
    private NavSubTypeModel nav1;

    @ChildResource
    private NavSubTypeModel nav2;

    @ChildResource
    private NavSubTypeModel nav3;

    @ChildResource
    private NavSubTypeModel nav4;

    @ChildResource
    private NavSubTypeModel nav5;

    @ChildResource
    private NavSubTypeModel nav6;

    @ChildResource(name = "top_panel")
    private NavigationTopPanelItemModel topPanelModel;

    @ValueMapValue
    private String logoLink;

    @ValueMapValue
    @Default(values = "_self")
    private String logoLinkTarget;

    @ValueMapValue
    private String logoImagePath;

    @ValueMapValue
    private String logoImageAltText;

    @ValueMapValue
    private String navLayout;

    @ValueMapValue
    private String searchPlaceholder;

    @ValueMapValue
    private String searchResultsPath;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @PostConstruct
    protected void init() {
        searchResultsPath = LinkUtil.addHTMLIfPage(resourceResolver, searchResultsPath);
    }

}
