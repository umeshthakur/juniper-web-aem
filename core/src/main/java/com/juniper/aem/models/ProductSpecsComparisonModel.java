package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.PageManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.juniper.aem.pojos.Details;
import com.juniper.aem.pojos.L10n;
import com.juniper.aem.pojos.ProductItem;
import com.juniper.aem.pojos.SpecCategory;
import com.juniper.aem.pojos.SpecsCompare;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.Utils;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductSpecsComparisonModel {
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    @Inject
    private List<ProductsDataContainerModel> productsDataContainer;
    @Inject
    private ProductService productService;
    @SlingObject
    private ResourceResolver resourceResolver;
    @SlingObject
    private Resource resource;
    private List<ProductModel> productList;
    private LinkedHashSet<String> specList;
    @Inject
    private String title;
    @Inject
    private String selectorLabel;
    @Inject
    private String addAllLabel;
    @Inject
    private String itemsLabel;
    @Inject
    private String ctaLabel;
    @Inject
    private String back;
    @Inject
    private String compareJson;
    @ChildResource
    private Resource specs;
    PageManager pageManager;

    @PostConstruct
    protected void init() {
        Gson gsonObj = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();
        productList = new ArrayList<ProductModel>();
        specList = new LinkedHashSet<String>();
        L10n backLabel = new L10n();
        backLabel.setBack(back);
        List<SpecCategory> specListing = new ArrayList<>();
        //NPE Check
        if(specs != null) {
            Iterator<Resource> iterator = specs.listChildren();
            while (iterator.hasNext()) {
                ValueMap valueMap = iterator.next().getValueMap();
                specListing.add(new SpecCategory(valueMap.get("productSpecs", String.class),
                        valueMap.get("productSpecs", String.class)));
            }
        }
        try {
            if (productsDataContainer.size() > 0) {

                List<ProductItem> productdetails = new ArrayList<>();
                for (ProductsDataContainerModel product : productsDataContainer) {
                    Map<String, String> inputMap = new HashMap<String, String>();
                    Details detail = new Details();
                    String productPath = product.getProductDataPath();
                    log.debug("path" + productPath);
                    if (productPath != null) {
                        ProductModel productModel = productService.getProduct(productPath);
                        if (productModel != null) {
                            if (productModel.getPrimaryDataSheet() != null) {
                                productModel.setPrimaryDataSheet(Utils.formatLink(productModel.getPrimaryDataSheet()));
                            }
                            productModel.setPagePath(Utils.formatLink(product.getCtaLink()));
                            detail.setImage(productModel.getImagePath());
                            detail.setPage(Utils.formatLink(product.getCtaLink()));
                            productList.add(productModel);
                            Iterator<ProductSpecsModel> it = productModel.getSpecs().iterator();
                            while (it.hasNext()) {
                                ProductSpecsModel specModel = it.next();
                                specList.add(specModel.getProductSpecs());
                            }
                        }
                        for (String spec : specList) {
                            inputMap.put(spec, productModel.getSpecsMap().get(spec));
                        }
                        productdetails.add(new ProductItem(productModel.getProductNameShort(),
                                productModel.getProductNameShort(), detail, inputMap));
                    }
                }
                SpecsCompare compare = new SpecsCompare(title, selectorLabel, addAllLabel, itemsLabel, ctaLabel,
                        specListing, productdetails, backLabel);
                compareJson = gsonObj.toJson(compare);
            }
        } catch (Exception e) {
            log.error("Can't extract product information from the resource", e);
        }
    }
}