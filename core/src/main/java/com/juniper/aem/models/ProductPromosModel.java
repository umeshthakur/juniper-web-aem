package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Model(adaptables = {SlingHttpServletRequest.class, Resource.class, ValueMap.class},
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductPromosModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductPromosModel.class);

    @Inject @Via("valueMap")
    private String body;

    @PostConstruct
    protected void init() {
    }

}
