package com.juniper.aem.models.globalnav;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SubNavTierTwoItemModel {

    @Inject
    private String itemLabel;
    
    @Inject
    private String itemUrl;
    
    @SlingObject
    private ResourceResolver resourceResolver;

	@PostConstruct
	protected void init() {
		itemUrl = LinkUtil.addHTMLIfPage(resourceResolver, itemUrl);
	}
}