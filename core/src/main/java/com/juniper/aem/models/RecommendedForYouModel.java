package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Model(adaptables = {Resource.class}, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RecommendedForYouModel {
    private static final Logger LOG = LoggerFactory.getLogger(RecommendedForYouModel.class);

    @Inject @Named("jcr:title")
    private String title;

    @Inject @Named("jcr:description")
    private String description;

    @Inject@Named("image")
    private String imagePath;

	@PostConstruct
    protected void init() {
    }
}