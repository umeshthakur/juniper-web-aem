package com.juniper.aem.models.globalnav;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class V2SubNavTierOneItemModel {

    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @SlingObject
    private Resource resource;

    @Inject
    private String title;

    @Inject
    private String descriptor;

    @Inject
    private String categoryLabel;

    @ChildResource
    private List<V2SubNavTierTwoItemModel> items;

    @PostConstruct
    protected void init() {
        ValueMap valueMap = resource.getParent().getValueMap();
        if (valueMap.get("title") != null) {
            title = valueMap.get("title").toString();
        }
        if (valueMap.get("descriptor") != null) {
            descriptor = valueMap.get("descriptor").toString();
        }
    }
}