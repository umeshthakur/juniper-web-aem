package com.juniper.aem.models;

import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.dam.cfm.ContentElement;
import com.adobe.cq.dam.cfm.ContentFragment;
import com.adobe.cq.dam.cfm.ContentVariation;
import com.juniper.aem.utils.LinkUtil;

import lombok.Setter;

@Setter

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SecondaryPromoModel {
	
	private static Logger log = LoggerFactory.getLogger(SecondaryPromoModel.class);
	
	@Inject @Named("fragmentPath")
    private String fragmentPath;

    @Inject @Named("variationName")
    private String variation;


    @Inject
    ResourceResolver resourceResolver;

    private Optional<ContentFragment> contentFragment;

    @PostConstruct
    public void init() {
        //NPE Check
        if (resourceResolver.getResource(fragmentPath) != null) {
            Resource fragmentResource = resourceResolver.getResource(fragmentPath);
            log.debug("cf path :: " + fragmentResource.getPath());
            contentFragment = Optional.ofNullable(fragmentResource.adaptTo(ContentFragment.class));
        }

    }
    
    public String getLeftButtonLink() {
        String link = "";
        
        if(isVariant()) {
            link = contentFragment
                .map(cv -> cv.getElement("left-button-link").getVariation(variation))
                .map(ContentVariation::getContent)
                .orElse(StringUtils.EMPTY);
        }
        else {
            link = contentFragment
                .map(cf -> cf.getElement("left-button-link"))
                .map(ContentElement::getContent)
                .orElse(StringUtils.EMPTY);

        }       
        
        return LinkUtil.addHTMLIfPage(resourceResolver, link);
    }
    
    public String getRightButtonLink() {
        String link = "";
        
        if(isVariant()) {
            link = contentFragment
                .map(cv -> cv.getElement("right-button-link").getVariation(variation))
                .map(ContentVariation::getContent)
                .orElse(StringUtils.EMPTY);
        }
        else {
            link = contentFragment
                .map(cf -> cf.getElement("right-button-link"))
                .map(ContentElement::getContent)
                .orElse(StringUtils.EMPTY);

        }       
        
        return LinkUtil.addHTMLIfPage(resourceResolver, link);
    }
    
    protected boolean isVariant() {
        return (variation != null && !variation.isEmpty() && !"master".equalsIgnoreCase(variation));
    }


}
