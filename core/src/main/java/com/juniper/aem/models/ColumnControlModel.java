package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;

import lombok.Getter;
import lombok.Setter;

/**
 * Helper for setting grid layout classes on columns
 * 
 * @author Sridhar	
 *
 */
@Getter
@Setter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ColumnControlModel {

	@Inject
	private String columnOpts;

	private String firstColumnClass;
	private String secondColumnClass;
	private String thirdColumnClass;
	private Boolean hasThirdColumn;
	private Boolean hasFourthColumn;

	@PostConstruct
	protected void init() {

		firstColumnClass = "";
		secondColumnClass = "";
		thirdColumnClass = "";
		
		switch (columnOpts) {
		case "50-50":
			firstColumnClass = "col-12 col-md-6";
			secondColumnClass = "col-12 col-md-6";
			hasThirdColumn = false;
			hasFourthColumn = false;
			break;
		case "33-33-33":
			firstColumnClass = "col-12 col-md-4";
			secondColumnClass = "col-12 col-md-4";
			thirdColumnClass = "col-12 col-md-4";
			hasThirdColumn = true;
			hasFourthColumn = false;
			break;
		case "33-66":
			firstColumnClass = "col-12 col-md-4";
			secondColumnClass = "col-12 col-md-8";
			hasThirdColumn = false;
			hasFourthColumn = false;
			break;
		case "66-33":
			firstColumnClass = "col-12 col-md-8";
			secondColumnClass = "col-12 col-md-4";
			hasThirdColumn = false;
			hasFourthColumn = false;
			break;
		case "25-25-25-25":
			firstColumnClass = "col-12 col-md-3";
			secondColumnClass = "col-12 col-md-3";
			thirdColumnClass = "col-12 col-md-3";
			hasThirdColumn = true;
			hasFourthColumn = true;
			break;
		}
		
	}
}
