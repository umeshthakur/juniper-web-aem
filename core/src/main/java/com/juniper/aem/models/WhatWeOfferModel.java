package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class WhatWeOfferModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String eyebrow;
    
    @Inject
    private String title;
    
    @Inject
    private String themeColor;
    
    @Inject
    private List<SolutionsPanelModel>  solutions;

}