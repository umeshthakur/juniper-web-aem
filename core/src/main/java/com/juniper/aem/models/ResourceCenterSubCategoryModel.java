package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;




@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ResourceCenterSubCategoryModel {
	private static Logger log = LoggerFactory.getLogger(ResourceCenterSubCategoryModel.class);
	@Inject
	ResourceResolver resourceResolver;

	@Inject
	private String subHeading;

	@Inject
	private List<ResourceCenterContainerModels> documents;

	public String getSubHeading() {
		return subHeading;
	}

	public List<ResourceCenterContainerModels> getDocuments() {
		return documents;
	}

	@PostConstruct
	protected void init() {

	}


}
