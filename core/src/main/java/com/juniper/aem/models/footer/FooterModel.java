package com.juniper.aem.models.footer;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FooterModel {

    @ChildResource
    private List<LinksModel> socialLinks;

    @ChildResource
    private List<LinksModel> mandatoryLinks;

    @ChildResource
    private List<NavItemModel> nav1;

    @ChildResource
    private List<NavItemModel> nav2;

    @ChildResource
    private List<NavItemModel> nav3;

    @ValueMapValue
    private String copyright;

    @ValueMapValue
    private String followUsLabel;

    @ValueMapValue
    private String signupLabel;

    @ValueMapValue
    private String signupUrl;

    @ValueMapValue
    private String signupStyle;

    @ValueMapValue
    private String tosLabel;

    @ValueMapValue
    private String tosPagePath;

    @ValueMapValue
    private String updateLabel;

    @ValueMapValue
    private String logoPath;

    @ValueMapValue
    private String logoDestination;

    @ValueMapValue
    private String openInNewWindow;

    @SlingObject
    private ResourceResolver resourceResolver;

    @SlingObject
    private Resource resource;

    @PostConstruct
    protected final void init() {
        signupUrl = LinkUtil.addHTMLIfPage(resourceResolver, signupUrl);
        tosPagePath = LinkUtil.addHTMLIfPage(resourceResolver, tosPagePath);
        logoDestination = LinkUtil.addHTMLIfPage(resourceResolver, tosPagePath);
    }
}