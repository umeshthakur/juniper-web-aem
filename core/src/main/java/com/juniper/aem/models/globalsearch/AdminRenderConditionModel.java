package com.juniper.aem.models.globalsearch;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.adobe.granite.ui.components.rendercondition.RenderCondition;
import com.juniper.aem.helpers.GroupRenderCondition;
import com.juniper.aem.services.GlobalSearchService;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;


@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AdminRenderConditionModel {

    @Self
    private SlingHttpServletRequest request;
    @AemObject
    private ResourceResolver resourceResolver;
    @OSGiService
    private GlobalSearchService globalSearchService;

    @PostConstruct
    protected final void init() {
        String[] groups = globalSearchService.getAdministratorGroups();
        if (ArrayUtils.isNotEmpty(groups)) {
            List<String> groupsList = Arrays.asList(groups);
            Authorizable authorizable = resourceResolver.adaptTo(Authorizable.class);
            if (authorizable != null) {
                request.setAttribute(RenderCondition.class.getName(), new GroupRenderCondition(authorizable, groupsList));
            }
        }
    }
}
