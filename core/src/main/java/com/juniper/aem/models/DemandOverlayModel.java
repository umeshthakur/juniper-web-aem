package com.juniper.aem.models;

import java.util.List;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class DemandOverlayModel {
    @Inject
    private String title;

    @Inject
    private String backgroundColor;

    @Inject
    private String textColor;

    @ChildResource
    private List<DemandOverlayItemModel> assets;

}