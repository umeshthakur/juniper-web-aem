package com.juniper.aem.models.globalsearch;

import com.juniper.aem.services.CoveoService;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GlobalSearchModel {

    @OSGiService
    private CoveoService coveoService;

    // General
    @ValueMapValue
    private String resultsMessage;

    @ValueMapValue
    private String feedbackLink;

    @ValueMapValue
    private String feedbackLabel;

    @ValueMapValue
    private String loginLink;

    @ValueMapValue
    private String loginLabel;

    @ValueMapValue
    private String logoutLabel;

    @ValueMapValue
    private String welcomeMessage;

    // Filters
    @ValueMapValue
    private String filtersHeader;

    @ValueMapValue
    private String resetFiltersLabel;

    @ValueMapValue
    private String siteSectionFilterHeader;
    @ChildResource
    private List<FilterModel> siteSectionFilters;

    @ValueMapValue
    private String productFilterHeader;
    @ChildResource
    private List<FilterModel> productFilters;

    @ValueMapValue
    private String formatFilterHeader;
    @ChildResource
    private List<FilterModel> formatFilters;

    @ValueMapValue
    private String languageFilterHeader;
    @ChildResource
    private List<FilterModel> languageFilters;


    // Errors handling
    @ValueMapValue
    private String errorModalHeader;

    @ValueMapValue
    private String errorMessage;

    @ValueMapValue
    private String noResultsMessage;

    // Featured Results
    @ValueMapValue
    private String featuredResultsHeader;

    @ChildResource
    private List<FeaturedResultsModel> featuredResults;

    public List<FeaturedResultsModel> getFeaturedResults() {
        return featuredResults;
    }

    public int getPageSize() {
        return coveoService.getPageSize();
    }

    public String getPublicSearchKey() {
        return coveoService.getPublicSearchKey();
    }

    public String getCoveoSuggestEndpoint() {
        return coveoService.getCoveoSuggestEndpoint();
    }

}
