package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextFamilyModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String eyebrow;
    
    @Inject
    private String eyebrowImage;

    @Inject
    private String eyebrowDescriptor;

    @Inject
    private String headerTextTitle;

    @Inject
    private String description;

    @Inject
    private String ctaIntro;

    @Inject
    private String headerTextLink;

    @Inject
    private String headerTextLinkTarget;

    @Inject
    private String headerTextSubTitle;

    @Inject
    private String intro;

    @Inject
    private String fileReference;

    @Inject
    private String imageLocation;

    @Inject
    private String imageLink;

    @Inject
    private String imageLinkTarget;

    @Inject
    private String ctaText;

    @Inject
    private String ctaLink;

    @Inject
    private String ctaType;

    @Inject
    private String ctaStyle;

    @Inject
    private String ctaButtonColor;

    @Inject
    private String target;

    @Inject
    private String inverse;

    @Inject
    private String backgroundColor;

    @Inject
    private String showBackgroundSquare;

    @Inject
    private String showDecorativeCircle;

    @Inject
    private String decorativeCircleColor;

    @Inject
    private String altText;

    @Inject
    private String showBackgroundCircle;

    @Inject
    private String topMargin;

    @Inject
    private String bottomMargin;

    @Inject
    private String topPadding;

    @Inject
    private String bottomPadding;

    @ChildResource
    private List<ListItemModel> listItems;

    @ChildResource
    private List<ImageModel> images;

    @ChildResource
    private List<ListItemModel> twiblimages;

    @ChildResource
    private List<ListItemModel> stwoiprops;

    @ChildResource
    private List<ListItemModel> stawiprops;

    @ChildResource
    private List<ListItemModel> etListItems;
    
    @ChildResource
    private List<ListItemModel> etwcListItems;

    @ChildResource
    private List<ListItemModel> stfim1props;

    @ChildResource
    private List<ListItemModel> stfim2props;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {
        ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, ctaLink);
        headerTextLink = LinkUtil.addHTMLIfPage(resourceResolver, headerTextLink);
    }
}
