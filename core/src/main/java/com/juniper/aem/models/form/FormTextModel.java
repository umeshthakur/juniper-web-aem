package com.juniper.aem.models.form;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FormTextModel {

    @Inject
    private String layoutType;

    @ChildResource
    private FormTextFamilyModel header;

    @ChildResource
    private FormTextFamilyModel description;

    @ChildResource
    private FormTextFamilyModel thwbc;

    @ChildResource
    private FormTextFamilyModel formimage;

    @ChildResource
    private FormTextFamilyModel formvideo;
    
    @ChildResource
    private FormTextFamilyModel imagewithsquarebackground;
    
    @ChildResource
    private FormTextFamilyModel noimagewithcirclebackground;

    private String formTheme;

    @Self
    Resource resource;

    @PostConstruct
    protected void init() throws ItemNotFoundException, RepositoryException{ 

        Node node = resource.adaptTo(Node.class);
        if(node !=null && node.getParent().getParent().hasProperty("theme")) {
            formTheme = node.getParent().getParent().getProperty("theme").getString();
        }
    }
}
