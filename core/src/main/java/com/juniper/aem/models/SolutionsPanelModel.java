package com.juniper.aem.models;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SolutionsPanelModel {
    private static Logger log = LoggerFactory.getLogger(SolutionsPanelModel.class);

    @Inject
    private String title;

    @Inject
    private String description;

    @Inject
    private String solutionDescription;

    @Inject
    private String playIconColor;

    @Inject
    private String boldDescription;

    @Inject
    private List<CtaModel> ctaContainer;

    @Inject
    private String imagePath;

    @Inject
    private String imageAltText;

    @Inject
    private String videoPath;

    @Inject
    private String ctaText;

    @Inject
    private String ctaLink;

    @Inject
    private String target;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        if(this.ctaLink != null) {
            ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, this.ctaLink);
        }

//        if(this.imagePath != null) {
//            imagePath = LinkUtil.addWebRendition(resourceResolver, imagePath);
//        }
    }
}