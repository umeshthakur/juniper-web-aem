package com.juniper.aem.models;

import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class StatisticsModel {
    @Inject
    private String largeText;
    
    @Inject
    private String smallText;
    
    @Inject
    private String description;
    
    @Inject
    private String textcolor;
}