package com.juniper.aem.models.globalsearch;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FeaturedResultsModel {

    @ChildResource
    private List<KeywordModel> keywords;

    @ChildResource
    private List<FeaturedResultModel> results;

    @PostConstruct
    protected final void init() {
        if (keywords == null) {
            keywords = new ArrayList<>();
        }
        if (results == null) {
            results = new ArrayList<>();
        }
    }

    public List<KeywordModel> getKeywords() {
        return keywords;
    }

    public List<FeaturedResultModel> getResults() {
        return results;
    }
}
