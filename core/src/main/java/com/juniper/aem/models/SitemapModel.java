package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SitemapModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    
    @Inject
    private String title;
    
    @SlingObject
    private Resource resource;

    @SlingObject
    private PageManager pageManager;
    
    @ChildResource
    private List<SitemapPageModel> pagePathContainer;

    @SlingObject
    private ResourceResolver resourceResolver;
    
    private List<SitemapPageModel> level1PageList = new ArrayList<SitemapPageModel>();
    
    
    private Map<String, List<SitemapPageModel>> level2Map = new HashMap<String, List<SitemapPageModel>>();
    private Map<String, List<SitemapPageModel>> level3Map = new HashMap<String, List<SitemapPageModel>>();
    
    @PostConstruct
    protected void init() {
        PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        
        if(pagePathContainer != null && pagePathContainer.size() > 0) {
            
            for(SitemapPageModel x : pagePathContainer) {
                
                if(pageManager.getContainingPage(x.pagePath) != null) {
                    
                    Page temp = pageManager.getContainingPage(x.pagePath);
                    SitemapPageModel spm = temp.getContentResource().adaptTo(SitemapPageModel.class);            
                    level1PageList.add(spm);            
                    
                    //get level2 pages
                    Iterator<Page> level2Pages = temp.listChildren();
                    List<SitemapPageModel> genericList = new ArrayList<SitemapPageModel>();
                    
                    while (level2Pages.hasNext()) {
                        Page temp2 = (Page) level2Pages.next();
                        log.debug("############ Page properties level 2 :: " + temp2.getName() + " :: parent : " + temp.getName());
                        
                        if(!temp2.getProperties().containsKey("hideFromSearch")) {
                            SitemapPageModel spm1 = temp2.getContentResource().adaptTo(SitemapPageModel.class);            
                            genericList.add(spm1);
                        }
                        
                            //get level3 pages
                            Iterator<Page> level3Pages = temp2.listChildren();
                            List<SitemapPageModel> genericList2 = new ArrayList<SitemapPageModel>();
                            
                            while (level3Pages.hasNext()) {
                                Page temp3 = (Page) level3Pages.next();
                                log.debug("############ Page properties level 3 :: " + temp3.getName() + " :: parent : " + temp2.getName());
                                
                                if(!temp3.getProperties().containsKey("hideFromSearch")) {
                                    SitemapPageModel spm2 = temp3.getContentResource().adaptTo(SitemapPageModel.class);            
                                    genericList2.add(spm2);
                                }
                            }
                            
                            level3Map.put(temp2.getName()+"-"+temp.getName(), genericList2);
                    }                
                    level2Map.put(temp.getName(), genericList);                 
                }
                log.info("Root level pages in dialog :: " + x.pagePath);           
                
            }
    }
         
    }

}