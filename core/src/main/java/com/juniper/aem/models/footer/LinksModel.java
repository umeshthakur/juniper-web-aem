package com.juniper.aem.models.footer;

import com.juniper.aem.utils.LinkUtil;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

import javax.annotation.PostConstruct;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class LinksModel {

    @ValueMapValue
    private String linkType;

    @ValueMapValue
    private String linkLabel;

    @ValueMapValue
    private String linkUrl;

    @ValueMapValue
    private String openLinkInNewWindow;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected void init() {
        linkUrl = LinkUtil.addHTMLIfPage(resourceResolver, linkUrl);
    }
}