package com.juniper.aem.models;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.Utils;
import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)

public class ProductCardsModel {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Inject
    private String[] productTags;

    @Inject
    private String variant;

    @Inject
    private String cardType;

    @Inject
    private String enableFilters;

    @Inject
    private String enableCompare;

    @Inject
    private String theme;

    @Inject
    private String cardsTheme;

    @Inject
    private String tagsTheme;

    @Inject
    private String compareCtaLabel;

    @Inject
    private String compareCtaLink;

    @Inject
    private String compareCtaTarget;

    @Inject
    private ProductService productService;

    @SlingObject
    private ResourceResolver resourceResolver;
    
    @SlingObject
    private Resource resource;

    private List<ProductModel> productList;

    private List<PagePropertiesModel> pagePropertiesModelList;

    @Inject
    private QueryBuilder builder;

    @PostConstruct
    protected void init() {
        productList= new ArrayList<ProductModel>();
        pagePropertiesModelList= new ArrayList<>();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            TagManager tagManager = resourceResolver.adaptTo(TagManager.class);
            if (pageManager != null && tagManager != null) {
                String languageRoot = pageManager.getContainingPage(resource).getAbsoluteParent(3).getPath();
                Map<String, String> map = new HashMap<>();

                map.put("path", languageRoot);
                map.put("type", "cq:Page");
                map.put("1_property", "jcr:content/cq:tags");
                map.put("1_property.or", "true");

                for (int i = 0; i < this.productTags.length; i++) {
                    map.put("1_property." + i + 1 + "_value", productTags[i]);
                }
                map.put("2_property", "jcr:content/productDataPath");
                map.put("2_property.operation", "exists");
                map.put("orderby", "@jcr:content/cardRank");
                map.put("orderby.sort", "asc");
                map.put("p.limit", "-1");

                Query query = builder.createQuery(PredicateGroup.create(map), session);
                log.debug("query" + query.getPredicates().toString());
                SearchResult result = query.getResult();
                for (Hit hit : result.getHits()) {
                    String path = hit.getPath();
                    log.debug("path" + path);
                    // Create a result element
                    Page productPage = pageManager.getContainingPage(path);
                    if (productPage != null && productPage.getProperties().get("productDataPath") != null) {
                        String productPath = productPage.getProperties().get("productDataPath").toString();
                        ProductModel productModel = productService.getProduct(productPath);
                        if (productModel != null) {
                            if (productModel.getPrimaryDataSheet() != null) {
                                productModel.setPrimaryDataSheet(Utils.formatLink(productModel.getPrimaryDataSheet()));
                            }
                            productModel.setPagePath(Utils.formatLink(path));
                            Tag[] filterTags = tagManager.getTags(productPage.getContentResource());
                            String[] filterTagsString = new String[filterTags.length];
                            for (int i = 0; i < filterTags.length; i++) {
                                filterTagsString[i] = filterTags[i].getName();
                            }
                            productModel.setFilterTags(filterTagsString);
                            log.debug("productModel" + productModel.getTitle());
                            productList.add(productModel);
                        }

                    }

                    if (productPage != null) {
                        PagePropertiesModel propertiesModel = new PagePropertiesModel();
                        propertiesModel.setPageProperties(productPage.getProperties());
                        propertiesModel.setPagePath(Utils.formatLink(path));
                        pagePropertiesModelList.add(propertiesModel);
                    }

                }
            }
        } catch (Exception e) {
            log.error("Can't extract product information from the resource", e);

        }
    }
}