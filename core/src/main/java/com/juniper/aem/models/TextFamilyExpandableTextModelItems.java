package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TextFamilyExpandableTextModelItems {

    @Inject
    private String title;
    
    @Inject
    private String titleLink;

    @Inject
    private String titleLinkTarget;

    @Inject
    private String description;
    
    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {

        titleLink = LinkUtil.addHTMLIfPage(resourceResolver, titleLink);
    }
}