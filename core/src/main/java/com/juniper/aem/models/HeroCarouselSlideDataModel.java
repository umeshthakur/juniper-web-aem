package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.juniper.aem.utils.LinkUtil;
import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeroCarouselSlideDataModel {

    @Inject
    @Via("valueMap")
    private String eyebrow;

    @Inject
    @Via("valueMap")
    private String headline;

    @Inject
    @Via("valueMap")
    private boolean headlineLink;

    @Inject
    @Via("valueMap")
    private String description;

    @Inject
    @Via("valueMap")
    private String shortDescription;

    @Inject
    @Via("valueMap")
    private String ctaLabel;

    @Inject
    @Via("valueMap")
    private String ctaLink;

    @Inject
    @Via("valueMap")
    private String openInNewTab;

    @Inject
    @Via("valueMap")
    private String imagePath;

    @Inject
    @Via("valueMap")
    private String imageAltText;

    @Inject
    @Via("valueMap")
    private String videoUrl;

    @Inject
    @Via("valueMap")
    private String playIconColor;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {

        if(this.ctaLink != null) {
            ctaLink = LinkUtil.addHTMLIfPage(resourceResolver,ctaLink);
        }
//        if(this.imagePath != null) {
//            imagePath = LinkUtil.addWebRendition(resourceResolver, imagePath);
//        }
    }

}
