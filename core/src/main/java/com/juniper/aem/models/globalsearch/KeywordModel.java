package com.juniper.aem.models.globalsearch;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

@Model(adaptables = Resource.class)
public class KeywordModel {

    @ValueMapValue
    private String keyword;

    public String getKeyword() {
        return keyword;
    }
}
