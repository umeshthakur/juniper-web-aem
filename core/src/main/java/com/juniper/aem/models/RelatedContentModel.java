package com.juniper.aem.models;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class RelatedContentModel {

    @Inject
    private String title;
    
    @Inject
    private String titleLink;
    
    @Inject
    private String titleLinkTarget;

    @Inject
    private String description;

    @Inject
    private List<RelatedContentDataModel> relatedCards;
    
    @SlingObject
    private ResourceResolver resourceResolver;
    
    @PostConstruct
    protected void init() {
        
        titleLink = LinkUtil.addHTMLIfPage(resourceResolver,titleLink);
        
    }

}
