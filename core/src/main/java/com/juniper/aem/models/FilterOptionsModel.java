package com.juniper.aem.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterOptionsModel {
    private String tagTitle;

    private long count;

    private String tagId;
}