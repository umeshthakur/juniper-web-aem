package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = {SlingHttpServletRequest.class, Resource.class, ValueMap.class},
	defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductFeaturesModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductFeaturesModel.class);

    @Inject @Named("productSpecs")
    private String title;

    @Inject @Named("featureDescription")
    private String value;

    @PostConstruct
    protected void init() {

    }
}
