package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class GraphsModel {

    @Inject
    private String header;

    @Inject
    private String description;

    @ChildResource
    private List<Resource> graphs;

    @PostConstruct
    protected final void init() {
        if (graphs == null) {
            graphs = new ArrayList<>();
        }
    }
}
