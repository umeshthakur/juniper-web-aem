package com.juniper.aem.models;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.models.injectors.annotation.AemObject;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = { Resource.class }, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ProductComparisonModel {
    private static final Logger LOG = LoggerFactory.getLogger(ProductComparisonModel.class);

    @Inject
    @Via("valueMap")
    private String theme;

    @Inject
    @Via("valueMap")
    private String heading;

    @Inject
    @Via("valueMap")
    private String product1DataPath;

    @Inject
    @Via("valueMap")
    private String product2DataPath;
    
    @Inject
    @Via("valueMap")
    private String compareCtaLabel;

    @Inject
    @Via("valueMap")
    private String comparePagePath;

    @Inject
    @Via("valueMap")
    private String viewAllProductsText;

    @Inject
    @Via("valueMap")
    private String allProductsPagePath;

    @Inject
    @Via("valueMap")
    private String viewAllCtaLabel;

    @Inject
    @Via("valueMap")
    private String compareText;

    @Inject
    @Via("valueMap")
    private String viewAllTarget;

    @Inject
    @Via("valueMap")
    private String comparectaTarget;

    @Inject @Via("valueMap")
    private String technicalFeatures;

    @Self
    Resource resource;

    @SlingObject
    private ResourceResolver resourceResolver;

    @Inject
    private ProductService productService;
    
    @AemObject
    private PageManager pageManager;

    List<ProductModel> productModelList = new ArrayList<ProductModel>();

    @PostConstruct
    protected void init() {
        Page currentPage = pageManager.getContainingPage(resource);
        if (currentPage != null) {
            addToModelList(currentPage.getPath().toString());
        }        
        if (product1DataPath != null) {
            addToModelList(product1DataPath);
        }
        if (product2DataPath != null) {
            addToModelList(product2DataPath);
        }
        allProductsPagePath = LinkUtil.addHTMLIfPage(resourceResolver, allProductsPagePath);
        comparePagePath = LinkUtil.addHTMLIfPage(resourceResolver, comparePagePath);
    }
    protected void addToModelList(String pagePath) {
        if(pageManager.getPage(pagePath)!=null)
        {   
            Page productPage = pageManager.getPage(pagePath);
            ValueMap productPageProperties = productPage.getProperties();
            if (productPageProperties != null && productPageProperties.get("productDataPath") != null) {
                String productDataPath = productPageProperties.get("productDataPath").toString();
                if (productDataPath != null) {
                    ProductModel productModel = productService.getProduct(productDataPath);
                    if (productModel != null) {
                        productModelList.add(productModel);
                    }
                }   
            }
        }
    }
}