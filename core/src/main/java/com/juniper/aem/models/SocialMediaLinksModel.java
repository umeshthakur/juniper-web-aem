package com.juniper.aem.models;
import lombok.Getter;
import lombok.Setter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import javax.inject.Inject;

@Getter
@Setter
@Model(adaptables = Resource.class,
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SocialMediaLinksModel {
   

    @Inject
    private String altText;

    @Inject
    private String icon;

    @Inject
    private String linkUrl;
}
