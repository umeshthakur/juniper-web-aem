package com.juniper.aem.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;

import com.juniper.aem.utils.LinkUtil;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ContentHeaderModel {

    @Inject
    private String headerTextTitle;

    @Inject
    private String description;

    @Inject
    private String fileReference;

    @Inject
    private String ctaText;

    @Inject
    private String ctaLink;

    @Inject
    private String buttonColor;

    @Inject
    private String target;

    @Inject
    private String altText;
    
    @Inject
    private String layoutType;
    
    @Inject
    private String imageAltText;
    
    @Inject
    private String backgroundColor;
    
    @Inject
    private String imagePosition;

    @SlingObject
    private ResourceResolver resourceResolver;

    @PostConstruct
    protected final void init() {
        ctaLink = LinkUtil.addHTMLIfPage(resourceResolver, ctaLink);
    }
}
