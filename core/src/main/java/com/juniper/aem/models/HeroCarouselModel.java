package com.juniper.aem.models;

import lombok.Getter;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;

import javax.inject.Inject;
import java.util.List;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HeroCarouselModel {

    @Inject
    @Via("valueMap")
    private String autoRotation;

    @Inject
    @Via("valueMap")
    private String sliderTime;
    
    @Inject
    @Via("valueMap")
    private String themeColor;

    @Inject
    @Via("valueMap")
    private String controlsThemeColor;

    @Inject
    @Via("valueMap")
    private String controlsThemeColorline;

    @ChildResource
    private List<HeroCarouselSlideDataModel> slides;
    
    
}
