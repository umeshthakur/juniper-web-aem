package com.juniper.aem.models.globalnav;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.SlingObject;
import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

import lombok.Getter;

@Getter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NavItemModel {
	
    protected final Logger LOG = LoggerFactory.getLogger(this.getClass());

	@SlingObject
    private Resource resource;

    @ValueMapValue
    private String navLabel;
    
    @ValueMapValue
    private String product1PagePath;
    
    @ValueMapValue
    private String product2PagePath;
    
    @ChildResource
    private List<SubNavItemModel> subnav;
    
    private List<Page> recommendedPageList;
    
	@PostConstruct
	protected void init() {
		
		PageManager pm = resource.getResourceResolver().adaptTo(PageManager.class);
		
		Page page = pm.getPage(product1PagePath);
		if(page != null) {
			recommendedPageList.add(page);
		}
		
		page = null;
		page = pm.getPage("product2PagePath");
		if(page != null) {
			recommendedPageList.add(page);
		}
	}
}