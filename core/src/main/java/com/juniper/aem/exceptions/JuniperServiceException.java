package com.juniper.aem.exceptions;

public class JuniperServiceException extends Exception {

    private final Integer errorCode;

    public JuniperServiceException(String message, int errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}