package com.juniper.aem.pojos;

import java.util.Map;

import com.google.gson.annotations.Expose;

public class Specs {
    @Expose
    private Map<String, String> specs;

    public Map<String, String> getSpecs() {
        return specs;
    }

    public void setSpecs(Map<String, String> specs) {
        this.specs = specs;
    }

    public Specs(Map<String, String> specs) {
        this.specs = specs;
    }
}
