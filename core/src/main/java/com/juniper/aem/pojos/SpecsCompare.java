package com.juniper.aem.pojos;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpecsCompare {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("selectorLabel")
    @Expose
    private String selectorLabel;
    @SerializedName("addAllLabel")
    @Expose
    private String addAllLabel;
    @SerializedName("itemsLabel")
    @Expose
    private String itemsLabel;
    @SerializedName("ctaLabel")
    @Expose
    private String ctaLabel;
    @SerializedName("specCategories")
    @Expose
    private List<SpecCategory> specCategories = null;
    @SerializedName("items")
    @Expose
    private List<ProductItem> items = null;
    @SerializedName("l10n")
    @Expose
    private L10n l10n;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSelectorLabel() {
        return selectorLabel;
    }

    public void setSelectorLabel(String selectorLabel) {
        this.selectorLabel = selectorLabel;
    }

    public String getAddAllLabel() {
        return addAllLabel;
    }

    public void setAddAllLabel(String addAllLabel) {
        this.addAllLabel = addAllLabel;
    }

    public String getItemsLabel() {
        return itemsLabel;
    }

    public void setItemsLabel(String itemsLabel) {
        this.itemsLabel = itemsLabel;
    }

    public String getCtaLabel() {
        return ctaLabel;
    }

    public void setCtaLabel(String ctaLabel) {
        this.ctaLabel = ctaLabel;
    }

    public List<SpecCategory> getSpecCategories() {
        return specCategories;
    }

    public void setSpecCategories(List<SpecCategory> specCategories) {
        this.specCategories = specCategories;
    }

    public List<ProductItem> getItems() {
        return items;
    }

    public void setItems(List<ProductItem> items) {
        this.items = items;
    }

    public L10n getL10n() {
        return l10n;
    }

    public void setL10n(L10n l10n) {
        this.l10n = l10n;
    }

    public SpecsCompare(String title, String selectorLabel, String addAllLabel, String itemsLabel, String ctaLabel,
            List<SpecCategory> specCategories, List<ProductItem> items, L10n l10n) {
        this.title = title;
        this.selectorLabel = selectorLabel;
        this.addAllLabel = addAllLabel;
        this.itemsLabel = itemsLabel;
        this.ctaLabel = ctaLabel;
        this.specCategories = specCategories;
        this.items = items;
        this.l10n = l10n;
    }
}
