package com.juniper.aem.pojos;

public class SearchParameters {

    private final String query;
    private final String languageFilter;
    private final String formatFilter;
    private final String productFilter;
    private final String sectionFilter;
    private final Integer pageNumber;

    public SearchParameters(String query, Integer pageNumber) {
        this.query = query;
        this.languageFilter = "";
        this.formatFilter = "";
        this.productFilter = "";
        this.sectionFilter = "";
        if (pageNumber != null) {
            this.pageNumber = pageNumber;
        } else {
            this.pageNumber = 0;
        }
    }

    public SearchParameters(String query, String languageFilter, String formatFilter, String productFilter, String sectionFilter, Integer pageNumber) {
        this.query = query;
        this.languageFilter = languageFilter;
        this.formatFilter = formatFilter;
        this.productFilter = productFilter;
        this.sectionFilter = sectionFilter;
        if (pageNumber != null) {
            this.pageNumber = pageNumber;
        } else {
            this.pageNumber = 0;
        }
    }

    public String getQuery() {
        return query;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public String getFormatFilter() {
        return formatFilter;
    }

    public String getLanguageFilter() {
        return languageFilter;
    }

    public String getProductFilter() {
        return productFilter;
    }

    public String getSectionFilter() {
        return sectionFilter;
    }
}
