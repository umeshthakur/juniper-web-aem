package com.juniper.aem.pojos;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import static com.juniper.aem.utils.CoveoConstants.*;

public class CoveoResponse {

    @SerializedName(PROP_TOTAL_COUNT)
    private final String totalCount;

    @SerializedName(PROP_PAGES_COUNT)
    private final String pagesCount;

    @SerializedName(PROP_PAGE_NUMBER)
    private final String pageNumber;

    @SerializedName(PROP_RESULTS)
    private final List<CoveoResultItem> results;

    public CoveoResponse(String totalCount, String pagesCount, String pageNumber, List<CoveoResultItem> results) {
        this.totalCount = totalCount;
        this.pagesCount = pagesCount;
        this.pageNumber = pageNumber;
        this.results = results;
    }

    public CoveoResponse() {
        this.totalCount = "0";
        this.pagesCount = "0";
        this.pageNumber = "0";
        this.results = new ArrayList<>();
    }
}
