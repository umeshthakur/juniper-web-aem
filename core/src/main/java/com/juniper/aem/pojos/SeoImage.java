package com.juniper.aem.pojos;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SeoImage {

    private String imagePath;
    
    private String altText;

    public SeoImage(String imagePath, String altText) {
        this.imagePath = imagePath;
        this.altText = altText;
    }
}