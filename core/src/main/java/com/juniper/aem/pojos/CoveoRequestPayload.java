package com.juniper.aem.pojos;

import static com.juniper.aem.utils.CoveoConstants.PARAM_ADVANCED_QUERY;
import static com.juniper.aem.utils.CoveoConstants.PARAM_COMPARATOR;
import static com.juniper.aem.utils.CoveoConstants.PARAM_COMPARATOR_EXACT;
import static com.juniper.aem.utils.CoveoConstants.PARAM_FORMAT_FILTER;
import static com.juniper.aem.utils.CoveoConstants.PARAM_LANGUAGE_FILTER;
import static com.juniper.aem.utils.CoveoConstants.PARAM_PRODUCT_FILTER;
import static com.juniper.aem.utils.CoveoConstants.PARAM_QUERY;
import static com.juniper.aem.utils.CoveoConstants.PARAM_RESULTS_LIMIT;
import static com.juniper.aem.utils.CoveoConstants.PARAM_RESULTS_START;
import static com.juniper.aem.utils.CoveoConstants.PARAM_SITE_SECTION_FILTER;
import static com.juniper.aem.utils.CoveoConstants.PARAM_COVEO_PIPELINE;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.SerializedName;

public class CoveoRequestPayload {

    @SerializedName(PARAM_QUERY)
    private final String query;

    @SerializedName(PARAM_ADVANCED_QUERY)
    private final String advancedQuery;

    @SerializedName(PARAM_RESULTS_LIMIT)
    private final Integer resultsLimit;

    @SerializedName(PARAM_RESULTS_START)
    private final Integer firstResultIndex;
    
    @SerializedName(PARAM_COVEO_PIPELINE)
    private final String pipeline;


    public CoveoRequestPayload(SearchParameters searchParameters, Integer pageSize, String kbMappings, String userTypeKbMappings, String pipeline) {
        StringBuilder advancedQueryAux = new StringBuilder();
        
        ArrayList<String> kbMapArr = new ArrayList<String>();
        kbMapArr.addAll(Arrays.asList(kbMappings.split(",")));
        
        Map<String,String> kbMap = new ConcurrentHashMap<String,String>();
        for(String x : kbMapArr){
           kbMap.put(x.split(":")[0], x.split(":")[1]);
        }
        
        //Logged-in USER Type to KB Internal groups mapping
        ArrayList<String> kbGroupMapArr = new ArrayList<String>();
        kbGroupMapArr.addAll(Arrays.asList(userTypeKbMappings.split(",")));
             
        Map<String,String> kbGroupMap = new ConcurrentHashMap<String,String>();
        for(String x : kbGroupMapArr){
            kbGroupMap.put(x.split(":")[0], x.split(":")[1]);
        }
        
        this.query = searchParameters.getQuery();
        this.resultsLimit = pageSize;
        this.firstResultIndex = searchParameters.getPageNumber() * pageSize;
        this.pipeline = pipeline;
        if (StringUtils.isNotBlank(searchParameters.getLanguageFilter())) {
            advancedQueryAux
                    .append('(')
                        .append(PARAM_LANGUAGE_FILTER)
                        .append(PARAM_COMPARATOR_EXACT)
                        .append(searchParameters.getLanguageFilter())
                    .append(')');
        }
        String formatFilter = searchParameters.getFormatFilter();
        if (StringUtils.isNotBlank(formatFilter)) {
            advancedQueryAux
                    .append('(')
                        .append(PARAM_FORMAT_FILTER)
                        .append(PARAM_COMPARATOR_EXACT)
                        .append('(')
                            .append(formatFilter)
                        .append(')')
                    .append(')');
        }
        String productFilter = searchParameters.getProductFilter();
        if (StringUtils.isNotBlank(productFilter)) {
            advancedQueryAux.append('(').append(PARAM_PRODUCT_FILTER).append(PARAM_COMPARATOR)
                    .append('(');
            String[] productFilterSplit = productFilter.split(",");
            int length = productFilterSplit.length;
            for (int i = 0; i < length; i++) {
                advancedQueryAux.append('"').append(productFilterSplit[i].replace('_', ' ')).append('"');
                if (i+1 != length) {
                    advancedQueryAux.append(',');
                }
            }
            advancedQueryAux.append(')').append(')');
        }
        
        //Section filters
        String sectionFilter = searchParameters.getSectionFilter();
        String modSectionFilter = "";
        if(StringUtils.isNotBlank(sectionFilter)) {
            
            String patternString = ".*(-junose|-tn|-jsa|-kb|-tsb)";
            Pattern pattern = Pattern.compile(patternString);
            Matcher matcher = null;
            
            if(sectionFilter.contains(",")){
                ArrayList<String> tempArr = new ArrayList();
                tempArr.addAll(Arrays.asList(sectionFilter.split(",")));
                
                String sources = "";
                String kbTypes = "";
                String sections = "";
                
                for (int i = 0; i < tempArr.size(); ++i) {
                    matcher = pattern.matcher(tempArr.get(i));
                    if(matcher.matches()){                       
                        kbTypes += kbMap.get(tempArr.get(i).split("-")[1]) + ",";
                        if(!sources.contains(tempArr.get(i).split("-")[0])){
                            sources += tempArr.get(i).split("-")[0] + ",";                           
                        }
                    } else {
                        if(tempArr.get(i).toString().length() > 1) {
                            
                            if(tempArr.get(i).contains("section-")){
                                sections += tempArr.get(i).split("-")[1] + ",";
                            } else {
                                if(!sources.contains(tempArr.get(i))){
                                    sources += tempArr.get(i) + ",";                           
                                }
                            }  
                        } 
                    }
                }
                
                if(!sources.equalsIgnoreCase("")){
                    sources = sources.replaceAll(",$", "");                 
                    modSectionFilter += "(" + "@source==(" + sources + "))";
                } 
                
                if(!sections.equalsIgnoreCase("")){
                    if(!modSectionFilter.equalsIgnoreCase("")){
                        modSectionFilter += " OR (" + "@section==(" + sections.replaceAll(",$", "") + "))";
                    } else {
                        modSectionFilter += "(" + "@section==(" + sections.replaceAll(",$", "") + "))";
                    }
                }
                
                if(!kbTypes.equalsIgnoreCase("")){
                    
                    kbTypes = kbTypes.replaceAll(",$", "");
                    
                    if(kbTypes.contains(",")){
                        if(!modSectionFilter.equalsIgnoreCase("")){
                            modSectionFilter += " OR (" + "@kbtype==(" + kbTypes + "))";
                        } else {
                            modSectionFilter += "(" + "@kbtype==(" + kbTypes + "))";
                        }
                        
                    } else {
                        if(!modSectionFilter.equalsIgnoreCase("")){
                            modSectionFilter += " OR (" + "@kbtype==" + kbTypes + ")";
                        } else {
                            modSectionFilter += "(" + "@kbtype==" + kbTypes + ")";
                        }
                        
                    }                    
                }
                
            } else {
                
                matcher = pattern.matcher(sectionFilter);
                
                if(matcher.matches()){
                    modSectionFilter += "(" + "@kbtype==" + kbMap.get(sectionFilter.split("-")[1]) + ")";                           
                } else {
                    
                    if(sectionFilter.contains("section-")){
                        modSectionFilter += "(" + "@section==" + sectionFilter.split("-")[1] + ")";
                    } else {
                        modSectionFilter += "(" + "@source==" + sectionFilter + ")";
                    }
                    
                }
            }
            
            modSectionFilter = "(" + modSectionFilter + ")";
            
        }
        if (StringUtils.isNotBlank(modSectionFilter)) {
            advancedQueryAux.append(modSectionFilter);           
        }
        this.advancedQuery = advancedQueryAux.toString();
    }

}
