package com.juniper.aem.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class L10n {
    @SerializedName("back")
    @Expose
    private String back;

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }
}
