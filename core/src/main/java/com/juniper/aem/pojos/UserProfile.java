package com.juniper.aem.pojos;

import com.google.gson.annotations.SerializedName;
import org.apache.jackrabbit.api.security.user.User;

import javax.jcr.RepositoryException;

public class UserProfile {
    public static final String PROFILE_PATH = "profile/";
    public static final String PROP_EMAIL = PROFILE_PATH + "email";
    public static final String PROP_FIRST_NAME = PROFILE_PATH + "givenName";
    public static final String PROP_LAST_NAME = PROFILE_PATH + "familyName";

    @SerializedName("username")
    private String username;

    @SerializedName("email")
    private String email;

    @SerializedName("firstName")
    private String firstName;

    @SerializedName("lastName")
    private String lastName;

    public UserProfile(User user) throws RepositoryException {
        this.username = user.getID();
        if (user.hasProperty(PROP_EMAIL)) {
            this.email = user.getProperty(PROP_EMAIL)[0].toString();
        }
        if (user.hasProperty(PROP_FIRST_NAME)) {
            this.firstName = user.getProperty(PROP_FIRST_NAME)[0].getString();
        }
        if (user.hasProperty(PROP_LAST_NAME)) {
            this.lastName = user.getProperty(PROP_LAST_NAME)[0].getString();
        }
    }

}
