package com.juniper.aem.pojos;

import com.google.gson.annotations.SerializedName;

import static com.juniper.aem.utils.CoveoConstants.*;

public class CoveoResultItem {

    @SerializedName(PROP_RESULT_TITLE)
    private final String title;

    @SerializedName(value = "url", alternate = PROP_RESULT_URL)
    private final String url;

    @SerializedName(PROP_RESULT_EXCERPT)
    private final String excerpt;

    public CoveoResultItem(String title, String url, String excerpt) {
        this.title = title;
        this.url = url;
        this.excerpt = excerpt;
    }

}
