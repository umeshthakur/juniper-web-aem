package com.juniper.aem.services;

public interface FeedbackSurveyService {

    String getSurveyUrl();

    String getSurveyuserName();

    String getSurveypassword();

    String getSurveyCompanyName();

}
