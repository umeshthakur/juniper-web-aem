package com.juniper.aem.services;

public interface ImageNamesService {

    String[] getImagePropertyNames();

}