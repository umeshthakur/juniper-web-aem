package com.juniper.aem.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.Designate;

import com.juniper.aem.services.RecaptchaService;
import com.juniper.aem.services.config.RecaptchaConfig;

@Component(service = RecaptchaService.class, configurationPolicy = ConfigurationPolicy.OPTIONAL)
@Designate(ocd = RecaptchaConfig.class)
@ServiceDescription("Recaptcha Service")
@ServiceVendor("Juniper")
public final class RecaptchaServiceImpl implements RecaptchaService {

    @Activate
    private RecaptchaConfig recaptchaConfig;

    @Override
    public String getRecaptchaUrl() {
        return recaptchaConfig.url();
    }

    @Override
    public String getRecaptchaSecretKey() {
        return recaptchaConfig.secretKey();
    }
}