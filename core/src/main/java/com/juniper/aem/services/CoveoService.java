package com.juniper.aem.services;

import java.io.IOException;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.SlingHttpServletRequest;

import com.google.gson.JsonObject;
import com.juniper.aem.exceptions.JuniperServiceException;
import com.juniper.aem.pojos.CoveoResponse;
import com.juniper.aem.pojos.SearchParameters;

public interface CoveoService {

    /**
     * Provide the Coveo key for public search.
     *
     * @return : String representing the Coveo key.
     */
    String getPublicSearchKey();

    /**
     * Provide the page size.
     *
     * @return : integer representing the size of each results page.
     */
    int getPageSize();

    /**
     * Provide the Coveo Suggest API endpoint URL.
     *
     * @return : String representing the Coveo Suggest API URL.
     */
    String getCoveoSuggestEndpoint();
    
    /**
     * Provide KB mappings between Search page filters and Coveo KB Type.
     *
     * @return : String representing the KB Mappings.
     */
    String getKbMappings();
    
    /**
     * Provide Mappings for LoggedIn User's Type to Groups in KB portal
     *
     * @return : String representing KB Groups
     */
    String getUserTypeKbMappings();
    
    /**
     * Provide coveo search pipeline
     *
     * @return : String representing KB Groups
     */
    String getPipeline();

    /**
     * Return the results found for a given query using the Coveo Public Search.
     *
     * @param searchParameters : {@link SearchParameters} object representing the search parameters.
     * @return : {@link JsonObject} with the results.
     */
    CoveoResponse getPublicResults(SearchParameters searchParameters) throws JuniperServiceException;

    /**
     * Return the full response
     *
     * @param searchParameters : {@link SearchParameters} object representing the search parameters.
     * @return : {@link JsonObject} with the results.
     */
    JsonObject getPublicRawResults(SearchParameters searchParameters) throws JuniperServiceException;

    /**
     * Return the full response
     *
     * @param query : {@link String} representing the query entered by the user.
     * @param pageNumber : Page number for pagination purposes.
     * @return : {@link JsonObject} with the results.
     */
    JsonObject getPublicRawResults(String query, Integer pageNumber) throws JuniperServiceException;

    /**
     * Return the results found for a given query using the Coveo Secure Search.
     *
     * @param searchParameters : {@link SearchParameters} object representing the search parameters.
     * @return : {@link JsonObject} with the results.
     */
    CoveoResponse getSecureResults(SearchParameters searchParameters) throws JuniperServiceException;

    /**
     * Return the full response
     *
     * @param searchParameters : {@link SearchParameters} object representing the search parameters.
     * @return : {@link JsonObject} with the results.
     */
    JsonObject getSecureRawResults(SearchParameters searchParameters) throws JuniperServiceException;

    /**
     * Return the full response
     *
     * @param query : {@link String} representing the query entered by the user.
     * @param pageNumber : Page number for pagination purposes.
     * @return : {@link JsonObject} with the results.
     */
    JsonObject getSecureRawResults(String query, Integer pageNumber) throws JuniperServiceException;
    
    /**
     * 
     * @param user
     * Get coveo search token for logged in user using impersonation
     * @return token
     * @throws RepositoryException 
     */
    String getSecureToken(User user, SlingHttpServletRequest request);
}
