package com.juniper.aem.services.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.models.ProductModel;
import com.juniper.aem.services.ProductService;
import com.juniper.aem.services.ResourceResolverService;

import java.util.Iterator;

@Component
public class ProductServiceImpl implements ProductService {

    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Reference
    private ResourceResolverService resourceResolverService;
    
    @Activate
    protected void activate() {
        // Placeholder for possible future use
    }

    @Override
    public ProductModel getProduct(String productPath) {
    	ProductModel productModel = null;
    	ResourceResolver resourceResolver = null;
    	if (StringUtils.isNotBlank(productPath) && !StringUtils.endsWith(productPath, "/master")) {
    	    productPath = productPath.concat("/master");
        }
        try {
            resourceResolver = resourceResolverService.getResourceResolver("ProductServiceImpl");
            if (resourceResolver != null) {
            	Resource resource = resourceResolver.getResource(productPath);
                Resource productResource = null;
            	if(resource != null) {
            	    Resource rootResource = resource.getChild("jcr:content/root");
            	    if (rootResource != null && rootResource.hasChildren()) {
                        Iterator<Resource> childResources = rootResource.listChildren();
                        while (childResources.hasNext()) {
                            Resource childResource = childResources.next();
                            if (childResource.getResourceType().equalsIgnoreCase("juniper/components/productdataentry")) {
                                productResource = childResource;
                                break;
                            }
                        }
                    }
            	    if(productResource != null) {
                        productModel = productResource.adaptTo(ProductModel.class);
                    }
            	}
            }
        } catch (LoginException e) {
            log.error("Error in getting Resource Resolver in ProductServiceImpl Service class: ", e);
        } finally {
        	if(resourceResolver != null) {
        		resourceResolver.close();
        	}
        }
        return productModel;
    }
}
