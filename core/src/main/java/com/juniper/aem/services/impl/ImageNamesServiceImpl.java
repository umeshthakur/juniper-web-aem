package com.juniper.aem.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.services.ImageNamesConfiguration;
import com.juniper.aem.services.ImageNamesService;

@Component(service = ImageNamesService.class, configurationPolicy = ConfigurationPolicy.OPTIONAL)
@Designate(ocd = ImageNamesConfiguration.class)
@ServiceDescription("ImageNames Service")
@ServiceVendor("Juniper")
public final class ImageNamesServiceImpl implements ImageNamesService {
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Activate
    private ImageNamesConfiguration imageNamesConfiguration;
    
    public String[] getImagePropertyNames() {
        return imageNamesConfiguration.imagePropertyNames();
    }
}