package com.juniper.aem.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Juniper - Coveo Configuration",
        description = "Juniper - Configuration for the Coveo Service")
public @interface CoveoServiceConfig {

    @AttributeDefinition(name = "Public search key", description = "Value of the Coveo Public Search key")
    String publicSearchKey();

    @AttributeDefinition(name = "Secure search key", description = "Value of the Coveo Secure Search key")
    String secureSearchKey();

    @AttributeDefinition(name = "Coveo Suggest API endpoint", description = "URL to the Coveo Suggest API")
    String coveoSuggestEndpoint() default "https://platform.cloud.coveo.com/rest/search/v2/querySuggest";

    @AttributeDefinition(name = "Coveo Search API endpoint", description = "URL to the Coveo Search API")
    String coveoSearchEndpoint() default "https://platform.cloud.coveo.com/rest/search/v2";

    @AttributeDefinition(name = "Results page size", description = "Limit of results returned per request.")
    int pageSize() default 10;
    
    @AttributeDefinition(name = "KB Allowed Groups", description = "KB Allowed Groups")
    String kbAllowedGroups(); 
    
    @AttributeDefinition(name = "KB Mappings", description = "KB mappings between Search page filters and Coveo KB Type")
    String kbMappings(); // default "junose:KNOWLEDGE_ASSET,kb:KB,jsa:SECURITY_ADVISORIES,tsb:TECHNICAL_BULLETINS,tn:TECHNOTES";
    
    @AttributeDefinition(name = "Coveo search pipeline", description = "Coveo search pipeline")
    String pipeline() default "public";
        
    @AttributeDefinition(name = "Usertype KB Mappings", description = "Mappings for LoggedIn User's Type to Groups in KB portal")
    String userTypeKbMappings(); 
}
