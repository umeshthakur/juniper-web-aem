package com.juniper.aem.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.juniper.aem.services.ResourceResolverService;

@Component(
	immediate = true, 
	service = ResourceResolverService.class
)
public class ResourceResolverServiceImpl implements ResourceResolverService {

    private ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    void setResourceResolverFactory(ResourceResolverFactory resourceResolverFactory) {
    	this.resourceResolverFactory = resourceResolverFactory;
    }

	@Override
	public ResourceResolver getResourceResolver(String subserviceName) throws LoginException {
		Map<String, Object> resourceResolverParams = new HashMap<String, Object>();
		ResourceResolver resourceResolver;

		resourceResolverParams.put(ResourceResolverFactory.SUBSERVICE, subserviceName);
		resourceResolver = resourceResolverFactory.getServiceResourceResolver(resourceResolverParams);

		return resourceResolver;
	}

	@Override
	public void closeResourceResolver(ResourceResolver resourceResolver) {
		if (resourceResolver != null && resourceResolver.isLive()) {
			resourceResolver.close();
		}
	}

}
