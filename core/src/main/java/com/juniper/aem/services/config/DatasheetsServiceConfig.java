package com.juniper.aem.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Juniper - DataSheets Service Configuration",
        description = "Juniper - Configuration for the Datasheets Service")
public @interface DatasheetsServiceConfig {

    @AttributeDefinition(name = "Service API endpoint", description = "URL to the Datasheets Service API")
    String datasheetsEndpoint() default "http://10.78.36.49:443/api/datasheet";

    @AttributeDefinition(name = "AEM UAT URL", description = "Value of the AEM Publish Instance URL")
    String uatURL() default "https://aem-publish-uat.juniper.net";
}
