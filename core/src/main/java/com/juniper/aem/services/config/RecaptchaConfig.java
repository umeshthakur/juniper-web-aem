package com.juniper.aem.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.juniper.aem.utils.Constants;

@ObjectClassDefinition(name = "Juniper - Recaptcha Configuration", description = "Juniper - Configuration for Recaptcha")
public @interface RecaptchaConfig {

    @AttributeDefinition(name = "Recpatcha URL", description = "Google recaptcha url")
    String url() default Constants.RECAPTCHA_URL;

    @AttributeDefinition(name = "Recpatcha secret key", description = "Google recaptcha secret key")
    String secretKey() default Constants.RECAPTCHA_SECRET_KEY;
}