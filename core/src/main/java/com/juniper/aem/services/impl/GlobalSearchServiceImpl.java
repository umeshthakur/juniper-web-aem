package com.juniper.aem.services.impl;

import java.io.IOException;
import java.util.List;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.juniper.aem.exceptions.JuniperServiceException;
import com.juniper.aem.models.globalsearch.FeaturedResultsModel;
import com.juniper.aem.models.globalsearch.GlobalSearchModel;
import com.juniper.aem.models.globalsearch.KeywordModel;
import com.juniper.aem.pojos.CoveoResponse;
import com.juniper.aem.pojos.SearchParameters;
import com.juniper.aem.services.CoveoService;
import com.juniper.aem.services.GlobalSearchService;
import com.juniper.aem.services.config.GlobalSearchServiceConfig;

@Component(service = GlobalSearchService.class, immediate = true)
@Designate(ocd = GlobalSearchServiceConfig.class)
public class GlobalSearchServiceImpl implements GlobalSearchService {

    private static final Gson gson = new Gson();
    @Reference
    CoveoService coveoService;
    private GlobalSearchServiceConfig config;

    @Activate
    @Modified
    protected void update(GlobalSearchServiceConfig config) {
        this.config = config;
    }

    @Override
    public JsonObject getCoveoRawResponse(String query, Integer pageNumber, Boolean isSecure) throws JuniperServiceException {
        if (isSecure) {
            return coveoService.getSecureRawResults(query, pageNumber);
        } else {
            return coveoService.getPublicRawResults(query, pageNumber);
        }
    }

    @Override
    public JsonObject getResults(SearchParameters searchParameters, Boolean isSecure, Resource resource) throws JuniperServiceException {
        JsonObject resultsJson = new JsonObject();
        CoveoResponse coveoResponse = new CoveoResponse();
        JsonArray featuredResults = getFeaturedResults(searchParameters.getQuery(), resource);
        resultsJson.add("featuredResults", featuredResults);
        if (isSecure) {
            coveoResponse = coveoService.getSecureResults(searchParameters);
        } else {
            coveoResponse = coveoService.getPublicResults(searchParameters);
        }
        resultsJson.add("coveoResponse", gson.toJsonTree(coveoResponse));

        return resultsJson;
    }

    @Override
    public String[] getAdministratorGroups() {
        return config.administratorGroups();
    }

    private JsonArray getFeaturedResults(String query, Resource resource) {
        JsonArray featuredResults = new JsonArray();
        GlobalSearchModel model = resource.adaptTo(GlobalSearchModel.class);
        if (model != null) {
            Gson gson = new GsonBuilder().create();
            List<FeaturedResultsModel> configuredFeaturedResults = model.getFeaturedResults();
            for (FeaturedResultsModel featuredResult : configuredFeaturedResults) {
                for (KeywordModel keyword : featuredResult.getKeywords()) {
                    if (query.equalsIgnoreCase(keyword.getKeyword())) {
                        featuredResults = gson.toJsonTree(featuredResult.getResults()).getAsJsonArray();
                    }
                }
            }
        }
        return featuredResults;
    }
    
    @Override
    public String getSecureTokenForUser(User user, SlingHttpServletRequest request) {        
        String secureToken = coveoService.getSecureToken(user, request);
        return secureToken;        
    }
}
