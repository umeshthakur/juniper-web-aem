package com.juniper.aem.services;

import com.juniper.aem.models.CaseStudyModel;

public interface CaseStudyService {

	CaseStudyModel getCaseStudy(String caseStudyPath);
}
