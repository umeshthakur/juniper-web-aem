package com.juniper.aem.services;

public interface RecaptchaService {

    String getRecaptchaUrl();
    
    String getRecaptchaSecretKey();
}