package com.juniper.aem.services.impl;

import static com.juniper.aem.utils.CoveoConstants.ERROR_MSG_FORBIDDEN;
import static com.juniper.aem.utils.CoveoConstants.ERROR_MSG_UNAVAILABLE;
import static com.juniper.aem.utils.CoveoConstants.ERROR_MSG_UNKNOWN;
import static com.juniper.aem.utils.CoveoConstants.ERROR_MSG__MALFORMED_RESPONSE;
import static com.juniper.aem.utils.CoveoConstants.PROP_PAGES_COUNT;
import static com.juniper.aem.utils.CoveoConstants.PROP_PAGE_NUMBER;
import static com.juniper.aem.utils.CoveoConstants.PROP_TOTAL_COUNT;

import java.io.IOException;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.jcr.RepositoryException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.SlingHttpServletRequest;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpResponseException;
import com.google.api.client.http.HttpStatusCodes;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.v2.ApacheHttpTransport;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.juniper.aem.exceptions.JuniperServiceException;
import com.juniper.aem.pojos.CoveoRequestPayload;
import com.juniper.aem.pojos.CoveoResponse;
import com.juniper.aem.pojos.SearchParameters;
import com.juniper.aem.services.CoveoService;
import com.juniper.aem.services.config.CoveoServiceConfig;
import com.juniper.aem.utils.Constants;

@Component(service = CoveoService.class, immediate = true)
@Designate(ocd = CoveoServiceConfig.class)
public class CoveoServiceImpl implements CoveoService {

    private static final Gson gson = new Gson();
    private static final Logger log = LoggerFactory.getLogger(CoveoServiceImpl.class);

    private CoveoServiceConfig config;
    private static String secureToken = "";

    @Activate
    @Modified
    protected void update(CoveoServiceConfig config) {
        this.config = config;
    }

    @Override
    public String getPublicSearchKey() {
        return config.publicSearchKey();
    }

    @Override
    public int getPageSize() {
        return config.pageSize();
    }
    
    @Override
    public String getKbMappings() {
        return config.kbMappings();
    }
    
    @Override
    public String getUserTypeKbMappings() {
        return config.userTypeKbMappings();
    }
    
    @Override
    public String getPipeline() {
        return config.pipeline();
    }
    
    @Override
    public String getCoveoSuggestEndpoint() {
        return config.coveoSuggestEndpoint();
    }

    @Override
    public CoveoResponse getPublicResults(SearchParameters searchParameters) throws JuniperServiceException {
        JsonObject resultsJson = getPublicRawResults(searchParameters);
        return gson.fromJson(resultsJson, CoveoResponse.class);
    }

    @Override
    public JsonObject getPublicRawResults(String query, Integer pageNumber) throws JuniperServiceException {
        SearchParameters searchParameters = new SearchParameters(query, pageNumber);
        return getPublicRawResults(searchParameters);
    }

    @Override
    public JsonObject getPublicRawResults(SearchParameters searchParameters) throws JuniperServiceException {
        return getRawResults(searchParameters, false);
    }

    @Override
    public JsonObject getSecureRawResults(String query,Integer pageNumber) throws JuniperServiceException {
        SearchParameters searchParameters = new SearchParameters(query, pageNumber);
        return getSecureRawResults(searchParameters);
    }

    @Override
    public CoveoResponse getSecureResults(SearchParameters searchParameters) throws JuniperServiceException {
        JsonObject resultsJson = getSecureRawResults(searchParameters);
        return gson.fromJson(resultsJson, CoveoResponse.class);
    }

    @Override
    public JsonObject getSecureRawResults(SearchParameters searchParameters) throws JuniperServiceException {
        return getRawResults(searchParameters, true);
    }

    private JsonObject getRawResults(SearchParameters searchParameters, Boolean isSecure) throws JuniperServiceException {
        try {
            CoveoRequestPayload coveoRequestPayload = new CoveoRequestPayload(searchParameters, config.pageSize(), config.kbMappings(), config.userTypeKbMappings(), config.pipeline());
            Integer firstResultIndex = searchParameters.getPageNumber() * config.pageSize();
            HttpTransport httpTransport = new ApacheHttpTransport();
            HttpRequestFactory requestFactory = httpTransport.createRequestFactory();
            GenericUrl endpointUrl = new GenericUrl(config.coveoSearchEndpoint());
            HttpContent httpContent = getHttpContent(coveoRequestPayload);
            HttpRequest request = requestFactory.buildPostRequest(endpointUrl, httpContent);
            request.getHeaders().setContentType(Constants.MEDIA_TYPE_APPLICATION_JSON);
            if (isSecure) {
                //request.getHeaders().setAuthorization("Bearer " + config.secureSearchKey());
                log.info("secure token in coveo search payload : {}", this.secureToken);
                request.getHeaders().setAuthorization("Bearer " + this.secureToken);
            } else {
                log.debug("public token in coveo search payload : {}", config.publicSearchKey());
                request.getHeaders().setAuthorization("Bearer " + config.publicSearchKey());
            }
            if (log.isDebugEnabled()) {
                log.debug("Executing POST call to {}. Authorization: {}", endpointUrl, request.getHeaders().getAuthorization());
                log.debug("Payload: {}.", gson.toJson(coveoRequestPayload));
                log.debug("Is secure search?: {}.", isSecure);
                log.debug("filters : {}" , searchParameters.getSectionFilter());
            }
            HttpResponse httpResponse = request.execute();
            JsonObject jsonResponse = handleHttpResponse(httpResponse);
            addExtraProperties(jsonResponse, searchParameters.getPageNumber());
            return jsonResponse;
        } catch (HttpResponseException httpException) {
            log.error("Error {} while executing POST call to {}", httpException.getStatusCode(),
                    config.coveoSearchEndpoint());
            log.error("HTTP request returned an error code.", httpException);
            throw new JuniperServiceException(httpException.getStatusMessage(), httpException.getStatusCode());
        } catch (IOException ioException) {
            log.error(ERROR_MSG_UNAVAILABLE, ioException);
            throw new JuniperServiceException(ERROR_MSG_UNAVAILABLE, HttpStatusCodes.STATUS_CODE_SERVER_ERROR);
        }
    }

    private void addExtraProperties(JsonObject jsonObject, int pageNumber) {
        if (jsonObject != null) {
            jsonObject.addProperty(PROP_PAGE_NUMBER, pageNumber);
            int pagesCount = 0;
            if (jsonObject.has(PROP_TOTAL_COUNT)) {
                int totalCount = jsonObject.get(PROP_TOTAL_COUNT).getAsInt();
                pagesCount = totalCount / config.pageSize();
                if (totalCount % config.pageSize() != 0) {
                    pagesCount++; // Plus one due to rounding.
                }
            }
            jsonObject.addProperty(PROP_PAGES_COUNT, pagesCount);
        }
    }

    private HttpContent getHttpContent(CoveoRequestPayload payload) throws JuniperServiceException {
        HttpContent httpContent;
        try {
            httpContent = new ByteArrayContent(Constants.MEDIA_TYPE_APPLICATION_JSON,
                    gson.toJson(payload).getBytes(Constants.CHARSET_UTF_8));
            return httpContent;
        } catch (UnsupportedEncodingException unsupportedException) {
            log.error("Error while creating the request payload.", unsupportedException);
            throw new JuniperServiceException(unsupportedException.getMessage(),
                    HttpStatusCodes.STATUS_CODE_SERVER_ERROR);
        }
    }

    private JsonObject handleHttpResponse(HttpResponse httpResponse) throws JuniperServiceException {
        JsonObject jsonResponse = new JsonObject();
        switch (httpResponse.getStatusCode()) {
            case (HttpStatusCodes.STATUS_CODE_OK):
                try {
                    String responseContent = httpResponse.parseAsString();
                    if (StringUtils.isEmpty(responseContent)) {
                        return jsonResponse;
                    }
                    if (log.isDebugEnabled()) {
                        //log.debug("Response: {}", responseContent);
                    }
                    jsonResponse = gson.fromJson(responseContent, JsonObject.class);
                    break;
                } catch (JsonSyntaxException jsonExc) {
                    log.error(ERROR_MSG__MALFORMED_RESPONSE, jsonExc);
                    throw new JuniperServiceException(ERROR_MSG_UNKNOWN, HttpStatusCodes.STATUS_CODE_SERVER_ERROR);
                } catch (IOException e) {
                    log.error(ERROR_MSG_UNKNOWN, e);
                    throw new JuniperServiceException(ERROR_MSG_UNKNOWN, HttpStatusCodes.STATUS_CODE_SERVER_ERROR);
                }
            case (HttpStatusCodes.STATUS_CODE_NOT_FOUND):
                log.error(ERROR_MSG_UNAVAILABLE, httpResponse.getStatusMessage());
                throw new JuniperServiceException(ERROR_MSG_UNAVAILABLE, HttpStatusCodes.STATUS_CODE_NOT_FOUND);
            case (HttpStatusCodes.STATUS_CODE_FORBIDDEN):
                log.error(ERROR_MSG_FORBIDDEN, httpResponse.getStatusMessage());
                throw new JuniperServiceException(ERROR_MSG_FORBIDDEN, HttpStatusCodes.STATUS_CODE_FORBIDDEN);
            default:
                log.error(ERROR_MSG_UNKNOWN, httpResponse.getStatusMessage());
                throw new JuniperServiceException(ERROR_MSG_UNKNOWN, HttpStatusCodes.STATUS_CODE_SERVER_ERROR);
        }
        return jsonResponse;
    }
    
    @Override
    public String getSecureToken(User user, SlingHttpServletRequest request) {        
        
        String userToken = "";
        
        try {            
            log.info("Loggedin user id: {} | email : {}  | user type : {}" , user.getID(), user.getProperty("profile/email"), user.getProperty("profile/jobTitle"));
            
            if(request.getSession().getAttribute("coveoSecureToken") == null || StringUtils.isBlank(request.getSession().getAttribute("coveoSecureToken").toString())) {
                
                String userEmail = user.getProperty("profile/email")[0].getString();
                String userType = user.getProperty("profile/jobTitle")[0].getString();
                
                if(StringUtils.isNotBlank(userEmail)) {
                    String genTokenURL = config.coveoSearchEndpoint() + "/token?access_token=" + config.secureSearchKey();
                    
                    //Logged-in USER Type to KB Internal groups mapping
                    ArrayList<String> kbGroupMapArr = new ArrayList();
                    kbGroupMapArr.addAll(Arrays.asList(config.userTypeKbMappings().split(",")));
                         
                    Map<String,String> kbGroupMap = new ConcurrentHashMap<String,String>();
                    for(String x : kbGroupMapArr){
                        kbGroupMap.put(x.split(":")[0], x.split(":")[1]);
                    }
                    
                    String kbGroupsOfUser = "";
                    
                    if(StringUtils.isNotBlank(kbGroupMap.get(userType.toLowerCase()))){
                        kbGroupsOfUser = kbGroupMap.get(userType.toLowerCase());
                    }
                    
                    ArrayList<String> tokenGroups = new ArrayList<String>();      
                    if(kbGroupsOfUser.contains(";")){
                        tokenGroups.addAll(Arrays.asList(kbGroupsOfUser.split(";")));
                    }
                    else {
                        tokenGroups.add(kbGroupsOfUser);
                    }
                    
                    String jsonString = "";
                    if(tokenGroups.size() > 0){
                        for(String x : tokenGroups){
                            if(!x.equalsIgnoreCase("")){
                                jsonString +=  "{\"name\": \"" + x.toLowerCase().trim()+"@"+x.toLowerCase().trim()+".com" 
                                  + "\",\"provider\":\"Email Security Provider\"}" + ",";       
                            }
                            
                        }               
                    }
                    jsonString += "{\"name\": \"" + userEmail.trim() + "\",\"provider\":\"Email Security Provider\"}";
                    
                    jsonString = "{\"userIds\": [" + jsonString + "]}";
                    log.info("json payload - " + jsonString);               
                    
                    DefaultHttpClient httpclient = new DefaultHttpClient();        
                    HttpPost httpPost = new HttpPost(genTokenURL);
                    httpPost.setHeader("Content-Type", "application/json");
                    StringEntity jsonEntity = new StringEntity(jsonString);
                    httpPost.setEntity(jsonEntity);           
                    
                    CloseableHttpResponse resp = null;
                    HttpEntity entity = null;
                    resp = httpclient.execute(httpPost);
                    
                    String code = resp.getStatusLine().toString();
                    log.info("status line for token url : " + resp.getStatusLine());
                    entity = resp.getEntity();
                    
                    if (null != resp && !"".equals(resp) && !code.matches("(.*)500(.*)")) {
                        
                         StringWriter writer = new StringWriter();
                         IOUtils.copy(entity.getContent(), writer);              
                         
                         ObjectMapper mapper = new ObjectMapper();
                         String tkResponse = writer.getBuffer().toString();
                         log.debug("token url response - " + tkResponse);
                         
                         JsonNode root = mapper.readTree(tkResponse);
                         JsonNode tokenNode = root.path("token");
                         userToken = tokenNode.textValue();
                         
                         
                         //Set secure token in SESSION
                         if(StringUtils.isNotBlank(userToken)) {
                             request.getSession().setAttribute("coveoSecureToken", userToken);
                         }
    
                    }                
                }
            } else {
                userToken = request.getSession().getAttribute("coveoSecureToken").toString();
                log.info("User secure token found in session : {} " , request.getSession().getAttribute("coveoSecureToken"));
            } 
        
        } catch(Exception e) {
            userToken = config.publicSearchKey();
            log.error("Could not get secure token for user, switching to public key");
            log.error(Constants.ERROR_MSG_UNKNOWN, e);
        }
        
        secureToken = userToken;       
        return userToken;
        
    }

}
