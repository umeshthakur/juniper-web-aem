package com.juniper.aem.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.juniper.aem.utils.Constants;

@ObjectClassDefinition(name = "Juniper - InMoment Survey Configuration", description = "Juniper - InMoment Configuration for Feedback Survey")
public @interface FeedbackSurveyConfig {

    @AttributeDefinition(name = "InMoment URL", description = "InMoment API url")
    String url() default Constants.INMOMENT_API_URL;

    @AttributeDefinition(name = "InMoment USERNAME", description = "InMoment USERNAME")
    String username() default Constants.INMOMENT_USER_NAME;

    @AttributeDefinition(name = "InMoment COMPANYNAME", description = "InMoment COMPANYNAME")
    String companyname() default Constants.INMOMENT_COMPANY_NAME;

    @AttributeDefinition(name = "InMoment PASSWORD", description = "InMoment PASSWORD")
    String secretKey() default Constants.INMOMENT_PASSWORD;
}
