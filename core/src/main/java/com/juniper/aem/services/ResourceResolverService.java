package com.juniper.aem.services;


import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;

/**
 * ResourceResolver service interface to manage the ResourceResolver object with
 * the AEM Service Users.
 */
public interface ResourceResolverService {

	/**
	 * Return the proper ResourceResolver object based on the sub service name.
	 * 
	 * @param subserviceName
	 *            Sub serviceName
	 * 
	 * @return ResourceResolver object
	 * 
	 * @throws LoginException
	 *             in case of error
	 */
	ResourceResolver getResourceResolver(String subserviceName) throws LoginException;

	/**
	 * Close a given resource resolver.
	 * 
	 * @param resourceResolver
	 *            ResourceResolver object to close
	 */
	void closeResourceResolver(ResourceResolver resourceResolver);

}
