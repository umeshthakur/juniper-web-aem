package com.juniper.aem.services.impl;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.juniper.aem.services.DatasheetsService;
import com.juniper.aem.services.config.DatasheetsServiceConfig;


@Component(service = DatasheetsService.class, immediate = true)
@Designate(ocd = DatasheetsServiceConfig.class)
public class DatasheetsServiceImpl implements DatasheetsService {

   
    private static final Logger log = LoggerFactory.getLogger(DatasheetsServiceImpl.class);

    private DatasheetsServiceConfig config;

    @Activate
    @Modified
    protected void update(DatasheetsServiceConfig config) {
        this.config = config;
    }

    @Override
    public String getDatasheetsEndpoint() {
        return config.datasheetsEndpoint();
    }

    @Override
    public String getUatURL() {
        return config.uatURL();
    }


}
