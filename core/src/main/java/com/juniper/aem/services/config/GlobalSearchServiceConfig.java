package com.juniper.aem.services.config;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@ObjectClassDefinition(name = "Juniper - Global Search Configuration",
        description = "Juniper - Configuration for the Global Search Service")
public @interface GlobalSearchServiceConfig {

    @AttributeDefinition(name = "Search administrators groups", description = "List of groups that are allowed to modify search logic. e.g.: Filters")
    String[] administratorGroups() default {"administrators"};

}
