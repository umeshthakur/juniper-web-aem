package com.juniper.aem.services.impl;

import com.juniper.aem.services.FeedbackSurveyService;
import com.juniper.aem.services.config.FeedbackSurveyConfig;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.propertytypes.ServiceDescription;
import org.osgi.service.component.propertytypes.ServiceVendor;
import org.osgi.service.metatype.annotations.Designate;


@Component(service = FeedbackSurveyService.class, configurationPolicy = ConfigurationPolicy.OPTIONAL)
@Designate(ocd = FeedbackSurveyConfig.class)
@ServiceDescription("Juniper InMoment Feedback Service")
@ServiceVendor("Juniper")

public final class FeedbackSurveyServiceImpl implements FeedbackSurveyService {
    @Activate
    private FeedbackSurveyConfig feedbackSurveyConfig;

    @Override
    public String getSurveyUrl() {
        return feedbackSurveyConfig.url();
    }

    @Override
    public String getSurveyuserName() {
        return feedbackSurveyConfig.username();
    }

    @Override
    public String getSurveypassword() {
        return feedbackSurveyConfig.secretKey();
    }

    @Override
    public String getSurveyCompanyName() {
        return feedbackSurveyConfig.companyname();
    }


}
