package com.juniper.aem.services.impl;

import java.util.Optional;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.dam.cfm.ContentFragment;
import com.juniper.aem.models.CaseStudyModel;
import com.juniper.aem.services.CaseStudyService;
import com.juniper.aem.services.ResourceResolverService;

@Component
public class CaseStudyServiceImpl implements CaseStudyService {

    private static final Logger log = LoggerFactory.getLogger(CaseStudyServiceImpl.class);

    @Reference
    private ResourceResolverService resourceResolverService;
    
    @Override
    public CaseStudyModel getCaseStudy(String caseStudyPath) {
    	CaseStudyModel caseStudyModel = null;
    	ResourceResolver resourceResolver = null;
        try {
            resourceResolver = resourceResolverService.getResourceResolver("ProductServiceImpl");
            if (resourceResolver != null) {
            	Resource resource = resourceResolver.getResource(caseStudyPath);
            	if(resource != null) {
            		Optional<ContentFragment> contentFragment = Optional.ofNullable(resource.adaptTo(ContentFragment.class));
            	       
            		caseStudyModel= new CaseStudyModel();
            		caseStudyModel.setResourceResolver(resourceResolver);
            		caseStudyModel.setFragmentPath(caseStudyPath);
            		caseStudyModel.setContentFragment(contentFragment);
            	}
            }
        } catch (LoginException e) {
            log.error("Error in getting Resource Resolver in CaseStudyServiceImpl Service class: ", e);
        } 
        return caseStudyModel;
    }
}
