package com.juniper.aem.services;

import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

import com.juniper.aem.utils.Constants;

/**
 * ImageNames OSGI configuration.
 */
@ObjectClassDefinition(name = "Juniper ImageNames Configuration", description = "Configuration to add Image property names.")
public @interface ImageNamesConfiguration {

    @AttributeDefinition(name = "Image Property Name", description = "Image Property Name",
            defaultValue = {Constants.CARD_IMAGE_PATH, Constants.IMAGE_PATH, Constants.IMAGE, Constants.FILE_REFERENCE, 
                    Constants.VIDEO_THUMBNAIL_IMAGE_PATH})
    String[] imagePropertyNames() default {Constants.CARD_IMAGE_PATH, Constants.IMAGE_PATH, Constants.IMAGE, Constants.FILE_REFERENCE, 
        Constants.VIDEO_THUMBNAIL_IMAGE_PATH};
}