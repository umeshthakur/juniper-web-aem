package com.juniper.aem.services;

import org.osgi.annotation.versioning.ProviderType;

import javax.activation.DataSource;
import javax.mail.internet.InternetAddress;
import java.util.List;
import java.util.Map;


@ProviderType
public interface EmailService {


    List<InternetAddress> sendEmail(String templatePath, Map<String, String> emailParams,
                                    InternetAddress... recipients);


    List<String> sendEmail(String templatePath, Map<String, String> emailParams, String... recipients);


    List<InternetAddress> sendEmail(String templatePath, Map<String, String> emailParams, Map<String, DataSource> attachments, InternetAddress... recipients);


    List<String> sendEmail(String templatePath, Map<String, String> emailParams, Map<String, DataSource> attachments, String... recipients);
}