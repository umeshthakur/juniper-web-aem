package com.juniper.aem.services;

import com.juniper.aem.models.ProductModel;

public interface ProductService {

	ProductModel getProduct(String productPath);

}
