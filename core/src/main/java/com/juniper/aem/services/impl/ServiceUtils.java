package com.juniper.aem.services.impl;

import com.day.cq.commons.jcr.JcrConstants;
import org.apache.sling.api.resource.*;

import java.util.HashMap;
import java.util.Map;

public class ServiceUtils {

	protected static Resource getOrCreateResource(String resourceName, Resource parentResource, ResourceResolver resourceResolver) throws PersistenceException {
		Resource resource = null;
		if (resourceResolver != null && parentResource != null) {
			resource = resourceResolver.getResource(parentResource.getPath() + "/" + resourceName);
			if (resource == null) {
				Map<String, Object> props = new HashMap<>();
				props.put(JcrConstants.JCR_PRIMARYTYPE, "oak:Unstructured");
				resource = resourceResolver.create(parentResource, resourceName, props);
			}
		}
		return resource;
	}

	protected static ResourceResolver getResourceResolver(ResourceResolverFactory resourceResolverFactory, String className) throws LoginException {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ResourceResolverFactory.SUBSERVICE, className);
		ResourceResolver resourceResolver = null;
		if (resourceResolverFactory != null) {
			resourceResolver = resourceResolverFactory.getServiceResourceResolver(param);
		}
		return resourceResolver;
	}

}
