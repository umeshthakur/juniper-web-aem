package com.juniper.aem.services;

import com.google.gson.JsonObject;
import com.juniper.aem.exceptions.JuniperServiceException;
import com.juniper.aem.pojos.SearchParameters;

import java.io.IOException;

import javax.jcr.RepositoryException;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

public interface GlobalSearchService {

    /**
     * Return a {@link JsonObject} with the entire response provided by Coveo.
     * This is meant to be used by external systems.
     *
     * @param query : {@link String} representing the query entered by the user.
     * @param pageNumber : Page number where to begin the results.
     * @param isSecure : Flag to indicate whether it should be a protected search or not.
     * @return : {@link JsonObject} with the response.
     * @throws JuniperServiceException : Internal exception thrown when there was an issue calling Coveo API.
     */
    JsonObject getCoveoRawResponse(String query, Integer pageNumber, Boolean isSecure) throws JuniperServiceException;

    /**
     * Return the Coveo results found for a given query plus the Featured Results set by the authors.
     *
     * @param searchParameters : {@link SearchParameters} Pojo object containing search parameters.
     * @param isSecure : Flag to indicate whether it should be a protected search or not.
     * @param resource         : {@link Resource} representing the Global Search component.
     * @return : {@link JsonObject} with the results.
     * @throws JuniperServiceException : Internal exception thrown when there was an issue calling Coveo API.
     */
    JsonObject getResults(SearchParameters searchParameters, Boolean isSecure, Resource resource) throws JuniperServiceException;

    String[] getAdministratorGroups();
    
    String getSecureTokenForUser(User user, SlingHttpServletRequest request);
}
