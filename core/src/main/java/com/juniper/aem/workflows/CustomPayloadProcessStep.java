package com.juniper.aem.workflows;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.apache.v2.ApacheHttpTransport;
import com.juniper.aem.services.DatasheetsService;
import com.juniper.aem.utils.Constants;
import com.juniper.aem.utils.WorkflowUtil;

@Component(service = WorkflowProcess.class, property = { "process.label=Custom Payload Process Step" })

public class CustomPayloadProcessStep implements WorkflowProcess {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    DatasheetsService dsService;

    public void execute(WorkItem item, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {

        try {
            Session session = workflowSession.adaptTo(Session.class);
            ResourceResolver resourceResolver = null;
            try {
                resourceResolver = WorkflowUtil.getResourceResolver(resourceResolverFactory, session);
               
            } catch (Exception e1) {
                log.error("LoginError", e1);
            }
            String payload=item.getWorkflowData().getPayload().toString(); 
            String payloadbits[]=payload.split("/");
            String datasheetName = payloadbits[payloadbits.length-1]+".pdf";
            String path = payload + "/jcr:content";
            Node node = session.getNode(path);
            String datasheetPath="/content/dam/juniper";
            try {
                if (node.hasProperty("datasheetPath")) {
                    log.debug("Node val" + node.getPath() + " datasheet Path:" + node.getProperty("datasheetPath").getValue());
                    datasheetPath = node.getProperty("datasheetPath").getValue().toString();
                }

            } catch (Exception e) {
                log.error("Error in reading datasheetPath", e);
            }
            datasheetPath=datasheetPath+"/"+datasheetName;
            HttpTransport httpTransport = new ApacheHttpTransport();
            HttpRequestFactory requestFactory = httpTransport.createRequestFactory();
            String payLoadURL=dsService.getUatURL()+payload+".xml";
            GenericUrl serviceURL = new GenericUrl(dsService.getDatasheetsEndpoint()+"?pageURL="+payLoadURL);
            HttpContent httpContent= new ByteArrayContent(Constants.MEDIA_TYPE_APPLICATION_XML,payLoadURL.getBytes());
            HttpRequest request=requestFactory.buildPostRequest(serviceURL,httpContent);
            request.getHeaders().setContentType(Constants.MEDIA_TYPE_APPLICATION_XML);
            HttpResponse httpResponse = request.execute();
            com.day.cq.dam.api.AssetManager assetMgr = resourceResolver.adaptTo(com.day.cq.dam.api.AssetManager.class);
            
            assetMgr.createAsset(datasheetPath, httpResponse.getContent(),"application/pdf", true);
            PageManager pageManager=resourceResolver.adaptTo(PageManager.class);
            Node datasheetNode = session.getNode(datasheetPath);
            if(datasheetNode !=null)
            { 
                Node metadata=datasheetNode.getNode("jcr:content/metadata");
                String title =node.hasProperty("pageTitle") ? node.getProperty("pageTitle").getString() : "";
                metadata.setProperty("dc:title", title);
                String description =node.hasProperty("jcr:description") ? node.getProperty("jcr:description").getString() : "";
                metadata.setProperty("dc:description", description);
               
                final List<String> values = new ArrayList<>();
                if(node.hasProperty("cq:tags")) {  
                    for (Value value : node.getProperty("cq:tags").getValues()) {
                        values.add(value.getString());
                    }
                }
                if(pageManager !=null)
                {
                    Page page=pageManager.getContainingPage(payload);
                    Page homepage = page.getAbsoluteParent(3);
                        if(homepage.getProperties().get("jcr:language") !=null)
                        {
                            String language=homepage.getProperties().get("jcr:language").toString();
                            metadata.setProperty("dc:language", language);
                        }
                }
                metadata.setProperty("cq:tags", values.toArray(new String[0]));
                metadata.setProperty("pdf:Title", title);
               
            }

        }

        catch (Exception e) {
            log.error("Exception ",e);
        }
    }

}