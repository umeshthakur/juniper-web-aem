package com.juniper.aem.workflows;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.jcr.Session;

import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.adobe.granite.workflow.model.WorkflowTransition;
import com.day.cq.commons.Externalizer;
import com.day.cq.replication.Replicator;
import com.juniper.aem.services.EmailService;
import com.juniper.aem.services.ResourceResolverService;
import com.juniper.aem.utils.LinkUtil;
import com.juniper.aem.utils.WorkflowUtil;

 
@Component(service = WorkflowProcess.class, property = { "process.label=Replication Agent" })

public class CustomProcessStep implements WorkflowProcess {
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    ResourceResolverService resourceResolver;
    
    @Reference
    private Replicator replicator;
    
    private EmailService emailService;
    

    @Reference
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public void execute(WorkItem item, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {

        try {
            
            Session session = workflowSession.adaptTo(Session.class);
            ResourceResolver resourceResolver = WorkflowUtil.getResourceResolver(resourceResolverFactory,session);
            //log.debug("workflow ID"+item.getWorkflow().getWorkflowModel().getTransitions().);
            item.getNode().getTitle();
            Iterator<WorkflowTransition> it = item.getNode().getIncomingTransitions().iterator();
            WorkflowTransition workflowTransition=null;
            // Print the first item
            while(it.hasNext())
            { 
                workflowTransition= it.next();
               
            log.debug("From ID:"+ workflowTransition.getFrom().getId()+":"+workflowTransition.getFrom().getTitle());
            log.debug("To ID:"+ workflowTransition.getTo().getId()+":"+workflowTransition.getTo().getTitle());
            }
            String environment=args.get("PAYLOAD_URL",String.class);
            String emailSuccessTemplatePath=args.get("EMAIL_SUCCESS_TEMPLATE",String.class);
            String emailFailureTemplatePath=args.get("EMAIL_FAILURE_TEMPLATE",String.class);
            String subject_success=args.get("SUBJECT_SUCCESS",String.class);
            String subject_failure=args.get("SUBJECT_FAILURE",String.class);
            Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
            String workflowinitiator=item.getWorkflow().getInitiator();
            String payload=item.getWorkflowData().getPayload().toString();
            payload=LinkUtil.addHTMLIfPage(resourceResolver, payload);
            if(environment.equalsIgnoreCase("author"))
            {   
                payload=externalizer.externalLink(resourceResolver, environment.toLowerCase(), "/editor.html"+payload);
            }
            else
            {
                payload=externalizer.externalLink(resourceResolver, environment.toLowerCase(), payload);
            }
            
            String hostPrefix=externalizer.externalLink(resourceResolver, "author", "");
            String replicationAgentName = args.get("REPLICATION_AGENT_NAME",String.class);
            log.debug("replicationAgentName:"+replicationAgentName);
            
            WorkflowData workflowData = item.getWorkflowData(); 
            String path = workflowData.getPayload().toString();
            UserManager manager = resourceResolver.adaptTo(UserManager.class);
            User sender=(User)manager.getAuthorizable(workflowinitiator);
            String senderEmailAddress=WorkflowUtil.getEmail(sender);
            final Map<String, String> emailParams = new HashMap<String, String>();
            emailParams.put("senderEmailAddress", senderEmailAddress);
            emailParams.put("senderName", workflowinitiator);
            emailParams.put("payload",payload);
            emailParams.put("hostPrefix",hostPrefix);
            
            final String[] recipients=new String[]{senderEmailAddress};
            log.debug("recipients length:"+recipients.length+" recipients:"+recipients.toString());
           
            if(WorkflowUtil.activateContent(replicator,path, workflowSession, replicationAgentName))
            {
                emailParams.put("subject", subject_success);
                emailService.sendEmail(emailSuccessTemplatePath, emailParams, recipients);
            }
            else
            {
                emailParams.put("subject", subject_failure);
                emailService.sendEmail(emailFailureTemplatePath, emailParams, recipients);
               // workflowTransition.setTo(workflowTransition.getFrom());
            }
            
        }

        catch (Exception e) {
           log.error("Exception ",e);
        }
    }


}