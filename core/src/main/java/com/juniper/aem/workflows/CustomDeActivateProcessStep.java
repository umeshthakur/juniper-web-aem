package com.juniper.aem.workflows;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.replication.Replicator;
import com.juniper.aem.services.ResourceResolverService;
import com.juniper.aem.utils.WorkflowUtil;


@Component(service = WorkflowProcess.class, property = { "process.label=Custom DeActivatePage" })

public class CustomDeActivateProcessStep implements WorkflowProcess {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    ResourceResolverService resourceResolver;

    @Reference
    private Replicator replicator;


    public void execute(WorkItem item, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {

        try {
            String replicationAgentName = (String)args.get("PROCESS_ARGS", String.class);
            String path = item.getWorkflowData().getPayload().toString();
            if(replicationAgentName.contains(","))
            {
                String[] agentNames=replicationAgentName.split(",");
                for(String agent: agentNames)
                {
                    WorkflowUtil.deActivateContent(replicator,path, workflowSession, agent);
                }
            }
            else{
                WorkflowUtil.deActivateContent(replicator,path, workflowSession, replicationAgentName);
            }
        }
        catch (Exception e) {
            log.error("Exception ",e);
        }
    }


}