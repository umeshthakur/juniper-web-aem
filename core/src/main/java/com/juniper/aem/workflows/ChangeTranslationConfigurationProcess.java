package com.juniper.aem.workflows;



import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.collection.ResourceCollection;
import com.day.cq.workflow.collection.ResourceCollectionManager;
import com.day.cq.workflow.collection.ResourceCollectionUtil;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.resource.api.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component(service = WorkflowProcess.class, property = { Constants.SERVICE_DESCRIPTION + "=Replaces Translation configuration property with specified value",
        Constants.SERVICE_VENDOR + "=Juniper Networks",
        "process.label=Change Translation Configuration Property" })
public class ChangeTranslationConfigurationProcess implements WorkflowProcess {
    private static final Logger log = LoggerFactory.getLogger(ChangeTranslationConfigurationProcess.class);
    private static final String CQ_CONF = "cq:conf";

    private static final String NN_VLT_DEFINITION = "vlt:definition";
    private static final String WORKFLOW_PAGE_RESOURCE_TYPE = "cq/workflow/components/collection/page";
    private static final String[] DEFAULT_WF_PACKAGE_TYPES = {"cq:Page"};
    private String[] workflowPackageTypes = DEFAULT_WF_PACKAGE_TYPES;



    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Reference
    private ResourceCollectionManager resourceCollectionManager;

    @Override
    public final void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap metaDataMap) throws WorkflowException {
        String wfPayload = null;

        try ( ResourceResolver resourceResolver = this.getResourceResolver(workflowSession.getSession()) ){
            final String translationConfiguration = metaDataMap.get("PROCESS_ARGS", "/conf/juniper");

            log.debug("process args===" + translationConfiguration);
            wfPayload = (String) workItem.getWorkflowData().getPayload();
            final List<String> payloads = getPaths(resourceResolver, wfPayload, workflowPackageTypes);

            for (final String payload : payloads) {

                final String path = resourceResolver.getResource(payload).getPath();

                log.debug("paths==" + path);

                String metadataPath = String.format("%s/%s",path, JcrConstants.JCR_CONTENT);
                Resource metadataResource = resourceResolver.getResource(metadataPath);

                if (metadataResource == null) {
                    String msg = String.format("Could not find the jcr:content node for Resource [ %s ]", path);
                    log.error(msg);
                    throw new WorkflowException(msg);

                }

                final ModifiableValueMap mvm = metadataResource.adaptTo(ModifiableValueMap.class);
                log.debug("CQ:CONF===" + mvm.get(CQ_CONF));
                mvm.put(CQ_CONF, translationConfiguration);
            }
        } catch (LoginException e) {
            log.error("Login Exception====");
            throw new WorkflowException("Could not get a ResourceResolver object from the WorkflowSession", e);
        } catch (RepositoryException e) {
            log.error("Repository Exception======");
            throw new WorkflowException(String.format("Could not find the payload for '%s'", wfPayload), e);
        }
    }

    private final List<String> getPaths(final ResourceResolver resourceResolver,
                                        final String path, final String[] nodeTypes) throws RepositoryException {
        final List<String> collectionPaths = new ArrayList<String>();

        final Resource resource = resourceResolver.getResource(path);

        if (resource == null) {
            log.warn("Requesting paths for a non-existent Resource [ {} ]; returning empty results.", path);
            return Collections.emptyList();

        } else if (!isWorkflowPackage(resourceResolver, path)) {
            log.debug("Requesting paths for a non-Resource Collection  [ {} ]; returning provided path.", path);
            return Arrays.asList(new String[]{ path });

        } else {
            final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            final Page page = pageManager.getContainingPage(path);

            if (page != null && page.getContentResource() != null) {
                final Node node = page.getContentResource().adaptTo(Node.class);

                final ResourceCollection resourceCollection = ResourceCollectionUtil.getResourceCollection(node, resourceCollectionManager);

                if (resourceCollection != null) {
                    final List<Node> members = resourceCollection.list(nodeTypes);
                    for (final Node member : members) {
                        collectionPaths.add(member.getPath());
                    }
                    return collectionPaths;
                }
            }

            return Arrays.asList(new String[]{ path });
        }
    }

    private final boolean isWorkflowPackage(final ResourceResolver resourceResolver, final String path) {
        final PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
        final Page workflowPackagesPage = pageManager.getPage(path);
        if (workflowPackagesPage == null) {
            return false;
        }

        final Resource contentResource = workflowPackagesPage.getContentResource();
        if (contentResource == null) {
            return false;
        }

        if (!contentResource.isResourceType(WORKFLOW_PAGE_RESOURCE_TYPE)) {
            return false;
        }

        if (contentResource.getChild(NN_VLT_DEFINITION) == null) {
            return false;
        }

        return true;
    }



    private ResourceResolver getResourceResolver(Session session) throws LoginException {
        final Map<String, Object> authInfo = new HashMap<String, Object>();
        authInfo.put(JcrResourceConstants.AUTHENTICATION_INFO_SESSION, session);
        return resourceResolverFactory.getResourceResolver(authInfo);
    }
}