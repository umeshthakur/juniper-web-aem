package com.juniper.aem.workflows;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.juniper.aem.services.EmailService;
import com.juniper.aem.utils.LinkUtil;
import com.juniper.aem.utils.WorkflowUtil;

@Component(service = WorkflowProcess.class, property = { "process.label=Custom Email Notification" })
public class EmailNotificationProcessStep implements WorkflowProcess {

    private static final String TYPE_JCR_PATH = "JCR_PATH";

    private static final Logger log = LoggerFactory.getLogger(EmailNotificationProcessStep.class);

    private EmailService emailService;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public void execute(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {

        String emailTemplatePath = "/etc/workflow/notification/email/en.html";
        ResourceResolver resourceResolver = null;
        String senderEmailAddress = "";
        String recipientEmailAddress = "";
        String subject = args.get("SUBJECT", String.class);
        String environment = args.get("PAYLOAD_URL", String.class);
        if (emailTemplatePath != null) {
            emailTemplatePath = args.get("EMAIL_TEMPLATE", String.class);
        }
        Session session = workflowSession.adaptTo(Session.class);
        try {
            resourceResolver = WorkflowUtil.getResourceResolver(resourceResolverFactory,session);
        } catch (LoginException e1) {
            log.error("LoginException:"+e1);
        }
        Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
        String hostPrefix=externalizer.externalLink(resourceResolver, "author", "");

        WorkflowData workflowData = workItem.getWorkflowData();
        if (workflowData.getPayloadType().equals(TYPE_JCR_PATH)) {
            String workflowinitiator = workItem.getWorkflow().getInitiator();
            String payload = workItem.getWorkflowData().getPayload().toString();
            String path = payload + "/jcr:content";
            payload=LinkUtil.addHTMLIfPage(resourceResolver, payload);
            if (environment.equalsIgnoreCase("author")) {
                payload=externalizer.externalLink(resourceResolver, environment.toLowerCase(), "/editor.html"+payload);
            } else {
                payload=externalizer.externalLink(resourceResolver, environment.toLowerCase(), payload);
            }
            try {

                UserManager manager = resourceResolver.adaptTo(UserManager.class);

                Authorizable authorizable = manager.getAuthorizable(workflowinitiator);

                Value[] email = authorizable.getProperty("./profile/email");
                try {
                    senderEmailAddress = email[0].toString();
                } catch (Exception e) {
                    log.error("Error in reading Email", e);
                }
                if (senderEmailAddress.isEmpty()) {
                    senderEmailAddress = workflowinitiator + "@juniper.net";
                }
                Node node = session.getNode(path);
                try {
                    if (node.getProperty("toAddress") != null) {
                        log.debug(
                                "Node val" + node.getPath() + " toAddress:" + node.getProperty("toAddress").getValue());

                        recipientEmailAddress = node.getProperty("toAddress").getValue().toString();
                    }
                } catch (Exception e) {
                    log.error("Error in reading recipient Email", e);
                }
                final Map<String, String> emailParams = new HashMap<String, String>();

                emailParams.put("senderEmailAddress", senderEmailAddress);
                emailParams.put("senderName", workflowinitiator);
                emailParams.put("subject", subject);
                emailParams.put("payload", payload);
                emailParams.put("hostPrefix", hostPrefix);
                final String[] recipients = recipientEmailAddress.split(",");
                if (recipients.length > 0) {
                    List<String> failureList= emailService.sendEmail(emailTemplatePath, emailParams, recipients);
                    if(failureList.size() > 0)
                    {
                        log.debug("Email delivery failure");
                        emailParams.put("subject", "Email Delivery Failure");
                        emailParams.put("content", "Email not deliverd to "+String.join(",", failureList));
                        emailService.sendEmail("/etc/workflow/notification/email/email-failure.html", emailParams, senderEmailAddress);
                    }
                } else {
                    log.debug("Email not found");
                }

            } catch (Exception e) {
               
                log.error("Error:", e);
            }
        }
    }

   
}
