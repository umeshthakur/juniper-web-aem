package com.juniper.aem.workflows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.jackrabbit.api.security.user.Authorizable;
import org.apache.jackrabbit.api.security.user.Group;
import org.apache.jackrabbit.api.security.user.User;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.commons.Externalizer;
import com.juniper.aem.services.EmailService;
import com.juniper.aem.utils.LinkUtil;
import com.juniper.aem.utils.WorkflowUtil;

@Component(service = ParticipantStepChooser.class, property = { "chooser.label=Custom Dynamic Participant Chooser" })
public class CustomDynamicParticipantStep implements ParticipantStepChooser {

    private static final String TYPE_JCR_PATH = "JCR_PATH";

    private static final Logger log = LoggerFactory.getLogger(CustomDynamicParticipantStep.class);

    private EmailService emailService;

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Reference
    public void setEmailService(EmailService emailService) {
        this.emailService = emailService;
    }

    public String getParticipant(WorkItem workItem, WorkflowSession workflowSession, MetaDataMap args){
        String emailTemplatePath = "";
        Session session = workflowSession.adaptTo(Session.class);
        ResourceResolver resourceResolver = null;
        try {
            resourceResolver = WorkflowUtil.getResourceResolver(resourceResolverFactory, session);
        } catch (LoginException e1) {
            log.error("LoginError", e1);
        }
        List<String> toAddress = new ArrayList<String>();
        String participant = args.get("PARTICIPANT", String.class);
        emailTemplatePath = args.get("EMAIL_TEMPLATE", String.class);
        String subject = args.get("SUBJECT", String.class);
        String payloadLink = args.get("PAYLOAD_LINK", String.class);
        String senderEmailAddress = "";
        String workflowinitiator = workItem.getWorkflow().getInitiator();
        if (participant == null) {
            participant = workflowinitiator;
        }
        if (emailTemplatePath == null) {
            emailTemplatePath = "/etc/workflow/notification/email/en.html";
        }
        String payload = workItem.getWorkflowData().getPayload().toString();
        payload = LinkUtil.addHTMLIfPage(resourceResolver, payload);
        Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
        if (payloadLink.equalsIgnoreCase("author")) {
            payload = externalizer.externalLink(resourceResolver, payloadLink.toLowerCase(), "/editor.html" + payload);
        } else {
            payload = externalizer.externalLink(resourceResolver, payloadLink.toLowerCase(), payload);
        }
        String hostPrefix = externalizer.externalLink(resourceResolver, "author", "");
        if (workItem.getWorkflowData().getPayloadType().equals(TYPE_JCR_PATH)) {

            UserManager manager = resourceResolver.adaptTo(UserManager.class);

            try {
                User sender = (User) manager.getAuthorizable(workflowinitiator);
                senderEmailAddress = WorkflowUtil.getEmail(sender);
                if (manager.getAuthorizable(participant).isGroup()) {
                    Group group = (Group) manager.getAuthorizable(participant);
                    for (Iterator<Authorizable> members = group.getMembers(); members.hasNext();) {
                        User member = (User) members.next();
                        String email = WorkflowUtil.getEmail(member);
                        toAddress.add(email);
                    }

                } else {
                    User user = (User) manager.getAuthorizable(participant);
                    String email = WorkflowUtil.getEmail(user);
                    toAddress.add(email);
                }
                final Map<String, String> emailParams = new HashMap<String, String>();
                emailParams.put("senderEmailAddress", senderEmailAddress);
                emailParams.put("senderName", workflowinitiator);
                emailParams.put("subject", subject);
                emailParams.put("payload", payload);
                emailParams.put("hostPrefix", hostPrefix);
                final String[] recipients = toAddress.toArray(new String[toAddress.size()]);
                if (recipients.length > 0) {
                    emailService.sendEmail(emailTemplatePath, emailParams, recipients);
                } else {
                    log.debug("Email not found");
                }
            } catch (RepositoryException e) {
                log.error("RepositoryException:", e);
            }
        }
        return participant;
    }

}
