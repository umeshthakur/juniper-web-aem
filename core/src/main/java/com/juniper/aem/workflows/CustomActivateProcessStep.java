package com.juniper.aem.workflows;
import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.jackrabbit.JcrConstants;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.commons.util.AssetReferenceSearch;
import com.day.cq.replication.Replicator;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.workflow.process.CreateVersionProcess;
import com.juniper.aem.services.ResourceResolverService;
import com.juniper.aem.utils.WorkflowUtil;

 
@Component(service = WorkflowProcess.class, property = { "process.label=Custom ActivatePage" })

public class CustomActivateProcessStep extends CreateVersionProcess implements WorkflowProcess {
    
    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceResolverFactory resourceResolverFactory;
    
    @Reference
    ResourceResolverService resourceResolver;
    
    @Reference
    private Replicator replicator;
 

    public void execute(WorkItem item, WorkflowSession workflowSession, MetaDataMap args) throws WorkflowException {

        try {
            String replicationAgentName = (String)args.get("PROCESS_ARGS", String.class);
            String path = item.getWorkflowData().getPayload().toString();
            //Activate the page
            doActivation(replicationAgentName, path, workflowSession);
            
            //Search and Activate Referenced files in page
            Session session = workflowSession.adaptTo(Session.class);
            ResourceResolver resourceResolver = WorkflowUtil.getResourceResolver(resourceResolverFactory,session);
            
            log.info("payload file :: " + path);
            Resource pageResource = resourceResolver.getResource(path);
            Page page = (pageResource != null && !ResourceUtil.isNonExistingResource(pageResource)) ? pageResource.adaptTo(Page.class) : null;

            if (page != null) {
                log.info("Assets references to be retrieved for page {} ", path );
                
                Node pageNode = session.getNode(path+"/"+JcrConstants.JCR_CONTENT);
                AssetReferenceSearch referenceSearch = new AssetReferenceSearch(pageNode,DamConstants.MOUNTPOINT_ASSETS,resourceResolver);

                if(referenceSearch.search().size() > 0) {
                        log.info(" {} assets references on page {} ", referenceSearch.search().size(), path );
                        for (String key: referenceSearch.search().keySet()) {
                            Asset asset = referenceSearch.search().get(key);                            
                            log.info("Activating Referenced asset :: " + asset.getPath());
                            doActivation(replicationAgentName, asset.getPath(), workflowSession);
                        }
                }
                
            }
        }
        catch (Exception e) {
           log.error("Exception ",e);
        }
    }
    
    private void doActivation(String replicationAgentName, String path, WorkflowSession workflowSession) {
        
        if(replicationAgentName.contains(","))
        {
            String[] agentNames=replicationAgentName.split(",");
            for(String agent: agentNames)
            {
                WorkflowUtil.activateContent(replicator,path, workflowSession, agent);
            }
        }
        else{
            WorkflowUtil.activateContent(replicator,path, workflowSession, replicationAgentName);
        }    
        
    }

}