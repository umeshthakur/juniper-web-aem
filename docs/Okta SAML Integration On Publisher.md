# Okta SAML Integration On Publisher

## Introduction
Setting up the integration to Okta for authentication involves setup of an application on Okta and configuration on the publish instances for AEM. This document describes the configuration needed to accomplish the integration.

## Okta Setup
Setup of the application and relating of users is done by IT. Ankit Gupta <ankitg@juniper.net>, was able to submit a request for this setup and obtained exported metadata. The metadata contains fields that will be used later.

## Trust Store setup
Certs for Okta as well as for AEM need to be saved and configured in AEM, and AEM utilizes the trust store for this.

1. Save public key from Okta in the trustore. The Certificate alias will be used in configuring the SamlAuthenticationHandler. The cert can be extracted from the saml metadata.


## Configure SamlAuthenticationHandler

![](./SamlAuthenticator1.png)
![](./SamlAuthenticator2.png)

## Change in configuration on SlingAuthenticator

Uncheck allow anonymous

## Setting on Pages or Path In Sites
![](./Site.png)

## Setup Logger
https://aem-publish-dev.juniper.net/system/console/slinglog

Add logger
DEBUG
Log File: saml.log
Logger: com.adobe.granite.auth.saml

## Configure Apache Sling Referrer Filter for IDP
Allow empty check
Allow Hosts=iam-signin-stage.juniper.net
![](./SlingReferrer.png)
