const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
	const files = [
		'./dist/elements/runtime.js',
		'./dist/elements/polyfills.js',
		'./dist/elements/main.js',
		'./dist/elements/vendor.js'
	]
	await fs.ensureDir('elements')
	await concat(files, 'elements/web-components.js');
	await fs.copyFile('./dist/elements/styles.css', 'elements/styles.css')
	const pathAssets = './dist/elements/assets/';
	fs.access(pathAssets, fs.F_OK, (err) => {
		if (err) {
			//console.error(err)
			return
		}

		fs.copy(pathAssets, 'elements/resources/' );
	})
})()