export class SwBreakpoint {
  constructor (
    public readonly name: string,
    public readonly min: string | null,
    public readonly max: string | null
  ) {
    this.minCondition = min ? `(min-width: ${min})` : '';
    this.maxCondition = max ? `(max-width: ${max})` : '';
    this.rangeCondition = [this.minCondition, this.maxCondition].filter(c => c).join(' and ');
  }

  readonly minCondition: string;
  readonly maxCondition: string;
  readonly rangeCondition: string;
}

export const enum SwBreakpointNames {
  xs = 'xs',
  sm = 'sm',
  md = 'md',
  lg = 'lg',
  xl = 'xl'
}

export const SwBreakpoints: Readonly<{ [K in SwBreakpointNames]: SwBreakpoint }> = {
  [SwBreakpointNames.xs]: new SwBreakpoint(SwBreakpointNames.xs, null, '359px'),
  [SwBreakpointNames.sm]: new SwBreakpoint(SwBreakpointNames.sm, '360px', '639px'),
  [SwBreakpointNames.md]: new SwBreakpoint(SwBreakpointNames.md, '640px', '959px'),
  [SwBreakpointNames.lg]: new SwBreakpoint(SwBreakpointNames.lg, '960px', '1279px'),
  [SwBreakpointNames.xl]: new SwBreakpoint(SwBreakpointNames.xl, '1280px', null)
};
