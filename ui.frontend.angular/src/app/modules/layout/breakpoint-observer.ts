import { Injectable, NgZone, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { coerceArray } from '@angular/cdk/coercion';

import { fromEventPattern, Observable, Subject } from 'rxjs';
import { map, startWith, takeUntil } from 'rxjs/operators';

/** The current state of a layout breakpoint. */
export interface SwBreakpointState {
  /** Whether the breakpoint is currently matching. */
  matches: boolean;
  /** Optional name */
  name?: string | undefined;
}

interface Query {
  observable: Observable<SwBreakpointState>;
  mql: MediaQueryList;
}

/** Utility for checking the matching state of @media queries. */
@Injectable()
export class SwBreakpointObserver implements OnDestroy {
  /**  A map of all media queries currently being listened for. */
  private _queries: Map<string, Query> = new Map();
  /** A subject for all other observables to takeUntil based on. */
  private _destroySubject: Subject<void> = new Subject();

  constructor (private mediaMatcher: MediaMatcher, private zone: NgZone) {}

  /** Completes the active subject, signalling to all other observables to complete. */
  ngOnDestroy () {
    this._destroySubject.next();
    this._destroySubject.complete();
  }

  /**
   * Whether one or more media queries match the current viewport size.
   * @param value One or more media queries to check.
   * @returns Whether any of the media queries match.
   */
  isMatched (value: string | string[]): boolean {
    const queries = coerceArray(value);
    return queries.some(mediaQuery => this._registerQuery(mediaQuery).mql.matches);
  }

  /**
   * Gets an observable of results for the given queries that will emit new results for any changes
   * in matching of the given queries.
   * @returns A stream of matches for the given queries.
   */
  observe (query: string, name?: string): Observable<SwBreakpointState> {
    const query$ = this._registerQuery(query).observable;

    return query$.pipe(
      map((state: SwBreakpointState) => {
        return {
          matches: state && state.matches,
          name
        };
      })
    );
  }

  /** Registers a specific query to be listened for. */
  private _registerQuery (query: string): Query {
    // Only set up a new MediaQueryList if it is not already being listened for.
    if (this._queries.has(query)) {
      return this._queries.get(query)!;
    }

    const mql: MediaQueryList = this.mediaMatcher.matchMedia(query);
    // Create callback for match changes and add it is as a listener.
    const queryObservable = fromEventPattern<MediaQueryList>(
      // Listener callback methods are wrapped to be placed back in ngZone. Callbacks must be placed
      // back into the zone because matchMedia is only included in Zone.js by loading the
      // webapis-media-query.js file alongside the zone.js file.  Additionally, some browsers do not
      // have MediaQueryList inherit from EventTarget, which causes inconsistencies in how Zone.js
      // patches it.
      (listener: Function) => {
        mql.addListener((e: any) => this.zone.run(() => listener(e)));
      },
      (listener: Function) => {
        mql.removeListener((e: any) => this.zone.run(() => listener(e)));
      })
      .pipe(
        startWith(mql),
        map((nextMql: MediaQueryList) => ({ matches: nextMql.matches })),
        takeUntil(this._destroySubject)
      );

    // Add the MediaQueryList to the set of queries.
    const output = { observable: queryObservable, mql: mql };
    this._queries.set(query, output);
    return output;
  }
}
