export { SwLayoutModule, MediaMatcher } from './layout.module';
export { SwBreakpointObserver } from './breakpoint-observer';
export { SwBreakpoints, SwBreakpointNames } from './breakpoints';
