import { async, inject, TestBed } from '@angular/core/testing';
import { Platform } from '@angular/cdk/platform';
import { MediaMatcher, SwLayoutModule } from './index';

describe('MediaMatcher', () => {
  let mediaMatcher: MediaMatcher;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SwLayoutModule]
      });
    })
  );

  beforeEach(inject([MediaMatcher], (mm: MediaMatcher) => {
    mediaMatcher = mm;
  }));

  it('correctly returns a MediaQueryList to check for matches', () => {
    expect(mediaMatcher.matchMedia('(min-width: 1px)').matches).toBeTruthy();
    expect(mediaMatcher.matchMedia('(max-width: 1px)').matches).toBeFalsy();
  });

  it('adds css rules for provided queries when the platform is webkit, otherwise adds nothing.',
    inject([Platform], (platform: Platform) => {
      const randomWidth = Math.random().toString();
      expect(document.head.textContent).not.toContain(randomWidth);
      mediaMatcher.matchMedia(`(width: ${randomWidth})`);

      if (platform.WEBKIT) {
        expect(document.head.textContent).toContain(randomWidth);
      } else {
        expect(document.head.textContent).not.toContain(randomWidth);
      }
    }));
});
