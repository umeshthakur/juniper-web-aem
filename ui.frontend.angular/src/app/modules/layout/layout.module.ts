import { NgModule } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { PlatformModule } from '@angular/cdk/platform';
import { SwBreakpointObserver } from './breakpoint-observer';

@NgModule({
  imports: [PlatformModule],
  providers: [SwBreakpointObserver, MediaMatcher]
})
export class SwLayoutModule {}

export { MediaMatcher } from '@angular/cdk/layout';
