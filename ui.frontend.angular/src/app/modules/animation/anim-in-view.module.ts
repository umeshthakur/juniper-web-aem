import { Directive, ElementRef, Host, Injectable, NgModule, OnDestroy, OnInit } from '@angular/core';
import {
  animate,
  AnimationBuilder,
  AnimationPlayer,
  AUTO_STYLE,
  keyframes,
  NoopAnimationPlayer,
  style
} from '@angular/animations';
import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { SwPageData, SwPageDataModule } from '../pageData/index';
import { ANIM_EASING_IN_OUT_CURVE } from '../../core/animation/animation';

const SCROLL_TIME: number = Math.round(1000 / 30);
const ANIM_DURATION: number = 750;
const ANIM_DELAY: number = 333;
const ANIM_EASING: string = ANIM_EASING_IN_OUT_CURVE;
const ANIM_DISPLACEMENT: number = 300;

class AnimateInViewElement {
  constructor (private _elRef: ElementRef, private _builder: AnimationBuilder, private _vRuler: ViewportRuler) {}

  private _init = false;
  private _player: AnimationPlayer = new NoopAnimationPlayer();

  playerInit () {
    this._player = this._generatePlayer();
    this._init = true;
    this._player.init();
  }

  playerPlay () {
    this._init = false;
    this._player.play();
  }

  private _generatePlayer () {
    /** Calc offset from center to determine horizontal offset */
    let offsetFromCenter = Math.round(this.offsetFromCenter);
    offsetFromCenter = typeof offsetFromCenter === 'number' && !isNaN(offsetFromCenter) ? offsetFromCenter : 0;
    const DELAY = Math.round(Math.random() * ANIM_DELAY);

    const animFactory = this._builder.build(
      animate(
        `${ANIM_DURATION + DELAY}ms ${ANIM_EASING}`,
        keyframes([
          style({ opacity: 0, offset: 0 }),
          style({
            transform: `translate(${offsetFromCenter}px, ${ANIM_DISPLACEMENT}px)`,
            opacity: 0,
            offset: DELAY / ANIM_DURATION
          }),
          style([AUTO_STYLE, { offset: 1 }])
        ])
      )
    );
    return animFactory.create(this.element);
  }

  get element (): HTMLElement {
    return this._elRef.nativeElement;
  }

  get scrollTop (): number {
    const el = this.element;
    const top = el.getBoundingClientRect().top;
    return this._init ? top - ANIM_DISPLACEMENT : top;
  }

  get offsetFromCenter (): number {
    const el = this.element;
    const vwBounds = this._vRuler.getViewportRect();
    const elBounds = el.getBoundingClientRect();

    const vwCenter = vwBounds.width / 2;
    const elCenter = elBounds.left + elBounds.width / 2;

    return elCenter - vwCenter;
  }
}

@Injectable()
export class SwAnimateInViewService implements OnDestroy {
  constructor (
    private _scrollDispatch: ScrollDispatcher,
    private _vRuler: ViewportRuler,
    private _builder: AnimationBuilder,
    _pageData: SwPageData
  ) {
    _pageData.fetchPageConfig()
      .pipe(take(1))
      .subscribe(config => {
        this._animateOnScroll = !!config.animateOnScroll;
        this._init();
      });
  }

  private _animateOnScroll: boolean = false;
  private _nextRegistryId: number = 0;
  private _registry: Map<number, AnimateInViewElement> = new Map();
  private _viewportSubscription: Subscription;
  private _viewportHeight: number = 0;
  private _checkInProgress: boolean = false;

  ngOnDestroy () {
    if (this._viewportSubscription) this._viewportSubscription.unsubscribe();
  }

  register (_el: ElementRef): number | false {
    if (!this._viewportSubscription) return false;

    const el = new AnimateInViewElement(_el, this._builder, this._vRuler);

    /** Only registers elements that are below the view */
    if (this._isBelowView(el)) {
      return this._addToRegistry(el);
    }

    return false;
  }

  unregister (uid: number): boolean {
    return this._registry.delete(uid);
  }

  private _addToRegistry (el: AnimateInViewElement) {
    const uid = this._nextRegistryId++;
    this._registry.set(uid, el);
    /** Initializing the player sets the first keyframe */
    el.playerInit();
    return uid;
  }

  private _init () {
    if (!this._animateOnScroll) return;

    if (!this._viewportSubscription) {
      this._viewportSubscription =
        this._scrollDispatch.scrolled(SCROLL_TIME).subscribe(() => this._scrollHandler());
      this._updateViewportMeasurements();
    }
  }

  private _scrollHandler () {
    if (!this._registry.size) return;

    this._updateViewportMeasurements();
    this._checkElementPositions();
  }

  private _checkElementPositions () {
    /**
     * Scroll might be faster that the DOM checks, so checks will be
     * deferred so that only a single one is done at a time.
     */
    if (this._checkInProgress) return;

    /** As elements are animated in, collect them to unregister them afterwards */
    const elsToUnregister: number[] = [];
    this._checkInProgress = true;

    this._registry.forEach((el, k) => {
      /** Check whether the el */
      const _isBelowView = this._isBelowView(el);
      if (!_isBelowView) {
        elsToUnregister.push(k);
        el.playerPlay();
      }
    });

    /** Unregister all elements that have come into view */
    elsToUnregister.forEach(k => this.unregister(k));

    /** The check is done, allow for more checks */
    this._checkInProgress = false;
  }

  private _isBelowView (el: AnimateInViewElement) {
    return el.scrollTop > this._viewportHeight;
  }

  /** Caches viewport bottom on scroll */
  private _updateViewportMeasurements (): void {
    this._viewportHeight = this._vRuler.getViewportRect().height;
  }
}

@Directive({
  selector: '[swAnimInView]'
})
export class SwAnimateInViewDirective implements OnInit, OnDestroy {
  constructor (private _animInViewSvc: SwAnimateInViewService, @Host() private _el: ElementRef) {}

  private _registryId: number;

  ngOnInit () {
    /**
     * Register async to allow for the DOM and styling to settle.
     * Otherwise, the element scrollTop could be largely incorrect.
     */
    setTimeout(() => {
      const id = this._animInViewSvc.register(this._el);
      if (typeof id === 'number') {
        this._registryId = id;
      }
    }, 0);
  }

  ngOnDestroy () {
    if (typeof this._registryId === 'number') this._animInViewSvc.unregister(this._registryId);
  }
}

@NgModule({
  imports: [SwPageDataModule],
  declarations: [SwAnimateInViewDirective],
  exports: [SwAnimateInViewDirective],
  providers: [SwAnimateInViewService]
})
export class SwAnimateInViewModule {}
