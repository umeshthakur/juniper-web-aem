import { Component, ElementRef, ViewChild } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Subject } from 'rxjs';

import { RxContextDirective } from './rxContext.directive';

@Component({
  template: `
    <div *rxContext="let user on user$">
      <span #job>{{user?.job}}</span>
      <span #name>{{user?.name}}</span>
    </div>
  `
})
class RxContextTestComponent {
  @ViewChild('job') jobRef: ElementRef;
  @ViewChild('name') nameRef: ElementRef;

  userData = [
    { name: 'Ana', job: 'Anthropologist' },
    { name: 'Barry', job: 'Bar tender' },
    { name: 'Claude', job: 'Camouflage artist' }
  ];
  user = new Subject<{ name: string, job: string }>();
  user$ = this.user.asObservable();
  index = -1;

  getNext () {
    this.index += 1;
    this.user.next(this.userData[this.index]);
  }
}

describe('RxContextDirective', () => {

  let comp: RxContextTestComponent;
  let fixture: ComponentFixture<RxContextTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RxContextTestComponent,
        RxContextDirective
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RxContextTestComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  it(`should connect a stream and respond to it\'s data`, () => {
    comp.getNext();
    fixture.detectChanges();

    expect(comp.jobRef.nativeElement.textContent).toBe(comp.userData[comp.index].job);
    expect(comp.nameRef.nativeElement.textContent).toBe(comp.userData[comp.index].name);

    comp.getNext();
    fixture.detectChanges();

    expect(comp.jobRef.nativeElement.textContent).toBe(comp.userData[comp.index].job);
    expect(comp.nameRef.nativeElement.textContent).toBe(comp.userData[comp.index].name);

    comp.getNext();
    fixture.detectChanges();

    expect(comp.jobRef.nativeElement.textContent).toBe(comp.userData[comp.index].job);
    expect(comp.nameRef.nativeElement.textContent).toBe(comp.userData[comp.index].name);

    comp.getNext();
    fixture.detectChanges();

    expect(comp.jobRef.nativeElement.textContent).toBe('');
    expect(comp.nameRef.nativeElement.textContent).toBe('');
  });

  it(`should disconnect and tear down`, () => {
    comp.getNext();
    fixture.detectChanges();

    expect(comp.jobRef.nativeElement.textContent).toBe(comp.userData[comp.index].job);
    expect(comp.nameRef.nativeElement.textContent).toBe(comp.userData[comp.index].name);

    comp.user$ = null;
    fixture.detectChanges();
  });

});
