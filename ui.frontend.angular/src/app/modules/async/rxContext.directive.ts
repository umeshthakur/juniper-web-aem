import { Directive, EmbeddedViewRef, Input, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';

/**
 * Directive useful to establish an observable in a given context
 * instead of using the `async` pipe multiple times, for example
 */
@Directive({
  selector: '[rxContext][rxContextOn]' // tslint:disable-line directive-selector
})
export class RxContextDirective implements OnDestroy {

  private _viewRef: EmbeddedViewRef<any>;
  private _rxSubscription: Subscription;

  @Input()
  set rxContextOn (rxCtx) {
    if (rxCtx) {
      this._connect(rxCtx);
    } else {
      this._disconnect();
    }
  }

  constructor (
    private _templateRef: TemplateRef<any>,
    private _vcr: ViewContainerRef) {}

  private _connect (rxContextOn) {
    this._disconnect();

    this._rxSubscription = rxContextOn.subscribe(v => {

      if (!this._viewRef) {
        this._viewRef = this._vcr.createEmbeddedView(this._templateRef);
      }

      this._viewRef.context.$implicit = v;
      this._viewRef.markForCheck();
      this._viewRef.detectChanges();
    });
  }

  private _disconnect () {
    if (this._rxSubscription) this._rxSubscription.unsubscribe();
    this._rxSubscription = null;
  }

  private _teardown () {
    this._disconnect();
    this._viewRef.destroy();
    this._viewRef = null;
  }

  ngOnDestroy () {
    this._teardown();
  }
}
