import { NgModule } from '@angular/core';
import { RxContextDirective } from './rxContext.directive';

@NgModule({
  declarations: [ RxContextDirective ],
  exports: [ RxContextDirective ]
})
export class AsyncModule {}
