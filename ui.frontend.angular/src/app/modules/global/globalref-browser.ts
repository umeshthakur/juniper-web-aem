import { Location } from '@angular/common';
import { Injectable, Optional, SkipSelf } from '@angular/core';

import { SwUrl } from '../../util/index';
import { GlobalRef } from './globalref';

/**
 * Return the global Window object
 */
export function _window (): Window {
  return window;
}

@Injectable()
export class BrowserGlobalRef extends GlobalRef {
  constructor (private _location: Location) {
    super();
  }

  /**
   * Returns the `Location` using a `PathLocationStrategy`.
   *
   * Not the Window or Document [Location
   * ](https://developer.mozilla.org/en-US/docs/Web/API/Location).
   * It is a service that’s exposed by Angular to interact with a browser's URL.
   */
  get location (): Location { return this._location; }

  /**
   * Returns the Window Location [search property
   * ](https://developer.mozilla.org/en-US/docs/Web/API/HTMLHyperlinkElementUtils/search)
   * parsed as a string.
   */
  get query (): string {
    return (new SwUrl(this.location.path())).query;
  }

  /**
   * Returns the Window Location [search property
   * ](https://developer.mozilla.org/en-US/docs/Web/API/HTMLHyperlinkElementUtils/search)
   * parsed as a key-value map.
   */
  get queryAsMap (): { [key: string]: string } {
    return (new SwUrl(this.location.path())).queryObject;
  }
}

export function BROWSER_GLOBALREF_PROVIDER_FACTORY (parentGlobalRef: GlobalRef, location: Location) {
  return parentGlobalRef || new BrowserGlobalRef(location);
}

export const BROWSER_GLOBALREF_PROVIDER = {
  // If there is already a GlobalRef available, use that. Otherwise, provide a new one.
  provide: GlobalRef,
  deps: [[new Optional(), new SkipSelf(), GlobalRef], Location],
  useFactory: BROWSER_GLOBALREF_PROVIDER_FACTORY
};
