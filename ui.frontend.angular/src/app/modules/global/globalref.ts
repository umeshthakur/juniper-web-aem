import { Location } from '@angular/common';
import { Optional, SkipSelf } from '@angular/core';
import { unimplemented } from '../../core/error';

/**
 * Does nothing by default. It's meant as a DI token
 * or to implement a mock window.
 */
export abstract class GlobalRef {
  get location (): Location {
    return unimplemented();
  }

  get query (): string {
    return unimplemented();
  }

  get queryAsMap (): { [key: string]: string } {
    return unimplemented();
  }
}

export class NoopGlobalRef extends GlobalRef {}

export function NOOP_GLOBALREF_PROVIDER_FACTORY (parentGlobalRef: GlobalRef) {
  return parentGlobalRef || new NoopGlobalRef();
}

export const NOOP_GLOBALREF_PROVIDER = {
  // If there is already a GlobalRef available, use that. Otherwise, provide a new one.
  provide: GlobalRef,
  deps: [[new Optional(), new SkipSelf(), GlobalRef]],
  useFactory: NOOP_GLOBALREF_PROVIDER_FACTORY
};
