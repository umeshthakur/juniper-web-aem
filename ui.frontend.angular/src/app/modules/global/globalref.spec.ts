import { GlobalRef } from './globalref';

class MockGlobalRef extends GlobalRef {}

describe('GlobalRef', () => {
  let _global: GlobalRef;

  beforeEach(() => {
    _global = new MockGlobalRef();
  });

  it('should report all properties as `unimplemented`', () => {
    expect(() => _global.location).toThrow();
    expect(() => _global.query).toThrow();
    expect(() => _global.queryAsMap).toThrow();
  });

});
