import {
  APP_BASE_HREF,
  CommonModule,
  Location,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';
import {
  ModuleWithProviders,
  NgModule
} from '@angular/core';
import { GlobalRef, NOOP_GLOBALREF_PROVIDER } from './globalref';
import { BrowserGlobalRef } from './globalref-browser';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    Location,
    { provide: APP_BASE_HREF, useValue: '' },
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    NOOP_GLOBALREF_PROVIDER
  ]
})
export class SwGlobalModule {
  static forBrowser (): ModuleWithProviders<SwGlobalModule> {
    return {
      ngModule: SwGlobalModule,
      providers: [
        { provide: GlobalRef, useClass: BrowserGlobalRef }
      ]
    };
  }
}
