export { SwGlobalModule } from './global.module';
export { GlobalRef, NOOP_GLOBALREF_PROVIDER } from './globalref';
export { BrowserGlobalRef, BROWSER_GLOBALREF_PROVIDER } from './globalref-browser';
