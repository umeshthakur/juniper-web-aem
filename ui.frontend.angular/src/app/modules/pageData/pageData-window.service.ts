import { Injectable } from '@angular/core';
import { EMPTY, Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { SwBasePage } from '../../core/base/index';
import { SwPageData } from './pageData.service';

@Injectable()
export class SwWindowPageData extends SwPageData {

  public fetchData (): Observable<SwBasePage> {
    /** Check if the data is already available */
    if (this._store.has(this.PAGE_DATA_KEY)) {
      return of(this._cloneData(this._store.get(this.PAGE_DATA_KEY)));
    }

    /** Otherwise, call for retrieval */
    return this._fetchPageDataFromWindow(this.PAGE_DATA_KEY).pipe(
      tap(d => this._store.set(this.PAGE_DATA_KEY, d)),
      map(d => this._cloneData(d))
    );
  }

  private _fetchPageDataFromWindow (varName?: string): Observable<SwBasePage> {
    if (window && typeof window[varName] === 'object' && window[varName]) {
      return of(new SwBasePage(window[varName]));
    } else {
      console.warn('No page data available on Window');
      return EMPTY;
    }
  }

  private readonly PAGE_DATA_KEY = '_PAGE_DATA';
}
