import { Optional, SkipSelf } from '@angular/core';
import { EMPTY, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { SwBasePage, SwBasePageConfig, SwBasePageMeta } from '../../core/base/index';
import { cloneObject } from '../../util/index';

export abstract class SwPageData {
  /**
   * A map of keys and stored page data
   */
  protected _store: Map<string, SwBasePage> = new Map<string, SwBasePage>();

  /**
   * Gets an iterable of the store
   */
  public get store (): IterableIterator<[string, SwBasePage]> { return this._store.entries(); }

  /** Clears the store */
  public clearStore (): void { this._store.clear(); }

  /**
   * Fetches data from the data source, given a key
   */
  abstract fetchData (key?: string, options?: object): Observable<SwBasePage>;

  /**
   * Fetches data from the data source, given a key
   */
  public fetchMeta (key?: string): Observable<SwBasePageMeta> {
    return this.fetchData(key).pipe(map(d => d.meta));
  }

  /**
   * Fetches data from the data source, given a key
   */
  public fetchPageConfig (key?: string): Observable<SwBasePageConfig> {
    return this.fetchData(key).pipe(map(d => d.config));
  }

  protected _cloneData<T> (data: T): T {
    return cloneObject(data);
  }
}

export class NoopSwPageData extends SwPageData {
  fetchData (): Observable<SwBasePage> { return EMPTY; }
}

export function PAGEDATA_PROVIDER_FACTORY (parentSwPageData: SwPageData) {
  return parentSwPageData || new NoopSwPageData();
}

export const PAGEDATA_PROVIDER = {
  // If there is already a SwPageData available, use that. Otherwise, provide a new one.
  provide: SwPageData,
  deps: [[new Optional(), new SkipSelf(), SwPageData]],
  useFactory: PAGEDATA_PROVIDER_FACTORY
};
