import { ModuleWithProviders, NgModule } from '@angular/core';
import { SwWindowPageData } from './pageData-window.service';
import { PAGEDATA_PROVIDER, SwPageData } from './pageData.service';

@NgModule({
  providers: [ PAGEDATA_PROVIDER ]
})
export class SwPageDataModule {
  static fromWindow (): ModuleWithProviders<any> {
    return {
      ngModule: SwPageDataModule,
      providers: [
        { provide: SwPageData, useClass: SwWindowPageData }
      ]
    };
  }
}

export { SwBasePage, SwBasePageMeta } from '../../core/base/index';
export { SwPageData };
