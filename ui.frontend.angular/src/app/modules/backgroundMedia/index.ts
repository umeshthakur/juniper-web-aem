import { NgModule } from '@angular/core';
import { SwVideoModule } from '../../jnpr-common/video/video.component';
import { SwBackgroundImageDirective, SwBackgroundImageModule } from './backgroundImage.module';
import { SwBackgroundVideoDirective } from './backgroundVideo.directive';

@NgModule({
  imports: [SwVideoModule, SwBackgroundImageModule],
  exports: [SwBackgroundImageDirective, SwBackgroundVideoDirective],
  declarations: [SwBackgroundVideoDirective]
})
export class SwBackgroundMediaModule {}
