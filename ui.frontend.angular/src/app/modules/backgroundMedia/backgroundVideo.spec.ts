import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SwVideoComponent, SwVideoModule } from '../../components/video/video.component';
import { SwBackgroundVideoDirective } from './backgroundVideo.directive';

@Component({
  template: `<div [swBackgroundVideo]="testUrl"></div>`
})
class SwBackgroundVideoTestComponent {
  testUrl: string;
}

describe(`SwBackgroundVideoDirective`, () => {
  const videoComponentSelector = 'sw-video';

  if (!videoComponentSelector) throw new Error(`Couldn't get video selector for \`backgroundVideo.directive\` tests.`);

  describe('integration test', () => {
    const testUrl = 'https://example.com/path/to/video.mp4';
    let fixture: ComponentFixture<SwBackgroundVideoTestComponent>;
    let hostTestComponent: SwBackgroundVideoTestComponent;
    let hostDebugElement: DebugElement;
    let hostNativeElement: HTMLElement;
    let bgVideoDirective: SwBackgroundVideoDirective;

    const queryForVideoComponent = (): HTMLElement[] => [].slice.call(hostNativeElement.querySelectorAll(videoComponentSelector));

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
          SwVideoModule
        ],
        declarations: [
          SwBackgroundVideoDirective,
          SwBackgroundVideoTestComponent
        ]
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SwBackgroundVideoTestComponent);
      fixture.detectChanges();

      hostTestComponent = fixture.debugElement.componentInstance;
      hostDebugElement = fixture.debugElement.query(By.directive(SwBackgroundVideoDirective));
      hostNativeElement = hostDebugElement.nativeElement;
      bgVideoDirective = hostDebugElement.injector.get<SwBackgroundVideoDirective>(SwBackgroundVideoDirective);
    });

    it('should attach/detach video components when setting a url', () => {
      /** Query video elements */
      let videoDebugElements = queryForVideoComponent();
      /** Expect no videos */
      expect(videoDebugElements.length).toBe(0);

      /** Set url */
      hostTestComponent.testUrl = testUrl;
      fixture.detectChanges();

      /** Query for video elements again */
      videoDebugElements = queryForVideoComponent();
      /** Expect a component type of Video to be the new ComponentRef */
      expect(bgVideoDirective.compRef.componentType).toBe(SwVideoComponent);
      expect(videoDebugElements.length).toBe(1);

      /** Remove url */
      hostTestComponent.testUrl = null;
      fixture.detectChanges();

      /** Query video elements */
      videoDebugElements = queryForVideoComponent();
      /** Expect no videos */
      expect(videoDebugElements.length).toBe(0);
    });

    it('should set styles to center the video horizontally and vertically', () => {
      /** Set url */
      hostTestComponent.testUrl = testUrl;
      fixture.detectChanges();

      /**
       * Check for `top`, `left`, `transform` etc.
       */
      const videoDebugElements = queryForVideoComponent();
      expect(videoDebugElements[0].style.top).toEqual('50%');
      expect(videoDebugElements[0].style.left).toEqual('50%');
      expect(/^translate\((-50%).+(-50%)\)$/.test(videoDebugElements[0].style.transform)).toBe(true);
      expect(videoDebugElements[0].style.overflow).toEqual('hidden');

    });

    it('should set the correct width depending on the aspect ratio of the video', () => {
      /**
       * Add mock horizontal, square, and vertical videos
       * with a short duration, e.g., 1 second.
       */
      const squareVideoUrl = 'https://jnpr-assets.swirl-staging.net/test/square-video-test.mp4';
      const horizontalVideoUrl = 'https://jnpr-assets.swirl-staging.net/test/horizontal-video-test.mp4';
      const verticalVideoUrl = 'https://jnpr-assets.swirl-staging.net/test/vertical-video-test.mp4';

      /** Test square video */
      hostTestComponent.testUrl = squareVideoUrl;
      fixture.detectChanges();

      const videoDebugElements = queryForVideoComponent();

      expect(videoDebugElements[0].style.width).toEqual(hostNativeElement.offsetWidth + 'px');

      /** Test vertical video */
      hostTestComponent.testUrl = verticalVideoUrl;
      fixture.detectChanges();

      expect(videoDebugElements[0].style.width).toEqual(hostNativeElement.offsetWidth + 'px');

      /** Test horizontal video with aspect ratio more than 16:9 (2.35 in this case) */
      hostNativeElement.style.height = '900px';
      hostNativeElement.style.width = '1600px';
      hostTestComponent.testUrl = horizontalVideoUrl;
      bgVideoDirective.bgVideoCoverSetStyle(false, 2.35, videoDebugElements[0]);
      fixture.detectChanges();

      expect(parseInt(videoDebugElements[0].style.width, 10)).toBeCloseTo(2115);
    });

  });

});
