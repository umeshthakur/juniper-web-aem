import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SwBackgroundImageDirective } from './backgroundImage.directive';

function testBgImageUrl (img: string, subject: string): boolean {
  /** Escape conflicting chars with RegExp */
  img = img.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  const imgRegexp = new RegExp(`url\(.+${img}.+\)`);

  return imgRegexp.test(subject);
}

@Component({
  template: `<div [swBackgroundImage]="testBackground"></div>`
})
class SwBackgroundImageTestComponent {
  testBackground: string;
}

describe(`SwBackgroundImageDirective`, () => {
  const baseImg = 'https://example.com/path/to/image/my-image.jpg';
  const defaultBgPosition: string = '50%';

  describe('in isolation', () => {
    let c: SwBackgroundImageDirective;

    beforeEach(() => {
      c = new SwBackgroundImageDirective();
    });

    it('should format URLs to conform with css `background-image`', () => {
      c.background = `${baseImg}`;
      expect(c.backgroundImage).toBe(`url(${baseImg})`);
    });

    it('should handle being passed rogue values', () => {
      c.background = 10 as any;
      expect(c.backgroundImage).toBe('');
    });

    describe('by reading the hash', () => {

      it('should set `background-position` to be centered by default', () => {
        c.background = `${baseImg}`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
      });

      it('should set default values for any coordinates given without value', () => {
        c.background = `${baseImg}#x`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
        c.background = `${baseImg}#y`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
        c.background = `${baseImg}#xy`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
        c.background = `${baseImg}#xyxy`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
      });

      it('to set `x` `background-position`', () => {
        const x = 75;
        c.background = `${baseImg}#x${x}`;
        expect(c.backgroundPosition).toBe(`${x}% ${defaultBgPosition}`);
      });

      it('to set `y` `background-position`', () => {
        const y = 25;
        c.background = `${baseImg}#y${y}`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${y}%`);
      });

      it('to set `x` & `y` `background-position`', () => {
        const x = 66;
        const y = 33;
        c.background = `${baseImg}#x${x}y${y}`;
        expect(c.backgroundPosition).toBe(`${x}% ${y}%`);
      });

      it('to set `x` & `y` `background-position` even if they\'re in a different order', () => {
        const x = 66;
        const y = 33;
        c.background = `${baseImg}#y${y}x${x}`;
        expect(c.backgroundPosition).toBe(`${x}% ${y}%`);
      });

      it('to use the last provided `x` and/or `y` values', () => {
        const x1 = 66;
        const y1 = 33;
        const x2 = 75;
        const y2 = 25;

        /** With repeated x value */
        c.background = `${baseImg}#x${x1}y${y1}x${x2}`;
        expect(c.backgroundPosition).toBe(`${x2}% ${y1}%`);
        /** With repeated y value */
        c.background = `${baseImg}#x${x1}y${y1}y${y2}`;
        expect(c.backgroundPosition).toBe(`${x1}% ${y2}%`);
        /** With both x and y repeated values */
        c.background = `${baseImg}#x${x1}y${y1}x${x2}y${y2}`;
        expect(c.backgroundPosition).toBe(`${x2}% ${y2}%`);
      });

      it('should handle odd hash scenarios', () => {
        const x = 25;
        const y = 66;

        c.background = `${baseImg}#`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
        c.background = `${baseImg}##`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
        c.background = `${baseImg}#y${y}#`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${y}%`);
        c.background = `${baseImg}##y#${y}#x${x}##`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${defaultBgPosition}`);
      });

      it('should set default `background-position` when values are out of range', () => {
        const x = 125;
        const y = 66;
        c.background = `${baseImg}#y${y}x${x}`;
        expect(c.backgroundPosition).toBe(`${defaultBgPosition} ${y}%`);
      });

    });

  });

  describe('integrated', () => {
    let fixture: ComponentFixture<SwBackgroundImageTestComponent>;
    let testComponent: SwBackgroundImageTestComponent;
    let debugDirective: DebugElement;
    let nativeDirectiveHost: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [
          SwBackgroundImageDirective,
          SwBackgroundImageTestComponent
        ]
      });
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(SwBackgroundImageTestComponent);
      fixture.detectChanges();

      testComponent = fixture.debugElement.componentInstance;
      debugDirective = fixture.debugElement.query(By.directive(SwBackgroundImageDirective));
      nativeDirectiveHost = debugDirective.nativeElement;
    });

    it('should set backgroundImage on host', () => {
      testComponent.testBackground = `${baseImg}`;
      fixture.detectChanges();
      expect(
        testBgImageUrl(
          `${baseImg}`,
          nativeDirectiveHost.style.backgroundImage
        )
      ).toBe(true);
    });

    it('should set backgroundPosition on host', () => {
      testComponent.testBackground = `${baseImg}`;
      fixture.detectChanges();
      expect(nativeDirectiveHost.style.backgroundPosition)
        .toBe(`${defaultBgPosition} ${defaultBgPosition}`);

      const x1 = 75;
      testComponent.testBackground = `${baseImg}#x${x1}`;
      fixture.detectChanges();
      expect(nativeDirectiveHost.style.backgroundPosition)
        .toBe(`${x1}% ${defaultBgPosition}`);

      const y2 = 25;
      testComponent.testBackground = `${baseImg}#y${y2}`;
      fixture.detectChanges();
      expect(nativeDirectiveHost.style.backgroundPosition)
        .toBe(`${defaultBgPosition} ${y2}%`);

      const x3 = 66;
      const y3 = 33;
      testComponent.testBackground = `${baseImg}#y${y3}x${x3}`;
      fixture.detectChanges();
      expect(nativeDirectiveHost.style.backgroundPosition)
        .toBe(`${x3}% ${y3}%`);
    });

  });

});
