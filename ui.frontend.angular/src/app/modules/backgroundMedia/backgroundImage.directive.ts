import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  Renderer2
} from '@angular/core';
import { SwUrl } from '../../util/index';

const directiveId = 'swBackgroundImage';

@Directive({
  selector: '[' + directiveId + ']',
  // tslint:disable-next-line use-host-property-decorator
  host: {
    'role': 'img'
  }
})
export class SwBackgroundImageDirective {
  private readonly _defaultBackgroundImage: string = '';
  private readonly _defaultBackgroundPosition: string = '50%';
  private _backgroundImage: string = this._defaultBackgroundImage;
  private _backgroundPosition: string = this._defaultBackgroundPosition;
  private _backgroundImageUrl: string;

  constructor (private _elementRef: ElementRef, private _renderer: Renderer2) {
    if (window && 'IntersectionObserver' in window) {
      const obs = new IntersectionObserver(entries => {
        entries.forEach(({ isIntersecting }) => {
          if (isIntersecting) {
            // tslint:disable-next-line:max-line-length
            this._renderer.setStyle(this._elementRef.nativeElement, 'backgroundImage', (typeof this._backgroundImageUrl === 'string' && this._backgroundImageUrl !== '' ? `url(${this._backgroundImageUrl})` : this._defaultBackgroundImage));
            obs.unobserve(this._elementRef.nativeElement);
          }
        });
      });
      obs.observe(this._elementRef.nativeElement);
    }
  }
  /**
   * Read the directive's value and set it
   * Same as adding `${directiveSetter}: ${directiveId}` on the input metadata
   */
  @Input(directiveId)
  set background (imageUrl: string) {
    const _imageUrl = new SwUrl(imageUrl);
    (window && 'IntersectionObserver' in window) ? this._backgroundImageUrl = _imageUrl.url : this.backgroundImage = _imageUrl.url;
    this.backgroundPosition = _imageUrl.fragment;
  }

  /**
   * [style.backgroundImage]="_backgroundImage"
   */
  @HostBinding('style.backgroundImage')
  get backgroundImage () { return this._backgroundImage; }

  set backgroundImage (imageUrl: string) {
    this._backgroundImage = (typeof imageUrl === 'string' && imageUrl !== '' ? `url(${imageUrl})` : this._defaultBackgroundImage);
  }

  /**
   * [style.backgroundPosition]="_backgroundPosition"
   */
  @HostBinding('style.backgroundPosition')
  get backgroundPosition () { return this._backgroundPosition; }

  set backgroundPosition (hash: string) {
    const backgroundPosition = {
      x: this._defaultBackgroundPosition,
      y: this._defaultBackgroundPosition
    };
    /** Matches an `x` or `y` followed by one or more numbers. */
    const matchPattern = /([xy])(\d{1,})/gi;
    let matches: RegExpExecArray;

    /** Execute the regex against the hash and iterate through the results */
    // tslint:disable-next-line no-conditional-assignment
    while ((matches = matchPattern.exec(hash)) !== null) {
      if (matches.index === matchPattern.lastIndex) {
        matchPattern.lastIndex++;
      }

      /**
       * matches[1] will match `x` or `y`
       * matches[2] will be the numerical value
       */
      const positionInt: number = parseInt(matches[2], 10);
      if (this.validatePositionValue(positionInt) === true) {
        backgroundPosition[matches[1]] = positionInt.toString() + '%';
      }
    }

    this._backgroundPosition = (backgroundPosition.x + ' ' + backgroundPosition.y);
  }

  validatePositionValue (positionValue): boolean {
    return typeof positionValue === 'number' &&
            isFinite(positionValue) &&
            Math.floor(positionValue) === positionValue &&
            positionValue >= 0 &&
            positionValue <= 100;
  }
}
