import {
  ApplicationRef,
  ComponentFactoryResolver,
  // ComponentRef,
  Directive,
  ElementRef,
  Injector,
  Input,
  // NgZone,
  OnDestroy,
  Renderer2
} from '@angular/core';
// import { ViewportRuler } from '@angular/cdk/scrolling';
// import { merge } from 'rxjs';
// import { startWith, switchMap, take } from 'rxjs/operators';

// import { HTML5videoOptions, SwVideoComponent, YoutubePlayerOptions } from '../../components/jnpr-common/video/video.component';
// import { ComponentPortal, DomPortalOutlet } from '@angular/cdk/portal';

const directiveId = 'swBackgroundVideo';

@Directive({
  selector: '[' + directiveId + ']',
  exportAs: directiveId
})
export class SwBackgroundVideoDirective implements OnDestroy {
  // compRef: ComponentRef<SwVideoComponent>;
  // private _portal: ComponentPortal<SwVideoComponent>;
  // private _portalOutlet: DomPortalOutlet;
  // private _resizeListener: Subscription;
  // private readonly videoOptions: HTML5videoOptions|YoutubePlayerOptions = { autoplay: 1, loop: 1, muted: 1, controls: 0 };

  constructor (
    // private _viewport: ViewportRuler,
    private _element: ElementRef,
    private _renderer: Renderer2,
    // private _ngZone: NgZone,
    _inj: Injector,
    _appRef: ApplicationRef,
    _cfr: ComponentFactoryResolver
  ) {
    // this._portal = new ComponentPortal(SwVideoComponent);
    // this._portalOutlet = new DomPortalOutlet(this._element.nativeElement, _cfr, _appRef, _inj);
  }

  ngOnDestroy () {
    // if (this._resizeListener) this._resizeListener.unsubscribe();
  }

  /**
   * Sets the styles necessary to size the video to cover the parent container
   */
  bgVideoCover (addExtraStyles: boolean = false) {
    // if (!(this.compRef && this.compRef.location && this.compRef.location.nativeElement)) return;

    // const videoAspectRatio = this.compRef.instance.aspectRatio;
    // const comp = this.compRef.location.nativeElement;

    // this.bgVideoCoverSetStyle(addExtraStyles, videoAspectRatio, comp);
  }

  bgVideoCoverSetStyle (addExtraStyles: boolean = false, videoAspectRatio: number, comp: any) {
    const h: number = this._element.nativeElement.offsetHeight;
    const w: number = this._element.nativeElement.offsetWidth;
    const contAspectRatio = w / h;

    /**
     * If `this.videoAspectRatio <= contAspectRatio`, the width to set
     * would be the width. Here we leverage that scenario to initialize;
     * However, if the video aspect ratio proves higher than the container
     * aspect ratio, we'll use the height and the video aspect ratio to
     * determine the width the video should be.
     */
    let res: number = Math.ceil(w);
    if (videoAspectRatio > contAspectRatio) {
      res = Math.ceil(h * videoAspectRatio);
    }

    /** Setting the video width is effective cross-browser */
    this._renderer.setStyle(comp, 'width', `${res}px`);

    /**
     * Cannot ensure autoplay in mobile platforms,
     * so video is hidden below 1024
     */
    this._renderer.setStyle(comp, 'display', (w < 1024 ? 'none' : null));

    /** Only add styles below when video is being set, not resized */
    if (!addExtraStyles) return;
    this._renderer.setStyle(comp, 'position', `absolute`);
    this._renderer.setStyle(comp, 'top', `50%`);
    this._renderer.setStyle(comp, 'left', `50%`);
    /* Webkit prefix on transform should cover: Ch <36, Saf 5.1+, iOS < 9.2, An =<4.4.4 */
    this._renderer.setStyle(comp, 'webkitTransform', `translate(-50%,-50%)`);
    this._renderer.setStyle(comp, 'transform', `translate(-50%,-50%)`);
    this._renderer.setStyle(comp, 'overflow', `hidden`);
  }

  /**
   * Read the directive's value and set it
   * Same as adding `${directiveSetter}: ${directiveId}` on the input metadata
   */
  @Input(directiveId)
  set video (videoUrl: string) {
    if (!this._element.nativeElement) return;

    try {
      // if (this._portal.isAttached) this._portal.detach();
      if (typeof videoUrl === 'string' && videoUrl.length > 0) {
        // this.compRef = this._portal.attach(this._portalOutlet);
        // this.compRef.changeDetectorRef.detach();

        // const videoProps: Partial<SwVideoComponent> = {
        //   url: videoUrl,
        //   options: this.videoOptions
        // };
        // this.compRef.instance.properties = videoProps;
        // this.compRef.changeDetectorRef.markForCheck();
        // this.compRef.changeDetectorRef.detectChanges();
        // this.bgVideoCover(true);
        this._listenForResize();
      }
    } catch (err) {
      console.error(err.message);
    }
  }

  private _listenForResize () {
    // if (!this.compRef) return;
    /** If already listening unsubscribe */
    // if (this._resizeListener) this._resizeListener.unsubscribe();

    // this._resizeListener = merge(this._viewport.change(), this.compRef.instance.loaded)
    //   .pipe(
    //     startWith(null),
    //     switchMap(() => this._ngZone.onMicrotaskEmpty.pipe(take(1)))
    //   )
    //   .subscribe(() => this.bgVideoCover());
  }
}
