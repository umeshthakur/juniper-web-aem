import { NgModule } from '@angular/core';
import { SwBackgroundImageDirective } from './backgroundImage.directive';

@NgModule({
  imports: [],
  exports: [SwBackgroundImageDirective],
  declarations: [SwBackgroundImageDirective]
})
export class SwBackgroundImageModule {}

export { SwBackgroundImageDirective };
