/**
 * Logger service
 */
export abstract class Logger {
  abstract error (...args: any[]): void;
  abstract debug (...args: any[]): void;
  abstract group (...args: any[]): void;
  abstract groupEnd (...args: any[]): void;
  abstract log (...args: any[]): void;
  abstract warn (...args: any[]): void;
}
