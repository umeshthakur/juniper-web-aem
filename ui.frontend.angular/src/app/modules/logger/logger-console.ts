import { Injectable, Optional, SkipSelf } from '@angular/core';
import { Logger } from './logger';

/**
 * Implements a logger service for the Javascript console.
 */
@Injectable()
export class SwConsoleLogger extends Logger {
  error (...args: any[]): void { this._getConsoleMethod('error').apply(console, args); }
  debug (...args: any[]): void { this._getConsoleMethod('debug').apply(console, args); }
  group (msg: string): void { this._getConsoleMethod('group').apply(console, [msg]); }
  groupEnd (): void { this._getConsoleMethod('groupEnd').apply(console, []); }
  log (...args: any[]): void { this._getConsoleMethod('log').apply(console, args); }
  warn (...args: any[]): void { this._getConsoleMethod('warn').apply(console, args); }

  private _getConsoleMethod (methodName: string): Function {
    return typeof console[methodName] === 'function' ? console[methodName] : console.log;
  }
}

export function CONSOLE_LOGGER_PROVIDER_FACTORY (parentLogger: Logger) {
  return parentLogger || new SwConsoleLogger();
}

export const CONSOLE_LOGGER_PROVIDER = {
  provide: Logger,
  deps: [[new Optional(), new SkipSelf(), Logger]],
  useFactory: CONSOLE_LOGGER_PROVIDER_FACTORY
};
