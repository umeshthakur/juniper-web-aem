import { NgModule } from '@angular/core';
import { SwObjectKeysPipe, SwObjectValuesPipe } from './obj-key-value.pipe';

@NgModule({
  declarations: [ SwObjectKeysPipe, SwObjectValuesPipe ],
  exports: [ SwObjectKeysPipe, SwObjectValuesPipe ]
})
export class SwObjectPipesModule {}
