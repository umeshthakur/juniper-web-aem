import { Pipe, PipeTransform } from '@angular/core';

/**
 * Returns an object's keys
 */
@Pipe({ name: 'swObjKeys', pure: false })
export class SwObjectKeysPipe implements PipeTransform {
  transform (object: object, args: any[] = null): string[] {
    /** Ensure object */
    if (typeof object !== 'object') {
      console.warn(`Non-object passed to 'SwObjectKeysPipe'`);
      return [];
    }

    /** Return the object keys */
    return Object.keys(object);
  }
}

/**
 * Returns an object's values
 */
@Pipe({ name: 'swObjValues', pure: false })
export class SwObjectValuesPipe implements PipeTransform {
  transform (object: object, args: any[] = null): any[] {
    /** Ensure object */
    if (typeof object !== 'object') {
      console.warn(`Non-object passed to 'SwObjectValuesPipe'`);
      return [];
    }

    /** Return the object values */
    return Object.keys(object).map(key => object[key]);
  }
}
