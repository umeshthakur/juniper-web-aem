import { Directive, HostListener, Injectable, Input, ModuleWithProviders, NgModule } from '@angular/core';

const evalSelector = 'swEval';

@Injectable()
export class SwEvalService {
  /**
   * Executes the passed string as code
   */
  run (code: string, e?: Event) {
    try {
      if (!code) return;

      if (typeof code === 'string') {
        if (code.trim().length > 0) new Function('event', `"use strict"; ${code}`)(e);
      } else {
        throw new TypeError(`Evaluation requires 'string' but got '${typeof code}'.`);
      }
    } catch (err) {
      console.error(err);
    }
  }
}

@Directive({
  selector: '[' + evalSelector + ']'
})
export class SwEvalDirective {
  constructor (private _eval: SwEvalService) {}

  // tslint:disable-next-line no-input-rename
  @Input(evalSelector) code = '';

  @HostListener('click', ['$event'])
  run (e: Event) {
    this._eval.run(this.code, e);
  }
}

@NgModule({
  declarations: [SwEvalDirective],
  exports: [SwEvalDirective]
})
export class SwEvalModule {
  static forRoot (): ModuleWithProviders<SwEvalModule> {
    return {
      ngModule: SwEvalModule,
      providers: [SwEvalService]
    };
  }
}
