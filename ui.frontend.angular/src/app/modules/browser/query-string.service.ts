import { Injectable } from '@angular/core';
import { GlobalRef } from '../../modules/global/index';

export interface QueryStringsObj {
  [key: string]: string;
}

export interface IQueryStringService {
  setUrlOptions (options: QueryStringsObj): void;
  getUrlOptions (): QueryStringsObj;
}

@Injectable()
export class QueryStringService implements IQueryStringService {

  constructor (private _global: GlobalRef) {}

  /** Sets the URL query with the given options */
  setUrlOptions (options: QueryStringsObj = {}) {
    /** If no options are passed, do nothing */
    if (Object.keys(options).length === 0) return;

    const paramsObj = Object.assign({}, this.getUrlOptions());

    for (const key in options) {
      if (options.hasOwnProperty(key)) {
        paramsObj[key] = options[key];
      }
    }

    const params = QueryStringService.stringifyParams(paramsObj);

    this._global.location.replaceState(this._global.location.path().split('?')[0], params);
  }

  /** Get URL query as object */
  getUrlOptions (): QueryStringsObj {
    return this._global.queryAsMap;
  }

  static stringifyParams (obj: object) {
    if (typeof obj !== 'object') return '';

    const formatter = (key, v) => (
      v === null ?
      encodeURIComponent(key) :
      `${encodeURIComponent(key)}=${encodeURIComponent(v)}`
    );

    return Object.keys(obj)
      /** Sort keys */
      .sort()
      /** Map keys to URI encoded pairs */
      .map((key) => {
        const val = obj[key];

        if (val === undefined) return '';

        if (val === null) return encodeURIComponent(key);

        if (Array.isArray(val)) {
          const result = [];

          val.slice()
            .filter(v => v !== undefined)
            .forEach((v) => {
              result.push(formatter(key, v));
            });

          return result.join('&');
        }

        return `${encodeURIComponent(key)}=${encodeURIComponent(val)}`;
      })
      /** Filter empty elements */
      .filter(x => x.length > 0)
      /** Join parts with ampersands */
      .join('&');
  }
}
