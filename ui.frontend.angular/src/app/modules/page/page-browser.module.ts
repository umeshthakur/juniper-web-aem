import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SwGlobalModule } from '../global/index';
import { SwLibComponentsModule } from '../libComponents/libComponents.module';
import { CONSOLE_LOGGER_PROVIDER } from '../logger/index';
import { SwPageDataModule } from '../pageData/index';
import { SwPageComponent } from './page.component';
import { SwMainComponentLibModule } from '../../main-lib';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SwPageDataModule.fromWindow(),
    SwGlobalModule.forBrowser(),
    SwMainComponentLibModule,
    SwLibComponentsModule.withCollection(SwMainComponentLibModule.getTypeStore())
  ],
  declarations: [SwPageComponent],
  providers: [CONSOLE_LOGGER_PROVIDER],
  bootstrap: [SwPageComponent]
})
export class SwBrowserPageModule {}
