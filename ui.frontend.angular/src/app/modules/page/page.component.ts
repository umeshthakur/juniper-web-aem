import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';
// import { SwLiveagentService } from '../../jnpr-common/liveagent/index';
import { Logger } from '../logger/index';
import { SwBasePage, SwPageData } from '../pageData/index';
import { defaultLocale, SwBaseLocale } from '../../core/base/index';

@Component({
  selector: 'sw-page',
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwPageComponent implements OnInit {
  _pageData: SwBasePage = new SwBasePage();

  @Input()
  set pageData (_data: SwBasePage) {
    this._pageData = (
      _data instanceof SwBasePage ?
      _data : new SwBasePage(_data)
    );

    this._logger.group(`pageData set`);
    this._logger.log(this._pageData);
    this._logger.groupEnd();

    // this._processLiveagentConfig();
  }

  private _locale: SwBaseLocale = defaultLocale;

  @HostBinding('attr.sw-locale')
  @HostBinding('attr.lang')
  get _attrLocale () {
    return this._locale.formatAsISOLanguageCode();
  }

  constructor (
    private _pageDataSvc: SwPageData,
    // private _liveagent: SwLiveagentService,
    private _logger: Logger
  ) {}

  ngOnInit () {
    this._getPageData();
    this._getLocale();
  }

  private _getPageData () {
    this._pageDataSvc
      .fetchData()
      .subscribe(data => {
        this.pageData = data;
      }, err => this._logger.error(err));
  }

  private _getLocale () {
    this._pageDataSvc
      .fetchMeta()
      .subscribe(data => {
        this._locale = data.locale;
      }, err => this._logger.error(err));
  }

  // private _processLiveagentConfig () {
  //   if (this._pageData &&
  //        this._pageData.globalComponents &&
  //        this._pageData.globalComponents.liveagent &&
  //        typeof this._pageData.globalComponents.liveagent === 'object' &&
  //        this._pageData.globalComponents.liveagent !== null) {
  //        this._liveagent.config = this._pageData.globalComponents.liveagent;
  //   }
  // }
}
