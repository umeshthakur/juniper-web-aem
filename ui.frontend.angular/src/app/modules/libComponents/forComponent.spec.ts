import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SwBaseComponent, SwBaseComponentObj } from '../../core/base';
import { SwLibComponentsModule } from './libComponents.module';
import { DataType, SwInputProp } from '../../core/validation/index';

const testComponentSelector = 'sw-test-component';

@Component({
  selector: testComponentSelector,
  template: `{{ headline }}`,
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestComponent extends SwBaseComponent {
  @SwInputProp<string>(DataType.string, '')
  headline: string;
}

/**
 * TODO: Create a real (non-test) NgModule as a workaround for
 * https://github.com/angular/angular/issues/10760
 */
@NgModule({
  declarations: [TestComponent],
  exports: [TestComponent],
  entryComponents: [TestComponent]
})
class TestModule {}

@Component({
  template: `<ng-template *swForComponent="mockComponentData"></ng-template>`
})
class SingleDataPointTestForComponentComponent {
  mockComponentData: SwBaseComponentObj[] = [
    {
      selector: testComponentSelector,
      properties: {
        headline: 'Test headline'
      }
    }
  ];

  removeItem () {
    this.mockComponentData.shift();
  }
}

describe('SwForComponentDirective', () => {
  let fixture: ComponentFixture<SingleDataPointTestForComponentComponent>;
  let testComponent: SingleDataPointTestForComponentComponent;
  let createdComponents: any[];

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [TestModule, SwLibComponentsModule.withCollection([TestComponent])],
        declarations: [SingleDataPointTestForComponentComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleDataPointTestForComponentComponent);
    fixture.detectChanges();
    testComponent = fixture.debugElement.componentInstance;
  });

  describe('given a single component', () => {
    it('should create a single component', () => {
      createdComponents = fixture.debugElement.queryAll(By.css(testComponentSelector));
      expect(createdComponents.length).toBe(1);
    });

    it('should create a single Marquee component', () => {
      createdComponents = fixture.debugElement.queryAll(By.directive(TestComponent));
      expect(createdComponents.length).toBe(1);
    });

    it('should remove a single component', () => {
      testComponent.removeItem();
      fixture.detectChanges();
      createdComponents = fixture.debugElement.queryAll(By.css(testComponentSelector));
      expect(createdComponents.length).toBe(0);
    });
  });
});
