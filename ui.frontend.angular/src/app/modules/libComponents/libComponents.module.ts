import { ANALYZE_FOR_ENTRY_COMPONENTS, ModuleWithProviders, NgModule, Provider, Type } from '@angular/core';
import { SwForComponentDirective } from './forComponent.directive';
import { SW_TYPE_STORE_COMPONENTS, SwComponentTypeStoreService } from './componentTypeStore.service';
import { SwBaseComponent } from '../../core/base/index';

export function swLibProvideTypeStore (typeStore: Type<SwBaseComponent>[]): Provider[] {
  return [
    { provide: SW_TYPE_STORE_COMPONENTS, useValue: typeStore },
    { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: typeStore, multi: true }
  ];
}

@NgModule({
  exports: [SwForComponentDirective],
  declarations: [SwForComponentDirective],
  providers: [SwComponentTypeStoreService, swLibProvideTypeStore([])]
})
export class SwLibComponentsModule {
  static withCollection (typeStore: Type<SwBaseComponent>[]): ModuleWithProviders<SwLibComponentsModule> {
    return {
      ngModule: SwLibComponentsModule,
      providers: [swLibProvideTypeStore(typeStore)]
    };
  }
}

export { SwComponentTypeStoreService } from './componentTypeStore.service';
