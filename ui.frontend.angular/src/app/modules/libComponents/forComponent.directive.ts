import {
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  DoCheck,
  Input,
  IterableChanges,
  IterableDiffer,
  IterableDiffers,
  NgIterable,
  OnChanges,
  SimpleChanges,
  ViewContainerRef
} from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { SwBaseComponentObj } from '../../core/base';
import { SwComponentTypeStoreService } from './componentTypeStore.service';

export interface ComponentRefItem {
  component: ComponentRef<any>;
  index: number;
}

// export const enum SwForComponentChangeState { ADDED = 1, REMOVED, MOVED, CHANGED }

// interface SwForComponentChange {
//   type: SwForComponentChangeState;
//   instance: ComponentRef<any>|null
// }

@Directive({
  selector: '[swForComponent]'
})
export class SwForComponentDirective implements DoCheck, OnChanges {

  constructor (
    private _componentTypeStore: SwComponentTypeStoreService,
    private _componentResolver: ComponentFactoryResolver,
    private _viewContainerRef: ViewContainerRef,
    private _differs: IterableDiffers
  ) {}

  @Input() swForComponent: NgIterable<SwBaseComponentObj>;

  /* tslint:disable no-output-rename */

  /** Emits all component created events */
  // @Output('create') createEvents = new EventEmitter<SwForComponentChange>();
  /** Emits all component destroyed events */
  // @Output('destroy') destroyEvents = new EventEmitter<SwForComponentChange>();
  /** Emits all component moved events */
  // @Output('move') moveEvents = new EventEmitter<SwForComponentChange>();
  /** Emits all component changed events */
  // @Output('change') changeEvents = Observable.merge<SwForComponentChange>(this.createEvents, this.destroyEvents, this.moveEvents);

  /* tslint:enable no-output-rename */

  private _differ: IterableDiffer<SwBaseComponentObj>|null = null;

  /**
   * Stores component references once they are attached
   */
  private _componentRefStore = new BehaviorSubject(new Map<SwBaseComponentObj, ComponentRefItem>());

  ngOnChanges (changes: SimpleChanges): void {
    if ('swForComponent' in changes) {
      // React on swForComponent changes only once all inputs have been initialized
      const value = changes['swForComponent'].currentValue;
      if (!this._differ && value) {
        try {
          this._differ = this._differs.find(value).create();
        } catch (e) {
          throw new Error(
            `Cannot find a differ in the provided object ${getTypeNameForDebugging(value)}. SwForComponent only supports Iterables.`);
        }
      }
    }
  }

  ngDoCheck (): void {
    if (this._differ) {
      const changes = this._differ.diff(this.swForComponent);
      if (changes) this._applyChanges(changes);
    }
  }

  private _applyChanges (changes: IterableChanges<SwBaseComponentObj>) {

    changes.forEachAddedItem(change => {
      const componentRef = this._addComponent(change.item);

      if (componentRef !== null) {
        this._componentRefStore.next(
          this._componentRefStore.getValue().set(change.item, { component: componentRef, index: change.currentIndex })
        );
        // this.createEvents.emit({ type: SwForComponentChangeState.ADDED, instance: componentRef.instance})
      } else {
        console.warn(`Invalid component entered:`, change.item);
      }
    });

    changes.forEachRemovedItem(change => {
      const refStore = this._componentRefStore.getValue();
      const storeRecord = refStore.get(change.item);

      if (storeRecord !== undefined) {
        const componentViewRefIndex = this._viewContainerRef.indexOf(storeRecord.component.hostView);
        this._viewContainerRef.remove(componentViewRefIndex);

        refStore.delete(change.item);
        this._componentRefStore.next(refStore);
        // this.destroyEvents.emit({ type: SwForComponentChangeState.REMOVED, instance: null })
      }
    });

    changes.forEachMovedItem(change => {
      const storeRecord = this._componentRefStore.getValue().get(change.item);

      if (storeRecord !== undefined) {
        this._componentRefStore.next(
          this._componentRefStore.getValue().set(
            change.item,
            { component: storeRecord.component, index: change.currentIndex }
          )
        );
        // this.moveEvents.emit({ type: SwForComponentChangeState.MOVED, instance: storeRecord.component.instance })
      }
    });
  }

  private _addComponent (swComponentObj: SwBaseComponentObj): ComponentRef<any>|null {
    const swComponent = this._componentTypeStore.get(swComponentObj.selector);

    /** Return false if `swComponent` isn't an actual component */
    if (swComponent === null) { return swComponent as null; }

    const swComponentFactory = this._componentResolver.resolveComponentFactory(swComponent);
    const componentRef = this._viewContainerRef.createComponent(swComponentFactory);
    if (componentRef.instance.properties && swComponentObj.properties) {
      componentRef.instance.properties = swComponentObj.properties;
    }
    return componentRef;
  }
}

function getTypeNameForDebugging (type: any): string {
  return type['name'] || typeof type;
}
