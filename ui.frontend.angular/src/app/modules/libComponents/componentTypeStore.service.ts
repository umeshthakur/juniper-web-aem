/**
 * Component Service
 */

import { ComponentFactoryResolver, Inject, Injectable, InjectionToken, Type } from '@angular/core';
import { SwBaseComponent } from '../../core/base/index';

export const SW_TYPE_STORE_COMPONENTS = new InjectionToken<Type<SwBaseComponent>[]>('SwTypeStoreComponents');

@Injectable()
export class SwComponentTypeStoreService {
  private _componentMap: Map<string, Type<SwBaseComponent>> = new Map<string, Type<SwBaseComponent>>();

  constructor (@Inject(SW_TYPE_STORE_COMPONENTS) entries: Type<SwBaseComponent>[], resolver: ComponentFactoryResolver) {
    for (const entry of entries) {
      const { selector } = resolver.resolveComponentFactory(entry);
      this._componentMap.set(selector, entry);
    }
  }

  get (id: string): Type<SwBaseComponent>|null {
    return this._componentMap.get(id) || null;
  }
}
