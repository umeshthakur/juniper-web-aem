export {
  SwScrollModule,
  SwScrollToMarker,
  SwScrollToService,
  SwStickyElementState,
  SwStickyManagerService
} from './scroll.module';

export { SCROLL_TO_PROVIDER } from './scroll-to.service';
