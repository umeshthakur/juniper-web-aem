import { ElementRef, Injectable, NgZone, OnDestroy, Optional, SkipSelf } from '@angular/core';
import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import { Platform } from '@angular/cdk/platform';
import { BehaviorSubject, fromEvent, Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';
import { stickyMarkerDefaultOrientation, stickyMarkerOrientationType } from './sticky-marker.directive';

export interface SwStickyElementState {
  fixed: boolean;
  top: number;
}

export interface SwStickyElement {
  marker: ElementRef;
  offset: number;
  height: number;
  enabled: boolean;
  state: BehaviorSubject<SwStickyElementState>;
  orientation: stickyMarkerOrientationType;
}

@Injectable()
export class SwStickyManagerService implements OnDestroy {
  constructor (private _scroll: ScrollDispatcher, private _viewport: ViewportRuler, platform: Platform, ngZone: NgZone) {
    /** Handle resize */
    this._viewport
      .change()
      .pipe(takeUntil(this._destroy$))
      .subscribe(() => this._processElements(true));

    /** Handle scroll */
    this._scroll
      .scrolled()
      .pipe(takeUntil(this._destroy$))
      .subscribe(() => ngZone.run(() => this._processElements()));

    if (platform.isBrowser && /loaded|complete/.test(document.readyState) === false) {
      fromEvent(document, 'DOMContentLoaded')
        .pipe(first())
        .subscribe(() => this._processElements(true));
    }
  }

  private _overallHeight = new BehaviorSubject<number>(0);
  overallHeight$ = this._overallHeight.asObservable();
  private _elements: ReadonlyArray<SwStickyElement> = [];
  private readonly _destroy$ = new Subject<void>();

  ngOnDestroy () {
    this._destroy$.next();
  }

  register (
    marker: ElementRef,
    enabled: boolean = true,
    orientation: stickyMarkerOrientationType = stickyMarkerDefaultOrientation
  ): Observable<SwStickyElementState> {
    const state = new BehaviorSubject<SwStickyElementState>({ fixed: false, top: 0 });
    const element = { marker, offset: 0, height: 0, enabled, state, orientation };
    this._elements = this._elements.concat(element);
    this._processSingleElement(element, true);
    return state.asObservable();
  }

  unregister (marker: ElementRef): void {
    this._elements = this._elements.filter(e => marker !== e.marker);
  }

  setEnabled (marker: ElementRef, enabled: boolean) {
    for (const element of this._elements) {
      if (marker === element.marker) {
        element.enabled = enabled;
      }
    }
  }

  private _processElements (refreshCache: boolean = false) {
    const viewportTop = this._viewport.getViewportScrollPosition().top;

    for (const element of this._elements) {
      this._processSingleElement(element, refreshCache, viewportTop);
    }

    if (refreshCache) this._overallHeight.next(this._heightAboveElement());
  }

  private _processSingleElement (
    element: SwStickyElement,
    refreshCache: boolean = false,
    viewportTop = this._viewport.getViewportScrollPosition().top
  ) {
    const currState = element.state.getValue();

    if (!element.enabled && currState.fixed === true) {
      element.state.next({ fixed: false, top: currState.top });
      return;
    }

    const el: HTMLElement = element.marker.nativeElement;
    let top = currState.top;
    let offset = element.offset;
    // let height = element.height;

    if (refreshCache) {
      const clientRect = el.getBoundingClientRect();
      element.offset = offset = clientRect.top + viewportTop;
      element.height = clientRect.height;
      top = this._heightAboveElement(element);
    }

    const fixed = element.orientation === 'bottom'
      ? viewportTop + this._viewport.getViewportSize().height >= offset
      : viewportTop > offset;

    if (fixed !== currState.fixed || top !== currState.top) element.state.next({ fixed, top });
  }

  private _heightAboveElement (element: SwStickyElement = null) {
    const elements = element ? this._elements.slice(0, this._elements.indexOf(element)) : this._elements;
    let sum = 0;

    for (const el of elements) {
      sum = sum + (el.enabled ? el.height : 0);
    }

    return sum;
  }
}

export function STICKY_MANAGER_PROVIDER_FACTORY (
  parentManager: SwStickyManagerService,
  scrollDispatcher: ScrollDispatcher,
  viewportRuler: ViewportRuler,
  platform: Platform,
  ngZone: NgZone
) {
  return parentManager || new SwStickyManagerService(scrollDispatcher, viewportRuler, platform, ngZone);
}

export const STICKY_MANAGER_PROVIDER = {
  provide: SwStickyManagerService,
  deps: [[new Optional(), new SkipSelf(), SwStickyManagerService], ScrollDispatcher, ViewportRuler, Platform, NgZone],
  useFactory: STICKY_MANAGER_PROVIDER_FACTORY
};
