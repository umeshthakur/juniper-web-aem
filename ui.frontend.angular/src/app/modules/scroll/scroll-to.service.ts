import { Injectable, NgZone, Optional, SkipSelf } from '@angular/core';
import { Observable, Subject } from 'rxjs';

export interface ICoordinates { x: number; y: number; }

export const DEFAULT_SCROLL_TIME = 500;
export type IRequestAnimationFrame = (callback: FrameRequestCallback) => number;

@Injectable()
export class SwScrollToService {

  constructor (private _ngZone: NgZone) {}

  scrollDuration = DEFAULT_SCROLL_TIME;

  scrollable: Element = window as any;

  raf: IRequestAnimationFrame = requestAnimationFrame;
  private _getRaf () { return this.raf; }

  /** Access point to kick off a scroll to animation loop */
  smooth (targetEl: Element, duration?: number) {
    if (this._validateTargetEl(targetEl)) {
      this._scrollTo(this.scrollable, { x: 0, y: this._getOffsetTop(targetEl) }, duration);
    }
  }

  /** Immediately jumps to the passed element */
  jump (targetEl: Element) {
    this.smooth(targetEl, 0);
  }

  scrollStart (): Observable<void> { return this._scrollStart.asObservable(); }
  private _scrollStart: Subject<void> = new Subject<void>();

  scrollEnd (): Observable<void> { return this._scrollEnd.asObservable(); }
  private _scrollEnd: Subject<void> = new Subject<void>();

  /** Returns the easing function */
  protected get _easingFn (): Function {
    const easeIn = p => t => Math.pow(t, p);
    const easeOut = p => t => (1 - Math.abs(Math.pow(t - 1, p)));
    const easeInOut = p => t => t < 0.5 ? easeIn(p)(t * 2) / 2 : easeOut(p)(t * 2 - 1) / 2 + 0.5;
    return easeInOut(3);
  }

  /** Gets the element offset top, and then recursively adds the offset top from the available offset parents */
  protected _getOffsetTop (el: Element): number {
    if (!el) return 0;

    const parent = (el as HTMLElement).offsetParent;
    let yOffset = (el as HTMLElement).offsetTop;
    yOffset += this._getOffsetTop(parent);

    return yOffset;
  }

  /**
   * Returns a progress point relative to the easing function.
   * When the value falls off the 0 to 1 range, it's truncated to thar range
   */
  protected _getProgressPoint (t: number): number {
    return (this._easingFn)(t > 1 ? 1 : t < 0 ? 0 : t);
  }

  /** Gets a scrollable element's scroll top */
  protected _getScrollTop (scrollable?: Element): number {
    return (
      scrollable && scrollable !== (window as any) ?
      (scrollable as HTMLElement).scrollTop :
      -document.documentElement.getBoundingClientRect().top || document.body.scrollTop ||
      window.scrollY || document.documentElement.scrollTop || 0);
  }

  /** Kicks off a requestAnimationFrame-driven loop to animate scroll position */
  protected _scrollTo (scrollable: Element, coords?: ICoordinates, duration: number = this.scrollDuration): void {
    if (typeof scrollable !== 'object' || scrollable === null) return;
    const _coords = Object.assign<ICoordinates, ICoordinates>({ x: 0, y: 0 }, coords);
    const currentY = this._getScrollTop(scrollable);
    const diffY = _coords.y - currentY;
    let startTimestamp = null;
    const useScrollTo = typeof scrollable.scrollTo === 'function';

    if (_coords.y === currentY) return;

    const doScroll = (currentTimestamp) => {
      if (startTimestamp === null) { startTimestamp = currentTimestamp; }

      const progress = currentTimestamp - startTimestamp;
      const fractionDone = (progress / duration);
      const pointOnSineWave = this._getProgressPoint(fractionDone);
      (useScrollTo ?
        scrollable.scrollTo(0, currentY + (diffY * pointOnSineWave)) :
        scrollable.scrollTop = currentY + (diffY * pointOnSineWave));

      if (progress < duration) {
        this._getRaf()(doScroll);
      } else {
        /** Ensure we're at our destination */
        this._scrollEnd.next();
        (useScrollTo ?
          scrollable.scrollTo(_coords.x, _coords.y) :
          scrollable.scrollTop = _coords.y);
      }
    };

    this._scrollStart.next();

    this._ngZone.runOutsideAngular(() => this._getRaf()(doScroll));
  }

  /** Validates that the target is attached in the window */
  protected _validateTargetEl (el: any): boolean {
    return typeof el === 'object' && typeof el.getBoundingClientRect === 'function';
  }
}

export function SCROLL_TO_PROVIDER_FACTORY (
    parentScrollTo: SwScrollToService, ngZone: NgZone) {
  return parentScrollTo || new SwScrollToService(ngZone);
}

export const SCROLL_TO_PROVIDER = {
  provide: SwScrollToService,
  deps: [[new Optional(), new SkipSelf(), SwScrollToService], NgZone],
  useFactory: SCROLL_TO_PROVIDER_FACTORY
};
