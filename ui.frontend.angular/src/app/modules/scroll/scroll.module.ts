import { NgModule } from '@angular/core';
import { PlatformModule } from '@angular/cdk/platform';
import { SwStickyMarkerDirective } from './sticky-marker.directive';
import { STICKY_MANAGER_PROVIDER } from './sticky-manager.service';
import { SCROLL_TO_PROVIDER } from './scroll-to.service';
import { SwScrollToMarkerComponent } from './scroll-to-marker.component';

@NgModule({
  imports: [ PlatformModule],
  declarations: [SwScrollToMarkerComponent, SwStickyMarkerDirective],
  exports: [SwScrollToMarkerComponent, SwStickyMarkerDirective],
  providers: [SCROLL_TO_PROVIDER, STICKY_MANAGER_PROVIDER]
})
export class SwScrollModule {}

export { SwScrollToMarker } from './scroll-to-marker.component';
export { SwScrollToService } from './scroll-to.service';
export { SwStickyElementState, SwStickyManagerService } from './sticky-manager.service';
