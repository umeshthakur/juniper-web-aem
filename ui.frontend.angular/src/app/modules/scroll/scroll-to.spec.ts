import { ICoordinates, SwScrollToService } from './scroll-to.service';
import { first } from 'rxjs/operators';

import { createMockRaf, MockRaf } from '../../core/testing/index.spec';
import { MockNgZone } from '../../core/testing/cdk/index';

class MockScrollToService extends SwScrollToService {
  __getScrollTop (scrollable?: Element): number {
    return super._getScrollTop(scrollable);
  }
  __scrollTo (scrollable: Element, coords?: ICoordinates, duration?: number): void {
    super._scrollTo(scrollable, coords, duration);
  }
}

// @Component({
//   template: `
//     <a href="#" #top (click)="scrollTo(down)">Go down using service</a>
//     <div>
//     </div>
//     <a href="#" #down (click)="scrollTo(top)">Go up using service</a>
//   `,
//   styles: [ `
//   div{ height: 8000px; }
//   ` ]
// })
// class SwScrollToTestingComponent {
//   constructor (private _scrollTo: SwScrollToService) {}

//   scrollTo (targetEl: HTMLElement) {
//     this._scrollTo.smooth(targetEl)
//   }
// }

describe('`SwScrollToService`', () => {

  describe('in isolation', () => {
    let scrollService: SwScrollToService;
    let scrollServiceFacade: FacadeScrollToService;

    beforeEach(() => {
      scrollService = new SwScrollToService(new MockNgZone());
      scrollServiceFacade = scrollService as FacadeScrollToService;

      spyOn(scrollServiceFacade, '_scrollTo');
      scrollTo(0, 0);
    });

    describe('scroll methods', () => {
      let veryTallEl: HTMLElement;
      const veryTallElHeight = 8000;
      let targetEl: HTMLElement;
      let mockScrollService: MockScrollToService;
      let mockRaf: MockRaf;
      let started = false;
      let ended = false;

      beforeEach(() => {
        mockScrollService = new MockScrollToService(new MockNgZone());
        veryTallEl = document.body.appendChild(document.createElement('div'));
        targetEl = document.body.appendChild(document.createElement('div'));
        veryTallEl.style.position = targetEl.style.position = 'relative';
        veryTallEl.style.height = `${veryTallElHeight}px`;
        targetEl.style.height = `0px`;

        mockRaf = createMockRaf();
        mockScrollService.raf = mockRaf.raf;
        mockScrollService.scrollStart().pipe(first())
          .subscribe(() => { started = true; });
        mockScrollService.scrollEnd().pipe(first())
          .subscribe(() => { ended = true; });

        scrollTo(0, 0);
      });

      afterEach(() => {
        veryTallEl.parentNode.removeChild(veryTallEl);
        targetEl.parentNode.removeChild(targetEl);
      });

      it('should take valid `Element`s', () => {
        const el = document.createElement('div');
        /** Testing to 'object' types */
        scrollService.smooth(el);
        scrollService.jump(el);

        expect(scrollServiceFacade._scrollTo).toHaveBeenCalledTimes(2);
      });

      it('should not take invalid `Element`s', () => {
        /** Testing to 'object' types */
        scrollService.smooth({} as any);
        scrollService.jump([] as any);
        expect(scrollServiceFacade._scrollTo).not.toHaveBeenCalled();
      });

      it('should not operate without a `scrollable` argument', () => {
        mockScrollService.__scrollTo(null, { x: 10, y: 10 });
        expect(started).toBeFalsy();
        expect(ended).toBeFalsy();
      });

      it('should scroll to a given element in the window', () => {
        const contextHeight = window.innerHeight + 20;
        mockScrollService.smooth(targetEl);
        mockRaf.step({ count: 10, time: 100 });

        expect(started).toBeTruthy();
        expect(ended).toBeTruthy();
        expect(mockScrollService.__getScrollTop())
          .toBeGreaterThanOrEqual(veryTallElHeight - contextHeight);
      });

      it('should scroll to a given element inside a container', () => {
        const scrollableEl = document.body.appendChild(document.createElement('div'));
        const nestedEl = scrollableEl.appendChild(document.createElement('div'));
        const targetEl = scrollableEl.appendChild(document.createElement('div'));
        const veryTallEl = document.body.appendChild(document.createElement('div'));
        const scrollableHeight = 200;

        scrollableEl.style.height = `${scrollableHeight}px`;
        scrollableEl.style.overflow = 'scroll';

        veryTallEl.style.height = nestedEl.style.height = `${veryTallElHeight}px`;
        targetEl.style.height = `0px`;

        mockScrollService.scrollable = scrollableEl;
        mockScrollService.smooth(targetEl);
        mockRaf.step({ count: 10, time: 100 });

        expect(started).toBeTruthy();
        expect(ended).toBeTruthy();
        expect(mockScrollService.__getScrollTop(scrollableEl))
          .toBeGreaterThanOrEqual(veryTallElHeight - scrollableHeight);

        nestedEl.parentNode.removeChild(nestedEl);
        scrollableEl.parentNode.removeChild(scrollableEl);
        veryTallEl.parentNode.removeChild(veryTallEl);
        targetEl.parentNode.removeChild(targetEl);
      });
    });

    describe('easing', () => {
      it('should ease in and out', () => {
        expect(scrollServiceFacade._getProgressPoint(0)).toBe(0);
        expect(scrollServiceFacade._getProgressPoint(0.25)).toBe(1 / 16);
        expect(scrollServiceFacade._getProgressPoint(0.75)).toBe(15 / 16);
        expect(scrollServiceFacade._getProgressPoint(1)).toBe(1);
      });

      it('should only ease within the bounds', () => {
        expect(scrollServiceFacade._getProgressPoint(-1)).toBe(0);
        expect(scrollServiceFacade._getProgressPoint(2)).toBe(1);
      });
    });

    describe('scrollTop', () => {
      const globalScrollTop = 500;
      const containerScrollTop = 1000;
      let scrollableEl: HTMLElement;
      let nestedEl: HTMLElement;
      let veryTallEl: HTMLElement;

      beforeEach(() => {
        scrollableEl = document.body.appendChild(document.createElement('div'));
        nestedEl = scrollableEl.appendChild(document.createElement('div'));
        veryTallEl = document.body.appendChild(document.createElement('div'));

        [scrollableEl, veryTallEl, nestedEl]
          .forEach(el => el.style.position = 'relative');

        scrollableEl.style.height = '200px';
        scrollableEl.style.overflow = 'scroll';

        veryTallEl.style.height = nestedEl.style.height = '8000px';

        scrollTo(0, 0);
      });

      afterEach(() => {
        nestedEl.parentNode.removeChild(nestedEl);
        scrollableEl.parentNode.removeChild(scrollableEl);
        veryTallEl.parentNode.removeChild(veryTallEl);
      });

      it('should detect the global scroll top', () => {
        expect(typeof scrollServiceFacade._getScrollTop()).toBe('number');
        expect(scrollServiceFacade._getScrollTop()).toBe(0);

        scrollTo(0, globalScrollTop);

        expect(typeof scrollServiceFacade._getScrollTop()).toBe('number');
        expect(scrollServiceFacade._getScrollTop()).toBe(globalScrollTop);

        scrollTo(0, 10);

        expect(typeof scrollServiceFacade._getScrollTop()).toBe('number');
        expect(scrollServiceFacade._getScrollTop()).toBe(10);

        scrollTo(0, globalScrollTop / 2);

        expect(typeof scrollServiceFacade._getScrollTop()).toBe('number');
        expect(scrollServiceFacade._getScrollTop()).toBe(globalScrollTop / 2);
      });

      it(`should detect a container's scroll top`, () => {
        expect(typeof scrollServiceFacade._getScrollTop(scrollableEl)).toBe('number');
        expect(scrollServiceFacade._getScrollTop(scrollableEl)).toBe(0);

        scrollableEl.scrollTop = containerScrollTop;

        expect(typeof scrollServiceFacade._getScrollTop(scrollableEl)).toBe('number');
        expect(scrollServiceFacade._getScrollTop(scrollableEl)).toBe(containerScrollTop);
      });
    });

  });

  describe('as a service', () => {

    // beforeEach(async(() => {
    //   TestBed.configureTestingModule({
    //     declarations: [SwScrollToTestingComponent],
    //     providers: [SwScrollToService]
    //   }).compileComponents();
    // }))

  });

});

class FacadeScrollToService extends SwScrollToService {
  get _easingFn (): Function {
    return this._easingFn;
  }
  _getOffsetTop (el: Element): number {
    return super._getOffsetTop(el);
  }
  _getProgressPoint (t: number): number {
    return super._getProgressPoint(t);
  }
  _getScrollTop (scrollable?: Element): number {
    return super._getScrollTop(scrollable);
  }
  _scrollTo (scrollable: Element, coords?: ICoordinates, duration?: number): void {
    super._scrollTo(scrollable, coords, duration);
  }
}
