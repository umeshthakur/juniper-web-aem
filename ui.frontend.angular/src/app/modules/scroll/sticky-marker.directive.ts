import { AfterViewInit, Directive, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { SwStickyElementState, SwStickyManagerService } from './sticky-manager.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { DataType, SwInputProp } from '../../core/validation';

export type stickyMarkerOrientationType = 'top' | 'bottom';
export const stickyMarkerDefaultOrientation: stickyMarkerOrientationType = 'top';
export const stickyMarkerOrientationRegex = /^(top|bottom)$/;

@Directive({
  selector: '[swStickyMarker]'
})
export class SwStickyMarkerDirective implements AfterViewInit, OnDestroy {
  constructor (private _element: ElementRef, private _stickyManager: SwStickyManagerService) {}

  /** An identifier of a orientation which can be top or bottom */
  @SwInputProp(DataType.string, stickyMarkerDefaultOrientation, { pattern: stickyMarkerOrientationRegex.source })
  @Input()
  orientation: stickyMarkerOrientationType;

  @Input()
  set enabled (enabled: boolean) {
    this._enabled = enabled;
    this._stickyManager.setEnabled(this._element, this.enabled);
  }
  get enabled (): boolean {
    return this._enabled;
  }
  private _enabled = true;

  @Output() fixedChange = new EventEmitter<SwStickyElementState>();

  private readonly _destroy$ = new Subject<void>();

  ngAfterViewInit () {
    this._stickyManager.register(this._element, this.enabled, this.orientation)
      .pipe(takeUntil(this._destroy$))
      .subscribe(s => this.fixedChange.emit(s));
  }

  ngOnDestroy () {
    this._stickyManager.unregister(this._element);
  }
}
