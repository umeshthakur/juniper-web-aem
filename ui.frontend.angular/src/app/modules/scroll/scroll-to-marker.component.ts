import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostBinding
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SwScrollToService } from './scroll-to.service';
import { SwStickyManagerService } from './sticky-manager.service';

export abstract class SwScrollToMarker {
  abstract scrollTo (): void;
}

@Component({
  selector: 'sw-scroll-to-marker',
  template: ``,
  styles: [
    `
      :host{
        display: block;
      }
    `
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: SwScrollToMarker, useExisting: forwardRef(() => SwScrollToMarkerComponent) }
  ]
})
export class SwScrollToMarkerComponent implements AfterViewInit, SwScrollToMarker {
  constructor (
    private _element: ElementRef,
    private _scrollTo: SwScrollToService,
    private _stickyManager: SwStickyManagerService,
    private _cd: ChangeDetectorRef
  ) {}

  @HostBinding('style.top.px')
  get offset (): number {
    // This padding is an aesthetic-driven value to have a bit of breathing room
    // between the sticky elements and the actual marker.
    const padding = 16;
    return this._stickyHeight * -1 - padding;
  }

  private readonly _destroy$ = new Subject<void>();
  private _stickyHeight = 0;

  ngAfterViewInit () {
    this._stickyManager.overallHeight$.pipe(takeUntil(this._destroy$)).subscribe(h => {
      const stickyHeight = h;

      if (stickyHeight !== this._stickyHeight) {
        this._stickyHeight = stickyHeight;
        this._cd.markForCheck();
        this._cd.detectChanges();
      }
    });
  }

  scrollTo () {
    this._scrollTo.smooth(this._element.nativeElement);
  }
}
