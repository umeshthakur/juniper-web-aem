import { NgModule } from '@angular/core';
import { SwImageDirective } from './image.directive';

@NgModule({
  imports: [],
  exports: [SwImageDirective],
  declarations: [SwImageDirective]
})
export class SwImageDirectiveModule { }

export { SwImageDirective };
