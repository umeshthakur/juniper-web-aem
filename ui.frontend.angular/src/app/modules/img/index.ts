import { NgModule } from '@angular/core';
import { SwImageDirective, SwImageDirectiveModule } from './image.module';

@NgModule({
  imports: [SwImageDirectiveModule],
  exports: [SwImageDirective],
  declarations: []
})
export class SwImageModule { }
