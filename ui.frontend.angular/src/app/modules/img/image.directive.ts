import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  Renderer2
} from '@angular/core';
import { SwUrl } from '../../util/index';

const directiveId = 'swImage';

@Directive({
  selector: '[' + directiveId + ']'
})
export class SwImageDirective {
  private readonly _defaultSrc: string = '';
  private _src: string = this._defaultSrc;
  private _imageUrl: string;

  constructor (private _elementRef: ElementRef, private _renderer: Renderer2) {
    if (window && 'IntersectionObserver' in window) {
      const obs = new IntersectionObserver(entries => {
        entries.forEach(({ isIntersecting }) => {
          if (isIntersecting) {
            // tslint:disable-next-line:max-line-length
            this._renderer.setAttribute(this._elementRef.nativeElement, 'src', (typeof this._imageUrl === 'string' && this._imageUrl !== '' ? `${this._imageUrl}` : this._defaultSrc));
            obs.unobserve(this._elementRef.nativeElement);
          }
        });
      });
      obs.observe(this._elementRef.nativeElement);
    }
  }

  /**
   * [src]="_src"
   */
  @HostBinding('src')
  get src () { return this._src; }

  set src (imageUrl: string) {
    this._src = (typeof imageUrl === 'string' && imageUrl !== '' ? `${imageUrl}` : this._defaultSrc);
  }
  /**
   * Read the directive's value and set it
   * Same as adding `${directiveSetter}: ${directiveId}` on the input metadata
   */
  @Input(directiveId)
  set source (imageUrl: string) {
    const _imageUrl = new SwUrl(imageUrl);
    (window && 'IntersectionObserver' in window) ? this._imageUrl = _imageUrl.url : this._src = _imageUrl.url;
  }
}
