import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SwImageDirective } from './image.directive';

function testBgImageUrl (img: string, subject: string): boolean {
  /** Escape conflicting chars with RegExp */
  img = img.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
  const imgRegexp = new RegExp(`url\(.+${img}.+\)`);

  return imgRegexp.test(subject);
}

@Component({
  template: `<div [swImage]="testBackground"></div>`
})
class SwImageTestComponent {
  testBackground: string;
}

describe(`SwImageDirective`, () => {
  const baseImg = 'https://example.com/path/to/image/my-image.jpg';

  describe('in isolation', () => {
    let c: SwImageDirective;

    beforeEach(() => {
      c = new SwImageDirective();
    });

    it('should format URLs to conform with css `background-image`', () => {
      c.src = `${baseImg}`;
      expect(c.src).toBe(`url(${baseImg})`);
    });

    it('should handle being passed rogue values', () => {
      c.src = 10 as any;
      expect(c.src).toBe('');
    });

  });

  describe('integrated', () => {
    let fixture: ComponentFixture<SwImageTestComponent>;
    let testComponent: SwImageTestComponent;
    let debugDirective: DebugElement;
    let nativeDirectiveHost: HTMLElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [
          SwImageTestComponent,
          SwImageTestComponent
        ]
      });
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(SwImageTestComponent);
      fixture.detectChanges();

      testComponent = fixture.debugElement.componentInstance;
      debugDirective = fixture.debugElement.query(By.directive(SwImageTestComponent));
      nativeDirectiveHost = debugDirective.nativeElement;
    });

    it('should set backgroundImage on host', () => {
      testComponent.testBackground = `${baseImg}`;
      fixture.detectChanges();
      expect(
        testBgImageUrl(
          `${baseImg}`,
          nativeDirectiveHost.style.backgroundImage
        )
      ).toBe(true);
    });

  });

});
