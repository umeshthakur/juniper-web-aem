import { NgModule } from '@angular/core';

import { SwPeripheralDialogService } from './peripheral-dialog.service';
import { SwPageDataModule } from '../pageData/index';
import { SwEditorialDialogModule } from '../../jnpr-common/editorial/index';
import { SwPeripheralDialogPipe } from './peripheral-dialog.pipe';

@NgModule({
  declarations: [SwPeripheralDialogPipe],
  exports: [SwPeripheralDialogPipe],
  imports: [SwEditorialDialogModule, SwPageDataModule],
  providers: [SwPeripheralDialogService]
})
export class SwPeripheralDialogModule {}
