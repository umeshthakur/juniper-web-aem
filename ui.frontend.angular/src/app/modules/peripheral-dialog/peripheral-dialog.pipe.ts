import { Pipe, PipeTransform } from '@angular/core';
import { peripheralDialogRegex } from './peripheral-dialog-regexp';

@Pipe({ name: 'swPeripheralDialog', pure: true })
export class SwPeripheralDialogPipe implements PipeTransform {
  transform (href: string) {
    return peripheralDialogRegex.test(href) ? `#${peripheralDialogRegex.exec(href)[1]}` : href;
  }
}
