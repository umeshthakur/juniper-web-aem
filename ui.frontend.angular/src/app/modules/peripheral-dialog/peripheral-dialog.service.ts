import { Injectable, Type } from '@angular/core';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

import { SwPageData } from '../pageData/index';
import { SwDialog, SwDialogRef } from '../../jnpr-common/dialog/index';
import { SwEditorialDialogComponent } from '../../jnpr-common/editorial/dialog/editorial-dialog.component';
import { SwListDialogComponent } from '../../jnpr-common/list/dialog/list-dialog.component';
import { SwPeripheralDialogComponentObj } from '../../core/base/index';
import { peripheralDialogRegex } from './peripheral-dialog-regexp';

interface SwPeripheralDialog<T> {
  title: string;
  config: Partial<T>;
}

const SwPeripheralDialogMap: {
  [selector: string]: Type<SwPeripheralDialog<{}>>
} = {
  'sw-editorial-dialog': SwEditorialDialogComponent,
  'sw-list-dialog': SwListDialogComponent
};

@Injectable()
export class SwPeripheralDialogService {

  constructor (private _pageData: SwPageData, private _dialog: SwDialog) {
    this._pageData.fetchData()
      .pipe(map(data => data.peripheralDialogs))
      .subscribe(this._peripherals);
  }

  private _peripherals = new BehaviorSubject<SwPeripheralDialogComponentObj[]>(null);

  test (uid: string) {
    return peripheralDialogRegex.test(uid);
  }

  open (uid: string, title: string = ''): SwDialogRef<{}> | void {
    const id = peripheralDialogRegex.exec(uid)[1];

    if (!id) return undefined;

    const dialog = this._seek(id);

    if (!dialog) return undefined;
    if (!(dialog.selector in SwPeripheralDialogMap)) {
      console.warn(`No peripheral dialog available with selector '${dialog.selector}'`);
      return undefined;
    }

    const data: SwPeripheralDialog<{}> = {
      title,
      config: dialog.properties
    };

    return this._dialog.open(SwPeripheralDialogMap[dialog.selector], {
      data,
      height: '100vh'
    });
  }

  private _seek (uid: string): SwPeripheralDialogComponentObj {
    const dialog = this._peripherals.getValue().filter(dialog => dialog.uid === uid)[0];

    if (!dialog) console.warn(`No peripheral dialog found with UID '${uid}'`);
    return dialog;
  }
}
