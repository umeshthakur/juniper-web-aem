/**
 * Matches a string that starts with `dialog://` followed by a letter and ending with a letter.
 * However, then the expression is executed, it will strip the `dialog://` at the beginning
 * hello    <== Invalid
 * dialog://he     <== Valid
 * dialog://hello  <== Valid
 * dialog://he-llo <== Valid
 * dialog://-hello <== Invalid
 * dialog://hello- <== Invalid
 */
export const peripheralDialogRegex = /^dialog\:\/\/(\w[\w-]*\w)$/;
