import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({ selector: '[swFocusRingHelper]' })
export class SwFocusRingHelperDirective {
  /** Whether the button has focus from the keyboard (not the mouse). Used for class binding. */
  @HostBinding('class.sw-focus-ring')
  _isKeyboardFocused: boolean = false;

  /** Whether a mousedown has occurred on this element in the last 100ms. */
  _isMouseDown: boolean = false;

  @HostListener('mousedown')
  _setMousedown () {
    /**
     * We only *show* the focus style when focus has come to the button via the keyboard.
     * Without doing this, the button continues to look :active after clicking.
     * @see http://marcysutton.com/button-focus-hell/
     */
    this._isMouseDown = true;
    setTimeout(() => {
      this._isMouseDown = false;
    }, 100);
  }

  @HostListener('focus')
  _setKeyboardFocus () {
    this._isKeyboardFocused = !this._isMouseDown;
  }

  @HostListener('blur')
  _removeKeyboardFocus () {
    this._isKeyboardFocused = false;
  }
}
