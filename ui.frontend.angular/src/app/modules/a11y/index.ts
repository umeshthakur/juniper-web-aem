import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwFocusRingHelperDirective } from './focus-ring-helper.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [SwFocusRingHelperDirective],
  exports: [SwFocusRingHelperDirective]
})
export class SwA11yModule {}
