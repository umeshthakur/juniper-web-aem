import { NgModule, Type } from '@angular/core';
import { SwBaseComponent } from '../app/core/base/index';
import {
  JnprDcSelectorComponent, JnprDcSelectorModule,
  SwProductCompareComponent, SwProductCompareModule,
  SwNotesComponent, SwNotesModule
} from './index';

const SW_COMPONENT_EXPORT_MODULES = [
  JnprDcSelectorModule,
  SwProductCompareModule
];

const SW_COMPONENT_IMPORT_MODULES = [
  JnprDcSelectorModule.fromApi(),
  SwProductCompareModule,
  SwNotesModule
];

@NgModule( {
  imports: [ SW_COMPONENT_IMPORT_MODULES ],
  exports: [ SW_COMPONENT_EXPORT_MODULES ]
} )
export class SwMainComponentLibModule {
  static getTypeStore (): Type<SwBaseComponent>[] {
    return [
      JnprDcSelectorComponent,
      SwProductCompareComponent,
      SwNotesComponent
    ];
  }
}
