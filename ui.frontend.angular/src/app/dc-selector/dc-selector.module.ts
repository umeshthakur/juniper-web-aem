import { ModuleWithProviders, NgModule, DoBootstrap, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { SwButtonModule } from '../jnpr-common/button/button.component';
import { SwDialogModule } from '../jnpr-common/dialog/index';
import { SwIconModule } from '../jnpr-common/icon/index';
import { SwMarqueeModule } from '../jnpr-common/marquee/marquee.component';
import { SwToolbarModule } from '../jnpr-common/toolbar/toolbar.component';
import { SwBackgroundMediaModule } from '../modules/backgroundMedia/index';
import { SwObjectPipesModule } from '../modules/obj-pipes/obj-pipes.module';
import { SwScrollModule } from '../modules/scroll/index';

import { SwDcSelectorApiDataService, SwDcSelectorDataService } from './data-service/index';
import { SELECTOR_STORE_PROVIDER } from './dc-selector-store.service';
import { JnprDcSelectorComponent } from './dc-selector.component';
import { SwDcSelectorDecisionComponent } from './dc-selector-decision.component';
import { MatCardModule } from '@angular/material/card';
import { MatSliderModule } from '@angular/material/slider';
import { SwImageModule } from '../modules/img';
import { createCustomElement } from '@angular/elements';

@NgModule( {
  imports: [
    BrowserModule,
    CommonModule,
    SwButtonModule,
    SwDialogModule,
    SwIconModule,
    SwImageModule,
    SwMarqueeModule,
    SwToolbarModule,
    SwObjectPipesModule,
    SwBackgroundMediaModule,
    HttpClientModule,
    SwScrollModule,
    MatCardModule,
    MatSliderModule
  ],
  declarations: [
    JnprDcSelectorComponent,
    SwDcSelectorDecisionComponent
  ],
  exports: [
    JnprDcSelectorComponent
  ],
  entryComponents: [
    JnprDcSelectorComponent
  ],
  providers: [ SELECTOR_STORE_PROVIDER ],
  bootstrap: [ JnprDcSelectorComponent ]
} )
export class JnprDcSelectorModule implements DoBootstrap {

  constructor(private injector: Injector) {
    customElements.define('jnpr-dc-selector', createCustomElement(JnprDcSelectorComponent, { injector: this.injector }));
  }

  static fromApi (): ModuleWithProviders<any> {
    return {
      ngModule: JnprDcSelectorModule,
      providers: [
        { provide: SwDcSelectorDataService, useClass: SwDcSelectorApiDataService }
      ]
    };
  }

  ngDoBootstrap() {}
}

export { JnprDcSelectorComponent } from './dc-selector.component';
