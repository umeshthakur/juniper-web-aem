// tslint:disable
export const npsMockResultsData = [
  {
    name: "EX2300",
    label: "EX2300",
    url: "https://www.juniper.net/us/en/products-services/switching/ex-series/ex2300/index.page#overview",
    imageUrl: "https://www.juniper.net/assets/img/products/image-library/ex-series/ex2300-24p/ex2300-24t-dc-frontwtop-low.png"
  }, {
    name: "EX3300",
    label: "EX3300",
    url: "https://www.juniper.net/us/en/products-services/switching/ex-series/ex3300/index.page#overview",
    imageUrl: "https://www.juniper.net/assets/img/products/image-library/ex-series/ex3300-48p/ex3300-48p-frontwtop-low.png"
  }, {
    name: "EX3400",
    label: "EX3400",
    url: "https://www.juniper.net/us/en/products-services/switching/ex-series/ex3400/index.page#overview",
    imageUrl: "https://www.juniper.net/assets/img/products/image-library/ex-series/ex3400-48t/ex3400-48t-front-low.png"
  }
]
