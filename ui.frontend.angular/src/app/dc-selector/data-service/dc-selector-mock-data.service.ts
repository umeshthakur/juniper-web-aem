import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  KeyedSurveyDataNodes,
  SW_HTTP_REQUEST_STATE,
  SwHttpResponseSnapshot,
  SwDcSelectorDataService
} from './dc-selector-data.service';
import { ISurveyData, SurveyDataNode } from './survey-data-node';
import { LsResultDataNode, ResultDataNode } from './result-data-node';

/**
 * NOTE: This is necessary because, at this time, Webpack tree shaking
 * doesn't seem to be smart enough to remove these imports, which simply
 * add to much bloat. Instead they're replaced with the below empty objects.
 * DO NOT commit changes with these files imported
 */
// import { npsSwitchesData as surveyMockData } from './data/nps-survey-mock';
// import { npsMockResultsData as resultMockData } from './data/nps-results-mock';

/**
 * Provides Data Center Selector data.
 */
@Injectable()
export class SwDcSelectorMockDataService extends SwDcSelectorDataService {

  resultMockData = [] as ResultDataNode[];
  surveyMockData = {} as ISurveyData;
  lsResultsMockData = [] as LsResultDataNode[];

  fetchLsResultData (): Observable<SwHttpResponseSnapshot<LsResultDataNode[]>> {
    /** Creates Observable for LS result data */
    const state = this.lsResultsMockData.length > 0 ? SW_HTTP_REQUEST_STATE.OK : SW_HTTP_REQUEST_STATE.NO_RESULTS;
    const ret: SwHttpResponseSnapshot<LsResultDataNode[]> = { state: state, value: this.lsResultsMockData };
    return of(this._cloneData(ret));
  }

  fetchResultData (): Observable<SwHttpResponseSnapshot<ResultDataNode[]>> {
    /** Creates Observable for result data */
    const state = this.resultMockData.length > 0 ? SW_HTTP_REQUEST_STATE.OK : SW_HTTP_REQUEST_STATE.NO_RESULTS;
    const ret: SwHttpResponseSnapshot<ResultDataNode[]> = { state: state, value: this.resultMockData };
    return of(this._cloneData(ret));
  }

  fetchSurveyData (): Observable<SwHttpResponseSnapshot<KeyedSurveyDataNodes>> {
    /**
     * Creates Observable for survey data
     * If importing from JSON, make sure to reference interface (surveyMockData as ISurveyData)
     */
    return of(this.surveyMockData).pipe(
      /** Map response to a KeyedSurveyDataNodes state snapshot */
      map<ISurveyData, SwHttpResponseSnapshot<KeyedSurveyDataNodes>>(res => {
        const surveyData: KeyedSurveyDataNodes = {};
        let state = SW_HTTP_REQUEST_STATE.NO_RESULTS;
        if (!!res.data && !!res.lookup && typeof res.data === 'object' && typeof res.lookup === 'object') {
          Object.keys(res.data)
            .forEach(k => surveyData[k] = new SurveyDataNode(res.data[k], res.lookup));
          state = SW_HTTP_REQUEST_STATE.OK;
        }
        return { state: state, value: this._cloneData(surveyData) };
      })
    );
  }
}
