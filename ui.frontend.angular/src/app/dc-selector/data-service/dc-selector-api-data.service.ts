import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { finalize, first, map, share, tap } from 'rxjs/operators';

import {
  KeyedSurveyDataNodes,
  SURVEY_DELAY$,
  SW_HTTP_REQUEST_STATE,
  SwHttpResponseSnapshot,
  SwDcSelectorDataService
} from './dc-selector-data.service';
import { ISurveyData, SurveyDataNode } from './survey-data-node';
import { LsResultDataNode, ResultDataNode } from './result-data-node';

/**
 * Provides Data Center Selector data.
 */
@Injectable()
export class SwDcSelectorApiDataService extends SwDcSelectorDataService {

  constructor (private _http: HttpClient) {
    super();
  }

  /** Returns additional result content LS */
  fetchLsResultData (lsresultPath?: string): Observable<SwHttpResponseSnapshot<LsResultDataNode[]>> {

    if (this._lssurveyStore.has(lsresultPath)) {
      return of(this._cloneData(this._lssurveyStore.get(lsresultPath)));
    }

    return new Observable<SwHttpResponseSnapshot<LsResultDataNode[]>>(
      observer => {
        /** Prepare for possible decision state to SLOW */
        const surveyDelay = SURVEY_DELAY$.subscribe(state => observer.next({ state: state, value: [] }));
        /** Set decision state to PENDING */
        observer.next({ state: SW_HTTP_REQUEST_STATE.PENDING, value: [] });
        /** Request survey data */
        this._fetchUrl(lsresultPath).pipe(
          first(),
          /** Map response to a LsResultDataNode[] state snapshot */
          map<LsResultDataNode[], SwHttpResponseSnapshot<LsResultDataNode[]>>(res => {
            const resultData = (
              Array.isArray(res) ?
                res.map(resultNode => new LsResultDataNode(resultNode)) :
                []
            );
            const state = resultData.length > 0 ? SW_HTTP_REQUEST_STATE.OK : SW_HTTP_REQUEST_STATE.NO_RESULTS;
            return { state: state, value: resultData };
          }),
          /** Store result in store by URL */
          tap(resSnap => this._lssurveyStore.set(lsresultPath, resSnap)),
          /**
           * Regardless of what happens,
           * a) Detach from the delay stream
           * b) Complete the observer
           */
          finalize(() => {
            if (surveyDelay) surveyDelay.unsubscribe();
            observer.complete();
          })
        ).subscribe(
          resSnap => observer.next(this._cloneData(resSnap)),
          _ => observer.next({ state: SW_HTTP_REQUEST_STATE.ERROR, value: [] })
        );
      });
  }

  fetchResultData (resultPath: string = '', resultFilter: string = ''): Observable<SwHttpResponseSnapshot<ResultDataNode[]>> {
    const resultUrl: string = this._trimSlashAtEnd(resultPath).concat('/', resultFilter);

    if (this._resultStore.has(resultUrl)) {
      return of(this._cloneData(this._resultStore.get(resultUrl)));
    }

    return new Observable<SwHttpResponseSnapshot<ResultDataNode[]>>(
      observer => {
        /** Prepare for possible decision state to SLOW */
        const surveyDelay = SURVEY_DELAY$.subscribe(state => observer.next({ state: state, value: [] }));
        /** Set decision state to PENDING */
        observer.next({ state: SW_HTTP_REQUEST_STATE.PENDING, value: [] });
        /** Request survey data */
        this._fetchUrl(resultUrl).pipe(
          first(),
          /** Map response to a ResultDataNode[] state snapshot */
          map<ResultDataNode[], SwHttpResponseSnapshot<ResultDataNode[]>>(res => {
            const resultData = (
              Array.isArray(res) ?
                res.map(resultNode => new ResultDataNode(resultNode))
                  .filter(resultNode => ResultDataNode.isValid(resultNode)) :
                []
            );
            const state = resultData.length > 0 ? SW_HTTP_REQUEST_STATE.OK : SW_HTTP_REQUEST_STATE.NO_RESULTS;
            return { state: state, value: resultData };
          }),
          /** Store result in store by URL */
          tap(resSnap => this._resultStore.set(resultUrl, resSnap)),
          /**
           * Regardless of what happens,
           * a) Detach from the delay stream
           * b) Complete the observer
           */
          finalize(() => {
            if (surveyDelay) surveyDelay.unsubscribe();
            observer.complete();
          })
        ).subscribe(
          resSnap => observer.next(this._cloneData(resSnap)),
          _ => observer.next({ state: SW_HTTP_REQUEST_STATE.ERROR, value: [] })
        );
      });
  }

  fetchSurveyData (surveyPath: string = ''): Observable<SwHttpResponseSnapshot<KeyedSurveyDataNodes>> {
    const surveyUrl: string = this._trimSlashAtEnd(surveyPath);

    if (this._surveyStore.has(surveyUrl)) {
      return of(this._cloneData(this._surveyStore.get(surveyUrl)));
    }

    return new Observable<SwHttpResponseSnapshot<KeyedSurveyDataNodes>>(
      observer => {
        /** Prepare for possible decision state to SLOW */
        const surveyDelay = SURVEY_DELAY$.subscribe(state => observer.next({ state: state, value: {} }));
        /** Set decision state to PENDING */
        observer.next({ state: SW_HTTP_REQUEST_STATE.PENDING, value: {} });
        /** Request survey data */
        this._fetchUrl(surveyUrl).pipe(
          first(),
          /** Map response to a KeyedSurveyDataNodes state snapshot */
          map<ISurveyData, SwHttpResponseSnapshot<KeyedSurveyDataNodes>>(res => {
            const surveyData: KeyedSurveyDataNodes = {};
            let state = SW_HTTP_REQUEST_STATE.NO_RESULTS;
            if (!!res.data && !!res.lookup && typeof res.data === 'object' && typeof res.lookup === 'object') {
              Object.keys(res.data)
                .forEach(k => surveyData[k] = new SurveyDataNode(res.data[k], res.lookup));
              state = SW_HTTP_REQUEST_STATE.OK;
            }
            return { state: state, value: surveyData };
          }),
          /** Store result in store by URL */
          tap(resSnap => this._surveyStore.set(surveyUrl, resSnap)),
          /**
           * Regardless of what happens,
           * a) Detach from the delay stream
           * b) Complete the observer
           */
          finalize(() => {
            if (surveyDelay) surveyDelay.unsubscribe();
            observer.complete();
          })
        ).subscribe(
          resSnap => observer.next(resSnap),
          _ => observer.next({ state: SW_HTTP_REQUEST_STATE.ERROR, value: {} })
        );
      });
  }

  private _fetchUrl (url: string) {
    /**
     * Avoid sending duplicate requests in the case where there's
     * already a fetch in progress for the requested URL.
     */
    if (this._inProgressUrlFetches.has(url)) {
      return this._inProgressUrlFetches.get(url);
    }

    /**
     * It's necessary to call share() on the Observable returned
     * by http.get() so that multiple subscribers don't cause
     * multiple XHRs.
     */
    const req = this._http.get(url).pipe(
      finalize(() => {
        this._inProgressUrlFetches.delete(url);
      }),
      share()
    );
    this._inProgressUrlFetches.set(url, req);
    return req;
  }

  private _trimSlashAtEnd (str: string) {
    return str.trim().replace(/(.*)\/$/, '$1');
  }
}
