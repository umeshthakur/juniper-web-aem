import { async, inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { npsSwitchesData as surveyMockData } from './data/nps-survey-mock';
import { npsMockResultsData as resultMockData } from './data/nps-results-mock';
import { KeyedSurveyDataNodes, SW_HTTP_REQUEST_STATE, SwHttpResponseSnapshot } from './dc-selector-data.service';
import { SwDcSelectorApiDataService } from './dc-selector-api-data.service';

const firstValueAfterPending = <T>() =>
  (source: Observable<SwHttpResponseSnapshot<T>>): Observable<T> => {
    return source.pipe(
      first(snap => snap.state !== SW_HTTP_REQUEST_STATE.PENDING),
      map(snap => snap.value)
    );
  };

describe(`SwDcSelectorApiDataService`, () => {

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        providers: [SwDcSelectorApiDataService],
        imports: [HttpClientTestingModule]
      });
    })
  );

  let apiService: SwDcSelectorApiDataService;
  let httpMock: HttpTestingController;

  beforeEach(
    inject(
      [SwDcSelectorApiDataService, HttpTestingController],
      (service: SwDcSelectorApiDataService, http: HttpTestingController) => {
        apiService = service;
        httpMock = http;
      }
    )
  );

  afterEach(() => {
    httpMock.verify();
  });

  describe(`Normal use case results`, () => {
    describe(`Fetching result data`, () => {
      const resultDataApi = '/api/result/';

      it(`should create an object to store properties`, () => {
        apiService
          .fetchResultData(resultDataApi)
          .pipe(firstValueAfterPending())
          .subscribe(results => {
            /** Make sure object for result data exists */
            expect(Array.isArray(results)).toBeTruthy('Expected results to be Array');
            expect(results).not.toBeNull(`Expected result object to not be null`);
          });

        const req = httpMock.expectOne(resultDataApi);
        expect(req.request.method).toBe('GET');
        req.flush(resultMockData);
      });

      it(`should map properties to 'ResultNode'`, () => {
        apiService
          .fetchResultData(resultDataApi)
          .pipe(firstValueAfterPending())
          .subscribe(results => {
            results.forEach(result => {
              /** Confirm object data matches expected `ResultNode` properties */
              expect(typeof result.name).toBe('string');
              expect(typeof result.label).toBe('string');
              expect(typeof result.url).toBe('string');
              expect(typeof result.imageUrl).toBe('string');
            });
          });

        const req = httpMock.expectOne(resultDataApi);
        expect(req.request.method).toBe('GET');
        req.flush(resultMockData);
      });
    });

    describe(`Fetching survey data`, () => {
      const surveyDataApi = '/api/survey';

      it(`should create an object to store properties`, () => {
        apiService
          .fetchSurveyData(surveyDataApi)
          .pipe(firstValueAfterPending())
          .subscribe(survey => {
            /** Make sure object for survey data exists */
            expect(typeof survey).toBe('object');
            expect(survey).not.toBeNull(`Expected survey object to not be null`);
          });

        const req = httpMock.expectOne(surveyDataApi);
        expect(req.request.method).toBe('GET');
        req.flush(surveyMockData);
      });

      it(`should use 'lookup[]' to map number properties to 'SurveyNode' string properties`, () => {
        apiService
          .fetchSurveyData(surveyDataApi)
          .pipe(firstValueAfterPending())
          .subscribe((surveyData: KeyedSurveyDataNodes) => {
            for (const index in surveyData) {
              /** Confirm object data has been converted to expected property types */
              expect(typeof surveyData[index].label).toBe(`string`);
              expect(typeof surveyData[index].options).toBe(`object`);
              expect(surveyData[index].options)
                .not.toBeNull(`Expected 'options' property in survey node (idx: ${surveyData[index]}) to not be null`);

              // `toolTip` is an optional property, so its type only needs to be checked if it exists (is defined)
              if (surveyData[index].toolTip !== undefined) {
                expect(typeof surveyData[index].toolTip).toBe('string');
              }
            }
          });

        const req = httpMock.expectOne(surveyDataApi);
        expect(req.request.method).toBe('GET');
        req.flush(surveyMockData);
      });
    });
  });

  describe(`Edge use case results`, () => {
    describe(`Fetching result data`, () => {
      describe(`Data not in array or is missing`, () => {
        const resultDataApi = '/api/result/';

        it(`should return an empty object when given an object literal`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(Array.isArray(results)).toBeTruthy('Expected results to be Array');
              expect(results.length).toBe(0);
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush({ propOne: [], propTwo: 'string' });
        });

        it(`should return an empty object when given a string`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(Array.isArray(results)).toBeTruthy('Expected results to be Array');
              expect(results.length).toBe(0);
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush('string');
        });

        it(`should return an empty object when given a number`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(Array.isArray(results)).toBeTruthy('Expected results to be Array');
              expect(results.length).toBe(0);
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(5);
        });

        // Skipping until https://github.com/angular/angular/issues/20690 is resolved
        xit(`should return an empty object when given a boolean`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(Array.isArray(results)).toBeTruthy('Expected results to be Array');
              expect(results.length).toBe(0);
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(true);
        });

        it(`should not map empty data array`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              results.forEach(result => {
                expect(result.name).toBeUndefined(`Expected 'result.name' to be undefined`);
                expect(result.label).toBeUndefined(`Expected 'result.label' to be undefined`);
                expect(result.url).toBeUndefined(`Expected 'result.url' to be undefined`);
                expect(result.imageUrl).toBeUndefined(`Expected 'result.imageUrl' to be undefined`);
              });
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush([]);
        });
      });

      describe(`Nodes or properties are incorrect type`, () => {
        let wrongNodeArray: [ object, number, boolean, object, string, object ];
        let wrongPropArray: [ object, object, object, object, object ];
        const resultDataApi = '/api/result/';

        beforeEach(() => {
          wrongNodeArray = [
            { name: 'string', label: 'string', url: 'string', imageUrl: 'string' },
            5, true, ['array'], 'string',
            { name: 'string', label: 'string', url: 'string', imageUrl: 'string' }
          ];

          wrongPropArray = [
            { name: 'string', label: 'string', url: 'string', imageUrl: 'string' },
            { name: 'string', label: 4, url: 'string', imageUrl: 'string' },
            { label: 4, url: 'string', imageUrl: 'string' },
            { name: 'string', label: 'string' },
            { name: 'string', label: 'string', url: 'string', imageUrl: 'string' }
          ];
        });

        it(`should not map incorrectly typed nodes`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(results.length).toBe(2, `Expected invalid nodes to be filtered`);
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(wrongNodeArray);
        });

        it(`should not map nodes with incorrectly typed properties`, () => {
          apiService
            .fetchResultData(resultDataApi)
            .pipe(firstValueAfterPending())
            .subscribe(results => {
              expect(results.length).toBe(2, `Expected invalid nodes to be filtered`);

              results.forEach(result => {
                expect(result.name).toBeDefined(`Expected 'name' property to be defined`);
                expect(result.name).not.toBeNull(`Expected 'name' property to not be null`);
                expect(result.label).toBeDefined(`Expected 'label' property to be defined`);
                expect(result.label).not.toBeNull(`Expected 'label' property to not be null`);
                expect(result.url).toBeDefined(`Expected 'url' property to be defined`);
                expect(result.url).not.toBeNull(`Expected 'url' property to not be null`);
                expect(result.imageUrl).toBeDefined(`Expected 'imageUrl' property to be defined`);
                expect(result.imageUrl).not.toBeNull(`Expected 'imageUrl' property to not be null`);
              });
            });

          const req = httpMock.expectOne(resultDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(wrongPropArray);
        });
      });
    });

    describe(`Fetching survey data`, () => {
      const surveyDataApi = '/api/survey';

      describe(`Data not in object or is missing`, () => {
        it(`should return an empty object when given an array`, () => {
          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(['array']);
        });

        it(`should return an empty object when given a string`, () => {
          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush('string');
        });

        it(`should return an empty object when given a number`, () => {
          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(5);
        });

        // Skipping until https://github.com/angular/angular/issues/20690 is resolved
        xit(`should return an empty object when given a boolean`, () => {
          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(true);
        });

        it(`should return an empty object when given null`, () => {
          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(surveyData.constructor).not.toBe(null);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(null);
        });
      });

      describe(`Properties are incorrect type or missing`, () => {
        it(`should return an empty object if 'lookup[]' is missing`, () => {
          const missingLookup = {
            data: {
              0: { options: { 1: { optionName: 14 }, 2: { optionName: 13 } }, label: 2 }
            }
          };

          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(surveyData.constructor).not.toBe(null);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(missingLookup);
        });

        it(`should return an empty object if 'lookup[]' is null`, () => {
          const nullLookup = {
            data: {
              0: { options: { 1: { optionName: 14 }, 2: { optionName: 13 } }, label: 2 }
            },
            lookup: null
          };

          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(surveyData.constructor).not.toBe(null);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(nullLookup);
        });

        it(`return an empty object if 'data' is missing`, () => {
          const missingData = {
            lookup: { 0: 'String 1', 2: 'String 2', 4: 'String 3' }
          };

          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(surveyData.constructor).not.toBe(null);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(missingData);
        });

        it(`return an empty object if 'data' is null`, () => {
          const nullData = {
            data: null,
            lookup: { 0: 'String 1', 2: 'String 2', 4: 'String 3' }
          };

          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(surveyData.constructor).toBe(Object);
              expect(surveyData.constructor).not.toBe(null);
              expect(Object.keys(surveyData).length).toBe(0);
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(nullData);
        });

        it(`should return valid SurveyNodes regardless of incorrectly typed properties`, () => {
          const wrongProps = {
            data: {
              0: { options: { 1: { optionName: 99 }, 2: { optionName: 3 } }, label: {} },
              1: { options: { 3: { optionName: '1' }, 4: { optionName: 4 } }, label: 2 },
              2: { options: [ { optionName: false }, { optionName: 0 } ], label: 'Three' },
              3: { options: { 5: { optionName: [false] }, 6: { optionName: 0 } }, label: ['Four'] }
            },
            lookup: { 1: 'String 1', 2: 'String 2', 3: 'String 3', 4: 'String 4', 5: 'String 5' }
          };

          apiService
            .fetchSurveyData(surveyDataApi)
            .pipe(firstValueAfterPending())
            .subscribe((surveyData: KeyedSurveyDataNodes) => {
              expect(typeof surveyData).toBe('object');
              expect(surveyData).not.toBeNull();

              for (const key in surveyData) {
                expect(typeof surveyData[key].label).toBe('string');
                expect(typeof surveyData[key].toolTip === 'string' || surveyData[key].toolTip === undefined).toBeTruthy();
                expect(typeof surveyData[key].options).toBe('object');
                expect(surveyData[key].options).not.toBeNull();
                for (const opKey in surveyData[key].options) {
                  expect(typeof surveyData[key].options[opKey].optionName).toBe('string');
                }
              }
            });

          const req = httpMock.expectOne(surveyDataApi);
          expect(req.request.method).toBe('GET');
          req.flush(wrongProps);
        });

      });
    });
  });

});
