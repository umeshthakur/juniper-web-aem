
export interface ISurveyData {
  data: { [key: string]: ISurveyDataNode };
  lookup: SurveyDataLookup;
}

export interface SurveyDataLookup { [key: string]: string; }

export interface ISurveyDataNode {
  label: string;
  toolTip?: string;
  options: ISurveyDataNodeOptions;
  inputType?: string;
  description?: string;
  sliderInput?: string;
}

export interface ISurveyDataNodeOptions {
  [key: string]: {
    optionName?: string;
    leafDetails?: any;
    spineDetails?: any;
    design?: any;
  };
}

/**
 * For retrieving survey information
 */
export class SurveyDataNode implements ISurveyDataNode {
  /** Declares that the values for `label` and `toolTip` will be strings */
  label: string = '';
  toolTip?: string;
  /** Declares how the data for the `options` object will appear */
  options: ISurveyDataNodeOptions = {};
  inputType?: string = '';
  description?: string = '';
  sliderInput?: string = '';

  /** Replaces the number value with the respective string */
  constructor(node: ISurveyDataNode, lookup?: SurveyDataLookup) {
    if (typeof lookup === 'object' && lookup !== null) {
      this._getValueAsString('label', node, lookup);
      this._getValueAsString('toolTip', node, lookup);
      this._getOptionAsString('optionName', node, lookup);
      this._getValueAsString('inputType', node, lookup);
      this._getValueAsString('description', node, lookup);
      this._getValueAsString('sliderInput', node, lookup);
      this._getDCObjects('spineDetails', node, lookup);
      this._getDCObjects('leafDetails', node, lookup);
      this._getDCObjects('design', node, lookup);
    }
  }

  private _getDCObjects(key: string, node: ISurveyDataNode, lookup?: SurveyDataLookup) {
    if (typeof node.options === 'object' && Array.isArray(node.options) && node.inputType && node.inputType === 'list') {
      /** Return options object to `this` */
      for (const item in node.options) {
        this.options[item][key] = node.options[item][key];
        if (key === 'design') {
          // tslint:disable-next-line:max-line-length
          node.options[item][key]['label'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['label'], lookup) || node.options[item][key]['label'];
          // tslint:disable-next-line:max-line-length
          node.options[item][key]['imageUrl'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['imageUrl'], lookup) || node.options[item][key]['imageUrl'];
          // tslint:disable-next-line: max-line-length
          node.options[item][key]['Operations'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['Operations'], lookup) || node.options[item][key]['Operations'];
          // tslint:disable-next-line: max-line-length
          node.options[item][key]['Notes'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['Notes'], lookup) || node.options[item][key]['Notes'];
        } else {
          // tslint:disable-next-line:max-line-length
          node.options[item][key]['product'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['product'], lookup) || node.options[item][key]['product'];
          // tslint:disable-next-line:max-line-length
          node.options[item][key]['imageUrl'] = SurveyDataNode.getIdxFromLookup(node.options[item][key]['imageUrl'], lookup) || node.options[item][key]['imageUrl'];
        }
      }
    }
  }

  /** Replaces number value set for each `objectName` with respective string */
  private _getOptionAsString(key: string, node: ISurveyDataNode, lookup?: SurveyDataLookup) {

    /** Create empty `option` object */
    const options = {};

    /** Map keys and values to options */
    const setKeyInOptions = (k, value) => {
      /** Ensure the options key is ab object */
      if (typeof options[k] !== 'object' || options[k]) options[k] = {};
      /** Ensure options value is string */
      options[k][key] = SurveyDataNode.getIdxFromLookup(value, lookup) || value;
    };

    if (typeof node.options === 'object' && node.options) {
      /** Iterate over options */
      Object.keys(node.options)
        /** Convert keys into numbers */
        // .map(k => parseInt(k, 10))
        /** Check to make sure `node.options[k][key]` exists */
        .filter(k => (node.options as Object).hasOwnProperty(k))
        .filter(k => (node.options[k] as Object).hasOwnProperty(key))
        /** For each option, set its key as a string */
        .forEach(k => setKeyInOptions(k, node.options[k][key]));
    }

    /** Return options object to `this` */
    Object.assign(this.options, options);
  }

  /** Replaces number value set for each `label` and `toolTip` with respective string */
  private _getValueAsString(key: string, node: ISurveyDataNode, lookup?: SurveyDataLookup): void {
    if (typeof lookup === 'object' && !!lookup && !!node[key] && typeof node[key] !== 'boolean') {
      const lookupVal = SurveyDataNode.getIdxFromLookup(node[key], lookup);
      /**
       * Ensure a string is being set. One of either, in order:
       * 1. The lookup retrieved value
       * 2. The node itself, if a string
       * 3. An empty string
       */
      this[key] = (lookupVal ? lookupVal : typeof node[key] === 'string' ? node[key] : '');
    }
  }

  /** Searches for lookup[] index number from number values within `label`, `toolTip` and `optionName` */
  static getIdxFromLookup(idx: number = -1, lookup: SurveyDataLookup = {}): string {
    const ret = lookup[idx];
    return (typeof ret === 'string' ? ret : null);
  }
}
