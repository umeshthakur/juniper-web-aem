import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { SurveyDataNode } from './survey-data-node';
import { LsResultDataNode, ResultDataNode } from './result-data-node';
import { cloneObject } from '../../util/index';
import { SW_HTTP_REQUEST_STATE, SwHttpResponseSnapshot } from '../../core/http/state';

export const SELECTOR_DATA_SLOW_DELAY = 10000;
export const SURVEY_DELAY$ = of(SW_HTTP_REQUEST_STATE.SLOW).pipe(delay(SELECTOR_DATA_SLOW_DELAY));

export interface KeyedSurveyDataNodes {
  [key: string]: SurveyDataNode;
}

/**
 * Provides Data Center Selector mock data.
 * Use only for dev, testing, or prototyping.
 */
export abstract class SwDcSelectorDataService {
  /** Maps for storing categories, items and survey */
  protected _inProgressUrlFetches = new Map<string, Observable<any>>();
  protected _resultStore = new Map<string, SwHttpResponseSnapshot<ResultDataNode[]>>();
  protected _surveyStore = new Map<string, SwHttpResponseSnapshot<KeyedSurveyDataNodes>>();
  protected _lssurveyStore = new Map<string, SwHttpResponseSnapshot<LsResultDataNode[]>>();

  /** Method to fetch `_resultStore` map data */
  abstract fetchResultData (resultPath?: string, resultFilter?: string): Observable<SwHttpResponseSnapshot<ResultDataNode[]>>;

  /** Method to fetch `_surveyStore` map data */
  abstract fetchSurveyData (surveyPath?: string): Observable<SwHttpResponseSnapshot<KeyedSurveyDataNodes>>;

  /** Method to fetch additional result content from LS */
  abstract fetchLsResultData (lsresultPath?: string): Observable<SwHttpResponseSnapshot<LsResultDataNode[]>>;

  protected _cloneData<T> (data: T): T {
    return cloneObject(data);
  }
}

export { SW_HTTP_REQUEST_STATE, SwHttpResponseSnapshot } from '../../core/http/state';
