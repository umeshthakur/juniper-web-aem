function isString(str: string) {
  return typeof str === 'string';
}

/**
 * For retrieving result information
 */
export class ResultDataNode {
  /** API result response properties */
  name: string;
  label: string;
  url: string;
  imageUrl: string;
  /** LS result response properties */

  constructor(node: ResultDataNode) {
    this.label = (node && isString(node.label) ? node.label : null);
    this.url = (node && isString(node.url) ? node.url : null);
    this.imageUrl = (node && isString(node.imageUrl) ? node.imageUrl : null);
    this.name = (node && isString(node.name) ? node.name : this.label);
  }

  static isValid(node: ResultDataNode) {
    return node && !!node.label && !!node.imageUrl && !!node.url;
  }
}

export class LsResultDataNode extends ResultDataNode {
  overviewLabel: string;
  overviewUrl: string;
  datasheetLabel: string;
  freetrialLabel: string;
  freetrialUrl: string;
  dataSheetUrl: string;

  constructor(node: LsResultDataNode) {
    super(node);
    this.overviewLabel = (node && isString(node.overviewLabel) ? node.overviewLabel : null);
    this.overviewUrl = (node && isString(node.overviewUrl) ? node.overviewUrl : null);
    this.datasheetLabel = (node && isString(node.datasheetLabel) ? node.datasheetLabel : null);
    this.dataSheetUrl = (node && isString(node.dataSheetUrl) ? node.dataSheetUrl : null);
    this.freetrialLabel = (node && isString(node.freetrialLabel) ? node.freetrialLabel : null);
    this.freetrialUrl = (node && isString(node.freetrialUrl) ? node.freetrialUrl : null);
  }
}

export interface MergedResultDataNode extends Partial<LsResultDataNode> { }
