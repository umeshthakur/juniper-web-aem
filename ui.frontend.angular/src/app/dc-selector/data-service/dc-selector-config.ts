export interface SwDcSelectorConfig {
  results: string;
  survey: string;
  lsresults: string;
}

export class SwDcSelectorApiConfig implements SwDcSelectorConfig {
  results: string;
  survey: string;
  lsresults: string;

  private _isValid: boolean = false;

  constructor (config?: SwDcSelectorConfig) {
    this.config = config;
  }

  set config (config: SwDcSelectorConfig) {
    if (SwDcSelectorApiConfig.validateConfig(config)) {
      this.results = config.results;
      this.survey = config.survey;
      this.lsresults = config.lsresults;
      this._isValid = true;
    }
  }

  get isValid () { return this._isValid; }

  static validateConfig (config: SwDcSelectorConfig) {
    return (typeof config === 'object' &&
            typeof config.results === 'string' && config.results.length > 0 &&
            typeof config.survey === 'string' && config.survey.length > 0 &&
            typeof config.lsresults === 'string' && config.lsresults.length > 0);
  }
}
