import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  TemplateRef,
  ViewChild
} from '@angular/core';
import { animate, AnimationTriggerMetadata, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { Subscription } from 'rxjs';

import { SwBaseComponent } from '../core/base';
import { DataType, simpleObjToSchemaConverter, SwInputCallback, SwInputProp } from '../core/validation';
import { MarqueeAction, marqueeDefaultAction, SwMarqueeComponent } from '../jnpr-common/marquee/marquee.component';
import { slideAnimation } from '../core/animation/animation';
import { SwScrollToService } from '../modules/scroll/index';
import { SwDialog, SwDialogRef } from '../jnpr-common/dialog/index';

import {
  SELECTOR_STORE_PROVIDER,
  SW_HTTP_REQUEST_STATE,
  SwDcSelectorConfig,
  SwDcSelectorStoreService,
  SwDcSelectorStoreServiceDecision,
  SwDcSelectorStoreServiceResult,
  SwDcSelectorStoreServiceState
} from './dc-selector-store.service';

export const dcSelectorSelector = 'jnpr-dc-selector';
export const defaultApi: SwDcSelectorConfig = { results: '', survey: '', lsresults: '' };

export interface DcSelectorL10n {
  intro_action: string;
  reset_action: string;
  reset_prompt: string;
  reset_accept: string;
  reset_decline: string;
  survey_title: string;
  result_overview: string;
  recommended_title: string;
  msg_error: string;
  msg_no_results: string;
  msg_pending: string;
  msg_slow: string;
}

export interface LinkItems {
  url: string;
  label: string;
  target: string;
  onclick: string;
  showonMobile: boolean;
}

export const l10nDefaults: DcSelectorL10n = {
  intro_action: 'Get Started',
  reset_action: 'Start Over',
  reset_prompt: 'Are you sure you want to start over?',
  reset_accept: 'Yes',
  reset_decline: 'No',
  survey_title: 'Data Center Design Questions',
  result_overview: 'Data Center Overview',
  recommended_title: 'Recommended $1',
  msg_error:
    "Apologies for the inconvenience. We're having some trouble retrieving data at this time. Please try again in a few&nbsp;minutes.",
  msg_no_results: 'Unfortunately, no results were&nbsp;found.',
  msg_pending: "One moment please. We're in the process of fetching your&nbsp;data.",
  msg_slow:
    'Excuse the inconvenience. Your query is taking longer than usual, but we expect to have a result set back&nbsp;shortly.'
};

export interface SelectorResultsMarqueeAction extends MarqueeAction { }

export function expandAnimation(): AnimationTriggerMetadata {
  return trigger('expandState', [
    state('true', style({ height: AUTO_STYLE, opacity: 1 })),
    state('false', style({ height: 0, opacity: 0 })),
    transition('* => *', animate('500ms cubic-bezier(0.35, 0, 0.25, 1)'))
  ]);
}

@Component({
  selector: dcSelectorSelector,
  templateUrl: './dc-selector.component.html',
  styleUrls: ['./dc-selector.component.scss'],
  providers: [SELECTOR_STORE_PROVIDER],
  animations: [expandAnimation(), slideAnimation('slowSlideAnimation', '1000ms'), slideAnimation('slideAnimation')],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class JnprDcSelectorComponent extends SwBaseComponent implements AfterContentInit, OnDestroy {
  @ViewChild('resetDialog', { read: TemplateRef })
  _resetDialogTmpl: TemplateRef<any>;
  _resetDialogRef: SwDialogRef<TemplateRef<any>>;

  /**
   * Configuration settings
   */
  @SwInputCallback('apiChange')
  @SwInputProp<SwDcSelectorConfig>(DataType.object, defaultApi, { schema: simpleObjToSchemaConverter(defaultApi) })
  api: SwDcSelectorConfig;
  /** Callback when API config is set */
  apiChange(api: SwDcSelectorConfig) {
    this._storeService.config = api;
  }

  /**
   * Result Action Marquee settings
   */
  @SwInputProp<string>(DataType.string, '')
  resultPromoBackgroundImage: string;

  @SwInputProp<string>(DataType.string, '')
  resultPromoEyebrow: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  resultPromoBody: string[];

  @SwInputProp(DataType.string, '')
  resultPromoHeadline: string;

  @SwInputProp<SelectorResultsMarqueeAction>(DataType.object, marqueeDefaultAction, {
    schema: simpleObjToSchemaConverter(marqueeDefaultAction)
  })
  resultPromoAction: SelectorResultsMarqueeAction;

  /**
   * Introductory Marquee settings
   */
  @SwInputProp(DataType.string, '')
  introHeadline: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  introBody: string[];

  @SwInputProp<string>(DataType.string, '')
  introBackgroundImage: string;

  @SwInputProp<any>(DataType.object, {})
  ctaDetails: any;

  @SwInputProp<LinkItems, LinkItems[]>( DataType.array, [], { type: DataType.object } )
  introLinks: LinkItems[];

  /**
   * Localization settings
   */
  @SwInputProp<DcSelectorL10n>(DataType.object, l10nDefaults, {
    schema: simpleObjToSchemaConverter(l10nDefaults, false)
  })
  l10n: Partial<DcSelectorL10n>;

  get _l10n() {
    return Object.assign({}, l10nDefaults, this.l10n);
  }

  /** Stores the component subscriptions */
  private _selectorSubscriptions: Subscription[] = [];

  /** Stores the current decision data */
  private _decisions: SwDcSelectorStoreServiceDecision[] = [];
  /** Returns the current decision data */
  get decisions() { return this._decisions; }
  /** Returns the first decision */
  get firstDecision() { return this.decisions && typeof this.decisions[0] === 'object' ? this.decisions[0] : null; }
  /** Supports ngFor to track indexes  */
  trackByDecision(idx, decision) { return decision ? decision.decisionId : undefined; }
  /** Stores the current decision state */
  private _decisionState: SW_HTTP_REQUEST_STATE;
  /** Returns the current decision state */
  get decisionState() { return this._getLocalizedState(this._decisionState); }

  /** Stores the current result data */
  private _results: SwDcSelectorStoreServiceResult[] = [];
  /** Returns the current result data */
  get results() { return this._results; }
  /** Stores the current result state */
  private _resultState: SW_HTTP_REQUEST_STATE;
  /** Returns the current result state */
  get resultState() { return this._getLocalizedState(this._resultState); }

  /** Whether the initial state should be displayed or not */
  displayInitial: boolean = true;
  /** Continues to survey, i.e., sets initial display to `false` */
  commenceSurvey() { this.displayInitial = false; }
  /** Whether the survey is being displayed or not */
  displaySurvey: boolean = true;
  /** Toggles the survey display */
  toggleSurveyDisplay() { this.displaySurvey = !this.displaySurvey; }
  /** Icon to show depending on survey display state */
  get surveyHeaderIcon() { return `expand_${this.displaySurvey ? 'less' : 'more'}`; }
  /** Whether the survey is being displayed or not */
  get displayResults(): boolean {
    return typeof this._resultState === 'number' && this._resultState !== SW_HTTP_REQUEST_STATE.UNAVAILABLE;
  }

  get introMarqueeProps(): Partial<SwMarqueeComponent> {
    return {
      variant: 'secondary',
      headline: this.introHeadline,
      body: this.introBody,
      backgroundImage: this.introBackgroundImage
    };
  }

  get resultMarqueeProps(): Partial<SwMarqueeComponent> {
    return {
      variant: 'secondary',
      eyebrow: this.resultPromoEyebrow,
      headline: this.resultPromoHeadline,
      body: this.resultPromoBody,
      backgroundImage: this.resultPromoBackgroundImage,
      action: this.resultPromoAction
    };
  }

  constructor(
    private _storeService: SwDcSelectorStoreService,
    private _scrollTo: SwScrollToService,
    private _dialog: SwDialog,
    private _cd: ChangeDetectorRef
  ) {
    super();
  }

  ngAfterContentInit() {
    this._selectorSubscriptions.push(
      this._storeService.decisionState$.subscribe(state => {
        this._decisionState = state;
        this._cd.markForCheck();
      })
    );

    this._selectorSubscriptions.push(
      this._storeService.decisions$.subscribe(v => {
        const decisions: SwDcSelectorStoreServiceDecision[] = [];
        v.forEach(decision => decisions.push(decision));
        this._decisions = decisions;
        this._cd.markForCheck();
      })
    );

    this._selectorSubscriptions.push(
      this._storeService.resultState$.subscribe(state => {
        if (state !== SW_HTTP_REQUEST_STATE.UNAVAILABLE && this._resultState === SW_HTTP_REQUEST_STATE.UNAVAILABLE) {
          this.displaySurvey = false;
        }
        this._resultState = state;
        this._cd.markForCheck();
      })
    );

    this._selectorSubscriptions.push(
      this._storeService.results$.subscribe(v => {
        this._results = v;
        this._cd.markForCheck();
      })
    );
    this._storeService.config = this.api;
  }

  ngOnDestroy() {
    if (this._selectorSubscriptions) {
      this._selectorSubscriptions.forEach(s => s.unsubscribe());
    }
  }

  /** Update state with new answer */
  continueSurvey(decision: SwDcSelectorStoreServiceDecision, selected: number) {
    this._storeService.updateState({
      type: SwDcSelectorStoreServiceState.SELECT_ITEM,
      decisionId: decision.decisionId,
      selectedId: selected
    });
  }

  /** Resets store to restart progress */
  resetSurvey(): void {
    this._resetDialogRef = this._dialog.open(this._resetDialogTmpl);
    this._resetDialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this._storeService.resetState();
        this.displaySurvey = true;
        this._resetDialogRef = null;
      }
    });
  }

  /** Toggle decision show/hide state */
  toggleDecisionDisplay(decision: SwDcSelectorStoreServiceDecision) {
    this._storeService.updateState({
      type: decision.expanded
        ? SwDcSelectorStoreServiceState.COLLAPSE_ITEM
        : SwDcSelectorStoreServiceState.EXPAND_ITEM,
      decisionId: decision.decisionId,
      selectedId: decision.selectedId
    });
  }

  scrollTo(el: Element, should: boolean = true) {
    if (should) this._scrollTo.smooth(el);
  }

  /** Retrieves the option name for a given selected decision option */
  public get _getFirstDecisionOptionName() {
    return typeof this.firstDecision.selectedId === 'number' &&
      this.firstDecision.options[this.firstDecision.selectedId] !== undefined
      ? this.firstDecision.options[this.firstDecision.selectedId].optionName || ''
      : '';
  }

  /** Converts a boolean to a string. Currently used for template animations */
  public _boolToStr(bool: boolean) {
    return bool.toString();
  }

  /** Replaces a string where a l10n token is found */
  public _localize(l10nStr: string = '', value: string = '') {
    return l10nStr.replace(/(\$\d+)/g, value);
  }

  public _getLocalizedState(state: SW_HTTP_REQUEST_STATE) {
    switch (state) {
      case SW_HTTP_REQUEST_STATE.ERROR:
        return this._l10n.msg_error;
      case SW_HTTP_REQUEST_STATE.NO_RESULTS:
        return this._l10n.msg_no_results;
      case SW_HTTP_REQUEST_STATE.PENDING:
        return this._l10n.msg_pending;
      case SW_HTTP_REQUEST_STATE.SLOW:
        return this._l10n.msg_slow;
      default:
        return null;
    }
  }
}
