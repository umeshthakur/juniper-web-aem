import { AfterContentInit, ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

import { dcSelectorSelector, expandAnimation } from './dc-selector.component';
import { SwDcSelectorStoreServiceDecision } from './dc-selector-store.service';
import { ISurveyDataNodeOptions } from './data-service/index';

interface defaultSlider {
  autoTicks: boolean;
  disabled: boolean;
  invert: boolean;
  max: number;
  min: number;
  showTicks: boolean;
  step: number;
  thumbLabel: boolean;
  value: number;
  vertical: boolean;
  tickInterval: number;
}

@Component({
  selector: dcSelectorSelector + '-decision',
  templateUrl: './dc-selector-decision.component.html',
  styleUrls: ['./dc-selector-decision.component.scss'],
  animations: [expandAnimation()],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwDcSelectorDecisionComponent implements SwDcSelectorStoreServiceDecision, AfterContentInit {

  ngAfterContentInit() {
    this.portSlider.max = this._optionsLength - 1;
  }

  selectedLength = 0;
  portSlider: defaultSlider = {
    autoTicks: true,
    disabled: false,
    invert: false,
    max: 25,
    min: 0,
    showTicks: true,
    step: 1,
    thumbLabel: true,
    value: 0,
    vertical: false,
    tickInterval: 1
  };

  /** Expanded state */
  @Input() expanded: boolean = true;

  /** Tooltip label */
  @Input() toolTip: string;
  /** Tooltip display state */
  private _tooltipDisplay: boolean = false;
  /**
   * Tooltip display state as exposed publicly.
   * The tooltip will not be shown when the decision isn't expanded
   */
  get tooltipDisplay(): boolean { return this.expanded && this._tooltipDisplay; }

  /** Decision label */
  @Input() label: string;

  /** Decision label */
  @Input() description: string;

  /** Decision label */
  @Input() sliderInput: string;

  /** Decision label */
  @Input() subtitle: string;

  /** Decision id */
  @Input() decisionId: number;

  /** The selected option id */
  @Input('selectedId')
  set selected(id: number) {
    this.selectedId = id;
    this._tooltipDisplay = false;
  }
  selectedId: number;

  /** The options object */
  @Input() options: ISurveyDataNodeOptions;

  /** The inputType object */
  @Input() inputType: string;

  /** Toggle decision show/hide state */
  @Output() onToggle = new EventEmitter<boolean>();
  toggleExpand() { this.onToggle.emit(); }

  /** Toggle tooltip show/hide state */
  @Output() onContinue = new EventEmitter<boolean>();
  toggleTooltip() { this._tooltipDisplay = !this._tooltipDisplay; }

  /** Trigger the survey to continue */
  continue(selection) { this.onContinue.emit(selection); }

  triggerContinue(ev) {
    this.selectedLength = ev.currentTarget.value;
  }
  /** Determines whether a selection can be changed once there's been a selection */
  get _decisionIsEditable() {
    return Object.keys(this.options).length > 1;
  }

  get _optionsLength() {
    return Object.keys(this.options).length;
  }

  get _displayOptionsString() {
    return JSON.stringify(this.options);
  }

  /** Retrieves the option name for a given selected decision option */
  get _selectedDecisionOptionName() {
    return (
      typeof this.selectedId === 'number' && this.options[this.selectedId] !== undefined ?
        this.options[this.selectedId].optionName || '' : ''
    );
  }

  formatPortLabel = (options: any) => {
    return (value: any) => {
      if (options[Object.keys(options)[value]]) return options[Object.keys(options)[value]].optionName;
      else return false;
    };
  }

  onInputChange(ev) {
    if (Object.keys(this.options)[ev.value] !== undefined) this.continue(Object.keys(this.options)[ev.value]);
  }

  public _boolToStr(bool: boolean) { return bool.toString(); }
}
