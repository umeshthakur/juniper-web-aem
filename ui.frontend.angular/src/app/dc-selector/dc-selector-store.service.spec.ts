import {
  SwDcSelectorApiConfig,
  SwDcSelectorConfig,
  SwDcSelectorStoreService,
  SwDcSelectorStoreServiceDecision,
  SwDcSelectorStoreServiceResult,
  SwDcSelectorStoreServiceState
} from './dc-selector-store.service';
import {
  ISurveyData,
  KeyedSurveyDataNodes,
  ResultDataNode
} from './data-service/index';
import { Subscription } from 'rxjs';
import { first, map } from 'rxjs/operators';

import { SwDcSelectorMockDataService } from './data-service/dc-selector-mock-data.service';
import { npsSwitchesData as surveyMockData } from './data-service/data/nps-survey-mock';
import { npsMockResultsData as resultMockData } from './data-service/data/nps-results-mock';
import { LsResultDataNode } from './data-service/result-data-node';

interface IMockDecision {
  decisionId: number;
  selectedId: number;
}

interface IMockChoices {
  first: IMockDecision;
  second: IMockDecision;
  third: IMockDecision;

  different: number;
}

describe('SwDcSelectorStoreService', () => {
  let store: SwDcSelectorStoreService;
  const config: SwDcSelectorConfig = new SwDcSelectorApiConfig({ results: 'results', survey: 'survey', lsresults: 'lsresults' });
  let receivedDecisions: Map<number, SwDcSelectorStoreServiceDecision>;
  let receivedResults: SwDcSelectorStoreServiceResult[];
  const subscriptions: Subscription[] = [];

  let mockChoices: IMockChoices;
  let mockData: KeyedSurveyDataNodes;

  // For getting results
  let mockChoiceTree: number[];
  let differentChoice: number;

  beforeEach(() => {
    const mockDataService = new SwDcSelectorMockDataService();
    mockDataService.resultMockData = resultMockData as ResultDataNode[];
    mockDataService.surveyMockData = surveyMockData as ISurveyData;
    mockDataService.lsResultsMockData = resultMockData as LsResultDataNode[];
    store = new SwDcSelectorStoreService(mockDataService);

    subscriptions.push(store.decisions$.subscribe(v => receivedDecisions = v));
    subscriptions.push(store.results$.subscribe(v => receivedResults = v));

    store.config = config;
    subscriptions.push(
      mockDataService.fetchSurveyData().pipe(
        first(),
        /** Get results from state snapshot */
        map(snap => snap.value)
      ).subscribe(values => mockData = values)
    );
    const firstD: IMockDecision = {
      decisionId: parseInt(receivedDecisions.get(0).decisionId.toString(), 10),
      selectedId: parseInt(Object.keys(receivedDecisions.get(0).options)[0], 10)
    };
    const secondD: IMockDecision = {
      decisionId: firstD.selectedId,
      selectedId: parseInt(Object.keys(mockData[firstD.selectedId].options)[0], 10)
    };
    const thirdD: IMockDecision = {
      decisionId: secondD.selectedId,
      selectedId: parseInt(Object.keys(mockData[secondD.selectedId].options)[0], 10)
    };
    mockChoices = {
      first: firstD, second: secondD, third: thirdD,
      different: parseInt(Object.keys(mockData[firstD.selectedId].options)[1], 10)
    };
  });

  afterEach(() => {
    subscriptions.forEach(s => s.unsubscribe());
  });

  describe('config', () => {
    it(`should take a config as long as it's valid`, () => {
      expect(store.config.results).toBe('results');
      expect(store.config.survey).toBe('survey');

      store.config = new SwDcSelectorApiConfig({ results: '', survey: '', lsresults: '' });

      expect(store.config).toEqual(store.config);
      expect(store.config.results).toBe('results');
      expect(store.config.survey).toBe('survey');
    });

    it('should re-init when a valid config is passed subsequent times', () => {
      store.updateState({
        type: SwDcSelectorStoreServiceState.SELECT_ITEM,
        decisionId: mockChoices.first.decisionId,
        selectedId: mockChoices.first.selectedId
      });

      expect(receivedDecisions.size).toBe(2);

      store.config = config;

      expect(receivedDecisions.size).toBe(1);
    });
  });

  describe(`initialize()`, () => {

    it(`should contain 1 index on page load`, () => {
      expect(receivedDecisions.size).toBe(1);
    });

    it(`shouldn't init with a selectedId`, () => {
      expect(receivedDecisions.get(0).selectedId).toBeNull();
    });

    describe('edge cases', () => {
      let config;
      let mockDataService;
      let store;
      let receivedDecisions;

      beforeEach(() => {
        config = new SwDcSelectorApiConfig({ results: 'r', survey: 's', lsresults: 'l' });
        mockDataService = new SwDcSelectorMockDataService();
        store = new SwDcSelectorStoreService(mockDataService);

        subscriptions.push(store.decisions$.subscribe(v => receivedDecisions = v));
        subscriptions.push(store.results$.subscribe(v => receivedResults = v));
      });

      it('should not init when there is no survey data', () => {
        store.config = config;

        expect(receivedDecisions.size).toBe(0);

        mockDataService.resultMockData = resultMockData;
        mockDataService.surveyMockData = surveyMockData;
        store.config = config;

        expect(receivedDecisions.size).toBe(1);
      });

      it('should not init until a valid config is passed', () => {
        mockDataService.resultMockData = resultMockData;
        mockDataService.surveyMockData = surveyMockData;

        store.config = new SwDcSelectorApiConfig({ results: '', survey: '', lsresults: '' });

        expect(receivedDecisions.size).toBe(0);

        store.config = config;

        expect(receivedDecisions.size).toBe(1);
      });
    });
  });

  describe(`updateState()`, () => {

    describe(`SELECT_ITEM`, () => {

      describe(`Progressing survey`, () => {

        it(`should map decisions to 'SwDcSelectorStoreDecision'`, () => {
          receivedDecisions.forEach(decision => {
            expect(typeof decision.decisionId).toBe('number');
            expect(decision.decisionId).toBeGreaterThan(-1);
            expect(typeof decision.label).toBe('string');
            expect(decision.label.length).toBeGreaterThan(0);
            expect(typeof decision.toolTip === 'string' || decision.toolTip === null).toBeTruthy();
            expect(typeof decision.options).toBe('object');
            expect(decision.options).not.toBeNull();
            expect(Object.keys(decision.options).length).toBeGreaterThan(0);
            expect(typeof decision.selectedId === 'number' || decision.selectedId === null).toBeTruthy();
            expect(typeof decision.expanded).toBe('boolean');
          });
        });

        it(`should progress survey by setting the \`selectedId\` if there is a next question`, () => {
          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.size).toBe(2);
          expect(typeof receivedDecisions.get(0).selectedId).toBe('number');
          expect(receivedDecisions.get(0).selectedId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.get(1).decisionId).toBe(mockChoices.second.decisionId);
          expect(receivedDecisions.get(1).selectedId).toBeNull();
          expect(receivedDecisions.get(1)).toBeDefined();

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.second.decisionId,
            selectedId: mockChoices.second.selectedId
          });

          expect(receivedDecisions.size).toBe(3);
          expect(typeof receivedDecisions.get(1).selectedId).toBe('number');
          expect(receivedDecisions.get(1).selectedId).toBe(mockChoices.second.selectedId);
          expect(receivedDecisions.get(2).decisionId).toBe(mockChoices.third.decisionId);
          expect(receivedDecisions.get(2).selectedId).toBeNull();
          expect(receivedDecisions.get(2)).toBeDefined();
        });

        it(`should truncate tree if a different option was selected`, () => {
          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.size).toBe(2);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.second.decisionId,
            selectedId: mockChoices.second.selectedId
          });

          expect(receivedDecisions.size).toBe(3);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.third.decisionId,
            selectedId: mockChoices.third.selectedId
          });

          expect(receivedDecisions.size).toBe(4);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.second.decisionId,
            selectedId: mockChoices.different
          });

          expect(receivedDecisions.size).toBe(3);
          expect(receivedDecisions.get(0).selectedId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.get(1).selectedId).toBe(mockChoices.different);
          expect(receivedDecisions.get(2).decisionId).toBe(mockChoices.different);
          expect(receivedDecisions.get(2).selectedId).toBeNull();
        });

        it(`should create expanded decisions by default and collapse the item on selection`, () => {
          expect(receivedDecisions.get(0).expanded).toBeTruthy();

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.get(0).expanded).toBeFalsy();
          expect(receivedDecisions.get(1).expanded).toBeTruthy();

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.second.decisionId,
            selectedId: mockChoices.second.selectedId
          });

          expect(receivedDecisions.get(1).expanded).toBeFalsy();
          expect(receivedDecisions.get(2).expanded).toBeTruthy();
        });

        it(`should be able to reset and progress decisions again`, () => {
          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.size).toBe(2);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.second.decisionId,
            selectedId: mockChoices.second.selectedId
          });

          expect(receivedDecisions.size).toBe(3);

          store.resetState();

          expect(receivedDecisions.size).toBe(1);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.size).toBe(2);
        });

        it(`shouldn't progress when the same decisionId is passed`, () => {
          expect(receivedDecisions.size).toBe(1);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.get(0).selectedId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.get(1).decisionId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.size).toBe(2);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoices.first.decisionId,
            selectedId: mockChoices.first.selectedId
          });

          expect(receivedDecisions.get(0).selectedId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.get(1).decisionId).toBe(mockChoices.first.selectedId);
          expect(receivedDecisions.size).toBe(2);
        });

      });

      describe(`Retrieving results`, () => {

        beforeEach(() => {
          mockChoiceTree = [0, 1, 10, 14, 65];
          differentChoice = 9;

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoiceTree[0],
            selectedId: mockChoiceTree[1]
          });

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoiceTree[1],
            selectedId: mockChoiceTree[2]
          });

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoiceTree[2],
            selectedId: mockChoiceTree[3]
          });

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoiceTree[3],
            selectedId: mockChoiceTree[4]
          });
        });

        it(`should get results if end of survey`, () => {
          expect(receivedResults.length).toBeGreaterThan(0);
        });

        it(`should map results to 'SwDcSelectorStoreServiceResult'`, () => {
          receivedResults.forEach(result => {
            expect(typeof result.name).toBe('string');
            expect(result.name.length).toBeGreaterThan(0);
            expect(typeof result.label).toBe('string');
            expect(result.label.length).toBeGreaterThan(0);
            expect(typeof result.url).toBe('string');
            expect(result.url.length).toBeGreaterThan(0);
            expect(typeof result.imageUrl).toBe('string');
            expect(result.imageUrl.length).toBeGreaterThan(0);
          });
        });

        it(`should delete results when survey is truncated`, () => {
          expect(receivedResults.length).toBeGreaterThan(0);

          store.updateState({
            type: SwDcSelectorStoreServiceState.SELECT_ITEM,
            decisionId: mockChoiceTree[1],
            selectedId: differentChoice
          });

          expect(receivedResults.length).toBe(0);
        });

        it(`should delete results when state is reset`, () => {
          store.resetState();
          expect(receivedResults.length).toBe(0);
        });

      });

    });

    describe(`EXPAND_ITEM/COLLAPSE_ITEM`, () => {

      it(`should expand and collapse decision nodes`, () => {
        expect(receivedDecisions.get(0).expanded).toBeTruthy();

        store.updateState({
          type: SwDcSelectorStoreServiceState.SELECT_ITEM,
          decisionId: mockChoices.first.decisionId,
          selectedId: mockChoices.first.selectedId
        });

        expect(receivedDecisions.get(0).expanded).toBeFalsy();

        store.updateState({
          type: SwDcSelectorStoreServiceState.EXPAND_ITEM,
          decisionId: mockChoices.first.decisionId,
          selectedId: mockChoices.first.selectedId
        });

        expect(receivedDecisions.get(0).expanded).toBeTruthy();

        store.updateState({
          type: SwDcSelectorStoreServiceState.COLLAPSE_ITEM,
          decisionId: mockChoices.first.decisionId,
          selectedId: mockChoices.first.selectedId
        });

        expect(receivedDecisions.get(0).expanded).toBeFalsy();
      });

    });

  });

});
