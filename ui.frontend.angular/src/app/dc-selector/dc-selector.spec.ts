import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwGlobalModule } from '../global/index';

import { JnprDcSelectorComponent, JnprDcSelectorModule } from './dc-selector.module';
import { SwDcSelectorDataService } from './data-service/index';
import { SwDcSelectorMockDataService } from './data-service/dc-selector-mock-data.service';
import { l10nDefaults as dcSelectorL10nDefaults } from './dc-selector.component';

describe('JnprDcSelectorComponent', () => {
  let comp: JnprDcSelectorComponent;
  let fixture: ComponentFixture<JnprDcSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SwGlobalModule.forBrowser(),
        JnprDcSelectorModule
      ],
      providers: [
        { provide: SwDcSelectorDataService, useClass: SwDcSelectorMockDataService }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JnprDcSelectorComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('authorable property', () => {

    describe('`l10n`', () => {

      it('should set a partial schema as long as the data types are correct', () => {
        const testL10n01 = { msg_no_results: 'Oops! There are no results here!' };
        const testL10n02 = { recommended_title: 'Your personalized $1 selection' };

        comp.l10n = testL10n01;
        fixture.detectChanges();
        expect(comp.l10n.msg_no_results).toBe(testL10n01.msg_no_results);
        expect(comp._l10n.msg_no_results).toBe(testL10n01.msg_no_results);
        expect(comp.l10n.recommended_title).toBeUndefined();
        expect(comp._l10n.recommended_title).toBe(dcSelectorL10nDefaults.recommended_title);

        comp.l10n = testL10n02;
        fixture.detectChanges();
        expect(comp.l10n.recommended_title).toBe(testL10n02.recommended_title);
        expect(comp._l10n.recommended_title).toBe(testL10n02.recommended_title);
        expect(comp.l10n.msg_no_results).toBeUndefined();
        expect(comp._l10n.msg_no_results).toBe(dcSelectorL10nDefaults.msg_no_results);
      });

      it('should not set a value if any value is of another data type', () => {
        const testL10n01 = { msg_no_results: 'Oops! There are no results here!', recommended_title: 0 } as any;
        const testL10n02 = { recommended_title: 'Your personalized $1 selection', msg_no_results: [] } as any;

        comp.l10n = testL10n01;
        fixture.detectChanges();
        expect(comp.l10n.msg_no_results).not.toBe(testL10n01.msg_no_results);
        expect(comp._l10n.msg_no_results).not.toBe(testL10n01.msg_no_results);
        expect(comp.l10n.msg_no_results).toBe(dcSelectorL10nDefaults.msg_no_results);
        expect(comp._l10n.msg_no_results).toBe(dcSelectorL10nDefaults.msg_no_results);
        expect(comp.l10n.recommended_title).toBeDefined();
        expect(comp.l10n.recommended_title).toBe(dcSelectorL10nDefaults.recommended_title);
        expect(comp._l10n.recommended_title).toBe(dcSelectorL10nDefaults.recommended_title);

        comp.l10n = testL10n02;
        fixture.detectChanges();
        expect(comp.l10n.recommended_title).not.toBe(testL10n02.recommended_title);
        expect(comp._l10n.recommended_title).not.toBe(testL10n02.recommended_title);
        expect(comp.l10n.recommended_title).toBe(dcSelectorL10nDefaults.recommended_title);
        expect(comp._l10n.recommended_title).toBe(dcSelectorL10nDefaults.recommended_title);
        expect(comp.l10n.msg_no_results).toBeDefined();
        expect(comp.l10n.msg_no_results).toBe(dcSelectorL10nDefaults.msg_no_results);
        expect(comp._l10n.msg_no_results).toBe(dcSelectorL10nDefaults.msg_no_results);
      });

      it('should work together with `_localize`', () => {
        const testL10n = { recommended_title: 'Your personalized $1 selection' };
        const testReplaceValue = 'TEST_VALUE';

        expect(comp._localize(comp.l10n.recommended_title, testReplaceValue)).toBe(`Recommended ${testReplaceValue}`);
        expect(comp._localize(comp._l10n.recommended_title, testReplaceValue)).toBe(`Recommended ${testReplaceValue}`);

        comp.l10n = testL10n;
        fixture.detectChanges();

        expect(comp._localize(comp.l10n.recommended_title, testReplaceValue)).toBe(`Your personalized ${testReplaceValue} selection`);
        expect(comp._localize(comp._l10n.recommended_title, testReplaceValue)).toBe(`Your personalized ${testReplaceValue} selection`);
      });

    });

  });

  describe('_localize', () => {

    it('should return an empty string when not provided any values', () => {
      expect(comp._localize()).toBe('');
    });

    it(`should strip out the wildcard value when the second param isn't provided`, () => {
      expect(comp._localize(comp.l10n.recommended_title)).toBe('Recommended ');
    });

  });

});
