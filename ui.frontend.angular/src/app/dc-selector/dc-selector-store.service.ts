import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs';
import { filter, map, takeUntil, tap } from 'rxjs/operators';

import {
  ISurveyDataNode,
  ISurveyDataNodeOptions,
  KeyedSurveyDataNodes,
  SW_HTTP_REQUEST_STATE,
  SwHttpResponseSnapshot,
  SwDcSelectorApiConfig,
  SwDcSelectorConfig,
  SwDcSelectorDataService
} from './data-service/index';
import { MergedResultDataNode } from './data-service/result-data-node';

export enum SwDcSelectorStoreServiceState {
  SELECT_ITEM,
  EXPAND_ITEM,
  COLLAPSE_ITEM
}

export interface SwDcSelectorStoreServiceAction {
  type: SwDcSelectorStoreServiceState;
  decisionId: number;
  selectedId: number;
}

export interface SwDcSelectorStoreServiceDecision extends ISurveyDataNode {
  decisionId: number;
  selectedId: number;
  expanded: boolean;
}

export interface SwDcSelectorStoreServiceResult extends MergedResultDataNode { }

type SelectorCorrelationMap = Map<number, number>;
type SelectorDecisionStore = Map<number, SwDcSelectorStoreServiceDecision>;
type SelectorResultStore = SwDcSelectorStoreServiceResult[];

@Injectable()
export class SwDcSelectorStoreService {

  constructor(private _dataService: SwDcSelectorDataService) { }

  /** A map to correlate between `decisionId` => `storeId` */
  private _correlationMap: SelectorCorrelationMap = new Map();

  /** Stores the decisions the user has taken */
  private _decisionStore: BehaviorSubject<SelectorDecisionStore> = new BehaviorSubject(new Map());
  /** Provides the decisions the user has taken */
  public decisions$ = this._decisionStore.asObservable();
  /** Gets total number of stored decisions */
  private get _decisionStoreCount() { return this._decisionStore.getValue().size; }
  /** Stores the decision store state */
  private _decisionStoreState = new BehaviorSubject(SW_HTTP_REQUEST_STATE.UNAVAILABLE);
  /** Provides the decision store state */
  public decisionState$ = this._decisionStoreState.asObservable();

  /** Stores the current result set */
  private _resultStore: BehaviorSubject<SelectorResultStore> = new BehaviorSubject([]);
  /** Provides the current result set */
  public results$ = this._resultStore.asObservable();
  /** Stores the decision store state */
  private _resultStoreState: BehaviorSubject<any> = new BehaviorSubject(SW_HTTP_REQUEST_STATE.UNAVAILABLE);
  /** Provides the result store state */
  public resultState$ = this._resultStoreState.asObservable();

  /** Stores the fetched survey data */
  private _surveyDataStore: KeyedSurveyDataNodes;
  /** Stores the fetched survey data keys */
  private _surveyDataKeys: (keyof KeyedSurveyDataNodes)[];
  /** Fetched survey data setter */
  private set _surveyData(data: KeyedSurveyDataNodes) {
    this._surveyDataStore = data;
    this._surveyDataKeys = Object.keys(this._surveyDataStore).sort();
  }

  /** The configuration to exchange with the data provider */
  private _config: SwDcSelectorApiConfig = new SwDcSelectorApiConfig();
  /** Returns the currently set config */
  get config() { return this._config; }
  /** Sets a config and then, if valid, executes survey data fetching */
  set config(config: SwDcSelectorConfig) {
    this._config.config = config;
    if (this._config.isValid) this._fetchSurveyData();
  }

  /** Stores correlationMap values as array items (except for 0) */
  public dcUrl: number[] = [];

  private _fetchData<T>(
    source$: Observable<SwHttpResponseSnapshot<T>>,
    stateStream$: Observable<SW_HTTP_REQUEST_STATE>
  ): Observable<SwHttpResponseSnapshot<T>> {
    let received = false;
    return source$.pipe(
      /**
       * If "NO_RESULTS", "OK" or "ERROR" are already set when the result
       * stream commences, it will cease immediately without any results.
       * Having this boolean param ensures the stream emits at least once.
       */
      tap(() => (received = true)),
      /** Continue passing data until we get one of the results below */
      takeUntil(
        stateStream$.pipe(
          filter(state => received && (
            state === SW_HTTP_REQUEST_STATE.NO_RESULTS ||
            state === SW_HTTP_REQUEST_STATE.OK ||
            state === SW_HTTP_REQUEST_STATE.ERROR
          ))
        )
      )
    );
  }

  /** Fetches survey data */
  private _fetchSurveyData() {
    if (this._surveyDataSubscription) this._surveyDataSubscription.unsubscribe();
    this._surveyDataSubscription =
      this._fetchData(this._dataService.fetchSurveyData(this._config.survey), this.decisionState$)
        .subscribe(res => {
          this._surveyData = res.value;
          this._resetStore(res.state);
        },
          err => console.error(err),
          () => console.warn(`Completed fetching survey for ${this.config.survey}`));
  }
  private _surveyDataSubscription: Subscription;

  /** Fetches result data */
  private _fetchResultData(dcPath: string) {

    const decisionIdFromURL: number = parseInt(this.config.survey.split('/')[this.config.survey.split('/').length - 1], 10);
    const _dcPath = typeof decisionIdFromURL === 'number' && !isNaN(decisionIdFromURL) && decisionIdFromURL !== 0
      ? decisionIdFromURL + '-' + dcPath
      : dcPath;

    if (this._resultDataSubscription) this._resultDataSubscription.unsubscribe();

    this._resultDataSubscription = combineLatest(
      this._fetchData(this._dataService.fetchResultData(this._config.results, _dcPath), this.resultState$),
      this._dataService.fetchLsResultData(this._config.lsresults)
    ).pipe(
      map(([resultData, lsresultSnap]): SwHttpResponseSnapshot<MergedResultDataNode[]> => {
        const lsresultEnded = (
          lsresultSnap.state === SW_HTTP_REQUEST_STATE.NO_RESULTS ||
          lsresultSnap.state === SW_HTTP_REQUEST_STATE.OK
        );
        const mergedResultData: MergedResultDataNode[] = lsresultEnded ? resultData.value : [];
        const mergedResultState: SW_HTTP_REQUEST_STATE = lsresultEnded ? resultData.state : lsresultSnap.state;

        mergedResultData.forEach(result => {
          lsresultSnap.value.forEach(element => {
            if (result.name === element.name || result.label === element.name) {
              result.label = element.label;
              if (element.imageUrl !== '') result.imageUrl = element.imageUrl;
              if (element.overviewLabel !== '') result.overviewLabel = element.overviewLabel;
              if (element.datasheetLabel !== '') result.datasheetLabel = element.datasheetLabel;
              if (element.freetrialLabel !== '') result.freetrialLabel = element.freetrialLabel;
              if (element.overviewUrl !== '') result.overviewUrl = element.overviewUrl;
              if (element.dataSheetUrl !== '') result.dataSheetUrl = element.dataSheetUrl;
              if (element.freetrialUrl !== '') result.freetrialUrl = element.freetrialUrl;
            }
          });
        });

        return { state: mergedResultState, value: mergedResultData };
      })
    ).subscribe(
      res => {
        this._resultStore.next(res.value);
        this._resultStoreState.next(res.state);
      },
      err => console.error(err),
      () => console.warn(`Completed fetching results for ${_dcPath}`)
    );
  }
  private _resultDataSubscription: Subscription;

  /** Returns a brand new, empty decision store */
  private get _emptyStore(): SelectorDecisionStore {
    return new Map<number, SwDcSelectorStoreServiceDecision>();
  }

  /**
   * Returns a tuple with a brand new, initialized decision store
   * and the number of the first decision id
   */
  private get _initialStore(): [SelectorDecisionStore, number] {
    const store = this._emptyStore;

    /** Not enough survey data to initialize store */
    if (this._surveyDataKeys && !this._surveyDataKeys.length) {
      return [store, null];
    }

    /** Set first decision id */
    const decisionIdFromURL = parseInt(this.config.survey.split('/')[this.config.survey.split('/').length - 1], 10);
    const firstDecisionId = typeof decisionIdFromURL === 'number' && !isNaN(decisionIdFromURL)
      ? decisionIdFromURL
      : parseInt(<string>this._surveyDataKeys[0], 10);

    return [
      store.set(0, this._newDecision(firstDecisionId)),
      firstDecisionId
    ];
  }

  /** Resets the store based on the current survey data available */
  private _resetStore(storeState: SW_HTTP_REQUEST_STATE = SW_HTTP_REQUEST_STATE.UNAVAILABLE) {
    const correlationMap: SelectorCorrelationMap = new Map();
    let decisionStoreState = storeState;
    let decisionStore: SelectorDecisionStore;
    let firstDecisionId: number;

    /** Destructure the initial store tuple */
    [decisionStore, firstDecisionId] = this._initialStore;

    if (typeof firstDecisionId === 'number' && !isNaN(firstDecisionId)) {
      correlationMap.set(firstDecisionId, 0);
      decisionStoreState = SW_HTTP_REQUEST_STATE.OK;
    }

    this._correlationMap = correlationMap;
    this._decisionStoreState.next(decisionStoreState);
    this._decisionStore.next(decisionStore);
    this._clearResults();
  }

  /** Public access to resetting the store */
  public resetState() { this._resetStore(); }

  private _generateDecision(
    decisionId: number,
    label: string,
    toolTip: string,
    options: ISurveyDataNodeOptions,
    selectedId: number,
    expanded: boolean,
    inputType: string,
    sliderInput: string,
    description: string): SwDcSelectorStoreServiceDecision {
    return {
      decisionId: decisionId,
      label: label,
      toolTip: toolTip,
      options: this._cloneObject(options),
      selectedId: selectedId,
      expanded: expanded,
      inputType: inputType,
      sliderInput: sliderInput,
      description: description
    };
  }

  /** Returns a new decision based on a `decisionId` */
  private _newDecision(decisionId: number): SwDcSelectorStoreServiceDecision {
    const decisionById = this._surveyDataStore[decisionId];
    return this._generateDecision(
      /** decisionId */ decisionId,
      /** label */ decisionById.label,
      /** toolTip */(typeof decisionById.toolTip === 'string' ? decisionById.toolTip : null),
      /** options */ decisionById.options,
      /** selectedId */ null,
      /** expanded */ true,
      /** inputType */(typeof decisionById.inputType === 'string' ? decisionById.inputType : null),
      /** sliderInput */(typeof decisionById.sliderInput === 'string' ? decisionById.sliderInput : null),
      /** description */(typeof decisionById.description === 'string' ? decisionById.description : null)
    );
  }

  /** Expands or collapses items */
  private _setExpandedState(index: number, state: boolean) {
    const decision = this._decisionStore.getValue().get(index);
    this._decisionStore.next(
      this._decisionStore.getValue().set(
        index,
        this._generateDecision(
          /** decisionId */ decision.decisionId,
          /** label */ decision.label,
          /** toolTip */ decision.toolTip,
          /** options */ decision.options,
          /** selectedId */ decision.selectedId,
          /** expanded */ state,
          /** inputType */ decision.inputType,
          /** sliderInput */ decision.sliderInput,
          /** description */ decision.description
        )
      )
    );
  }

  /** Updates the survey status */
  public updateState(action: SwDcSelectorStoreServiceAction) {
    if (!action) return;

    switch (action.type) {
      /** Update storage and send next available option (if there) */
      case SwDcSelectorStoreServiceState.SELECT_ITEM:
        /** Ensure selectedId is a number */
        const selectedId = parseInt(action.selectedId.toString(), 10);

        /**
         * STEP 01: CHECK FOR SELECTION CHANGE
         * If the correlation map contains the action's decision id,
         * the user is editing a previous selection.
         * The store will be truncated to such previous state
         * to reflect the new selection
         */
        if (this._correlationMap.has(action.decisionId)) {

          const storeId = this._correlationMap.get(action.decisionId);
          const decision = this._decisionStore.getValue().get(storeId);

          /** Continue and truncate tree only if new option is selected */
          if (decision.selectedId === selectedId) return;

          this._truncateStore(decision, selectedId, storeId);
        }

        /**
         * STEP 02: CHECK FOR EXISTENCE OF A NEXT DECISION
         * If there is a next decision, it will simply be displayed.
         */

        /** Seek a next decision */
        let nextDecision = this._surveyDataStore[selectedId];
        nextDecision = (typeof nextDecision === 'object' && nextDecision !== null ? nextDecision : null);

        /** A next decision is available in the tree */
        if (nextDecision !== null) {
          /** Fetch next store index for storage */
          const storeId = this._decisionStoreCount;

          /** Set new decision */
          this._decisionStore.next(
            this._decisionStore.getValue().set(
              storeId, this._newDecision(selectedId)
            )
          );
          /** Add entry to correlation map */
          this._correlationMap.set(selectedId, storeId);
          /** Ensure results are cleared */
          this._clearResults();
          /** End process */
          return;
        }

        /**
         * STEP 03: DISPLAY RESULT DATA
         * If there was no next decision, then it signifies the end
         * of the decision tree and therefore result data needs to be fetched
         */

        /** Prepare a datacenter URL destination */
        let dcPath: string = '';
        const dcUrl = [];

        /** Iterate through the correlation map to get the decisions made */
        this._correlationMap.forEach(idx => {
          if (idx > 0) {
            const decision = this._decisionStore.getValue().get(idx).decisionId;
            dcPath = dcPath.concat(decision.toString(), '-');
            dcUrl.push(decision);
          }
        });

        /** Add last item for url */
        dcUrl.push(selectedId);
        dcPath = dcPath.concat(selectedId.toString());

        this._fetchResultData(dcPath);
        break;

      /** Show decision */
      case SwDcSelectorStoreServiceState.EXPAND_ITEM:
        this._setExpandedState(this._correlationMap.get(action.decisionId), true);
        break;

      /** Hide decision */
      case SwDcSelectorStoreServiceState.COLLAPSE_ITEM:
        this._setExpandedState(this._correlationMap.get(action.decisionId), false);
        break;
    }
  }

  private _truncateStore(decision: SwDcSelectorStoreServiceDecision, selectedId: number, storeId: number) {

    this._decisionStore.next(
      this._decisionStore.getValue().set(
        storeId,
        this._generateDecision(
          /** decisionId */decision.decisionId,
          /** label */decision.label,
          /** toolTip */decision.toolTip,
          /** options */decision.options,
          /** selectedId */selectedId,
          /** expanded */false,
          /** inputType */ decision.inputType,
          /** sliderInput */ decision.sliderInput,
          /** description */ decision.description
        )
      )
    );

    /** Stores truncated decision list in new map */
    const truncatedStore: SelectorDecisionStore = this._emptyStore;
    const truncatedCorrelationMap: SelectorCorrelationMap = new Map();

    /** Store decisions from further up in the tree */
    this._correlationMap.forEach(idx => {
      /** Cache store value temporarily */
      const store = this._decisionStore.getValue();
      if (idx <= storeId) {
        /** Contains only the current decision and the ones before it */
        truncatedStore.set(idx, store.get(idx));
        truncatedCorrelationMap.set(truncatedStore.get(idx).decisionId, idx);
      }
    });

    /** Replace maps to update current position in decision tree */
    this._decisionStore.next(truncatedStore);
    /** Reset the result store when the tree is truncated */
    this._clearResults();
    this._correlationMap = truncatedCorrelationMap;
  }

  /** Clears the results store */
  private _clearResults() {
    this.dcUrl = [];
    this._resultStore.next([]);
    this._resultStoreState.next(SW_HTTP_REQUEST_STATE.UNAVAILABLE);
  }

  /** Clones a JSON-compliant object */
  private _cloneObject(object) { return JSON.parse(JSON.stringify(object)); }
}

export { SwDcSelectorConfig, SwDcSelectorApiConfig, SW_HTTP_REQUEST_STATE } from './data-service/index';

export function SELECTOR_STORE_PROVIDER_FACTORY(dataService: SwDcSelectorDataService) {
  return new SwDcSelectorStoreService(dataService);
}

export const SELECTOR_STORE_PROVIDER = {
  provide: SwDcSelectorStoreService,
  deps: [SwDcSelectorDataService],
  useFactory: SELECTOR_STORE_PROVIDER_FACTORY
};
