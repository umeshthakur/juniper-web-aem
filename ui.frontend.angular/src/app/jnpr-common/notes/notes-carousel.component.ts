import { Component } from '@angular/core';

import { SwBaseComponent } from '../../core/base/index';
import { DataType,simpleObjToSchemaConverter, SwInputCallback, SwInputProp } from '../../core/validation/index';
import { SwNotesItemComponent } from './notes-item.component';
import { PaginationMode, SwCarouselSlidesPerViewWidth } from '../carousel/carousel.component';
import { SwGridHeaderComponent } from '../grid-header/grid-header.component';
import { SwVideoAction } from '../../core/base/action/action.class';
import { CanIconOverride } from '../common-behaviors/icon-override';

export interface BannerAction extends SwVideoAction, CanIconOverride { }

export const bannerDefaultAction: Partial<BannerAction> = { url: '', label: '' , target: '', onclick: ''};

/**
 * Notes Carousel
 */
@Component({
  selector: 'sw-notes-carousel',
  templateUrl: './notes-carousel.component.html',
  styleUrls: ['./notes-carousel.component.scss']
})
export class SwNotesCarouselComponent extends SwBaseComponent {
  public readonly slidesByWidth: SwCarouselSlidesPerViewWidth = { xs: 1, sm: 1, md: 2, lg: 3, xl: 4 };
  public readonly paginationMode: PaginationMode = 'show';

  @SwInputProp<string>(DataType.string, '')
  title: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body?: string[];

  @SwInputProp<BannerAction>(DataType.object, bannerDefaultAction, { schema: simpleObjToSchemaConverter(bannerDefaultAction) })
  action: BannerAction;

  @SwInputCallback('_setItems')
  @SwInputProp<SwNotesItemComponent, SwNotesItemComponent[]>(DataType.array, [], { min: 1, type: DataType.object })
  items: ReadonlyArray<SwNotesItemComponent>;
  _items: ReadonlyArray<SwNotesItemComponent> = [];

  _setItems (_items: ReadonlyArray<SwNotesItemComponent>) {
    const items: SwNotesItemComponent[] = JSON.parse(JSON.stringify(_items));
    this._items = items.map(i => Object.assign(i, { variant: 'card' }));
  }

  get _headerProps (): Partial<SwGridHeaderComponent> {
    return {
      title: this.title
    };
  }
}
