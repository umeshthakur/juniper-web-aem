import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { createSampleDataByType } from '../../../core/testing/index.spec';
import { SwNotesComponent, SwNotesModule } from './notes.module';

describe('SwNotesComponent', () => {
  let comp: SwNotesComponent;
  let fixture: ComponentFixture<SwNotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwNotesModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwNotesComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('authorable property', () => {

    describe('`title`', () => {
      const currPropDefault = '';

      it('should take strings', () => {
        const testValues = ['[hello]', '{goodbye}', '', 'My Title', 'Give me a\n\rbreak'];

        testValues.forEach(v => {
          comp.title = v;
          expect(comp.title).toBe(v);
        });
      });

      it(`for any value that isn't a string type`, () => {
        const testValues = createSampleDataByType('string');

        testValues.forEach(v => {
          comp.title = v;
          expect(comp.title).not.toBe(v);
          expect(comp.title).toBe(currPropDefault);
        });
      });
    });

    describe('`body`', () => {
      const currPropDefault = [];

      it('should take string arrays', () => {
        const testValues = [['[hello]', '{goodbye}'], [], [''], ['My Title', 'Give me a\n\rbreak']];

        testValues.forEach(v => {
          comp.body = v;
          expect(comp.body).toEqual(v);
        });
      });

      it(`for any value that isn't a string array`, () => {
        const testValues = createSampleDataByType('array')
          // Remove string arrays and empty arrays
          .filter(i => !Array.isArray(i) || (Array.isArray(i) && i.length > 0 && typeof i[0] !== 'string'));

        testValues.forEach(v => {
          comp.body = v;
          expect(comp.body).not.toEqual(v);
          expect(comp.body).toEqual(currPropDefault);
        });
      });
    });

  });

});
