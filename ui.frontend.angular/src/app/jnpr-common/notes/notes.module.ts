import { DoBootstrap, Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwButtonModule } from '../button/button.component';
import { SwGridHeaderModule } from '../grid-header/grid-header.component';
import { SwIconModule } from '../icon/index';
import { SwBackgroundMediaModule } from '../../modules/backgroundMedia/index';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { SwVideoDialogModule } from '../video/dialog/video-dialog.component';

import { SwNotesComponent } from './notes.component';
import { SwNotesItemComponent } from './notes-item.component';
import { SwNotesLegacyComponent } from './notes-legacy.component';
import { SwNotesItemLegacyComponent } from './notes-item-legacy.component';
import { SwNotesCarouselComponent } from './notes-carousel.component';
import { SwCarouselModule } from '../carousel/carousel.module';
import { createCustomElement } from '@angular/elements';

@NgModule({
  imports: [
    CommonModule,
    SwButtonModule,
    SwCarouselModule,
    SwGridHeaderModule,
    SwIconModule,
    SwBackgroundMediaModule,
    SwEvalModule.forRoot(),
    SwVideoDialogModule
  ],
  exports: [SwNotesComponent, SwNotesLegacyComponent],
  declarations: [
    SwNotesComponent,
    SwNotesItemComponent,
    SwNotesLegacyComponent,
    SwNotesItemLegacyComponent,
    SwNotesCarouselComponent
  ],
  entryComponents: [SwNotesComponent, SwNotesLegacyComponent, SwNotesCarouselComponent]
})
export class SwNotesModule implements DoBootstrap{
  constructor(private injector: Injector) {
    customElements.define('sw-notes', createCustomElement(SwNotesComponent, { injector: this.injector }));
  }
  ngDoBootstrap() {}
}

export { SwNotesComponent } from './notes.component';
export { SwNotesLegacyComponent } from './notes-legacy.component';
export { SwNotesItemComponent } from './notes-item.component';
export { SwNotesItemLegacyComponent } from './notes-item-legacy.component';
export { SwNotesCarouselComponent } from './notes-carousel.component';
