import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { createSampleDataByType } from '../../../core/testing/index.spec';
import { SwNotesLegacyComponent, SwNotesModule } from './notes.module';
import { notesVariantType } from './notes-item-legacy.component';

describe('SwNotesLegacyComponent', () => {
  let comp: SwNotesLegacyComponent;
  let fixture: ComponentFixture<SwNotesLegacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwNotesModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwNotesLegacyComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('authorable property', () => {
    const defaultVariant: notesVariantType = 'simple';

    describe('`variant`', () => {
      const currPropDefault = defaultVariant;
      const validVariants: notesVariantType[] = ['simple','card'];

      describe('should be deemed as valid', () => {
        it('for all allowed variants', () => {
          const testValues = validVariants;

          testValues.forEach(v => {
            comp.variant = v;
            expect(comp.variant).toEqual(v);
          });
        });
      });

      describe('should be deemed as invalid', () => {
        it('for anything but the allowed variants', () => {
          const testValues = ['tertiary', '', 'hello'] as any[];

          testValues.forEach(v => {
            comp.variant = v;
            expect(comp.variant).not.toBe(v);
            expect(comp.variant).toBe(currPropDefault);
          });
        });

        it('for any value that isn\'t a string type', () => {
          const testValues = createSampleDataByType().filter(v => validVariants.indexOf(v) === -1);

          testValues.forEach(v => {
            comp.variant = v;
            expect(comp.variant).not.toBe(v);
            expect(comp.variant).toBe(currPropDefault);
          });
        });
      });

      it('should reset the value to default when setting an incorrect value', () => {
        const correctTestValue = 'card';
        const incorrectTestValue = 'tertiary' as any;

        comp.variant = correctTestValue;
        expect(comp.variant).toEqual(correctTestValue);

        comp.variant = incorrectTestValue;
        expect(comp.variant).not.toEqual(incorrectTestValue);
        expect(comp.variant).toEqual(currPropDefault);
      });
    });

    describe('`title`', () => {
      const currPropDefault = '';

      it('should take strings', () => {
        const testValues = ['[hello]', '{goodbye}', '', 'My Title', 'Give me a\n\rbreak'];

        testValues.forEach(v => {
          comp.title = v;
          expect(comp.title).toBe(v);
        });
      });

      it(`for any value that isn't a string type`, () => {
        const testValues = createSampleDataByType('string');

        testValues.forEach(v => {
          comp.title = v;
          expect(comp.title).not.toBe(v);
          expect(comp.title).toBe(currPropDefault);
        });
      });
    });

    describe('`body`', () => {
      const currPropDefault = [];

      it('should take string arrays', () => {
        const testValues = [['[hello]', '{goodbye}'], [], [''], ['My Title', 'Give me a\n\rbreak']];

        testValues.forEach(v => {
          comp.body = v;
          expect(comp.body).toEqual(v);
        });
      });

      it(`for any value that isn't a string array`, () => {
        const testValues = createSampleDataByType('array')
          // Remove string arrays and empty arrays
          .filter(i => !Array.isArray(i) || (Array.isArray(i) && i.length > 0 && typeof i[0] !== 'string'));

        testValues.forEach(v => {
          comp.body = v;
          expect(comp.body).not.toEqual(v);
          expect(comp.body).toEqual(currPropDefault);
        });
      });
    });

  });

});
