import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { SwBaseComponent } from '../../core/base/index';
import { DataType, SwInputCallback, SwInputProp } from '../../core/validation/index';
import { SwGridHeaderComponent } from '../grid-header/grid-header.component';

import { notesDefaultVariant, notesVariantRegex, notesVariantType, SwNotesItemLegacyComponent } from './notes-item-legacy.component';
import { alignmentType, SW_ALIGNMENT_LEFT, SW_THEME_NONE, themeType } from '../common-behaviors/input-prop-types';

/**
 * Notes Component
 */
@Component({
  selector: 'sw-notes-legacy',
  templateUrl: './notes-legacy.component.html',
  styleUrls: [ './notes-legacy.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwNotesLegacyComponent extends SwBaseComponent {

  @SwInputProp<notesVariantType>(DataType.string, notesDefaultVariant, { pattern: notesVariantRegex.source })
  variant: notesVariantType;

  @SwInputProp<alignmentType>(DataType.string, SW_ALIGNMENT_LEFT)
  headerTextAlign: alignmentType;

  @SwInputProp<themeType>(DataType.string, SW_THEME_NONE)
  headerTheme: themeType;

  @SwInputProp<alignmentType>(DataType.string, SW_ALIGNMENT_LEFT)
  itemAlign: alignmentType;

  @SwInputProp<string>(DataType.string, '')
  title: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body?: string[];

  @SwInputCallback('_setItems')
  @SwInputProp<SwNotesItemLegacyComponent, SwNotesItemLegacyComponent[]>(DataType.array, [], { min: 1, type: DataType.object })
  items: ReadonlyArray<SwNotesItemLegacyComponent>;
  _items: ReadonlyArray<SwNotesItemLegacyComponent> = [];

  /** The maximum columns that the notes component can have */
  @Input() maxCols: number = 4;

  /** Whether we want to show icons on the card headers */
  @Input() showIcons: boolean = true;

  /** The component theme */
  @Input() theme: 'none' | 'light' | 'dark' = 'none';

  _setItems (_items: ReadonlyArray<SwNotesItemLegacyComponent>) {
    const items = JSON.parse(JSON.stringify(_items));
    this._items = items.slice()
      .map(i => Object.assign<SwNotesItemLegacyComponent, Partial<SwNotesItemLegacyComponent>>(i, { variant: this.variant }));
  }

  /** Class to set based on maximum number of columns */
  get _colClass () { return this.maxCols === 3 ? 'notes-col-3' : 'notes-col-4'; }

  get _headerProps (): Partial<SwGridHeaderComponent> {
    return {
      textAlign: this.headerTextAlign,
      theme: this.headerTheme,
      title: this.title,
      body: this.body
    };
  }
}
