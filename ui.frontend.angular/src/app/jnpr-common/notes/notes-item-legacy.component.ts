import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

import { SwBaseComponent } from '../../core/base/index';
import { DataType, SwInputCallback, SwInputProp } from '../../core/validation/index';
import { SwBaseAction } from '../../core/base/action/action.class';
import { isVideo } from '../../util/index';
import { SwDialog } from '../dialog/index';
import { SwVideoDialogComponent } from '../video/dialog/video-dialog.component';
import { ComponentHostType } from '../../core/generics/index';
import { SwVideoEndCardComponent } from '../video/video-end-card.component';
import { CanIconOverride, mixinIconOverride } from '../common-behaviors/icon-override';
import { SwBaseNestedItemList, SwBaseNestedListItem } from '../../core/base/list/list';

export type notesVariantType = 'simple'|'card';
export const notesDefaultVariant: notesVariantType = 'simple';
export const notesVariantRegex = /^(simple|card)$/;

export interface SwNotesItemAction extends SwBaseAction {}

export type SwNotesItemLegacyBodyElementTypeKeys = 'paragraph' | 'list';
export type SwNotesItemLegacyBodyElementProps = { text: string } | { items: SwBaseNestedItemList };

export class SwNotesItemLegacyBodyElement {
  type: SwNotesItemLegacyBodyElementTypeKeys = 'paragraph';
  properties: SwNotesItemLegacyBodyElementProps = { text: '' };

  constructor (obj: Partial<SwNotesItemLegacyBodyElement>|string) {
    if (typeof obj === 'string') {
      this.properties = { text: obj };
      return;
    }

    if (obj && typeof obj === 'object' && !Array.isArray(obj)) {
      const props: any = obj.properties && typeof obj.properties === 'object' && !Array.isArray(obj.properties) ? obj.properties : {};

      if (obj.type === 'list') {
        this.type = obj.type;
        this.properties = { items: SwBaseNestedListItem.parseItemList(props.items) };
      } else {
        this.properties = { text: typeof props.text === 'string' ? props.text : '' };
      }
    }
  }
}

const notesItemHost: ComponentHostType<SwNotesItemLegacyComponent> = {
  '[attr.sw-variant]': 'variant'
};

/**
 * Notes single card
 */
@Component({
  selector: 'sw-notes-item-legacy',
  templateUrl: './notes-item-legacy.component.html',
  styleUrls: [ './notes-item-legacy.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-host-property-decorator
  host: notesItemHost,
  // tslint:disable-next-line use-input-property-decorator
  inputs: ['properties']
})
export class SwNotesItemLegacyComponent extends mixinIconOverride(SwBaseComponent) implements CanIconOverride, SwBaseAction {

  @SwInputProp<string>(DataType.string, notesDefaultVariant, { pattern: notesVariantRegex.source })
  variant: notesVariantType;

  @SwInputProp(DataType.string, '')
  backgroundImage: string;

  @SwInputProp(DataType.string, '')
  label: string;

  @SwInputProp(DataType.string, '')
  url: string;

  @SwInputProp(DataType.object, {})
  endCard: Partial<SwVideoEndCardComponent>;

  @SwInputCallback('_parseBody')
  @SwInputProp<string, string[]>(DataType.array, [], { type: [DataType.string, DataType.object] })
  body?: (string|SwNotesItemLegacyBodyElement)[];
  _body: SwNotesItemLegacyBodyElement[] = [];

  @SwInputProp<SwNotesItemAction, SwNotesItemAction[]>(DataType.array, [], { type: DataType.object })
  actions?: SwNotesItemAction[];

  @SwInputProp(DataType.string, '')
  onclick?: string;

  @SwInputProp(DataType.string, '')
  target?: string;

  /** Whether we want to show icons on the card headers */
  @Input() showIcons: boolean = true;

  /** The component theme */
  @HostBinding('attr.sw-theme')
  @Input() theme: 'none' | 'light' | 'dark' = 'none';

  constructor (private _dialog: SwDialog) {
    super();
  }

  get _hasBackground () { return this.backgroundImage.length > 0; }

  get _hasVideoUrl (): boolean { return isVideo(this.url); }

  get _hasUrl () { return this.url.length > 0; }

  get _validIconOverride (): boolean {
    return !this._hasVideoUrl && this.iconOverride.length > 0;
  }

  get _wrapperClasses (): Object {
    return {
      'has-fab': this._hasVideoUrl || this._validIconOverride,
      'has-background': this._hasBackground
    };
  }

  get _shouldShowDots () { return this.showIcons && this.variant === 'card' && !this._hasBackground; }

  get _isDark () { return this.theme === 'dark'; }

  _handleClick (e: MouseEvent) {
    if (this._hasVideoUrl) {
      e.preventDefault();
      this._openVideoModal();
    }
  }

  private _openVideoModal () {
    const videoConfig: SwVideoDialogComponent = {
      title: this.label,
      config: {
        url: this.url,
        endCard: this.endCard && typeof this.endCard === 'object' ? this.endCard : {},
        options: {
          autoplay: 1
        }
      }
    };
    this._dialog.open(SwVideoDialogComponent, {
      data: videoConfig
    });
  }

  _parseBody (body: (string|SwNotesItemLegacyBodyElement)[]) {
    this._body = body.map(b => new SwNotesItemLegacyBodyElement(b));
  }
}
