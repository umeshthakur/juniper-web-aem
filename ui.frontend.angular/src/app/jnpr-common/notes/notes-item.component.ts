import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { SwBaseComponent } from '../../core/base/index';
import { DataType, SwInputCallback, SwInputProp } from '../../core/validation/index';
import { SwBaseAction } from '../../core/base/action/action.class';
import { isVideo } from '../../util/index';
import { SwDialog } from '../dialog/index';
import { SwVideoDialogComponent } from '../video/dialog/video-dialog.component';
import { SwVideoEndCardComponent } from '../video/video-end-card.component';
import { CanIconOverride, mixinIconOverride } from '../common-behaviors/icon-override';
import { SwBaseNestedItemList, SwBaseNestedListItem } from '../../core/base/list/list';

export interface SwNotesItemAction extends SwBaseAction {}

export type SwNotesItemBodyElementTypeKeys = 'paragraph' | 'list';
export type SwNotesItemBodyElementProps = { text: string } | { items: SwBaseNestedItemList };

export class SwNotesItemBodyElement {
  type: SwNotesItemBodyElementTypeKeys = 'paragraph';
  properties: SwNotesItemBodyElementProps = { text: '' };

  constructor (obj: Partial<SwNotesItemBodyElement>|string) {
    if (typeof obj === 'string') {
      this.properties = { text: obj };
      return;
    }

    if (obj && typeof obj === 'object' && !Array.isArray(obj)) {
      const props: any = obj.properties && typeof obj.properties === 'object' && !Array.isArray(obj.properties) ? obj.properties : {};

      if (obj.type === 'list') {
        this.type = obj.type;
        this.properties = { items: SwBaseNestedListItem.parseItemList(props.items) };
      } else {
        this.properties = { text: typeof props.text === 'string' ? props.text : '' };
      }
    }
  }
}

/**
 * Notes single card
 */
@Component({
  selector: 'sw-notes-item',
  templateUrl: './notes-item.component.html',
  styleUrls: [ './notes-item.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-input-property-decorator
  inputs: ['properties']
})
export class SwNotesItemComponent extends mixinIconOverride(SwBaseComponent) implements CanIconOverride, SwBaseAction {

  @SwInputProp(DataType.string, '')
  backgroundImage: string;

  @SwInputProp(DataType.string, '')
  eyebrow: string;

  @SwInputProp(DataType.string, '')
  label: string;

  @SwInputProp(DataType.string, '')
  url: string;

  @SwInputProp(DataType.string, '')
  callToAction: string;

  @SwInputProp(DataType.object, {})
  endCard: Partial<SwVideoEndCardComponent>;

  @SwInputCallback('_parseBody')
  @SwInputProp<string, string[]>(DataType.array, [], { type: [DataType.string, DataType.object] })
  body?: (string|SwNotesItemBodyElement)[];
  _body: SwNotesItemBodyElement[] = [];

  @SwInputProp<SwNotesItemAction, SwNotesItemAction[]>(DataType.array, [], { type: DataType.object })
  actions?: SwNotesItemAction[];

  @SwInputProp(DataType.string, '')
  onclick?: string;

  @SwInputProp(DataType.string, '')
  target?: string;

  constructor (private _dialog: SwDialog) {
    super();
  }

  get _hasBackground () { return this.backgroundImage.length > 0; }

  get _hasVideoUrl (): boolean { return isVideo(this.url); }

  @HostBinding('class.note--active')
  get _hasUrl () { return this.url.length > 0; }

  get _validIconOverride (): boolean {
    return !this._hasVideoUrl && this.iconOverride.length > 0;
  }

  _handleClick (e: MouseEvent) {
    if (this._hasVideoUrl) {
      e.preventDefault();
      this._openVideoModal();
    }
  }

  private _openVideoModal () {
    const videoConfig: SwVideoDialogComponent = {
      title: this.label,
      config: {
        url: this.url,
        endCard: this.endCard && typeof this.endCard === 'object' ? this.endCard : {},
        options: {
          autoplay: 1
        }
      }
    };
    this._dialog.open(SwVideoDialogComponent, {
      data: videoConfig
    });
  }

  _parseBody (body: (string|SwNotesItemBodyElement)[]) {
    this._body = body.map(b => new SwNotesItemBodyElement(b));
  }
}
