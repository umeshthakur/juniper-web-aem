import { ChangeDetectionStrategy, Component, HostBinding, Input } from '@angular/core';

import { SwBaseComponent } from '../../core/base/index';
import { DataType, SwInputCallback, SwInputProp } from '../../core/validation/index';
import { SwGridHeaderComponent } from '../grid-header/grid-header.component';

import { SwNotesItemComponent } from './notes-item.component';
import { alignmentType, SW_ALIGNMENT_LEFT, SW_THEME_NONE, themeType } from '../common-behaviors/input-prop-types';

@Component({
  selector: 'sw-notes',
  templateUrl: './notes.component.html',
  styleUrls: [ './notes.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwNotesComponent extends SwBaseComponent {

  @SwInputProp<alignmentType>(DataType.string, SW_ALIGNMENT_LEFT)
  headerTextAlign: alignmentType;

  @SwInputProp<themeType>(DataType.string, SW_THEME_NONE)
  headerTheme: themeType;

  @SwInputProp<alignmentType>(DataType.string, SW_ALIGNMENT_LEFT)
  itemAlign: alignmentType;

  @HostBinding('attr.sw-theme')
  @SwInputProp<themeType>(DataType.string, SW_THEME_NONE)
  theme: themeType;

  @SwInputProp<string>(DataType.string, '')
  title: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body?: string[];

  @SwInputCallback('_setItems')
  @SwInputProp<SwNotesItemComponent, SwNotesItemComponent[]>(DataType.array, [], { min: 1, type: DataType.object })
  items: ReadonlyArray<SwNotesItemComponent>;
  _items: ReadonlyArray<SwNotesItemComponent> = [];

  /** The maximum columns that the notes component can have */
  @Input() maxCols: number = 4;

  _setItems (_items: ReadonlyArray<SwNotesItemComponent>) {
    this._items = [..._items];
  }

  /** Class to set based on maximum number of columns */
  get _colClass () { return this.maxCols === 3 ? 'notes-col-3' : 'notes-col-4'; }

  get _headerProps (): Partial<SwGridHeaderComponent> {
    return {
      textAlign: this.headerTextAlign,
      theme: this.headerTheme,
      title: this.title,
      body: this.body
    };
  }
}
