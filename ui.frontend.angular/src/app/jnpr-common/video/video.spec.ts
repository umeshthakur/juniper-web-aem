import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SwVideoComponent, SwVideoModule } from './video.component';
import { TestAgainst } from '../../../core/testing/commonTestCases.spec';

const html5VideoValid = [
  'https://www.quirksmode.org/html5/videos/big_buck_bunny.mp4',
  '//raw.githubusercontent.com/mediaelement/mediaelement-files/master/big_buck_bunny.mp4',
  '/samples/1d-1.mp4'
];

const youtubeVideoValid = [
  'https://www.youtube.com/watch?v=U52dD0tegsA',
  'https://youtu.be/cmGr0RszHc8',
  'https://youtu.be/U52dD0tegsA?t=2m48s',
  'https://www.youtube.com/watch?v=cmGr0RszHc8&feature=youtu.be&t=30s',
  '//youtu.be/U52dD0tegsA',
  '//youtube.com/watch?v=cmGr0RszHc8',
  'youtube.com/watch?v=U52dD0tegsA',
  'youtu.be/cmGr0RszHc8',
  'youtu.be/U52dD0tegsA?t=2m48s'
];

const youkuVideoValid = [
  'https://v.youku.com/v_show/id_XMTg0NTc0NjM2NA==.html?spm=a2hww.20023042.m_223465.5~5~5~5~5~5~A&from=y1.3-idx-beta-1519-23042.223465.1-1',
  'https://player.youku.com/player.php/sid/XMTg0NTc0NjM2NA==/v.swf',
  'https://player.youku.com/embed/XMTg0NTc0NjM2NA==',
  'https://v.youku.com/v_show/id_XMTg1MjIwMjY3Ng==.html'
];

describe('SwVideoComponent', () => {
  let comp: SwVideoComponent;
  let fixture: ComponentFixture<SwVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwVideoModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwVideoComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('authorable property', () => {

    describe('`url`', () => {
      const currPropDefault = '';

      describe('should be deemed as valid', () => {
        it('for an empty value', () => {
          TestAgainst.validValues(comp, 'url', ['']);
        });

        it('for any kind of URL', () => {
          comp.options = { autoplay: 1, poster: 'bar', preload: true } as any;

          const testValues = [
            ...html5VideoValid,
            ...youtubeVideoValid,
            ...youkuVideoValid,
            ...['example.com']
          ];

          TestAgainst.validValues(comp, 'url', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for any value that isn\'t a string', () => {
          TestAgainst.otherDataTypes(comp, 'url', 'string', currPropDefault);
        });
      });
    });

    describe('`options`', () => {
      const currPropDefault = {};

      describe('should be deemed as valid', () => {
        it('for any kind of object', () => {
          const testValues = [
            {},
            { autoplay: true, controls: false, loop: true, muted: false, preload: 'metadata' },
            { autoplay: 1, h1: 'foo', poster: 'bar' }
          ] as any[];
          TestAgainst.validValues(comp, 'options', testValues, TestAgainst.NON_STRICT_EQ);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for any value that isn\'t an object type', () => {
          TestAgainst.otherDataTypes(comp, 'options', 'object', currPropDefault, TestAgainst.NON_STRICT_EQ);
        });
      });

    });

  });

  describe('`aspectRatio` property', () => {
    it('should return a number regardless of a video being set or not', () => {
      expect(typeof comp.aspectRatio).toBe('number');
      expect(comp.aspectRatio).not.toBeNaN();
      expect(comp.aspectRatio).toBe(0);
    });
    it('should return a number for an HTML video', () => {
      comp.url = html5VideoValid[0];
      fixture.detectChanges();
      expect(typeof comp.aspectRatio).toBe('number');
      expect(comp.aspectRatio).not.toBeNaN();
      expect(comp.aspectRatio).toBeGreaterThan(0);
    });
  });

  describe('`play` method', () => {
    it('should not throw regardless of having a video or not', () => {
      expect(() => comp.play()).not.toThrow();
    });
    it('should call the play method for HTML videos', () => {
      comp.url = html5VideoValid[0];
      fixture.detectChanges();
      expect(() => comp.play()).not.toThrow();
    });
  });

  describe('`stop` method', () => {
    it('should not throw regardless of having a video or not', () => {
      expect(() => comp.stop()).not.toThrow();
    });
    it('should call the stop method for HTML videos', () => {
      comp.url = html5VideoValid[0];
      fixture.detectChanges();
      expect(() => comp.stop()).not.toThrow();
    });
  });

});
