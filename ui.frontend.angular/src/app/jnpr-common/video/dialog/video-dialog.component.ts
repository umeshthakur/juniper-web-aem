import { Component, Inject, Input, NgModule } from '@angular/core';

import { SwVideoComponent, SwVideoModule } from '../video.component';
import { SwToolbarDialogModule } from '../../toolbar/dialog/toolbar-dialog.component';
import { SW_DIALOG_DATA } from '../../dialog/index';

@Component({
  selector: 'sw-video-dialog',
  template: `<sw-toolbar-dialog [title]="title"><sw-video [properties]="config"></sw-video></sw-toolbar-dialog>`,
  styleUrls: ['./video-dialog.component.scss']
})
export class SwVideoDialogComponent {
  constructor (@Inject(SW_DIALOG_DATA) data: SwVideoDialogComponent) {
    if (data && data.title && typeof data.title === 'string') this.title = data.title;
    if (data && data.config) this.config = data.config;
  }

  @Input() title: string;
  @Input() config: Partial<SwVideoComponent> = {};
}

@NgModule({
  imports: [SwToolbarDialogModule, SwVideoModule],
  exports: [SwVideoDialogComponent, SwVideoModule],
  declarations: [SwVideoDialogComponent],
  entryComponents: [SwVideoDialogComponent]
})
export class SwVideoDialogModule {}
