import { ChangeDetectionStrategy, Component } from '@angular/core';

import { SwBaseComponent } from '../../core/base';
import { DataType, simpleObjToSchemaConverter, SwInputProp } from '../../core/validation';
import { SwBaseAction } from '../../core/base/action/action.class';

export interface EndCardAction extends SwBaseAction {}
export const endCardDefaultAction: SwBaseAction = { url: '', label: '' };

/**
 * Video End-card Component
 */
@Component({
  selector: 'sw-video-end-card',
  templateUrl: './video-end-card.component.html',
  styleUrls: ['./video-end-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwVideoEndCardComponent extends SwBaseComponent {
  @SwInputProp<string>(DataType.string, '')
  eyebrow: string;

  @SwInputProp<string>(DataType.string, '')
  headline: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body: string[];

  @SwInputProp<EndCardAction>(DataType.object, endCardDefaultAction, {
    schema: simpleObjToSchemaConverter(endCardDefaultAction)
  })
  action: EndCardAction;

  @SwInputProp<string>(DataType.string, '')
  backgroundImage: string;

  _hasStrLen (str: string) {
    return str && str.length && str.length > 0;
  }
}
