import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgModule,
  OnDestroy,
  Output,
  Renderer2,
  ViewChild
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, fromEvent, Observable, of, Subject } from 'rxjs';
import { filter, map, take, takeUntil } from 'rxjs/operators';

import { SwBaseComponent } from '../../core/base';
import { DataType, SwInputCallback, SwInputProp } from '../../core/validation';
import { SwPageDataModule } from '../../modules/pageData/index';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { SwButtonModule } from '../button/button.component';
import { SwBackgroundImageModule } from '../../modules/backgroundMedia/backgroundImage.module';
import { PlatformModule } from '@angular/cdk/platform';

import { isHTML5Video, isYoukuVideo, isYoutubeVideo, SwUrl } from '../../util/index';
import { SwVideoEndCardComponent } from './video-end-card.component';
import { HTMLVideoRef, NoopVideoRef, VideoRef, YoukuVideoRef, YoutubeVideoRef } from './video-ref';
import {
  SW_YOUTUBE_API_REF_PROVIDER,
  SW_YT_PLAYER_FACTORY_PROVIDER,
  SwYouTubePlayerFactory,
  YoutubeEvent,
  YoutubePlayerOptions,
  YoutubePlayerObject
} from './video-reg.service';

const videoSelector = 'sw-video';

enum VideoTypes {
  youtube,
  youku,
  html5
}

export interface HTML5videoOptions {
  autoplay?: boolean;
  controls?: boolean;
  loop?: boolean;
  muted?: boolean;
  preload?: 'none' | 'metadata' | 'auto' | '';
  poster?: string;
  playsinline?: boolean;
  hl?: string;
}

type BuildVideoFn = () => Observable<VideoRef>;

export interface SwVideoEndCard extends Partial<SwVideoEndCardComponent> {}
export type VideoOptions = HTML5videoOptions | YoutubePlayerOptions;

const jnprStateChangeHandler = 'juniperVideoOnPlayerStateChange';

@Component({
  selector: videoSelector,
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwVideoComponent extends SwBaseComponent implements AfterViewInit, OnDestroy {
  constructor (
    private _renderer: Renderer2,
    private _ytFactory: SwYouTubePlayerFactory,
    private _cd: ChangeDetectorRef
  ) {
    super();
  }

  @Output()
  loaded = new EventEmitter<Event | YoutubeEvent>();
  @Output()
  ended = new EventEmitter<Event | YoutubeEvent>();

  @ViewChild('videoContainer', { read: ElementRef })
  _videoContainerRef: ElementRef;

  @SwInputCallback('_setVideo')
  @SwInputProp<string>(DataType.string, '')
  @Input()
  url: string;

  @SwInputCallback('_setVideo')
  @SwInputProp<VideoOptions>(DataType.object, {})
  @Input()
  options: VideoOptions;

  @SwInputProp<SwVideoEndCardComponent>(DataType.object, {})
  endCard: SwVideoEndCard;

  /**
   * The video player reference
   * It will change depending on whether the video's source
   */
  videoRef: VideoRef = new NoopVideoRef();

  /** The video aspect ratio */
  get aspectRatio (): number {
    return this.videoRef.aspectRatio;
  }

  /** The video type */
  get videoType () {
    return VideoTypes[this._videoType];
  }

  /** The end card object */
  get _endCard (): SwVideoEndCard {
    const e = this.endCard;
    return {
      eyebrow: e.eyebrow,
      headline: e.headline,
      body: e.body,
      action: e.action,
      backgroundImage: e.backgroundImage
    };
  }

  /** Whether to display the end card or not */
  _showEndCard: boolean = false;

  /** The class to be placed on the player element */
  private readonly _videoClass = `${videoSelector}__el`;

  /** Stores the video type */
  private _videoType: VideoTypes;

  /** The video container reference */
  private _videoContainer: BehaviorSubject<HTMLElement> = new BehaviorSubject<HTMLElement>(null);

  /** A stream of the video container */
  private _videoContainer$: Observable<any> = this._videoContainer.asObservable().pipe(
    filter(Boolean),
    take(1)
  );

  /** A stream to stop video event subscriptions */
  private _unsubscribe$: Subject<void> = new Subject();

  /** Determines wether the end card is empty or not */
  private get _isEndCardEmpty (): boolean {
    return Object.keys(this.endCard).length === 0;
  }

  ngAfterViewInit () {
    this._videoContainer.next(this._videoContainerRef.nativeElement);
  }

  ngOnDestroy () {
    this._unsubscribe$.next();
  }

  /** Play the video */
  play (): void {
    this.videoRef.play();
  }

  /** Stop the video */
  stop (): void {
    this.videoRef.stop();
  }

  /** Triggers setting the video player */
  _setVideo () {
    if (this.url === '') return;

    switch (true) {
      case isYoutubeVideo(this.url):
        this._setupVideo(VideoTypes.youtube);
        break;
      case isHTML5Video(this.url):
        this._setupVideo(VideoTypes.html5);
        break;
      case isYoukuVideo(this.url):
        this._setupVideo(VideoTypes.youku);
        break;
      default:
        console.warn(`Invalid or incompatible video url passed: ${this.url}.`);
        break;
    }
  }

  /**
   * Sets the video type and does the video container
   * setup depending on said video type
   */
  private _setupVideo (type: VideoTypes) {
    this._videoContainer$.subscribe(v => {
      /** Save the video type in component instance */
      if (type in VideoTypes) this._videoType = type;

      /**
       * Returns a single youtube instance
       */
      const buildYoutube: BuildVideoFn = (): Observable<YoutubeVideoRef> => {
        const ytVideoId = this._extractYoutubeId(this.url);
        const element: HTMLElement = this._renderer.createElement('div');
        this._renderer.appendChild(v, element);

        return this._ytFactory
          .getYoutubePlayer(element, ytVideoId, this.options as YoutubePlayerOptions)
          .pipe(
            filter(Boolean),
            take(1),
            map(vid => {
              const ctrl: any = vid as YoutubePlayerObject;
              const Iframe: any = vid && ctrl.getIframe && ctrl.getIframe();
              return new YoutubeVideoRef({ ctrl: ctrl, ref: Iframe })
            })
          );
      };

      /**
       * Returns a single
       */
      const buildHTML: BuildVideoFn = (): Observable<HTMLVideoRef> => {
        const parsedUrl = new SwUrl(this.url);
        const element: HTMLVideoElement = this._renderer.createElement('video');
        const source: HTMLSourceElement = this._renderer.createElement('source');
        this._renderer.setAttribute(source, 'src', this.url);
        this._renderer.setAttribute(source, 'type', `video/${parsedUrl.fileExtension}`);

        const _op = this.options as HTML5videoOptions;

        Object.keys(_op)
          /** Don't do anything for these keys. See below */
          .filter(prop => prop !== 'controls')
          .forEach(prop => {
            /** Attributes where value is meaningful */
            if (prop === 'preload' || prop === 'poster') {
              this._renderer.setAttribute(element, prop, _op[prop]);

              /** Boolean attributes */
            } else if (coerceBooleanProperty(_op[prop])) {
              this._renderer.setAttribute(element, prop, '');
              element[prop] = true;

              /** Otherwise remove attribute */
            } else {
              this._renderer.removeAttribute(element, prop);
              element[prop] = false;
            }
          });

        /** The controls property should always be set */
        const shouldDisplayControls =
          _op.controls == null || _op.controls === undefined ? true : _op.controls;
        if (shouldDisplayControls) {
          this._renderer.setAttribute(element, 'controls', '');
        } else {
          this._renderer.removeAttribute(element, 'controls');
        }

        this._renderer.appendChild(element, source);
        this._renderer.appendChild(v, element);

        return of(new HTMLVideoRef({ ctrl: element, ref: element }));
      };

      const buildYoukuRes: BuildVideoFn = (): Observable<YoukuVideoRef> => {
        const element: HTMLIFrameElement = this._renderer.createElement('iframe');
        this._renderer.setAttribute(element, 'frameborder', '0');
        this._renderer.setAttribute(element, 'src', this.url);
        this._renderer.appendChild(v, element);

        return of(new YoukuVideoRef({ ctrl: null, ref: element }));
      };

      this._clearVideoContainer();
      this._unsubscribe$.next();

      switch (type) {
        case VideoTypes.youtube:
          buildYoutube().subscribe(res => {
            this.videoRef = res;
            this._setVideoClass(res.ref);
            this._youTubeEventSubscribe();
          });
          break;

        case VideoTypes.html5:
          buildHTML().subscribe(res => {
            this.videoRef = res;
            this._setVideoClass(res.ref);
            this._htmlEventSubscribe();
          });
          break;

        case VideoTypes.youku:
          buildYoukuRes().subscribe(res => {
            this.videoRef = res;
            this._setVideoClass(res.ref);
          });
      }
    });
  }

  /** Sets the video class into a passed element */
  private _setVideoClass (el: HTMLElement) {
    this._renderer.addClass(el, this._videoClass);
  }

  /** Retrieves the Youtube Id from a URL string */
  private _extractYoutubeId (url: string): string {
    const _url = new SwUrl(url);
    return _url.queryObject.v === undefined ? _url.file : _url.queryObject.v;
  }

  /** Subscribes to HTML5 video events */
  private _htmlEventSubscribe () {
    fromEvent<Event>(this.videoRef.ctrl, 'loadedmetadata')
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe((e: Event) => this.loaded.emit(e));

    fromEvent<Event>(this.videoRef.ctrl, 'ended')
      .pipe(takeUntil(this._unsubscribe$))
      .subscribe(e => {
        this.ended.emit();
        this._triggerEndCard();
      });
  }

  /** Subscribes to Youtube video events */
  private _youTubeEventSubscribe () {
    this._ytFactory.youtubeEventState$.pipe(takeUntil(this._unsubscribe$)).subscribe(e => {
      const winVideoHandler = window[jnprStateChangeHandler];

      /** Display end card when relevant */
      try {
        const playerState = this._ytFactory.PlayerState;
        if (e.data === playerState.UNSTARTED) {
          this.loaded.emit(e);
        }
        if (e.data === playerState.ENDED) {
          this.ended.emit();
          this._triggerEndCard();
        }
      } catch (err) {
        console.error(err);
      }

      /** Call window video handler */
      try {
        if (typeof winVideoHandler === 'function') winVideoHandler(e);
      } catch (err) {
        console.error(jnprStateChangeHandler, err.message);
      }
    });
  }

  private _triggerEndCard () {
    this._showEndCard = true && !this._isEndCardEmpty;
    this._cd.detectChanges();
  }

  private _clearVideoContainer () {
    const element = this._videoContainerRef.nativeElement;
    const childCount = element.children.length;

    /**
     * Remove existing child nodes and add the new SVG element. Note that we can't
     * use innerHTML, because IE will throw if the element has a data binding.
     */
    for (let i = 0; i < childCount; i++) {
      if (element.children[i]) {
        this._renderer.removeChild(element, element.children[i]);
      }
    }
  }
}

@NgModule({
  imports: [
    CommonModule,
    SwPageDataModule,
    SwButtonModule,
    SwEvalModule.forRoot(),
    PlatformModule,
    SwBackgroundImageModule
  ],
  exports: [SwVideoComponent],
  declarations: [SwVideoComponent, SwVideoEndCardComponent],
  providers: [SW_YOUTUBE_API_REF_PROVIDER, SW_YT_PLAYER_FACTORY_PROVIDER],
  entryComponents: [SwVideoComponent]
})
export class SwVideoModule {}

export { YoutubePlayerOptions } from './video-reg.service';
