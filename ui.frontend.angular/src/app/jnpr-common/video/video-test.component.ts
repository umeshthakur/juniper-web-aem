import { Component, NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject } from 'rxjs';

import { SwBaseComponent } from '../../../core/base';
import { SwVideoComponent, SwVideoModule } from './video.component';

export interface VideoTestItem {
  title: string;
  url: string;
}

@Component({
  selector: 'sw-video-demo',
  template: `
  <h1>Video test</h1>

  <section>
    <h2>URLs</h2>
    <button *ngFor="let item of items; index as i" (click)="current=i">{{item.title}}</button>
  </section>

  <section>
    <h2>Options</h2>
    <button (click)="_toggleOption('autoplay')">Toggle 'autoplay'</button>
    <button (click)="_toggleOption('controls')">Toggle 'controls'</button>
    <button (click)="_toggleOption('loop')">Toggle 'loop'</button>
    <button (click)="_toggleOption('hl')">Toggle 'hl'</button><br/>
    <button (click)="_nullifyOption('autoplay')">Nullify 'autoplay'</button>
    <button (click)="_nullifyOption('controls')">Nullify 'controls'</button>
    <button (click)="_nullifyOption('loop')">Nullify 'loop'</button>
    <button (click)="_nullifyOption('hl')">Nullify 'hl'</button>
  </section>

  <ng-container *ngIf="videoProps$|async as videoProps">
    <section>
        <pre>{{videoProps|json}}</pre>
        <pre>Aspect Ratio: {{vid.aspectRatio|json}}</pre>
    </section>

    <section>
        <sw-video #vid [properties]="videoProps"></sw-video>
    </section>
  </ng-container>
  `,
  styles: [
    `
    :host {
      display: block;
    }
    h2 {
      margin-bottom: 0.5em;
    }
    section, pre {
      margin-bottom: 2rem;
    }
    sw-video {
      max-width: 80vw;
    }
  `
  ]
})
export class SwVideoTestComponent extends SwBaseComponent implements OnInit {
  autoplay: boolean | null = null;
  controls: boolean | null = null;
  loop: boolean | null = null;
  hl: string | null = null;

  _current: number = 0;
  set current (i: number) {
    if (i in this.items) {
      this._current = i;
      this.setVideoProps();
    }
  }

  readonly videoProps$: BehaviorSubject<Partial<SwVideoComponent>> = new BehaviorSubject({});

  items: VideoTestItem[] = [
    {
      title: 'YouTube from query string',
      url: 'https://www.youtube.com/watch?v=wZXx98Uz4sE'
    },
    {
      title: 'Youtube from short URL (youtu.be)',
      url: 'https://youtu.be/wZXx98Uz4sE'
    },
    {
      title: 'Youtube from embed',
      url: 'https://www.youtube.com/embed/wZXx98Uz4sE'
    },
    {
      title: 'HTML video',
      url: 'https://jnpr-assets.swirl-staging.net/jnpr2/video/city-cinemagraph.mp4'
    },
    {
      title: 'Youku from embed',
      url: 'https://player.youku.com/embed/XMjkzNzkwMTc4NA=='
    }
  ];

  ngOnInit () {
    this.setVideoProps();
  }

  setVideoProps () {
    const props: Partial<SwVideoComponent> = {
      url: this.items[this._current].url,
      options: {
        autoplay: this.autoplay,
        controls: this.controls,
        loop: this.loop,
        hl: this.hl
      }
    };
    this.videoProps$.next(props);
  }

  _toggleOption (key: 'autoplay' | 'controls' | 'loop' | 'hl') {
    if (key === 'hl') {
      this.hl = this.hl === 'fr' ? 'en' : 'fr';
    } else {
      const val = !!this[key];
      this[key] = !val;
    }
    this.setVideoProps();
  }

  _nullifyOption (key: any) {
    this[key] = null;
    this.setVideoProps();
  }

  _pp (val: any) {
    return val && val.toString ? val.toString() : 'null';
  }
}

@NgModule({
  imports: [CommonModule, SwVideoModule],
  exports: [SwVideoTestComponent],
  declarations: [SwVideoTestComponent],
  entryComponents: [SwVideoTestComponent]
})
export class SwVideoTestModule {}
