import { YoutubePlayerObject } from './video-reg.service';

export abstract class VideoRef {
  abstract ctrl;
  abstract ref;
  abstract get aspectRatio (): number;
  abstract play (): void;
  abstract stop (): void;
}

export class NoopVideoRef extends VideoRef {
  ctrl;
  ref;
  get aspectRatio (): number {
    if (!this.ref) return 0;
    return 16 / 9;
  }
  play () {
    /** Noop */
  }
  stop () {
    /** Noop */
  }
}

export class YoutubeVideoRef extends NoopVideoRef {
  constructor (el: Partial<YoutubeVideoRef>) {
    super();
    this.ctrl = el.ctrl;
    this.ref = el.ref;
  }

  ctrl: YoutubePlayerObject;
  ref: HTMLIFrameElement;

  play () {
    if (!this.ctrl) return;
    if (this.ctrl.playVideo) this.ctrl.playVideo();
  }

  stop () {
    if (!this.ctrl) return;
    if (this.ctrl.stopVideo) this.ctrl.stopVideo();
  }
}

export class HTMLVideoRef extends NoopVideoRef {
  constructor (el: Partial<HTMLVideoRef>) {
    super();
    this.ctrl = el.ctrl;
    this.ref = el.ref;
  }

  ctrl: HTMLVideoElement;
  ref: HTMLVideoElement;

  get aspectRatio (): number {
    if (!this.ref) return 0;

    const w = this.ref.videoWidth || 16;
    const h = Math.max(1, this.ref.videoHeight || 9);
    return w / h;
  }

  play () {
    if (!this.ctrl) { return; }
    if (this.ctrl.play) {
      const play = this.ctrl.play();

      if (play && typeof play.catch === 'function') {
        // Catch error if video cannot be autoplayed. See https://goo.gl/xX8pDD.
        play.catch(err => console.warn(err));
      }
    }
  }

  stop () {
    if (!this.ctrl) return;
    if (this.ctrl.pause) this.ctrl.pause();
    if (this.ctrl.currentTime) this.ctrl.currentTime = 0;
  }
}

export class YoukuVideoRef extends NoopVideoRef {
  constructor (el: Partial<YoukuVideoRef>) {
    super();
    this.ctrl = el.ctrl;
    this.ref = el.ref;
  }

  ctrl: null;
  ref: HTMLIFrameElement;
}
