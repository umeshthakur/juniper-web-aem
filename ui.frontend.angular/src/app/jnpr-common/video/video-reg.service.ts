import { Injectable, Optional, SkipSelf } from '@angular/core';
import { BehaviorSubject, forkJoin, Observable, Subject } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

import { SwPageData } from '../../modules/pageData/index';
import { Platform } from '@angular/cdk/platform';
import { getScript } from '../../util/index';
import { SwBaseLocale } from '../../core/base/index';

export interface YoutubePlayerObject {
  cueVideoById: (videoId: string, startSeconds: number, suggestedQuality: string) => void;
  cueVideoByUrl: (mediaContentUrl: string, startSeconds: number, suggestedQuality: string) => void;
  loadVideoById: (videoId: string, startSeconds: number, suggestedQuality: string) => void;
  loadVideoByUrl: (mediaContentUrl: string, startSeconds: number, suggestedQuality: string) => void;
  playVideo: () => void;
  pauseVideo: () => void;
  stopVideo: () => void;
  seekTo: (seconds: number, allowSeekAhead: boolean) => void;
  mute: () => void;
  unMute: () => void;
  isMuted: () => boolean;
  setVolume: (volume: number) => void;
  getVolume: () => number;
  setSize: (width: number, height: number) => Object;
  getPlaybackRate: () => number;
  setPlaybackRate: (suggestedRate: number) => void;
  getAvailablePlaybackRates: () => number[];
  getVideoLoadedFraction: () => number;
  getPlayerState: () => number;
  getCurrentTime: () => number;
  getDuration: () => number;
  getVideoUrl: () => string;
  getVideoEmbedCode: () => string;
  addEventListener: (event: string, listener: string) => void;
  removeEventListener: (event: string, listener: string) => void;
  getIframe: () => HTMLIFrameElement;
  destroy: () => void;
}

export interface YoutubePlayerOptions {
  autoplay?: 0 | 1 | boolean;
  cc_load_policy?: 0 | 1 | boolean;
  controls?: 0 | 1 | 2;
  disablekb?: 0 | 1 | boolean;
  enablejsapi?: 0 | 1 | boolean;
  fs?: 0 | 1 | boolean;
  hl?: string;
  loop?: 0 | 1 | boolean;
  modestbranding?: 0 | 1 | boolean;
  playlist?: string;
  playsinline?: 0 | 1 | boolean;
  rel?: 0 | 1 | boolean;
  showinfo?: 0 | 1 | boolean;
  wmode?: string;
  /**
   * The below are not actual Youtube options.
   * These will be part of a somewhat "extended"
   * set of options that for the most part will
   * leverage player methods vs the above which
   * are generally passed as player params on
   * the constructor.
   */
  muted?: 0 | 1 | boolean;
}

export interface YoutubePlayerState {
  BUFFERING: number;
  CUED: number;
  ENDED: number;
  PAUSED: number;
  PLAYING: number;
  UNSTARTED: number;
}

export interface YoutubeEvent {
  data: number;
  target: YoutubePlayerObject;
}

export abstract class YouTubeAPIRef {
  abstract get loaded (): 0 | 1;
  abstract get loading (): 0 | 1;
  abstract get PlayerState (): YoutubePlayerState;
  abstract get Player (): any;
  abstract set APIReadyCallback (v: Function | null);
  abstract loadAPI (): Promise<void>;
  abstract valueOf (): YouTubeAPIRef;
}

export class BrowserYoutubeAPI extends YouTubeAPIRef {
  constructor (private _platform: Platform) {
    super();
  }
  get loaded (): 0 | 1 {
    return this.YT.loaded;
  }
  get loading (): 0 | 1 {
    return this.YT.loading;
  }
  get PlayerState (): YoutubePlayerState {
    return this.YT.PlayerState;
  }
  get Player (): any {
    return this.YT.Player;
  }
  set APIReadyCallback (v: Function | null) {
    if (this._platform.isBrowser) window['onYouTubeIframeAPIReady'] = v;
  }
  private get YT () {
    return this._platform.isBrowser ? window['YT'] : undefined;
  }
  loadAPI (): Promise<void> {
    return this._platform.isBrowser
      ? getScript('https://www.youtube.com/iframe_api', document)
      : new Promise(() => {
          /** Noop */
      });
  }
  valueOf () {
    return this.YT;
  }
}

@Injectable()
export class SwYouTubePlayerFactory {
  constructor (private _YT: YouTubeAPIRef, private _pageDataSvc: SwPageData) {
    this._checkYoutubeAPI();
  }

  private _youtubeAPIState$: BehaviorSubject<any> = new BehaviorSubject(false);

  private _youtubeEventState: Subject<YoutubeEvent> = new Subject();
  public youtubeEventState$: Observable<YoutubeEvent> = this._youtubeEventState.asObservable().pipe(filter(i => !!i));

  get PlayerState () {
    return this._YT.PlayerState;
  }

  public getYoutubePlayer (
    videoContainer: HTMLElement,
    youtubeId: string,
    options: YoutubePlayerOptions
  ): Observable<YoutubePlayerObject> {
    return new Observable(observer => {
      this._getPageLocale().subscribe(locale => {
        const playerOptions: YoutubePlayerOptions = {
          enablejsapi: 1,
          disablekb: 1,
          cc_load_policy: 1,
          fs: 1,
          modestbranding: 0,
          /**
           * `playlist` should be set to the same single video to allow looping.
           * From https://developers.google.com/youtube/player_parameters#loop:
           * > [The `loop`] parameter has limited support in the AS3 player and
           * > in IFrame embeds, which could load either the AS3 or HTML5 player.
           * > Currently, the loop parameter only works in the AS3 player when
           * > used in conjunction with the playlist parameter. To loop a single
           * > video, set the loop parameter value to 1 and set the playlist
           * > parameter value to the same video ID already specified in the
           * > Player API URL.
           */
          playlist: options.loop == 1 ? youtubeId : '', // tslint:disable-line triple-equals
          playsinline: 1,
          rel: 0,
          showinfo: 0,
          wmode: 'transparent',
          /** From here on are all authorable options for YouTube */
          // tslint:disable triple-equals
          controls: options.controls == 0 ? 0 : 2,
          loop: options.loop == 1 ? 1 : 0,
          autoplay: options.autoplay == 1 ? 1 : 0,
          // tslint:enable triple-equals
          hl: options.hl === undefined ? locale.language : options.hl
        };

        // tslint:disable-next-line no-unused-expression
        new this._YT.Player(videoContainer, {
          videoId: youtubeId,
          playerVars: playerOptions,
          events: {
            onReady: (e: YoutubeEvent) => {
              if (options.muted == 1) e.target.mute(); // tslint:disable-line triple-equals
              observer.next(e.target);
              observer.complete();
            },
            onStateChange: (e: YoutubeEvent) => this._youtubeEventState.next(e)
          }
        });
      });
    });
  }

  private _getPageLocale (): Observable<SwBaseLocale> {
    const pageLocale$ = this._pageDataSvc.fetchMeta().pipe(map(d => d.locale), take(1));
    const youtubeAPIState$ = this._youtubeAPIState$.pipe(filter(s => s), take(1));
    return forkJoin(pageLocale$, youtubeAPIState$).pipe(map(([l]) => l));
  }

  private _setOnYouTubeIframeAPIReady () {
    this._YT.APIReadyCallback = () => {
      this._youtubeAPIState$.next(true);
      this._YT.APIReadyCallback = null;
    };
  }

  private _checkYoutubeAPI (): void {
    /**
     * Load YouTube
     * If the script has loaded but the script isn't done setting up, set API ready callback
     * Otherwise, set state confirming that API is loaded
     */
    !this._YT.valueOf()
      ? this._YT.loadAPI().then(() => this._setOnYouTubeIframeAPIReady())
      : !this._YT.loaded ? this._setOnYouTubeIframeAPIReady() : this._youtubeAPIState$.next(true);
  }
}

export function SW_YOUTUBE_API_REF_PROVIDER_FACTORY (parentApiRef: YouTubeAPIRef, platform: Platform) {
  return parentApiRef || new BrowserYoutubeAPI(platform);
}

export const SW_YOUTUBE_API_REF_PROVIDER = {
  // If there is already a YouTubeAPIRef available, use that. Otherwise, provide a new one.
  provide: YouTubeAPIRef,
  deps: [[new Optional(), new SkipSelf(), YouTubeAPIRef], Platform],
  useFactory: SW_YOUTUBE_API_REF_PROVIDER_FACTORY
};

export function SW_YT_PLAYER_FACTORY_PROVIDER_FACTORY (
  ytPlayerFactory: SwYouTubePlayerFactory,
  ytApiRef: YouTubeAPIRef,
  pageData: SwPageData
) {
  return ytPlayerFactory || new SwYouTubePlayerFactory(ytApiRef, pageData);
}

export const SW_YT_PLAYER_FACTORY_PROVIDER = {
  // If there is already a SwYouTubePlayerFactory available, use that. Otherwise, provide a new one.
  provide: SwYouTubePlayerFactory,
  deps: [[new Optional(), new SkipSelf(), SwYouTubePlayerFactory], YouTubeAPIRef, SwPageData],
  useFactory: SW_YT_PLAYER_FACTORY_PROVIDER_FACTORY
};
