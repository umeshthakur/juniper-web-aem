import { ChangeDetectionStrategy, Component, HostBinding, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwBaseComponent } from '../../core/base';
import { DataType, SwInputProp } from '../../core/validation';
import {
  alignmentRegex,
  alignmentType,
  SW_ALIGNMENT_LEFT,
  SW_THEME_NONE,
  themeRegex,
  themeType
} from '../common-behaviors/input-prop-types';

/**
 * Grid Header
 */
@Component({
  selector: 'sw-grid-header',
  templateUrl: './grid-header.component.html',
  styleUrls: [ './grid-header.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwGridHeaderComponent extends SwBaseComponent {

  @HostBinding('attr.sw-text-align')
  @SwInputProp<alignmentType>(DataType.string, SW_ALIGNMENT_LEFT, { pattern: alignmentRegex.source })
  textAlign: alignmentType;

  @SwInputProp<themeType>(DataType.string, SW_THEME_NONE, { pattern: themeRegex.source })
  theme: themeType;

  @SwInputProp<string>(DataType.string, '')
  title: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body?: string[];

  @HostBinding('attr.sw-theme')
  get _attrTheme () {
    return this.body.length > 0 ? this.theme : SW_THEME_NONE;
  }
}

@NgModule({
  imports: [ CommonModule ],
  exports: [ SwGridHeaderComponent ],
  declarations: [ SwGridHeaderComponent ],
  entryComponents: [ SwGridHeaderComponent ]
})
export class SwGridHeaderModule { }
