import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwBaseComponent } from '../../core/base';
import { DataType, isTypeSchema, simpleObjToSchemaConverter, SwInputProp } from '../../core/validation';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { isVideo } from '../../util/index';
import { SwButtonModule } from '../button/button.component';
import { SwDialog, SwDialogModule } from '../dialog/index';
import { SwIconModule } from '../icon/index';
import { SwVideoDialogComponent, SwVideoDialogModule } from '../video/dialog/video-dialog.component';
import { SwBaseAction, SwVideoAction } from '../../core/base/action/action.class';
import { SwVideoEndCardComponent } from '../video/video-end-card.component';

const listDefaultAction: SwBaseAction = { url: '', label: '' };
export const listItemsSchema: isTypeSchema = {
  items: simpleObjToSchemaConverter(listDefaultAction)
};

export const listButtonColorDefault = 'primary';
export const listButtonColorRegex = /^(dark|invert|primary|accent|warn)$/;

/**
 * List component
 */
@Component({
  selector: 'sw-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SwListComponent extends SwBaseComponent {
  @SwInputProp<SwVideoAction, SwVideoAction[]>(DataType.array, [], { type: DataType.object, schema: listItemsSchema })
  @Input()
  items: SwVideoAction[];

  @SwInputProp<string>(DataType.string, listButtonColorDefault, { pattern: listButtonColorRegex.source })
  @Input()
  buttonColor: string;

  @SwInputProp(DataType.object, {})
  endCard: Partial<SwVideoEndCardComponent>;

  constructor (private _dialog: SwDialog) {
    super();
  }
  _hasLength (val: string | any[]) {
    return val && val.length && val.length > 0;
  }
  handleClick (e: Event, actionItem: SwVideoAction) {
    if (isVideo(actionItem.url)) {
      e.preventDefault();
      this.openVideoModal(actionItem);
    }
  }

  openVideoModal (actionItem: SwVideoAction) {
    const videoConfig: SwVideoDialogComponent = {
      title: actionItem.label,
      config: {
        url: actionItem.url,
        endCard: actionItem.endCard && typeof actionItem.endCard === 'object' ? actionItem.endCard : {},
        options: {
          autoplay: 1
        }
      }
    };
    this._dialog.open(SwVideoDialogComponent, {
      data: videoConfig
    });
  }
}

@NgModule({
  imports: [
    CommonModule,
    SwButtonModule,
    SwEvalModule.forRoot(),
    SwIconModule,
    SwVideoDialogModule,
    SwDialogModule
  ],
  exports: [SwListComponent],
  declarations: [SwListComponent],
  entryComponents: [SwListComponent]
})
export class SwListModule {}
