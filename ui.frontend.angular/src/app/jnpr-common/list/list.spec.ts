import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SwListComponent, SwListModule } from './list.component';
import { createSampleDataByType } from '../../../core/testing/sampleDataByType.spec';

describe('SwListComponent', () => {
  let comp: SwListComponent;
  let fixture: ComponentFixture<SwListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwListModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SwListComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('authorable property', () => {

    describe('`items`', () => {
      const currPropDefault = [];

      describe('should be deemed as valid', () => {
        it('for all allowed object[] types', () => {
          const testValues = [
            [],
            [{ url: 'foo', label: 'bar' }],
            [{ url: 'foo', label: 'bar' }, { url: 'hello', label: 'world' }]
          ];

          testValues.forEach(v => {
            comp.items = v;
            expect(comp.items).toEqual(v);
          });
        });
      });

      describe('should be deemed as invalid', () => {
        it(`for object[] types that don't have the required properties`, () => {
          const testValues = [
            [{}],
            [{ url: 'foo' }, { label: 'world' }],
            [{ fooA: 'bar', fooB: 'bar' }, { fooC: 'bar', fooD: 'bar' }]
          ] as any;

          testValues.forEach(v => {
            comp.items = v;
            expect(comp.items).not.toEqual(v);
            expect(comp.items).toEqual(currPropDefault);
          });
        });

        it('for any value that isn\'t a string type', () => {
          const testValues = createSampleDataByType().filter(v => (Array.isArray(v) && v.length > 0 || !Array.isArray(v)));

          testValues.forEach(v => {
            comp.items = v;
            expect(comp.items).not.toEqual(v);
            expect(comp.items).toEqual(currPropDefault);
          });
        });
      });

      it('should reset the value to default when setting an incorrect value', () => {
        const correctTestValue = [{ url: 'foo', label: 'bar' }];
        const incorrectTestValue = [{ url: 'foo' }, { label: 'world' }] as any;

        comp.items = correctTestValue;
        expect(comp.items).toEqual(correctTestValue);

        comp.items = incorrectTestValue;
        expect(comp.items).not.toEqual(incorrectTestValue);
        expect(comp.items).toEqual(currPropDefault);
      });

    });

  });

});
