import { Component, Inject, Input, NgModule } from '@angular/core';

import { SwListComponent, SwListModule } from '../list.component';
import { SwToolbarDialogModule } from '../../toolbar/dialog/toolbar-dialog.component';
import { SW_DIALOG_DATA } from '../../dialog/index';

/**
 * List Dialog
 */
@Component({
  selector: 'sw-list-dialog',
  template: `<sw-toolbar-dialog [title]="title"><sw-list [properties]="config"></sw-list></sw-toolbar-dialog>`,
  styleUrls: ['./list-dialog.component.scss']
})
export class SwListDialogComponent {
  constructor (@Inject(SW_DIALOG_DATA) data: SwListDialogComponent) {
    if (data && data.title && typeof data.title === 'string') this.title = data.title;
    if (data && data.config) this.config = data.config;
  }

  @Input() title: string;
  @Input() config: Partial<SwListComponent> = {};
}

@NgModule({
  imports: [SwToolbarDialogModule, SwListModule],
  exports: [SwListDialogComponent, SwListModule],
  declarations: [SwListDialogComponent],
  entryComponents: [SwListDialogComponent]
})
export class SwListDialogModule {}
