import { CommonModule } from '@angular/common';
import {
  AfterContentChecked,
  ChangeDetectorRef,
  Component,
  ContentChild,
  ContentChildren,
  Directive,
  ElementRef,
  forwardRef,
  HostBinding,
  Inject,
  Input,
  NgModule,
  OnInit,
  QueryList,
  TemplateRef
} from '@angular/core';

import { SwButtonModule } from '../button/button.component';
import { SwIconModule } from '../icon/index';
import { SwBaseComponent } from '../../core/base/component/baseComponent';
import { Keycodes } from '../../core/keyboard/keycodes';
import { Platform, PlatformModule } from '@angular/cdk/platform';
import { QueryStringService } from '../../modules/browser/query-string.service';
import { SwBreakpointObserver, SwLayoutModule } from '../../modules/layout/index';

/**
 * This directive should be used to wrap tab titles that need to contain HTML markup or other directives.
 */
@Directive({ selector: 'ng-template[swTabTitle]' })
export class SwTabTitleDirective {
  constructor (public templateRef: TemplateRef<any>) {}
}

/**
 * This directive must be used to wrap content to be displayed in a tab.
 */
@Directive({ selector: 'ng-template[swTabContent]' })
export class SwTabContentDirective {
  constructor (public templateRef: TemplateRef<any>) {}
}

@Component({
  selector: 'sw-tab',
  template: `<ng-content></ng-content>`
})
export class SwTabComponent implements OnInit {
  @ContentChild(SwTabContentDirective) contentTpl: SwTabContentDirective;
  @ContentChild(SwTabTitleDirective) titleTpl: SwTabTitleDirective;

  /** Unique tab identifier. Must be unique for the entire document for proper accessibility support. */
  @HostBinding('attr.id')
  @Input()
  set id (id: string) {
    if (typeof id === 'string' && id.length) this._customId = id;
  }
  get id () {
    return this._customId || this.uid;
  }

  uid: string;
  private _customId: string;

  /** Simple (string only) title. Use the "SwTabTitle" directive for more complex use-cases. */
  @Input() title: string;

  constructor (
    // tslint:disable-next-line no-use-before-declare
    @Inject(forwardRef(() => SwTabsetComponent)) public _parentTabset: SwTabsetComponent
  ) {}

  ngOnInit () {
    this.uid = `${this._parentTabset.id}-tab-${this._parentTabset.nextTabId++}`;
  }
}

let nextId = 0;

@Component({
  selector: 'sw-tabset',
  exportAs: 'swTabset',
  templateUrl: './tabset.component.html'
})
export class SwTabsetComponent extends SwBaseComponent implements AfterContentChecked {
  @ContentChildren(SwTabComponent) tabs: QueryList<SwTabComponent>;

  /**
   * An identifier of an initially selected (active) tab. Use the "selectTab" method to switch a tab programmatically.
   */
  @Input() activeId: string;

  /**
   * Whether the active tab should be persisted in the URL query
   */
  @Input() persistToQuery: boolean = false;

  nextTabId: number = 0;

  /**
   * Unique tab identifier. Must be unique for the entire document for proper accessibility support.
   */
  readonly uid = `sw-tabset-${nextId++}`;

  _shouldHover: boolean = true;

  @HostBinding('attr.id')
  @Input()
  set id (id: string) {
    if (typeof id === 'string' && id.length) this._customId = id;
  }
  get id () {
    return this._customId || this.uid;
  }

  private _customId: string;

  constructor (
    private _queryStringSvc: QueryStringService,
    protected _cdr: ChangeDetectorRef,
    protected _el: ElementRef,
    private _platform: Platform,
    protected _breakpointObserver: SwBreakpointObserver
  ) {
    super();

    this._shouldHover = this._platform.IOS || this._platform.ANDROID ? false : true;
  }

  /**
   * Selects the tab with the given id and shows its associated pane.
   * Any other tab that was previously selected becomes unselected and its associated pane is hidden.
   */
  select (tabId: string) {
    const selectedTab = this._getTabById(tabId);
    this.activeId = selectedTab.id;
    this._updateUrl();
    return false;
  }

  ngAfterContentChecked () {
    const idFromQuery = this._queryStringSvc.getUrlOptions()[this.id];

    /** Use id in query if it proves to correlate to a valid tab id */
    if (this._getTabById(idFromQuery)) {
      this.activeId = idFromQuery;
      return;
    }

    /** Auto-correct activeId that might have been set incorrectly as input */
    const activeTab = this._getTabById(this.activeId);
    this.activeId = activeTab ? activeTab.id : (this.tabs.length ? this.tabs.first.id : null);
    this._updateUrl();
  }

  private _getTabById (id: string): SwTabComponent {
    const tabsWithId: SwTabComponent[] = this.tabs.filter(tab => tab.id === id);
    return tabsWithId.length ? tabsWithId[0] : null;
  }

  private _updateUrl () {
    if (this.persistToQuery) {
      this._queryStringSvc.setUrlOptions({
        [this.id]: this.activeId
      });
    }
  }

  private _seekTabToFocus (dir: -1 | 1) {
    try {
      const focusedTab = this._getTabById(document.activeElement.id || this.activeId);
      const tabArr = this.tabs.toArray().slice();
      const focusedIdx = tabArr.indexOf(focusedTab);
      const idToFocus = tabArr[focusedIdx + dir].id;

      if (idToFocus) {
        const el = this._el.nativeElement.querySelector(`#${idToFocus}`);
        if (el && el.focus) el.focus();
      }
    } catch (e) {
      // NoOp
    }
  }

  _onKeyDown (event: KeyboardEvent) {
    // tslint:disable-next-line deprecation
    switch (event.keyCode) {
      case Keycodes.LEFT_ARROW:
      case Keycodes.UP_ARROW:
        this._seekTabToFocus(-1);
        break;
      case Keycodes.RIGHT_ARROW:
      case Keycodes.DOWN_ARROW:
        this._seekTabToFocus(1);
        break;
      // Any other key press is ignored and passed back to the browser.
      default:
        return;
    }
    // The browser might have some native functionality bound to the arrow
    // keys, home or end. The element calls `preventDefault` to prevent the
    // browser from taking any actions.
    event.preventDefault();
  }
}

@NgModule({
  imports: [CommonModule, SwButtonModule, SwIconModule, PlatformModule, SwLayoutModule],
  exports: [SwTabsetComponent, SwTabComponent, SwTabTitleDirective, SwTabContentDirective],
  declarations: [SwTabsetComponent, SwTabComponent, SwTabTitleDirective, SwTabContentDirective],
  entryComponents: [SwTabsetComponent],
  providers: [QueryStringService]
})
export class SwTabsetModule {}
