import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { SwBlockarrowTabsetModule } from '../tabset/blockarrow-tabset.component';
import { SwGlobalModule } from '../../../modules/global/global.module';

// tslint:disable no-angle-bracket-type-assertion

function createGenericTestComponent<T> (html: string, type: { new (...args: any[]): T }): ComponentFixture<T> {
  TestBed.overrideComponent(type, { set: { template: html } });
  const fixture = TestBed.createComponent(type);
  fixture.detectChanges();
  return fixture as ComponentFixture<T>;
}

@Component({ selector: 'sw-test-cmp', template: '' })
class TestComponent {
  activeTabId: string;
}

const createTestComponent = (html: string) =>
  createGenericTestComponent(html, TestComponent) as ComponentFixture<TestComponent>;

describe('blockarrow-tabset', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent],
      imports: [SwBlockarrowTabsetModule, SwGlobalModule.forBrowser()]
    }).compileComponents();
  });

  it('should render tabs with `block` variant by default', () => {
    const fixture = createTestComponent(`
      <sw-blockarrow-tabset>
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-blockarrow-tabset>
    `);

    const el = fixture.debugElement.query(By.css('[sw-variant]')).nativeElement;
    expect(el.getAttribute('sw-variant')).toBe('block');
  });

  it('should render tabs with `arrow` variant', () => {
    const fixture = createTestComponent(`
      <sw-blockarrow-tabset variant="arrow">
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-blockarrow-tabset>
    `);

    const el = fixture.debugElement.query(By.css('[sw-variant]')).nativeElement;
    expect(el.getAttribute('sw-variant')).toBe('arrow');
  });

  it('should render tabs with custom tabset id', () => {
    const fixture = createTestComponent(`
      <sw-blockarrow-tabset id="custom-tabset-id">
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-blockarrow-tabset>
    `);

    const el = fixture.debugElement.children[0].nativeElement;
    expect(el.getAttribute('id')).toBe('custom-tabset-id');
  });
});
