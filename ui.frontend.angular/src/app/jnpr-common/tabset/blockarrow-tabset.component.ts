import { AfterViewInit, Component, forwardRef, Input, NgModule, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { SwTabsetComponent, SwTabsetModule } from './tabset.component';
import { DataType, SwInputProp } from '../../core/validation';
import { SwBreakpoints } from '../../modules/layout/index';

export type blockArrowTabsetThemeType = 'none' | 'light' | 'dark';
const blockArrowTabsetDefaultTheme: blockArrowTabsetThemeType = 'none';
export const blockArrowTabsetThemeRegex = /^(none|light|dark)$/;

export type blockArrowTabsetVariantType = 'block' | 'arrow';
export const blockArrowTabsetDefaultVariant: blockArrowTabsetVariantType = 'block';
export const blockArrowTabsetVariantRegex = /^(block|arrow)$/;

export type blockArrowTabsetOrientationType = 'vertical' | 'horizontal';
export const blockArrowTabsetDefaultOrientation: blockArrowTabsetOrientationType = 'horizontal';
export const blockArrowTabsetOrientationRegex = /^(vertical|horizontal)$/;

@Component({
  selector: 'sw-blockarrow-tabset',
  templateUrl: './blockarrow-tabset.component.html',
  styleUrls: ['./blockarrow-tabset.component.scss'],
  providers: [
    { provide: SwTabsetComponent, useExisting: forwardRef(() => SwBlockarrowTabsetComponent) }
  ]
})
export class SwBlockarrowTabsetComponent extends SwTabsetComponent implements AfterViewInit, OnDestroy {
  /** Used to make broad coloring adjustments */
  @SwInputProp<string>(DataType.string, blockArrowTabsetDefaultTheme, { pattern: blockArrowTabsetThemeRegex.source })
  @Input()
  theme: blockArrowTabsetThemeType = blockArrowTabsetDefaultTheme;

  /** An identifier of a variant which can be blocks or arrows */
  @SwInputProp(DataType.string, blockArrowTabsetDefaultVariant, { pattern: blockArrowTabsetVariantRegex.source })
  @Input()
  variant: blockArrowTabsetVariantType = blockArrowTabsetDefaultVariant;

  /** An identifier of a orientation which can be horizontal or vertical */
  @SwInputProp(DataType.string, blockArrowTabsetDefaultOrientation, { pattern: blockArrowTabsetOrientationRegex.source })
  @Input()
  orientation: blockArrowTabsetOrientationType = blockArrowTabsetDefaultOrientation;

  /** An identifier of whether or not the tabset should stagger on mobile */
  @SwInputProp<boolean>(DataType.boolean, false)
  @Input()
  staggerOnMobile: boolean = false;

  /** Whether to persist the tabset ID to the URL query */
  @SwInputProp(DataType.boolean, false)
  @Input()
  persistToQuery: boolean = false;

  _mobileView: boolean = true;

  private _destroy$ = new Subject<void>();

  ngAfterViewInit () {
    this._breakpointObserver.observe(SwBreakpoints.md.maxCondition)
      .pipe(takeUntil(this._destroy$))
      .subscribe(res => {
        this._mobileView = res.matches;
        this._cdr.markForCheck();
        this._cdr.detectChanges();
      });
  }

  ngOnDestroy () {
    this._destroy$.next();
  }
}

@NgModule({
  imports: [CommonModule, SwTabsetModule],
  exports: [SwBlockarrowTabsetComponent, SwTabsetModule],
  declarations: [SwBlockarrowTabsetComponent]
})
export class SwBlockarrowTabsetModule {}
