import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Injectable } from '@angular/core';
import { SwTabsetModule } from '../tabset/tabset.component';
import { SwGlobalModule } from '../../../modules/global/global.module';
import { IQueryStringService, QueryStringService, QueryStringsObj } from '../../../modules/browser/query-string.service';

// tslint:disable no-angle-bracket-type-assertion

@Component({ selector: 'sw-test-cmp', template: '' })
class TestComponent {
  activeTabId: string;
}

function createGenericTestComponent<T> (html: string, type: { new (...args: any[]): T }): ComponentFixture<T> {
  TestBed.overrideComponent(type, { set: { template: html } });
  const fixture = TestBed.createComponent(type);
  fixture.detectChanges();
  return fixture as ComponentFixture<T>;
}

const createTestComponent = (html: string) =>
  createGenericTestComponent(html, TestComponent) as ComponentFixture<TestComponent>;

function getTabTitles (nativeEl: HTMLElement) {
  return nativeEl.querySelectorAll('.tab a');
}

function getTabContent (nativeEl: HTMLElement) {
  return nativeEl.querySelectorAll('.tab-content .tab-pane');
}

function expectTabs (nativeEl: HTMLElement, active: boolean[]) {
  const tabTitles = getTabTitles(nativeEl);
  const tabContent = getTabContent(nativeEl);
  const anyTabsActive = active.reduce((prev, curr) => prev || curr, false);

  expect(tabTitles.length).toBe(active.length);
  // expect(tabContent.length).toBe(active.length);
  expect(tabContent.length).toBe(anyTabsActive ? active.length : 0);

  for (let i = 0; i < active.length; i++) {
    if (active[i]) {
      expect(tabTitles[i].classList.contains('active')).toBe(true);
    } else {
      expect(tabTitles[i].classList.contains('active')).toBe(false);
    }
  }
}

function getButton (nativeEl: HTMLElement) {
  return nativeEl.querySelectorAll('button');
}

@Injectable()
export class FakeQueryStringService implements IQueryStringService {
  private options: QueryStringsObj = {};

  setUrlOptions (options: QueryStringsObj) {
    this.options = Object.assign({}, this.options, options);
  }

  getUrlOptions (): QueryStringsObj {
    return this.options;
  }
}

describe('sw-tabset', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent],
      imports: [SwTabsetModule, SwGlobalModule.forBrowser()],
      providers: [{ provide: QueryStringService, useClass: FakeQueryStringService }]
    }).compileComponents();
  });

  it('should render tabs and select first tab as active by default', () => {
    const fixture = createTestComponent(`
      <sw-tabset [persistToQuery]="true">
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);

    const service = fixture.debugElement.injector.get<QueryStringService>(QueryStringService);

    const tabTitles = getTabTitles(fixture.nativeElement);
    const tabContent = getTabContent(fixture.nativeElement);

    expect(tabTitles[0].textContent).toMatch(/foo/);
    expect(tabTitles[1].textContent).toMatch(/bar/);
    expect(tabContent.length).toBe(2);
    expect(tabContent[0].textContent).toMatch(/Foo/);
    expectTabs(fixture.nativeElement, [true, false]);

    expect(service).toBeDefined();
    const el = fixture.debugElement.children[0].nativeElement;
    expect(service.getUrlOptions()[el.getAttribute('id')]).toBe(tabTitles[0].id);
  });

  it('should have aria attributes', () => {
    const fixture = createTestComponent(`
      <sw-tabset>
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);

    const compiled: HTMLElement = fixture.nativeElement;
    const tabTitles = getTabTitles(compiled);
    const tabContent = getTabContent(compiled);

    expect(tabTitles[0].getAttribute('role')).toBe('tab');
    expect(tabTitles[0].getAttribute('aria-expanded')).toBe('true');
    expect(tabTitles[0].getAttribute('aria-controls')).toBe(tabContent[0].getAttribute('id'));

    expect(tabContent[0].getAttribute('role')).toBe('tabpanel');
    expect(tabContent[0].getAttribute('aria-expanded')).toBe('true');
    expect(tabContent[0].getAttribute('aria-labelledby')).toBe(tabTitles[0].id);

    expect(tabTitles[1].getAttribute('role')).toBe('tab');
    expect(tabTitles[1].getAttribute('aria-expanded')).toBe('false');
    expect(tabTitles[1].getAttribute('aria-controls')).toBe(tabContent[1].getAttribute('id'));
  });

  it('should allow mix of text and HTML in tab titles', () => {
    const fixture = createTestComponent(`
      <sw-tabset>
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab>
          <ng-template swTabTitle><b>bar</b></ng-template>
          <ng-template swTabContent>Bar</ng-template>
        </sw-tab>
        <sw-tab title="baz">
          <ng-template swTabTitle>baz</ng-template>
          <ng-template swTabContent>Baz</ng-template>
        </sw-tab>
      </sw-tabset>
    `);

    const tabTitles = getTabTitles(fixture.nativeElement);

    expect(tabTitles[0].textContent).toMatch(/foo/);
    expect(tabTitles[1].innerHTML).toMatch(/<b>bar<\/b>/);
    expect(tabTitles[2].textContent).toMatch(/bazbaz/);
  });

  it('should not crash for empty tabsets', () => {
    const fixture = createTestComponent(`<sw-tabset activeId="2"></sw-tabset>`);

    expectTabs(fixture.nativeElement, []);
  });

  it('should mark the requested tab as active', () => {
    const fixture = createTestComponent(`
      <sw-tabset activeId="2" [persistToQuery]="true">
        <sw-tab title="foo" id="1"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar" id="2"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);
    const service = fixture.debugElement.injector.get<QueryStringService>(QueryStringService);
    expectTabs(fixture.nativeElement, [false, true]);
    const el = fixture.debugElement.children[0].nativeElement;
    expect(service.getUrlOptions()[el.getAttribute('id')]).toBe('2');
  });

  it('should auto-correct requested active tab id', () => {
    const fixture = createTestComponent(`
      <sw-tabset activeId="doesntExist">
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);

    expectTabs(fixture.nativeElement, [true, false]);
  });

  it('should auto-correct requested active tab id for undefined ids', () => {
    const fixture = createTestComponent(`
      <sw-tabset [activeId]="activeTabId">
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);

    expectTabs(fixture.nativeElement, [true, false]);
  });

  it('should change active tab on tab title click', () => {
    const fixture = createTestComponent(`
      <sw-tabset>
        <sw-tab title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
    `);

    const tabTitles = getTabTitles(fixture.nativeElement);

    (<HTMLElement>tabTitles[1]).click();
    fixture.detectChanges();
    expectTabs(fixture.nativeElement, [false, true]);

    (<HTMLElement>tabTitles[0]).click();
    fixture.detectChanges();
    expectTabs(fixture.nativeElement, [true, false]);
  });

  it('should change active tab by calling select on an exported directive instance', () => {
    const fixture = createTestComponent(`
      <sw-tabset #myTabSet="swTabset">
        <sw-tab id="myFirstTab" title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab id="mySecondTab" title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
      <button (click)="myTabSet.select('myFirstTab')">Select the first Tab</button>
      <button (click)="myTabSet.select('mySecondTab')">Select the second Tab</button>
    `);

    const button = getButton(fixture.nativeElement);

    // Click on a button to select the second tab
    (<HTMLElement>button[1]).click();
    fixture.detectChanges();
    expectTabs(fixture.nativeElement, [false, true]);

    // Click on a button to select the first tab
    (<HTMLElement>button[0]).click();
    fixture.detectChanges();
    expectTabs(fixture.nativeElement, [true, false]);
  });

  it('should not remove inactive tabs content from DOM', () => {
    const fixture = createTestComponent(`
      <sw-tabset #myTabSet="swTabset">
        <sw-tab id="myFirstTab" title="foo"><ng-template swTabContent>Foo</ng-template></sw-tab>
        <sw-tab id="mySecondTab" title="bar"><ng-template swTabContent>Bar</ng-template></sw-tab>
      </sw-tabset>
      <button (click)="myTabSet.select('mySecondTab')">Select the second Tab</button>
    `);

    const button = getButton(fixture.nativeElement);

    // Click on a button to select the second tab
    (<HTMLElement>button[0]).click();
    fixture.detectChanges();
    const tabContents = getTabContent(fixture.nativeElement);
    expect(tabContents.length).toBe(2);
    // expect(tabContents[1]).toHaveCssClass('active');
    expect(tabContents[1].classList.contains('active')).toBe(true);
  });
});
