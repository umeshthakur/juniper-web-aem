import {
  Directive,
  ElementRef,
  forwardRef,
  Inject,
  Input,
  TemplateRef
} from '@angular/core';
import { SwCarouselComponent } from './carousel.component';

/**
 * Represents an individual slide to be used within a carousel.
 */
@Directive({
  selector: 'ng-template[swCarouselSlide]',
  exportAs: 'swCarouselSlide'
})
export class SwCarouselSlideDirective {
  @Input() index = 0;
  @Input() title;

  private _id: string = '';

  /**
   * Unique slide identifier. Must be unique for the entire document for proper accessibility support.
   * Will be auto-generated if not provided.
   */
  @Input()
  set id (id: string) {
    this._id = `${id}`.trim();
  }
  get id () {
    return this._id.length ? this._id : `${this._parentCarousel.uid}-slide${this.index}`;
  }

  /** Whether the slide is active or not */
  get active (): boolean {
    return this.index === this._parentCarousel.currentIndex;
  }

  /** Whether the slide is visible or not */
  get visible (): boolean {
    return this._parentCarousel._isIndexVisible(this.index);
  }

  constructor (
    public tplRef: TemplateRef<SwCarouselSlideDirective>,
    // tslint:disable-next-line no-use-before-declare
    @Inject(forwardRef(() => SwCarouselComponent))
    private _parentCarousel: SwCarouselComponent
  ) {}
}

/**
 * Represents an individual slide to be used within a carousel.
 */
@Directive({ selector: '[swCarouselSlide]:not(ng-template)' })
export class SwCarouselSlideIxDirective {
  /** Whether an item is visible or not */
  private _visible: boolean;

  /** Sets the visible state */
  @Input()
  set visible (visible: boolean) {
    if (visible !== this._visible) this._setA11yAttrs(visible);
    this._visible = visible;
  }

  constructor (private _el: ElementRef) {}

  /** Sets the relevant focus a11y attributes inside each slide */
  private _setA11yAttrs (visible: boolean) {
    if (!(this._el && this._el.nativeElement)) return;

    const origTabIndexAttr = `data-origTabIndex`;
    const potentiallyFocusable = 'a, input, button, select, textarea, [contenteditable], [tabIndex]';
    const el = this._el.nativeElement;
    const elements = [].slice.call(el.querySelectorAll(potentiallyFocusable));

    if (visible) {
      elements.forEach((el: HTMLElement) => {
        let origTabIndex;
        const tabIndex = el.getAttribute('tabIndex');

        if (tabIndex) {
          origTabIndex = el.getAttribute(origTabIndexAttr);
          /**
           * If theres a tabIndex and we saved a tabIndex, set the saved value.
           * Otherwise, remove the tabIndex.
           */
          if (origTabIndex && origTabIndex !== 'null') {
            el.setAttribute('tabIndex', origTabIndex);
          } else {
            el.removeAttribute('tabIndex');
          }
        }
      });
    } else {
      elements.forEach((el: HTMLElement) => {
        let origTabIndex;
        const tabIndex = el.getAttribute('tabIndex');

        if (tabIndex) {
          origTabIndex = el.getAttribute(origTabIndexAttr);
          /** If there's a tabIndex set and no value had been saved, saved the value */
          if (!origTabIndex) el.setAttribute(origTabIndexAttr, tabIndex);
        } else {
          el.setAttribute(origTabIndexAttr, 'null');
        }
        /** Prevent focus */
        el.setAttribute('tabIndex', '-1');
      });
    }
  }
}
