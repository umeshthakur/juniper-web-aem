import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwButtonModule } from '../button/button.component';
import { SwIconModule } from '../icon/index';
import { SwA11yModule } from '../../modules/a11y/index';

import { SwCarouselComponent } from './carousel.component';
import { SwCarouselSlideDirective, SwCarouselSlideIxDirective } from './carousel-directives';
import { SwLayoutModule } from '../../modules/layout/index';

@NgModule({
  imports: [
    CommonModule,
    SwButtonModule,
    SwIconModule,
    SwA11yModule,
    SwLayoutModule
  ],
  exports: [SwCarouselComponent, SwCarouselSlideDirective],
  declarations: [
    SwCarouselComponent,
    SwCarouselSlideDirective,
    SwCarouselSlideIxDirective
  ],
  entryComponents: [SwCarouselComponent]
})
export class SwCarouselModule {}

export {
  SwCarouselComponent,
  SwCarouselKeyboardNav,
  SwCarouselSlideEvent,
  SwCarouselSlidesPerViewWidth
} from './carousel.component';
