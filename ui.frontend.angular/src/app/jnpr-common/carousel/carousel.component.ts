import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList,
  TrackByFunction,
  ViewChild
} from '@angular/core';
import { animate, AnimationBuilder, AnimationPlayer, keyframes, NoopAnimationPlayer, style } from '@angular/animations';
import { BehaviorSubject, merge, Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { SwBaseComponent } from '../../core/base';
import { ANIM_EASING_IN_OUT_CURVE } from '../../core/animation/animation';
import { SwCarouselSlideDirective } from './carousel-directives';
import { Keycodes } from '../../core/keyboard/keycodes';
import { SwBreakpointNames, SwBreakpointObserver, SwBreakpoints } from '../../modules/layout/index';

/** Keyboard navigation options. */
export enum SwCarouselKeyboardNav {
  OFF, // Disables keyboard navigation
  SLIDE, // Enables keyboard nav per slide
  PAGE // Enables keyboard nav per page
}

/** Event emitted on carousel change */
export interface SwCarouselSlideEvent {
  /** Previous slide id */
  prev: number;
  /** Current slide id */
  current: number;
  // TODO: add direction
}

export type SwCarouselSlidesPerViewWidth = {
  [K in SwBreakpointNames]: number;
};

/** Possible carousel pagination modes. */
export type PaginationMode = 'never' | 'show' | 'always';

export interface SwCarouselPaginationState {
  mode: PaginationMode;
  groups: boolean[];
}

/** Possible politeness levels. */
export type AriaLivePoliteness = 'off' | 'polite' | 'assertive';

const carouselSelector = 'sw-carousel';
let _nextUniqueId = 0;
const defaultDuration = 500;
export const defaultSlideIndex = 0;
export const defaultSlidesPerView = 1;
const defaultPaginationState: SwCarouselPaginationState = {
  mode: 'never',
  groups: []
};

/**
 * Carousel component
 */
@Component({
  selector: carouselSelector,
  templateUrl: 'carousel.component.html',
  styleUrls: ['./carousel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwCarouselComponent extends SwBaseComponent implements AfterViewInit, OnDestroy {
  /** Carousel unique identifier */
  uid = `${carouselSelector}${_nextUniqueId++}`;

  /**
   * Reference to the container around the slides
   * @internal
   */
  @ViewChild('carouselContainer') _carouselContainer: ElementRef;

  /**
   * Reference to the container around the slides
   * @internal
   */
  @ViewChild('liveRegion') _liveRegion: ElementRef;

  /** The slide templates */
  @ContentChildren(SwCarouselSlideDirective) slides: QueryList<SwCarouselSlideDirective>;

  /** The carousel title */
  @Input() title: string = '';

  /** The politeness with which to announce. See `aria-live` */
  @Input() announcePoliteness: AriaLivePoliteness = 'polite';

  /** The role to attach to the carousel */
  @Input() role: string = 'group';

  /** The carousel config */
  @Input() keyboard: SwCarouselKeyboardNav = SwCarouselKeyboardNav.SLIDE;

  /** The initial slide to be set */
  @Input() initialSlide: number = defaultSlideIndex;

  /** The number of slides to be shown in view */
  @Input() slidesPerView: number = defaultSlidesPerView;

  /**
   * An object that describes `viewportKey, slidesPerView` pairs, e.g.,
   * `{ 'xs': 1, 'sm': 1, 'md': 2, 'lg': 3, 'xl': 4 }`
   */
  @Input() slidesPerViewWidth: Partial<SwCarouselSlidesPerViewWidth> = {};

  /** Determines whether the carousel wrapper will be inset relative to the arrow navigation or not */
  @Input() inset: boolean = false;

  /**
   * The size to use for the nav arrows
   * Available values are `xs`, `sm`, `md`, `lg`, `xl`.
   */
  @Input() arrSize: string = 'sm';

  /**
   * Whether the arrows should persistently be shown or not.
   * Regardless of this value, if the number of slides doesn't
   * warrant navigation, the arrows won't be shown.
   */
  @Input() persistArrows: boolean = false;

  @Input()
  set paginationMode (mode: PaginationMode) {
    const modes: PaginationMode[] = ['never', 'show', 'always'];
    const regex = new RegExp(`^(${modes.join('|')})$`);

    if (regex.test(mode)) {
      const mods: Partial<SwCarouselPaginationState> = { mode };
      this._paginationState$.next(Object.assign({}, this._paginationState$.getValue(), mods));
    }
  }

  /**
   * A carousel slide event fired when the slide change is completed.
   * See SwCarouselSlideEvent
   */
  @Output() slideChange = new EventEmitter<SwCarouselSlideEvent>();

  /** Holds the current pagination state */
  readonly _paginationState$ = new BehaviorSubject<SwCarouselPaginationState>(defaultPaginationState);

  /** Returns the current slide */
  get currentIndex (): number {
    return this._currentIndex;
  }

  /** Returns the total number of slides */
  get slideTotal (): number {
    return this.slides ? this.slides.length : 0;
  }

  /** The index for the first slide in the current view */
  get firstSlideInView () {
    return Math.floor(this.currentIndex / this.slidesPerView) * this.slidesPerView;
  }

  /** The index for the last slide in the current view */
  get lastSlideInView () {
    return this.firstSlideInView + this.slidesPerView;
  }

  /**
   * An array with the indexes in view.
   * Depends on getting the information from each slide once it's calculated
   */
  get indexesInView () {
    return this.slides
      ? this.slides.reduce((acc, slide) => slide.visible ? acc.concat(slide.index) : acc, [])
      : [];
  }

  /** The animation player */
  get animPlayer (): AnimationPlayer {
    return this._animPlayer;
  }

  get _attrSlidesPerView () {
    return Math.min(Math.max(this.slidesPerView, 1), 6);
  }

  /** Main message to announce when a screen reader focuses on the carousel. */
  get _mainAnnounce (): string {
    const msg = [];

    /** Title */
    if (typeof this.title === 'string' && this.title.length) {
      msg.push(`${this.title}.`);
    }

    /** Slide items */
    msg.push(`${this.slideTotal} items.`);

    /** Keyboard nav availability */
    if (this.keyboard !== SwCarouselKeyboardNav.OFF && this._shouldDisplayNav) {
      msg.push(`Keyboard navigation available.`);
    }

    return msg.join(' ');
  }

  /**
   * Returns whether nav arrows should be shown at all,
   * regardless of `prev` or `next` awareness.
   */
  get _shouldDisplayNav (): boolean {
    /** Slide total should be more than 1 and above the slides per view */
    return this.slideTotal > 1 && this.slideTotal > this.slidesPerView;
  }

  /** Returns whether the `prev` nav should display */
  get _shouldDisplayPrev (): boolean {
    return (
      /** Nav in general should be visible */
      !this._shouldDisplayNav ||
      /**
       * If `persistArrows` is true, always display.
       * Otherwise only when the first slide in view is above zero
       */
      (this.persistArrows ? true : this.firstSlideInView > 0)
    );
  }

  /** Returns whether the `next` nav should display */
  get _shouldDisplayNext (): boolean {
    return (
      /** Nav in general should be visible */
      !this._shouldDisplayNav ||
      /**
       * If `persistArrows` is true, always display.
       * Otherwise only when the last slide in view
       * is less than the total slides.
       */
      (this.persistArrows ? true : this.firstSlideInView + this.slidesPerView < this.slideTotal)
    );
  }

  /** Returns whether `prev` should be disabled or not */
  get _shouldDisablePrev (): boolean {
    /** If transitioning or the first slide in view is in fact the first slide */
    return this.firstSlideInView === 0;
  }

  /** Returns whether `next` should be disabled or not */
  get _shouldDisableNext (): boolean {
    /** If transitioning or the last slide must be in view */
    return this.firstSlideInView + this.slidesPerView >= this.slideTotal;
  }

  /** A trigger to dispose subscriptions */
  private _dispose$ = new Subject<void>();

  /** Carousel transition state */
  private _isTransitioning: boolean = false;

  /** The current displacement of slides as a number, not a percentage */
  private _currentDisplacement = 0;

  /** The presently tracked slide index */
  private _currentIndex: number = defaultSlideIndex;

  /** The animation player */
  private _animPlayer: AnimationPlayer = new NoopAnimationPlayer();

  /** The current slide object */
  private get _currentSlide (): SwCarouselSlideDirective {
    return this._getSlideAtIndex(this.currentIndex);
  }

  /** The number of slide groups currently available */
  private get _groupCount (): number {
    return !this.slidesPerView ? 0 : Math.ceil(this.slideTotal / this.slidesPerView);
  }

  constructor (
    private _cd: ChangeDetectorRef,
    private _breakpointObserver: SwBreakpointObserver,
    private _builder: AnimationBuilder
  ) {
    super();
  }

  ngOnDestroy () {
    this._dispose$.next();
  }

  ngAfterViewInit () {
    this.slides.changes
      .pipe(takeUntil(this._dispose$))
      .subscribe((slides: QueryList<SwCarouselSlideDirective>) => {
        slides.forEach((s, i) => (s.index = i));
        /** Enable the navigation buttons */
        this._setPaginationState();
        this._setTransitionState(false);
      });
    this.slides.notifyOnChanges();

    /**
     * Observe match media events and use them to set the slides per view,
     * according to the key-value pairs within `slidesPerViewWidth`
     */
    const breakpointChange$ = Object.keys(SwBreakpoints).map(k => {
      const key = <SwBreakpointNames>k;
      const query = SwBreakpoints[key].rangeCondition;
      const name = SwBreakpoints[key].name;
      return this._breakpointObserver.observe(query, name);
    });

    merge(...breakpointChange$)
      .pipe(
        filter(e => e.matches && this.slidesPerViewWidth.hasOwnProperty(e.name)),
        takeUntil(this._dispose$)
      )
      .subscribe(e => {
        this.slidesPerView = this.slidesPerViewWidth[e.name];
        this.goTo(this._currentIndex, 0);
        this._cd.markForCheck();
      });

    /** Use and go to the initial slide */
    this.goTo(this.initialSlide, 0);
  }

  /** Navigates the slider to a specified index. */
  goTo (index: number, duration?: number): void {
    // Return if index is out of bounds
    if (this._isTransitioning || index < 0 || index > this.slideTotal - 1) return;

    // Note: 1ms is the lowest possible duration; zero results in no animation
    duration = typeof duration === 'number' && duration >= 0
      ? Math.max(1, duration)
      : defaultDuration;

    const element = this._carouselContainer.nativeElement;
    const nextPosition = this._getCarouselPosition(index);

    /** Setup animation to go from the current displacement to the next displacement */
    const animFactory = this._builder.build(
      animate(
        `${duration}ms ${ANIM_EASING_IN_OUT_CURVE}`,
        keyframes([
          style({ transform: `translateX(${this._currentDisplacement}%)`, offset: 0 }),
          style({ transform: `translateX(${nextPosition}%)`, offset: 1 })
        ])
      )
    );

    this._animPlayer = animFactory.create(element);
    this._animPlayer.onStart(() => this._onAnimationStart());
    this._animPlayer.onDone(() => this._onAnimationDone(index, nextPosition));
    this._animPlayer.play();
  }

  /** Animation start */
  private _onAnimationStart () {
    this._setTransitionState(true);
  }

  /**
   * Animation Done
   * - Current state is committed
   * - Nav buttons are enabled
   * - Focus is returned to the appropriate element, if applicable
   */
  private _onAnimationDone (index: number, nextPosition: number) {
    /** Save the current displacement */
    this._currentDisplacement = nextPosition;
    /** Emit event */
    this.slideChange.emit({ prev: this._currentIndex, current: index });
    /** Save the current slide index */
    this._currentIndex = index;
    /** Set a non-operational AnimationPlayer */
    this._animPlayer = new NoopAnimationPlayer();
    /** Enable the navigation buttons */
    this._setTransitionState(false);
    /** Commit pagination state */
    this._setPaginationState();
    /** Announce carousel change */
    this._liveAnnounce();
  }

  goToSlideGroup (grpIndex: number) {
    this.goTo(this.slidesPerView * grpIndex);
  }

  /** Handle keydown events in carousel */
  _handleKeyDown (e: KeyboardEvent) {
    const keyboardPageNav = this.keyboard === SwCarouselKeyboardNav.PAGE;

    /** Execute prev key interaction */
    // tslint:disable-next-line deprecation
    if (e.keyCode === Keycodes.UP_ARROW || e.keyCode === Keycodes.LEFT_ARROW) {
      e.preventDefault();
      keyboardPageNav ? this.prevSlideGroup() : this.prevSlide();
    }
    /** Execute next key interaction */
    // tslint:disable-next-line deprecation
    if (e.keyCode === Keycodes.DOWN_ARROW || e.keyCode === Keycodes.RIGHT_ARROW) {
      e.preventDefault();
      keyboardPageNav ? this.nextSlideGroup() : this.nextSlide();
    }
  }

  /** Execute prev arrow interaction */
  arrowPrev () {
    this.prevSlideGroup();
  }

  /** Execute next arrow interaction */
  arrowNext () {
    this.nextSlideGroup();
  }

  /** Navigates to the next set of slides, if applicable */
  nextSlide () {
    this.goTo(this.currentIndex + 1);
  }

  /** Navigates to the previous set of slides, if applicable */
  prevSlide () {
    this.goTo(this.currentIndex - 1);
  }

  /** Navigates to the next set of slides, if applicable */
  nextSlideGroup () {
    if (!this._shouldDisplayNext && this._shouldDisableNext) return;
    this.goTo(this.firstSlideInView + this.slidesPerView);
  }

  /** Navigates to the previous set of slides, if applicable */
  prevSlideGroup () {
    if (!this._shouldDisplayPrev && this._shouldDisablePrev) return;
    this.goTo(this.firstSlideInView - this.slidesPerView);
  }

  /**
   * Announces a message to screenreaders.
   * @param message Message to be announced to the screenreader
   * @param politeness The politeness of the announcer element
   */
  _liveAnnounce (): void {
    if (!this._liveRegion || !this._currentSlide) return;

    /** Announcer */
    const _liveRegion: HTMLElement = this._liveRegion.nativeElement;

    const slide = this._currentSlide;
    const slideTitle = slide.title ? `. ${slide.title}` : '';
    const message = `${slide.index + 1} of ${this.slideTotal}${slideTitle}`;

    /** Clear text */
    _liveRegion.textContent = '';

    /**
     * This 100ms timeout is necessary for some browser + screen-reader combinations:
     * - Both JAWS and NVDA over IE11 will not announce anything without a non-zero timeout.
     * - With Chrome and IE11 with NVDA or JAWS, a repeated (identical) message won't be read a
     *   second time without clearing and then using a non-zero delay.
     */
    setTimeout(() => (_liveRegion.textContent = message), 100);
  }

  /**
   * Checks whether a provided index is visible given the current state
   * @param i - A slide's index
   */
  _isIndexVisible (i: number): boolean {
    return i >= this.firstSlideInView && i < this.lastSlideInView;
  }

  /**
   * Will determine whether pagination is displayed
   * `never` - Won't display under any circumstance
   * `show` - Will only show when pagination is applicable
   * `always` - Will show regardless of circumstance
   */
  _shouldShowPagination (mode: PaginationMode) {
    switch (mode) {
      case 'always':
        return true;
      case 'show':
        return this._groupCount && this._groupCount > 1 ? true : false;
      default:
        return false;
    }
  }

  /**
   * Returns the carousel displacement as a percentage
   * number (no '%' symbol) based on the passed index
   */
  private _getCarouselPosition (index: number = this._currentIndex): number {
    let pos = this.slideTotal <= this.slidesPerView ? 0 : Math.floor(index / this.slidesPerView) * -100;
    pos = pos < 0 ? pos : 0;
    return pos;
  }

  /** Sets the transition state: enabled or disabled */
  private _setTransitionState (state: boolean) {
    this._isTransitioning = state;
    this._cd.markForCheck();
    this._cd.detectChanges();
  }

  private _setPaginationState () {
    const currentGroup = this.firstSlideInView / this.slidesPerView;
    const mods: Partial<SwCarouselPaginationState> = { groups: Array<boolean>(this._groupCount).fill(false) };
    mods.groups[currentGroup] = true;
    this._paginationState$.next(Object.assign({}, this._paginationState$.getValue(), mods));
    this._cd.markForCheck();
    this._cd.detectChanges();
  }

  private _getSlideAtIndex (i: number): SwCarouselSlideDirective {
    return this.slides ? this.slides.toArray()[i] : null;
  }

  readonly _paginationTrackByIdx: TrackByFunction<boolean> = (idx: number, item: boolean) => idx;
}
