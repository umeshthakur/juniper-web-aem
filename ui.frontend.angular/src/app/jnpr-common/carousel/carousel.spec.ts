import { ChangeDetectorRef, ElementRef, QueryList } from '@angular/core';
import { AnimationBuilder, AnimationFactory, NoopAnimationPlayer } from '@angular/animations';
import { async } from '@angular/core/testing';
import { MediaMatcher } from '@angular/cdk/layout';
import { Platform } from '@angular/cdk/platform';
import { take } from 'rxjs/operators';

import { defaultSlideIndex, SwCarouselComponent } from './carousel.component';
import { SwCarouselSlideDirective } from './carousel-directives';
import { SwBreakpointObserver } from '../../../modules/layout/index';
import { MockNgZone } from '../../../core/testing/cdk/index';

// tslint:disable no-empty
class NoopChangeDetectorRef extends ChangeDetectorRef {
  markForCheck (): void {}
  detach (): void {}
  detectChanges (): void {}
  checkNoChanges (): void {}
  reattach (): void {}
}

class NoopAnimationFactory extends AnimationFactory {
  create (): NoopAnimationPlayer { return new NoopAnimationPlayer(); }
}

class NoopAnimationBuilder extends AnimationBuilder {
  build (): AnimationFactory { return new NoopAnimationFactory(); }
}

/** Creates a mock set of QueryList items */
function mockQueryListResults (length: number = 0, parent: SwCarouselComponent): SwCarouselSlideDirective[] {
  const tpl: any = {};
  return Array(length).fill(new SwCarouselSlideDirective(tpl, parent));
}
// tslint:enable no-empty

describe('SwCarouselComponent', () => {
  const _breakpointObserver = new SwBreakpointObserver(new MediaMatcher(new Platform()), new MockNgZone());
  const _cdr = new NoopChangeDetectorRef();
  const _builder = new NoopAnimationBuilder();

  describe('in isolation', () => {
    let c: SwCarouselComponent;

    beforeEach(() => {
      c = new SwCarouselComponent(_cdr, _breakpointObserver, _builder);
      c._carouselContainer = new ElementRef({});
      c.slides = new QueryList<SwCarouselSlideDirective>();
      c.slides.reset([]);
    });

    it('should track correctly when there\'s no items', () => {
      expect(c.currentIndex).toBe(0, 'Failed on current slide initial test');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide initial test');

      c.arrowNext();
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after one `next`');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after one `next`');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after one `next`');

      c.arrowNext();
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after second `next`');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after second `next`');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after second `next`');

      c.arrowPrev();
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after first `prev`');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after first `prev`');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after first `prev`');

      c.arrowPrev();
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after second `prev`');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after second `prev`');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after second `prev`');

      c.goTo(2);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after `goTo` 2');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after `goTo` 2');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after `goTo` 2');

      c.goTo(0);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(0, 'Failed on current slide after `goTo` 0');
      expect(c.firstSlideInView).toBe(0, 'Failed on first slide after `goTo` 0');
      expect(c._shouldDisablePrev && c._shouldDisableNext)
        .toBe(true, 'Failed on disabled buttons after `goTo` 0');
    });

    it('should track correctly with one slide per view', () => {
      let i = defaultSlideIndex;
      c.slides.reset(mockQueryListResults(3, c));

      expect(c.currentIndex).toBe(i, 'Failed on current slide initial test');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide initial test');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` initial test');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` initial test');

      c.arrowNext();
      c.animPlayer.finish();
      i++;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after first `next`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after first `next`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after first `next`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after first `next`');

      c.arrowNext();
      c.animPlayer.finish();
      i++;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after second `next`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after second `next`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after second `next`');
      expect(c._shouldDisableNext).toBe(true, 'Failed for `next` after second `next`');

      c.arrowPrev();
      c.animPlayer.finish();
      i--;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after first `prev`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after first `prev`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after first `prev`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after first `prev`');

      c.arrowPrev();
      c.animPlayer.finish();
      i--;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after second `prev`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after second `prev`');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` after second `prev`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after second `prev`');

      i = 2;
      c.goTo(i);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` 2');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after `goTo` 2');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after `goTo` 2');
      expect(c._shouldDisableNext).toBe(true, 'Failed for `next` after `goTo` 2');

      i = 0;
      c.goTo(i);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` 0');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after `goTo` 0');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` after `goTo` 0');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after `goTo` 0');
    });

    it('should track correctly with multiple slides per view', () => {
      let i = defaultSlideIndex;
      const slidesPerView = 3;
      c.slidesPerView = slidesPerView;
      c.slides.reset(mockQueryListResults(7, c));

      expect(c.currentIndex).toBe(i, 'Failed on current slide initial test');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide initial test');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` initial test');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` initial test');

      c.arrowNext();
      c.animPlayer.finish();
      i += slidesPerView;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after first `next`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after first `next`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after first `next`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after first `next`');

      c.arrowNext();
      c.animPlayer.finish();
      i += slidesPerView;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after second `next`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after second `next`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after second `next`');
      expect(c._shouldDisableNext).toBe(true, 'Failed for `next` after second `next`');

      c.arrowPrev();
      c.animPlayer.finish();
      i -= slidesPerView;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after first `prev`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after first `prev`');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after first `prev`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after first `prev`');

      c.arrowPrev();
      c.animPlayer.finish();
      i -= slidesPerView;
      expect(c.currentIndex).toBe(i, 'Failed on current slide after second `prev`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after second `prev`');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` after second `prev`');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after second `prev`');

      i = slidesPerView + 1;
      c.goTo(i);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` 4');
      expect(c.firstSlideInView).toBe(slidesPerView, 'Failed on first slide after `goTo` 4');
      expect(c._shouldDisablePrev).toBe(false, 'Failed for `prev` after `goTo` 4');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after `goTo` 4');

      i = 0;
      c.goTo(i);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` 0');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after `goTo` 0');
      expect(c._shouldDisablePrev).toBe(true, 'Failed for `prev` after `goTo` 0');
      expect(c._shouldDisableNext).toBe(false, 'Failed for `next` after `goTo` 0');
    });

    it('should not navigate while transitioning', () => {
      const i = defaultSlideIndex;
      c.slides.reset(mockQueryListResults(7, c));

      /** Execute directive to go to `2` */
      c.goTo(2, 5);
      /** Execute directive to go to `1`. shouldn't do anything. */
      c.goTo(1);
      expect(c.currentIndex).toBe(i, 'Failed on current slide after first `next`');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after first `next`');

      c.animPlayer.finish();
      expect(c.currentIndex).toBe(2, 'Failed on current slide after second `next`');
      expect(c.firstSlideInView).toBe(2, 'Failed on first slide after second `next`');
    });

    it('should not navigate when passed an index out of bounds', () => {
      const i = defaultSlideIndex;
      const numberOfSlides = 7;
      c.slides.reset(mockQueryListResults(numberOfSlides, c));

      c.goTo(numberOfSlides + 1);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` attempt to a too-high index');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after `goTo` attempt to a too-high index');

      c.goTo(-1);
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(i, 'Failed on current slide after `goTo` attempt to a too-low index');
      expect(c.firstSlideInView).toBe(i, 'Failed on first slide after `goTo` attempt to a too-low index');
    });

    it('should use and go to the initial slide, if any is passed', () => {
      const numberOfItems = 7;
      const items = mockQueryListResults(numberOfItems, c);
      const lastItemIdx = items.length - 1;
      c.slides.reset(items);
      c.initialSlide = lastItemIdx;

      expect(c.currentIndex).toBe(defaultSlideIndex, 'Failed on current slide before `ngAfterViewInit`');
      expect(c.firstSlideInView).toBe(defaultSlideIndex, 'Failed on first slide before `ngAfterViewInit`');
      c.ngAfterViewInit();
      c.animPlayer.finish();
      expect(c.currentIndex).toBe(lastItemIdx, 'Failed on current slide after `ngAfterViewInit`');
      expect(c.firstSlideInView).toBe(lastItemIdx, 'Failed on first slide after `ngAfterViewInit`');
      c.ngAfterViewInit();
    });

    describe('pagination', () => {
      const defaultPaginationMode = 'never';

      it('should be `never` with no groups by default', async(() => {
        c._paginationState$.subscribe(s => {
          expect(s.mode).toBe(defaultPaginationMode);
          expect(s.groups).toEqual([]);
        });
      }));

      it('should be `show` when set to `show`', () => {
        c.paginationMode = 'show';
        c._paginationState$.subscribe(s => {
          expect(s.mode).toBe('show');
          expect(s.mode).not.toBe(defaultPaginationMode);
        });
      });

      it('should be `always` when set to `always`', () => {
        c.paginationMode = 'always';
        c._paginationState$.subscribe(s => {
          expect(s.mode).toBe('always');
          expect(s.mode).not.toBe(defaultPaginationMode);
        });
      });

      it('should be `never` when a disallowed value is set', () => {
        c.paginationMode = 'none' as any;
        c._paginationState$.subscribe(s => {
          expect(s.mode).toBe(defaultPaginationMode);
          expect(s.mode).not.toBe('none');
        });
      });

      describe('should show the correct amount of groups', () => {
        it('when there\'s a single slide per view', () => {
          c.ngAfterViewInit();
          c.paginationMode = 'always';
          c.slides.reset(mockQueryListResults(3, c));
          c.slides.notifyOnChanges();

          c._paginationState$
            .pipe(take(1))
            .subscribe(s => {
              expect(s.mode).toBe('always');
              expect(s.groups.length).toBe(3);
            });
        });

        it('when there\'s a more than one slide per view', () => {
          c.ngAfterViewInit();
          c.slidesPerView = 3;
          c.paginationMode = 'always';
          c.slides.reset(mockQueryListResults(7, c));
          c.slides.notifyOnChanges();

          c._paginationState$
            .pipe(take(1))
            .subscribe(s => {
              expect(s.mode).toBe('always');
              expect(s.groups.length).toBe(3);
            });
        });
      });

      describe('should update state upon navigation', () => {
        it('when goTo() is called', () => {
          c.ngAfterViewInit();
          c.paginationMode = 'always';
          c.slides.reset(mockQueryListResults(3, c));
          c.slides.notifyOnChanges();
          c.goTo(1);
          c.animPlayer.finish();

          c._paginationState$
            .pipe(take(1))
            .subscribe(s => {
              expect(s.groups.indexOf(true)).toBe(1);
            });
        });

        it('when next arrowNext() is called', () => {
          c.ngAfterViewInit();
          c.paginationMode = 'always';
          c.slides.reset(mockQueryListResults(3, c));
          c.slides.notifyOnChanges();
          c.arrowNext();
          c.animPlayer.finish();

          c._paginationState$
            .pipe(take(1))
            .subscribe(s => {
              expect(s.groups.indexOf(true)).toBe(1);
            });
        });

        it('when next arrowPrev() is called', () => {
          c.ngAfterViewInit();
          c.paginationMode = 'always';
          c.slides.reset(mockQueryListResults(3, c));
          c.slides.notifyOnChanges();
          c.goTo(2);
          c.animPlayer.finish();
          c.arrowPrev();
          c.animPlayer.finish();

          c._paginationState$
            .pipe(take(1))
            .subscribe(s => {
              expect(s.groups.indexOf(true)).toBe(1);
            });
        });
      });

    });

  });

});
