import { Component, forwardRef, Inject, Input, NgModule } from '@angular/core';

import { SwEditorialModule } from '../editorial.module';
import { SW_EDITORIAL_PARENT, SwEditorialComponent } from '../editorial.component';
import { SwToolbarDialogModule } from '../../toolbar/dialog/toolbar-dialog.component';
import { SW_DIALOG_DATA } from '../../dialog/index';

@Component({
  selector: 'sw-editorial-dialog',
  template: `<sw-toolbar-dialog><sw-editorial [properties]="config"></sw-editorial></sw-toolbar-dialog>`,
  styleUrls: ['./editorial-dialog.component.scss'],
  providers: [
    { provide: SW_EDITORIAL_PARENT, useExisting: forwardRef(() => SwEditorialDialogComponent) }
  ]
})
export class SwEditorialDialogComponent {
  constructor (@Inject(SW_DIALOG_DATA) data: SwEditorialDialogComponent) {
    if (data && data.title && typeof data.title === 'string') this.title = data.title;
    if (data && data.config) this.config = data.config;
  }

  @Input() title: string;
  @Input() config: Partial<SwEditorialComponent> = {};
}

@NgModule({
  imports: [SwToolbarDialogModule, SwEditorialModule],
  exports: [SwEditorialDialogComponent, SwToolbarDialogModule, SwEditorialModule],
  declarations: [SwEditorialDialogComponent],
  entryComponents: [SwEditorialDialogComponent]
})
export class SwEditorialDialogModule {}
