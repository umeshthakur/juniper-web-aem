import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { SwEditorialComponent, SwEditorialItemDirective } from './editorial.component';
import { SW_EDITORIAL_ELEMENT_MAP_PROVIDER, SW_EDITORIAL_ELEMENT_MULTI_PROVIDER, SwEditorialElementMapType } from './editorial-providers';

@NgModule({
  imports: [CommonModule, SwEvalModule.forRoot()],
  exports: [SwEditorialComponent],
  declarations: [SwEditorialComponent, SwEditorialItemDirective],
  entryComponents: [SwEditorialComponent],
  providers: [SW_EDITORIAL_ELEMENT_MULTI_PROVIDER]
})
export class SwEditorialModule {
  static withComponentMap (elementMap: SwEditorialElementMapType): ModuleWithProviders<any> {
    return {
      ngModule: SwEditorialModule,
      providers: [SW_EDITORIAL_ELEMENT_MAP_PROVIDER(elementMap)]
    };
  }
}
