import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  forwardRef,
  HostBinding,
  Inject,
  InjectionToken,
  Input,
  OnChanges,
  OnInit,
  Optional,
  SkipSelf,
  Type,
  ViewContainerRef
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';

import { SwBaseComponent } from '../../core/base';
import { DataType, isTypeSchema, SwInputProp } from '../../core/validation';
import { editorialTextAlignDefault, editorialTextAlignRegex, editorialTextAlignType, SwEditorialElement } from './directives/index';
import { SW_EDITORIAL_ELEMENT_MULTI, SwEditorialElementMapType } from './editorial-providers';

export const DynamicEditorialItemsSchema: isTypeSchema = {
  items: {
    props: {
      type: <isTypeSchema>{ type: DataType.string, required: true },
      properties: <isTypeSchema>{ type: DataType.object }
    }
  } as isTypeSchema
};

const editorialSelector = 'sw-editorial';
let _editorialUniqueIdCounter = 0;

export type editorialThemeType = 'none' | 'light' | 'dark';
const editorialDefaultTheme: editorialThemeType = 'none';
export const editorialThemeRegex = /^(none|light|dark)$/;

export type editorialWidthType = 'full' | 'condensed';
export const editorialWidthDefault: editorialWidthType = 'full';
export const editorialWidthRegex = /^(full|condensed)$/;

export const SW_EDITORIAL_PARENT = new InjectionToken<Type<any>>('SW_EDITORIAL_PARENT');

export interface EditorialItem {
  type: string;
  properties?: object;
}

/**
 * Instantiates the correct editorial component being authored based on an injected map
 */
@Directive({
  selector: '[swEditorialItem]'
})
export class SwEditorialItemDirective implements EditorialItem, OnChanges, OnInit {
  @Input() index: number;
  @Input() group: string;
  @Input() groupTheme: editorialThemeType;
  @Input() groupTextAlign: editorialTextAlignType;

  get uid (): string {
    return `${this.group}-item${this.index}`;
  }

  /**
   * The `type` of component that should be dynamically created.
   * The allowed types will depend on the injected keys in the `_elementMap`
   */
  @Input() type: string;

  @Input() prevType: string;

  @Input() properties: object = {};

  component: ComponentRef<SwEditorialElement>;

  constructor (
    @Inject(SW_EDITORIAL_ELEMENT_MULTI) private _elementMap: SwEditorialElementMapType,
    private _cfr: ComponentFactoryResolver,
    private _vcr: ViewContainerRef
  ) {}

  ngOnChanges () {
    this._setComponentProps();
  }

  ngOnInit () {
    if (!this._elementMap[this.type]) {
      const supportedTypes = Object.keys(this._elementMap).join('|');
      console.warn(
        `Attempted to use an unsupported editorial type \`${this.type}\`. Supported types: ${supportedTypes}.`
      );
      return;
    }
    const component = this._cfr.resolveComponentFactory<SwEditorialElement>(this._elementMap[this.type]);
    this.component = this._vcr.createComponent(component);
    this._setComponentProps();
  }

  private _setComponentProps () {
    if (this.component) {
      this.component.instance.uid = this.uid;
      this.component.instance.groupTheme = this.groupTheme;
      this.component.instance.groupTextAlign = this.groupTextAlign;
      this.component.instance.prevType = this.prevType;
      this.component.instance.properties = this.properties;
    }
  }
}

/**
 * Root functionality to the Editorial component
 * Based on the items that it's given, it instantiates Editorial Elements
 */
@Component({
  selector: editorialSelector,
  template: `
    <div class="wrapper">
      <ng-container
        *ngFor="let i of items; let idx = index"
        swEditorialItem
        [group]="uid"
        [groupTheme]="theme"
        [groupTextAlign]="textAlign"
        [index]="idx"
        [prevType]="_getPrevType(idx)"
        [type]="i.type"
        [properties]="i.properties">
      </ng-container>
    </div>
  `,
  styleUrls: ['./editorial.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    { provide: SW_EDITORIAL_PARENT, useExisting: forwardRef(() => SwEditorialComponent) }
  ]
})
export class SwEditorialComponent extends SwBaseComponent {
  constructor (
    @Optional() @SkipSelf() @Inject(SW_EDITORIAL_PARENT) public _parent: Type<any>
  ) {
    super();
    this._nested = coerceBooleanProperty(_parent) || null;
  }

  uid: string = `${editorialSelector}-group${_editorialUniqueIdCounter++}`;

  /** Used to make broad coloring adjustments */
  @HostBinding('attr.sw-theme')
  @SwInputProp<string>(DataType.string, editorialDefaultTheme, { pattern: editorialThemeRegex.source })
  theme: editorialThemeType;

  /** The text alignment that the component as a whole should start off with */
  @SwInputProp<string>(DataType.string, editorialTextAlignDefault, { pattern: editorialTextAlignRegex.source })
  textAlign: editorialTextAlignType;

  /** The text alignment that the component as a whole should start off with */
  @HostBinding('attr.sw-width')
  @SwInputProp<string>(DataType.string, editorialWidthDefault, { pattern: editorialWidthRegex.source })
  width: editorialWidthType;

  /** A series objects that describe editorial items to be instantiated */
  @SwInputProp(DataType.array, [], { type: DataType.object, schema: DynamicEditorialItemsSchema })
  items: EditorialItem[];

  @HostBinding('attr.sw-nested')
  _nested: boolean | null;

  /** Returns the previous editorial item type */
  public _getPrevType (idx: number): string|null {
    const item = this.items[idx - 1];
    return (item ? item.type : null);
  }
}
