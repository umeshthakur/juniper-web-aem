export { SwEditorialModule } from './editorial.module';
export { SwEditorialItemsModule } from './editorial-items.module';
export { SW_EDITORIAL_ELEMENT_MAP_PROVIDER } from './editorial-providers';
export { SwEditorialComponent, SW_EDITORIAL_PARENT } from './editorial.component';
export { SwEditorialDialogModule } from './dialog/editorial-dialog.component';
