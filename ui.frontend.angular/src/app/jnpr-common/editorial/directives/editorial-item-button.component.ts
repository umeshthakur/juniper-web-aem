import { ChangeDetectionStrategy, Component } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';
import { SwBaseAction } from '../../../core/base/action/action.class';

export const editorialButtonDefaultStyle = 'regular';
export const editorialButtonStyleRegex = /^(regular|raised|outlined)$/;

export const editorialButtonPreRegex = /^(|drive_file)$/;

/**
 * A button element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-button',
  templateUrl: './editorial-item-button.component.html',
  styleUrls: ['./editorial-item-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialButtonComponent extends SwEditorialElement implements SwBaseAction {
  @SwInputProp(DataType.string, editorialButtonDefaultStyle, { pattern: editorialButtonStyleRegex.source })
  style: string;

  @SwInputProp(DataType.string, '', { pattern: editorialButtonPreRegex.source })
  pre: string;

  @SwInputProp(DataType.string, '')
  label: string;

  @SwInputProp(DataType.string, '')
  url: string;

  @SwInputProp(DataType.string, '')
  target: string;

  @SwInputProp(DataType.string, '')
  onclick: string;
}
