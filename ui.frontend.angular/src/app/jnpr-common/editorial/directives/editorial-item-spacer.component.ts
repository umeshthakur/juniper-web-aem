import { ChangeDetectionStrategy, Component } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { mixinSpacer, SwSpacerMixinType } from '../../spacer/spacer.component';
import { Constructor } from '../../../core/generics/index';

export const SwEditorialSpacerBase: Constructor<SwSpacerMixinType> & typeof SwEditorialElement = mixinSpacer(SwEditorialElement);

/**
 * An element that adds or removes vertical space between editorial components
 */
@Component({
  selector: editorialElementSelector + '-spacer',
  template: ``,
  styleUrls: ['../../spacer/spacer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-host-property-decorator
  host: {
    '[style.margin]': '_attrSpacer',
    '[class.horizontal-rule]': 'horizontalRule'
  }
})
export class SwEditorialSpacerComponent extends SwEditorialSpacerBase {}
