import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputCallback, SwInputProp } from '../../../core/validation/index';
import {
  SwBaseNestedItemList,
  SwBaseNestedListItem,
  SwInputBaseNestedListItemType
} from '../../../core/base/list/list';

/**
 * A nested list element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-list',
  templateUrl: './editorial-item-list.component.html',
  styleUrls: ['./editorial-item-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialListComponent extends SwEditorialElement {
  @SwInputCallback('_setListItems')
  @SwInputProp(DataType.array, [], { type: [DataType.string, DataType.object] })
  items: SwBaseNestedItemList;
  _items: SwBaseNestedItemList;

  @HostBinding('class.wrap')
  @SwInputProp<boolean>(DataType.boolean, false)
  wrap: boolean;

  protected _setListItems (_items: ReadonlyArray<SwInputBaseNestedListItemType>) {
    this._items = SwBaseNestedListItem.parseItemList(_items);
  }
}
