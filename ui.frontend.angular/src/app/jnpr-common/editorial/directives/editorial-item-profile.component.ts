import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';

export type editorialProfileImgStyleType = 'square'|'circle'|'rounded';
export const editorialProfileImgStyleDefault: editorialProfileImgStyleType = 'square';
export const editorialProfileImgStyleRegex = /^(square|circle|rounded)$/;

export type editorialProfileItemsAlignType = 'row'|'column';
export const editorialProfileItemsAlignDefault: editorialProfileItemsAlignType = 'row';
export const editorialProfileItemsAlignRegex = /^(row|column)$/;

/**
 * A profile element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-profile',
  templateUrl: './editorial-item-profile.component.html',
  styleUrls: ['./editorial-item-profile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialProfileComponent extends SwEditorialElement {
  @SwInputProp(DataType.string, '')
  subject: string;

  @SwInputProp(DataType.string, '')
  url: string;

  @SwInputProp(DataType.string, '')
  description: string;

  @SwInputProp(DataType.string, '')
  img: string;

  @SwInputProp(DataType.string, '')
  target: string;

  @SwInputProp(DataType.string, '')
  onclick: string;

  @SwInputProp(DataType.string, editorialProfileImgStyleDefault, { pattern: editorialProfileImgStyleRegex.source })
  imgStyle: editorialProfileImgStyleType;

  get _hasUrl () {
    return this.url.length > 0;
  }

  @HostBinding('class.sw-empty')
  get isEmpty () {
    return !(this.img.length > 0 || this.subject.length > 0 || this.description.length > 0);
  }
}

/**
 * A set of profile elements in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-profile-items',
  template: `<div class="wrapper" [ngClass]="_direction">
    <sw-editorial-item-profile
      *ngFor="let i of items"
      [properties]="i" [groupTheme]="groupTheme">
    </sw-editorial-item-profile>
  </div>`,
  styleUrls: ['./editorial-item-profile-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialProfileItemsComponent extends SwEditorialElement {
  @SwInputProp(DataType.array, [], { type: DataType.object })
  items: SwEditorialProfileComponent[];

  @SwInputProp(DataType.string, editorialProfileItemsAlignDefault, { pattern: editorialProfileItemsAlignRegex.source })
  direction: editorialProfileItemsAlignType;

  get _direction () {
    return `sw-dir-${this.direction}`;
  }
}
