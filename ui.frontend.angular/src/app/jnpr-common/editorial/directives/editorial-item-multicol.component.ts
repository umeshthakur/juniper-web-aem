import { ChangeDetectionStrategy, Component } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';

/**
 * A series of paragraph elements in an editorial context.
 * Lays out in two columns
 */
@Component({
  selector: editorialElementSelector + '-multicol-text',
  template: `<p *ngFor="let p of text" [innerHTML]="p"></p>`,
  styleUrls: ['./editorial-item-multicol.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialMultiColTextComponent extends SwEditorialElement {
  /** A series of strings that will output paragraph elements, one per each array item */
  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  text: string[];
}
