import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';
import { SwDialog } from '../../dialog/index';
import { SwVideoEndCardComponent } from '../../video/video-end-card.component';
import { isVideo } from '../../../util/detectVideoType.util';
import { SwVideoDialogComponent } from '../../video/dialog/video-dialog.component';
import { CanIconOverride, mixinIconOverride } from '../../common-behaviors/icon-override';

export type editorialSizeType = 'sm' | 'md' | 'lg' | 'auto';
const editorialDefaultSize: editorialSizeType = 'sm';
export const editorialSizeRegex = /^(sm|md|lg|auto)$/;

export type editorialFloatType = 'none' | 'left' | 'right';
const editorialDefaultFloat: editorialFloatType = 'none';
export const editorialFloatRegex = /^(none|left|right)$/;

/**
 * A media element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-media',
  templateUrl: './editorial-item-media.component.html',
  styleUrls: ['./editorial-item-media.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-input-property-decorator
  inputs: ['properties']
})
export class SwEditorialMediaComponent extends mixinIconOverride(SwEditorialElement) implements CanIconOverride {
  constructor (private _dialog: SwDialog) {
    super();
  }

  @SwInputProp(DataType.string, '')
  img: string;

  @SwInputProp(DataType.string, '')
  url: string;

  @SwInputProp(DataType.object, {})
  endCard: Partial<SwVideoEndCardComponent>;

  @SwInputProp(DataType.string, '')
  caption: string;

  @SwInputProp(DataType.string, '')
  label: string;

  @SwInputProp(DataType.string, '')
  onclick: string;

  @SwInputProp(DataType.string, '')
  target: string;

  @SwInputProp(DataType.string, editorialDefaultSize, { pattern: editorialSizeRegex.source })
  size: editorialSizeType;

  @SwInputProp(DataType.string, editorialDefaultFloat, { pattern: editorialFloatRegex.source })
  float: editorialFloatType;

  /**
   * Value to bind to the host as the `float` value
   * TL;DR: When `size` is large, no floating should happen
   */
  @HostBinding('attr.sw-float')
  get _attrFloat (): editorialFloatType {
    return this.size === 'lg' ? 'none' : this.float;
  }

  /**
   * Value to bind to the host as the `size` value
   * TL;DR: When the element is video, auto size will be replaced with the default size
   * to avoid issues with figcaption on Safari
   */
  @HostBinding('attr.sw-size')
  get _attrSize (): editorialSizeType {
    return this._urlIsVideo && this.size === 'auto' ? editorialDefaultSize : this.size;
  }

  get _urlIsVideo (): boolean {
    return isVideo(this.url);
  }

  get _validIconOverride (): boolean {
    return !this._urlIsVideo && this.iconOverride.length > 0;
  }

  _handleClick (e: MouseEvent) {

    if (this._urlIsVideo) {
      e.preventDefault();
      const videoConfig: SwVideoDialogComponent = {
        title: this.label,
        config: {
          url: this.url,
          endCard: this.endCard && typeof this.endCard === 'object' ? this.endCard : {},
          options: {
            autoplay: 1
          }
        }
      };
      this._dialog.open(SwVideoDialogComponent, {
        data: videoConfig
      });
    }
  }
}
