import {
  ChangeDetectionStrategy,
  Component
} from '@angular/core';
import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputCallback, SwInputProp } from '../../../core/validation/index';

import { SwNotesItemLegacyComponent, SwNotesLegacyComponent } from '../../notes/notes.module';

/**
 * A notes component in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-notes',
  templateUrl: './editorial-item-notes.component.html',
  styleUrls: ['./editorial-item-notes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialNotesComponent extends SwEditorialElement {

  @SwInputCallback('_setNotes')
  @SwInputProp<SwNotesItemLegacyComponent, SwNotesItemLegacyComponent[]>(DataType.array, [], { type: DataType.object })
  items: ReadonlyArray<SwNotesItemLegacyComponent> = [];
  _notesProps: Partial<SwNotesLegacyComponent> = {};

  _setNotes (items) {
    this._notesProps = {
      variant: 'card',
      items: items
    };
  }

}
