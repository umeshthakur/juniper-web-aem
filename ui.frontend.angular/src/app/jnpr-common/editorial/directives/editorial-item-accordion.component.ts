import { ChangeDetectionStrategy, Component } from '@angular/core';
import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, isTypeSchema, SwInputProp } from '../../../core/validation/index';

export interface AccordionPanel {
  title: string;
  children: SwEditorialElement[];
  expanded: boolean;
}

export const panelsSchema: isTypeSchema = {
  items: {
    props: {
      title: { type: DataType.string },
      children: { type: DataType.array },
      expanded: { type: DataType.boolean }
    }
  }
};

@Component({
  selector: editorialElementSelector + '-accordion',
  templateUrl: './editorial-item-accordion.component.html',
  styleUrls: ['./editorial-item-accordion.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialAccordionComponent extends SwEditorialElement {
  /** Whether the accordion should allow multiple expanded accordion items simultaneously. */
  @SwInputProp(DataType.boolean, true)
  multi: boolean;

  /** The input data for the accordion panels */
  @SwInputProp<AccordionPanel, AccordionPanel[]>(DataType.array, [], {
    type: DataType.object,
    schema: panelsSchema
  })
  items: AccordionPanel[];

  /** An authorable unique ID */
  @SwInputProp(DataType.string, '')
  id: string;
}
