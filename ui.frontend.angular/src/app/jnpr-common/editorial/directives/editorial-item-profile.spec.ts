import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { SwEvalModule } from '../../../../modules/eval/eval.module';
import { SwButtonModule } from '../../button/button.component';
import { SwIconModule } from '../../icon/index';
import { SwEditorialProfileComponent, SwEditorialProfileItemsComponent } from './editorial-item-profile.component';

describe(`SwEditorialProfileComponent`, () => {

  let comp: SwEditorialProfileComponent;
  let fixture: ComponentFixture<SwEditorialProfileComponent>;
  let debugEl: DebugElement;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SwButtonModule, SwIconModule, SwEvalModule.forRoot()],
        declarations: [SwEditorialProfileComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SwEditorialProfileComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
    debugEl = fixture.debugElement;
  });

  describe('`isEmpty` getter', () => {

    it('should report `true` as long as at least one of the main properties are non-empty', () => {
      comp.properties = {};
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(true, 'Failed on empty object');

      comp.properties = <SwEditorialProfileComponent>{
        url: 'url'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(true, 'Failed on only `url`');

      comp.properties = <SwEditorialProfileComponent>{
        imgStyle: 'rounded'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(true, 'Failed on only `imgStyle`');

      comp.properties = <SwEditorialProfileComponent>{
        url: 'url',
        imgStyle: 'rounded'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(true, 'Failed on `imgStyle` and `url`');
    });

    it('should report `false` when all the main properties are non-empty', () => {
      comp.properties = <SwEditorialProfileComponent>{
        img: 'img',
        subject: '',
        description: ''
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `img`');

      comp.properties = <SwEditorialProfileComponent>{
        img: '',
        subject: 'subject',
        description: ''
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `subject`');

      comp.properties = <SwEditorialProfileComponent>{
        img: '',
        subject: '',
        description: 'description'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `description`');

      comp.properties = <SwEditorialProfileComponent>{
        img: 'img',
        subject: '',
        description: 'description'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `img` and `description`');

      comp.properties = <SwEditorialProfileComponent>{
        img: 'img',
        subject: 'subject',
        description: ''
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `img` and `subject`');

      comp.properties = <SwEditorialProfileComponent>{
        img: '',
        subject: 'subject',
        description: 'description'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false, 'Failed on only `subject` and `description`');

      comp.properties = <SwEditorialProfileComponent>{
        img: 'img',
        subject: 'subject',
        description: 'description'
      };
      fixture.detectChanges();
      expect(comp.isEmpty).toBe(false);
    });

    it('should set an `sw-empty` class when appropriate', () => {
      const emptyClass = 'sw-empty';

      expect(debugEl.classes[emptyClass]).toBeTruthy('Failed on having class at the beginning');

      comp.img = 'img';
      fixture.detectChanges();
      expect(debugEl.classes[emptyClass]).toBeFalsy('Failed setting the empty class');

      comp.img = null;
      fixture.detectChanges();
      expect(debugEl.classes[emptyClass]).toBeTruthy('Failed removing the empty class');
    });

  });

  describe('_hasUrl', () => {
    it('should be true when `url` is non-empty', () => {
      expect(comp._hasUrl).toBeFalsy('Failed at the beginning');

      comp.url = 'url';
      fixture.detectChanges();
      expect(comp._hasUrl).toBeTruthy('Failed after setting URL');

      comp.url = null;
      fixture.detectChanges();
      expect(comp._hasUrl).toBeFalsy('Failed after unsetting URL');
    });
  });

});

describe(`SwEditorialProfileItemsComponent`, () => {

  let comp: SwEditorialProfileItemsComponent;
  let fixture: ComponentFixture<SwEditorialProfileItemsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [SwButtonModule, SwIconModule, SwEvalModule.forRoot()],
        declarations: [SwEditorialProfileComponent, SwEditorialProfileItemsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SwEditorialProfileItemsComponent);
    comp = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('_direction', () => {

    it('should return a direction class based on the `direction` value', () => {
      let direction = 'row';

      expect(comp._direction).toBe(`sw-dir-${direction}`, 'Failed at the beginning');

      comp.direction = direction = 'column';
      fixture.detectChanges();
      expect(comp._direction).toBe(`sw-dir-${direction}`);

      direction = 'row';
      comp.direction = null;
      fixture.detectChanges();
      expect(comp._direction).toBe(`sw-dir-${direction}`, 'Failed after setting `null`');
    });
  });

});
