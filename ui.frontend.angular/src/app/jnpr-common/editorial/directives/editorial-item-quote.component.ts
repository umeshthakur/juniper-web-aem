import { ChangeDetectionStrategy, Component } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';
import { SwEditorialProfileComponent } from './editorial-item-profile.component';

/**
 * A quote element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-quote',
  templateUrl: './editorial-item-quote.component.html',
  styleUrls: ['./editorial-item-quote.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialQuoteComponent extends SwEditorialElement {
  @SwInputProp(DataType.string, '')
  text: string;

  @SwInputProp(DataType.object, {})
  profile: Partial<SwEditorialProfileComponent>;
}
