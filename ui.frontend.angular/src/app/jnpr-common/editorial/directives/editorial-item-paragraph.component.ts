import { ChangeDetectionStrategy, Component } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';

/**
 * A paragraph element in an editorial context.
 * Lays out in a single column
 */
@Component({
  selector: editorialElementSelector + '-paragraph',
  template: `<p [innerHTML]="text"></p>`,
  styleUrls: ['./editorial-item-paragraph.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialParagraphComponent extends SwEditorialElement {
  @SwInputProp(DataType.string, '')
  text: string;
}
