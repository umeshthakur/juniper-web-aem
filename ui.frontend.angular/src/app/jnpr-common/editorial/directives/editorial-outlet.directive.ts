import {
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  Input,
  OnChanges,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import { EditorialItem, editorialThemeType, SwEditorialComponent } from '../editorial.component';

@Directive({
  selector: '[swEditorialOutlet]'
})
export class SwEditorialOutletDirective implements OnChanges, OnInit {
  constructor (private _cfr: ComponentFactoryResolver, private _vcr: ViewContainerRef) {}

  @Input() theme: editorialThemeType;

  @Input() items: EditorialItem[] = [];

  private _editorialFactory: ComponentFactory<SwEditorialComponent>;
  private _component: ComponentRef<SwEditorialComponent>;

  ngOnChanges () {
    this._setComponentProps();
  }

  ngOnInit () {
    const factory =
      this._editorialFactory || this._cfr.resolveComponentFactory<SwEditorialComponent>(SwEditorialComponent);
    this._component = this._vcr.createComponent(factory);
    this._setComponentProps();
  }

  private _setComponentProps () {
    if (this._component) {
      this._component.instance.theme = this.theme;
      this._component.instance.items = this.items;
    }
  }
}
