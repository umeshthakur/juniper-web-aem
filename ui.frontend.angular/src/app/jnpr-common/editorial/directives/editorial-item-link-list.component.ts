import { ChangeDetectionStrategy, Component } from '@angular/core';
import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';
import { SwBaseAction } from '../../../core/base/action/action.class';

/**
 * A nested list element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-link-list',
  templateUrl: './editorial-item-link-list.component.html',
  styleUrls: ['./editorial-item-link-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialLinkListComponent extends SwEditorialElement {
  @SwInputProp<SwBaseAction, SwBaseAction[]>(DataType.array, [], { type: DataType.object })
  items: SwBaseAction[];
}
