import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';
import {
  blockArrowTabsetDefaultOrientation,
  blockArrowTabsetDefaultVariant,
  blockArrowTabsetOrientationRegex,
  blockArrowTabsetOrientationType,
  blockArrowTabsetVariantRegex,
  blockArrowTabsetVariantType
} from '../../tabset/blockarrow-tabset.component';

export type editorialTabsetThemeType = 'none' | 'light' | 'dark';
const editorialTabsetDefaultTheme: editorialTabsetThemeType = 'none';
export const editorialTabsetThemeRegex = /^(none|light|dark)$/;

export interface SwTabItem {
  tabTitle: string;
  tabId: string;
  children: SwEditorialElement[];
}

export type SwTabItemList = ReadonlyArray<SwTabItem>;

/**
 * A tabset element in an editorial context
 */
@Component({
  selector: editorialElementSelector + '-tabset',
  templateUrl: './editorial-item-tabset.component.html',
  styleUrls: ['./editorial-item-tabset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwEditorialTabsetComponent extends SwEditorialElement {
  /** Used to make broad coloring adjustments */
  @HostBinding('attr.sw-theme')
  @SwInputProp<string>(DataType.string, editorialTabsetDefaultTheme, { pattern: editorialTabsetThemeRegex.source })
  theme: editorialTabsetThemeType = editorialTabsetDefaultTheme;

  /** An identifier of a variant which can be blocks or arrows */
  @HostBinding('attr.sw-variant')
  @SwInputProp(DataType.string, blockArrowTabsetDefaultVariant, { pattern: blockArrowTabsetVariantRegex.source })
  variant: blockArrowTabsetVariantType = blockArrowTabsetDefaultVariant;

  /** An identifier of a orientation which can be horizontal or vertical */
  @SwInputProp(DataType.string, blockArrowTabsetDefaultOrientation, { pattern: blockArrowTabsetOrientationRegex.source })
  orientation: blockArrowTabsetOrientationType = blockArrowTabsetDefaultOrientation;

  @SwInputProp(DataType.string, '')
  id: string = '';

  @SwInputProp(DataType.string, '')
  activeId: string = '';

  @SwInputProp(DataType.boolean, false)
  persistToQuery: boolean = false;

  @SwInputProp<boolean>(DataType.boolean, false)
  staggerOnMobile: boolean = false;

  @SwInputProp(DataType.array, [], { type: DataType.object })
  items: SwTabItemList = [];
}
