import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';

import { editorialElementSelector, SwEditorialElement } from './editorial-item-base';
import { DataType, SwInputProp } from '../../../core/validation/index';

/**
 * A text-based element of hierarchical importance.
 *
 * It uses the `heading` Aria role and the `aria-level` Aria property to establish
 * the correct accessibility-compliant hierarchy, i.e., given that the `h1` element
 * is equivalent to using `role="heading" aria-level="1"`, that is what is used here.
 *
 * It will use a level 2 importance header by default and currently will only support
 * up to a level 6 of importance given that should fulfill most use cases.
 *
 * For more information on those Aria topics:
 *
 * https://www.w3.org/TR/wai-aria/roles#heading
 * https://www.w3.org/TR/wai-aria/states_and_properties#aria-level
 */
@Component({
  selector: editorialElementSelector + '-headline',
  template: `<span [innerHTML]="text"></span>`,
  styleUrls: ['./editorial-item-headline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-host-property-decorator
  host: {
    'role': 'heading',
    'tabIndex': '-1'
  }
})
export class SwEditorialHeadlineComponent extends SwEditorialElement {
  @HostBinding('attr.id')
  get _id () {
    return this.id.length > 0 ? this.id : this.uid;
  }

  /**
   * Will set headline's `id` property. If none is set, an automatic and unique one will be provided
   * Should not be set unless needed.
   */
  @SwInputProp(DataType.string, '')
  id: string;

  /** The headline text to be displayed. */
  @SwInputProp(DataType.string, '')
  text: string;

  /**
   * The hierarchy level that the headline should use. Impacts hierarchy accessibility-wise and visually
   * Only allows values from `1` through `6`
   */
  @HostBinding('attr.aria-level')
  @SwInputProp(DataType.number, 2, { min: 1, max: 6 })
  hierarchy: number;
}
