import { HostBinding, Input, Directive } from '@angular/core';

import { SwBaseComponent } from '../../../core/base/index';
import { DataType, SwInputProp } from '../../../core/validation/index';
import { editorialThemeType } from '../editorial.component';

export const editorialElementSelector = 'sw-editorial-item';

export type editorialTextAlignType = 'left' | 'center' | 'inherit';
export const editorialTextAlignDefault: editorialTextAlignType = 'left';
export const editorialTextAlignRegex = /^(left|center)$/;

/** A base set of properties that every editorial element needs to have */
@Directive()
export class SwEditorialElement extends SwBaseComponent {
  @Input() uid: string;

  @HostBinding('attr.sw-editorial-prev-type')
  prevType: string;

  @Input()
  @HostBinding('attr.sw-group-theme')
  groupTheme: editorialThemeType;

  @Input() groupTextAlign: editorialTextAlignType;

  @SwInputProp<string>(DataType.string, 'inherit', { pattern: editorialTextAlignRegex.source })
  textAlign: editorialTextAlignType;

  @HostBinding('attr.sw-text-align')
  get _getTextAlign () {
    return this.textAlign === 'inherit' ? this.groupTextAlign : this.textAlign;
  }

  @HostBinding('attr.' + editorialElementSelector)
  readonly _attrType = '';

  get _linkTheme () {
    return this.groupTheme === 'dark' ? 'invert' : 'primary';
  }
}
