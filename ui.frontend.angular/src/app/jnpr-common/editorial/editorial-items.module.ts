import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwBackgroundMediaModule } from '../../modules/backgroundMedia/index';
import { SwButtonModule } from '../button/button.component';
import { SwDialogModule } from '../dialog/index';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { SwIconModule } from '../icon/index';
import { SwVideoDialogModule } from '../video/dialog/video-dialog.component';
import { SwBlockarrowTabsetModule } from '../tabset/blockarrow-tabset.component';
import { SwNotesModule } from '../notes/notes.module';
import { SwA11yModule } from '../../modules/a11y/index';
import { SwAccordionModule } from '../accordion/index';
import { SwImageModule } from '../../modules/img';

import {
  SwEditorialAccordionComponent,
  SwEditorialButtonComponent,
  SwEditorialHeadlineComponent,
  SwEditorialLinkListComponent,
  SwEditorialListComponent,
  SwEditorialMediaComponent,
  SwEditorialMultiColTextComponent,
  SwEditorialNotesComponent,
  SwEditorialOutletDirective,
  SwEditorialParagraphComponent,
  SwEditorialProfileComponent,
  SwEditorialProfileItemsComponent,
  SwEditorialQuoteComponent,
  SwEditorialSpacerComponent,
  SwEditorialTabsetComponent
} from './directives/index';
import { SwEditorialElementMapType } from './editorial-providers';


@NgModule({
  imports: [
    CommonModule,
    SwA11yModule,
    SwButtonModule,
    SwIconModule,
    SwBackgroundMediaModule,
    SwDialogModule,
    SwNotesModule,
    SwImageModule,
    SwVideoDialogModule,
    SwEvalModule.forRoot(),
    SwBlockarrowTabsetModule,
    SwAccordionModule
  ],
  exports: [
    SwEditorialAccordionComponent,
    SwEditorialButtonComponent,
    SwEditorialHeadlineComponent,
    SwEditorialLinkListComponent,
    SwEditorialListComponent,
    SwEditorialMediaComponent,
    SwEditorialMultiColTextComponent,
    SwEditorialNotesComponent,
    SwEditorialParagraphComponent,
    SwEditorialProfileComponent,
    SwEditorialProfileItemsComponent,
    SwEditorialQuoteComponent,
    SwEditorialSpacerComponent,
    SwEditorialTabsetComponent
  ],
  declarations: [
    SwEditorialAccordionComponent,
    SwEditorialButtonComponent,
    SwEditorialHeadlineComponent,
    SwEditorialLinkListComponent,
    SwEditorialListComponent,
    SwEditorialMediaComponent,
    SwEditorialMultiColTextComponent,
    SwEditorialNotesComponent,
    SwEditorialParagraphComponent,
    SwEditorialProfileComponent,
    SwEditorialProfileItemsComponent,
    SwEditorialQuoteComponent,
    SwEditorialSpacerComponent,
    SwEditorialTabsetComponent,
    SwEditorialOutletDirective
  ]
})
export class SwEditorialItemsModule {
  /** Component map that the Editorial Component interoperates with */
  static getElementMap (): SwEditorialElementMapType {
    return {
      accordion: SwEditorialAccordionComponent,
      button: SwEditorialButtonComponent,
      headline: SwEditorialHeadlineComponent,
      'link-list': SwEditorialLinkListComponent,
      list: SwEditorialListComponent,
      media: SwEditorialMediaComponent,
      'multicol-text': SwEditorialMultiColTextComponent,
      notes: SwEditorialNotesComponent,
      paragraph: SwEditorialParagraphComponent,
      profile: SwEditorialProfileItemsComponent,
      quote: SwEditorialQuoteComponent,
      spacer: SwEditorialSpacerComponent,
      tabset: SwEditorialTabsetComponent
    };
  }
}
