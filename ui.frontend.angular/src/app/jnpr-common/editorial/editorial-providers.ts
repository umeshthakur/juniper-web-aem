import { ANALYZE_FOR_ENTRY_COMPONENTS, InjectionToken, Optional, Provider, Type } from '@angular/core';
import { SwEditorialElement } from './directives/index';

/** Describes the component map that the Editorial Component interoperates with */
export interface SwEditorialElementMapType { [key: string]: Type<SwEditorialElement>; }

/** Token to provide an Editorial Component maps */
export const SW_EDITORIAL_ELEMENT_MAP = new InjectionToken<SwEditorialElementMapType>('SW_EDITORIAL_ELEMENT_MAP');

/** Token to provide a flattened collection of Editorial Component maps */
export const SW_EDITORIAL_ELEMENT_MULTI = new InjectionToken<SwEditorialElementMapType>('SW_EDITORIAL_ELEMENT_MULTI');

export function SW_EDITORIAL_ELEMENT_MULTI_PROVIDER_FACTORY (elementMaps: SwEditorialElementMapType[]|null) {
  const elMaps = Array.isArray(elementMaps) && elementMaps.length > 0 ? elementMaps : [];
  return elMaps.filter(m => !!m).reduce((acc, m) => Object.assign({}, acc, m), {});
}

export const SW_EDITORIAL_ELEMENT_MULTI_PROVIDER = {
  provide: SW_EDITORIAL_ELEMENT_MULTI,
  deps: [[new Optional(), SW_EDITORIAL_ELEMENT_MAP]],
  useFactory: SW_EDITORIAL_ELEMENT_MULTI_PROVIDER_FACTORY
};

export function SW_EDITORIAL_ELEMENT_MAP_PROVIDER (elementMap: SwEditorialElementMapType): Provider[] {
  return [
    { provide: SW_EDITORIAL_ELEMENT_MAP, useValue: elementMap, multi: true },
    { provide: ANALYZE_FOR_ENTRY_COMPONENTS, useValue: elementMap, multi: true }
  ];
}
