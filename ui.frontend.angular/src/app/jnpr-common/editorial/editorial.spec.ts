import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { SwEditorialComponent, SwEditorialModule } from './index';
import { editorialThemeType } from './editorial.component';
import { TestAgainst } from '../../../core/testing/commonTestCases.spec';
import { SwEditorialElement, SwEditorialHeadlineComponent } from './directives/index';
import { SwEditorialItemsModule } from './editorial-items.module';

@Component({
  selector: 'sw-editorial-item-pizza',
  template: ``
})
export class SwEditorialPizzaComponent extends SwEditorialElement {}

@Component({
  selector: 'sw-editorial-item-chocolate',
  template: ``
})
export class SwEditorialChocolateComponent extends SwEditorialElement {}

@Component({
  template: `<sw-editorial [properties]="props"></sw-editorial>`
})
class HelperComponent {
  props: Partial<SwEditorialComponent> = {};
}

describe('SwEditorialComponent', () => {

  describe('component maps', () => {
    let comp: HelperComponent;
    let fixture: ComponentFixture<HelperComponent>;
    let debugEl: DebugElement;
    let itemDebugEls: DebugElement[];

    describe('single', () => {
      beforeEach(async(() => {
        TestBed.configureTestingModule({
          imports: [
            SwEditorialModule.withComponentMap({
              pizza: SwEditorialPizzaComponent,
              chocolate: SwEditorialChocolateComponent
            })
          ],
          declarations: [
            HelperComponent,
            SwEditorialPizzaComponent,
            SwEditorialChocolateComponent
          ]
        }).compileComponents();
      }));

      beforeEach(() => {
        fixture = TestBed.createComponent(HelperComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
        debugEl = fixture.debugElement;

        comp.props = {
          items: [
            { type: 'pizza' },
            { type: 'water' },
            { type: 'chocolate' }
          ]
        };
        fixture.detectChanges();

        itemDebugEls = debugEl.queryAll(By.css('[sw-editorial-item]'));
      });

      it('should only instantiate elements from the map', () => {
        expect(itemDebugEls.length).toBe(2);
      });

      it('should instantiate the appropriate items based on type', () => {
        const pizza = debugEl.query(By.directive(SwEditorialPizzaComponent));
        const chocolate = debugEl.query(By.directive(SwEditorialChocolateComponent));
        expect(itemDebugEls[0].nativeElement).toBe(pizza.nativeElement);
        expect(itemDebugEls[1].nativeElement).toBe(chocolate.nativeElement);
      });
    });

    describe('multi provider', () => {
      beforeEach(async(() => {
        TestBed.configureTestingModule({
          imports: [
            SwEditorialModule.withComponentMap({
              pizza: SwEditorialPizzaComponent,
              chocolate: SwEditorialChocolateComponent
            }),
            SwEditorialModule.withComponentMap(SwEditorialItemsModule.getElementMap()),
            SwEditorialItemsModule
          ],
          declarations: [
            HelperComponent,
            SwEditorialPizzaComponent,
            SwEditorialChocolateComponent
          ]
        }).compileComponents();
      }));

      beforeEach(() => {
        fixture = TestBed.createComponent(HelperComponent);
        comp = fixture.componentInstance;
        fixture.detectChanges();
        debugEl = fixture.debugElement;

        comp.props = {
          items: [
            { type: 'pizza' },
            { type: 'headline' },
            { type: 'chocolate' }
          ]
        };
        fixture.detectChanges();

        itemDebugEls = debugEl.queryAll(By.css('[sw-editorial-item]'));
      });

      it('should instantiate elements from multiple maps', () => {
        expect(itemDebugEls.length).toBe(3);
      });

      it('should instantiate the appropriate items based on type', () => {
        const pizza = debugEl.query(By.directive(SwEditorialPizzaComponent));
        const headline = debugEl.query(By.directive(SwEditorialHeadlineComponent));
        const chocolate = debugEl.query(By.directive(SwEditorialChocolateComponent));
        expect(itemDebugEls[0].nativeElement).toBe(pizza.nativeElement);
        expect(itemDebugEls[1].nativeElement).toBe(headline.nativeElement);
        expect(itemDebugEls[2].nativeElement).toBe(chocolate.nativeElement);
      });
    });

  });

  describe('authorable properties', () => {
    let comp: SwEditorialComponent;
    let fixture: ComponentFixture<SwEditorialComponent>;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [SwEditorialModule]
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SwEditorialComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });

    describe('`theme`', () => {
      const currPropDefault = 'none';

      describe('should be deemed as valid', () => {
        it('for all allowed themes', () => {
          const testValues: editorialThemeType[] = ['none', 'light', 'dark'];
          TestAgainst.validValues(comp, 'theme', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for any value that isn\'t any of the allowed themes', () => {
          TestAgainst.otherDataTypes(comp, 'theme', null, currPropDefault);
        });

        it('should reset the value to default when setting an incorrect value', () => {
          const valid = 'dark';
          const invalid = 'pizza';
          TestAgainst.resettingToFallback(comp, 'theme', valid, invalid, currPropDefault);
        });
      });
    });

    describe('`items`', () => {
      const currPropDefault = [];

      describe('should be deemed as valid', () => {
        it('for any object[]', () => {
          const testValues = [
            [],
            [{ type: '' }],
            [{ type: 'paragraph' }],
            [{ type: 'paragraph', properties: {} }]
          ];
          TestAgainst.validValues(comp, 'items', testValues, TestAgainst.NON_STRICT_EQ);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for arrays that have something other than the expected object type in them', () => {
          const testValues = [
            [{}],
            [{ properties: {} }],
            [{ type: {} }]
          ] as any[];
          TestAgainst.invalidValues(comp, 'items', testValues, currPropDefault, TestAgainst.NON_STRICT_EQ);
        });

        it('for any value that isn\'t an object[] type', () => {
          TestAgainst.otherDataTypes(comp, 'items', 'array', currPropDefault, TestAgainst.NON_STRICT_EQ);
        });

        it('should reset the value to default when setting an incorrect value', () => {
          const valid = [{ type: 'paragraph', properties: {} }];
          const invalid = [{ properties: {} }];

          TestAgainst.resettingToFallback(comp, 'items', valid, invalid, currPropDefault, TestAgainst.NON_STRICT_EQ);
        });
      });
    });

  });

});
