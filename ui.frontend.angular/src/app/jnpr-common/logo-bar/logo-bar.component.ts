import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, NgModule } from '@angular/core';

import { SwBaseComponent } from '../../core/base';
import { DataType, SwInputProp } from '../../core/validation';
import { SwBaseAction } from '../../core/base/action/action.class';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { SwImageModule } from '../../modules/img';

/**
 * Logo Bar component
 */
@Component({
  selector: 'sw-logo-bar',
  templateUrl: './logo-bar.component.html',
  styleUrls: ['./logo-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwLogoBarComponent extends SwBaseComponent implements SwBaseAction {
  @SwInputProp<string>(DataType.string, '')
  img: string;

  @SwInputProp<string>(DataType.string, '')
  url: string;

  @SwInputProp<string>(DataType.string, '')
  label: string;

  @SwInputProp<string>(DataType.string, '')
  onclick: string;

  @SwInputProp<string>(DataType.string, '')
  target: string;

  static hasMinimumProps (o: Partial<SwLogoBarComponent>): boolean {
    return typeof o.img === 'string' && o.img.length > 0;
  }
}

@NgModule({
  imports: [CommonModule, SwImageModule, SwEvalModule.forRoot()],
  exports: [SwLogoBarComponent],
  declarations: [SwLogoBarComponent],
  entryComponents: [SwLogoBarComponent]
})
export class SwLogoBarModule {}
