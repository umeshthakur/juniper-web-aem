export type alignmentType = 'left'|'center';
export const SW_ALIGNMENT_LEFT: alignmentType = 'left';
export const alignmentRegex = /^(left|center)$/;

export type themeType = 'none'|'light'|'dark';
export const SW_THEME_NONE: themeType = 'none';
export const SW_THEME_LIGHT: themeType = 'light';
export const themeRegex = /^(none|light|dark)$/;
