import { Constructor } from '../../core/generics/index';
import { DataType, SwInputProp } from '../../core/validation/index';

export type iconOverrideType = '' | 'video_library';
export const iconOverrideRegex = /^(|video_library)$/;

export interface CanIconOverride {
  iconOverride?: iconOverrideType;
}

/** Mixin to augment a directive with an `iconOverride` property. */
export function mixinIconOverride<T extends Constructor<{}>> (base: T): Constructor<CanIconOverride> & T {
  class SwIconOverride extends base implements CanIconOverride {
    @SwInputProp(DataType.string, '', { pattern: iconOverrideRegex.source })
    iconOverride: iconOverrideType;

    constructor (...args: any[]) { super(...args); }
  }

  return SwIconOverride;
}
