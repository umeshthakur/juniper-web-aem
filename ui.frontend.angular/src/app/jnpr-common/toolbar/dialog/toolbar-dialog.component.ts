import { Component, Input, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwButtonModule } from '../../button/button.component';
import { SwIconModule } from '../../icon/index';
import { SwToolbarModule } from '../../toolbar/toolbar.component';
import { PortalModule } from '@angular/cdk/portal';
import { SwDialogModule } from '../../dialog/index';

/**
 * A base component to implement a dialog with a toolbar
 */
@Component({
  selector: 'sw-toolbar-dialog',
  templateUrl: './toolbar-dialog.component.html'
})
export class SwToolbarDialogComponent {
  @Input() title: string = '';
  @Input() result: any;
}

@NgModule({
  imports: [CommonModule, SwButtonModule, SwIconModule, SwToolbarModule, PortalModule, SwDialogModule],
  exports: [SwToolbarDialogComponent, SwDialogModule, SwButtonModule, SwIconModule, SwToolbarModule],
  declarations: [SwToolbarDialogComponent],
  entryComponents: [SwToolbarDialogComponent]
})
export class SwToolbarDialogModule {}
