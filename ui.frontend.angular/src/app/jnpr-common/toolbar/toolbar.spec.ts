import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { SwToolbarModule } from './toolbar.component';

@Component({ template: `<sw-toolbar>Test Toolbar</sw-toolbar>` })
class TestAppComponent {
  toolbarColor: string;
}

describe('SwToolbarComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SwToolbarModule],
      declarations: [TestAppComponent]
    }).compileComponents();
  }));

});
