import { ChangeDetectionStrategy, Component, Directive, NgModule } from '@angular/core';
import { SwButtonModule } from '../button/button.component';
import { SwIconModule } from '../icon/index';

@Directive({
  /** Purposefully making `sw-toolbar-row` a directive because it shouldn't have a view. */
  // tslint:disable-next-line directive-selector
  selector: 'sw-toolbar-row'
})
export class SwToolbarRowDirective {}

@Component({
  selector: 'sw-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: [ './toolbar.component.scss' ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwToolbarComponent {}

@NgModule({
  imports: [
    SwButtonModule,
    SwIconModule
  ],
  exports: [ SwToolbarComponent, SwToolbarRowDirective ],
  declarations: [ SwToolbarComponent, SwToolbarRowDirective ]
})
export class SwToolbarModule {}
