import { DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SwSpacerComponent, SwSpacerModule } from './spacer.component';
import { TestAgainst } from '../../../core/testing/commonTestCases.spec';

describe('SwSpacerComponent', () => {
  let comp: SwSpacerComponent;

  beforeEach(() => {
    comp = new SwSpacerComponent();
  });

  describe('authorable property', () => {

    describe('`baseValue`', () => {
      const currPropDefault = 20;

      describe('should be deemed as valid', () => {
        it('for natural numbers', () => {
          const testValues = [1, 5, 8, 20, 100];
          TestAgainst.validValues(comp, 'baseValue', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for non-natural numbers', () => {
          const testValues = [-1, -100, -3.4, 0, Math.PI];
          TestAgainst.invalidValues(comp, 'baseValue', testValues, currPropDefault);
        });

        it('for any value that isn\'t a number type', () => {
          TestAgainst.otherDataTypes(comp, 'baseValue', 'number', currPropDefault);
        });
      });
    });

    describe('`baseUnit`', () => {
      const currPropDefault = 'px';

      describe('should be deemed as valid', () => {
        it('for white-listed css units', () => {
          const testValues = ['em', 'rem', 'px', '%'];
          TestAgainst.validValues(comp, 'baseUnit', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for disallowed css units', () => {
          const testValues = ['cm', 'vw', 'in', ''];
          TestAgainst.invalidValues(comp, 'baseUnit', testValues, currPropDefault);
        });

        it('for any value that isn\'t a string type', () => {
          TestAgainst.otherDataTypes(comp, 'baseUnit', 'string', currPropDefault);
        });
      });
    });

    describe('`multiple`', () => {
      const currPropDefault = 0;

      describe('should be deemed as valid', () => {
        it('for integers', () => {
          const testValues = [-100, -25, -3, 0, 1, 10, 32];
          TestAgainst.validValues(comp, 'multiple', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for non-integers', () => {
          const testValues = [-3.146, 6.24, 0.1];
          TestAgainst.invalidValues(comp, 'multiple', testValues, currPropDefault);
        });

        it('for any value that isn\'t a number type', () => {
          TestAgainst.otherDataTypes(comp, 'multiple', 'number', currPropDefault);
        });
      });
    });

  });

  it('should return zero pixels by default', () => {
    expect(comp._attrSpacer).toBe('0px auto');
    expect(typeof comp._attrSpacer).toBe('string');
  });

  it('should multiply base values based on the `multiple` set', () => {
    comp.multiple = 5;
    expect(comp._attrSpacer).toBe('50px auto');
    expect(typeof comp._attrSpacer).toBe('string');

    comp.multiple = -2;
    expect(comp._attrSpacer).toBe('-20px auto');
    expect(typeof comp._attrSpacer).toBe('string');
  });

  it('should modify the values returned after modifying the `baseValue`', () => {
    comp.multiple = 10;

    comp.baseValue = 5;
    expect(comp._attrSpacer).toBe('25px auto');
    expect(typeof comp._attrSpacer).toBe('string');

    comp.baseValue = -2;
    expect(comp._attrSpacer).toBe('100px auto');
    expect(typeof comp._attrSpacer).toBe('string');
  });

  it('should be able to return values of different units', () => {
    comp.multiple = 10;

    comp.baseUnit = '%';
    expect(comp._attrSpacer).toBe('100% auto');
    expect(typeof comp._attrSpacer).toBe('string');

    comp.baseUnit = 'vh';
    expect(comp._attrSpacer).toBe('100px auto');
    expect(typeof comp._attrSpacer).toBe('string');

    comp.baseUnit = 'rem';
    expect(comp._attrSpacer).toBe('100rem auto');
    expect(typeof comp._attrSpacer).toBe('string');
  });

  describe('integrated', () => {
    let comp: SwSpacerComponent;
    let fixture: ComponentFixture<SwSpacerComponent>;
    let debugEl: DebugElement;
    let nativeEl: HTMLElement;

    beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [ SwSpacerModule ]
      }).compileComponents();
    }));

    beforeEach(() => {
      fixture = TestBed.createComponent(SwSpacerComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
      debugEl = fixture.debugElement;
      nativeEl = debugEl.nativeElement;
    });

    it(`should bind the spacer's attribute to the host element`, () => {
      comp.multiple = 5;
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '50px auto');

      comp.multiple = -2;
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '-20px auto');

      comp.multiple = 10;

      comp.baseValue = 5;
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '25px auto');

      comp.baseValue = -2;
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '100px auto');

      comp.baseUnit = '%';
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '100% auto');

      comp.baseUnit = 'vh';
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '100px auto');

      comp.baseUnit = 'rem';
      fixture.detectChanges();
      testSpacerAttr(nativeEl, '100rem auto');
    });

  });

});

function testSpacerAttr (nativeEl: HTMLElement, value: string) {
  expect(nativeEl.hasAttribute('style')).toBeTruthy();
  expect(nativeEl.style.margin).toBe(value);
}
