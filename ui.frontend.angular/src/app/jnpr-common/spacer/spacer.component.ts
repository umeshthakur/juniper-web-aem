import { ChangeDetectionStrategy, Component, HostBinding, NgModule, Directive } from '@angular/core';

import { SwBaseComponent } from '../../core/base';
import { DataType, SwInputProp } from '../../core/validation';
import { Constructor } from '../../core/generics/index';

type spacerUnitType = 'em' | 'rem' | 'px' | '%';
const spacerDefaultUnit: spacerUnitType = 'px';
export const spacerUnitRegex = /^(em|rem|px|%)$/;

export interface SwSpacerMixinType {
  baseValue: number;
  baseUnit: string;
  multiple: number;
  horizontalRule: boolean;
  readonly _attrSpacer: string;
}

/** Mixin to augment a directive with a `disabled` property. */
export function mixinSpacer<T extends Constructor<{}>> (
  base: T
): Constructor<SwSpacerMixinType> & T {
  @Directive()
  class SwSpacerMixin extends base implements SwSpacerMixinType {
    @SwInputProp(DataType.natural, 20, { min: 0 })
    baseValue: number;

    @SwInputProp(DataType.string, spacerDefaultUnit, { pattern: spacerUnitRegex.source })
    baseUnit: string;

    @SwInputProp(DataType.integer, 0)
    multiple: number;

    @HostBinding('class.horizontal-rule')
    @SwInputProp(DataType.boolean, false)
    horizontalRule: boolean;

    @HostBinding('style.margin')
    get _attrSpacer (): string {
      return `${this.baseValue * this.multiple / 2}${this.baseUnit} auto`;
    }
  }

  return SwSpacerMixin;
}

/**
 * Utility component to add and remove vertical space between two components
 */
@Component({
  selector: 'sw-spacer',
  template: ``,
  styleUrls: ['./spacer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  // tslint:disable-next-line use-host-property-decorator
  host: {
    '[style.margin]': '_attrSpacer',
    '[class.horizontal-rule]': 'horizontalRule'
  }
})

export class SwSpacerComponent extends mixinSpacer(SwBaseComponent) {}

@NgModule({
  imports: [],
  exports: [SwSpacerComponent],
  declarations: [SwSpacerComponent],
  entryComponents: [SwSpacerComponent]
})
export class SwSpacerModule {}
