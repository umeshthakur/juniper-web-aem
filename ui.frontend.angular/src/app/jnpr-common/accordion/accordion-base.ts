import { InjectionToken } from '@angular/core';
import { CdkAccordion } from '@angular/cdk/accordion';

export interface SwAccordionBase extends CdkAccordion {
  /** Handles keyboard events coming in from the panel headers. */
  _handleHeaderKeydown: (event: KeyboardEvent) => void;

  /** Handles focus events on the panel headers. */
  _handleHeaderFocus: (header: any) => void;
}

export const SW_ACCORDION = new InjectionToken<SwAccordionBase>('SW_ACCORDION');
