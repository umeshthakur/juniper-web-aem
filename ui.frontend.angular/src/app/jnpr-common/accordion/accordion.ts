import { AfterContentInit, ContentChildren, Directive, QueryList } from '@angular/core';
import { CdkAccordion } from '@angular/cdk/accordion';
import { FocusKeyManager } from '@angular/cdk/a11y';
import * as Key from '@angular/cdk/keycodes';
import { SW_ACCORDION, SwAccordionBase } from './accordion-base';
import { SwAccordionPanelHeaderComponent } from './accordion-header';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: 'sw-accordion',
  exportAs: 'swAccordion',
  providers: [
    {
      provide: SW_ACCORDION,
      useExisting: SwAccordionDirective
    }
  ],
  // tslint:disable-next-line:use-input-property-decorator
  inputs: ['multi']
})
export class SwAccordionDirective extends CdkAccordion implements SwAccordionBase, AfterContentInit {
  private _keyManager: FocusKeyManager<SwAccordionPanelHeaderComponent>;

  @ContentChildren(SwAccordionPanelHeaderComponent, { descendants: true })
  _headers: QueryList<SwAccordionPanelHeaderComponent>;

  ngAfterContentInit () {
    this._keyManager = new FocusKeyManager(this._headers).withWrap().withHorizontalOrientation('ltr');
  }

  /** Handles keyboard events coming in from the panel headers. */
  _handleHeaderKeydown (event: KeyboardEvent) {
    // tslint:disable-next-line:deprecation
    const { keyCode } = event;
    const manager = this._keyManager;

    if (keyCode === Key.HOME) {
      manager.setFirstItemActive();
      event.preventDefault();
    } else if (keyCode === Key.END) {
      manager.setLastItemActive();
      event.preventDefault();
    } else {
      this._keyManager.onKeydown(event);
    }
  }

  _handleHeaderFocus (header: SwAccordionPanelHeaderComponent) {
    this._keyManager.updateActiveItem(header);
  }
}
