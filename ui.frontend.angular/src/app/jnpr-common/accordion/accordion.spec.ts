// tslint:disable component-class-suffix

import { async, inject, TestBed } from '@angular/core/testing';
import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  SwAccordionDirective,
  SwAccordionModule,
  SwAccordionPanelComponent,
  SwAccordionPanelHeaderComponent
} from './accordion.module';
import { DOWN_ARROW, END, HOME, UP_ARROW } from '@angular/cdk/keycodes';
import { FocusMonitor } from '@angular/cdk/a11y';
import { dispatchKeyboardEvent } from '../../../core/testing/cdk';

describe('SwAccordion', () => {
  let focusMonitor: FocusMonitor;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, SwAccordionModule],
      declarations: [NestedPanel, SetOfItems]
    });
    TestBed.compileComponents();

    inject([FocusMonitor], (fm: FocusMonitor) => {
      focusMonitor = fm;
    })();
  }));

  it('should ensure only one item is expanded at a time', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const items = fixture.debugElement.queryAll(By.css('.sw-accordion-panel'));
    const panelInstances = fixture.componentInstance.panels.toArray();

    panelInstances[0].expanded = true;
    fixture.detectChanges();
    expect(items[0].classes['state--expanded']).toBeTruthy();
    expect(items[1].classes['state--expanded']).toBeFalsy();

    panelInstances[1].expanded = true;
    fixture.detectChanges();
    expect(items[0].classes['state--expanded']).toBeFalsy();
    expect(items[1].classes['state--expanded']).toBeTruthy();
  });

  it('should allow multiple items to be expanded simultaneously', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.componentInstance.multi = true;
    fixture.detectChanges();

    const panels = fixture.debugElement.queryAll(By.css('.sw-accordion-panel'));
    const panelInstances = fixture.componentInstance.panels.toArray();

    panelInstances[0].expanded = true;
    panelInstances[1].expanded = true;
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeTruthy();
    expect(panels[1].classes['state--expanded']).toBeTruthy();
  });

  it('should expand or collapse all enabled items', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const panels = fixture.debugElement.queryAll(By.css('.sw-accordion-panel'));

    fixture.componentInstance.multi = true;
    fixture.componentInstance.panels.toArray()[1].expanded = true;
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeFalsy();
    expect(panels[1].classes['state--expanded']).toBeTruthy();

    fixture.componentInstance.accordion.openAll();
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeTruthy();
    expect(panels[1].classes['state--expanded']).toBeTruthy();

    fixture.componentInstance.accordion.closeAll();
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeFalsy();
    expect(panels[1].classes['state--expanded']).toBeFalsy();
  });

  it('should not expand or collapse disabled items', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const panels = fixture.debugElement.queryAll(By.css('.sw-accordion-panel'));

    fixture.componentInstance.multi = true;
    fixture.componentInstance.panels.toArray()[1].disabled = true;
    fixture.detectChanges();
    fixture.componentInstance.accordion.openAll();
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeTruthy();
    expect(panels[1].classes['state--expanded']).toBeFalsy();

    fixture.componentInstance.accordion.closeAll();
    fixture.detectChanges();
    expect(panels[0].classes['state--expanded']).toBeFalsy();
    expect(panels[1].classes['state--expanded']).toBeFalsy();
  });

  it('should not register nested panels to the same accordion', () => {
    const fixture = TestBed.createComponent(NestedPanel);
    const innerPanel = fixture.componentInstance.innerPanel;
    const outerPanel = fixture.componentInstance.outerPanel;

    expect(innerPanel.accordion).not.toBe(outerPanel.accordion);
  });

  it('should move focus to the next header when pressing the down arrow', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const headerElements = fixture.debugElement.queryAll(By.css('sw-accordion-panel-header'));
    const headers = fixture.componentInstance.headers.toArray();

    focusMonitor.focusVia(headerElements[0].nativeElement, 'keyboard');
    headers.forEach(header => spyOn(header, 'focus'));

    // Stop at the second-last header so focus doesn't wrap around.
    for (let i = 0; i < headerElements.length - 1; i++) {
      dispatchKeyboardEvent(headerElements[i].nativeElement, 'keydown', DOWN_ARROW);
      fixture.detectChanges();
      expect(headers[i + 1].focus).toHaveBeenCalledTimes(1);
    }
  });

  it('should move focus to the next header when pressing the up arrow', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const headerElements = fixture.debugElement.queryAll(By.css('sw-accordion-panel-header'));
    const headers = fixture.componentInstance.headers.toArray();

    focusMonitor.focusVia(headerElements[headerElements.length - 1].nativeElement, 'keyboard');
    headers.forEach(header => spyOn(header, 'focus'));

    // Stop before the first header
    for (let i = headers.length - 1; i > 0; i--) {
      dispatchKeyboardEvent(headerElements[i].nativeElement, 'keydown', UP_ARROW);
      fixture.detectChanges();
      expect(headers[i - 1].focus).toHaveBeenCalledTimes(1);
    }
  });

  it('should skip disabled items when moving focus with the keyboard', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const headerElements = fixture.debugElement.queryAll(By.css('sw-accordion-panel-header'));
    const panels = fixture.componentInstance.panels.toArray();
    const headers = fixture.componentInstance.headers.toArray();

    focusMonitor.focusVia(headerElements[0].nativeElement, 'keyboard');
    headers.forEach(header => spyOn(header, 'focus'));
    panels[1].disabled = true;
    fixture.detectChanges();

    dispatchKeyboardEvent(headerElements[0].nativeElement, 'keydown', DOWN_ARROW);
    fixture.detectChanges();

    expect(headers[1].focus).not.toHaveBeenCalled();
    expect(headers[2].focus).toHaveBeenCalledTimes(1);
  });

  it('should focus the first header when pressing the home key', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const headerElements = fixture.debugElement.queryAll(By.css('sw-accordion-panel-header'));
    const headers = fixture.componentInstance.headers.toArray();

    headers.forEach(header => spyOn(header, 'focus'));
    dispatchKeyboardEvent(headerElements[headerElements.length - 1].nativeElement, 'keydown', HOME);
    fixture.detectChanges();

    expect(headers[0].focus).toHaveBeenCalledTimes(1);
  });

  it('should focus the last header when pressing the end key', () => {
    const fixture = TestBed.createComponent(SetOfItems);
    fixture.detectChanges();

    const headerElements = fixture.debugElement.queryAll(By.css('sw-accordion-panel-header'));
    const headers = fixture.componentInstance.headers.toArray();

    headers.forEach(header => spyOn(header, 'focus'));
    dispatchKeyboardEvent(headerElements[0].nativeElement, 'keydown', END);
    fixture.detectChanges();

    expect(headers[headers.length - 1].focus).toHaveBeenCalledTimes(1);
  });
});

@Component({
  template: `
  <sw-accordion [multi]="multi">
    <sw-accordion-panel *ngFor="let i of [0, 1, 2, 3]">
      <sw-accordion-panel-header>Summary {{i}}</sw-accordion-panel-header>
      <p>Content</p>
    </sw-accordion-panel>
  </sw-accordion>`
})
class SetOfItems {
  @ViewChild(SwAccordionDirective)
  accordion: SwAccordionDirective;

  @ViewChildren(SwAccordionPanelComponent)
  panels: QueryList<SwAccordionPanelComponent>;

  @ViewChildren(SwAccordionPanelHeaderComponent)
  headers: QueryList<SwAccordionPanelHeaderComponent>;

  multi: boolean = false;
}

@Component({
  template: `
  <sw-accordion>
    <sw-accordion-panel #outerPanel="swAccordionPanel">
      <sw-accordion-panel-header>Outer Panel</sw-accordion-panel-header>
      <sw-accordion-panel #innerPanel="swAccordionPanel">
        <sw-accordion-panel-header>Inner Panel</sw-accordion-panel-header>
        <p>Content</p>
      </sw-accordion-panel>
    </sw-accordion-panel>
  </sw-accordion>`
})
class NestedPanel {
  @ViewChild('outerPanel')
  outerPanel: SwAccordionPanelComponent;

  @ViewChild('innerPanel')
  innerPanel: SwAccordionPanelComponent;
}
