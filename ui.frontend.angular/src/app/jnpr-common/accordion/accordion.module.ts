import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CdkAccordionModule } from '@angular/cdk/accordion';
import { PortalModule } from '@angular/cdk/portal';
import { SwIconModule } from '../icon';
import { SwAccordionDirective } from './accordion';
import { SwAccordionPanelComponent } from './accordion-panel';
import { SwAccordionPanelHeaderComponent } from './accordion-header';

@NgModule({
  imports: [
    CommonModule,
    CdkAccordionModule,
    PortalModule,
    SwIconModule
  ],
  declarations: [
    SwAccordionDirective,
    SwAccordionPanelComponent,
    SwAccordionPanelHeaderComponent
  ],
  exports: [
    SwAccordionDirective,
    SwAccordionPanelComponent,
    SwAccordionPanelHeaderComponent
  ]
})
export class SwAccordionModule {}

export { SwAccordionDirective } from './accordion';
export { SwAccordionPanelComponent } from './accordion-panel';
export { SwAccordionPanelHeaderComponent } from './accordion-header';
