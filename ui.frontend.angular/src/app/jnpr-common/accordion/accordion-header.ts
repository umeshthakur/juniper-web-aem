import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Host,
  OnDestroy
} from '@angular/core';
import { FocusableOption, FocusMonitor, FocusOrigin } from '@angular/cdk/a11y';
import * as Key from '@angular/cdk/keycodes';
import { merge, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { SwAccordionPanelComponent } from './accordion-panel';
import { swAccordionAnimations } from './accordion-animations';

@Component({
  selector: 'sw-accordion-panel-header',
  styleUrls: ['./accordion-header.scss'],
  templateUrl: './accordion-header.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [swAccordionAnimations.indicatorRotate],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    class: 'sw-accordion-panel-header',
    role: 'button',
    '[attr.id]': 'panel._headerId',
    '[attr.tabindex]': 'disabled ? -1 : 0',
    '[attr.aria-controls]': '_getPanelId()',
    '[attr.aria-expanded]': '_isExpanded()',
    '[attr.aria-disabled]': 'panel.disabled',
    '[class.state--expanded]': '_isExpanded()',
    '(click)': '_toggle()',
    '(keydown)': '_keydown($event)'
  }
})
export class SwAccordionPanelHeaderComponent implements OnDestroy, FocusableOption {
  private _parentChangeSubscription = Subscription.EMPTY;

  constructor (
    @Host() public panel: SwAccordionPanelComponent,
    private _element: ElementRef,
    private _focusMonitor: FocusMonitor,
    private _changeDetectorRef: ChangeDetectorRef
  ) {
    // Since the toggle state depends on an @Input on the panel, we
    // need to subscribe and trigger change detection manually.
    this._parentChangeSubscription = merge(
      panel.opened,
      panel.closed,
      panel._inputChanges.pipe(filter(changes => !!changes.disabled))
    ).subscribe(() => this._changeDetectorRef.markForCheck());

    // Avoids focus being lost if the panel contained the focused element and was closed.
    panel.closed
      .pipe(filter(() => panel._containsFocus()))
      .subscribe(() => _focusMonitor.focusVia(_element.nativeElement, 'program'));

    _focusMonitor.monitor(_element.nativeElement).subscribe(origin => {
      if (origin && panel.accordion) {
        panel.accordion._handleHeaderFocus(this);
      }
    });
  }

  get disabled () {
    return this.panel.disabled;
  }

  _toggle (): void {
    this.panel.toggle();
  }

  _isExpanded (): boolean {
    return this.panel.expanded;
  }

  _getExpandedState (): string {
    return this.panel._getExpandedState();
  }

  _getPanelId (): string {
    return this.panel.id;
  }

  _keydown (event: KeyboardEvent) {
    // tslint:disable-next-line:deprecation
    switch (event.keyCode) {
      case Key.SPACE:
      case Key.ENTER:
        event.preventDefault();
        this._toggle();
        break;
      default:
        if (this.panel.accordion) {
          this.panel.accordion._handleHeaderKeydown(event);
        }
        return;
    }
  }

  focus (origin: FocusOrigin = 'program') {
    this._focusMonitor.focusVia(this._element.nativeElement, origin);
  }

  ngOnDestroy () {
    this._parentChangeSubscription.unsubscribe();
    this._focusMonitor.stopMonitoring(this._element.nativeElement);
  }
}
