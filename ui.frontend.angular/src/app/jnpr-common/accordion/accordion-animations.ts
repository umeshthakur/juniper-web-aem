import {
  animate,
  AnimationTriggerMetadata,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

export const ACCORDION_ANIMATION_TIMING = '225ms cubic-bezier(0.4,0.0,0.2,1)';

export const swAccordionAnimations: {
  readonly indicatorRotate: AnimationTriggerMetadata;
  readonly bodyExpansion: AnimationTriggerMetadata;
} = {
  indicatorRotate: trigger('indicatorRotate', [
    state('collapsed, void', style({ transform: 'rotate(0deg)' })),
    state('expanded', style({ transform: 'rotate(135deg)' })),
    transition('expanded <=> collapsed, void => collapsed', animate(ACCORDION_ANIMATION_TIMING))
  ]),
  bodyExpansion: trigger('bodyExpansion', [
    state('collapsed, void', style({ height: '0px', visibility: 'hidden' })),
    state('expanded', style({ height: '*', visibility: 'visible' })),
    transition('expanded <=> collapsed, void => collapsed', animate(ACCORDION_ANIMATION_TIMING))
  ])
};
