import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  OnChanges,
  OnDestroy,
  Optional,
  Output,
  SimpleChanges,
  SkipSelf,
  ViewChild
} from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { AnimationEvent } from '@angular/animations';
import { UniqueSelectionDispatcher } from '@angular/cdk/collections';
import { TemplatePortal } from '@angular/cdk/portal';
import { CdkAccordionItem } from '@angular/cdk/accordion';
import { Subject } from 'rxjs';
import { SW_ACCORDION, SwAccordionBase } from './accordion-base';
import { swAccordionAnimations } from './accordion-animations';

export type SwAccordionPanelState = 'expanded' | 'collapsed';

let uniqueId = 0;

@Component({
  selector: 'sw-accordion-panel',
  exportAs: 'swAccordionPanel',
  templateUrl: './accordion-panel.html',
  styleUrls: ['./accordion-panel.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [swAccordionAnimations.bodyExpansion],
  providers: [{ provide: SW_ACCORDION, useValue: undefined }],
  // tslint:disable-next-line:use-input-property-decorator
  inputs: ['disabled', 'expanded'],
  // tslint:disable-next-line:use-output-property-decorator
  outputs: ['opened', 'closed', 'expandedChange'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    'class': 'sw-accordion-panel',
    '[class.state--expanded]': 'expanded'
  }
})
export class SwAccordionPanelComponent extends CdkAccordionItem implements OnChanges, OnDestroy {
  private _document: Document;

  @Output()
  afterExpand = new EventEmitter<void>();

  @Output()
  afterCollapse = new EventEmitter<void>();

  readonly _inputChanges = new Subject<SimpleChanges>();

  /** Parent accordion. */
  accordion: SwAccordionBase;

  @ViewChild('body')
  _body: ElementRef<HTMLElement>;

  _portal: TemplatePortal;

  _headerId = `sw-accordion-panel-header-${uniqueId++}`;

  constructor (
    @Optional() @SkipSelf() @Inject(SW_ACCORDION) accordion: SwAccordionBase,
    _changeDetectorRef: ChangeDetectorRef,
    _uniqueSelectionDispatcher: UniqueSelectionDispatcher,
    @Inject(DOCUMENT) _document: Document
  ) {
    super(accordion, _changeDetectorRef, _uniqueSelectionDispatcher);
    this.accordion = accordion;
    this._document = _document;
  }

  /** Gets the expanded state string. */
  _getExpandedState (): SwAccordionPanelState {
    return this.expanded ? 'expanded' : 'collapsed';
  }

  ngOnChanges (changes: SimpleChanges) {
    this._inputChanges.next(changes);
  }

  ngOnDestroy () {
    super.ngOnDestroy();
    this._inputChanges.complete();
  }

  _bodyAnimation (event: AnimationEvent) {
    const { phaseName, toState, fromState } = event;

    if (phaseName === 'done' && toState === 'expanded' && fromState !== 'void') {
      this.afterExpand.emit();
    }
    if (phaseName === 'done' && toState === 'collapsed' && fromState !== 'void') {
      this.afterCollapse.emit();
    }
  }

  /** Checks whether the expansion panel's content contains the currently-focused element. */
  _containsFocus (): boolean {
    if (this._body && this._document) {
      const focusedElement = this._document.activeElement;
      const bodyElement = this._body.nativeElement;
      return focusedElement === bodyElement || bodyElement.contains(focusedElement);
    }

    return false;
  }
}
