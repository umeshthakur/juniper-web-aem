export {
  SwAccordionModule,
  SwAccordionDirective,
  SwAccordionPanelComponent,
  SwAccordionPanelHeaderComponent
} from './accordion.module';
