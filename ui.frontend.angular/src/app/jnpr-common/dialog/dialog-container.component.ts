import {
  ChangeDetectorRef,
  Component,
  ComponentRef,
  ElementRef,
  EmbeddedViewRef,
  EventEmitter,
  Inject,
  Optional,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { animate, AnimationEvent, state, style, transition, trigger } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { BasePortalOutlet, CdkPortalOutlet, ComponentPortal, TemplatePortal } from '@angular/cdk/portal';
import { FocusTrap, FocusTrapFactory } from '@angular/cdk/a11y';

import { SwDialogConfig } from './dialog-config.class';
import { ANIM_EASING_IN_CURVE, ANIM_EASING_OUT_CURVE } from '../../core/animation/animation';

const dialogContainerSelector = 'sw-dialog-container';

/**
 * Throws an exception for the case when a ComponentPortal is
 * attached to a DomPortalOutlet without an origin.
 */
export function throwSwDialogContentAlreadyAttachedError () {
  throw Error('Attempting to attach dialog content after content is already attached');
}

/**
 * Internal component that wraps user-provided dialog content.
 */
@Component({
  selector: dialogContainerSelector,
  templateUrl: './dialog-container.component.html',
  styleUrls: ['./dialog.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('slideDialog', [
      /**
       * Note: The `enter` animation doesn't transition to something like `translate3d(0, 0, 0)
       * scale(1)`, because for some reason specifying the transform explicitly, causes IE both
       * to blur the dialog content and decimate the animation performance. Leaving it as `none`
       * solves both issues.
       */
      state('enter', style({ transform: 'none', opacity: 1 })),
      state('void', style({ transform: 'translate3d(0, 2%, 0) scale(0.9)', opacity: 0 })),
      state('exit', style({ transform: 'translate3d(0, 2%, 0)', opacity: 0 })),
      transition('* => enter', animate('200ms ' + ANIM_EASING_OUT_CURVE)),
      transition('enter => *', animate('250ms ' + ANIM_EASING_IN_CURVE))
    ])
  ],
  // tslint:disable-next-line use-host-property-decorator
  host: {
    'class': dialogContainerSelector,
    'tabIndex': '-1',
    '[attr.role]': '_config?.role',
    '[attr.aria-labelledby]': '_ariaLabelledBy',
    '[attr.aria-describedby]': '_config?.ariaDescribedBy || null',
    '[@slideDialog]': '_state',
    '(@slideDialog.start)': '_onAnimationStart($event)',
    '(@slideDialog.done)': '_onAnimationDone($event)'
  }
})
export class SwDialogContainerComponent extends BasePortalOutlet {
  /** The portal host inside of this container into which the dialog content will be loaded. */
  @ViewChild(CdkPortalOutlet, {static:true}) _portalOutlet: CdkPortalOutlet;

  /** The class that traps and manages focus within the dialog. */
  private _focusTrap: FocusTrap;

  /** Element that was focused before the dialog was opened. Save this to restore upon close. */
  private _elementFocusedBeforeDialogWasOpened: HTMLElement | null = null;

  /** The dialog configuration. */
  _config: SwDialogConfig;

  /** State of the dialog animation. */
  _state: 'void' | 'enter' | 'exit' = 'enter';

  /** Emits when an animation state changes. */
  _animationStateChanged = new EventEmitter<AnimationEvent>();

  /** ID of the element that should be considered as the dialog's label. */
  _ariaLabelledBy: string | null = null;

  constructor (
    private _elementRef: ElementRef,
    private _focusTrapFactory: FocusTrapFactory,
    private _changeDetectorRef: ChangeDetectorRef,
    @Optional() @Inject(DOCUMENT) private _document: any) {

    super();
  }

  /** Attach a ComponentPortal as content to this dialog container */
  attachComponentPortal<T> (portal: ComponentPortal<T>): ComponentRef<T> {
    if (this._portalOutlet.hasAttached()) {
      throwSwDialogContentAlreadyAttachedError();
    }

    this._savePreviouslyFocusedElement();
    return this._portalOutlet.attachComponentPortal(portal);
  }

  /** Attach a TemplatePortal as content to this dialog container. */
  attachTemplatePortal<C> (portal: TemplatePortal<C>): EmbeddedViewRef<C> {
    if (this._portalOutlet.hasAttached()) {
      throwSwDialogContentAlreadyAttachedError();
    }

    this._savePreviouslyFocusedElement();
    return this._portalOutlet.attachTemplatePortal(portal);
  }

  /** Moves the focus inside the focus trap. */
  private _trapFocus () {
    if (!this._focusTrap) {
      this._focusTrap = this._focusTrapFactory.create(this._elementRef.nativeElement);
    }

    /**
     * If were to attempt to focus immediately, then the content of the dialog would not yet be
     * ready in instances where change detection has to run first. To deal with this, we simply
     * wait for the microtask queue to be empty.
     */
    this._focusTrap.focusInitialElementWhenReady();
  }

  /** Restores focus to the element that was focused before the dialog opened. */
  private _restoreFocus () {
    const toFocus = this._elementFocusedBeforeDialogWasOpened;

    // We need the extra check, because IE can set the `activeElement` to null in some cases.
    if (toFocus && typeof toFocus.focus === 'function') {
      toFocus.focus();
    }

    if (this._focusTrap) {
      this._focusTrap.destroy();
    }
  }

  /** Saves a reference to the element that was focused before the dialog was opened. */
  private _savePreviouslyFocusedElement () {
    if (this._document) {
      this._elementFocusedBeforeDialogWasOpened = this._document.activeElement as HTMLElement;

      // Move focus onto the dialog immediately after render in order to prevent the user from
      // accidentally opening multiple dialogs simultaneously.
      Promise.resolve().then(() => this._elementRef.nativeElement.focus());
    }
  }

  /** Callback, invoked whenever an animation on the host completes. */
  _onAnimationDone (event: AnimationEvent) {
    if (event.toState === 'enter') {
      this._trapFocus();
    } else if (event.toState === 'exit') {
      this._restoreFocus();
    }

    this._animationStateChanged.emit(event);
  }

  /** Callback, invoked when an animation on the host starts. */
  _onAnimationStart (event: AnimationEvent) {
    this._animationStateChanged.emit(event);
  }

  /** Starts the dialog exit animation. */
  _startExitAnimation (): void {
    this._state = 'exit';

    // Mark the container for check so it can react if the
    // view container is using OnPush change detection.
    this._changeDetectorRef.markForCheck();
  }
}
