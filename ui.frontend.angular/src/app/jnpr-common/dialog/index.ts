import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PortalModule } from '@angular/cdk/portal';
import { A11yModule } from '@angular/cdk/a11y';
import { OverlayModule } from '@angular/cdk/overlay';

import { SwButtonModule } from '../button/button.component';
import { SwIconModule } from '../icon/index';
import { SwToolbarModule } from '../toolbar/toolbar.component';

import { SwDialogContainerComponent } from './dialog-container.component';
import { SwDialogActions, SwDialogClose, SwDialogContent, SwDialogTitle } from './dialog-content-directives';
import { SW_DIALOG_SCROLL_STRATEGY_PROVIDER, SwDialog } from './dialog.service';

@NgModule( {
  imports: [
    A11yModule,
    CommonModule,
    OverlayModule,
    PortalModule,
    SwButtonModule,
    SwIconModule,
    SwToolbarModule
  ],
  exports: [
    SwDialogContainerComponent,
    SwDialogActions,
    SwDialogClose,
    SwDialogContent,
    SwDialogTitle
  ],
  declarations: [
    SwDialogContainerComponent,
    SwDialogActions,
    SwDialogClose,
    SwDialogContent,
    SwDialogTitle
  ],
  providers: [
    SwDialog,
    SW_DIALOG_SCROLL_STRATEGY_PROVIDER
  ],
  entryComponents: [
    SwDialogContainerComponent
  ]
} )
export class SwDialogModule { }

export { SwDialog, SW_DIALOG_DATA, SW_DIALOG_SCROLL_STRATEGY } from './dialog.service';
export { SwDialogContainerComponent } from './dialog-container.component';
export { SwDialogActions, SwDialogClose, SwDialogContent, SwDialogTitle } from './dialog-content-directives';
export { DialogPosition, SwDialogConfig, SwDialogRole } from './dialog-config.class';
export { SwDialogRef } from './dialog-ref.class';
