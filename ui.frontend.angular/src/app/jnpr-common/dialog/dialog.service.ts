import {
  ComponentRef,
  Inject,
  Injectable,
  InjectionToken,
  Injector,
  Optional,
  SkipSelf,
  TemplateRef
} from '@angular/core';
import { Location } from '@angular/common';
import { Overlay, OverlayConfig, OverlayRef, ScrollStrategy } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector, TemplatePortal } from '@angular/cdk/portal';
import { defer, Observable, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';

import { ComponentType } from '../../core/generics/index';
import { SwDialogConfig } from './dialog-config.class';
import { SwDialogContainerComponent } from './dialog-container.component';
import { SwDialogRef } from './dialog-ref.class';

export const SW_DIALOG_DATA = new InjectionToken<any>('SwDialogData');

/** Injection token that determines the scroll handling while the dialog is open. */
export const SW_DIALOG_SCROLL_STRATEGY =
    new InjectionToken<() => ScrollStrategy>('sw-dialog-scroll-strategy');

export function SW_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY (overlay: Overlay):
    () => ScrollStrategy {
  return () => overlay.scrollStrategies.block();
}

export const SW_DIALOG_SCROLL_STRATEGY_PROVIDER = {
  provide: SW_DIALOG_SCROLL_STRATEGY,
  deps: [Overlay],
  useFactory: SW_DIALOG_SCROLL_STRATEGY_PROVIDER_FACTORY
};

const SW_DIALOG_OVERLAY_CLASSES = ['sw-dialog-overlay-pane'];

/**
 * Service to open modal dialogs.
 */
@Injectable()
export class SwDialog {
  private _openDialogsAtThisLevel: SwDialogRef<any>[] = [];
  private _afterAllClosedAtThisLevel = new Subject<void>();
  private _afterOpenAtThisLevel = new Subject<SwDialogRef<any>>();

  /** Keeps track of the currently-open dialogs. */
  get openDialogs (): SwDialogRef<any>[] {
    return this._parentDialog ? this._parentDialog.openDialogs : this._openDialogsAtThisLevel;
  }

  /** Stream that emits when a dialog has been opened. */
  get afterOpen (): Subject<SwDialogRef<any>> {
    return this._parentDialog ? this._parentDialog.afterOpen : this._afterOpenAtThisLevel;
  }

  get _afterAllClosed (): Subject<void> {
    const parent = this._parentDialog;
    return parent ? parent._afterAllClosed : this._afterAllClosedAtThisLevel;
  }

  /**
   * Stream that emits when all open dialog have finished closing.
   * Will emit on subscribe if there are no open dialogs to begin with.
   */
  afterAllClosed: Observable<void> = defer(() => (
    this.openDialogs.length ?
      this._afterAllClosed :
      this._afterAllClosed.pipe(startWith(undefined))
    )
  );

  constructor (
      private _overlay: Overlay,
      private _injector: Injector,
      @Inject(SW_DIALOG_SCROLL_STRATEGY) private _scrollStrategy,
      @Optional() _location: Location,
      @Optional() @SkipSelf() private _parentDialog: SwDialog) {

    /**
     * Close all of the dialogs when the user goes forwards/backwards in history or when the
     * location hash changes. Note that this usually doesn't include clicking on links (unless
     * the user is using the `HashLocationStrategy`).
     */
    if (!_parentDialog && _location) {
      _location.subscribe(() => this.closeAll());
    }
  }

  /**
   * Opens a modal dialog containing the given component.
   * @param componentOrTemplateRef Type of the component to load into the dialog,
   *     or a TemplateRef to instantiate as the dialog content.
   * @param config Extra configuration options.
   * @returns Reference to the newly-opened dialog.
   */
  open<T, D = any> (componentOrTemplateRef: ComponentType<T> | TemplateRef<T>,
          config?: SwDialogConfig<D>): SwDialogRef<T> {

    config = Object.assign(new SwDialogConfig(), config);

    if (config.id && this.getDialogById(config.id)) {
      throw Error(`Dialog with id "${config.id}" exists already. The dialog id must be unique.`);
    }

    const overlayRef = this._createOverlay(config);
    const dialogContainer = this._attachDialogContainer(overlayRef, config);
    const dialogRef =
        this._attachDialogContent<T>(componentOrTemplateRef, dialogContainer, overlayRef, config);

    this.openDialogs.push(dialogRef);
    dialogRef.afterClosed().subscribe(() => this._removeOpenDialog(dialogRef));
    this.afterOpen.next(dialogRef);

    return dialogRef;
  }

  /**
   * Closes all of the currently-open dialogs.
   */
  closeAll (): void {
    let i = this.openDialogs.length;

    while (i--) {
      /**
       * The `_openDialogs` property isn't updated after close until the rxjs subscription
       * runs on the next microtask, in addition to modifying the array as we're going
       * through it. We loop through all of them and call close without assuming that
       * they'll be removed from the list instantaneously.
       */
      this.openDialogs[i].close();
    }
  }

  /**
   * Finds an open dialog by its id.
   * @param id ID to use when looking up the dialog.
   */
  getDialogById (id: string): SwDialogRef<any> | undefined {
    return this.openDialogs.find(dialog => dialog.id === id);
  }

  /**
   * Creates the overlay into which the dialog will be loaded.
   * @param config The dialog configuration.
   * @returns A promise resolving to the OverlayRef for the created overlay.
   */
  private _createOverlay (config: SwDialogConfig): OverlayRef {
    const overlayState = this._getOverlayConfig(config);
    return this._overlay.create(overlayState);
  }

  /**
   * Creates an overlay state from a dialog config.
   * @param dialogConfig The dialog configuration.
   * @returns The overlay configuration.
   */
  private _getOverlayConfig (dialogConfig: SwDialogConfig): OverlayConfig {
    const state = new OverlayConfig({
      positionStrategy: this._overlay.position().global(),
      scrollStrategy: dialogConfig.scrollStrategy || this._scrollStrategy(),
      hasBackdrop: dialogConfig.hasBackdrop,
      minWidth: dialogConfig.minWidth,
      minHeight: dialogConfig.minHeight,
      maxWidth: dialogConfig.maxWidth,
      maxHeight: dialogConfig.maxHeight
    });

    /**
     * We can't do a spread here, because IE doesn't support setting multiple classes.
     * Filter out empty classes to avoid errors around empty tokens
     */
    state.panelClass = (<string[]>[])
      .concat(dialogConfig.panelClass)
      .filter(s => s.length)
      .concat(SW_DIALOG_OVERLAY_CLASSES);

    if (dialogConfig.backdropClass) {
      state.backdropClass = dialogConfig.backdropClass;
    }

    return state;
  }

  /**
   * Attaches an SwDialogContainer to a dialog's already-created overlay.
   * @param overlay Reference to the dialog's underlying overlay.
   * @param config The dialog configuration.
   * @returns A promise resolving to a ComponentRef for the attached container.
   */
  private _attachDialogContainer (overlay: OverlayRef, config: SwDialogConfig): SwDialogContainerComponent {
    const containerPortal = new ComponentPortal(SwDialogContainerComponent, config.viewContainerRef);
    const containerRef: ComponentRef<SwDialogContainerComponent> = overlay.attach(containerPortal);
    containerRef.instance._config = config;

    return containerRef.instance;
  }

  /**
   * Attaches the user-provided component to the already-created SwDialogContainer.
   * @param componentOrTemplateRef The type of component being loaded into the dialog,
   *     or a TemplateRef to instantiate as the content.
   * @param dialogContainer Reference to the wrapping SwDialogContainer.
   * @param overlayRef Reference to the overlay in which the dialog resides.
   * @param config The dialog configuration.
   * @returns A promise resolving to the SwDialogRef that should be returned to the user.
   */
  private _attachDialogContent<T> (
      componentOrTemplateRef: ComponentType<T> | TemplateRef<T>,
      dialogContainer: SwDialogContainerComponent,
      overlayRef: OverlayRef,
      config: SwDialogConfig): SwDialogRef<T> {

    /**
     * Create a reference to the dialog we're creating in order to give the user a handle
     * to modify and close it.
     */
    const dialogRef = new SwDialogRef<T>(overlayRef, dialogContainer, config.id);

    // When the dialog backdrop is clicked, we want to close it.
    if (config.hasBackdrop) {
      overlayRef.backdropClick().subscribe(() => {
        if (!dialogRef.disableClose) {
          dialogRef.close();
        }
      });
    }

    if (componentOrTemplateRef instanceof TemplateRef) {
      dialogContainer.attachTemplatePortal(
        new TemplatePortal<T>(componentOrTemplateRef, null,
          { $implicit: config.data, dialogRef } as any));
    } else {
      const injector = this._createInjector<T>(config, dialogRef, dialogContainer);
      const contentRef = dialogContainer.attachComponentPortal<T>(
          new ComponentPortal(componentOrTemplateRef, undefined, injector));
      dialogRef.componentInstance = contentRef.instance;
    }

    dialogRef
      .updateSize(config.width, config.height)
      .updatePosition(config.position);

    return dialogRef;
  }

  /**
   * Creates a custom injector to be used inside the dialog. This allows a component loaded inside
   * of a dialog to close itself and, optionally, to return a value.
   * @param config Config object that is used to construct the dialog.
   * @param dialogRef Reference to the dialog.
   * @param container Dialog container element that wraps all of the contents.
   * @returns The custom injector that can be used inside the dialog.
   */
  private _createInjector<T> (
      config: SwDialogConfig,
      dialogRef: SwDialogRef<T>,
      dialogContainer: SwDialogContainerComponent): PortalInjector {

    const userInjector = config && config.viewContainerRef && config.viewContainerRef.injector;
    const injectionTokens = new WeakMap();

    injectionTokens.set(SwDialogRef, dialogRef);
    injectionTokens.set(SwDialogContainerComponent, dialogContainer);
    injectionTokens.set(SW_DIALOG_DATA, config.data);

    return new PortalInjector(userInjector || this._injector, injectionTokens);
  }

  /**
   * Removes a dialog from the array of open dialogs.
   * @param dialogRef Dialog to be removed.
   */
  private _removeOpenDialog (dialogRef: SwDialogRef<any>) {
    const index = this.openDialogs.indexOf(dialogRef);

    if (index > -1) {
      this.openDialogs.splice(index, 1);

      // no open dialogs are left, call next on afterAllClosed Subject
      if (!this.openDialogs.length) {
        this._afterAllClosed.next();
      }
    }
  }
}
