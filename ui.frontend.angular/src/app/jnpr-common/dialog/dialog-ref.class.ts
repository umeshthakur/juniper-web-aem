import { GlobalPositionStrategy, OverlayRef } from '@angular/cdk/overlay';
import { Observable, Subject } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { DialogPosition } from './dialog-config.class';
import { SwDialogContainerComponent } from './dialog-container.component';
import { Keycodes } from '../../core/keyboard/keycodes';

// TODO: resizing
// TODO: afterOpen

// Counter for unique dialog ids.
let uniqueId = 0;

/** Reference to a dialog opened via the SwDialog service. */
export class SwDialogRef<T> {
  /** The instance of component opened into the dialog. */
  componentInstance: T;

  /** Whether the user is allowed to close the dialog. */
  disableClose = this._containerInstance._config.disableClose;

  /** Subject for notifying the user that the dialog has finished closing. */
  private _afterClosed: Subject<any> = new Subject();

  /** Subject for notifying the user that the dialog has started closing. */
  private _beforeClose: Subject<any> = new Subject();

  /** Result to be passed to afterClosed. */
  private _result: any;

  constructor (private _overlayRef: OverlayRef,
    private _containerInstance: SwDialogContainerComponent,
    public readonly id: string = `sw-dialog-${uniqueId++}`) {

    _containerInstance._animationStateChanged.asObservable()
      .pipe(
        filter(e => e.phaseName === 'done' && e.toState === 'exit'),
        first()
      )
      .subscribe(() => {
        this._overlayRef.dispose();
        this._afterClosed.next(this._result);
        this._afterClosed.complete();
        this.componentInstance = null;
      });

    _overlayRef.keydownEvents()
      // tslint:disable-next-line deprecation
      .pipe(filter(event => event.keyCode === Keycodes.ESCAPE && !this.disableClose))
      .subscribe(() => this.close());
  }

  /**
   * Close the dialog.
   * @param dialogResult Optional result to return to the dialog opener.
   */
  close (dialogResult?: any): void {
    this._result = dialogResult;

    // Transition the backdrop in parallel to the dialog.
    this._containerInstance._animationStateChanged.asObservable()
      .pipe(
        filter(e => e.phaseName === 'start'),
        first()
      )
      .subscribe(() => {
        this._beforeClose.next(dialogResult);
        this._beforeClose.complete();
        this._overlayRef.detachBackdrop();
      });

    this._containerInstance._startExitAnimation();
  }

  /**
   * Gets an observable that is notified when the dialog is finished closing.
   */
  afterClosed (): Observable<any> {
    return this._afterClosed.asObservable();
  }

  /**
   * Gets an observable that is notified when the dialog has started closing.
   */
  beforeClose (): Observable<any> {
    return this._beforeClose.asObservable();
  }

  /**
   * Gets an observable that emits when the overlay's backdrop has been clicked.
   */
  backdropClick (): Observable<MouseEvent> {
    return this._overlayRef.backdropClick();
  }

  /**
   * Gets an observable that emits when keydown events are targeted on the overlay.
   */
  keydownEvents (): Observable<KeyboardEvent> {
    return this._overlayRef.keydownEvents();
  }

  /**
   * Updates the dialog's position.
   * @param position New dialog position.
   */
  updatePosition (position?: DialogPosition): this {
    const strategy = this._getPositionStrategy();

    if (position && (position.left || position.right)) {
      position.left ? strategy.left(position.left) : strategy.right(position.right);
    } else {
      strategy.centerHorizontally();
    }

    if (position && (position.top || position.bottom)) {
      position.top ? strategy.top(position.top) : strategy.bottom(position.bottom);
    } else {
      strategy.centerVertically();
    }

    this._overlayRef.updatePosition();

    return this;
  }

  /**
   * Updates the dialog's width and height.
   * @param width New width of the dialog.
   * @param height New height of the dialog.
   */
  updateSize (width = 'auto', height = 'auto'): this {
    this._overlayRef.updateSize({ width, height });
    this._overlayRef.updatePosition();
    return this;
  }

  /** Fetches the position strategy object from the overlay ref. */
  private _getPositionStrategy (): GlobalPositionStrategy {
    return this._overlayRef.getConfig().positionStrategy as GlobalPositionStrategy;
  }
}
