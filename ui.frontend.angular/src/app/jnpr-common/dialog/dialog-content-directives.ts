import { Directive, Input, OnInit, Optional } from '@angular/core';
import { SwDialogRef } from './dialog-ref.class';
import { SwDialogContainerComponent } from './dialog-container.component';

// tslint:disable directive-selector directive-class-suffix use-host-property-decorator no-input-rename

/** Counter used to generate unique IDs for dialog elements. */
let dialogElementUid = 0;

/** Button that will close the current dialog */
@Directive({
  selector: 'button[sw-dialog-close]',
  host: {
    '(click)': 'dialogRef.close(dialogResult)',
    '[attr.aria-label]': 'ariaLabel',
    'type': 'button'
  }
})
export class SwDialogClose {
  /** Screenreader label for the button. */
  @Input('aria-label') ariaLabel: string = 'Close dialog';

  /** Dialog close input. */
  @Input('sw-dialog-close') dialogResult: any;

  constructor (public dialogRef: SwDialogRef<any>) {}
}

/** Title of a dialog element. Stays fixed to the top of the dialog when scrolling */
@Directive({
  selector: '[sw-dialog-title]',
  host: {
    'class': 'sw-dialog-title',
    '[id]': 'id'
  }
})
export class SwDialogTitle implements OnInit {
  @Input() id = `sw-dialog-title-${dialogElementUid++}`;

  constructor (@Optional() private _container: SwDialogContainerComponent) {}

  ngOnInit () {
    if (this._container && !this._container._ariaLabelledBy) {
      Promise.resolve().then(() => this._container._ariaLabelledBy = this.id);
    }
  }
}

/** Scrollable content container of a dialog */
@Directive({
  selector: '[sw-dialog-content], sw-dialog-content',
  host: { 'class': 'sw-dialog-content' }
})
export class SwDialogContent { }

/**
 * Container for the bottom action buttons in a dialog.
 * Stays fixed to the bottom when scrolling.
 */
@Directive({
  selector: '[sw-dialog-actions], sw-dialog-actions',
  host: { 'class': 'sw-dialog-actions' }
})
export class SwDialogActions { }
