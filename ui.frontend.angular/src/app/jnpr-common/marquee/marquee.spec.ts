import { Component, DebugElement } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { marqueeVariantType, SwMarqueeComponent, SwMarqueeModule } from './marquee.component';
import { TestAgainst } from '../core/testing/commonTestCases.spec';
import { SwEvalDirective } from '../eval/eval.module';

/**
 * Using a helper component due to testing issues related to OnPush
 * See https://github.com/angular/angular/issues/12313
 */
@Component({
  template: `<sw-marquee [properties]="props"></sw-marquee>`
})
class HelperComponent {
  props: Partial<SwMarqueeComponent> = {};
}

@Component({
  template: `
  <sw-marquee id="nonTranscluded" [properties]="marqueeProps"></sw-marquee>
  <sw-marquee id="otherTranscluded" [properties]="marqueeProps">
    <div>Action replacement</div>
  </sw-marquee>
  <sw-marquee id="transcluded" [properties]="marqueeProps">
    <div sw-marquee-action>Action replacement</div>
  </sw-marquee>
  `
})
class MarqueeActionTransclusionComponent {
  marqueeProps: Partial<SwMarqueeComponent> = {
    action: {
      url: 'https://www.google.com/',
      label: 'Go to Google'
    }
  };
}

describe('`SwMarqueeComponent`', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SwMarqueeModule
      ],
      declarations: [
        HelperComponent,
        MarqueeActionTransclusionComponent
      ]
    }).compileComponents();
  }));

  describe('authorable property', () => {
    let comp: SwMarqueeComponent;
    let fixture: ComponentFixture<SwMarqueeComponent>;
    const defaultVariant = 'primary';

    beforeEach(() => {
      fixture = TestBed.createComponent(SwMarqueeComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
    });

    describe('`variant`', () => {
      const currPropDefault = defaultVariant;

      describe('should be deemed as valid', () => {
        it('for all allowed variants', () => {
          const testValues: marqueeVariantType[] = ['primary','secondary'];
          TestAgainst.validValues(comp, 'variant', testValues);
        });
      });

      describe('should be deemed as invalid', () => {
        it('for any value that isn\'t any of the allowed variants', () => {
          TestAgainst.otherDataTypes(comp, 'variant', null, currPropDefault);
        });

        it('should reset the value to default when setting an incorrect value', () => {
          const valid = 'secondary';
          const invalid = 'pizza';
          TestAgainst.resettingToFallback(comp, 'variant', valid, invalid, currPropDefault);
        });
      });

      it('should recognize when the primary variant is set', () => {
        expect(comp._isPrimary).toBeTruthy();
        comp.variant = 'secondary';
        expect(comp._isPrimary).toBeFalsy();
        comp.variant = defaultVariant;
        expect(comp._isPrimary).toBeTruthy();
      });

      it('should set the variant at the host root', () => {
        let currVariant = defaultVariant;

        expect(fixture.nativeElement.getAttribute('sw-variant')).toBe(currVariant);

        comp.variant = currVariant = 'secondary';
        fixture.detectChanges();
        expect(fixture.nativeElement.getAttribute('sw-variant')).toBe(currVariant);
      });
    });

    describe('`aside`', () => {
      const currPropDefault = [];

      describe('should be deemed as valid', () => {
        it('arrays of objects the required properties', () => {
          const testValues = [
            [{ url: '', label: '' }],
            [{ url: '', label: '' }, { url: '', label: '' }]
          ];
          TestAgainst.validValues(comp, 'aside', testValues, TestAgainst.NON_STRICT_EQ);
        });

        it('arrays of objects with extra properties', () => {
          const testValues = [
            [{ url: '', label: '', onclick: '', target: '' }]
          ] as any;
          TestAgainst.validValues(comp, 'aside', testValues, TestAgainst.NON_STRICT_EQ);
        });
      });

      describe('should be deemed as invalid', () => {
        it('object[] not containing all the required properties', () => {
          const testValues = [
            [{ url: '' }],
            [{ label: '' }, { url: '', label: '' }]
          ] as any[];
          TestAgainst.invalidValues(comp, 'aside', testValues, currPropDefault, TestAgainst.NON_STRICT_EQ);
        });

        it('arrays of objects containing all the required properties but incorrect value types', () => {
          const testValues = [
            [{ url: 10, label: '' }],
            [{ url: '', label: 10 }, { url: '', label: '' }],
            [1,2,3],
            [{},2,''],
            [{ url: '', label: '' },2,'']
          ] as any[];
          TestAgainst.invalidValues(comp, 'aside', testValues, currPropDefault, TestAgainst.NON_STRICT_EQ);
        });

        it('for any value that isn\'t an array type', () => {
          TestAgainst.otherDataTypes(comp, 'aside', 'array', currPropDefault, TestAgainst.NON_STRICT_EQ);
        });

        it('should reset the value to default when setting an incorrect value', () => {
          const valid = [{ url: '', label: '' }];
          const invalid = [{ url: '' }];
          TestAgainst.resettingToFallback(comp, 'aside', valid, invalid, currPropDefault, TestAgainst.NON_STRICT_EQ);
        });
      });

      it('should indicate aside presence at the host root', () => {
        let currAside = currPropDefault;
        const correctTestValue = [{ url: '', label: '' }];
        const incorrectTestValue = [{ url: '' }] as any[];
        const asideAttr = 'sw-has-aside';

        expect(comp.aside).toEqual(currAside);
        expect(comp._attrHasAside).toBe(null);
        expect(fixture.nativeElement.hasAttribute(asideAttr)).toBe(false);

        comp.aside = currAside = correctTestValue;
        fixture.detectChanges();
        expect(comp._attrHasAside).toBe(true);
        expect(fixture.nativeElement.hasAttribute(asideAttr)).toBe(true);

        comp.aside = currAside = incorrectTestValue;
        fixture.detectChanges();
        expect(comp._attrHasAside).toBe(null);
        expect(fixture.nativeElement.hasAttribute(asideAttr)).toBe(false);
      });
    });
  });

  describe('DOM checks', () => {
    let comp: HelperComponent;
    let fixture: ComponentFixture<HelperComponent>;
    let marqueeDebugEl: DebugElement;

    beforeEach(() => {
      fixture = TestBed.createComponent(HelperComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
      marqueeDebugEl = fixture.debugElement.query(By.directive(SwMarqueeComponent));
    });

    describe('`action`', () => {
      it('should set `target` when provided', () => {
        comp.props = {
          action: { url: 'url', label: 'label' }
        };
        fixture.detectChanges();

        const actionWrapper = marqueeDebugEl.query(By.css('.action-wrapper:not(.no-projection)'));
        let action = actionWrapper.queryAll(By.css('a'));
        expect(action.length).toBe(1);
        expect(action[0].attributes.target).toBe('');

        comp.props = {
          action: { url: 'url', label: 'label', target: 'PIZZA' }
        };
        fixture.detectChanges();

        action = actionWrapper.queryAll(By.css('a'));
        expect(action.length).toBe(1);
        expect(action[0].attributes.target).toBe('PIZZA');
      });

      it('should set `onclick` when provided', () => {
        comp.props = {
          action: { url: 'url', label: 'label' }
        };
        fixture.detectChanges();

        const actionWrapper = marqueeDebugEl.query(By.css('.action-wrapper:not(.no-projection)'));
        let actionEvals = actionWrapper.queryAll(By.directive(SwEvalDirective))
            .map(de => de.injector.get<SwEvalDirective>(SwEvalDirective));
        expect(actionEvals.length).toBe(1);
        expect(actionEvals[0].code).toBeUndefined();

        comp.props = {
          action: { url: 'url', label: 'label', onclick: 'PIZZA' }
        };
        fixture.detectChanges();

        actionEvals = actionWrapper.queryAll(By.directive(SwEvalDirective))
            .map(de => de.injector.get<SwEvalDirective>(SwEvalDirective));
        expect(actionEvals.length).toBe(1);
        expect(actionEvals[0].code).toBe('PIZZA');
      });
    });

    describe('`aside`', () => {
      it('should set `target` when provided', () => {
        comp.props = {
          aside: [{ url: 'url', label: 'label' }, { url: 'url', label: 'label' }]
        };
        fixture.detectChanges();

        let asideButtons = marqueeDebugEl.queryAll(By.css('.aside-item__action'));
        expect(asideButtons.length).toBe(2);
        expect(asideButtons.map(a => a.properties.target)).toEqual(['', '']);
        expect(asideButtons.map(a => a.nativeElement.getAttribute('target'))).toEqual(['', '']);

        comp.props = {
          aside: [{ url: 'url', label: 'label', target: 'HELLO' }, { url: 'url', label: 'label', target: 'PIZZA' }]
        };
        fixture.detectChanges();

        asideButtons = marqueeDebugEl.queryAll(By.css('.aside-item__action'));
        expect(asideButtons.length).toBe(2);
        expect(asideButtons.map(a => a.properties.target)).toEqual(['HELLO', 'PIZZA']);
        expect(asideButtons.map(a => a.nativeElement.getAttribute('target'))).toEqual(['HELLO', 'PIZZA']);
      });

      it('should set `onclick` when provided', () => {
        comp.props = {
          aside: [{ url: 'url', label: 'label' }, { url: 'url', label: 'label' }]
        };
        fixture.detectChanges();

        const aside = marqueeDebugEl.query(By.css('.aside'));
        let asideEvals = aside.queryAll(By.directive(SwEvalDirective))
            .map(de => de.injector.get<SwEvalDirective>(SwEvalDirective));
        expect(asideEvals.length).toBe(2);
        expect(asideEvals.map(ae => ae.code)).toEqual([undefined, undefined]);

        comp.props = {
          aside: [{ url: 'url', label: 'label', onclick: 'HELLO' }, { url: 'url', label: 'label', onclick: 'PIZZA' }]
        };
        fixture.detectChanges();

        asideEvals = aside.queryAll(By.directive(SwEvalDirective))
            .map(de => de.injector.get<SwEvalDirective>(SwEvalDirective));
        expect(asideEvals.length).toBe(2);
        expect(asideEvals.map(ae => ae.code)).toEqual(['HELLO', 'PIZZA']);
      });
    });
  });

  describe('action transclusion', () => {
    let comp: MarqueeActionTransclusionComponent;
    let fixture: ComponentFixture<MarqueeActionTransclusionComponent>;
    let debugNonTranscludedEl: DebugElement;
    let nativeNonTranscludedEl: HTMLElement;
    let debugOtherTranscludedEl: DebugElement;
    let nativeOtherTranscludedEl: HTMLElement;
    let debugTranscludedEl: DebugElement;
    let nativeTranscludedEl: HTMLElement;
    let actionWrapperSel: NodeListOf<Element>;
    let actionSel: NodeListOf<Element>;
    const actionWrapperSelector = '.action-wrapper';
    const actionSelector = '[sw-marquee-action]';

    beforeEach(() => {
      fixture = TestBed.createComponent(MarqueeActionTransclusionComponent);
      comp = fixture.componentInstance;
      fixture.detectChanges();
      debugNonTranscludedEl = fixture.debugElement.query(By.css('#nonTranscluded'));
      nativeNonTranscludedEl = debugNonTranscludedEl.nativeElement;
      debugOtherTranscludedEl = fixture.debugElement.query(By.css('#otherTranscluded'));
      nativeOtherTranscludedEl = debugOtherTranscludedEl.nativeElement;
      debugTranscludedEl = fixture.debugElement.query(By.css('#transcluded'));
      nativeTranscludedEl = debugTranscludedEl.nativeElement;
    });

    it(`should show the data-driven when there's no elements to transclude`, () => {
      actionWrapperSel = nativeNonTranscludedEl.querySelectorAll(actionWrapperSelector);
      actionSel = nativeNonTranscludedEl.querySelectorAll(actionSelector);
      const buttonSel = nativeNonTranscludedEl.querySelector(`${actionWrapperSelector} a`) as HTMLElement;

      expect(actionWrapperSel.length).toBe(2);
      expect(actionSel.length).toBe(0);
      expect(buttonSel).not.toBeNull();
      expect(buttonSel.getAttribute('href')).toBe(comp.marqueeProps.action.url);
    });

    it(`should show the data-driven when there's the incorrect elements to transclude`, () => {
      actionWrapperSel = nativeOtherTranscludedEl.querySelectorAll(actionWrapperSelector);
      actionSel = nativeOtherTranscludedEl.querySelectorAll(actionSelector);
      const buttonSel = nativeOtherTranscludedEl.querySelector(`${actionWrapperSelector} a`) as HTMLElement;

      expect(actionWrapperSel.length).toBe(2);
      expect(actionSel.length).toBe(0);
      expect(buttonSel).not.toBeNull();
      expect(buttonSel.getAttribute('href')).toBe(comp.marqueeProps.action.url);
    });

    it(`should show the transcluded action when there's the correct element to transclude`, () => {
      actionWrapperSel = nativeTranscludedEl.querySelectorAll(actionWrapperSelector);
      actionSel = nativeTranscludedEl.querySelectorAll(actionSelector);

      expect(actionWrapperSel.length).toBe(1);
      expect(actionSel.length).toBe(1);
      expect(actionSel[0].textContent).toBe('Action replacement');
    });

  });

});
