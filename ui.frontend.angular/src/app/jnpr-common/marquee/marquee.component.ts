import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, Directive, HostBinding, NgModule } from '@angular/core';

import { SwBaseComponent } from '../../core/base';
import { DataType, simpleObjToSchemaConverter, SwInputProp } from '../../core/validation';
import { SwBackgroundMediaModule } from '../../modules/backgroundMedia/index';
import { SwEvalModule } from '../../modules/eval/eval.module';
import { isVideo } from '../../util/index';
import { SwButtonModule } from '../button/button.component';
import { SwDialog, SwDialogModule } from '../dialog/index';
import { SwIconModule } from '../../jnpr-common/icon/index';
import { SwVideoDialogComponent, SwVideoDialogModule } from '../video/dialog/video-dialog.component';
import { SwVideoAction } from '../../core/base/action/action.class';

import { SwLogoBarComponent, SwLogoBarModule } from '../logo-bar/logo-bar.component';
import { SwAnimateInViewModule } from '../../modules/animation/anim-in-view.module';
import { CanIconOverride, iconOverrideRegex } from '../common-behaviors/icon-override';
import { SwPeripheralDialogModule, SwPeripheralDialogService } from '../../modules/peripheral-dialog/index';

export const marqueeSelector = 'sw-marquee';

export type marqueeVariantType = 'primary' | 'secondary';
export const marqueeDefaultVariant: marqueeVariantType = 'primary';
export const marqueeVariantRegex = /^(primary|secondary)$/;
export type marqueeDensityType = 'regular' | 'condensed';
export const marqueeDefaultDensity: marqueeDensityType = 'regular';
export const marqueeDensityRegex = /^(regular|condensed)$/;
export const marqueeDefaultAction: SwVideoAction = { url: '', label: '' };
export const l10nDefaults = { watch_video: 'Watch video' };

export interface MarqueeAction extends SwVideoAction, CanIconOverride {}

export interface MarqueeAsideItem extends MarqueeAction, CanIconOverride {
  eyebrow?: string;
  image?: string;
  cta?: string;
}

/**
 * Marquee Component
 */
@Component({
  selector: marqueeSelector,
  templateUrl: './marquee.component.html',
  styleUrls: ['./marquee.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwMarqueeComponent extends SwBaseComponent {

  constructor (private _dialog: SwDialog, private _periDialog: SwPeripheralDialogService) {
    super();
  }

  @HostBinding('attr.sw-variant')
  @SwInputProp<marqueeVariantType>(DataType.string, marqueeDefaultVariant, { pattern: marqueeVariantRegex.source })
  variant: marqueeVariantType;

  @HostBinding('attr.sw-density')
  @SwInputProp<marqueeDensityType>(DataType.string, marqueeDefaultDensity, { pattern: marqueeDensityRegex.source })
  density: marqueeDensityType;

  @SwInputProp<string>(DataType.string, '')
  eyebrow: string;

  @SwInputProp<string>(DataType.string, '')
  headline: string;

  @SwInputProp<string, string[]>(DataType.array, [], { type: DataType.string })
  body: string[];

  @SwInputProp<MarqueeAction>(DataType.object, marqueeDefaultAction, {
    schema: simpleObjToSchemaConverter(marqueeDefaultAction)
  })
  action: MarqueeAction;

  @SwInputProp<string>(DataType.string, '')
  backgroundImage: string;

  @SwInputProp<string>(DataType.string, '')
  backgroundVideo: string;

  @SwInputProp<MarqueeAsideItem, MarqueeAsideItem[]>(DataType.array, [], {
    schema: { items: simpleObjToSchemaConverter(marqueeDefaultAction) }
  })
  aside: MarqueeAsideItem[];

  @SwInputProp<any>(DataType.object, l10nDefaults, { schema: simpleObjToSchemaConverter(l10nDefaults) })
  l10n;

  @SwInputProp<Partial<SwLogoBarComponent>>(DataType.object, {})
  logo: Partial<SwLogoBarComponent>;

  get _logoValid (): boolean {
    return SwLogoBarComponent.hasMinimumProps(this.logo);
  }

  @HostBinding('attr.sw-has-logo')
  get _attrHasLogo (): boolean {
    return this._logoValid || null;
  }

  @HostBinding('attr.sw-has-aside')
  get _attrHasAside () {
    return this.aside.length > 0 ? true : null;
  }

  get _isPrimary (): boolean {
    return this.variant === marqueeDefaultVariant;
  }

  _actionIsVideo (action: MarqueeAction | MarqueeAsideItem) {
    return typeof action.url === 'string' ? isVideo(action.url) : false;
  }

  _actionIconOverride (action: MarqueeAction | MarqueeAsideItem): boolean {
    return !this._actionIsVideo(action) && iconOverrideRegex.test(action.iconOverride);
  }

  _handleActionClick (e: Event, actionItem: SwVideoAction) {
    if (this._periDialog.test(actionItem.url)) {
      e.preventDefault();
      this._periDialog.open(actionItem.url, actionItem.label);
    }
    if (isVideo(actionItem.url)) {
      e.preventDefault();
      this._openVideoModal(actionItem);
    }
  }

  private _openVideoModal (actionItem: SwVideoAction) {
    const videoConfig: SwVideoDialogComponent = {
      title: actionItem.label,
      config: {
        url: actionItem.url,
        endCard: actionItem.endCard && typeof actionItem.endCard === 'object' ? actionItem.endCard : {},
        options: {
          autoplay: 1
        }
      }
    };
    this._dialog.open(SwVideoDialogComponent, {
      data: videoConfig
    });
  }

  _hasStrLen (str: string) {
    return str && str.length && str.length > 0;
  }
}

// tslint:disable-next-line directive-selector
@Directive({ selector: '[sw-marquee-action]' })
export class SwMarqueeComponentActionDirective {}

@NgModule({
  imports: [
    CommonModule,
    SwButtonModule,
    SwIconModule,
    SwDialogModule,
    SwBackgroundMediaModule,
    SwVideoDialogModule,
    SwEvalModule.forRoot(),
    SwLogoBarModule,
    SwAnimateInViewModule,
    SwPeripheralDialogModule
  ],
  exports: [SwMarqueeComponent, SwMarqueeComponentActionDirective],
  declarations: [SwMarqueeComponent, SwMarqueeComponentActionDirective],
  entryComponents: [SwMarqueeComponent]
})
export class SwMarqueeModule {}
