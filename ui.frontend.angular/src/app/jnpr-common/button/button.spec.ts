import { Component } from '@angular/core';
import { async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { SwButtonModule } from './button.component';

/**
 * Test component containing an SwButtonComponent.
 */
@Component({
  selector: 'sw-test-app',
  template: `
    <button sw-button type="button" (click)="increment()"
      [disabled]="isDisabled" [color]="buttonColor">
      Go
    </button>
    <a href="https://www.theverge.com/" sw-button [disabled]="isDisabled" [color]="buttonColor">Link</a>
  `
})
class TestAppComponent {
  clickCount: number = 0;
  isDisabled: boolean = false;
  buttonColor;

  increment () {
    this.clickCount++;
  }
}

describe('SwButtonComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ SwButtonModule ],
      declarations: [ TestAppComponent ]
    }).compileComponents();
  }));

  /**
   * General button tests
   */

  it('should apply class based on color attribute', () => {
    const fixture = TestBed.createComponent(TestAppComponent);

    const testComponent = fixture.debugElement.componentInstance;
    const buttonDebugElement = fixture.debugElement.query(By.css('button'));
    const aDebugElement = fixture.debugElement.query(By.css('a'));

    testComponent.buttonColor = 'primary';
    fixture.detectChanges();
    expect(buttonDebugElement.nativeElement.getAttribute('sw-theme') === 'primary').toBe(true);
    expect(aDebugElement.nativeElement.getAttribute('sw-theme') === 'primary').toBe(true);

    testComponent.buttonColor = 'accent';
    fixture.detectChanges();
    expect(buttonDebugElement.nativeElement.getAttribute('sw-theme') === 'accent').toBe(true);
    expect(aDebugElement.nativeElement.getAttribute('sw-theme') === 'accent').toBe(true);
  });

  /**
   * Regular button tests
   */

  describe('button[sw-button]', () => {
    it('should handle a click on the button', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('button'));

      buttonDebugElement.nativeElement.click();
      expect(testComponent.clickCount).toBe(1);
    });

    it('should not increment if disabled', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('button'));

      testComponent.isDisabled = true;
      fixture.detectChanges();

      buttonDebugElement.nativeElement.click();

      expect(testComponent.clickCount).toBe(0);
    });

  });

  /**
   * Anchor button tests
   */

  describe('a[sw-button]', () => {
    it('should not redirect if disabled', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('a'));

      testComponent.isDisabled = true;
      fixture.detectChanges();

      buttonDebugElement.nativeElement.click();
    });

    it('should remove tabindex if disabled', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('a'));
      expect(buttonDebugElement.nativeElement.getAttribute('tabIndex')).toBe(null);

      testComponent.isDisabled = true;
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('tabIndex')).toBe('-1');
    });

    it('should add aria-disabled attribute if disabled', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('a'));
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled')).toBe('false');

      testComponent.isDisabled = true;
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled')).toBe('true');
    });

    it('should not add aria-disabled attribute if disabled is false', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('a'));
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled'))
        .toBe('false', 'Expect aria-disabled="false"');
      expect(buttonDebugElement.nativeElement.getAttribute('disabled'))
        .toBeNull('Expect disabled="false"');

      testComponent.isDisabled = false;
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled'))
        .toBe('false', 'Expect no aria-disabled');
      expect(buttonDebugElement.nativeElement.getAttribute('disabled'))
        .toBeNull('Expect no disabled');
    });

    it('should not add aria-disabled attribute if disabled is false', () => {
      const fixture = TestBed.createComponent(TestAppComponent);
      const testComponent = fixture.debugElement.componentInstance;
      const buttonDebugElement = fixture.debugElement.query(By.css('a'));
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled'))
        .toBe('false', 'Expect aria-disabled="false"');
      expect(buttonDebugElement.nativeElement.getAttribute('disabled'))
        .toBeNull('Expect disabled="false"');

      testComponent.isDisabled = false;
      fixture.detectChanges();
      expect(buttonDebugElement.nativeElement.getAttribute('aria-disabled'))
        .toBe('false', 'Expect no aria-disabled');
      expect(buttonDebugElement.nativeElement.getAttribute('disabled'))
        .toBeNull('Expect no disabled');
    });
  });

});
