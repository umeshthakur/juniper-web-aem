import { CommonModule } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  forwardRef,
  HostBinding,
  HostListener,
  Input,
  NgModule,
  Renderer2,
  ViewChild
} from '@angular/core';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Platform, PlatformModule } from '@angular/cdk/platform';
import { SwAbstractButton } from './button-abstract';

/**
 * Button component
 */
@Component({
  /** Selector includes tag and attribute to leverage the benefits of `button`. */
  // tslint:disable-next-line component-selector
  selector: `
    a[jnpr-primary-cta],button[jnpr-primary-cta], a[jnpr-secondary-cta],button[sw-button], a[sw-button],
    button[sw-raised-button], a[sw-raised-button],
    button[sw-outlined-button], a[sw-outlined-button],
    button[sw-icon-button], a[sw-icon-button],
    button[sw-fab], a[sw-fab],
    button[sw-mini-fab], a[sw-mini-fab]`,
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: SwAbstractButton, useExisting: forwardRef(() => SwButtonComponent) }]
})
export class SwButtonComponent implements SwAbstractButton {
  private _color: string;
  // private _size: string;
  _disabled: boolean = null;
  _target: string = '';
  _wrap: boolean = null;

  @ViewChild('contentWrapper') _contentWrapper: ElementRef;

  /** Whether the button has focus from the keyboard (not the mouse). Used for class binding. */
  @HostBinding('class.sw-button-focus') _isKeyboardFocused: boolean = false;

  @HostBinding('class.sw-button-hover') _shouldHover: boolean = true;

  /** Whether a mousedown has occurred on this element in the last 100ms. */
  _isMouseDown: boolean = false;

  constructor (private _elementRef: ElementRef, private _renderer: Renderer2, private _platform: Platform) {
    /** On iOS, avoid hover completely due to the sticky hover */
    this._shouldHover = this._platform.IOS ? false : true;
  }

  set textContent (text: string) {
    try {
      if (!this._contentWrapper) throw new Error(`Button view not yet initialized.`);
      if (typeof text !== 'string') throw new TypeError(`Expected \`string\` but instead got \`${typeof text}\`.`);
      this._contentWrapper.nativeElement.textContent = text;
    } catch (err) {
      console.warn(err);
    }
  }

  @Input()
  get color (): string {
    return this._color;
  }

  set color (value: string) {
    this._updateColor(value);
  }

  @HostListener('mousedown')
  _setMousedown () {
    /**
     * We only *show* the focus style when focus has come to the button via the keyboard.
     * Without doing this, the button continues to look :active after clicking.
     * @see https://marcysutton.com/button-focus-hell/
     */
    this._isMouseDown = true;
    setTimeout(() => {
      this._isMouseDown = false;
    }, 100);
  }

  _updateColor (newColor: string) {
    this._setElementColor(this._color, false);
    this._setElementColor(newColor, true);
    this._color = newColor;
  }



  _setElementColor (color: string, isAdd: boolean) {
    if (typeof color === 'string' && color.trim().length > 0) {
      const el = this._elementRef.nativeElement;
      const attr = 'sw-theme';

      if (isAdd) {
        this._renderer.setAttribute(el, attr, color);
      } else {
        this._renderer.removeAttribute(el, attr);
      }
    }
  }


  @HostListener('focus')
  _setKeyboardFocus () {
    this._isKeyboardFocused = !this._isMouseDown;
  }

  @HostListener('blur')
  _removeKeyboardFocus () {
    this._isKeyboardFocused = false;
  }

  @HostBinding('tabIndex')
  get attrTabIndex (): number {
    return this.disabled ? -1 : this.tabIndex;
  }

  @Input() tabIndex: number = 0;

  @HostBinding('attr.aria-disabled')
  get isAriaDisabled (): string {
    return this.disabled ? 'true' : 'false';
  }

  @HostBinding('attr.disabled')
  @Input('disabled')
  get disabled () {
    return this._disabled || null;
  }

  set disabled (value: boolean) {
    /** The presence of *any* disabled value makes the component disabled, *except* for false. */
    this._disabled = coerceBooleanProperty(value);
  }

  @HostBinding('attr.target')
  @Input('target')
  get target () {
    return this._target;
  }

  set target (value: string) {
    this._target = typeof value === 'string' ? value : '';
  }

  @HostListener('click', ['$event'])
  _haltDisabledEvents (event: Event) {
    /** A disabled button shouldn't apply any actions */
    if (this.disabled) {
      event.preventDefault();
      event.stopImmediatePropagation();
    }
  }

  @HostBinding('attr.sw-wrap')
  @Input()
  get wrap () {
    return this._wrap;
  }

  set wrap (value: boolean) {
    this._wrap = coerceBooleanProperty(value);
  }

  /** TODO: e2e test this function. */
  focus () {
    this._elementRef.nativeElement.focus();
  }

  isRoundButton () {
    const el = this._elementRef.nativeElement;
    return el.hasAttribute('sw-icon-button') || el.hasAttribute('sw-fab') || el.hasAttribute('sw-mini-fab');
  }
}

export { SwAbstractButton };

@NgModule({
  imports: [CommonModule, PlatformModule],
  exports: [SwButtonComponent],
  declarations: [SwButtonComponent],
  entryComponents: [SwButtonComponent]
})
export class SwButtonModule {}
