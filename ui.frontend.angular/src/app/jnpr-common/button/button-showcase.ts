import {
  Component,
  NgModule,
  ViewContainerRef
} from '@angular/core';
import { SwDialog, SwDialogConfig, SwDialogModule, SwDialogRef } from '../dialog/index';
import { SwIconModule } from '../icon/index';
import { SwMarqueeComponent, SwMarqueeModule } from '../marquee/marquee.component';
import { SwButtonModule } from './button.component';
import { SwBaseComponent } from '../core/base';

@Component({
  selector: 'sw-button-showcase',
  templateUrl: './button-showcase.html',
  styles: [`
    .demo-button button, .demo-button a {
      margin: 8px;
    }
    .demo-button section {
      display: flex;
      flex-direction: row;
      flex-wrap: wrap;
      align-items: center;
      margin: 8px;
      background-color: #fafafa;
    }
    .demo-button p {
      padding:5px 15px;
    }
  `]
})
export class SwButtonShowcaseComponent extends SwBaseComponent {

  isDisabled: boolean = false;
  clickCounter: number = 0;

  dialogRef: SwDialogRef<SwMarqueeComponent>;
  lastCloseResult: string;

  constructor (
      public dialog: SwDialog,
      public viewContainerRef: ViewContainerRef
    ) {
      super();
    }

  open () {
    const config = new SwDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    this.dialogRef = this.dialog.open(SwMarqueeComponent, config);
    this.dialogRef.componentInstance.properties = {
      'variant': 'primary',
      'headline': 'Products &amp Services',
      'backgroundImage': 'http://jnpr-assets.swirl-staging.net/jnpr2/sample-pages/generic/15337878384_826dadedf3_k.jpg'
    };

    this.dialogRef.afterClosed().subscribe(result => {
      this.lastCloseResult = result;
      this.dialogRef = null;
    });
  }
}

@Component({
  selector: 'sw-demo-jazz-dialog',
  template: `
  <p>It's Jazz!</p>
  <p><label>How much? <input #howMuch></label></p>
  <button type="button" (click)="dialogRef.close(howMuch.value)">Close dialog</button>`
})
export class JazzDialogAbcComponent {
  public dialogRef: SwDialogRef<JazzDialogAbcComponent>;
  constructor (dialogRef: SwDialogRef<JazzDialogAbcComponent>) { this.dialogRef = dialogRef; }
}

@NgModule({
  imports: [
    SwButtonModule,
    SwDialogModule,
    SwIconModule,
    SwMarqueeModule
  ],
  exports: [ SwButtonShowcaseComponent],
  declarations: [ SwButtonShowcaseComponent, JazzDialogAbcComponent ],
  entryComponents: [ SwButtonShowcaseComponent, JazzDialogAbcComponent ]
})
export class SwButtonShowcaseModule { }
