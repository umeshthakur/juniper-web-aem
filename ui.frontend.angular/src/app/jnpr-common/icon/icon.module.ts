import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { SwIconComponent } from './icon.component';
import { SW_ICON_REGISTRY_PROVIDER } from './icon-reg.service';

@NgModule({
  imports: [HttpClientModule],
  exports: [SwIconComponent],
  declarations: [SwIconComponent],
  providers: [SW_ICON_REGISTRY_PROVIDER]
})
export class SwIconModule {}
