import { Component } from '@angular/core';
import { async, fakeAsync, inject, TestBed, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SwIconRegistryService } from './icon-reg.service';
import { SwIconModule } from './icon.module';

/**
 * Fake URLs and associated SVG documents used by tests.
 */
const FAKE_SVGS = (() => {
  const svgs = new Map<string, string>();
  svgs.set('cat.svg', '<svg><path id="meow" name="meow"></path></svg>');
  svgs.set('dog.svg', '<svg><path id="woof" name="woof"></path></svg>');
  return svgs;
})();

/** Returns the CSS classes assigned to an element as a sorted array. */
const sortedClassNames = (elem: Element) => elem.className.split(' ').sort();

/**
 * Verifies that an element contains a single <svg> child element, and returns that child.
 */
const verifyAndGetSingleSvgChild = (element: SVGElement): SVGElement => {
  expect(element.id).toBeFalsy();
  expect(element.childNodes.length).toBe(1);
  const svgChild = element.childNodes[0] as SVGElement;
  expect(svgChild.tagName.toLowerCase()).toBe('svg');
  return svgChild;
};

/**
 * Verifies that an element contains a single <path> child element whose "id" attribute has
 * the specified value.
 */
const verifyPathChildElement = (element: Element, attributeValue: string): void => {
  expect(element.childNodes.length).toBe(1);
  const pathElement = element.childNodes[0] as SVGPathElement;
  expect(pathElement.tagName.toLowerCase()).toBe('path');

  // The testing data SVGs have the name attribute set for verification.
  expect(pathElement.getAttribute('name')).toBe(attributeValue);
};

/** Test components that contain an SwIconComponent. */
@Component({
  selector: 'sw-test-app',
  template: `<sw-icon>{{iconName}}</sw-icon>`
})
class SwIconLigatureTestAppComponent {
  ariaLabel: string = null;
  iconName = '';
}

@Component({
  selector: 'sw-test-app',
  template: `<sw-icon [color]="iconColor">{{iconName}}</sw-icon>`
})
class SwIconColorTestAppComponent {
  ariaLabel: string = null;
  iconName = '';
  iconColor = 'primary';
}

@Component({
  selector: 'sw-test-app',
  template: `<sw-icon [aria-label]="ariaLabel" [alt]="altText">{{iconName}}</sw-icon>`
})
class SwIconLigatureWithAriaBindingTestAppComponent {
  ariaLabel: string = null;
  iconName = '';
  altText;
}

@Component({
  selector: 'sw-test-app',
  template: `
      <sw-icon [fontSet]="fontSet" [aria-label]="ariaLabel"></sw-icon>
  `
})
class SwIconCustomFontCssTestAppComponent {
  ariaLabel: string = null;
  fontSet = '';
}

@Component({
  selector: 'sw-test-app',
  template: `<sw-icon [svgSrc]="iconUrl" [aria-label]="ariaLabel"></sw-icon>`
})
class SwIconFromSvgUrlTestAppComponent {
  ariaLabel: string = null;
  iconUrl = '';
}

describe('SwIconComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, SwIconModule],
      declarations: [
        SwIconColorTestAppComponent,
        SwIconLigatureTestAppComponent,
        SwIconLigatureWithAriaBindingTestAppComponent,
        SwIconCustomFontCssTestAppComponent,
        SwIconFromSvgUrlTestAppComponent
      ]
    }).compileComponents();
  }));

  let iconRegistry: SwIconRegistryService;
  let httpMock: HttpTestingController;

  beforeEach(
    inject(
      [SwIconRegistryService, HttpTestingController],
      (registry: SwIconRegistryService, http: HttpTestingController) => {
        iconRegistry = registry;
        httpMock = http;
      }
    )
  );

  afterEach(() => {
    httpMock.verify();
  });

  it('should apply class based on color attribute', () => {
    const fixture = TestBed.createComponent(SwIconColorTestAppComponent);

    const testComponent = fixture.debugElement.componentInstance;
    const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
    testComponent.iconName = 'home';
    testComponent.iconColor = 'primary';
    fixture.detectChanges();
    expect(sortedClassNames(iconElement)).toEqual(['sw-primary', 'swmaterialicon']);
  });

  describe('Ligature icons', () => {
    it('should add swmaterialicon class by default', () => {
      const fixture = TestBed.createComponent(SwIconLigatureTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.iconName = 'home';
      fixture.detectChanges();
      expect(sortedClassNames(iconElement)).toEqual(['swmaterialicon']);
    });
  });

  describe('Icons from URLs', () => {
    it('should fetch SVG icon from URL and inline the content', fakeAsync(() => {
      const fixture = TestBed.createComponent(SwIconFromSvgUrlTestAppComponent);
      const testComponent: SwIconFromSvgUrlTestAppComponent = fixture.componentInstance;
      const iconElement: SVGElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      let svgElement: any;

      testComponent.iconUrl = 'cat.svg';
      fixture.detectChanges();
      httpMock.expectOne('cat.svg').flush(FAKE_SVGS.get('cat.svg'));
      /** An <svg> element should have been added as a child of <sw-icon>. */
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      /** Default attributes should be set. */
      expect(svgElement.getAttribute('height')).toBe('100%');
      /** Make sure SVG content is taken from response. */
      verifyPathChildElement(svgElement, 'meow');

      /** Change the icon, and the SVG element should be replaced. */
      testComponent.iconUrl = 'dog.svg';
      fixture.detectChanges();
      httpMock.expectOne('dog.svg').flush(FAKE_SVGS.get('dog.svg'));
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      verifyPathChildElement(svgElement, 'woof');

      /** Using an icon from a previously loaded URL should not cause another HTTP request. */
      testComponent.iconUrl = 'cat.svg';
      fixture.detectChanges();
      httpMock.expectNone('cat.svg');
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      verifyPathChildElement(svgElement, 'meow');

      // Assert that a registered icon can be looked-up by url.
      iconRegistry.getSvgIconFromUrl('dog.svg').subscribe(element => {
        verifyPathChildElement(element, 'woof');
      });

      tick();
    }));

    it('should return unmodified copies of icons from URLs', () => {
      const fixture = TestBed.createComponent(SwIconFromSvgUrlTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      let svgElement: any;

      testComponent.iconUrl = 'cat.svg';
      fixture.detectChanges();
      httpMock.expectOne('cat.svg').flush(FAKE_SVGS.get('cat.svg'));
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      verifyPathChildElement(svgElement, 'meow');
      /** Modify the SVG element by setting a viewBox attribute. */
      svgElement.setAttribute('viewBox', '0 0 100 100');

      /** Switch to a different icon. */
      testComponent.iconUrl = 'dog.svg';
      fixture.detectChanges();
      httpMock.expectOne('dog.svg').flush(FAKE_SVGS.get('dog.svg'));
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      verifyPathChildElement(svgElement, 'woof');

      /** Switch back to the first icon. The viewBox attribute should not be present. */
      testComponent.iconUrl = 'cat.svg';
      fixture.detectChanges();
      httpMock.expectNone('cat.svg');
      svgElement = verifyAndGetSingleSvgChild(iconElement);
      verifyPathChildElement(svgElement, 'meow');
      expect(svgElement.getAttribute('viewBox')).toBeFalsy();
    });
  });

  describe('custom fonts', () => {
    it('should apply CSS classes for custom font', () => {
      iconRegistry.registerFontClassAlias('f1', 'font1');
      iconRegistry.registerFontClassAlias('f2');

      const fixture = TestBed.createComponent(SwIconCustomFontCssTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.fontSet = 'f1';
      fixture.detectChanges();
      expect(sortedClassNames(iconElement)).toEqual(['font1']);

      testComponent.fontSet = 'f2';
      fixture.detectChanges();
      expect(sortedClassNames(iconElement)).toEqual(['f2']);

      testComponent.fontSet = 'f3';
      fixture.detectChanges();
      expect(sortedClassNames(iconElement)).toEqual(['f3']);
    });
  });

  describe('aria label', () => {
    it('should set aria label from text content if not specified', () => {
      const fixture = TestBed.createComponent(SwIconLigatureTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.iconName = 'home';

      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('home');

      testComponent.iconName = 'hand';
      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('hand');
    });

    it('should not set aria label unless it actually changed', () => {
      const fixture = TestBed.createComponent(SwIconLigatureTestAppComponent);

      const testComponent = fixture.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.iconName = 'home';

      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('home');

      iconElement.removeAttribute('aria-label');
      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBeFalsy();
    });

    it('should use alt tag if aria label is not specified', () => {
      const fixture = TestBed.createComponent(SwIconLigatureWithAriaBindingTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.iconName = 'home';
      testComponent.altText = 'castle';
      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('castle');

      testComponent.ariaLabel = 'house';
      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('house');
    });

    it('should use provided aria label rather than icon name', () => {
      const fixture = TestBed.createComponent(SwIconLigatureWithAriaBindingTestAppComponent);

      const testComponent = fixture.debugElement.componentInstance;
      const iconElement = fixture.debugElement.nativeElement.querySelector('sw-icon');
      testComponent.iconName = 'home';
      testComponent.ariaLabel = 'house';
      fixture.detectChanges();
      expect(iconElement.getAttribute('aria-label')).toBe('house');
    });

  });
});
