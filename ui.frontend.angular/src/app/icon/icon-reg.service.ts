import { Inject, Injectable, Optional, SkipSelf } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { finalize, map, share, tap } from 'rxjs/operators';

import { SwIconSvgTagNotFoundError } from './icon-error';

class RemoteSvgIcon {
  svgElement: SVGElement = null;
  constructor (public url: string) { }
}

/** Service to register and display SVG icons. */
@Injectable()
export class SwIconRegistryService {

  /** Cache for icons loaded by direct URLs. */
  private _cachedIconsByUrl = new Map<string, SVGElement>();

  /**
   * In-progress icon fetches. Used to coalesce multiple requests
   * to the same URL.
   */
  private _inProgressUrlFetches = new Map<string, Observable<string>>();

  /** Map from font identifiers to their CSS class names. Used for icon fonts. */
  private _fontCssClassesByAlias = new Map<string, string>();

  /** Default font set is a customized version of Material Design with Font Awesome social icons */
  private _defaultFontSetClass = 'swmaterialicon';

  constructor (private _http: HttpClient, @Inject(DOCUMENT) private _document: Document) { }

  /**
   * Returns the CSS class name associated with the alias by a
   * previous call to registerFontClassAlias. If no CSS class
   * has been associated, returns the alias unmodified.
   */
  classNameForFontAlias (alias: string): string {
    return this._fontCssClassesByAlias.get(alias) || alias;
  }

  /**
   * Returns the CSS class name to be used for icon fonts when an
   * `<sw-icon>` component does not have a `fontSet` input value,
   * and is not loading an icon by name or URL.
   */
  getDefaultFontSetClass (): string {
    return this._defaultFontSetClass;
  }

  /**
   * Returns an Observable that produces the icon (as an <svg> DOM
   * element) from the given URL. The response from the URL may be
   * cached so this will not always cause an HTTP request, but the
   * produced element will always be a new copy of the originally
   * fetched icon. (That is, it will not contain any modifications
   * made to elements previously returned).
   */
  public getSvgIconFromUrl (url: string): Observable<SVGElement> {
    /** Check if the icon URL has already been requested. */
    if (this._cachedIconsByUrl.has(url)) {
      return of(cloneSvg(this._cachedIconsByUrl.get(url)));
    }
    /** Otherwise */
    return this._loadRemoteSvgIcon(new RemoteSvgIcon(url)).pipe(
      tap(svg => this._cachedIconsByUrl.set(url, svg)),
      map(svg => cloneSvg(svg))
    );
  }

  /**
   * Defines an alias for a CSS class name to be used for icon fonts.
   * Creating an `<sw-icon>` component with the alias as the `fontSet`
   * input will cause the class name to be applied to the <sw-icon> element.
   */
  registerFontClassAlias (alias: string, className = alias): this {
    this._fontCssClassesByAlias.set(alias, className);
    return this;
  }

  /** Creates a DOM element from the given SVG string, and adds default attributes. */
  private _createSvgElementForSingleIcon (responseText: string): SVGElement {
    const svg = this._svgElementFromString(responseText);
    this._setSvgAttributes(svg);
    return svg;
  }

  /**
   * Returns an Observable which produces the string contents of the
   * given URL. Results may be cached, so future calls with the same
   * URL may not cause another HTTP request.
   */
  private _fetchUrl (url: string): Observable<string> {
    /**
     * Avoid sending duplicate requests in the case where there's
     * already a fetch in progress for the requested URL.
     */
    if (this._inProgressUrlFetches.has(url)) {
      return this._inProgressUrlFetches.get(url);
    }

    /**
     * It's necessary to call share() on the Observable returned
     * by http.get() so that multiple subscribers don't cause
     * multiple XHRs.
     */
    const req = this._http.get(url, { responseType: 'text' }).pipe(
      finalize(() => this._inProgressUrlFetches.delete(url)),
      share()
    );

    this._inProgressUrlFetches.set(url, req);
    return req;
  }

  /**
   * Loads the content of the icon URL specified in the RemoteSvgIcon
   * and creates an SVG element from it.
   */
  private _loadRemoteSvgIcon (icon: RemoteSvgIcon): Observable<SVGElement> {
    return this._fetchUrl(icon.url).pipe(
      map(svgText => this._createSvgElementForSingleIcon(svgText))
    );
  }

  /** Sets the default attributes for an SVG element to be used as an icon */
  private _setSvgAttributes (svg: SVGElement): SVGElement {
    if (!svg.getAttribute('xmlns')) {
      svg.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    }
    svg.setAttribute('fit', '');
    svg.setAttribute('height', '100%');
    svg.setAttribute('width', '100%');
    svg.setAttribute('preserveAspectRatio', 'xMidYMid meet');
    svg.setAttribute('focusable', 'false'); // Disable IE11 default behavior to make SVGs focusable.
    return svg;
  }

  /** Creates a DOM element from the given SVG string */
  private _svgElementFromString (str: string): SVGElement {
    const div = this._document.createElement('DIV');
    div.innerHTML = str;
    const svg = div.querySelector('svg') as SVGElement;
    if (!svg) {
      throw new SwIconSvgTagNotFoundError();
    }
    return svg;
  }

}

export function SW_ICON_REGISTRY_PROVIDER_FACTORY (
  parentRegistry: SwIconRegistryService,
  http: HttpClient,
  document?: Document) {
  return parentRegistry || new SwIconRegistryService(http, document);
}

export const SW_ICON_REGISTRY_PROVIDER = {
  provide: SwIconRegistryService,
  deps: [
    [new Optional(), new SkipSelf(), SwIconRegistryService],
    [new Optional(), HttpClient],
    [new Optional(), DOCUMENT]
  ],
  useFactory: SW_ICON_REGISTRY_PROVIDER_FACTORY
};

/** Clones an SVGElement while preserving type information. */
function cloneSvg (svg: SVGElement): SVGElement {
  return svg.cloneNode(true) as SVGElement;
}
