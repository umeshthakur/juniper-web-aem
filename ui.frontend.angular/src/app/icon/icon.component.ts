import {
  AfterViewChecked,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges
} from '@angular/core';
import { first } from 'rxjs/operators';

import { SwIconRegistryService } from './icon-reg.service';

const iconSizes = {
  xs: 'xs',
  sm: 'sm',
  md: 'md',
  lg: 'lg',
  xl: 'xl',
  default: 'sm'
};

/**
 * Component to display an icon. It can be used in the following ways:
 * - Setting the `svgSrc` input  will load an SVG icon from a URL. SVG
 *   content will be directly inlined as a child of the component, making
 *   it possible to easily apply CSS styles can easily be applied to it.
 *   The URL is loaded via an XMLHttpRequest, so it must be on the same
 *   domain as the page or its server must be configured to allow
 *   cross-domain requests.
 *   NOTE: The SVG icon *must have* `viewBox` in it's attributes.
 *   Example:
 *     `<sw-icon svgSrc="assets/arrow.svg"></sw-icon>`
 *
 * - Font ligatures as an icons can be used by placing the ligature text
 *   as the contents of this component. An alternate font can be specified
 *   by setting the `fontSet` input to either the CSS class to apply to use
 *   the desired font, or to an alias previously registered with
 *   SwIconRegistryService.registerFontClassAlias.
 *   Examples:
 *     <sw-icon>home</sw-icon>
 *     <sw-icon fontSet="myfont">sun</sw-icon>
 */
@Component({
  selector: 'sw-icon',
  exportAs: 'swIcon',
  template: '<ng-content></ng-content>',
  styleUrls: ['./icon.component.scss'],
  // tslint:disable-next-line:use-host-property-decorator
  host: {
    role: 'img'
  },
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SwIconComponent implements OnInit, OnChanges, AfterViewChecked {
  @Input() svgSrc: string = '';
  @Input() alt: string;
  @Input() size: string;
  // tslint:disable-next-line:no-input-rename
  @Input('aria-label') hostAriaLabel: string = '';

  /** Font set that the icon is a part of. */
  @Input()
  get fontSet (): string {
    return this._fontSet;
  }
  set fontSet (value: string) {
    this._fontSet = this._cleanupFontValue(value);
  }
  private _fontSet: string;

  /** Color of the icon */
  @Input()
  get color (): string {
    return this._color;
  }
  set color (value: string) {
    if (value !== this._color) {
      if (this._color) {
        this._elementRef.nativeElement.classList.remove(`sw-${this._color}`);
      }
      if (value) {
        this._elementRef.nativeElement.classList.add(`sw-${value}`);
      }
      this._color = value;
    }
  }
  private _color: string;

  private _previousFontSetClass: string;
  private _previousAriaLabel: string;

  constructor (private _elementRef: ElementRef, private _iconRegistry: SwIconRegistryService) {}

  ngOnChanges (changes: SimpleChanges) {
    /** Only update the inline SVG icon if the inputs changed, to avoid unnecessary DOM operations. */
    if (changes.svgSrc) {
      if (this.svgSrc) {
        this._iconRegistry
          .getSvgIconFromUrl(this.svgSrc)
          .pipe(first())
          .subscribe(
            svg => this._setSvgElement(svg),
            (err: Error) => console.log(`Error retrieving icon: ${err.message}`)
          );
      } else {
        this._clearSvgElement();
      }
    }

    if (this._usingFontIcon()) {
      this._updateFontIconClasses();
    }

    this._updateAriaLabel();

    if (changes.size) {
      this._updateIconSize();
    }
  }

  ngOnInit () {
    /**
     * Update font classes because ngOnChanges won't be called if
     * none of the inputs are present, e.g. <sw-icon>arrow</sw-icon>.
     * In this case we need to add a CSS class for the default font.
     */
    if (this._usingFontIcon()) {
      this._updateFontIconClasses();
    }
  }

  ngAfterViewChecked () {
    /**
     * Update aria label here because it may depend on the projected text content.
     * (e.g. <sw-icon>home</sw-icon> should use 'home').
     */
    this._updateAriaLabel();
  }

  private _updateAriaLabel () {
    const ariaLabel = this._getAriaLabel();
    /** Only set `aria-label` if different */
    if (ariaLabel && ariaLabel !== this._previousAriaLabel) {
      this._previousAriaLabel = ariaLabel;
      (this._elementRef.nativeElement as HTMLElement).setAttribute('aria-label', ariaLabel);
    }
  }

  private _getAriaLabel () {
    /**
     * If the parent provided an aria-label attribute value, use it as-is. Otherwise look for a
     * reasonable value from the alt attribute, font icon name, SVG icon name, or (for ligatures)
     * the text content of the directive.
     */
    const label = this.hostAriaLabel || this.alt;
    if (label) {
      return label;
    }
    /** The "content" of an SVG icon is not a useful label. */
    if (this._usingFontIcon()) {
      const text = this._elementRef.nativeElement.textContent;
      if (text) {
        return text;
      }
    }
    // TODO: Warn here in dev mode.
    return null;
  }

  private _usingFontIcon (): boolean {
    return !this.svgSrc;
  }

  private _setSvgElement (svg: SVGElement) {
    this._clearSvgElement();
    this._elementRef.nativeElement.appendChild(svg);
  }

  private _clearSvgElement () {
    const layoutElement: HTMLElement = this._elementRef.nativeElement;
    const childCount = layoutElement.childNodes.length;

    /**
     * Remove existing child nodes and add the new SVG element. Note that we can't
     * use innerHTML, because IE will throw if the element has a data binding.
     */
    for (let i = 0; i < childCount; i++) {
      layoutElement.removeChild(layoutElement.childNodes[i]);
    }
  }

  private _updateFontIconClasses () {
    if (!this._usingFontIcon()) {
      return;
    }

    const elem: HTMLElement = this._elementRef.nativeElement;
    const fontSetClass = this.fontSet
      ? this._iconRegistry.classNameForFontAlias(this.fontSet)
      : this._iconRegistry.getDefaultFontSetClass();

    if (fontSetClass !== this._previousFontSetClass) {
      if (this._previousFontSetClass) {
        elem.classList.remove(this._previousFontSetClass);
      }
      if (fontSetClass) {
        elem.classList.add(fontSetClass);
      }
      this._previousFontSetClass = fontSetClass;
    }
  }

  private _updateIconSize () {
    const size = iconSizes.hasOwnProperty(this.size) ? this.size : iconSizes.default;
    const elem: HTMLElement = this._elementRef.nativeElement;
    elem.setAttribute('sw-icon-size', size);
  }

  /**
   * Cleans up a value to be used as a fontIcon or fontSet.
   * Since the value ends up being assigned as a CSS class, we
   * have to trim the value and omit space-separated values.
   */
  private _cleanupFontValue (value: string) {
    return typeof value === 'string' ? value.trim().split(' ')[0] : value;
  }
}
