import { SwError } from '../core/error';

/**
 * Exception thrown when attempting to load SVG content that does not
 * contain the expected <svg> tag.
 */
export class SwIconSvgTagNotFoundError extends SwError {
  constructor () {
    super('<svg> tag not found');
    this.name = SwIconSvgTagNotFoundError.name;
  }
}
