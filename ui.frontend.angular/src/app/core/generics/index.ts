/**
 * Generic interface for a Class
 */
export type Constructor<T> = new(...args: any[]) => T;

/**
 * Generic interface for a Component
 */
export interface ComponentType<T> {
  new (...args: any[]): T;
}

export interface ComponentHostType<T> { [key: string]: keyof T; }
