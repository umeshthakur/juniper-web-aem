import { animate, AnimationTriggerMetadata, AUTO_STYLE, style, transition, trigger } from '@angular/animations';

export const ANIM_EASING_IN_OUT_CURVE = 'cubic-bezier(0.35, 0, 0.25, 1)';
export const ANIM_EASING_OUT_CURVE = 'cubic-bezier(0.25, 0.8, 0.25, 1)';
export const ANIM_EASING_IN_CURVE = 'cubic-bezier(0.55, 0, 0.55, 0.2)';

export function slideAnimation (name: string, leave: string = '250ms', enter: string = '250ms'): AnimationTriggerMetadata {
  return trigger(name, [
    transition(':enter', [
      style({ height: 0, opacity: 0 }),
      animate(enter + ' ' + ANIM_EASING_IN_OUT_CURVE, style({ height: AUTO_STYLE, opacity: 1 }))
    ]),
    transition(':leave', [
      style({ height: AUTO_STYLE }),
      animate(leave + ' ' + ANIM_EASING_IN_OUT_CURVE, style({ height: 0, opacity: 0 }))
    ])
  ]);
}
