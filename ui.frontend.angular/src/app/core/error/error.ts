/**
 * Wrapper around Error that sets the error message.
 */
export class SwError extends Error {
  constructor (value: string) {
    super(value);
  }
}
