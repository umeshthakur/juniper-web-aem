/**
 * Error used mostly to throw when using an abstract class
 * that is meant to be implemented and replaced through DI
 */
export function unimplemented (): any {
  throw new Error('unimplemented');
}
