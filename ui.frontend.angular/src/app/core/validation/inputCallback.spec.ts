import { SwInputCallback } from './inputCallback.decorator';

function setterTargetFn () {
  throw new Error('In setterTargetFn!');
}

const InitPropDecorator: PropertyDecorator = function (target: any, key: string | symbol) {
  'use strict';
  try {
    if (delete target[key]) {
      let _val;
      Object.defineProperty(target, key, {
        get: function () { return _val; },
        set: function (val) { _val = val; },
        configurable: false,
        enumerable: true
      });
    }
  } catch (err) {
    console.error(err);
  }
};

const InitPropGetterDecorator: PropertyDecorator = function (target: any, key: string | symbol) {
  'use strict';
  try {
    if (delete target[key]) {
      Object.defineProperty(target, key, {
        get: function () { return 29; },
        configurable: false,
        enumerable: true
      });
    }
  } catch (err) {
    console.error(err);
  }
};

class InputCallbackTestClass {

  @SwInputCallback(setterTargetFn)
  setterSrcFn: any;

  @SwInputCallback('setterTarget')
  @InitPropDecorator
  setterSrc: any;

  @SwInputCallback('setterMissTarget')
  @InitPropGetterDecorator
  setterMissSrc: any;

  @SwInputCallback('methodTarget')
  methodSrc (): any {} // tslint:disable-line no-empty

  setterTarget (val: any) {
    this._setterTargetValue = val;
    this._setterTargetCalled += 1;
  }
  _setterTargetValue: any = 'setter';
  _setterTargetCalled: any = 0;

  methodTarget (val: any) {
    this._methodTargetValue = val;
    this._methodTargetCalled += 1;
  }
  _methodTargetValue: any = 'method';
  _methodTargetCalled: any = 0;
}

describe(`SwInputCallback`, () => {

  describe('in isolation', () => {
    let decoratorInstance;

    beforeEach(() => {
      decoratorInstance = SwInputCallback('');
    });

    it('should return a function', () => {
      expect(typeof decoratorInstance).toBe('function');
    });

  });

  describe('in a class context', () => {
    it('should throw', () => {
      expect(() => {
        @SwInputCallback('fromClassDecorator')
        class ClassDecoratorClass {}
        // The following is to avoid compiler error for unused locals
        // tslint:disable-next-line no-unused-expression
        new ClassDecoratorClass();
      }).toThrow();
    });
  });

  describe('in a param context', () => {
    it('should throw', () => {
      expect(() => {
        class ParamDecoratorClass {
          subject (@SwInputCallback('fromParamDecorator') hi) {}  // tslint:disable-line no-empty
        }
        // The following is to avoid compiler error for unused locals
        // tslint:disable-next-line no-unused-expression
        new ParamDecoratorClass();
      }).toThrow();
    });
  });

  describe('in a property context', () => {
    let classInstance;

    beforeEach(() => {
      classInstance = new InputCallbackTestClass();
    });

    it('should not throw', () => {
      expect(() => {
        class PropDecoratorClass {
          @SwInputCallback('fromPropDecorator')
          subject;
        }
        // The following is to avoid compiler error for unused locals
        // tslint:disable-next-line no-unused-expression
        new PropDecoratorClass();
      }).not.toThrow();
    });

    it('should not interfere the expected setter functionality', () => {
      const valueToSet = 'Hello setterSrc';
      classInstance.setterSrc = valueToSet;
      expect(classInstance.setterSrc).toEqual(valueToSet);
    });

    it('should execute a callback in the same class', () => {
      const valueToSet = 'Hello setterSrc';
      const exclamations = '!!!';
      classInstance.setterSrc = exclamations;
      expect(classInstance.setterSrc).toEqual(exclamations);
      expect(classInstance._setterTargetCalled).toEqual(1);
      expect(classInstance._setterTargetValue).toEqual(exclamations);

      classInstance.setterSrc = valueToSet + exclamations;
      expect(classInstance._setterTargetCalled).toEqual(2);
      expect(classInstance.setterSrc).toEqual(valueToSet + exclamations);
      expect(classInstance._setterTargetValue).toEqual(valueToSet + exclamations);
    });

    it('should execute a callback in a function', () => {
      expect(() => classInstance.setterSrcFn = 'Uh-oh').toThrowError('In setterTargetFn!');
      expect(classInstance.setterSrcFn).toBe('Uh-oh');
    });

    it('should handle when the passed callback is invalid', () => {
      expect(() => classInstance.setterMissSrc = 'Who are you?').not.toThrow();
      /** The property is a getter-only that exclusively returns `29` */
      expect(classInstance.setterMissSrc).toBe(29);
    });

  });

  describe('in a method context', () => {
    beforeEach(() => {
      // tslint:disable-next-line no-unused-expression
      new InputCallbackTestClass();
    });

    it('should not throw', () => {
      expect(() => {
        class MethodDecoratorClass {
          @SwInputCallback('fromMethodDecorator')
          subject () {}  // tslint:disable-line no-empty
        }
        // The following is to avoid compiler error for unused locals
        // tslint:disable-next-line no-unused-expression
        new MethodDecoratorClass();
      }).not.toThrow();
    });
  });

});
