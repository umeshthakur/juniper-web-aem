import { DataType, is } from 'is-datatype';
import {
  isOptions,
  isOptionsArray,
  isOptionsNumber,
  isOptionsObject,
  isOptionsString,
  isTypeSchema
} from 'is-datatype';
import { SwBaseComponentProperties } from '../base';
import { cloneObject } from '../../util/cloneObject.util';

/** Token to provide property access across all SwInput decorators */
const PropsToken = Symbol('PropsToken');
/** Token to indicate init status across all the SwInput decorators */
const PropsSetToken = Symbol('PropsSetToken');

function _getDebuggingName (instance: any) {
  return instance.constructor.name || '';
}

/**
 * Decorator that provides a method to easily set
 * all properties available through the `PropsToken`.
 * No extraneous properties will be able to be
 * accessed there.
 */
export function SwInputBaseProps (): PropertyDecorator {
  return function (target: any, key: string | symbol) {
    'use strict';

    try {
      if (delete target[key]) {
        Object.defineProperties(target, {
          /** Properties map */
          [key]: {
            get: function () {
              return this[PropsToken] || (this[PropsToken] = new SwBaseComponentProperties({}));
            },
            set: function (props: SwBaseComponentProperties) {
              const propsSet: Set<string> = this[PropsSetToken];
              if ( typeof props === 'string'){
                props = JSON.parse(props);
              }
              for (const key in props) {
                if (propsSet && propsSet.has(key)) {
                  this[key] = props[key];
                } else {
                  console.warn(`Attempted to set invalid property \`${key && key.toString()}\` on \`${_getDebuggingName(this)}\``);
                }
              }
            }
          }
        });
      }
    } catch (err) {
      /**
       * The delete operator throws in strict mode if
       * the property is an own non-configurable property
       */
      console.warn(`Cannot delete non-configurable \`${key && key.toString()}\``, err);
    }
  };
}

/**
 * Decorator used to register properties and provide getter/setter
 * access via the centralized `PropsToken`. Additionally,it provides
 * type and optionally custom validation of such properties when set.
 */
export function SwInputProp<T extends undefined | boolean> (type: DataType, defaultVal: undefined | boolean);
export function SwInputProp<T extends number> (type: DataType, defaultVal: number, options?: isOptionsNumber);
export function SwInputProp<T extends string> (type: DataType, defaultVal: string, options?: isOptionsString);
export function SwInputProp<U, T extends U[]> (type: DataType, defaultVal: U[], options?: isOptionsArray);
export function SwInputProp<T extends object> (type: DataType, defaultVal: object, options?: isOptionsObject);
export function SwInputProp<T> (type: DataType, defaultVal: T, options?: isOptions): PropertyDecorator {
  if (is(defaultVal, DataType.function)) throw new Error(`Functions are not allowed as default values.`);
  /** Check whether the default is a primitive or not */
  const defaultIsPrimitive = defaultVal == null || (typeof defaultVal !== 'function' && typeof defaultVal !== 'object');

  return function (target: any, key: string | symbol) {
    'use strict';

    try {
      if (delete target[key]) {
        Object.defineProperty(target, key, {
          /** Get prop from within the `_properties` object */
          get: function () {
            /** Initialize props token if not already declared */
            if (!this[PropsToken]) {
              this[PropsToken] = new SwBaseComponentProperties({});
            }
            /** If already initialized, return */
            if (this[PropsToken][key] !== undefined) {
              return this[PropsToken][key];
            }
            /** If not initialized, return default value */
            return (this[PropsToken][key] = defaultIsPrimitive ? defaultVal : cloneObject(defaultVal));
          },
          /** Set prop into the `_properties` object */
          set: function (val: T) {
            /** Initialize props token if not already declared */
            if (!this[PropsToken]) {
              this[PropsToken] = new SwBaseComponentProperties({});
            }

            /** Check whether the passed value is valid. If so, set ot; otherwise, set fallback */
            const valid = is(val, type, options);
            this[PropsToken][key] = valid ? val : defaultVal;

            /** If value is not valid, warn in console */
            if (!valid && val !== undefined) {
              console.warn(`Attempted to set invalid value for property \`${key && key.toString()}\` on \`${_getDebuggingName(this)}\``);
            }
          },
          enumerable: true
        });
        /** Add to the prop set */
        if (!target[PropsSetToken]) {
          target[PropsSetToken] = new Set();
        }
        target[PropsSetToken].add(key);
      }
    } catch (err) {
      /**
       * The delete operator throws in strict mode if
       * the property is an own non-configurable property
       */
      console.warn(`Cannot delete non-configurable \`${key && key.toString()}\``, err);
    }
  };
}

/**
 * Converts a generic Object of a single dimension into an `isTypeSchema`
 * object by iterating through the key-value pairs and determining which
 * DataType to use, mostly based on `typeof`.
 *
 * Optionally, the values can be set to just check for type or strictly
 * make them required—the default behavior.
 */
export function simpleObjToSchemaConverter (obj: any, required = true): isTypeSchema {
  const props = {};

  /**
   * Somewhat naive type extraction
   * 1. Iterates through first level of props.
   * 2. Determines type
   * 3. Saves it into the object in an `isTypeSchema` compat manner
   */
  Object.keys(obj).forEach(prop => {
    const val = obj[prop];
    const type: DataType =
      /** First, check if value is array because of `typeof` */
      Array.isArray(val)
        ? DataType.array
        : /** Check if val is something meaningful. If so, use `typeof` value; otherwise use `any` */
          (val === null || val === undefined) && (DataType as Object).hasOwnProperty(typeof val)
          ? DataType.any
          : DataType[typeof obj[prop]];
    props[prop] = { type: type, required: required };
  });

  return {
    type: DataType.object,
    props: props
  };
}

export { DataType, isTypeSchema };
