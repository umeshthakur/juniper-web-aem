import { DataType, is } from 'is-datatype';
import { DecoratorType, getDecoratorType } from '../../util/decoratorType.util';

/**
 * Determine callback.
 * If function, use that.
 * Otherwise, check for method in class and use it.
 */
function getCallback (scope, cb) {
  return (
    is(cb, DataType.function) ?
    cb as Function :
    is(scope[cb as string], DataType.function) ?
    scope[cb as string] :
    null
  );
}

function createCallbackMethod (callback: Function | string): MethodDecorator {
  return function<T> (target: any, key: string | symbol, descriptor: TypedPropertyDescriptor<T>) {
    console.warn(`Currently, \`SwInputCallback\` is not doing anything. To be implemented.`);
    return descriptor;
  };
}

function createCallbackProp (callback: Function | string): PropertyDecorator {
  return function (target: any, key: string | symbol): void {
    /**
     * Strict mode ensures an error is thrown below
     * if necessary, instead of evaluating to a falsy value;
     */
    'use strict';

    /** Check for existence of PropertyDescriptor */
    const descriptor = (
      key in target ?
      Object.getOwnPropertyDescriptor(target, key) :
      undefined
    );

    try {
      /** Property is defined in target */
      if (descriptor) {
        /** Check for the original method, if any */
        // tslint:disable-next-line no-empty
        const originalMethod = (descriptor.set || function () {});

        Object.defineProperty(target, key, {
          set: function (...args: any[]) {
            /** Determine callback */
            const cb: Function = getCallback(this, callback);
            /** Pass arguments to original method */
            originalMethod.apply(this, args);
            /** Call callback */
            if (cb) cb.call(this, this[key]);
          }
        });

      /** Property is NOT defined in target */
      } else {
        /** Delete original property from the class prototype */
        if (delete target[key]) {
          let _val;
          Object.defineProperty(target, key, {
            get: function () { return _val; },
            set: function (newVal) {
              /** Determine callback */
              const cb: Function = getCallback(this, callback);
              /** Persist */
              _val = newVal;
              /** Call callback */
              if (cb) cb.call(this, _val);
            },
            enumerable: true,
            configurable: true
          });
        }
      }
    } catch (err) {
      /**
       * The delete operator throws in strict mode if
       * the property is an own non-configurable property
       */
      console.warn(`Attempted to \`delete\` non-configurable property \`${key && key.toString()}\``, err);
    }
  };
}

/**
 * Decorator factory that ultimately handles
 * callbacks for property setters and method calls
 */
export function SwInputCallback (cb: Function | string) {
  return (...args: any[]) => {

    switch (getDecoratorType(...args)) {
      case DecoratorType.Property:
        return createCallbackProp(cb).apply(null, args);
      case DecoratorType.Method:
        return createCallbackMethod(cb).apply(null, args);
    }

    /** Otherwise, throw */
    throw new Error('Invalid decorator use for SwInputCallback');
  };
}
