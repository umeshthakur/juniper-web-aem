export { SwBasePage } from './page';
export {
  SwBaseComponent,
  SwBaseComponentObj,
  SwBaseComponentProperties,
  SwPeripheralDialogComponentObj
} from './component';
export { SwBasePageMeta, SwBasePageMetaItem } from './meta';
export { SwBasePageConfig } from './config';
export { defaultLocale, SwBaseLocale } from './locale';
