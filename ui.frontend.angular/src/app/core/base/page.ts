// import { LiveAgentConfig } from '../../jnpr-common/liveagent/index';
import { SwBasePageMeta } from './meta/index';
import { defaultPageConfig, SwBasePageConfig } from './config';
import { SwBaseComponentObj, SwPeripheralDialogComponentObj } from './component/index';
import { pagePropsHandler } from './shared/pagePropsHandler';
import { StrictTypeArray } from './shared/strictTypeArray';

export class SwBaseGlobalComponents {
  navigation = new SwBaseComponentObj({});
  partnernavigation = new SwBaseComponentObj({});
  partnerlogin = new SwBaseComponentObj({});
  footer = new SwBaseComponentObj({});
  backToTop = new SwBaseComponentObj({});
  // TODO: move live agent to a more appropriate class
  // liveagent = new LiveAgentConfig({});

  constructor (inputObj?: Partial<SwBaseGlobalComponents>) {
    pagePropsHandler(this as SwBaseGlobalComponents, inputObj);
  }
}

export const defaultGlobalComponents = new SwBaseGlobalComponents();

export interface ISwBasePage {
  layout?: string;
  meta?: SwBasePageMeta;
  config?: SwBasePageConfig;
  globalComponents?: SwBaseGlobalComponents;
  components?: SwBaseComponentObj[];
}

/**
 * Page object structure in the Json interface
 */
export class SwBasePage implements ISwBasePage {
  /** Keyword meant to provide an input that defines large-scale layout */
  layout: string = '';
  /** Property that describes page metadata. */
  meta: SwBasePageMeta = new SwBasePageMeta({});
  /** An object with defined, keyed properties that match to global page components. */
  config: SwBasePageConfig = defaultPageConfig;
  /** An object with defined, keyed properties that match to global page components. */
  globalComponents: SwBaseGlobalComponents = defaultGlobalComponents;
  /** An array of component definition objects. */
  components: SwBaseComponentObj[] = StrictTypeArray(SwBaseComponentObj);
  /** An array of component definition objects. */
  peripheralDialogs: SwPeripheralDialogComponentObj[] = StrictTypeArray(SwPeripheralDialogComponentObj);

  constructor (inputObj?: ISwBasePage) {
    pagePropsHandler(this as SwBasePage, inputObj);
  }
}
