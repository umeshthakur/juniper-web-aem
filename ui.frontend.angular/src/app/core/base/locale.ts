import { DataType, is } from 'is-datatype';

export interface LocaleObject {
  language: string;
  region: string;
}

export const defaultLocaleLanguage: string = 'en';
export const defaultLocaleRegion: string = 'us';

export class SwBaseLocale implements LocaleObject {
  public language: string = defaultLocaleLanguage;
  public region: string = defaultLocaleRegion;

  constructor (inputLocale?: LocaleObject) {
    if (is(inputLocale, DataType.object)) {
      this.language = (typeof inputLocale.language === 'string' ? inputLocale.language : defaultLocaleLanguage);
      this.region = (typeof inputLocale.region === 'string' ? inputLocale.region : defaultLocaleRegion);
    }
  }

  /**
   * Returns a locale formatted in compliance with the HTML standard
   * as detailed in ["Specifying the language of content: the lang attribute"](https://www.w3.org/TR/html401/struct/dirlang.html#h-8.1)
   * by the W3C which implements the [Language codes - ISO 639](https://www.iso.org/iso-639-language-codes.html) standard.
   *
   * Example: `en-US`
   */
  public formatAsISOLanguageCode (): string {
    return `${this.language}-${this.region.toUpperCase()}`;
  }
}

export const defaultLocale: SwBaseLocale = new SwBaseLocale();
