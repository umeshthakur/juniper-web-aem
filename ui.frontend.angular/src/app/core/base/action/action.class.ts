import { SwMarqueeComponent } from '../../../jnpr-common/marquee/marquee.component';

/**
 * Pattern class for generic actions
 */
export class SwBaseAction {

  /** The label to be displayed to describe the action */
  readonly label: string = '';

  /** The hyperlink reference that the action will refer to */
  readonly url: string = '';

  /** The target attribute specifies where to open the linked document */
  readonly target?: string = '';

  /** String attribute that gets evaluated on user click */
  readonly onclick?: string = '';

  constructor (action?: Partial<SwBaseAction>) {
    if (action && typeof action === 'object') {
      for (const k in action) {
        if ((this as object).hasOwnProperty(k) && typeof action[k] === 'string') {
          this[k] = action[k];
        }
      }
    }
  }
}

export class SwVideoAction extends SwBaseAction {
  readonly endCard?: SwMarqueeComponent;
}
