import { SwBaseAction } from './action.class';

describe('SwBaseAction', () => {
  let a: SwBaseAction;

  describe('should create action with default properties', () => {
    it('with no params', () => {
      a = new SwBaseAction();
      baseActionTest(a);
      expect(a.label).toBe('');
      expect(a.url).toBe('');
      expect(a.target).toBe('');
      expect(a.onclick).toBe('');
    });

    it('with an empty object', () => {
      a = new SwBaseAction({});
      baseActionTest(a);
      expect(a.label).toBe('');
      expect(a.url).toBe('');
      expect(a.target).toBe('');
      expect(a.onclick).toBe('');
    });

    it('with null', () => {
      a = new SwBaseAction(null);
      baseActionTest(a);
      expect(a.label).toBe('');
      expect(a.url).toBe('');
      expect(a.target).toBe('');
      expect(a.onclick).toBe('');
    });

    it('with null', () => {
      a = new SwBaseAction({
        hello: '',
        world: ''
      } as any);
      baseActionTest(a);
      expect(a.label).toBe('');
      expect(a.url).toBe('');
      expect(a.target).toBe('');
      expect(a.onclick).toBe('');
    });
  });

  it('should not change its properties type', () => {
    const testValues: any[] = [0, {}, [], true, false, NaN, Math.sin];

    testValues.forEach(v => {
      a = new SwBaseAction({
        label: v,
        url: v,
        target: v,
        onclick: v
      });
      baseActionTest(a);
      expect(a.label).toBe('');
      expect(a.url).toBe('');
      expect(a.target).toBe('');
      expect(a.onclick).toBe('');
    });
  });

  it('should be able to set all values', () => {
    const testValues = ['hello', 'world'];

    testValues.forEach(v => {
      a = new SwBaseAction({
        label: v,
        url: v,
        target: v,
        onclick: v
      });
      baseActionTest(a);
      expect(a.label).toBe(v);
      expect(a.url).toBe(v);
      expect(a.target).toBe(v);
      expect(a.onclick).toBe(v);
    });
  });

  it('should be able to set partial values', () => {
    const testValues = ['hello', 'world'];

    testValues.forEach(v => {
      a = new SwBaseAction({
        label: v,
        target: v
      });
      baseActionTest(a);
      expect(a.label).toBe(v);
      expect(a.url).toBe('');
      expect(a.target).toBe(v);
      expect(a.onclick).toBe('');
    });
  });
});

function baseActionTest (a: SwBaseAction) {
  expect(a).toBeDefined();
  expect(typeof a.label).toBe('string');
  expect(typeof a.url).toBe('string');
  expect(typeof a.target).toBe('string');
  expect(typeof a.onclick).toBe('string');
}
