import { SwBaseComponentObj } from './component/index';
import { SwBaseGlobalComponents, SwBasePage } from './page';

describe(`SwBasePage`, () => {
  let basePage: SwBasePage;

  it('should create an object with the default prop values', () => {
    basePage = new SwBasePage({});
    basePageRepetitiveExpectations(basePage);
  });

  it('should handle being passed non-object values', () => {
    basePage = new SwBasePage(null);
    basePageRepetitiveExpectations(basePage);
  });

  it('should weed out invalid properties', () => {
    basePage = new SwBasePage({ randomProp: 'randomProp' } as any);
    basePageRepetitiveExpectations(basePage);
    expect((basePage as any).randomProp).toBeUndefined();
  });

  describe(`\`layout\` property`, () => {

    it('should be authorable through the constructor', () => {
      const layoutValue = 'fullscreen';
      basePage = new SwBasePage({
        layout: layoutValue
      });
      basePageRepetitiveExpectations(basePage);
      expect(basePage.layout).toBe(layoutValue);
    });

    it('should not allow other type to be set through the constructor', () => {
      const layoutValue = new Array();
      basePage = new SwBasePage({
        layout: layoutValue as any
      });
      basePageRepetitiveExpectations(basePage);
      expect(basePage.layout).toBe('');
    });

  });

  describe(`\`globalComponents\` property`, () => {

    it('should be authorable through the constructor', () => {
      const globalComponentsArr = new SwBaseGlobalComponents({});
      basePage = new SwBasePage({
        globalComponents: globalComponentsArr
      });
      basePageRepetitiveExpectations(basePage);
      expect(basePage.globalComponents).toEqual(globalComponentsArr);
    });

  });

  describe(`\`components\` property`, () => {

    it('should be authorable through the constructor', () => {
      const baseCmpObj = new SwBaseComponentObj({ selector: 'mySelector' });
      const componentsArr = [ baseCmpObj ];
      basePage = new SwBasePage({
        components: componentsArr as any
      });
      basePageRepetitiveExpectations(basePage);
      expect(typeof basePage.components[0]).toBe('object');
      expect(basePage.components[0]).toEqual(baseCmpObj);
    });

    it('should not allow other type to be set through the constructor', () => {
      const componentsArr = [ '' ];
      basePage = new SwBasePage({
        components: componentsArr as any
      });
      basePageRepetitiveExpectations(basePage);
      expect(basePage.components.length).toBe(1);
      expect(typeof basePage.components[0]).toBe('object');
      expect(basePage.components[0]).toEqual(new SwBaseComponentObj({}));
    });

  });

});

function basePageRepetitiveExpectations (basePage: SwBasePage) {
  expect(typeof basePage.layout).toBe('string');
  expect(typeof basePage.globalComponents).toBe('object');
  expect(typeof basePage.components).toBe('object');
  expect(Array.isArray(basePage.components)).toBe(true);
}
