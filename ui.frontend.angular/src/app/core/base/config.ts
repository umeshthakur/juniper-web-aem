import { pagePropsHandler } from './shared/pagePropsHandler';

export class SwBasePageConfig implements Partial<SwBasePageConfig> {
  /** The page's title  */
  animateOnScroll: boolean = false;

  constructor (inputObj?: Partial<SwBasePageConfig>) {
    pagePropsHandler(this as SwBasePageConfig, inputObj);
  }
}

export const defaultPageConfig = new SwBasePageConfig();
