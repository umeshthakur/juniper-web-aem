import { DataType, is } from 'is-datatype';

/**
 * An object that describes a set of public Component properties
 */
export class SwBaseComponentProperties {
  constructor (cmpPropObj?: any) {
    /** Ensure object */
    cmpPropObj = (is(cmpPropObj, DataType.object) ? cmpPropObj : {});
    /** Move enumerable properties to `this` instance one by one */
    Object.keys(cmpPropObj)
      .filter(ck => cmpPropObj.hasOwnProperty(ck))
      .forEach(ck => this[ck] = cmpPropObj[ck]);
  }
}
