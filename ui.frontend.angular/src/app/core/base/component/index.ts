export { SwBaseComponent } from './baseComponent';
export { SwBaseComponentObj, SwPeripheralDialogComponentObj } from './baseComponentObj';
export { SwBaseComponentProperties } from './baseComponentProps';
