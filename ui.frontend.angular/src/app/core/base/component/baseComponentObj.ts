import { pagePropsHandler } from '../shared/pagePropsHandler';
import { SwBaseComponentProperties } from './baseComponentProps';

/**
 * Page component object structure in the json interface
 */
export class SwBaseComponentObj {
  /** A selector corresponding to a Component class */
  selector: string = '';
  /** The properties object of a component */
  properties: SwBaseComponentProperties = new SwBaseComponentProperties({});

  constructor (inputObj?: Partial<SwBaseComponentObj>) {
    pagePropsHandler(this as SwBaseComponentObj, inputObj);
  }
}

/**
 * Page component object structure in the json interface
 */
export class SwPeripheralDialogComponentObj extends SwBaseComponentObj {
  /** A selector corresponding to a Component class */
  uid: string = '';

  constructor (inputObj?: Partial<SwPeripheralDialogComponentObj>) {
    super(inputObj);
    pagePropsHandler(this as SwPeripheralDialogComponentObj, inputObj);
  }
}
