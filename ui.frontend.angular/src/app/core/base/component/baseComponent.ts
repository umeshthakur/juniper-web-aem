import { Directive, Input } from '@angular/core';
import { SwInputBaseProps } from '../../validation';
import { SwBaseComponentProperties } from './baseComponentProps';

/** Shared logic for all core components */
@Directive()
export class SwBaseComponent {
  /** Object to store the data that will be exposed as public Component properties */
  @Input()
  @SwInputBaseProps()
  properties: SwBaseComponentProperties;
}
