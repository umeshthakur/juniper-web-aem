import { StrictTypeArray, strictTypeArrayKey } from './strictTypeArray';

class MockClass {}

describe('StrictTypeArray', () => {
  let arr: any[];

  it(`should return an array with a \`${strictTypeArrayKey}\` when passed a class`, () => {
    arr = StrictTypeArray(MockClass);
    expect(Array.isArray(arr)).toBeTruthy();
    expect(typeof arr[strictTypeArrayKey]).toBe('function');
    expect(arr[strictTypeArrayKey]).toEqual(MockClass);
  });

  it('should return a regular array when not passed a class', () => {
    arr = StrictTypeArray({} as any);
    expect(Array.isArray(arr)).toBeTruthy();
    expect(arr[strictTypeArrayKey]).toBeUndefined();
  });

});
