import { Constructor } from '../../generics/index';

export const strictTypeArrayKey = 'swType';

/**
 * Array factory containing an attribute that details the type of items
 * that the array should contain. There is no way for us to enforce this
 * behavior currently without doing something more elaborate, but having
 * the class available to implement validation on a case-by-case basis
 * is as close as we can get currently.
 *
 * @returns An array with a property describing the
 */
export const StrictTypeArray = function StrictTypeArray<T> (type: Constructor<T>) {
  /** Generates a new array */
  const array = new Array();

  /**
   * If the `type` passed is a class (`function` type),
   * declare an immutable property on the array.
   */
  if (typeof type === 'function') {
    Object.defineProperty(array, strictTypeArrayKey, {
      configurable: false,
      enumerable: false,
      writable: false,
      value: type
    });
  }

  return array;
};
