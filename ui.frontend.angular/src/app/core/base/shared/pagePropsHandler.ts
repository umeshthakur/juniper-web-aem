import { DataType, is } from 'is-datatype';
import { strictTypeArrayKey } from './strictTypeArray';

export function pagePropsHandler<T, K> (_this: T, inputObj: K) {
  /** Ensure object */
  inputObj = (is(inputObj, DataType.object) ? inputObj : {} as K);

  /** Iterate through the object keys */
  Object.keys(inputObj)
    /** Only values that have been initialized */
    .filter(ik => _this.hasOwnProperty(ik))
    /** Ensure variable type remains the same */
    // TODO: Also ensure type consistency beyond the constructor
    .filter(ik => typeof _this[ik] === typeof inputObj[ik])
    .forEach(ik => {
      /** Store ref to current input object prop */
      const iv: any = inputObj[ik];

      /**
       * If value in `_this` is an array, and it has the `StrictTypeArray`
       * key as a class, pass all values through the Array type constructor.
       */
      if (is(_this[ik], DataType.array) && typeof _this[ik][strictTypeArrayKey] === 'function') {
        const arrayTypeClass = _this[ik][strictTypeArrayKey];
        _this[ik] = iv.map(v => (new arrayTypeClass(v)));
        return;
      }

      /** If the in `_this` is an object and not null, call that constructor when setting. */
      if (is(iv, DataType.object)) {
        _this[ik] = new _this[ik].constructor(iv);
        return;
      }

      /** Otherwise simply set the value */
      _this[ik] = iv;
    });
}
