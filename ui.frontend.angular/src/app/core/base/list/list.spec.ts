import { SwBaseNestedListItem } from './list';

describe('SwBaseNestedListItem', () => {

  function testEditorialListItemProps (item: Partial<SwBaseNestedListItem>) {
    expect(item).toBeDefined();
    expect(typeof item).toBe('object');
    expect(item.text).toBeDefined();
    expect(typeof item.text).toBe('string');
    expect(item.children).toBeDefined();
    expect(typeof item.children).toBe('object');
    expect(Array.isArray(item.children)).toBeTruthy();
  }

  it('should be a function', () => {
    expect(typeof SwBaseNestedListItem).toBe('function');
  });

  it('should create an object with initialized props', () => {
    testEditorialListItemProps(new SwBaseNestedListItem());
  });

  it('should convert strings to SwBaseNestedListItem objects', () => {
    testEditorialListItemProps(new SwBaseNestedListItem('test'));
  });

  describe('validation', () => {

    it('should only take strings for the `text` property', () => {
      [
        new SwBaseNestedListItem({ text: 0 } as any),
        new SwBaseNestedListItem({ text: [] } as any),
        new SwBaseNestedListItem({ text: {} } as any),
        new SwBaseNestedListItem({ text: '' })
      ].forEach(test => {
        testEditorialListItemProps(test);
        expect(test.text).toBe('');
      });
    });

    it('should only take arrays for the `children` property', () => {
      [
        new SwBaseNestedListItem({ children: {} } as any),
        new SwBaseNestedListItem({ children: 0 } as any),
        new SwBaseNestedListItem({ children: '' } as any),
        new SwBaseNestedListItem({ children: [] })
      ].forEach(test => {
        testEditorialListItemProps(test);
        expect(test.children).toEqual([]);
      });
    });

  });

  it('should create unidimensional list items', () => {
    [
      new SwBaseNestedListItem('test'),
      new SwBaseNestedListItem({ text: 'test' })
    ].forEach(test => {
      testEditorialListItemProps(test);
      expect(test.text).toBe('test');
      expect(test.children).toEqual([]);
    });
  });

  it('should create single-level nested list items', () => {
    [
      new SwBaseNestedListItem({
        text: 'test',
        children: [
          'test0',
          { text: 'test1' }
        ]
      })
    ].forEach(test => {
      testEditorialListItemProps(test);
      expect(test.text).toBe('test');
      expect(Array.isArray(test.children)).toBeTruthy();
      expect(test.children.length).toBe(2);
      test.children.forEach((v, i) => {
        expect(typeof v).toBe('object');
        expect(v.text).toBe('test' + i.toString());
      });
    });
  });

  it('should create multi-level nested list items', () => {
    [
      new SwBaseNestedListItem({
        text: 'test',
        children: [
          'test0',
          {
            text: 'test1',
            children: [
              'test0',
              { text: 'test1' }
            ]
          }
        ]
      })
    ].forEach(test => {
      testEditorialListItemProps(test);
      expect(test.text).toBe('test');
      expect(Array.isArray(test.children)).toBeTruthy();
      expect(test.children.length).toBe(2);
      test.children.forEach((v, i) => {
        expect(typeof v).toBe('object');
        expect(v.text).toBe('test' + i.toString());
      });
      expect(Array.isArray(test.children[1].children)).toBeTruthy();
      expect(test.children[1].children.length).toBe(2);
      test.children[1].children.forEach((v, i) => {
        expect(typeof v).toBe('object');
        expect(v.text).toBe('test' + i.toString());
      });
    });
  });

  describe('parseItemList static method', () => {

    it('should return an empty array by default', () => {
      expect(SwBaseNestedListItem.parseItemList()).toEqual([]);
      expect(SwBaseNestedListItem.parseItemList([])).toEqual([]);
      expect(SwBaseNestedListItem.parseItemList({} as any)).toEqual([], 'Object param should not have influenced result');
      expect(SwBaseNestedListItem.parseItemList(null)).toEqual([], 'Null param should not have influenced result');
      expect(SwBaseNestedListItem.parseItemList(undefined)).toEqual([], 'Undefined param should not have influenced result');
    });

    it('should process arrays of SwBaseNestedListItem', () => {
      const test = SwBaseNestedListItem.parseItemList([
        'test0',
        {
          text: 'test1',
          children: [
            'test0',
            {
              text: 'test1',
              children: [
                'test0',
                { text: 'test1' }
              ]
            }
          ]
        }
      ]);

      expect(test).toBeDefined();
      expect(Array.isArray(test)).toBeTruthy();

      test.forEach((v, i) => {
        testEditorialListItemProps(v);
        expect(v.text).toBe('test' + i.toString());
        expect(Array.isArray(v.children)).toBeTruthy();
      });

      expect(test[1].children.length).toBe(2);
      test[1].children.forEach((v, i) => {
        testEditorialListItemProps(v);
        expect(v.text).toBe('test' + i.toString());
        expect(Array.isArray(v.children)).toBeTruthy();
      });

      expect(test[1].children[1].children.length).toBe(2);
      test[1].children[1].children.forEach((v, i) => {
        testEditorialListItemProps(v);
        expect(v.text).toBe('test' + i.toString());
        expect(Array.isArray(v.children)).toBeTruthy();
      });
    });

  });

});
