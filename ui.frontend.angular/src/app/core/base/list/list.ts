export interface SwInputBaseNestedListItem {
  text?: string;
  children?: (string|SwInputBaseNestedListItem)[];
}
export type SwInputBaseNestedListItemType = string|Partial<SwInputBaseNestedListItem>;
export type SwBaseNestedItemList = ReadonlyArray<SwBaseNestedListItem>;

/**
 * An object to create nested lists
 * {
 *   text: '',
 *   children: [
 *     {
 *       text: '',
 *       children: []
 *     }
 *   ]
 * }
 *
 * The `parseItemList` static method parses an array of these, which would
 * have the matching schema as what the `children` property would have.
 */
export class SwBaseNestedListItem {
  readonly text: string = '';
  readonly children: SwBaseNestedItemList = [];

  constructor (_item?: SwInputBaseNestedListItemType) {
    /** If string */
    if (typeof _item === 'string') this.text = _item.trim();

    /** If object, excl Array */
    if (typeof _item === 'object' && _item && !Array.isArray(_item)) {
      this.text = (typeof _item.text === 'string' ? _item.text.trim() : '');
      this.children = (Array.isArray(_item.children) ? _item.children.map(v => new SwBaseNestedListItem(v)) : []);
    }
  }

  static parseItemList (_list?: ReadonlyArray<SwInputBaseNestedListItemType>): SwBaseNestedItemList {
    /** Only allow strings and objects. Both will be converted to `SwBaseNestedListItem`s */
    return (Array.isArray(_list) ? _list.map(v => new SwBaseNestedListItem(v)) : []);
  }
}
