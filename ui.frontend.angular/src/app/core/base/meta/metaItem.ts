/**
 * Page meta item object structure in the Json interface
 */
export class SwBasePageMetaItem {
  name?: string;
  property?: string;
  content: string = '';

  constructor (inputObject) {
    for (const inputObjectProp in inputObject) {
      if (inputObject.hasOwnProperty(inputObjectProp) === true) {
        switch (inputObjectProp) {
          default:
            this[inputObjectProp] = inputObject[inputObjectProp];
            break;
        }
      }
    }
  }
}
