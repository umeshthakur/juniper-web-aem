import { SwBaseLocale } from '../locale';
import { pagePropsHandler } from '../shared/pagePropsHandler';
import { StrictTypeArray } from '../shared/strictTypeArray';
import { SwBasePageMetaItem } from './metaItem';

export interface ISwBasePageMeta {
  title?: string;
  contentType?: string;
  canonical?: string;
  locale?: SwBaseLocale;
  attr?: SwBasePageMetaItem[];
}

/**
 * Object that describes a page's metadata
 */
export class SwBasePageMeta implements ISwBasePageMeta {
  /** The page's title  */
  title: string = '';
  /** The page's content type */
  contentType: string = '';
  /** The page's canonical URL */
  canonical: string = '';
  /** The page's locale */
  locale: SwBaseLocale = new SwBaseLocale();
  /** An array of attributes that for the most part will be transformed into `meta` tags */
  attr: SwBasePageMetaItem[] = StrictTypeArray(SwBasePageMetaItem);

  constructor (inputObj?: ISwBasePageMeta) {
    pagePropsHandler(this as SwBasePageMeta, inputObj);
  }
}
