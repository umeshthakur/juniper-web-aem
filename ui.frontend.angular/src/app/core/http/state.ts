/**
 * Enumeration of states a request may go through
 * Helpful as a token to convey an HTTP request's status in terms of business logic status
 */
export enum SW_HTTP_REQUEST_STATE { UNAVAILABLE, PENDING, SLOW, OK, NO_RESULTS, ERROR }

/**
 * Describes a snapshot in an HTTP request's life cycle by putting a state context around a value
 */
export interface SwHttpResponseSnapshot<T> {
  readonly state: SW_HTTP_REQUEST_STATE;
  readonly value: T;
}
