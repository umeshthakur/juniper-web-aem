export { createSampleDataByType } from './sampleDataByType.spec';
export { createMockRaf, MockRaf, MockRafOpts, MockRafStore } from './createMockRaf.spec';
export { stringLiteralArray } from './stringLiteralArray.spec';
