export interface MockRaf {
  now: () => number;
  raf: (cb: FrameRequestCallback) => number;
  step: (opts?: MockRafOpts) => void;
  cancel: (id: number) => void;
}

export interface MockRafStore {
  [id: number]: FrameRequestCallback;
}

export interface MockRafOpts {
  time?: number;
  count?: number;
}

export function createMockRaf (): MockRaf {
  let allCallbacks: MockRafStore = {};
  let callbacksLength = 0;
  let prevTime = 0;

  const now = () => prevTime;

  const raf = (cb: FrameRequestCallback) => {
    callbacksLength += 1;
    allCallbacks[callbacksLength] = cb;
    return callbacksLength;
  };

  const cancel = (id: number) => { delete allCallbacks[id]; };

  const step = (opts?: MockRafOpts) => {
    const options: MockRafOpts = Object.assign({}, {
      time: 1000 / 60,
      count: 1
    }, opts);

    let oldAllCallbacks;

    for (let i = 0; i < options.count; i++) {
      oldAllCallbacks = allCallbacks;
      allCallbacks = {};

      Object
        .keys(oldAllCallbacks)
        .forEach(function (id) {
          const cb = oldAllCallbacks[id];
          cb(prevTime + options.time);
        });

      prevTime += options.time;
    }
  };

  return {
    now: now,
    raf: raf,
    cancel: cancel,
    step: step
  };
}
