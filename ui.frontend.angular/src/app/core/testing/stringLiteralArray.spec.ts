export function stringLiteralArray<T extends string> (o: Array<T>) {
  return o.map(res => res);
}
