import { createSampleDataByType, SampleDataTypeExclusions } from './sampleDataByType.spec';

const TEST_AGAINST_DEFAULT_STRICT_EQ = true;

function testAgainstValidValues<T, K extends keyof T, J extends T[K]> (
  instance: T,
  property: K,
  values: J[],
  strictEq: boolean = TEST_AGAINST_DEFAULT_STRICT_EQ
) {
  const name = instance.constructor.name;
  const testOperator: keyof jasmine.Matchers<J> = strictEq ? 'toBe' : 'toEqual';

  values.forEach((v, i) => {
    instance[property] = v;
    expect(instance[property])[testOperator](
      v,
      `Failed allowed values test for ${name}.${property} with value '${v}'(${i})`
    );
  });
}

function testAgainstInvalidValues<T, K extends keyof T, J extends T[K]> (
  instance: T,
  property: K,
  values: J[],
  fallback: J,
  strictEq: boolean = TEST_AGAINST_DEFAULT_STRICT_EQ
) {
  const name = instance.constructor.name;
  const testOperator: keyof jasmine.Matchers<J> = strictEq ? 'toBe' : 'toEqual';

  values.forEach((v, i) => {
    instance[property] = v;
    expect(instance[property]).not[testOperator](
      v,
      `Failed invalid values test for ${name}.${property} with value '${v}'(${i})`
    );
    expect(instance[property])[testOperator](
      fallback,
      `Failed invalid values test for ${name}.${property}; expected fallback value '${fallback}'(${i})`
    );
  });
}

function testAgainstResettingToFallbackAfterInvalid<T, K extends keyof T, J extends T[K]> (
  instance: T,
  property: K,
  valid: J,
  invalid: any,
  fallback: J,
  strictEq: boolean = TEST_AGAINST_DEFAULT_STRICT_EQ
) {
  const name = instance.constructor.name;
  const testOperator: keyof jasmine.Matchers<J> = strictEq ? 'toBe' : 'toEqual';

  instance[property] = valid;
  expect(instance[property])[testOperator](
    valid,
    `Failed reset to fallback test for ${name}.${property} when setting a valid value '${valid}'`
  );

  instance[property] = invalid;
  expect(instance[property]).not[testOperator](
    invalid,
    `Failed reset to fallback test for ${name}.${property} when setting an invalid value '${invalid}'`
  );
  expect(instance[property])[testOperator](
    fallback,
    `Failed reset to fallback test for ${name}.${property}; expected fallback value '${fallback}'`
  );
}

function testAgainstOtherDataTypes<T, K extends keyof T, J extends T[K]> (
  instance: T,
  property: K,
  exclusions: SampleDataTypeExclusions | SampleDataTypeExclusions[],
  fallback: J,
  strictEq: boolean = TEST_AGAINST_DEFAULT_STRICT_EQ
) {
  const values = createSampleDataByType(exclusions);
  const name = instance.constructor.name;
  const testOperator: keyof jasmine.Matchers<J> = strictEq ? 'toBe' : 'toEqual';

  values.forEach((v, i) => {
    instance[property] = v;
    expect(instance[property]).not[testOperator](
      v,
      `Failed invalid values test for ${name}.${property} with value '${v}'(${i})`
    );
    expect(instance[property])[testOperator](
      fallback,
      `Failed invalid values test for ${instance.constructor
        .name}.${property}; expected fallback value '${fallback}'(${i})`
    );
  });
}

export const TestAgainst = {
  validValues: testAgainstValidValues,
  invalidValues: testAgainstInvalidValues,
  otherDataTypes: testAgainstOtherDataTypes,
  resettingToFallback: testAgainstResettingToFallbackAfterInvalid,

  NON_STRICT_EQ: false,
  STRICT_EQ: true
};
