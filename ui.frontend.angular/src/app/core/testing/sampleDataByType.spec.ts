
export interface SampleDataTypeObj {
  array: any[][];
  boolean: boolean[];
  function: Function[];
  number: number[];
  object: object[];
  string: string[];
}

/** A sampling of objects to be returned with `createSampleDataByType` */
const obj: SampleDataTypeObj = {
  array: [[], [1,2,3], [[], []], [{}, {}], ['a','b']],
  boolean: [true, false],
  function: [new Function(), Math.sin],
  number: [-1000, -10, -Math.PI, -1, 0, 1, Math.LN10, 10, 1000],
  object: [{}, new Date()],
  string: ['', 'hello world', 'pizza party', 'Give me a\n\rbreak', 'https://inbox.google.com/u/0/']
};

export type SampleDataTypeExclusions = keyof SampleDataTypeObj;

/**
 * Returns an array with sample data from different data types
 */
export function createSampleDataByType (
  exclusions?: SampleDataTypeExclusions|SampleDataTypeExclusions[]): any[] {
  /** Ensure array */
  let _excl = Array.isArray(exclusions) ? exclusions : [exclusions];
  _excl = _excl.filter(v => typeof v === 'string');

  /**
   * Create array from sample data types.
   * Filters out exclusions and remaps into an array of arrays of sample data
   */
  const sampling = Object.keys(obj)
    .filter(k => _excl.indexOf(k as SampleDataTypeExclusions) === -1)
    .map(k => obj[k]);

  /** Flatten array as we return */
  return [].concat.apply([], sampling);
}
