//  new product page
export {
    JnprDcSelectorComponent, JnprDcSelectorModule,
} from './dc-selector/dc-selector.module';
export {
    SwProductCompareComponent, SwProductCompareModule
} from './product-compare/product-compare.module';
export {
  SwNotesComponent, SwNotesModule
} from './jnpr-common/notes/notes.module';
