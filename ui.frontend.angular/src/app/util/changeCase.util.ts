/**
 * CamelCase to hyphen-case and viceversa
 */

const toCamelCasePattern: RegExp = /-([a-z])/g;
const toHyphenCasePattern: RegExp = /([a-z][A-Z])/g;
const toTitleCasePattern: RegExp = /\w\S*/g;
const defaultReturnValue = '';

export function toCamelCase(value: string): string {
  return typeof value === 'string'
    ? value.replace(toCamelCasePattern, g => {
      return g[1].toUpperCase();
    })
    : defaultReturnValue;
}

export function toHyphenCase(value: string): string {
  return typeof value === 'string'
    ? value.replace(toHyphenCasePattern, g => {
      return `${g[0]}-${g[1].toLowerCase()}`;
    })
    : defaultReturnValue;
}

export function toTitleCase(value: string): string {
  return typeof value === 'string'
    ? value.replace(toTitleCasePattern, g => {
      return g.charAt(0).toUpperCase() + g.substr(1).toLowerCase();
    })
    : defaultReturnValue;
}
