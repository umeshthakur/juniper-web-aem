import {
  isHTML5Video,
  isVideo,
  isYoukuVideo,
  isYoutubeVideo
} from './detectVideoType.util';

const html5VideoValid = [
  'http://sdpd.univ-lemans.fr/course/week-1/sample2.mp4',
  'http://www.iucr.org/iucr-top/comm/cpd/QARR/raw/cpd-1b.mp4',
  '//sdpd.univ-lemans.fr/course/week-1/rigaku.mp4',
  'other/samples/Cu3Au-1.mp4',
  '/samples/1d-1.mp4'
];

const html5VideoInvalid = [
  'http://mp4.univ-lemans.fr/course/week-1/sample2.raw',
  'http://sdpd.mp4.fr/course/week-1/sample2.raw',
  'http://sdpd.univ-lemans.mp4/course/mp4/sample2.raw',
  'http://sdpd.univ-lemans.fr/course/week-1/mp4.raw',
  'http://sdpd.univ-lemans.fr/course/week-1.mp4.raw',
  'http://mp4.iucr.org/iucr-top/comm/cpd/QARR/raw/cpd-1b',
  'http://www.mp4.org/iucr-top/comm/cpd/QARR/raw/cpd-1b',
  'http://www.iucr.mp4/iucr-top/comm/cpd/QARR/raw/cpd-1b',
  'http://www.iucr.org/iucr-top/mp4/cpd/QARR/raw/cpd-1b',
  'http://www.iucr.org/iucr-top/comm/cpd/QARR/raw/mp4',
  '//mp4.univ-lemans.fr/course/week-1/rigaku.zip',
  '//sdpd.mp4.fr/course/week-1/rigaku.zip',
  '//sdpd.univ-lemans.mp4/course/week-1/rigaku.zip',
  '//sdpd.univ-lemans.fr/course/mp4/rigaku.zip',
  '//sdpd.univ-lemans.fr/course/week-1/mp4.zip',
  '//sdpd.univ-lemans.fr/course/week-1/rigaku.mp4.zip',
  'mp4/samples/Cu3Au-1.raw',
  'other/mp4/Cu3Au-1.raw',
  'other/samples/mp4.raw',
  'other/samples.mp4.raw',
  '/mp4/1d-1.spe',
  '/samples/mp4.spe'
];

const youtubeVideoValid = [
  'https://www.youtube.com/watch?v=U52dD0tegsA',
  'https://youtu.be/cmGr0RszHc8',
  'https://youtu.be/U52dD0tegsA?t=2m48s',
  'https://www.youtube.com/watch?v=cmGr0RszHc8&feature=youtu.be&t=30s',
  '//youtu.be/U52dD0tegsA',
  '//youtube.com/watch?v=cmGr0RszHc8',
  'youtube.com/watch?v=U52dD0tegsA',
  'youtu.be/cmGr0RszHc8',
  'youtu.be/U52dD0tegsA?t=2m48s'
];

const youkuVideoValid = [
  'https://v.youku.com/v_show/id_XMTg0NTc0NjM2NA==.html?spm=a2hww.20023042.m_223465.5~5~5~5~5~5~A&from=y1.3-idx-beta-1519-23042.223465.1-1',
  'https://player.youku.com/player.php/sid/XMTg0NTc0NjM2NA==/v.swf',
  'https://player.youku.com/embed/XMTg0NTc0NjM2NA==',
  'https://v.youku.com/v_show/id_XMTg1MjIwMjY3Ng==.html'
];

describe('isHTML5Video', () => {

  const validateFunc = isHTML5Video;

  it('should validate as true with valid URLs', () => {
    html5VideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
  });

  it('should validate as false with invalid URLs', () => {
    html5VideoInvalid.forEach(u => expect(validateFunc(u)).toBe(false));
    youtubeVideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
    youkuVideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
  });

});

describe('isYoutubeVideo', () => {

  const validateFunc = isYoutubeVideo;

  it('should validate as true with valid URLs', () => {
    youtubeVideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
  });

  it('should validate as false with invalid URLs', () => {
    html5VideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
    html5VideoInvalid.forEach(u => expect(validateFunc(u)).toBe(false));
    youkuVideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
  });

});

describe('isYoukuVideo', () => {

  const validateFunc = isYoukuVideo;

  it('should validate as true with valid URLs', () => {
    youkuVideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
  });

  it('should validate as false with invalid URLs', () => {
    html5VideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
    html5VideoInvalid.forEach(u => expect(validateFunc(u)).toBe(false));
    youtubeVideoValid.forEach(u => expect(validateFunc(u)).toBe(false));
  });

});

describe('isVideo', () => {

  const validateFunc = isVideo;

  it('should validate as true with valid URLs', () => {
    html5VideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
    youtubeVideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
    youkuVideoValid.forEach(u => expect(validateFunc(u)).toBe(true));
  });

  it('should validate as false with invalid URLs', () => {
    html5VideoInvalid.forEach(u => expect(validateFunc(u)).toBe(false));
  });

});
