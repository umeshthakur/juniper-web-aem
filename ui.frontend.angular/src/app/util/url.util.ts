/**
 * Splits / Extracts the current url or a given one into its partials
 */
export class SwUrl {
  private _url: string;
  private _protocol: string;
  private _authorization: string;
  private _username: string;
  private _password: string;
  private _domain: string;
  private _port: string;
  private _domainList: string[];
  private _domainLevels: string[];
  private _request: string;
  private _path: string;
  private _pathList: string[];
  private _file: string;
  private _fileName: string;
  private _fileExtension: string;
  private _directory: string;
  private _directoryList: string[];
  private _query: string;
  private _queryList: string[];
  private _queryObject: any;
  private _fragment: string;

  constructor(url) {
    this.url = url;
  }

  set url(url: string) {
    this._url = (typeof url === 'string' ? url : '');

    // Reset all the URL's parts
    this._protocol = null;
    this._authorization = null;
    this._username = null;
    this._password = null;
    this._domain = null;
    this._port = null;
    this._domainList = null;
    this._domainLevels = null;
    this._request = null;
    this._path = null;
    this._pathList = null;
    this._file = null;
    this._fileName = null;
    this._fileExtension = null;
    this._directory = null;
    this._directoryList = null;
    this._query = null;
    this._queryList = null;
    this._queryObject = null;
    this._fragment = null;
  }

  /**
   * Returns the source URL.
   */
  get url(): string {
    return this._url;
  }

  /**
   * Returns the protocol of the given url, e.g., `https`;
   */
  get protocol(): string {
    if (typeof this._protocol !== 'string') {
      const splitDomain = this.url.split('://');
      this._protocol = (splitDomain[1] ? splitDomain[0] : '');
    }
    return this._protocol;
  }

  /**
   * Returns the authorization of the given url.
   * The authorization is generally the `{username}:{password}` or simply `{username}` element before the domain.
   */
  get authorization(): string {
    if (typeof this._authorization !== 'string') {
      let url = this.url;
      if (this.protocol !== null) {
        url = url.replace(this.protocol + '://', '');
      }
      const domainSplit = url.split('@');
      this._authorization = domainSplit[1] ? domainSplit[0] : '';
    }
    return this._authorization;
  }

  /**
   * Returns the username from the authorization part of the given url.
   */
  get username(): string {
    if (typeof this._username !== 'string') {
      this._username = this.authorization.split(':')[0];
    }
    return this._username;
  }

  /**
   * Returns the password from the authorization part of the given url.
   */
  get password(): string {
    if (typeof this._password !== 'string') {
      const authorizationSplit = this.authorization.split(':');
      this._password = (authorizationSplit[1] ? authorizationSplit[1] : '');
    }
    return this._password;
  }

  /**
   * Returns the complete domain of the given url.
   */
  get domain(): string {
    if (typeof this._domain !== 'string') {
      let url = this.url;
      if (this.protocol) {
        url = url.replace(this.protocol + '://', '');
      }
      if (this.authorization) {
        url = url.replace(this.authorization + '@', '');
      }
      if (this.port) {
        url = url.replace(':' + this.port, '');
      }
      this._domain = url.replace(this.request, '');
    }
    return this._domain;
  }

  /**
   * Returns the domain parts of the given url as array.
   */
  get domainList(): string[] {
    if (Array.isArray(this._domainList) === false) {
      this._domainList = this.domain.split('.').filter(domainPart => domainPart.length > 0);
    }
    return this._domainList;
  }

  /**
   * Returns the domain parts of the given url as array in order of their level.
   * For example, for `www.subdomain.example.com` the return value would be [ 'www', 'subdomain', 'example', 'com' ]
   */
  get domainLevels(): string[] {
    if (Array.isArray(this._domainLevels) === false) {
      // slice is used to create a copy of the array and leave the original unmodified
      this._domainLevels = this.domainList.slice().reverse();
    }
    return this._domainLevels;
  }

  /**
   * Returns the port of the given url.
   */
  get port(): string {
    if (typeof this._port !== 'string') {
      const urlReplace = this.url
        .replace(this.protocol + '://', '')
        .replace(this.authorization + '@', '');
      const urlSplit = urlReplace.split('/')[0].split(':');
      this._port = (urlSplit[1] !== undefined ? urlSplit[1] : '');
    }
    return this._port;
  }

  /**
   * Returns the request of the given url, i.e., generally everything that follows the domain and port elements.
   */
  get request(): string {
    if (typeof this._request !== 'string') {
      const urlWithoutProtocol = this.url.replace(this.protocol + '://', '');
      const position = urlWithoutProtocol.indexOf('/');
      this._request = ((position > -1) ? urlWithoutProtocol.substr(position) : '');
    }
    return this._request;
  }

  /**
   * Returns the path portion of the request part of the given url.
   */
  get path(): string {
    if (typeof this._path !== 'string') {
      this._path = this.request.split('?')[0];
    }
    return this._path;
  }

  /**
   * Returns an array listing the individual parts of the path from the request part of the given url.
   */
  get pathList(): string[] {
    if (Array.isArray(this._pathList) === false) {
      const pathList = this.path.split('/');
      const amount = pathList.length;

      for (let i = 0; i < amount; i++) {
        if (i < amount - 1) {
          pathList[i] += '/';
        } else {
          /** If request ends with a slash-char remove this item from pathList */
          if (pathList[i] === '') {
            pathList.splice(-1);
          }
        }
      }
      this._pathList = pathList;
    }
    return this._pathList;
  }

  /**
   * Returns the file portion of the request part of the given url.
   * For example, for a file named `my-image.ext`, that full value will be returned.
   */
  get file(): string {
    if (typeof this._file !== 'string') {
      let file;
      let itemSplitDash;
      let itemSplitDot;
      const lastItem = this.pathList[(this.pathList.length - 1)];

      if (lastItem) {
        itemSplitDash = lastItem ? lastItem.split('/') : '';

        if (itemSplitDash.length > 1) {
          file = '';
        } else {
          itemSplitDot = lastItem.split('.');
          file = itemSplitDot[1] ? lastItem : itemSplitDot[0];
        }
      } else {
        file = '';
      }
      this._file = file;
    }
    return this._file;
  }

  /**
   * Returns the filename portion of the request part of the given url.
   * For example, for a file named `my-image.ext`, `my-image` will be returned.
   */
  get fileName(): string {
    if (typeof this._fileName !== 'string') {
      const fileSplit = this.file.split('.');
      this._fileName = (fileSplit.length > 1 ? fileSplit.slice(0, -1).join('.') : fileSplit[0]);
    }
    return this._fileName;
  }

  /**
   * Returns the file extension from the request part of the given url.
   * For example, for a file named `my-image.ext`, `ext` will be returned.
   */
  get fileExtension(): string {
    if (typeof this._fileExtension !== 'string') {
      const fileReplaced = this.file.replace(this.fileName, '');
      this._fileExtension = (fileReplaced[0] === '.' ? fileReplaced.replace('.', '') : fileReplaced);
    }
    return this._fileExtension;
  }

  /**
   * Returns the directory portion of the request part of the given url.
   * The directory is the path excluding the requested file.
   */
  get directory(): string {
    if (typeof this._directory !== 'string') {
      this._directory = this.directoryList.join('');
    }
    return this._directory;
  }

  /**
   * Returns the directory parts from the request part of the given url as array.
   * The directory is the path excluding the requested file.
   */
  get directoryList(): string[] {
    if (Array.isArray(this._directoryList) === false) {
      this._directoryList = (this.file ? this.pathList.slice(0, -1) : this.pathList);
    }
    return this._directoryList;
  }

  /**
   * Returns the query portion of the request part of the given url.
   * The query is also known as the "search" part of a request.
   */
  get query(): string {
    if (typeof this._query !== 'string') {
      const requestSplit = this.request.split('?');
      this._query = (requestSplit[1] !== undefined ? requestSplit[1].split('#')[0] : '');
    }
    return this._query;
  }

  /**
   * Returns the query parts from the request part of the given url as array.
   */
  get queryList(): string[] {
    if (Array.isArray(this._queryList) === false) {
      this._queryList = this.query.split('&').filter(queryItem => queryItem.length > 0);
    }
    return this._queryList;
  }

  /**
   * Returns all parameters from the request part of the given url as an object.
   */
  get queryObject(): any {
    if (this._queryObject === null) {
      const amount = this.queryList.length;
      const queryObject = {};

      for (let i = 0; i < amount; i++) {
        const item = this.queryList[i].split('=');

        if (item[0] !== undefined) {
          queryObject[item[0]] = item[1] === undefined ? '' : item[1];
        }
      }
      this._queryObject = queryObject;
    }
    return this._queryObject;
  }

  /**
   * Returns the fragment from the request part of the given url.
   * The fragment is also known as "anchor" or "hash".
   */
  get fragment(): string {
    if (typeof this._fragment !== 'string') {
      const requestSplit = this.request.split('#');
      this._fragment = (requestSplit[1] !== undefined ? requestSplit[1] : '');
    }
    return this._fragment;
  }

  /**
   * Returns the value of the given parameter in the given url.
   */
  getQueryValue(param: string): any | null {
    const parameterObject = this.queryObject;

    for (const item in parameterObject) {
      if (parameterObject.hasOwnProperty(item) && item === param) {
        return parameterObject[item];
      }
    }
    return null;
  }
}
