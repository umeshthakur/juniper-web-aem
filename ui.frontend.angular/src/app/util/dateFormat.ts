import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'swFormatDate'})
export class FormatDate implements PipeTransform {
  transform(date: string): string {
    date = date.replace(/-/g, '/').slice(0,10);
    date = (new Date(date).toISOString().slice(0,10)).toString();
    return date.replace(/-/g, '.').slice(0,10);
  }
}

