import { DecoratorType, getDecoratorType } from './decoratorType.util';

describe('getDecoratorType', () => {

  describe('in isolation', () => {

    it('should return `None` if no arguments are passed', () => {
      const test = getDecoratorType();
      expect(test).toBe(DecoratorType.None);
    });

    it('should return `None` more arguments than allowed are passed', () => {
      const test = getDecoratorType('', '', '', '');
      expect(test).toBe(DecoratorType.None);
    });

    it('should return `Class` more arguments than allowed are passed', () => {
      const test = getDecoratorType('');
      expect(test).toBe(DecoratorType.Class);
    });

    it('should return `Property` more arguments than allowed are passed', () => {
      const test = getDecoratorType('', '');
      expect(test).toBe(DecoratorType.Property);
    });

    it('should return `Method` more arguments than allowed are passed', () => {
      const test = getDecoratorType('', '', '');
      expect(test).toBe(DecoratorType.Method);
    });

    it('should return `Parameter` more arguments than allowed are passed', () => {
      const test = getDecoratorType('', '', 0);
      expect(test).toBe(DecoratorType.Parameter);
    });

  });

  describe('integrated', () => {

    let lastReportedWhich = null;

    function MyDecorator () {
      return (...args: any[]) => { lastReportedWhich = getDecoratorType(...args); };
    }

    it('into a class context should return `Class`', () => {
      @MyDecorator()
      class ClassDecoratorClass {}
      // The following is to avoid compiler error for unused locals
      // tslint:disable-next-line no-unused-expression
      new ClassDecoratorClass();

      expect(lastReportedWhich).toBe(DecoratorType.Class);
    });

    it('should return `Property` more arguments than allowed are passed', () => {
      class PropertyDecoratorClass {
        @MyDecorator()
        myProp;
      }
      // The following is to avoid compiler error for unused locals
      // tslint:disable-next-line no-unused-expression
      new PropertyDecoratorClass();

      expect(lastReportedWhich).toBe(DecoratorType.Property);
    });

    it('should return `Method` more arguments than allowed are passed', () => {
      class MethodDecoratorClass {
        @MyDecorator()
        myProp (a = '') { a = null; }
      }
      // The following is to avoid compiler error for unused locals
      // tslint:disable-next-line no-unused-expression
      new MethodDecoratorClass();

      expect(lastReportedWhich).toBe(DecoratorType.Method);
    });

    it('into a param context should return `Parameter`', () => {
      class MethodDecoratorClass {
        myProp (@MyDecorator() a = '') { a = null; }
      }
      // The following is to avoid compiler error for unused locals
      // tslint:disable-next-line no-unused-expression
      new MethodDecoratorClass();

      expect(lastReportedWhich).toBe(DecoratorType.Parameter);
    });

  });

});
