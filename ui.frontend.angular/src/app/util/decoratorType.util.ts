/** An enumeration of the decorator types */
export enum DecoratorType {
  None,
  Class, // ClassDecorator
  Property, // PropertyDecorator
  Method, // MethodDecorator
  Parameter // ParameterDecorator
}

/**
 * Returns what type of decorator we're dealing with based on argument length
 * For documentation purposes, the following types are the ones being used to determine the type of decorator:
 *
 * **One argument:**
 *
 * *declare type ClassDecorator = <TFunction extends Function>(target: TFunction) => TFunction | void;*
 *
 * **Two arguments:**
 *
 * *declare type PropertyDecorator = (target: Object, propertyKey: string | symbol) => void;*
 *
 * **Three arguments:**
 *
 * *declare type MethodDecorator =
 *  <T>(target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<T>) => TypedPropertyDescriptor<T> | void;*
 * *declare type ParameterDecorator = (target: Object, propertyKey: string | symbol, parameterIndex: number) => void;*
 *
 * The differentiation between method and parameter decorators is made by checking the third argument type, i.e.,
 * the parameter decorator has a number as the third argument.
 *
 * Otherwise, will return "None"
 */
export function getDecoratorType (...args: any[]) {
  /** Filter-out invalid params */
  const _args = args.filter(a => a !== undefined);

  switch (_args.length) {
    case 1: return DecoratorType.Class;
    case 2: return DecoratorType.Property;
    case 3: return (typeof _args[2] === 'number' ? DecoratorType.Parameter : DecoratorType.Method);
  }
  return DecoratorType.None;
}
