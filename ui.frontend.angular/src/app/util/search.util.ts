function getCookie (name) {
  let value = '; ' + document.cookie;
  let parts = value.split('; ' + name + '=');
  if (parts.length === 2) return parts.pop().split(';').shift();
  else return null;
}

function setCookie (szName, szValue, szExpires, szPath, szDomain, bSecure) {
  let szCookieText = szName + '=' + szValue;
  szCookieText += (szExpires ? '; EXPIRES=' + szExpires : '');
  szCookieText += (szPath ? '; PATH=' + szPath : '');
  szCookieText += (szDomain ? '; DOMAIN=' + szDomain : '');
  document.cookie = szCookieText;
}

export {getCookie, setCookie};
