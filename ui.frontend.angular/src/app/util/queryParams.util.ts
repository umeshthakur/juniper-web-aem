function getObjectToQueryString(data: Object): string {
  const query = [];
  // Loop through the data object
  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      if (Object.prototype.toString.call(data[key]) === '[object Array]') {
        for (const subarray in data[key]) {
          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key][subarray]));
        }
      } else {
        if (data[key] != null && data[key] !== '') {
          query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
      }
    }
  }
  // Join each item in the array with a `&` and return the resulting string
  return query.join('&');
}

function getQueryStringToObject(queryString: string): Object {
  const pairs = queryString.split('&');
  const result = {};
  pairs.forEach(function (p) {
    const pair = p.split('=');
    const key = pair[0];
    const value = decodeURIComponent(pair[1] || '');

    if (result[key]) {
      if (Object.prototype.toString.call(result[key]) === '[object Array]') {
        result[key].push(value);
      } else {
        result[key] = [result[key], value];
      }
    } else {
      result[key] = value;
    }
  });
  return JSON.parse(JSON.stringify(result));
}

export { getObjectToQueryString, getQueryStringToObject };
