/**
 * Gets a script and attaches it to the document.
 * This function returns a promise, which will be
 * resolved when the script is loaded.
 */
export function getScript (source, doc: Document): Promise<void> {
  if (!doc || doc instanceof Document === false) throw Error(`'getScript' requires a Document.`);

  /** Create a script. Assign `any` type to avoid ts static analysis errors */
  let script: any = doc.createElement('script');
  const prior = doc.getElementsByTagName('script')[0];
  script.src = source;
  script.async = true;

  /**
   * We'll return this promise, but we're looking to fully declare it
   * before actually appending the script to the DOM
   */
  const ret = new Promise<void>((resolve) => {
    script.onload = script.onreadystatechange = (_: HTMLScriptElement, isAbort: Event): any => {
      if (isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
        /** Avoid future loading events from this script (e.g., if `src` changes) */
        script.onload = script.onreadystatechange = null;
        script = undefined;

        if (!isAbort) { resolve(); }
      }
    };
  });

  /**
   * Cannot simply appendChild, because of
   * an old IE bug around unclosed elements
   */
  prior.parentNode.insertBefore(script, prior);
  return ret;
}
