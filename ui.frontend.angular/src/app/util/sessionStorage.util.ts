
function setSessionStorage(key: string, data: Object) {
  sessionStorage.setItem(key, JSON.stringify(data));
}

function getSessionStorage(key: string) {
  return JSON.parse(sessionStorage.getItem(key));
}

export { setSessionStorage, getSessionStorage };
