function _instanceof(obj, type) {
  return type != null && obj instanceof type;
}

let nativeMap;
try {
  nativeMap = Map;
} catch (_) {
  nativeMap = new Function();
}

let nativeSet;
try {
  nativeSet = Set;
} catch (_) {
  nativeSet = new Function();
}

let nativePromise;
try {
  nativePromise = Promise;
} catch (_) {
  nativePromise = new Function();
}

function __objToStr(o) {
  return Object.prototype.toString.call(o);
}

function __isDate(o) {
  return typeof o === 'object' && __objToStr(o) === '[object Date]';
}

function __isArray(o) {
  return typeof o === 'object' && __objToStr(o) === '[object Array]';
}

function __isRegExp(o) {
  return typeof o === 'object' && __objToStr(o) === '[object RegExp]';
}

function __getRegExpFlags(re) {
  let flags = '';
  if (re.global) flags += 'g';
  if (re.ignoreCase) flags += 'i';
  if (re.multiline) flags += 'm';
  return flags;
}

/**
 * Deep clones an Object
 *
 * Please note that this function supports circular dependencies, BUT NOT BY DEFAULT.
 * Circular support is `false` by default for performance reasons. To enable it simply
 * set the corresponding parameter to true.
 *
 * @param parent - The object to be clone
 * @param [circular=false] - Whether to support circular references or not
 * @param [depth=Infinity] - How deep the clone should go
 * @param [includeNonEnumerable=false] - Whether to enable cloning of non-enumerable properties
 */
export function cloneObject<T>(
  parent: T,
  circular: boolean = false,
  depth: number = Infinity,
  includeNonEnumerable: boolean = false
): T {
  /**
   * Storage for the circular references
   */
  const allParents = [];
  const allChildren = [];

  function _clone(parent: any, depth) {
    if (parent === null) return null;
    if (depth === 0) return parent;

    let child;
    let proto;

    if (typeof parent !== 'object') {
      return parent;
    }

    if (_instanceof(parent, nativeMap)) {
      child = new nativeMap();
    } else if (_instanceof(parent, nativeSet)) {
      child = new nativeSet();
    } else if (_instanceof(parent, nativePromise)) {
      child = new nativePromise(function (resolve, reject) {
        parent.then(
          function (value) {
            resolve(_clone(value, depth - 1));
          },
          function (err) {
            reject(_clone(err, depth - 1));
          }
        );
      });
    } else if (__isArray(parent)) {
      child = [];
    } else if (__isRegExp(parent)) {
      child = new RegExp(parent.source, __getRegExpFlags(parent));
      if (parent.lastIndex) child.lastIndex = parent.lastIndex;
    } else if (__isDate(parent)) {
      child = new Date(parent.getTime());
    } else if (_instanceof(parent, Error)) {
      child = Object.create(parent);
    } else {
      proto = Object.getPrototypeOf(parent);
      child = Object.create(proto);
    }

    if (circular) {
      const index = allParents.indexOf(parent);

      if (index !== -1) {
        return allChildren[index];
      }
      allParents.push(parent);
      allChildren.push(child);
    }

    if (_instanceof(parent, nativeMap)) {
      parent.forEach(function (value, key) {
        const keyChild = _clone(key, depth - 1);
        const valueChild = _clone(value, depth - 1);
        child.set(keyChild, valueChild);
      });
    }
    if (_instanceof(parent, nativeSet)) {
      parent.forEach(function (value) {
        const entryChild = _clone(value, depth - 1);
        child.add(entryChild);
      });
    }

    for (const i in parent) {
      let attrs;
      if (proto) {
        attrs = Object.getOwnPropertyDescriptor(proto, i);
      }

      if (attrs && attrs.set == null) {
        continue;
      }
      child[i] = _clone(parent[i], depth - 1);
    }

    if (Object.getOwnPropertySymbols) {
      const symbols = Object.getOwnPropertySymbols(parent);
      for (let j = 0; j < symbols.length; j++) {
        /** Symbols are primitives, so they don't need to be cloned */
        const symbol = symbols[j];
        const descriptor = Object.getOwnPropertyDescriptor(parent, symbol);
        if (descriptor && !descriptor.enumerable && !includeNonEnumerable) {
          continue;
        }
        child[symbol] = _clone(parent[symbol], depth - 1);
        if (!descriptor.enumerable) {
          Object.defineProperty(child, symbol, {
            enumerable: false
          });
        }
      }
    }

    if (includeNonEnumerable) {
      const allPropertyNames = Object.getOwnPropertyNames(parent);
      for (let k = 0; k < allPropertyNames.length; k++) {
        const propertyName = allPropertyNames[k];
        const descriptor = Object.getOwnPropertyDescriptor(parent, propertyName);
        if (descriptor && descriptor.enumerable) {
          continue;
        }
        child[propertyName] = _clone(parent[propertyName], depth - 1);
        Object.defineProperty(child, propertyName, {
          enumerable: false
        });
      }
    }

    return child;
  }

  return _clone(parent, depth);
}
