import { SwUrl } from './url.util';

/**
 * Takes a url, parses it, and determines whether the given url refers to a native HTML video.
 */
export function isHTML5Video (url: string): boolean {
  const mp4Regex = /(mp4)$/i;
  return mp4Regex.test(new SwUrl(url).fileExtension);
}

export function isYoutubeVideo (url: string): boolean {
  const youtubeRegex = /(youtube\.com|youtu\.be)/ig;
  return youtubeRegex.test(url);
}

export function isYoukuVideo (url: string): boolean {
  const youkuRegex = /(youku\.com)/ig;
  return youkuRegex.test(url);
}

export function isVideo (url: string): boolean {
  return (isHTML5Video(url) || isYoutubeVideo(url) || isYoukuVideo(url));
}
