import { Component } from '@angular/core';
import { environment } from './../environments/environment';

@Component({
  selector: 'juniper-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor() {
    console.log(environment.resourcesPath); // Logs false for default environment
  }
  title = 'juniper-elements';
}
