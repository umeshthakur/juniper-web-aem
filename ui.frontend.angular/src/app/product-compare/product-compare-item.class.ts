
/** The available states in a compare item */
export enum CompareItemState {
  None = 0,
  Enabled = 1 << 0,
  Pinned = 1 << 1,
  EnabledAndUnpinned = Enabled,
  EnabledAndPinned = Enabled | Pinned
}

/** The main object handled by the Product Compare tool */
export class SwProductCompareItem {
  readonly state: CompareItemState;
  readonly id: CompareItemId;
  readonly name: string;
  readonly details: ProductDetails;
  readonly specs: ProductCompareItemSpecs;

  constructor (item: Partial<SwProductCompareItem>) {
    this.state = item.state !== undefined ? item.state : CompareItemState.None;
    this.id = item.id;
    this.name = item.name;
    this.details = Object.assign({}, item.details);
    this.specs = Object.assign({}, item.specs);
  }

  get isEnabled () { return !!(this.state & CompareItemState.Enabled); }

  get isPinned () { return !!(this.state & CompareItemState.Pinned) && this.isEnabled; }

  /** Validates that product compare items have essential properties */
  static validate (item: Partial<SwProductCompareItem>) {
    return (
      item.id && typeof item.id === 'string' &&
      item.name && typeof item.name === 'string' &&
      item.details && typeof item.details === 'object' &&
      item.specs && Object.keys(item.specs).length > 0
    );
  }
}

export type CompareItemId = string;

export interface ProductCompareItemSpecs {
  readonly [key: string]: string;
}

export interface ProductDetails {
  readonly page: string;
  readonly image: string;
}
