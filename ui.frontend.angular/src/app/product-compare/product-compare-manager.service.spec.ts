import { combineLatest, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { CompareItemState, SwProductCompareItem } from './product-compare-item.class';
import { CompareActionType, CompareItemsArray, SwProductCompareStoreService } from './product-compare-manager.service';
import { ɵisObservable as isObservable } from '@angular/core';

interface CompareStoreCombined {
  allItems: CompareItemsArray;
  displayItems: CompareItemsArray;
  prevStepCount: number;
  nextStepCount: number;
  serializedEnabledItems: string[];
  ableToClose: boolean;
  ableToPin: boolean;

  disabledItems: CompareItemsArray;
  enabledItems: CompareItemsArray;
  pinnedItems: CompareItemsArray;
  displayedPinnedItems: CompareItemsArray;
  unpinnedItems: CompareItemsArray;
  displayedUnpinnedItems: CompareItemsArray;
}

describe('SwProductCompareStoreService', () => {

  let service: SwProductCompareStoreService;
  let items: Array<SwProductCompareItem>;

  let combined$: Observable<CompareStoreCombined>;

  beforeEach(() => {
    service = new SwProductCompareStoreService();
    items = initItems();

    combined$ = combineLatest(
      service.allItems$,
      service.displayItems$,
      service.nextStepCount$,
      service.prevStepCount$,
      service.serializedEnabledItems$,
      service.ableToClose$,
      service.ableToPin$,
      (
        allItems: CompareItemsArray,
        displayItems: CompareItemsArray,
        nextStepCount: number,
        prevStepCount: number,
        serializedEnabledItems: string,
        ableToClose: boolean,
        ableToPin: boolean
      ): CompareStoreCombined => ({
        allItems: allItems,
        displayItems: displayItems,
        nextStepCount: nextStepCount,
        prevStepCount: prevStepCount,
        serializedEnabledItems: serializedEnabledItems.split(',').filter(s => s.trim().length > 0),
        ableToClose: ableToClose,
        ableToPin: ableToPin,

        disabledItems: allItems.filter(item => !(item.state & CompareItemState.Enabled)),
        enabledItems: allItems.filter(item => item.state & CompareItemState.Enabled),
        pinnedItems: allItems.filter(item => item.isPinned),
        displayedPinnedItems: displayItems.filter(item => item.isPinned),
        unpinnedItems: allItems.filter(item => !item.isPinned && item.isEnabled),
        displayedUnpinnedItems: displayItems.filter(item => !item.isPinned)
      })
    ).pipe(first());

    service.dispatchDisplayCountChange(4);
    service.dispatchItemSet(items);
  });

  it('service and public methods should be defined and correctly typed', () => {
    expect(service).toBeDefined();
    expect(typeof service.dispatch).toBe('function', 'Expected `dispatch` to be a function');
    expect(typeof service.dispatchDisplayCountChange).toBe(
      'function',
      'Expected `dispatchDisplayCountChange` to be a function'
    );
    expect(typeof service.dispatchItemSet).toBe('function', 'Expected `dispatchItemSet` to be a function');
    expect(typeof service.dispatchSpecSet).toBe('function', 'Expected `dispatchSpecSet` to be a function');
    expect(isObservable(service.allItems$)).toBeTruthy('Expected `allItems$` to be an observable');
    expect(isObservable(service.displayItems$)).toBeTruthy('Expected `displayItems$` to be an observable');
    expect(isObservable(service.nextStepCount$)).toBeTruthy('Expected `nextStepCount$` to be an observable');
    expect(isObservable(service.prevStepCount$)).toBeTruthy('Expected `prevStepCount$` to be an observable');
    expect(isObservable(service.serializedEnabledItems$)).toBeTruthy(
      'Expected `serializedEnabledItems$` to be an observable'
    );
    expect(isObservable(service.ableToClose$)).toBeTruthy('Expected `ableToClose$` to be an observable');
    expect(isObservable(service.ableToPin$)).toBeTruthy('Expected `ableToPin$` to be an observable');
    expect(isObservable(service.compareSpecs$)).toBeTruthy('Expected `compareSpecs$` to be an observable');
  });

  it('check initial properties of the service', done => {
    combined$.subscribe(_ => {
      expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
      expect(_.displayItems.length).toBe(0, 'Failed on `displayItems`');
      expect(_.nextStepCount).toBe(0, 'Failed on `nextStepCount`');
      expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
      expect(_.serializedEnabledItems.length).toBe(0, 'Failed on `serializedEnabledItems`');
      expect(_.ableToClose).toBeFalsy('Failed on `ableToClose`');
      expect(_.ableToPin).toBeFalsy('Failed on `ableToPin`');

      expect(_.disabledItems.length).toBe(10, 'Failed on `disabledItems`');
      expect(_.enabledItems.length).toBe(0, 'Failed on `enabledItems`');
      expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
      expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
      expect(_.unpinnedItems.length).toBe(0, 'Failed on `unpinnedItems`');
      expect(_.displayedUnpinnedItems.length).toBe(0, 'Failed on `displayedUnpinnedItems`');
      done();
    });
  });

  it('should be able to enable all items through enable all', done => {
    service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
    service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });

    combined$.subscribe(_ => {
      expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
      expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
      expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
      expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
      expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
      expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
      expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

      expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
      expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
      expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
      expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
      expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
      expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');
      done();
    });
  });

  it("should allow to set of specs, as long as they're valid", done => {
    const specs = [
      { id: 'dimensions', label: 'Dimensions (W x H x D)' },
      { id: 'weight', label: 'Weight (lb)' },
      { id: 'weight', label: 'Weight (lb)' },
      { id: 'just-the-id' },
      { label: 'just-the-label' },
      { id: 0, label: ['Weight (lb)'] }
    ];
    service.dispatchSpecSet(specs as any);

    service.compareSpecs$
      .pipe(first())
      .subscribe(specs => {
        expect(typeof specs).toBe('object');
        expect(Array.isArray(specs)).toBeTruthy();
        expect(specs.length).toBe(2, 'Failed on store length');
        expect(specs[0]).toEqual(specs[0], 'Failed on first item equality check');
        expect(specs[1]).toEqual(specs[1], 'Failed on second item equality check');

        done();
      });
  });

  describe('pinning', () => {
    beforeEach(() => {
      service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
    });

    it('should pin items by placing them at the beginning of the displayed items', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(1).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(1).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(1, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(1, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(9, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(3, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-1');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-0');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-2');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-3');

        done();
      });
    });

    it("should order pinned items based on the internal store order, not the order in which they've been pinned", done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(8).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(3).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(2, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(2, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(8, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(2, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-3');
        expect(_.displayItems[1].isPinned).toBeTruthy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-8');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-0');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-1');

        done();
      });
    });

    it('should not pin items beyond the max pinned items', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(8).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(3).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(5).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(2).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeFalsy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(3, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(3, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(7, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(1, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-2');
        expect(_.displayItems[1].isPinned).toBeTruthy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-3');
        expect(_.displayItems[2].isPinned).toBeTruthy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-5');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-0');

        done();
      });
    });

    it('should be able to pin and unpin items', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(1).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(3).id });
      service.dispatch({ type: CompareActionType.UNPIN, id: getOneItemByIdx(1).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(4).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(5).id });
      service.dispatch({ type: CompareActionType.UNPIN, id: getOneItemByIdx(4).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(2, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(2, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(8, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(2, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-3');
        expect(_.displayItems[1].isPinned).toBeTruthy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-5');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-0');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-1');

        done();
      });
    });
  });

  describe('enabling/disabling', () => {
    beforeEach(() => {
      service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
    });

    it('should not be able to change enable state of items that are not in the store', done => {
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(20).id });
      service.dispatch({ type: CompareActionType.ENABLE, id: getOneItemByIdx(22).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-0');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-1');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-2');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-3');

        done();
      });
    });

    it('should be able to disable pinned items', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(0).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(3).id });
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(0).id });
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(3).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(4, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(8, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(2, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(8, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(8, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-1');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-2');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-4');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-5');

        done();
      });
    });

    it('should shift the unpinned items when disabling the last item', done => {
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(9).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(0, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(5, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(9, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(1, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(9, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(9, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-5');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-6');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-7');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-8');

        done();
      });
    });

    it('should shift the unpinned items when disabling the last item regardless of pinned items', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(0).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(1).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(7).id });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(9).id });
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(8).id });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(0, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(4, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(8, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeFalsy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(2, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(8, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(3, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(3, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(5, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(1, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-0');
        expect(_.displayItems[1].isPinned).toBeTruthy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-1');
        expect(_.displayItems[2].isPinned).toBeTruthy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-7');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-6');

        done();
      });
    });
  });

  describe('enabling/disabling limits', () => {
    beforeEach(() => {
      service.dispatch({ type: CompareActionType.ENABLE, id: getOneItemByIdx(3).id });
      service.dispatch({ type: CompareActionType.ENABLE, id: getOneItemByIdx(9).id });
    });

    it('should be able to disable all items except one', done => {
      service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(3).id });
      // service.dispatch({ type: CompareActionType.DISABLE, id: getOneItemByIdx(9).id })

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(1, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(0, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(1, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeFalsy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeFalsy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(9, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(1, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(1, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(1, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-9');

        done();
      });
    });
  });

  describe('`dispatchDisplayCountChange`', () => {
    beforeEach(() => {
      service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
    });

    it('should not take quantities below one', done => {
      service.dispatchDisplayCountChange(0);

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');
        done();
      });
    });

    it('should scale the number of displayed items', done => {
      service.dispatchDisplayCountChange(2);

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(2, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(8, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(2, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-0');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-1');

        done();
      });
    });

    it('should scale down the number of pinned items when sizing down', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(3).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(5).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(8).id });
      service.dispatchDisplayCountChange(2);

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(2, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(8, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeFalsy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(1, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(1, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(9, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(1, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-3');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-0');

        done();
      });
    });
  });

  describe('navigation', () => {
    beforeEach(() => {
      service.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
    });

    it('should navigate next and previous', done => {
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(3, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(3, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-3');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-4');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-5');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-6');

        done();
      });
    });

    it('should keep pinned items in view despite navigation', done => {
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(4).id });
      service.dispatch({ type: CompareActionType.PIN, id: getOneItemByIdx(8).id });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(3, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(3, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(2, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(2, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(8, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(2, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeTruthy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-4');
        expect(_.displayItems[1].isPinned).toBeTruthy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-8');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-3');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-5');

        done();
      });
    });

    it('should keep in bounds while attempting to navigate to previous items', done => {
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });
      service.dispatch({ type: CompareActionType.NAV_PREV, id: null });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(6, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(0, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-0');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-1');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-2');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-3');

        done();
      });
    });

    it('should keep in bounds while attempting to navigate to next items', done => {
      // 12 "next"s
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
      service.dispatch({ type: CompareActionType.NAV_NEXT, id: null });

      combined$.subscribe(_ => {
        expect(_.allItems.length).toBe(10, 'Failed on `allItems`');
        expect(_.displayItems.length).toBe(4, 'Failed on `displayItems`');
        expect(_.nextStepCount).toBe(0, 'Failed on `nextStepCount`');
        expect(_.prevStepCount).toBe(6, 'Failed on `prevStepCount`');
        expect(_.serializedEnabledItems.length).toBe(10, 'Failed on `serializedEnabledItems`');
        expect(_.ableToClose).toBeTruthy('Failed on `ableToClose`');
        expect(_.ableToPin).toBeTruthy('Failed on `ableToPin`');

        expect(_.disabledItems.length).toBe(0, 'Failed on `disabledItems`');
        expect(_.enabledItems.length).toBe(10, 'Failed on `enabledItems`');
        expect(_.pinnedItems.length).toBe(0, 'Failed on `pinnedItems`');
        expect(_.displayedPinnedItems.length).toBe(0, 'Failed on `displayedPinnedItems`');
        expect(_.unpinnedItems.length).toBe(10, 'Failed on `unpinnedItems`');
        expect(_.displayedUnpinnedItems.length).toBe(4, 'Failed on `displayedUnpinnedItems`');

        expect(_.displayItems[0].isPinned).toBeFalsy('Failed on first item pinned state');
        expect(_.displayItems[0].id).toBe('item-6');
        expect(_.displayItems[1].isPinned).toBeFalsy('Failed on second item pinned state');
        expect(_.displayItems[1].id).toBe('item-7');
        expect(_.displayItems[2].isPinned).toBeFalsy('Failed on third item pinned state');
        expect(_.displayItems[2].id).toBe('item-8');
        expect(_.displayItems[3].isPinned).toBeFalsy('Failed on fourth item pinned state');
        expect(_.displayItems[3].id).toBe('item-9');

        done();
      });
    });
  });
});

function getOneItemByIdx (i: number) {
  return new SwProductCompareItem({
    id: `item-${i}`,
    name: `item-name-${i}`,
    details: { page: '', image: '' },
    specs: { specName: '' }
  });
}

function initItems (count: number = 10) {
  const items = [];
  for (let i = 0; i < count; i++) {
    items.push(getOneItemByIdx(i));
  }
  return items;
}
