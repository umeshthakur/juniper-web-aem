import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, filter, first, map, share } from 'rxjs/operators';

import { CompareItemId, CompareItemState, SwProductCompareItem } from './product-compare-item.class';
import { SwProductCompareSpec } from './product-compare-spec.class';

export const enum CompareActionType {
  PIN = 1,
  UNPIN,
  ENABLE,
  DISABLE,
  ENABLE_ALL,
  NAV_NEXT,
  NAV_PREV
}

export interface CompareAction {
  type: CompareActionType;
  id: CompareItemId;
}

type CompareItemsStore = Map<CompareItemId, SwProductCompareItem>;
export type CompareItemsArray = ReadonlyArray<SwProductCompareItem>;

/** Compares two instances of CompareItemsArray to determine change */
function CompareItemsArrayDiff (a: CompareItemsArray, b: CompareItemsArray): boolean {
  return (
    a.length === b.length &&
    b.every((bItem, idx) => {
      const aItem = a[idx];
      return (aItem && aItem.id === bItem.id && aItem.state === bItem.state);
    })
  );
}

/**
 * Service for managing product items for product-compare component, it works as a state machine
 * and provides access to the items' collections
 */
@Injectable()
export class SwProductCompareStoreService {

  /** Record for the item order */
  private _compareItemsIndexStore: ReadonlyArray<CompareItemId> = [];

  /** Stores the compare items by compare item id */
  private _compareItemsStore: BehaviorSubject<CompareItemsStore> = new BehaviorSubject(new Map());

  /** Stores the compare spec items */
  private _compareSpecsStore = new BehaviorSubject<ReadonlyArray<SwProductCompareSpec>>([]);

  /** A steam of compare spec items */
  public readonly compareSpecs$ = this._compareSpecsStore.asObservable();

  /** The maximum items that can be displayed */
  private _maxDisplayItems: BehaviorSubject<number> = new BehaviorSubject(2);

  /** A steam with the maximum items that can be displayed */
  private readonly _maxDisplayItems$ = this._maxDisplayItems.asObservable();

  /** The minimum number of items that can be displayed */
  private readonly _minimumItemDisplayCount = 1;

  /** Index that tracks the leftmost unpinned item shown */
  private _unpinnedTrackIndex: BehaviorSubject<number> = new BehaviorSubject(0);

  /** Stream for the leftmost unpinned item shown */
  private readonly _unpinnedTrackIndex$ = this._unpinnedTrackIndex.pipe(distinctUntilChanged());

  /** Indicates whether the minimum of unpinned items, visible or not, has been reached */
  private _reachedMinUnpinnedItems = new BehaviorSubject<boolean>(false);
  private readonly _reachedMinUnpinnedItems$ = this._reachedMinUnpinnedItems.pipe(distinctUntilChanged());

  /** Indicates whether the minimum of displayed unpinned items has been reached */
  private _reachedMinDisplayedUnpinnedItems = new BehaviorSubject<boolean>(false);
  private readonly _reachedMinDisplayedUnpinnedItems$ = this._reachedMinDisplayedUnpinnedItems.pipe(distinctUntilChanged());

  /** A stream containing all items in the store */
  public get allItems$ (): Observable<CompareItemsArray> {
    return this._compareItemsStore.pipe(
      /** Map all indexes to compare item objects */
      map(store => this._compareItemsIndexStore.map(id => store.get(id))),
      distinctUntilChanged(CompareItemsArrayDiff),
      share()
    );
  }

  /** A stream containing all the enabled items */
  private get _enabledItems$ (): Observable<CompareItemsArray> {
    return this.allItems$.pipe(
      /** Filter for all enabled items */
      map(items => items.filter(item => !!item && item.state & CompareItemState.Enabled)),
      distinctUntilChanged(CompareItemsArrayDiff),
      share()
    );
  }

  /** A stream of serialized and sorted enabled items */
  public get serializedEnabledItems$ (): Observable<string> {
    return this._enabledItems$.pipe(
      map(items => items.map(item => item.id).sort().join(','))
    );
  }

  /** Attempts decreasing the unpinned track index */
  private _decreaseUnpinnedIndex () {
    this._unpinnedTrackIndex$.pipe(first())
      /** `Math.max` ensures no negative values */
      .subscribe(currentIdx => this._unpinnedTrackIndex.next(Math.max(0, currentIdx - 1)));
  }

  /** Attempts increasing the unpinned track index */
  private _increaseUnpinnedIndex () {
    combineLatest(
      this._unpinnedTrackIndex$,
      this._enabledItems$,
      this._maxDisplayItems$
    ).pipe(
      map(([currentIdx, enabledItems, maxDisplayItems]) => {
        const max = enabledItems.length - maxDisplayItems;
        return Math.min(max, currentIdx + 1);
      }),
      first()
    )
    /** `Math.min` ensures not going beyond the max */
    .subscribe(next => this._unpinnedTrackIndex.next(next));
  }

  /** A stream containing all the displayed items */
  public get displayItems$ (): Observable<CompareItemsArray> {
    return combineLatest(
      this._enabledItems$,
      this._maxDisplayItems$,
      this._unpinnedTrackIndex.asObservable()
    ).pipe(
      /** Split enabled items into pinned and unpinned */
      map(([enabledItems, maxDisplayItems, unpinnedTrackIndex]): [CompareItemsArray, CompareItemsArray, number, number] => {
        /** Separate pinned and unpinned */
        const pinnedItems = enabledItems.filter(item => item.isPinned);
        const unpinnedItems = enabledItems.filter(item => !item.isPinned);

        return [pinnedItems, unpinnedItems, maxDisplayItems, unpinnedTrackIndex];
      }),
      filter(([pinnedItems, unpinnedItems, maxDisplayItems, unpinnedTrackIndex]) => {
        /** Get pinned/unpinned items count */
        const pinnedCount = pinnedItems.length;
        const unpinnedCount = unpinnedItems.length;
        const potentialUnpinnedOnScreenCount = unpinnedCount - unpinnedTrackIndex;

        /**
         * Determine whether there's a surplus of pinned items.
         * A surplus is determined by the fact that there should never be all pinned items on the screen.
         *
         * If there is a surplus, call to unpin the last item from the pinned items.
         */
        const pinnedSurplus = Math.max(0, pinnedCount - maxDisplayItems + this._minimumItemDisplayCount) > 0;

        if (pinnedSurplus) {
          const pinnedItemToRemove = pinnedItems.slice(-1)[0];
          this.dispatch({ type: CompareActionType.UNPIN, id: pinnedItemToRemove.id });

          return false;
        }

        /**
         * Avoid potential gaps to the right when there's items that could be shown.
         * If there are potential gaps to be avoided, we call to decrease the unpinned index
         *
         * If there's going to be less unpinned items on screen that the minimum display count
         * or the remaining unpinned slots AND there's enough unpinned items to fill the gap in either
         * one of those scenarios, decrease the unpinned offset
         */
        const shouldOffset = (
          (potentialUnpinnedOnScreenCount < this._minimumItemDisplayCount ||
            potentialUnpinnedOnScreenCount < maxDisplayItems - pinnedCount) &&
          unpinnedCount >= this._minimumItemDisplayCount && unpinnedCount >= maxDisplayItems - pinnedCount
        );

        if (shouldOffset) this.dispatch({ type: CompareActionType.NAV_PREV, id: null });

        return !shouldOffset;
      }),
      /**
       * Calculate display items
       */
      map(([pinnedItems, unpinnedItems, maxDisplayItems, unpinnedTrackIndex]) => {
        /** Get pinned/unpinned items count */
        const pinnedCount = pinnedItems.length;
        const unpinnedCount = unpinnedItems.length;

        /** Slice the unpinned array with the smallest of the two indexes */
        const slicedUnpinned = unpinnedItems.slice(unpinnedTrackIndex);
        /** Concatenate and trim to final display max size */
        const joint = pinnedItems.concat(slicedUnpinned);
        /** Get final display items */
        const displayItems = joint.slice(0, maxDisplayItems);

        /**
         * Housekeeping across the board
         */
        this._reachedMinUnpinnedItems.next(unpinnedCount <= this._minimumItemDisplayCount);
        this._reachedMinDisplayedUnpinnedItems.next(displayItems.length - pinnedCount <= this._minimumItemDisplayCount);

        return displayItems;
      }),
      distinctUntilChanged(CompareItemsArrayDiff),
      share()
    );
  }

  /** Whether nav previous is still possible */
  public get prevStepCount$ (): Observable<number> {
    return this._unpinnedTrackIndex.asObservable();
  }

  /** Whether nav next is still possible */
  public get nextStepCount$ (): Observable<number> {
    return combineLatest(
      this._enabledItems$,
      this._unpinnedTrackIndex.asObservable(),
      this.displayItems$
    ).pipe(
      map(([enabledItems, unpinnedTrackIndex, displayItems]) =>
        (enabledItems.length - unpinnedTrackIndex - displayItems.length)),
      share()
    );
  }

  public get ableToPin$ (): Observable<boolean> {
    return this._reachedMinDisplayedUnpinnedItems$.pipe(map(bool => !bool));
  }

  public get ableToClose$ (): Observable<boolean> {
    return this._reachedMinUnpinnedItems$.pipe(map(bool => !bool));
  }

  /** Takes care of all actions that need to be executed in the store */
  public dispatch (action: CompareAction) {
    switch (action.type) {
      case CompareActionType.PIN:
        this._setPinnedState(action.id, true);
        break;
      case CompareActionType.UNPIN:
        this._setPinnedState(action.id, false);
        break;
      case CompareActionType.ENABLE:
        this._setEnabledState(action.id, true);
        break;
      case CompareActionType.DISABLE:
        this._setEnabledState(action.id, false);
        break;
      case CompareActionType.ENABLE_ALL:
        this._enableAll();
        break;
      case CompareActionType.NAV_NEXT:
        this._increaseUnpinnedIndex();
        break;
      case CompareActionType.NAV_PREV:
        this._decreaseUnpinnedIndex();
        break;
    }
  }

  /** Sets a brand new set of items in the store */
  public dispatchItemSet (set: CompareItemsArray) {
    const itemsIndexStore: string[] = [];
    const itemsStore: Map<string, SwProductCompareItem> = new Map();

    set.filter(item => SwProductCompareItem.validate(item))
      .forEach(item => {
        if (itemsIndexStore.indexOf(item.id) < 0) {
          itemsIndexStore.push(item.id);
        }
        itemsStore.set(item.id,
          new SwProductCompareItem({
            id: item.id,
            name: item.name,
            details: item.details,
            specs: item.specs
          })
        );
      });

    this._compareItemsIndexStore = itemsIndexStore;
    this._compareItemsStore.next(itemsStore);
  }

  /** Sets a brand new set of specs */
  public dispatchSpecSet (set: ReadonlyArray<SwProductCompareSpec>) {
    const itemsIndexStore: string[] = [];
    const specsStore: SwProductCompareSpec[] = [];

    set.filter(item => SwProductCompareSpec.validate(item))
      .forEach(item => {
        if (itemsIndexStore.indexOf(item.id) < 0) {
          itemsIndexStore.push(item.id);
          specsStore.push(
            new SwProductCompareSpec(item)
          );
        }
      });

    this._compareSpecsStore.next(specsStore);
  }

  /** Shifts the maximum number of display items */
  public dispatchDisplayCountChange (n: number) {
    if (n > 1) {
      this._maxDisplayItems.next(n);
    }
  }

  /** Attempts to set an either pinned or unpinned state by id */
  private _setPinnedState (id: CompareItemId, attemptPin: boolean) {
    /** Retrieve item */
    const itemById = this._getItemById(id);

    /** Pinning/unpinning always happens amongst enabled items */
    const state = (
      attemptPin ?
      CompareItemState.EnabledAndPinned :
      CompareItemState.EnabledAndUnpinned
    );

    this.ableToPin$.pipe(
      first(),
      filter(ableToPin => ableToPin || !attemptPin)
    ).subscribe(() => {
      const next = this._compareItemsStore.getValue().set(id, this._setItemState(itemById, state));
      this._compareItemsStore.next(next);
    });
  }

  /** Attempts to set an either enabled or disabled state by id */
  private _setEnabledState (id: CompareItemId, attemptEnable: boolean) {
    /** Retrieve item */
    const itemById = this._getItemById(id);

    if (!itemById) return;

    const state = (
      attemptEnable ?
      itemById.state | CompareItemState.Enabled :
      CompareItemState.None
    );

    this.ableToClose$.pipe(
      first(),
      filter(ableToClose => (itemById.isPinned || (ableToClose || attemptEnable)))
    ).subscribe(() => {
      const next = this._compareItemsStore.getValue().set(id, this._setItemState(itemById, state));
      this._compareItemsStore.next(next);
    });
  }

  /** Enables all items in the store */
  private _enableAll () {
    const itemsStore = new Map();

    this._compareItemsStore.getValue().forEach((item: SwProductCompareItem, id: CompareItemId) => {
      /**
       * Pass along same item if it's already enabled.
       * If it isn't issue the creation of a new item with enabled state
       */
      const newItem = (
        item.state & CompareItemState.Enabled ? item :
        this._setItemState(item, item.state | CompareItemState.Enabled)
      );
      /** Set to new map */
      itemsStore.set(id, newItem);
    });

    this._compareItemsStore.next(itemsStore);
  }

  /** Creates new items with a changed state */
  private _setItemState (item: SwProductCompareItem, state: CompareItemState): SwProductCompareItem {
    return new SwProductCompareItem(
      Object.assign({}, item, { state: state })
    );
  }

  /** Retrieves an item in the store by id */
  private _getItemById (id: CompareItemId) {
    return this._compareItemsStore.getValue().get(id);
  }
}
