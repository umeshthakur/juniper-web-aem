import { ModuleWithProviders, NgModule, DoBootstrap, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SwButtonModule } from '../jnpr-common/button/button.component';
import { SwIconModule } from '../jnpr-common/icon/index';
import { SwBackgroundMediaModule } from '../modules/backgroundMedia/index';
import { SwProductCompareComponent } from './product-compare.component';
import { createCustomElement } from '@angular/elements';
import { SwGlobalModule } from '../modules/global';
import { OverlayModule } from '@angular/cdk/overlay';
import { AsyncModule } from '../modules/async';
import { SwProductCompareStoreService } from './product-compare-manager.service';
import { BrowserModule } from '@angular/platform-browser';

@NgModule( {
  imports: [
    BrowserModule,
    CommonModule,
    SwIconModule,
    SwButtonModule,
    SwGlobalModule,
    OverlayModule,
    SwBackgroundMediaModule,
    AsyncModule
  ],
  exports: [SwProductCompareComponent],
  declarations: [SwProductCompareComponent],
  entryComponents: [SwProductCompareComponent],
  providers: [SwProductCompareStoreService],
  bootstrap: [ SwProductCompareComponent ]
} )
export class SwProductCompareModule implements DoBootstrap {

  constructor(private injector: Injector) {
    customElements.define('sw-product-compare', createCustomElement(SwProductCompareComponent, { injector: this.injector }));
  }


  ngDoBootstrap() {}
}

export { SwProductCompareComponent } from './product-compare.component';
