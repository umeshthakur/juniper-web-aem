import { CommonModule } from '@angular/common';
import {
  AfterContentInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnDestroy,
  TemplateRef,
  TrackByFunction,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { animate, AUTO_STYLE, style, transition, trigger } from '@angular/animations';
import { ScrollDispatcher, ViewportRuler } from '@angular/cdk/scrolling';
import { merge, Observable, Subject, Subscription } from 'rxjs';
import { debounceTime, first, map, takeUntil } from 'rxjs/operators';

import { SwBaseComponent } from '../core/base';
import { GlobalRef, SwGlobalModule } from '../modules/global/index';
import {
  CdkOverlayOrigin,
  ConnectionPositionPair,
  Overlay,
  OverlayConfig,
  OverlayRef
} from '@angular/cdk/overlay';
import { TemplatePortal } from '@angular/cdk/portal';
import { DataType, simpleObjToSchemaConverter, SwInputCallback, SwInputProp } from '../core/validation';
import { SwBreakpointNames, SwBreakpointObserver, SwBreakpoints } from '../modules/layout/index';

import { SwProductCompareItem } from './product-compare-item.class';
import { SwProductCompareSpec } from './product-compare-spec.class';
import { CompareActionType, SwProductCompareStoreService } from './product-compare-manager.service';
import { Injector } from '@angular/core';

export const l10nDefaults = { back: 'Back' };
const queryKey = 'p';

/**
 * Product Compare
 */
@Component({
  selector: 'sw-product-compare',
  templateUrl: './product-compare.component.html',
  styleUrls: ['./product-compare.component.scss'],
  providers: [SwProductCompareStoreService],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('itemAnimationState', [
      transition(':enter', [
        style({ width: 0, opacity: 0 }),
        animate('400ms cubic-bezier(0.35, 0, 0.25, 1)', style(AUTO_STYLE))
      ]),
      transition(':leave', [animate('400ms cubic-bezier(0.35, 0, 0.25, 1)', style({ width: 0, opacity: 0 }))])
    ])
  ]
})
export class SwProductCompareComponent extends SwBaseComponent implements AfterContentInit, OnDestroy {
  @ViewChild('labelOverlay', { read: TemplateRef })
  labelOverlay: TemplateRef<any>;

  @SwInputProp<string>(DataType.string, '')
  title: string;

  @SwInputProp<string>(DataType.string, '')
  selectorLabel: string;

  @SwInputProp<string>(DataType.string, '')
  addAllLabel: string;

  @SwInputProp<string>(DataType.string, '')
  ctaLabel: string;

  @SwInputProp<string>(DataType.string, '')
  itemsLabel: string;

  @SwInputCallback('_setSpecCategories')
  @SwInputProp<SwProductCompareSpec, SwProductCompareSpec[]>(DataType.array, [], { type: DataType.object })
  specCategories: SwProductCompareSpec[];

  @SwInputCallback('_setItems')
  @SwInputProp<SwProductCompareItem, SwProductCompareItem[]>(DataType.array, [], { type: DataType.object })
  items: SwProductCompareItem[];

  @SwInputProp<any>(DataType.object, l10nDefaults, { schema: simpleObjToSchemaConverter(l10nDefaults) })
  l10n;

  @Input() columnsInView: number = 1;

  get mobileView () {
    return this._mobileView ? true : null;
  }
  private _mobileView: boolean = true;

  rowIds: string[] = [];

  constructor(
    private elementRef: ElementRef,
    private store: SwProductCompareStoreService,
    private _cd: ChangeDetectorRef,
    private _global: GlobalRef,
    private injector: Injector,
    private _overlay: Overlay,
    private _viewContainerRef: ViewContainerRef,
    private _viewport: ViewportRuler,
    private _scroll: ScrollDispatcher,
    private _breakpointObserver: SwBreakpointObserver
  ) {
    super();
  }

  streamCancel$ = new Subject<void>();
  isNavPrevDisabled$: Observable<boolean> = this.store.prevStepCount$.pipe(
    map(n => !n),
    takeUntil(this.streamCancel$)
  );
  isNavNextDisabled$: Observable<boolean> = this.store.nextStepCount$.pipe(
    map(n => n === 0),
    takeUntil(this.streamCancel$)
  );
  disableClose$: Observable<boolean> = this.store.ableToClose$.pipe(
    map(n => !n),
    takeUntil(this.streamCancel$)
  );
  disablePin$: Observable<boolean> = this.store.ableToPin$.pipe(
    map(n => !n),
    takeUntil(this.streamCancel$)
  );
  compareSpecs$ = this.store.compareSpecs$.pipe(takeUntil(this.streamCancel$));
  allItems$ = this.store.allItems$.pipe(takeUntil(this.streamCancel$));
  displayItems$ = this.store.displayItems$.pipe(takeUntil(this.streamCancel$));

  /**
   * Initializes the component
   */
  ngAfterContentInit () {
    /** resizing the screen changes the number of displayed items */
    const breakpointChange$ = Object.keys(SwBreakpoints).map(k => {
      const key = <SwBreakpointNames>k;
      const query = SwBreakpoints[key].rangeCondition;
      const name = SwBreakpoints[key].name;
      return this._breakpointObserver.observe(query, name);
    });

    merge(...breakpointChange$)
      .pipe(takeUntil(this.streamCancel$))
      .subscribe(e => {
        if (e.matches) {
          this.updateColState(<SwBreakpointNames>e.name);
          this._cd.markForCheck();
          this._cd.detectChanges();
        }
      });

    /** Parse URL to call for the removal of those ids found */
    this._parseUrlQueryForIds(this.items);
    /** Subscribe to serialized enabled items and use those to set the query string */
    this.store.serializedEnabledItems$
      .pipe(takeUntil(this.streamCancel$))
      .subscribe(query =>
        this._global.location.replaceState(this._global.location.path().split('?')[0], queryKey + '=' + query)
      );
  }

  _setItems (items: SwProductCompareItem[] = []) {
    this.store.dispatchItemSet(this.items);
  }

  _setSpecCategories (items: SwProductCompareSpec[] = []) {
    /** Set specs */
    this.store.dispatchSpecSet(this.specCategories);
  }

  /**
   * Parses the query portion of the URL in the hoped of extracting product ids.
   * If any are found, only those will be enabled; otherwise, all will be enabled
   */
  private _parseUrlQueryForIds (items: SwProductCompareItem[] = []) {
    /** Get the valid ids */
    const validIds = items.map(item => item.id);
    /** Get relevant query parameter */
    const queryMap = this._global.queryAsMap;
    const query = typeof queryMap === 'object' && queryMap.hasOwnProperty(queryKey) ? queryMap[queryKey] as string : '';
    /** Fetch valid items from URL */
    const itemsFromUrl = query.split(',').filter(id => validIds.indexOf(id) >= 0);

    if (itemsFromUrl.length > 0) {
      itemsFromUrl.forEach(id => this.store.dispatch({ type: CompareActionType.ENABLE, id: id }));
    } else {
      this.addAll();
    }
  }

  /**
   * Updates the number of displayed items
   * @param breakpoint - breakpoint string
   */
  private updateColState (breakpoint: SwBreakpointNames) {
    let isMobile = false;
    switch (breakpoint) {
      case SwBreakpointNames.xs:
        this.store.dispatchDisplayCountChange(1);
        this.columnsInView = 2;
        isMobile = true;
        break;
      case SwBreakpointNames.sm:
        this.store.dispatchDisplayCountChange(2);
        this.columnsInView = 3;
        isMobile = true;
        break;
      case SwBreakpointNames.md:
        this.store.dispatchDisplayCountChange(2);
        this.columnsInView = 3;
        break;
      case SwBreakpointNames.lg:
        this.store.dispatchDisplayCountChange(3);
        this.columnsInView = 4;
        break;
      case SwBreakpointNames.xl:
        this.store.dispatchDisplayCountChange(4);
        this.columnsInView = 5;
        break;
    }
    this._mobileView = isMobile;
  }

  /**
   * Changes the state of an item
   * @param type - type of action (pin or show)
   * @param item - the item that action applies to
   */
  public toggle (type: string, item: SwProductCompareItem) {
    switch (type) {
      case 'show':
        this.toggleShow(item);
        break;
      case 'pin':
        this.togglePin(item);
        break;
    }
  }

  /** Enables/disables items */
  public toggleShow (item: SwProductCompareItem) {
    if (!item.isEnabled) {
      this.store.dispatch({ type: CompareActionType.ENABLE, id: item.id });
    } else {
      this.store.dispatch({ type: CompareActionType.DISABLE, id: item.id });
    }
  }

  /** Pins/unpins items */
  public togglePin (item: SwProductCompareItem) {
    if (!item.isPinned) {
      this.store.dispatch({ type: CompareActionType.PIN, id: item.id });
    } else {
      this.store.dispatch({ type: CompareActionType.UNPIN, id: item.id });
    }
  }

  /**
   * Activates all items and updates query string accordingly
   */
  public addAll () {
    this.store.dispatch({ type: CompareActionType.ENABLE_ALL, id: null });
  }

  /** Navigates to the right */
  public next () {
    this.store.dispatch({ type: CompareActionType.NAV_NEXT, id: null });
  }

  /** Navigates to the left */
  public prev () {
    this.store.dispatch({ type: CompareActionType.NAV_PREV, id: null });
  }

  /**
   * Implements going back to the page where the user came from
   */
  public goBack () {
    this._global.location.back();
  }

  /** Supports ngFor to track indexes  */
  readonly trackById: TrackByFunction<SwProductCompareItem> = (idx: number, item: SwProductCompareItem) => {
    return item ? `${item.id}${item.isPinned}` : undefined;
  };

  /** Provides the appropriate pin icon to each product column */
  getPinIcon (item: SwProductCompareItem) {
    return item.isPinned ? 'pin' : 'pin_outline';
  }

  /** Provides the appropriate alt tag for the icon above */
  getPinIconAlt (item: SwProductCompareItem) {
    return item.isPinned ? 'pinned' : 'unpinned';
  }

  /** Animates items */
  public onItemAnimStart (id: string) {
    this.onItemAnim(id, true);
  }
  public onItemAnimDone (id: string) {
    this.onItemAnim(id, false);
  }
  private onItemAnim (id: string, isStart: boolean = true) {
    const sel = Array.prototype.slice.call(
      this.elementRef.nativeElement.querySelectorAll(`[data-key='${id}'] .cell__content`)
    );
    const colWidth = isStart ? this._columnWidths || 0 : 0;

    for (const el of sel) {
      el.style.width = colWidth ? `${colWidth}px` : '';
    }

    if (!isStart) this._fetchColWidthsTrigger.next();
  }

  /** A cached value for the column widths */
  private _columnWidths: number;

  /** A subject who's sole purpose is to next the _fetchColumnWidths$ */
  private _fetchColWidthsTrigger = new Subject<void>();

  /**
   * A stream listening to the global resize event and
   * a trigger from when the column widths change.
   * On every stream trigger, the offset width of columns is cached
   */
  private _fetchColumnWidths$ = merge(this._viewport.change(), this._fetchColWidthsTrigger).pipe(
    debounceTime(100),
    takeUntil(this.streamCancel$)
  ).subscribe(() => {
    const _el: HTMLElement = this.elementRef.nativeElement.querySelector(`[data-key] .cell__content`);
    const _elWidth = _el ? _el.offsetWidth : 0;

    if (_el && _elWidth) this._columnWidths = _elWidth;
  });

  /** Determines the state on whether the label overlay is set */
  get isLabelOpen (): boolean {
    return this._labelOverlayRef ? true : false;
  }

  /** The currently displayed label in an overlay */
  public currentLabel;

  /** OverlayRef from the attached label overlay */
  private _labelOverlayRef: OverlayRef;

  /** Stores a reference to the merged close label streams */
  private _labelEvtHandler: Subscription;

  /** Trigger to open an overlaid label */
  public openLabel (origin: CdkOverlayOrigin, label: string) {
    if (this.isLabelOpen) this.closeLabel();

    this.currentLabel = label;
    this._createLabel(origin);
    const portal = new TemplatePortal(this.labelOverlay, this._viewContainerRef);

    this._labelEvtHandler = merge(
      this._labelOverlayRef.backdropClick().pipe(first()),
      this._scroll.scrolled(),
      this._viewport.change()
    ).subscribe(() => this.closeLabel());

    this._labelOverlayRef.attach(portal);
  }

  /** Disposal of references created for the label overlay */
  private closeLabel () {
    if (this.isLabelOpen) {
      this._labelEvtHandler.unsubscribe();
      this._labelOverlayRef.dispose();
      this._labelOverlayRef = null;
      this.currentLabel = null;
    }
  }

  /** Overlay connection pair config */
  private readonly _labelPosition: ConnectionPositionPair =
    new ConnectionPositionPair(
      { originX: 'start', originY: 'top' },
      { overlayX: 'start', overlayY: 'top' }
    );

  /** Creates the label overlay based */
  private _createLabel (origin: CdkOverlayOrigin): void {
    const strategy = this._overlay.position()
      .flexibleConnectedTo(origin.elementRef)
      .withPositions([{
        originX: this._labelPosition.originX,
        originY: this._labelPosition.originY,
        overlayX: this._labelPosition.overlayX,
        overlayY: this._labelPosition.overlayY,
        offsetX: 40
      }]);

    const config = new OverlayConfig();
    config.positionStrategy = strategy;
    config.backdropClass = 'cdk-overlay-transparent-backdrop';
    config.hasBackdrop = true;
    config.width = 'auto';

    this._labelOverlayRef = this._overlay.create(config);
  }

  ngOnDestroy () {
    this.streamCancel$.next();
    if (this._fetchColumnWidths$) this._fetchColumnWidths$.unsubscribe();
    this.closeLabel();
  }

}

