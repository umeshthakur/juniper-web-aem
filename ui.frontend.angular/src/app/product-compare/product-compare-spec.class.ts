
export class SwProductCompareSpec {
  readonly id: string;
  readonly label: string;

  constructor (item: SwProductCompareSpec) {
    this.id = item.id;
    this.label = item.label;
  }

  static validate (item: SwProductCompareSpec): boolean {
    return (
      item.id && typeof item.id === 'string' &&
      item.label && typeof item.label === 'string'
    );
  }
}
