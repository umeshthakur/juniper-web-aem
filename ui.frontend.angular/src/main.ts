import { enableProdMode } from '@angular/core';
import { platformBrowser } from '@angular/platform-browser';

import { SwBrowserPageModule } from '../src/app/modules/page/page-browser.module';

import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowser().bootstrapModule(SwBrowserPageModule).catch(e => console.error(e));