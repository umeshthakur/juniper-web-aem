const express = require('express');
const fs = require('fs');
const open = require('open');
const app = express();
const port = 3000;
const {createProxyMiddleware} = require('http-proxy-middleware');
const {exec} = require("child_process");
const chokidar = require('chokidar');
const distFolder = './dist/elements';
const end_timeout = 4000;
let ready = false;
const chalk = require('chalk');
const log = console.log;
// Watcher

const watcher = chokidar.watch(distFolder, {ignored: /^\./, persistent: true, awaitWriteFinish: true});

watcher
	.on('add', function (path) {
		log(chalk.green('File', path, 'has been added'));
		if (ready === false) {
			ready = true;
			fs.stat(path, function (err, stat) {
				// Replace error checking with something appropriate for your app.
				if (err) throw err;
				setTimeout(checkEnd, end_timeout, path, stat);
			});
		}
	})
	.on('change', function (path) {
		log(chalk.yellow('File', path, 'has been changed'));
		if (ready === false) {
			ready = true;
			fs.stat(path, function (err, stat) {
				// Replace error checking with something appropriate for your app.
				if (err) throw err;
				setTimeout(checkEnd, end_timeout, path, stat);
			});
		}
	})
	.on('unlink', function (path) {
		log(chalk.white.bgRed('File', path, 'has been removed'));

	})
	.on('error', function (error) {
		log(chalk.bold.white.bgRed('Error happened', error));

	})
	.on('ready', function () {

	})

function checkEnd(path, prev) {
	fs.stat(path, function (err, stat) {
		// Replace error checking with something appropriate for your app.
		if (err) throw err;
		if (stat.mtime.getTime() === prev.mtime.getTime()) {
			console.log("finished");
			exec("npm run postbuild", (error, stdout, stderr) => {
				if (error) {
					log(`error: ${error.message}`);
					return;
				}
				if (stderr) {
					log(`stderr: ${stderr}`);
					return;
				}
				log(chalk.yellow(`${stdout}`));
				ready = false;
			});

		} else {
			setTimeout(checkEnd, end_timeout, path, stat);
		}
	});
}

//Express Server

app.use('/bin/', createProxyMiddleware({target: 'http://localhost:4502/bin/', changeOrigin: true}));
app.use('/etc.clientlibs/', createProxyMiddleware({
	target: 'http://localhost:4502/etc.clientlibs/',
	changeOrigin: true
}));
app.use('/', express.static(__dirname + '/elements'));


app.get('/', (req, res) => {
	res.sendFile('elements/index.html', {root: __dirname});
});


app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
	open(`http://localhost:${port}`);
})

