// Globs for entrypoint files (in the order they need to be required)
const target = 'es5';
const jsEntrypoints = [
  '../elements/web-components.js'
];
const cssEntrypoints = ['**/*.css'];
const entrypoints = [...jsEntrypoints, ...cssEntrypoints];

module.exports = {
  jsEntrypoints,
  cssEntrypoints,
  entrypoints
};
