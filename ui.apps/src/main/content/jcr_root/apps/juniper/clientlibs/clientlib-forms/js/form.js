var state_cntry_code_list = ['US', 'CA'];

var state_cntry_list = ['United States', 'Canada'];

var optinSubscriptionValue = undefined;

var country = "";

var isKnownVisitor = false;

var SHOWN_NOT_TICKED = "Shown Not Ticked";

var NOT_SHOWN = "Not Shown";

var ACCEPTED = "Accepted";

var excl_forms = ["http://forms.juniper.net/2018-Opt-in-LP-Wave2-PrefPage"];

var submitFlag = true;

let resetSearchTimeout;

let searchKey = '';

var optin_txt_jp_en = "Please <strong>uncheck the box below</strong> if you do not agree that Juniper Networks, Inc. may email and call you with information regarding our products and services, as well as event invitations or other tailored information.";

var optin_txt_jp_jp = "製品とサービスに関する情報やイベントへの招待、ニーズに応じた情報について、ジュニパーネットワークスからメールや電話でご連絡させていただくことに同意しない場合は、<strong>以下のチェック ボックスをオフにしてください。</strong>";

var optin_txt_default = "";

var noZipCountries = ["AS", "AI", "AW", "BS", "BZ", "BM", "GY", "JM", "MS", "PA", "KN", "LC", "SR", "TO", "TT", "TC", "UM", "CK", "FJ", "HK", "KI", "NR", "NU", "SB", "TK", "TV", "VU", "AO", "BJ", "BW", "BF", "BI", "CF", "KM", "CG", "CI", "DJ", "GQ", "ER", "GM", "GH", "GN", "KE", "MW", "ML", "MR", "MU", "QA", "RW", "ST", "SA", "SC", "SL", "SO", "TZ", "UG", "YE", "ZW"];

var showStateCountries = ["US", "CA", "AU", "IN", "BR", "MX"];

var optin_cntry_code_list = ["AX", "AD", "AR", "AU", "AT", "BA", "BH", "BE", "BO", "BR", "BG", "CM", "CA", "CL", "CN", "CO", "CR", "CI", "EE",
    "HR", "CY", "CZ", "DK", "FO", "FI", "FR", "GA", "GE", "DE", "GI", "GW", "GR", "GL", "GG", "HK", "HU", "IS", "ID", "IE", "IM", "IL", "IT", "JP",
    "JE", "KZ", "KR", "KG", "LV", "LI", "LT", "LU", "MO", "MG", "MY", "MT", "MC", "ME", "MA", "NL", "AN", "NZ", "NO", "OM", "PK", "PE", "PH", "PL", "VA",
    "PO", "PT", "QA", "RO", "RU", "SA", "RS", "SG", "SK", "SI", "ZA", "ES", "SE", "CH", "TN", "TR", "UA", "AE", "UK", "UY", "VN"
];

var optin_cntry_list = ["Aland Islands", , "Andorra", , "Argentina", , "Australia", , "Austria", , "Bahrain", , "Belgium", , "Bolivia", , "Bosnia and Herzegovina", , "Brazil", , "Bulgaria", , "Cameroon", , "Canada", , "Chile", , "China", , "Colombia", , "Costa Rica", , "Cote D'Ivoire", , "Croatia", , "Cyprus", , "Czech Republic", , "Denmark", , "Estonia", , "Faroe Islands", , "Finland", , "France", , "Gabon", , "Georgia", , "Germany", , "Gibraltar", , "Greece", , "Greenland", , "Guernsey", , "Guinea-bissau", "Hong Kong", , "Hungary", , "Iceland", , "Indonesia", , "Ireland", , "Isle of Man", , "Israel", , "Italy", , "Japan", , "Jersey", , "Kazakhstan", , "Korea, South", , "Kyrgyzstan", , "Latvia", , "Liechtenstein", , "Lithuania", , "Luxembourg", , "Macao", , "Madagascar", , "Malaysia", , "Malta", , "Monaco", , "Montenegro", , "Morocco", , "Netherlands", , "Netherlands Antilles", , "New Zealand", , "Norway", , "Oman", , "Pakistan", , "Peru", , "Philippines", , "Poland", , "Portugal", , "Qatar", , "Romania", , "Russia", , "Saudi Arabia", , "Serbia", , "Singapore", , "Slovakia", , "Slovenia", , "South Africa", , "Spain", , "Sweden", , "Switzerland", , "Tunisia", , "Turkey", , "Ukraine", , "United Arab Emirates", , "United Kingdom", , "Uruguay", , "Vatican", , "Viet Nam", , "Vietnam"];

var countryCodeMap = {
    "United States": "US",
    "Canada": "CA",
    "Afghanistan": "AF",
    "Aland Islands": "AX",
    "Albania": "AL",
    "Algeria": "DZ",
    "American Samoa": "AS",
    "Andorra": "AD",
    "Angola": "AO",
    "Anguilla": "AI",
    "Antarctica": "AQ",
    "Antigua and Barbuda": "AG",
    "Argentina": "AR",
    "Armenia": "AM",
    "Aruba": "AW",
    "Australia": "AU",
    "Austria": "AT",
    "Azerbaijan": "AZ",
    "Bahamas": "BS",
    "Bahrain": "BH",
    "Bangladesh": "BD",
    "Barbados": "BB",
    "Belarus": "BY",
    "Belgium": "BE",
    "Belize": "BZ",
    "Benin": "BJ",
    "Bermuda": "BM",
    "Bhutan": "BT",
    "Bolivia": "BO",
    "Bosnia and Herzegovina": "BA",
    "Botswana": "BW",
    "Bouvet Island": "BV",
    "Brazil": "BR",
    "British Indian Ocean Territory": "IO",
    "Brunei Darussalam": "BN",
    "Bulgaria": "BG",
    "Burkina Faso": "BF",
    "Burundi": "BI",
    "Cambodia": "KH",
    "Cameroon": "CM",
    "Cape Verde": "CV",
    "Cayman Islands": "KY",
    "Central African Republic": "CF",
    "Chad": "TD",
    "Chile": "CL",
    "China": "CN",
    "Christmas Island": "CX",
    "Cocos (Keeling) Islands": "CC",
    "Colombia": "CO",
    "Comoros": "KM",
    "Congo": "CG",
    "Congo, The Democratic Republic": "CD",
    "Cook Islands": "CK",
    "Costa Rica": "CR",
    "Cote D'Ivoire": "CI",
    "Croatia": "HR",
    "Cuba": "CU",
    "Cyprus": "CY",
    "Czech Rep.": "CZ",
    "Denmark": "DK",
    "Djibouti": "DJ",
    "Dominica": "DM",
    "Dominican Republic": "DO",
    "Ecuador": "EC",
    "Egypt": "EG",
    "El Salvador": "SV",
    "Equatorial Guinea": "GQ",
    "Eritrea": "ER",
    "Estonia": "EE",
    "Ethiopia": "ET",
    "Falkland Islands (Malvinas)": "FK",
    "Faroe Islands": "FO",
    "Fiji": "FJ",
    "Finland": "FI",
    "France": "FR",
    "French Guiana": "GF",
    "French Polynesia": "PF",
    "French Southern Territories": "TF",
    "Gabon": "GA",
    "Gambia": "GM",
    "Georgia, Republic of": "GE",
    "Germany": "DE",
    "Ghana": "GH",
    "Gibraltar": "GI",
    "Greece": "GR",
    "Greenland": "GL",
    "Grenada": "GD",
    "Guadeloupe": "GP",
    "Guam": "GU",
    "Guatemala": "GT",
    "Guernsey": "GG",
    "Guinea": "GN",
    "Guinea-bissau": "GW",
    "Guyana": "GY",
    "Haiti": "HT",
    "Heard Island &amp; McDonald Islands": "HM",
    "Honduras": "HN",
    "Hong Kong": "HK",
    "Hungary": "HU",
    "Iceland": "IS",
    "India": "IN",
    "Indonesia": "ID",
    "Iran": "IR",
    "Iraq": "IQ",
    "Ireland": "IE",
    "Isle of Man": "IM",
    "Israel": "IL",
    "Italy": "IT",
    "Ivory Coast": "CI",
    "Jamaica": "JM",
    "Japan": "JP",
    "Jersey": "JE",
    "Jordan": "JO",
    "Kazakhstan": "KZ",
    "Kenya": "KE",
    "Kiribati": "KI",
    "Korea, North": "KP",
    "Korea, South": "KR",
    "Kuwait": "KW",
    "Kyrgyzstan": "KG",
    "Lao People's Democratic Republic": "LA",
    "Latvia": "LV",
    "Lebanon": "LB",
    "Lesotho": "LS",
    "Liberia": "LR",
    "Libyan Arab Jamahiriya": "LY",
    "Liechtenstein": "LI",
    "Lithuania": "LT",
    "Luxembourg": "LU",
    "Macau": "MO",
    "Macedonia": "MK",
    "Madagascar": "MG",
    "Malawi": "MW",
    "Malaysia": "MY",
    "Maldives": "MV",
    "Mali": "ML",
    "Malta": "MT",
    "Marshall Islands": "MH",
    "Martinique": "MQ",
    "Mauritania": "MR",
    "Mauritius": "MU",
    "Mayotte": "YT",
    "Mexico": "MX",
    "Micronesia": "FM",
    "Moldova": "MD",
    "Monaco": "MC",
    "Mongolia": "MN",
    "Montenegro": "ME",
    "Montserrat": "MS",
    "Morocco": "MA",
    "Mozambique": "MZ",
    "Myanmar": "MM",
    "Namibia": "NA",
    "Nauru": "NR",
    "Nepal": "NP",
    "Netherlands Antilles": "AN",
    "Netherlands": "NL",
    "New Caledonia": "NC",
    "New Zealand": "NZ",
    "Nicaragua": "NI",
    "Niger": "NE",
    "Nigeria": "NG",
    "Niue": "NU",
    "Norfolk Island": "NF",
    "Northern Mariana Islands": "MP",
    "Norway": "NO",
    "Oman": "OM",
    "Pakistan": "PK",
    "Palau": "PW",
    "Palestinian Territory, Occupied": "PS",
    "Panama": "PA",
    "Papua New Guinea": "PG",
    "Paraguay": "PY",
    "Peru": "PE",
    "Philippines": "PH",
    "Pitcairn": "PN",
    "Poland": "PL",
    "Portugal": "PT",
    "Puerto Rico": "PR",
    "Qatar": "QA",
    "Reunion": "RE",
    "Romania": "RO",
    "Russian Federation": "RU",
    "Rwanda": "RW",
    "Saint Helena": "SH",
    "Saint Kitts &amp; Nevis": "KN",
    "Saint Lucia": "LC",
    "Saint Pierre and Miquelon": "PM",
    "Saint Vincent and The Grenadines": "VC",
    "Samoa": "WS",
    "San Marino": "SM",
    "Sao Tome and Principe": "ST",
    "Saudi Arabia": "SA",
    "Senegal": "SN",
    "Serbia": "RS",
    "Seychelles": "SC",
    "Sierra Leone": "SL",
    "Singapore": "SG",
    "Slovakia": "SK",
    "Slovenia": "SI",
    "Solomon Islands": "SB",
    "Somalia": "SO",
    "South Africa": "ZA",
    "S Georgia &amp; The S Sandwich Islands": "GS",
    "Spain": "ES",
    "Sri Lanka": "LK",
    "Sudan": "SD",
    "Suriname": "SR",
    "Svalbard and Jan Mayen": "SJ",
    "Swaziland": "SZ",
    "Sweden": "SE",
    "Switzerland": "CH",
    "Syrian Arab Republic": "SY",
    "Taiwan": "TW",
    "Tajikistan": "TJ",
    "Tanzania": "TZ",
    "Thailand": "TH",
    "Timor-Leste": "TL",
    "Togo": "TG",
    "Tokelau": "TK",
    "Tonga": "TO",
    "Trinidad and Tobago": "TT",
    "Tunisia": "TN",
    "Turkey": "TR",
    "Turkmenistan": "TM",
    "Turks and Caicos Islands": "TC",
    "Tuvalu": "TV",
    "Uganda": "UG",
    "Ukraine": "UA",
    "United Arab Emirates": "AE",
    "United Kingdom": "UK",
    "US Minor Outlying Islands": "UM",
    "Uruguay": "UY",
    "Uzbekistan": "UZ",
    "Vanuatu": "VU",
    "Vatican": "VA",
    "Venezuela": "VE",
    "Vietnam": "VN",
    "Virgin Islands, British": "VG",
    "Virgin Islands, U.S.": "VI",
    "Wallis and Futuna": "WF",
    "Western Sahara": "EH",
    "Yemen": "YE",
    "Zambia": "ZM",
    "Zimbabwe": "ZW"
};

function printStateMenu(country) {

    if ($('#Country').length > 0) {
        $('#Country').val(country);
    }

    var stateSelect = '';
    var stateDrop = document.getElementById("stateProv");
    var stateDropContainer = document.getElementById("C_State_Prov");
    $('#C_State_Prov').attr('required', false);
    $('#C_State_Prov').hide();

    if (country == 'US') {
        stateSelect =
            '<option value="">State</option>' +
            '<option value="AK">AK-Alaska</option>' +
            '<option value="AL">AL-Alabama</option>' +
            '<option value="AS">AS-American Samoa</option>' +
            '<option value="AR">AR-Arkansas</option>' +
            '<option value="AZ">AZ-Arizona</option>' +
            '<option value="CA">CA-California</option>' +
            '<option value="CO">CO-Colorado</option>' +
            '<option value="CT">CT-Connecticut</option>' +
            '<option value="DC">DC-District of Columbia</option>' +
            '<option value="DE">DE-Delaware</option>' +
            '<option value="FL">FL-Florida</option>' +
            '<option value="GA">GA-Georgia</option>' +
            '<option value="HI">HI-Hawaii</option>' +
            '<option value="IA">IA-Iowa</option>' +
            '<option value="ID">ID-Idaho</option>' +
            '<option value="IL">IL-Illinois</option>' +
            '<option value="IN">IN-Indiana</option>' +
            '<option value="KS">KS-Kansas</option>' +
            '<option value="KY">KY-Kentucky</option>' +
            '<option value="LA">LA-Louisiana</option>' +
            '<option value="MA">MA-Massachusetts</option>' +
            '<option value="MD">MD-Maryland</option>' +
            '<option value="ME">ME-Maine</option>' +
            '<option value="MI">MI-Michigan</option>' +
            '<option value="MN">MN-Minnesota</option>' +
            '<option value="MO">MO-Missouri</option>' +
            '<option value="MS">MS-Mississippi</option>' +
            '<option value="MT">MT-Montana</option>' +
            '<option value="NC">NC-North Carolina</option>' +
            '<option value="ND">ND-North Dakota</option>' +
            '<option value="NE">NE-Nebraska</option>' +
            '<option value="NH">NH-New Hampshire</option>' +
            '<option value="NJ">NJ-New Jersey</option>' +
            '<option value="NM">NM-New Mexico</option>' +
            '<option value="NV">NV-Nevada</option>' +
            '<option value="NY">NY-New York</option>' +
            '<option value="OH">OH-Ohio</option>' +
            '<option value="OK">OK-Oklahoma</option>' +
            '<option value="OR">OR-Oregon</option>' +
            '<option value="PA">PA-Pennsylvania</option>' +
            '<option value="RI">RI-Rhode Island</option>' +
            '<option value="SC">SC-South Carolina</option>' +
            '<option value="SD">SD-South Dakota</option>' +
            '<option value="TN">TN-Tennessee</option>' +
            '<option value="TX">TX-Texas</option>' +
            '<option value="UT">UT-Utah</option>' +
            '<option value="VA">VA-Virginia</option>' +
            '<option value="VT">VT-Vermont</option>' +
            '<option value="WA">WA-Washington</option>' +
            '<option value="WI">WI-Wisconsin</option>' +
            '<option value="WV">WV-West Virginia</option>' +
            '<option value="WY">WY-Wyoming</option>';
        showProvince(stateSelect);

    } else if (country == 'CA') {
        stateSelect =
            '<option value="">Province</option>' +
            '<option value="AB">AB-Alberta</option>' +
            '<option value="BC">BC-British Columbia</option>' +
            '<option value="MB">MB-Manitoba</option>' +
            '<option value="NB">NB-New Brunswick</option>' +
            '<option value="NL">NL-Newfoundland and Labrador</option>' +
            '<option value="NT">NT-Northwest Territories</option>' +
            '<option value="NS">NS-Nova Scotia</option>' +
            '<option value="NU">NU-Nunavut</option>' +
            '<option value="ON">ON-Ontario</option>' +
            '<option value="PE">PE-Prince Edward Island</option>' +
            '<option value="QC">QC-Quebec</option>' +
            '<option value="SK">SK-Saskatchewan</option>' +
            '<option value="YT">YT-Yukon</option>';
        showProvince(stateSelect);


    } else if (country == 'IN') {
        stateSelect =
            '<option value="">State*</option>' +
            '<option value="AP">AP-Andhra Pradesh</option>' +
            '<option value="AR">AR-Arunachal Pradesh</option>' +
            '<option value="ASM">AS-Assam</option>' +
            '<option value="BHR">BR-Bihar</option>' +
            '<option value="CT">CT-Chhattisgarh</option>' +
            '<option value="GOA">GA-Goa</option>' +
            '<option value="GUJ">GJ-Gujarat</option>' +
            '<option value="HAR">HR-Haryana</option>' +
            '<option value="HP">HP-Himachal Pradesh</option>' +
            '<option value="J&amp;K">JK-Jammu and Kashmir</option>' +
            '<option value="JH">JH-Jharkhand</option>' +
            '<option value="KAR">KA-Karnataka</option>' +
            '<option value="KER">KL-Kerala</option>' +
            '<option value="MP">MP-Madhya Pradesh</option>' +
            '<option value="MH">MH-Maharashtra</option>' +
            '<option value="MN">MN-Manipur</option>' +
            '<option value="MEG">ML-Meghalaya</option>' +
            '<option value="MZ">MZ-Mizoram</option>' +
            '<option value="NL">NL-Nagaland</option>' +
            '<option value="ORI">OR-Odisha</option>' +
            '<option value="PUJ">PB-Punjab</option>' +
            '<option value="RAJ">RJ-Rajasthan</option>' +
            '<option value="SK">SK-Sikkim</option>' +
            '<option value="TN">TN-Tamil Nadu</option>' +
            '<option value="TG">TG-Telangana</option>' +
            '<option value="UP">UP-Uttar Pradesh</option>' +
            '<option value="UT">UT-Uttarakhand</option>' +
            '<option value="WB">WB-West Bengal</option>' +
            '<option value="AN">AN-Andoman and Nicobar Islands</option>' +
            '<option value="CH">CH-Chandigarh</option>' +
            '<option value="DN">DN-Dadra and Nagar Haveli</option>' +
            '<option value="D&amp;D">DD-Daman and Diu</option>' +
            '<option value="DEL">DL-Delhi</option>' +
            '<option value="LD">LD-Lakshadweep</option>' +
            '<option value="PON">PY-Puducherry</option>';
        showProvince(stateSelect);

    } else if (country == 'AU') {
        stateSelect =
            '<option value="">Province*</option>' +
            '<option value="NSW">NSW-New South Wales</option>' +
            '<option value="QLD">QLD-Queensland</option>' +
            '<option value="SA">SA-South Australia</option>' +
            '<option value="TAS">TAS-Tasmania</option>' +
            '<option value="VIC">VIC-Victoria</option>' +
            '<option value="WA">WA-Western Australia</option>' +
            '<option value="ACT">ACT-Australian Capital Territory</option>' +
            '<option value="NT">NT-Northern Territory</option>';
        showProvince(stateSelect);

    } else if (country == 'BR' || country == 'Brazil') {
        stateSelect =
            '<option value="">State</option>' +
            '<option value="DF">Distrito Federal</option>' +
            '<option value="ES">Esparito Santo</option>' +
            '<option value="MG">Mato Grosso</option>' +
            '<option value="PB">Paraaba</option>' +
            '<option value="RJ">Rio de Janeiro</option>' +
            '<option value="RR">Rondania</option>' +
            '<option value="RS">Roraima</option>' +
            '<option value="SE">Sergipe</option>';
        showProvince(stateSelect);


    } else if (country == 'MX' || country == 'Mexico') {
        stateSelect =
            '<option value="">State</option>' +
            '<option value="AGS.">Aguascalientes</option>' +
            '<option value="B.C.">Baja California</option>' +
            '<option value="B.C.S.">Baja California Sur</option>' +
            '<option value="CAM.">Campeche</option>' +
            '<option value="CHIH.">Chiapas</option>' +
            '<option value="CHIS.">Chihuahua</option>' +
            '<option value="COAH.">Coahuila</option>' +
            '<option value="COL.">Colima</option>' +
            '<option value="DGO.">Durango</option>' +
            '<option value="GRO.">Guanajuato</option>' +
            '<option value="GTO.">Guerrero</option>' +
            '<option value="HGO.">Hidalgo</option>' +
            '<option value="JAL.">Jalisco</option>' +
            '<option value="MOR.">M&eacute;xico</option>' +
            '<option value="MEX.">MI-Michoac&aacute;n</option>' +
            '<option value="MICH.">MO-Morelos</option>' +
            '<option value="N.L.">NA-Nayarit</option>' +
            '<option value="NAY.">NL-Nuevo Le&oacute;n</option>' +
            '<option value="OAX.">OA-Oaxaca</option>' +
            '<option value="PUE.">PU-Puebla</option>' +
            '<option value="Q.R.">QT-Quer&eacute;taro</option>' +
            '<option value="QRO.">QR-Quintana Roo</option>' +
            '<option value="S.L.P.">SL-San Luis Potos&iacute;</option>' +
            '<option value="SIN.">SI-Sinaloa</option>' +
            '<option value="SON.">SO-Sonora</option>' +
            '<option value="TAB.">TB-Tabasco</option>' +
            '<option value="TAMS.">TM-Tamaulipas</option>' +
            '<option value="TLAX.">TL-Tlaxcala</option>' +
            '<option value="VER.">VE-Veracruz</option>' +
            '<option value="YUC.">YU-Yucat&aacute;n</option>' +
            '<option value="ZA">ZA-Zacatecas</option>';
        showProvince(stateSelect);


    } else {
        stateSelect = '<div><select class="select-alt select-campaign-form icon-select-down select-97 placeholdersjs disabled" name="C_State_Prov" id="stateProv" disabled="disabled"><option value="Other">State/Province</option></select></div>';
        $('#C_State_Prov').attr('required', false);
        $('#C_State_Prov').hide();
    }
    if (stateDropContainer != undefined) {
        stateDropContainer.innerHTML = stateSelect;
        if ((country == 'AU' || country == 'Australia') || (country == 'IN' || country == 'India')) {
            $(stateDropContainer).parent('div').addClass('validate-presence');
        }
    }
}

function scrollSelect(elem) {
    let activeElement = $(elem).find(".same-as-selected");
    let dropdownElement = $(elem).find(".select-items");
    if (activeElement[0]) {
        let activeTop =
            activeElement[0].offsetTop + activeElement[0].offsetHeight + 15;
        let scrollContainerHeight = dropdownElement[0].offsetHeight;
        if (activeTop > scrollContainerHeight) {
            dropdownElement[0].scrollTop = activeTop - scrollContainerHeight;
        } else {
            dropdownElement[0].scrollTop = 0;
        }
    }
}


function showProvince(stateSelect) {

    var stateDropContainer = document.getElementById("C_State_Prov");
    stateDropContainer.innerHTML = stateSelect;
    $('#C_State_Prov').show();
    $('#C_Zip_Postal').show();


    var x, i, j, l, ll, selElmnt, a, b, c;
    x = document.getElementsByClassName("C_State_Prov");
    $('.C_State_Prov').find('.select-selected').remove();
    $('.C_State_Prov').find('.select-items').remove();
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.setAttribute("contenteditable", "true");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                e.stopPropagation();
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();

            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            scrollSelect($('.C_State_Prov'));
        });

        a.addEventListener("keydown", function(e) {
            e.preventDefault();
            if (e.keyCode == 40) {
                var $this = $('.C_State_Prov .same-as-selected');
                if ($this.next().length) {
                    $this.next().addClass('same-as-selected');
                    $this.removeClass('same-as-selected');
                    $('.C_State_Prov .select-selected')[0].innerHTML = $this.next().text();
                    scrollSelect($('.C_State_Prov'));
                }
                return false;
            } else if (e.keyCode == 38) {
                var $this = $('.C_State_Prov .same-as-selected');
                if ($this.prev().length) {
                    $this.prev().addClass('same-as-selected');
                    $this.removeClass('same-as-selected');
                    $('.C_State_Prov .select-selected')[0].innerHTML = $this.prev().text();
                    scrollSelect($('.C_State_Prov'));
                }
                return false;
            } else if (e.keyCode == 13) {
                var i, s, h, sl;
                s = this.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == $('.C_State_Prov .same-as-selected')[0].innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = $('.C_State_Prov .same-as-selected')[0].innerHTML;
                        break;
                    }
                }
                h.click();
            } else if (e.keyCode >= 48 && e.keyCode <= 90) {
                $('.C_State_Prov .select-items div').each(function(index) {
                    if ($(this).hasClass('same-as-selected')) {
                        $(this).removeClass('same-as-selected');
                        return false;
                    }
                });

                if (resetSearchTimeout) {
                    clearTimeout(resetSearchTimeout);
                }

                resetSearchTimeout = setTimeout(() => {
                    searchKey = '';
                }, 1500);

                searchKey += String.fromCharCode(e.keyCode);

                $('.C_State_Prov .select-items div').each(function(index) {
                    if ($(this).text().toUpperCase().substr(0, searchKey.length) === searchKey) {
                        $(this).addClass('same-as-selected');
                        $('.C_State_Prov .select-selected')[0].innerHTML = $(this).text();
                        scrollSelect($('.C_State_Prov'));
                        return false;
                    }
                });
                return false;
            } else {
                return false;
            }
        });
    }
}

function updateSubscription(inputCountry) {

    var elqFormURL = $(location).attr('href');

    if (jQuery.inArray(elqFormURL, excl_forms) == -1) {
        if ($('#opt_in').is(':checked')) {
            $('#opt_in').trigger('click');
            $('input#opt_in').val(ACCEPTED);
            $('input[name="opt_in"]').val(ACCEPTED);
        } else {
            $('input#opt_in').val(SHOWN_NOT_TICKED);
            $('input[name="opt_in"]').val(SHOWN_NOT_TICKED);
            $('input[name="opt_in"]').val("Declined");
        }

        if (optinSubscriptionValue == null || optinSubscriptionValue == undefined || optinSubscriptionValue == '') {

            if (jQuery.inArray(inputCountry, optin_cntry_code_list) !== -1 || jQuery.inArray(inputCountry, optin_cntry_list) !== -1) {
                $('input#opt-in-opt-out').val(SHOWN_NOT_TICKED);
                $('#privacytext').hide();
                $('#opt-in-out').show();
            } else {
                $('input#opt-in-opt-out').val(NOT_SHOWN);
                $('#opt-in-out').hide();
                $('#privacytext').show();
            }
        } else if (optinSubscriptionValue == 'Double Opt In' || optinSubscriptionValue == 'Explicit Opt In' || optinSubscriptionValue == 'Pending Opt IN') {
            $('input#opt-in-opt-out').val(NOT_SHOWN);
            $('#opt-in-out').hide();
            $('#privacytext').show();
        } else if (optinSubscriptionValue == 'Explicit Opt Out' || optinSubscriptionValue == 'System Opt Out' || optinSubscriptionValue == 'Implicit Opt In') {

            if (jQuery.inArray(inputCountry, optin_cntry_code_list) !== -1 || jQuery.inArray(inputCountry, optin_cntry_list) !== -1) {
                $('input#opt-in-opt-out').val(NOT_SHOWN);
                $('#privacytext').hide();
                $('#opt-in-out').show();
            } else {
                $('input#opt-in-opt-out').val(SHOWN_NOT_TICKED);
                $('#opt-in-out').show();
                $('#privacytext').hide();
            }
        } else {
            $('input#opt-in-opt-out').val(NOT_SHOWN);
            $('#opt-in-out').hide();
            $('#privacytext').show();
        }
    }
}

function eloquaContactLookup(email) {
    if (email != "") {
        isKnownVisitor = true;
        elqTracker.getData({
            key: "ead5a049-7b99-4dd8-807d-46eb7b7b791f",
            lookup: "<C_EmailAddress>" + email + "</C_EmailAddress>",
            success: function() {
                if (typeof GetElqContentPersonalizationValue == "function") {

                    if (GetElqContentPersonalizationValue("C_EmailAddress") === email) {
                        optinSubscriptionValue = GetElqContentPersonalizationValue("C_Subscription_Current_Status1");
                        country = GetElqContentPersonalizationValue("C_Country");
                        updateSubscription(country);
                        setCountry(country);
                        loadFields();
                        setState(GetElqContentPersonalizationValue("C_State_Prov"));
                    } else {
                        reset();
                        setEmail(email);
                    }

                } else {
                    reset();
                    setEmail(email);
                }
            },
            error: function() {
                reset();
                setEmail(email);
            }
        });
    } else {
        reset();
    }

}

function eloquaContactLookupByEmail(email) {
    if (email != "") {
        isKnownVisitor = true;
        elqTracker.getData({
            key: "ead5a049-7b99-4dd8-807d-46eb7b7b791f",
            lookup: "<C_EmailAddress>" + email + "</C_EmailAddress>",
            success: function() {
                if (typeof GetElqContentPersonalizationValue == "function") {
                    if (GetElqContentPersonalizationValue("C_EmailAddress") === email) {
                        optinSubscriptionValue = GetElqContentPersonalizationValue("C_Subscription_Current_Status1");
                        country = GetElqContentPersonalizationValue("C_Country");
                        updateSubscription(country);
                        setCountry(country);
                    } else {
                        reset();
                        setEmail(email);
                    }

                } else {
                    reset();
                    setEmail(email);
                }
            },
            error: function() {
                reset();
                setEmail(email);
            }
        });
    } else {
        reset();
    }

}

function reset() {
    optinSubscriptionValue = undefined;
    country = "";
    updateSubscription(country);
    setCountry(country);
}

function setContactFields() {
    $('#C_FirstName').val(GetElqContentPersonalizationValue("C_FirstName"));
    $('#C_LastName').val(GetElqContentPersonalizationValue("C_LastName"));
    $('#C_Company').val(GetElqContentPersonalizationValue("C_Company"));
    $('#C_Zip_Postal').val(GetElqContentPersonalizationValue("C_Zip_Postal"));
    $('#C_Title').val(GetElqContentPersonalizationValue("C_Title"));
    $('#C_BusPhone').val(GetElqContentPersonalizationValue("C_BusPhone"));
    setState(GetElqContentPersonalizationValue("C_State_Prov"));
}

function toggleState(inputCountry) {
    if (($.inArray(inputCountry, state_cntry_code_list) >= 0) || ($.inArray(inputCountry, state_cntry_list) >= 0)) {
        $('#stateProvContainer').show();
    } else {
        $('#stateProvContainer').hide();
    }
}

function setCountry(country) {
    var countryNode = $('select[name="C_Country"]');
    var countryNameNode = $('input[name="country"]');
    var countryCode = getCountryCode(country);
    if (countryCode == null || countryCode == '') {
        var hasOption = $('#C_Country option[value="' + country + '"]');
        if (hasOption.length == 0) {
            countryNode.val('');
        } else {
            countryNode.val(country);
        }
    } else {
        countryNode.val(countryCode);
    }
    if (countryNameNode != null && countryNameNode.length > 0) {
        countryNameNode.val(countryNode.val());
    }
    $('.C_Country .select-selected').text($("#C_Country option:selected").text());
    $('.C_Country .select-items div').each(function(index) {
        if ($(this).text() == $("#C_Country option:selected").text()) {
            $(this).addClass('same-as-selected');
        }
    });

    countryNode.trigger('change');
}

function setState(state) {
    var stateNode = $('select[name="C_State_Prov"]');
    if (stateNode.length > 0) {
        if (state != null && state != '') {
            var hasOption = $('#C_State_Prov option[value="' + state + '"]');
            if (hasOption.length == 0) {
                stateNode.val('');
            } else {
                stateNode.val(state);
            }
        } else {
            stateNode.val('');
        }
        $(".C_State_Prov .select-selected").text($("#C_State_Prov option:selected").text());
        $('.C_State_Prov .select-items div').each(function(index) {
            if ($(this).text() == $("#C_State_Prov option:selected").text()) {
                $(this).addClass('same-as-selected');
            }
        });
    }
}

function setEmail(email) {
    if ($('#C_EmailAddress').length > 0) {
        $('#C_EmailAddress').val(email);
    } else {
        if ($('input[name="C_EmailAddress"]').length > 0) {
            $('input[name="C_EmailAddress"]').val(email);
        }
    }
}

function closeAllSelect(elmnt) {
    var x, y, i, xl, yl, cs1, cs2, arrNo = [];
    cs1 = document.getElementById('C_Country');
    cs2 = document.getElementById('C_State_Prov');


    if ($('.C_Country .select-arrow-active').length) {
        for (i = 0; i < cs1.length; i++) {
            if ($('.C_Country .same-as-selected').length) {
                if (cs1.options[i].innerHTML == $('.C_Country .same-as-selected')[0].innerHTML) {
                    cs1.selectedIndex = i;
                    $('#C_Country').trigger("change");
                    break;
                }
            }
        }
    }

    if ($('.C_State_Prov .select-arrow-active').length) {
        for (i = 0; i < cs2.length; i++) {
            if ($('.C_State_Prov .same-as-selected').length) {
                if (cs2.options[i].innerHTML == $('.C_State_Prov .same-as-selected')[0].innerHTML) {
                    cs2.selectedIndex = i;
                    break;
                }
            }

        }
    }

    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

function showHideState() {
    var selCountry = $('select[name="C_Country"]').val();
    if (selCountry && selCountry != '' && showStateCountries.indexOf(selCountry) != -1) {
        $('#stateProvContainer').show();
        $('select[name="C_State_Prov"]').parent().show();
        if (selCountry == 'IN' || selCountry == 'AU') {
            if ($('#label_state').length > 0) {
                $('#label_state').text('State/Province*');
            }
            $('#stateProv').parent('div').addClass('validate-presence');
            $('#C_State_Prov').parent('div').addClass('validate-presence');
        } else {
            if ($('#label_state').length > 0) {
                $('#label_state').text('State/Province');
            }
            $('#stateProv').parent('div').removeClass('validate-presence');
            $('#C_State_Prov').parent('div').removeClass('validate-presence');
            if ($('#C_State_Prov').parent('div').hasClass('is-invalid') || $('#C_State_Prov').hasClass('is-invalid')) {
                $('#C_State_Prov').parent('div').removeClass('is-invalid');
                $('#C_State_Prov').removeClass('is-invalid');
            }
            if ($('#stateProv').parent('div').hasClass('is-invalid') || $('#stateProv').hasClass('is-invalid')) {
                $('#stateProv').parent('div').removeClass('is-invalid');
                $('#stateProv').removeClass('is-invalid');
            }
        }
    } else {
        $('#stateProvContainer').hide();
        $('select[name="C_State_Prov"]').parent().hide();
        $('#stateProv').parent('div').removeClass('validate-presence');
        $('#C_State_Prov').parent('div').removeClass('validate-presence');
        if ($('#C_State_Prov').parent('div').hasClass('is-invalid') || $('#C_State_Prov').hasClass('is-invalid')) {
            $('#C_State_Prov').parent('div').removeClass('is-invalid');
            $('#C_State_Prov').removeClass('is-invalid');
        }
        if ($('#stateProv').parent('div').hasClass('is-invalid') || $('#stateProv').hasClass('is-invalid')) {
            $('#stateProv').parent('div').removeClass('is-invalid');
            $('#stateProv').removeClass('is-invalid');
        }
    }
}


function showHideZip() {
    var selCountry = $('select[name="C_Country"]').val();
    if (selCountry == '' || selCountry == undefined) {
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').hide().parent().removeClass('validate-zip');
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').hide().parent().removeClass('validate-presence');
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().hide();
    } else if (selCountry && selCountry != '' && noZipCountries.indexOf(selCountry) != -1) {
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').hide().parent().removeClass('validate-zip');
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').hide().parent().removeClass('validate-presence');
        $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().hide();
        if ($('#C_Zip_Postal').attr('placeholder') != undefined && $('#C_Zip_Postal').attr('placeholder') != '') {
            var placeholder = $('#C_Zip_Postal').attr('placeholder');
            if (placeholder.indexOf('*') !== -1) {
                $('#C_Zip_Postal').attr('placeholder', placeholder.substring(0, placeholder.indexOf("*")).trim());
            }
        }
    } else {
        $('#C_Zip_Postal').val("");
        if (selCountry == 'DE' || selCountry == 'UK') {
            if ($('#label_zip').length > 0) {
                $('#label_zip').text('Zip/Postal Code*');
            }
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().show();
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').show().parent().addClass('validate-zip');
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').show().parent().addClass('validate-presence');
            if ($('#C_Zip_Postal').attr('placeholder') != undefined && $('#C_Zip_Postal').attr('placeholder') != '') {
                var placeholderVal = $('#C_Zip_Postal').attr('placeholder');
                if (placeholderVal.indexOf('*') === -1) {
                    placeholderVal = placeholderVal + " *";
                    $('#C_Zip_Postal').attr('placeholder', placeholderVal);
                }
            }
        } else {
            if ($('#label_zip').length > 0) {
                $('#label_zip').text('Zip/Postal Code');
            }
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().show();
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').show();
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().removeClass('validate-zip');
            $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').parent().removeClass('validate-presence');
            if ($('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').hasClass('is-invalid')) {
                $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').removeClass('is-invalid');
                $('label[for="C_Zip_Postal"], input[name="C_Zip_Postal"]').siblings('.invalidMsg').hide();
            }
            if ($('#C_Zip_Postal').attr('placeholder') != undefined && $('#C_Zip_Postal').attr('placeholder') != '') {
                var placeholder = $('#C_Zip_Postal').attr('placeholder');
                if (placeholder.indexOf('*') !== -1) {
                    $('#C_Zip_Postal').attr('placeholder', placeholder.substring(0, placeholder.indexOf("*")).trim());
                }
            }
        }
        $('#C_Zip_Postal').prev(".label").show();
    }
}


function getCountryName(countryCode) {
    var cName = '';
    $('select[name="C_Country"] option').each(function(i, itm) {
        if ($(this).attr('value') == countryCode) {
            cName = $(this).html();
        }
    });
    return cName;
}

function getCountryCode(countryName) {
    var cCode = '';
    $('select[name="C_Country"] option').each(function(i, itm) {
        if ($(this).html() == countryName) {
            cCode = $(this).attr('value');
        }
    });
    if (cCode === '') {
        cCode = countryCodeMap[countryName];
    }
    return cCode;
}

function loadFields() {

    var container, inputs, index;

    var optinValue;

    inputs = $('input, select');
    for (index = 0; index < inputs.length; index++) {
        var LookupID = inputs[index].name;
        var LookupValue = GetElqContentPersonalizationValue(LookupID);
        var LowerCaseValue = LookupValue.toLowerCase();

        if (inputs[index].name == LookupID && inputs[index].name != 'C_Country') {
            if (inputs[index].type == "text") {
                inputs[index].value = LookupValue;
            } else if (inputs[index].type == "select" || inputs[index].type == 'select-one') {
                for (var dropdownIndex = 0; dropdownIndex < $(inputs[index])[0].options.length; dropdownIndex++) {
                    if (($(inputs[index])[0].options[dropdownIndex].value == LookupValue) || ($(inputs[index])[0].options[dropdownIndex].innerText == LookupValue)) {
                        LookupValue = $(inputs[index])[0].options[dropdownIndex].value;
                        $(inputs[index]).val(LookupValue);
                        break;
                    }
                }
            } else {
                // All Other Inputs = do nothing
            }
        }
    }

    if ($('#First').length > 0) {
        $('#First').val($('#C_FirstName').val());
    }

    if ($('#firstname').length > 0) {
        $('#firstname').val($('#C_FirstName').val());
    }

    if ($('#Last').length > 0) {
        $('#Last').val($('#C_LastName').val());
    }

    if ($('#lastname').length > 0) {
        $('#lastname').val($('#C_LastName').val());
    }

    if ($('#Phone').length > 0) {
        $('#Phone').val($('#C_BusPhone').val());
    }

    if ($('#jobtitle').length > 0) {
        $('#jobtitle').val($('#C_Title').val());
    }

}

$('body').on('change blur', '#C_EmailAddress, input[name="C_EmailAddress"]', function() {

    var email = $('#C_EmailAddress').val();
    if ($('#C_EmailAddress').length == 0 && $('input[name="C_EmailAddress"]').length == 1) {
        email = $('input[name="C_EmailAddress"]').val();
    }
    eloquaContactLookupByEmail(email);

});

//add elqCustomerGUID for server side submit
var timerId = null,
    timeout = 5,
    myForm = null;

function WaitUntilCustomerGUIDIsRetrieved() {

    if (!!(timerId)) {
        if (timeout == 0) {
            return;
        }

        if (typeof this.GetElqCustomerGUID === 'function') {

            myForm.append('<input type="hidden" name="elqCustomerGUID" value="">');
            myForm.append('<input type="hidden" name="elqCookieWrite" value="0">');
            document.forms[myForm.attr('name')]["elqCustomerGUID"].value = GetElqCustomerGUID();
            return;
        }
        timeout -= 1;
    }
    timerId = setTimeout("WaitUntilCustomerGUIDIsRetrieved()", 500);
    return;
}

$(function() {

    $('form').each(function() {

        if ($(this).attr('action') == '/iw/csc/eloqua.jsp') {
            myForm = $(this);
        }
    });

    if (myForm != 'undefined' && myForm != null) {

        WaitUntilCustomerGUIDIsRetrieved();
        _elqQ.push(['elqGetCustomerGUID']);
    }

});

(function($) {
    eloquaForm = {
        $utm_campaign: $('input[name="utm_campaign"]'),
        $utm_content: $('input[name="utm_content"]'),
        $utm_medium: $('input[name="utm_medium"]'),
        $utm_source: $('input[name="utm_source"]'),
        $utm_term: $('input[name="utm_term"]'),
        $cid: $('input[name="cid"]'),
        $DataCardCreationDate: $('input[name="DataCardCreationDate"]'),
        $gclid: $('input[name="gclid"]'),
        $C_Most_Recent_SFDC_Campaign_ID1: $('input[name="C_Most_Recent_SFDC_Campaign_ID1"]'),
        setCreationDate: function() {
            var d = new Date();
            var curr_date = ('0' + (d.getDate())).slice(-2);
            var curr_month = ('0' + (d.getMonth() + 1)).slice(-2);
            var curr_year = d.getFullYear();
            eloquaForm.$DataCardCreationDate.val(curr_date + "/" + curr_month + "/" + curr_year);
        },
        setUTMValues: function() {
            var queryString = eloquaForm.setQueryString();
            if (queryString.utm_campaign !== undefined) {
                $('input[name=utm_campaign]').val(queryString.utm_campaign);
            }
            if (queryString.utm_content !== undefined) {
                $('input[name=utm_content]').val(queryString.utm_content);
            }
            if (queryString.utm_medium !== undefined) {
                $('input[name=utm_medium]').val(queryString.utm_medium);
            }
            if (queryString.utm_source !== undefined) {
                $('input[name=utm_source]').val(queryString.utm_source);
            }
            if (queryString.utm_term !== undefined) {
                $('input[name=utm_term]').val(queryString.utm_term);
            }
            if (queryString.cid !== undefined) {
                $('input[name=cid]').val(queryString.cid);
                $('input[name=C_Most_Recent_SFDC_Campaign_ID1]').val(queryString.cid);
            } else {
                // $('input[name=C_Most_Recent_SFDC_Campaign_ID1]').val(window.location.href.split('?')[0]);
            }
            var Campaign_ID1 = document.forms["CampusNetworkBuyersGuide"]["C_Most_Recent_SFDC_Campaign_ID1"].value;
            $('input[name=C_Most_Recent_SFDC_Campaign_ID1]').val(Campaign_ID1);
            if (queryString.gclid !== undefined) {
                $('input[name=gclid]').val(queryString.gclid);
            }
        },
        setQueryString: function() {
            var query_string = {};
            var query = parent.window.location.search.substring(1);
            var vars = query.split("&");
            for (var i = 0; i < vars.length; i++) {
                var pair = vars[i].split("=");
                // If first entry with this name
                if (typeof query_string[pair[0]] === "undefined") {
                    query_string[pair[0]] = pair[1];
                    // If second entry with this name
                } else if (typeof query_string[pair[0]] === "string") {
                    var arr = [query_string[pair[0]], pair[1]];
                    query_string[pair[0]] = arr;
                    // If third or later entry with this name
                } else {
                    query_string[pair[0]].push(pair[1]);
                }
            }
            return query_string;
        },
    }
    $(document).ready(function() {
        eloquaForm.setCreationDate();
        eloquaForm.setUTMValues();
    });
})(jQuery);

var inherits = function(ctor, superCtor) {
    ctor.super_ = superCtor;
    Object.beget = function(obj) {
        var fn = function() { }
        fn.prototype = obj;
        return new fn(); // now only its prototype is cloned.
    }
    ctor.prototype = Object.beget(superCtor.prototype);
};

var customElq = function(siteid) {

    var elqVer = "v200";
    var siteid = siteid;
    var url = (document.location.protocol == "https:" ? "https://secure" : "http://now") + ".eloqua.com/visitor/" + elqVer + "/svrGP.aspx";

    customElq.super_.apply(this, arguments); // call A's constructor
    this.getData = function(options) {
        settings = $.extend({
            key: "",
            lookup: "",
            success: "",
            error: ""
        }, options);

        if (settings.key != "") {
            var ms = new Date().getMilliseconds();
            var dlookup = url + "?pps=50&siteid=" + siteid + "&DLKey=" + settings.key + "&DLLookup=" + settings.lookup + "&ms=" + ms;

            $.ajax({
                url: dlookup,
                async: false,
                dataType: "script",
                success: function() {
                    if (typeof settings.success == "function") {
                        settings.success();
                    } else {
                        return false;
                    }
                },
                error: function(err) {
                    if (typeof settings.error == "function") {
                        settings.error();
                    } else {
                        return false;
                    }
                }
            });
        }
    }
};


// code to add jnpr_vID starts here - Code by Brian Davis.

$(document).ready(function() {

    //from the end of the domain find characters between dots that is >3 and then take that and anything to the right as the root domain
    var rootDomain = '';
    var domainParts = document.location.host.split('.');
    for (i = 0; i < domainParts.length; i++) {
        var slot = domainParts.length - i - 1
        if (domainParts[slot].length > 3) {
            for (ii = slot; ii < domainParts.length; ii++) {
                rootDomain = rootDomain + (rootDomain ? '.' : '') + domainParts[ii]
            }
            break;
        }
    }

    if(_satellite.buildInfo !== undefined) _satellite.buildInfo.environment = rootDomain && rootDomain.indexOf('.') >= 0 ? rootDomain : document.location.host;

    _satellite.setCookieWithDomain = _satellite.setCookieWithDomain || function(t, n, i, d) {
        var a; if (i) {
            var r = new Date;
            r.setTime(r.getTime() + 24 * i * 60 * 60 * 1e3), a = "; expires=" + r.toGMTString()
        } else a = "";
        document.cookie = t + "=" + n + a + ";domain=" + d + ";path=/"
    }


    //check with juniper.net
    if ((typeof XMLHttpRequest != 'undefined' || typeof XDomainRequest != 'undefined') && !_satellite.readCookie('jnpr_vID_check') && document.location.hostname.indexOf('.juniper.net') <= 0) {
        function gatewayListener() {

            var response = JSON.parse(this.responseText);
            var jvidHere = _satellite.readCookie('jnpr_vID')
            var jvidThere = response.jnpr_vID;
            var jvidFinal = '';

            //if has J vID use J vID
            if (response.status == 'existing') {
                jvidFinal = jvidThere;
            }

            //if no 3rd-party vID and new J vID use J vID
            else if (!jvidHere && jvidThere) {
                jvidFinal = jvidThere;
            }

            //if has 3rd-party vID and new J vID use 3rd-party vID
            else if (jvidHere && response.status == 'new') {
                jvidFinal = jvidHere;
            }

            //update cookie here
            _satellite.setCookieWithDomain("jnpr_vID", jvidFinal, 730, _satellite.buildInfo.environment);
            _satellite.setCookieWithDomain("jnpr_vID_check", "true", 1 / 24 / 6, document.location.hostname);


            window.jnpr_vID_sync = function() {

                var srcURL = "https://dme.juniper.net/jnpriframe/default.html";
                srcURL += "?jnpr_vID=" + encodeURIComponent(_satellite.readCookie('jnpr_vID'))
                var ifrm = document.createElement("iframe");
                ifrm.setAttribute("src", srcURL);
                ifrm.setAttribute("id", 'jnpr_vID_sync');
                ifrm.style.width = "1";
                ifrm.style.height = "1";
                ifrm.style.frameborder = "0";
                ifrm.style.display = "none";
                document.body.appendChild(ifrm);
            }

            //Sync cookie back to J
            if (typeof document.body != 'undefined' && document.body != null) {
                jnpr_vID_sync();
            } else {
                document.addEventListener("DOMContentLoaded", jnpr_vID_sync);
            }


        }

        //see https://stackoverflow.com/questions/3076414/ways-to-circumvent-the-same-origin-policy
        var gatewayReq = new XMLHttpRequest();
        gatewayReq.addEventListener("load", gatewayListener);
        if ("withCredentials" in gatewayReq) {
            gatewayReq.open("GET", "https://dme.juniper.net/gateway/");
            gatewayReq.withCredentials = true;
        } else if (typeof XDomainRequest != "undefined") {
            gatewayReq = new XDomainRequest();
            gatewayReq.open("GET", "https://dme.juniper.net/gateway/");
        } else {
            gatewayReq = null;
        }

        if (gatewayReq != null) {
            gatewayReq.send();
        }

    } else if (document.location.hostname.indexOf('.juniper.net') >= 0) {
        _satellite.getVar('jnpr_vID');
    }





    function addHidden(a, c, d) {
        //if the element does not already exist in the form
        if (a.c === undefined) {
            //create input element and add attributes
            var b = document.createElement("input");
            b.type = "hidden";
            b.name = c;
            b.value = d;
            //append input element to form
            (a = document.getElementById(a)) && a.appendChild(b);
        }
    }

    function replaceExisting(a, c, d) {
        //replace existing value by
        var val = getCookie(d);
        if (val) {
            //get the form
            var f = document.getElementById(a);
            //get the element in the form
            var input = f.elements[c];
            //replace the value of the element in the form
            input.value = val;
        }
    }

    function getCookie(a) {
        //returns the value of a cookie identified by name
        return (match = document.cookie.match(new RegExp("(^| )" + a + "\x3d([^;]+)"))) ? match[2] : ""
    }

    function getParam(a) {
        //returns the value of a query string parameter by key
        return (a = RegExp("[?\x26]" + a + "\x3d([^\x26]*)").exec(window.location.search)) && decodeURIComponent(a[1].replace(/\+/g, " "))
    }

    function checkFieldName(form, field, value) {
        //If the form fields do not have ids, we have to identify them by name
        //determine if the field id already exists in the form
        var check = document.getElementById(form);
        var name = check.elements[field];
        if (typeof name !== 'undefined') {
            //if the field already exists, replace the current field value
            //see comments in replaceExisting function
            replaceExisting(form, field, value);
        } else {
            //if the field does not already exist, add it
            //see comments in addHidden function
            addHidden(form, field, getCookie(value));
        }
    }

    function checkField(form, field, value) {
        //determine if the field id already exists in the form
        var check = document.getElementById(form)[field];
        if (typeof check !== 'undefined') {
            //if the field already exists, replace the current field value
            //see comments in replaceExisting function
            replaceExisting(form, field, value);
        } else {
            //if the field does not already exist, add it
            //see comments in addHidden function
            addHidden(form, field, getCookie(value));
        }
    }

    function doHtbForm(form) {
        //see comments in checkField function
        //we were specifically told not to replace the campaign code or cid
        //checkField(form, "Campaign", "utm_campaign");
        checkField(form, "utm_campaign", "utm_campaign");
        checkField(form, "Juniper_vID", "jnpr_vID");
        checkField(form, "utm_content", "utm_content");
        checkField(form, "utm_source", "utm_source");
        checkField(form, "utm_medium", "utm_medium");
        checkField(form, "utm_term", "utm_term");
        //checkField(form, "cid", "cid");
        checkField(form, "gclid", "gclid");
    }

    function doRaqForm(form) {
        //see comments in checkField function
        //we were specifically told not to replace the campaign code or cid
        //checkField(form, "utm_campaign", "utm_campaign");
        checkField(form, "Juniper_vID", "jnpr_vID");
        checkField(form, "utm_content", "utm_content");
        checkField(form, "utm_source", "utm_source");
        checkField(form, "utm_medium", "utm_medium");
        checkField(form, "utm_term", "utm_term");
        //checkField(form, "cid", "cid");
        checkField(form, "gclid", "gclid");
    }

    function doTurboForm(form) {
        //see comments in checkFieldName function
        checkFieldName(form, "utm_campaign", "utm_campaign");
        checkFieldName(form, "Juniper_vID", "jnpr_vID");
        checkFieldName(form, "utm_content", "utm_content");
        checkFieldName(form, "utm_source", "utm_source");
        checkFieldName(form, "utm_medium", "utm_medium");
        checkFieldName(form, "utm_term", "utm_term");
        checkFieldName(form, "cid", "cid");
        checkFieldName(form, "gclid", "gclid");
    }

    //Start by looping through all of the form elements on the page.
    for (var Forums = document.forms, i = 0; i < Forums.length; ++i) {
        //identify the id of the form
        var form = Forums[i].id;
        //identify the action assigned to the form
        var action = Forums[i].action;
        //define the array of form id values that we want to run out scripts on
        var elqForms = ['CampusNetworkBuyersGuide', 'htbForm', 'blindEloquaForm', 'eloquaForm', 'Q413GBLInboundWebRequestAQuote'];
        //If the id of the form is in the list or the form action redirect to  eloqua url then run the script
        //if ((elqForms.indexOf(form) > -1) || (action == 'https://s1229.t.eloqua.com/e/f2')){
        if ((elqForms.indexOf(form) > -1) || (action.indexOf('//s1229.t.eloqua.com/e/f2') > -1)) {
            //because different forms use different field names we need to have separate function handlders per form id
            if (form == 'htbForm') {
                //see comments in function above
                doHtbForm(form);
            } else if (form == 'Q413GBLInboundWebRequestAQuote') {
                //see comments in function above
                doRaqForm(form);
            } else if (form == 'blindEloquaForm' || form == 'eloquaForm') {
                //see comments in function above
                doTurboForm(form);
            } else {
                console.log('Inside Else');
                //run the default functions for forms without a specific function
                checkField(form, "utm_campaign", "utm_campaign");
                checkField(form, "Juniper_vID", "jnpr_vID");
                checkField(form, "utm_content", "utm_content");
                checkField(form, "utm_source", "utm_source");
                checkField(form, "utm_medium", "utm_medium");
                checkField(form, "utm_term", "utm_term");
                checkField(form, "cid", "cid");
                checkField(form, "gclid", "gclid");
            }
        }
    }

});

inherits(customElq, jQuery.elq);

var elqTracker = new customElq(1229); //first do the visitor lookup

$(document).ready(function() {



    elqTracker.getData({
        key: "b6d7deeb884e49d38a938b84779fbd59",
        lookup: "",
        success: function() {
            //then check for an email address on the visitor record and do a contact data lookup if there is one
            if (typeof GetElqContentPersonalizationValue == "function") {
                var email = GetElqContentPersonalizationValue("V_Email_Address");
                setEmail(email);
                eloquaContactLookup(email);
            } else {
                var cntry = $('#C_Country').val();
                if (cntry != undefined && cntry != '') {
                    updateSubscription(cntry);
                } else {
                    updateSubscription("");
                }
                var isJapanURL = (function() {
                    return ((window.location.href).indexOf('/jp/ja/') !== -1);
                })();
                if (isJapanURL) {
                    cntry = 'JP';
                    setCountry(cntry);
                }
            }

        },
        error: function() {
            updateSubscription("");
        }
    });
    var isSubmitButtonClicked = false;

    $('input[name="opt_in"]').attr("id", "opt-in-opt-out");

    var country = $("#C_Country").val();


    if (country == 'IN' || country == 'AU') {

        var state = $("#C_State_Prov").val();


        if (state == "") {


            $("#C_State_Prov").attr('required', 'required');
            $("#C_State_Prov").addClass("sw-input-invalid");
            $("#C_State_Prov").parent('div').find('.label').addClass('label-error');
            var errorMessage = '<div _ngcontent-c11="" class="errorMessage">State/Province is required</div>';
            $("#C_State_Prov").closest("div").after(errorMessage);
        } else {


            $("#C_State_Prov").removeAttr('required', 'required');
            $("#C_State_Prov").removeClass("sw-input-invalid");
            $("#C_State_Prov").parent('div').find('.label').removeClass('label-error');
            $('#C_State_Prov').closest("div").next(".errorMessage").html('');
        }
    }

    //Start Added validation for input fields phone, Zip code. it will only accept numeric value

    $('#C_BusPhone').attr('onkeypress', 'if ( isNaN( String.fromCharCode(event.keyCode) )) return false;');

    $('#C_Zip_Postal').keypress(function(event) {
        if ($('#C_Country').val() == 'CA') {
            console.log("country is canada so no zipcode validation");
        } else if (event.which != 8 && isNaN(String.fromCharCode(event.which))) {
            event.preventDefault(); //stop character from entering input
        } else console.log("Inside zip code validation");
    });

    //End Added validation for input fields phone, Zip code. it will only accept numeric value

    /*************** Opt In/Opt Out starts here ************************/


    $('#country, #C_Country, #Country, .country, #C_CountryID1, #C_CountryID2').on('change', function() {

        if (typeof updateSubscription == 'function') {
            updateSubscription(this.value); /* Method to perform show-hide opt-in checkbox & set it's value */
            printStateMenu(this.value); /* Method to refresh state drop-down values based on country input */
            if (typeof showHideZip == 'function') showHideZip(); /* Method to determine the rendering logic of zip/postal code field*/
            if (typeof showHideState == 'function') showHideState(); /* Method to determine the rendering logic of state dropdown field */
        } else {
            /* This block is a legacy code snippet */
            if (this.value == 'US' || this.value == 'CA' || this.value == 'United_States' || this.value == 'Canada') {
                $('#opt-in-out').show();

                if ($('input[name="opt_in"]').prop('checked')) {
                    $('input#opt_in').val(ACCEPTED);
                    $('input[name="opt_in"]').val(ACCEPTED);

                } else {
                    $('input#opt_in').val(SHOWN_NOT_TICKED);
                    $('input[name="opt_in"]').val(SHOWN_NOT_TICKED);
                    $('input[name="opt_in"]').val("Declined");
                }
            } else {
                $('input#opt_in').val(NOT_SHOWN);
                $('input[name="opt_in"]').val(NOT_SHOWN);
                $('#opt-in-out').hide();

            }
        }


    });


    $('#C_Country').on('change', function() {

        var country = $("#C_Country").val();
        var zipcode = $("#C_Zip_Postal").val();

        if (country == 'UK' || country == 'DE') {

            $("#C_Zip_Postal").attr('required', 'required');


        } else {
            $("#C_Zip_Postal").removeAttr('required', 'required');
            $("#C_Zip_Postal").removeClass("sw-input-invalid");
            $('#C_Zip_Postal').closest("div").next(".errorMessage").html('');

        }


        if ($(this).val() == 'IN' || $(this).val() == 'AU') {

            var state = $("#C_State_Prov").val();
            if (state == " ") {

                $("#C_State_Prov").attr('required', 'required');
                $("#C_State_Prov").addClass("sw-input-invalid");
                $("#C_State_Prov").parent('div').find('.label').addClass('label-error');
                var errorMessage = '<div _ngcontent-c11="" class="errorMessage">State/Province is required</div>';
                $("#C_State_Prov").closest("div").after(errorMessage);
            }
        } else {

            $("#C_State_Prov").removeAttr('required', 'required');
            $("#C_State_Prov").removeClass("sw-input-invalid");
            $("#C_State_Prov").parent('div').find('.label').removeClass('label-error');
            $('#C_State_Prov').closest("div").next(".errorMessage").html('');
        }


        if ($('#country').length > 0) {
            var countryVal = $('#C_Country').val();
            $('#country').val(countryVal);
        }
        $('#Country').val($('#C_Country').val());

    });


    $('#C_Zip_Postal').on('change', function() {
        if ($('#postal_code').length > 0) {
            var zipCodeVal = $('#C_Zip_Postal').val();
            $('#postal_code').val(zipCodeVal);
        }

        if ($('#C_Zip_Postal').length > 0) {
            $('#C_Zip_Postal').closest("div").next(".errorMessage").remove();
        }
    });



    $('#C_Purchase_Time_Frame1').on('change', function() {

        var purchVal = $('#C_Purchase_Time_Frame1').val();
        $('#Timeframe').val(purchVal);

    });

    $('#C_Comments').on('change', function() {
        if ($('#C_Comments').length > 0) {
            var CommVal = $('#C_Comments').val();
            $('#Comments').val(CommVal);
        }
    });

    $('#C_State_Prov').on('change', function() {

        $('#StateorProvince').val($('#C_State_Prov').val());

    });
    $('#C_FirstName').on('change', function() {
        if ($('#First').length > 0) {
            var firstNameVal = $('#C_FirstName').val();
            $('#First').val(firstNameVal);
        }
        if ($('#firstname').length > 0) {
            var firstNameVal = $('#C_FirstName').val();
            $('#firstname').val(firstNameVal);
        }
    });

    $('#C_LastName').on('change', function() {
        if ($('#Last').length > 0) {
            var lastNameVal = $('#C_LastName').val();
            $('#Last').val(lastNameVal);
        }
        if ($('#lastname').length > 0) {
            var lastNameVal = $('#C_LastName').val();
            $('#lastname').val(lastNameVal);
        }
    });

    $('#C_BusPhone').on('change', function() {
        if ($('#Phone').length > 0) {
            var phoneVal = $('#C_BusPhone').val();
            $('#Phone').val(phoneVal);
        }
    });


    $('#C_Title').on('change', function() {
        if ($('#jobtitle').length > 0) {
            var titleVal = $('#C_Title').val();
            $('#jobtitle').val(titleVal);
        }
    });

    $('input[name="opt_in"]:checkbox').change(function() {
        if ($(this).prop('checked')) {

            $('input#opt_in').val(ACCEPTED);
            $('input[name="opt_in"]').val(ACCEPTED);

        } else {

            $('input#opt_in').val(SHOWN_NOT_TICKED);
            $('input[name="opt_in"]').val(SHOWN_NOT_TICKED);

        }
    });
    /*************** Opt In/Opt Out ends here ************************/


    $('input[name="opt_in_out"]:checkbox').change(function() {
        if ($(this).prop('checked')) {

            $('input#opt_in').val(ACCEPTED);
            $('input[name="opt_in"]').val(ACCEPTED);

        } else {

            $('input#opt_in').val(SHOWN_NOT_TICKED);
            $('input[name="opt_in"]').val(SHOWN_NOT_TICKED);

        }
    });

    $(".ju-checkbox").on('click', function() {
        $(this).text(function(i, v) {
            if (v === 'check_box_outline_blank' ? true : false) {
                $(this).parents('.submit-row').find('.ju-submit-button').attr('aria-disabled', 'false');
                $(this).parents('.submit-row').find('.ju-submit-button').attr('tabindex', '0');
                $(this).parents('.submit-row').find('.ju-submit-button').removeAttr('disabled');
                $(this).attr('aria-label', 'check_box');
                return 'check_box';
            } else {
                $(this).parents('.submit-row').find('.ju-submit-button').attr('aria-disabled', 'true');
                $(this).parents('.submit-row').find('.ju-submit-button').attr('tabindex', '-1');
                $(this).parents('.submit-row').find('.ju-submit-button').attr('disabled', 'true');
                $(this).attr('aria-label', 'check_box_outline_blank');
                return 'check_box_outline_blank';
            }
        })
    });

    // Removing error message in form validation
    $(".jnpr-form input").on('keyup', function() {
        if ($(this).val() != "") {
            $(this).removeClass("ng-invalid");
            $(this).removeClass("sw-input-invalid");
            $(this).parent('div').find('.label').removeClass("label-error");
            $(this).closest('sw-form-field').find('.errorMessage').remove();
            $(this).closest("div").next(".errorMessage").remove();
        }
    });
    $(".jnpr-form select").on('change', function() {
        if ($(this).val() != "") {
            $(this).removeClass("ng-invalid");
            $(this).removeClass("sw-input-invalid");
            $(this).parent('div').find('.label').removeClass("label-error");
            $(this).closest('sw-form-field').find('.errorMessage').remove();
            $(this).closest("div").next(".errorMessage").remove();
        }
    });
    // End Removing error message in form validation
    //setting Default Refferal URL for Feedback
    if (window.location.href.indexOf("feedback") > -1) {
        $('#url').val(document.referrer);
              $('#hidden_url').val(document.referrer);
                this.URL = document.referrer;
                 if(this.URL.includes("www.juniper.net/documentation")){
                    this.pageLocale = document.referrer.split('/')[4];
                  }
                  else{
                    this.pageLocale = document.referrer.split('/')[3];
                  }
                  const localeLanguage = new Map([
                    [ 'us', 'English' ],
                    [ 'uk', 'English UK' ],
                    [ 'de', 'German' ],
                    [ 'fr', 'French' ],
                    [ 'cn', 'Chinese (Simplified)' ],
                    [ 'kr', 'Korean' ],
                    [ 'jp', 'Japanese' ],
                    [ 'mx', 'Spanish (Latin America)' ],
                    [ 'es', 'Spanish (Spain)' ],
                    [ 'br', 'Portuguese(Brazil)' ],
                    [ 'ru', 'Russian' ],
                    [ 'br', 'Japanese' ],
                    [ 'ko', 'Korean' ],
                    [ 'en_us', 'English' ],
                    [ 'ja', 'Japanese' ],
                    [ 'nl', 'Dutch' ],
                    [ 'pt_BR', 'Portuguese(Brazil)' ],
                  ]);
                  this.Lang = localeLanguage.get(this.pageLocale);
                  if(this.Lang === null || this.Lang === undefined || this.Lang === null){
                    this.Lang = 'English';
                  }
                $('#lang').val(this.Lang);
    }

    // Form validations
    $("form").on("click", 'button.jnpr-submit-button', function(e) {
        e.preventDefault();
        var isValid = 0;
        $("form.jnpr-form").find("input").each(function() {
            var isRequired = $(this).attr("required");
            if (typeof isRequired !== typeof undefined && isRequired !== false && ($(this)[0].id) != 'C_Zip_Postal') {
                var errorMessage = '<div class="errorMessage">' + $(this).attr("data-error-label") + '</div>';
                if (!$(this).val()) {
                    $(this).addClass("sw-input-invalid");
                    $(this).parent('div').find('.label').addClass('label-error');
                    $(this).closest("div").next(".errorMessage").remove();
                    $(this).closest("div").after(errorMessage);
                    isValid = isValid + 1;
                } else {
                    $(this).removeClass("sw-input-invalid");
                    $(this).parent('div').find('.label').removeClass('label-error');
                    $(this).closest("div").next(".errorMessage").remove();
                }
            }
        });

        $("form.jnpr-form").find("select").each(function() {
            var isRequired = $(this).attr("required");
            if (typeof isRequired !== typeof undefined && isRequired !== false && ($(this)[0].id) != 'C_State_Prov') {
                var errorMessage = '<div class="errorMessage">' + $(this).attr("data-error-label") + '</div>';
                if (!$(this).val()) {
                    $(this).addClass("sw-input-invalid");
                    $(this).parent('div').find('.label').addClass('label-error');
                    $(this).closest("div").next(".errorMessage").remove();
                    $(this).closest("div").after(errorMessage);
                    isValid = isValid + 1;
                } else {
                    $(this).removeClass("sw-input-invalid");
                    $(this).parent('div').find('.label').removeClass('label-error');
                    $(this).closest("div").next(".errorMessage").remove();
                }

            }
        });


        var country = $("#C_Country").val();
        var state = $("#C_State_Prov").val();
        if (country == 'IN' || country == 'AU' || country == 'CA' || country == 'US') {

            if (state == "") {
                $("#C_State_Prov").attr('required', 'required');
                $("#C_State_Prov").addClass("sw-input-invalid");
                $("#C_State_Prov").parent('div').find('.label').addClass('label-error');
                $("#C_State_Prov").closest("div").next(".errorMessage").remove();
                var errorMessage = '<div class="errorMessage">State/Province is required</div>';
                $("#C_State_Prov").closest("div").after(errorMessage);
                isValid = isValid + 1;
            } else {
                $("#C_State_Prov").removeAttr('required', 'required');
                $("#C_State_Prov").removeClass("sw-input-invalid");
                $("#C_State_Prov").parent('div').find('.label').removeClass('label-error');
                $('#C_State_Prov').closest("div").next(".errorMessage").html('');
            }
        }

        var zipcode = $("#C_Zip_Postal").val();

        if (country == 'UK' || country == 'DE') {

            if (zipcode == "") {
                $("#C_Zip_Postal").attr('required', 'required');
                $("#C_Zip_Postal").addClass("sw-input-invalid");
                $("#C_Zip_Postal").parent('div').find('.label').addClass('label-error');
                $("#C_Zip_Postal").closest("div").next(".errorMessage").remove();
                var errorMessage = '<div _ngcontent-c11="" class="errorMessage">Zip Code is required</div>';
                $("#C_Zip_Postal").closest("div").after(errorMessage);
                isValid = isValid + 1;
            } else {
                $("#C_Zip_Postal").removeAttr('required', 'required');
                $("#C_Zip_Postal").removeClass("sw-input-invalid");
                $("#C_Zip_Postal").parent('div').find('.label').removeClass('label-error');
                $('#C_Zip_Postal').closest("div").next(".errorMessage").html('');
            }

        }


        //Start Validating Email address
        var email_reg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

        var emailinput = $('#C_EmailAddress').val();

        if (email_reg.test(emailinput) == false) {
            var errorMessage = '<div class="errorMessage">Invalid Email Format</div>';
            $('#C_EmailAddress').addClass("sw-input-invalid");
            $('#C_EmailAddress').parent('div').find('.label').addClass('label-error');
            $('#C_EmailAddress').closest("div").next(".errorMessage").remove();
            $('#C_EmailAddress').closest("div").after(errorMessage);
            isValid = isValid + 1;
        }
        //End Validating Email address

        $('#C_FirstName, #C_LastName, #C_JobTitle, #C_Zip_Postal').each(function() {
            var str = $.trim($(this).val());
            if (str != "") {
                var regx = /^[A-Za-z0-9 ]+$/;
                if (!regx.test(str)) {
                    if ($(this).parent().find('.field-message').length == 0) {
                        $(this).parent().append(
                            '<span class="field-message" style="margin-bottom:8px; display:block; color: red; font-size: 1.5em; font-style: italic; width: 100%">Please fill out only alphanumeric charecters</span>'
                        );
                    }

                } else {
                    $(this).parent().find("span.field-message").remove();

                }
            }
        });

        if (isValid !== 0) {
            $('.button .submitErrorMessage').css('display', 'block');
            return false;
        } else {
            $('.button .submitErrorMessage').css('display', 'none');
            if (submitFlag === true) {

                if ($('.captcha').length) {
                    $.post('/bin/juniper/captcha.html',
                        $('.jnpr-form').serialize(),
                        function(data) {
                            resp = JSON.parse(data);
                            if (resp.response == "captcha valid") {
                                submitFlag = false;
                                $("#invalid-cap").hide();
                                $("#error-form-submit").hide();
                                if(isSubmitButtonClicked !== true){
                                    if (window.Metadata){
                                        var forms = document.getElementsByClassName('jnpr-form');
                                        for (var i = 0; i < forms.length; i++) {
                                            var id = forms[i].id;
                                            var doMetadata = "return window.Metadata.pixel.sendFormData(window['" + id + "']);";
                                            document.getElementById(id).setAttribute("onsubmit", doMetadata);
                                        }
                                    }
                                    $(".jnpr-form").submit();
                                    isSubmitButtonClicked = true;
                                }
                            } else if (resp.response == "invalid-captcha") {
                                $("#invalid-cap").show();
                                $("#error-form-submit").hide();
                                grecaptcha.reset();
                                $("#waiting-gif").hide();
                            } else {
                                $("#invalid-cap").hide();
                                $("#error-form-submit").show();
                                $("#waiting-gif").hide();
                            }
                        }
                    );
                } else {
                    if(isSubmitButtonClicked !== true){
                        if (window.Metadata){
                            var forms = document.getElementsByClassName('jnpr-form');
                            for (var i = 0; i < forms.length; i++) {
                                var id = forms[i].id;
                                var doMetadata = "return window.Metadata.pixel.sendFormData(window['" + id + "']);";
                                document.getElementById(id).setAttribute("onsubmit", doMetadata);
                            }
                        }
                        $(".jnpr-form").submit();
                        isSubmitButtonClicked = true;
                    }

                }

            }
        }

    });

    // Code to Add Country List to Form

    var selectValues = {
        "": "Select Country/Region *",
        "": "Select Country/Region",
        "US": "United States",
        "CA": "Canada",
        "AF": "Afghanistan",
        "AX": "Aland Islands",
        "AL": "Albania",
        "DZ": "Algeria",
        "AS": "American Samoa",
        "AD": "Andorra",
        "AO": "Angola",
        "AI": "Anguilla",
        "AQ": "Antarctica",
        "AG": "Antigua and Barbuda",
        "AR": "Argentina",
        "AM": "Armenia",
        "AW": "Aruba",
        "AU": "Australia",
        "AT": "Austria",
        "AZ": "Azerbaijan",
        "BS": "Bahamas",
        "BH": "Bahrain",
        "BD": "Bangladesh",
        "BB": "Barbados",
        "BY": "Belarus",
        "BE": "Belgium",
        "BZ": "Belize",
        "BJ": "Benin",
        "BM": "Bermuda",
        "BT": "Bhutan",
        "BO": "Bolivia",
        "BA": "Bosnia and Herzegovina",
        "BW": "Botswana",
        "BV": "Bouvet Island",
        "BR": "Brazil",
        "IO": "British Indian Ocean Territory",
        "BN": "Brunei Darussalam",
        "BG": "Bulgaria",
        "BF": "Burkina Faso",
        "BI": "Burundi",
        "KH": "Cambodia",
        "CM": "Cameroon",
        "CV": "Cape Verde",
        "KY": "Cayman Islands",
        "CF": "Central African Republic",
        "TD": "Chad",
        "CL": "Chile",
        "CN": "China",
        "CX": "Christmas Island",
        "CC": "Cocos (Keeling) Islands",
        "CO": "Colombia",
        "KM": "Comoros",
        "CG": "Congo",
        "CD": "Congo, The Democratic Republic",
        "CK": "Cook Islands",
        "CR": "Costa Rica",
        "CI": "Cote D'Ivoire",
        "HR": "Croatia",
        "CU": "Cuba",
        "CY": "Cyprus",
        "CZ": "Czech Rep.",
        "DK": "Denmark",
        "DJ": "Djibouti",
        "DM": "Dominica",
        "DO": "Dominican Republic",
        "EC": "Ecuador",
        "EG": "Egypt",
        "SV": "El Salvador",
        "GQ": "Equatorial Guinea",
        "ER": "Eritrea",
        "EE": "Estonia",
        "ET": "Ethiopia",
        "FK": "Falkland Islands (Malvinas)",
        "FO": "Faroe Islands",
        "FJ": "Fiji",
        "FI": "Finland",
        "FR": "France",
        "GF": "French Guiana",
        "PF": "French Polynesia",
        "TF": "French Southern Territories",
        "GA": "Gabon",
        "GM": "Gambia",
        "GE": "Georgia, Republic of",
        "DE": "Germany",
        "GH": "Ghana",
        "GI": "Gibraltar",
        "GR": "Greece",
        "GL": "Greenland",
        "GD": "Grenada",
        "GP": "Guadeloupe",
        "GU": "Guam",
        "GT": "Guatemala",
        "GG": "Guernsey",
        "GN": "Guinea",
        "GW": "Guinea-bissau",
        "GY": "Guyana",
        "HT": "Haiti",
        "HM": "Heard Island & McDonald Islands",
        "HN": "Honduras",
        "HK": "Hong Kong",
        "HU": "Hungary",
        "IS": "Iceland",
        "IN": "India",
        "ID": "Indonesia",
        "IR": "Iran",
        "IQ": "Iraq",
        "IE": "Ireland",
        "IM": "Isle of Man",
        "IL": "Israel",
        "IT": "Italy",
        "JM": "Jamaica",
        "JP": "Japan",
        "JE": "Jersey",
        "JO": "Jordan",
        "KZ": "Kazakhstan",
        "KE": "Kenya",
        "KI": "Kiribati",
        "KP": "Korea, North",
        "KR": "Korea, South",
        "KW": "Kuwait",
        "KG": "Kyrgyzstan",
        "LA": "Lao People's Democratic Republic",
        "LV": "Latvia",
        "LB": "Lebanon",
        "LS": "Lesotho",
        "LR": "Liberia",
        "LY": "Libyan Arab Jamahiriya",
        "LI": "Liechtenstein",
        "LT": "Lithuania",
        "LU": "Luxembourg",
        "MO": "Macau",
        "MK": "Macedonia",
        "MG": "Madagascar",
        "MW": "Malawi",
        "MY": "Malaysia",
        "MV": "Maldives",
        "ML": "Mali",
        "MT": "Malta",
        "MH": "Marshall Islands",
        "MQ": "Martinique",
        "MR": "Mauritania",
        "MU": "Mauritius",
        "YT": "Mayotte",
        "MX": "Mexico",
        "FM": "Micronesia",
        "MD": "Moldova",
        "MC": "Monaco",
        "MN": "Mongolia",
        "ME": "Montenegro",
        "MS": "Montserrat",
        "MA": "Morocco",
        "MZ": "Mozambique",
        "MM": "Myanmar",
        "NA": "Namibia",
        "NR": "Nauru",
        "NP": "Nepal",
        "AN": "Netherlands Antilles",
        "NL": "Netherlands",
        "NC": "New Caledonia",
        "NZ": "New Zealand",
        "NI": "Nicaragua",
        "NE": "Niger",
        "NG": "Nigeria",
        "NU": "Niue",
        "NF": "Norfolk Island",
        "MP": "Northern Mariana Islands",
        "NO": "Norway",
        "OM": "Oman",
        "PK": "Pakistan",
        "PW": "Palau",
        "PS": "Palestinian Territory, Occupied",
        "PA": "Panama",
        "PG": "Papua New Guinea",
        "PY": "Paraguay",
        "PE": "Peru",
        "PH": "Philippines",
        "PN": "Pitcairn",
        "PL": "Poland",
        "PT": "Portugal",
        "PR": "Puerto Rico",
        "QA": "Qatar",
        "RE": "Reunion",
        "RO": "Romania",
        "RU": "Russian Federation",
        "RW": "Rwanda",
        "SH": "Saint Helena",
        "KN": "Saint Kitts & Nevis",
        "LC": "Saint Lucia",
        "PM": "Saint Pierre and Miquelon",
        "VC": "Saint Vincent and The Grenadines",
        "WS": "Samoa",
        "SM": "San Marino",
        "ST": "Sao Tome and Principe",
        "SA": "Saudi Arabia",
        "SN": "Senegal",
        "RS": "Serbia",
        "SC": "Seychelles",
        "SL": "Sierra Leone",
        "SG": "Singapore",
        "SK": "Slovakia",
        "SI": "Slovenia",
        "SB": "Solomon Islands",
        "SO": "Somalia",
        "ZA": "South Africa",
        "GS": "S Georgia & The S Sandwich Islands",
        "ES": "Spain",
        "LK": "Sri Lanka",
        "SD": "Sudan",
        "SR": "Suriname",
        "SJ": "Svalbard and Jan Mayen",
        "SZ": "Swaziland",
        "SE": "Sweden",
        "CH": "Switzerland",
        "SY": "Syrian Arab Republic",
        "TW": "Taiwan",
        "TJ": "Tajikistan",
        "TZ": "Tanzania",
        "TH": "Thailand",
        "TL": "Timor-Leste",
        "TG": "Togo",
        "TK": "Tokelau",
        "TO": "Tonga",
        "TT": "Trinidad and Tobago",
        "TN": "Tunisia",
        "TR": "Turkey",
        "TM": "Turkmenistan",
        "TC": "Turks and Caicos Islands",
        "TV": "Tuvalu",
        "UG": "Uganda",
        "UA": "Ukraine",
        "AE": "United Arab Emirates",
        "UK": "United Kingdom",
        "UM": "US Minor Outlying Islands",
        "UY": "Uruguay",
        "UZ": "Uzbekistan",
        "VU": "Vanuatu",
        "VA": "Vatican",
        "VE": "Venezuela",
        "VN": "Vietnam",
        "VG": "Virgin Islands, British",
        "VI": "Virgin Islands, U.S.",
        "WF": "Wallis and Futuna",
        "EH": "Western Sahara",
        "YE": "Yemen",
        "ZM": "Zambia",
        "ZW": "Zimbabwe"
    };

    var $mySelect = $('#C_Country');
    $.each(selectValues, function(key, value) {
        var $option = $("<option/>", {
            value: key,
            text: value
        });
        $mySelect.append($option);
    });

    //Code to hide State & Zip Code field
    $('#C_State_Prov').hide();
    $('#C_Zip_Postal').hide();
    $('#C_Zip_Postal').prev(".label").hide();




    var x, i, j, l, ll, selElmnt, a, b, c;
    x = document.getElementsByClassName("C_Country");
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.setAttribute("contenteditable", "true");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
                $('#C_Country').trigger("change");
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
            scrollSelect($('.C_Country'));
        });

        a.addEventListener("keydown", function(e) {
            e.preventDefault();
            if (e.keyCode == 40) {
                var $this = $('.C_Country .same-as-selected');
                if ($this.next().length) {
                    $this.next().addClass('same-as-selected');
                    $this.removeClass('same-as-selected');
                    $('.C_Country .select-selected')[0].innerHTML = $this.next().text();
                    scrollSelect($('.C_Country'));
                }
                return false;
            } else if (e.keyCode == 38) {
                var $this = $('.C_Country .same-as-selected');
                if ($this.prev().length) {
                    $this.prev().addClass('same-as-selected');
                    $this.removeClass('same-as-selected');
                    $('.C_Country .select-selected')[0].innerHTML = $this.prev().text();
                    scrollSelect($('.C_Country'));
                }
                return false;
            } else if (e.keyCode == 13) {
                var i, s, h, sl;
                s = this.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this;
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == $('.C_Country .same-as-selected')[0].innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = $('.C_Country .same-as-selected')[0].innerHTML;
                        break;
                    }
                }
                h.click();
                $('#C_Country').trigger("change");

            } else if (e.keyCode >= 48 && e.keyCode <= 90) {
                $('.C_Country .select-items div').each(function(index) {
                    if ($(this).hasClass('same-as-selected')) {
                        $(this).removeClass('same-as-selected');
                        return false;
                    }
                });

                if (resetSearchTimeout) {
                    clearTimeout(resetSearchTimeout);
                }

                resetSearchTimeout = setTimeout(() => {
                    searchKey = '';
                }, 1500);

                searchKey += String.fromCharCode(e.keyCode);

                $('.C_Country .select-items div').each(function(index) {
                    if ($(this).text().toUpperCase().substr(0, searchKey.length) === searchKey) {
                        $(this).addClass('same-as-selected');
                        $('.C_Country .select-selected')[0].innerHTML = $(this).text();
                        scrollSelect($('.C_Country'));
                        return false;
                    }
                });
                return false;
            } else {
                return false;
            }
        });

    }

    document.addEventListener("click", closeAllSelect);


});