(function($window) {
    "use strict";
    var registry = $window.adaptTo("foundation-registry");
    // multifield specific element count requirement
    registry.register("foundation.validation.validator", {
        selector: "[data-required-element-count]",
        validate: function(element) {
            var currentCount = $(element).children(".coral3-Multifield-item").length;
            var validCounts = $(element).data("requiredElementCount");
            if (!Array.isArray(validCounts)) {
                validCounts = [validCounts];
            }
            if (validCounts.indexOf(currentCount) < 0) {
                return "There must be exactly " +
                    validCounts.slice(0, -1).join(", ") +
                    (validCounts.length > 1 ? " or " : "") +
                    validCounts[validCounts.length - 1] +
                    " elements in this multifield";
            }
        }
    });
    // multifield maximum elements
    registry.register("foundation.validation.validator", {
        selector: "[data-max-element-count]",
        validate: function(element) {
            var currentCount = $(element).children(".coral3-Multifield-item").length;
            var maxCount = $(element).data("maxElementCount");
            if (currentCount > maxCount) {
                return "This multifield may contain at most " + maxCount + " elements";
            }
        }
    });
    // multifield minimum elements
    registry.register("foundation.validation.validator", {
        selector: "[data-min-element-count]",
        validate: function(element) {
            var currentCount = $(element).children(".coral3-Multifield-item").length;
            var minCount = $(element).data("minElementCount");
            if (!$(element).parents().hasClass('hide'))
            {  
                if ((minCount !=0) && (currentCount < minCount)) {
                    console.log("if");
                    return "This multifield must contain atleast " + minCount + " elements";
                }
            }
        }
    });
    // pattern matching
    registry.register("foundation.validation.validator", {
        selector: "[data-foundation-validation=\"pattern-match\"]",
        validate: function(element) {
            var patterns = {
                email: {
                    description: "This field must be a valid email address.",
                    regex: /.+@.+\..+/
                },
                "decimal-number": {
                    description: "Please insert a valid decimal number.",
                    regex: /^[0-9]+[.]?[0-9]*$/
                }
            };
            var selectedPattern = patterns[element.getAttribute("data-validation-pattern")];
            if (element.value && !selectedPattern.regex.test(element.value)) {
                return selectedPattern.description;
            }
        }
    });
})($(window));