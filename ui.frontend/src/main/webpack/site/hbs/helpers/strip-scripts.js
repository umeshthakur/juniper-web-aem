module.exports = function (context) {
	const tmp = document.createElement("DIV");
	tmp.innerHTML = context;
	return tmp.textContent||tmp.innerText;
};
