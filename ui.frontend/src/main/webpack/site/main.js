// Stylesheets
import './scss/main.scss';

// Javascript or Typescript
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import '../components/**/js/*.js';
