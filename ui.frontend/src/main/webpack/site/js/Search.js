import Subject from "./Subject";

class Search extends Subject {
    constructor() {
        super();
    }
}

export default Search;
