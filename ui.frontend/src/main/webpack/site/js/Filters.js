import Subject from "./Subject";

class Filters extends Subject {
    constructor() {
        super();
    }

    /** Main filter function
     *
     * @param filters {Array}
     * @param cards {Object}
     * @param callback {Object}
     * @param category {boolean} if category its TRUE will be apply in the querylogic to search with category
     */
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    filterCards(filters, cards, callback = () => {
    }, category = false) {

        const Cards = this.cardsParse(cards, category);
        if (filters.length) {
            $(cards).hide();
            const Filters = this.groupBy(filters, 'category');
            $(Cards).each((i, card) => {
                if (this.queryLogic(Filters, card.tags, category)) {
                    //Show the card if query logic return TRUE
                    $(card.card).show();
                }
            });
        } else {
            callback.apply();
        }
    }

    /** Cards Parse
     * Helper function to create a Big Json file with all cards and parsing properties
     *
     * @param cards
     * @param category {Boolean}
     * @returns {[]}
     */
    cardsParse(cards, category) {
        const Cards = [];
        cards.map((index, card) => {
            const tags = $('.js-tag-filter', card);
            const arrayTags = [];
            tags.each((i, tag) => {
                if ($(tag).data('tag')) {
                    let pair = '';
                    const ArrayTagsObject = {};

                    ArrayTagsObject.value = $(tag).data('tag').toLowerCase().replace(' ', '-');

                    if (category) {
                        pair = $(tag).data('tag').split('/');
                        ArrayTagsObject.value = pair[1].toLowerCase().replace(' ', '-');
                        ArrayTagsObject.category = pair[0].toLowerCase().replace(' ', '-');
                    }

                    arrayTags.push(ArrayTagsObject);
                }
            });
            Cards.push({
                card: card,
                title: $('.js-title', card).text().trim(),
                description: $('.js-description', card).text().trim(),
                tags: arrayTags
            });
        });
        return Cards;
    }


    /** Group By
     *  Function to group filters by categories
     *
     * @param xs
     * @param key
     * @returns {*}
     */
    groupBy(xs, key) {
        return xs.reduce(function (rv, x) {
            (rv[x[key]] = rv[x[key]] || []).push(x);
            return rv;
        }, {});
    };


    /**
     *
     * @param filters
     * @param tags
     * @param category {boolean} if category its TRUE will be apply in the querylogic to search with category
     * @returns {boolean}
     */
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    queryLogic(filters, tags, category = false) {

        let queryString = '';
        Object.entries(filters).map((obj, idx) => {

            //if its other category we add AND operator between each category groups of tags
            //AND
            if (idx > 0) {
                queryString += ' && ';
            }

            // Add OR operator between same category tags
            //OR
            $(obj[1]).map((index, filter) => {
                if (index === 0) {
                    queryString += '(';
                }
                if (index > 0) {
                    queryString += ' || ';
                }
                if (!category) {
                    queryString += "tags.find(function(tag){return tag.value ==='" + filter.value + "'})";
                } else {
                    queryString += "tags.find(function(tag){return tag.value ==='" + filter.value + "' && tag.category ==='" + filter.category + "' }) ";
                }
                if (index === obj[1].length - 1) {
                    queryString += ')';
                }
            });


        });
        // Eval query String and return TRUE of FALSE
        return !!eval(queryString);
    }
}

export default Filters;
