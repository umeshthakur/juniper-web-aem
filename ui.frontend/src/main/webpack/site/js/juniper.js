import {registerComponent, Component, getComponentsRegistered, getComponent} from "./Component";
import Filters from "./Filters";
import Searchs from "./Search";
import {Utils} from "./Utils";
import UserData from "./UserData";
import {Constants} from "./Constants";
import cookies from "./Cookies";
const axios = require('axios');
import device from "current-device";
import SessionCookies from "./SessionCookies";

import Url from "./Url";


const Filter = new Filters();
const Search = new Searchs();
const UserService = new UserData();
const url = new Url();
const sessionCookies = new SessionCookies();

export {
	registerComponent,
	Component,
	getComponentsRegistered,
	getComponent,
	Filter,
	Search,
	Constants,
	device,
	axios,
	Utils,
	UserService,
    url,
	cookies,
	sessionCookies
};
