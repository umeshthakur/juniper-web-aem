const queryString = require('query-string');
import {cookies as Cookies} from './juniper';
import {Constants} from './juniper';

export default class SessionCookies {
    constructor() {
        this.init();
    }

    init() {
        this.urlParams = this.getParams(window.location);
        this.setSessionCookies();
    };

    getParams(Url) {
        const urlParams = Url.search.replace('?', '');
        return queryString.parse(urlParams, {arrayFormat: 'comma'});
    }

    setSessionCookies() {
        $.each(this.urlParams, (key, value) => {
            $.each(Constants.SessionCookies, (ConstantIndex, ConstantValue) => {
                if(ConstantValue === key){
                    Cookies.setCookies(key, value);
                }
            });
        });
    }

    getSessionCookie(key) {
        Cookies.getCookies(key);
    }
}