const $ = require('jquery');
const Components = [];

class Component {
	constructor(element) {
		if (element) {
			/**
			 * @property {object} el Reference to the DOM element
			 * @memberOf Component
			 */
			this.element = element;
			/**
			 * @property {object} elData Element configuration
			 * @memberOf Component
			 */
			this.elData = element.dataset;
			/**
			 * @property {object} Jquery Element
			 * @memberOf Component
			 */
			this.$el = $(element);
		}
		this.init();
		return this;
	}

	init() {
		return this.getComponent();
	}

	getComponent() {
		return this;
	}
};

function registerComponent(selector, componentClass) {
	[...document.querySelectorAll("[data-mod='" + selector + "']")].map(function (element) {
		const component = new componentClass(element);
		if (component) {
			Components.push(component);
		}
	});
}

async function getComponentsRegistered() {
	return Components;
};

async function getComponent(selector) {
	try {
		return await getComponentsRegistered().then((data)=>{
			return data.filter(function (comp){
				return comp.elData.mod === selector;
			});
		});
	} catch (err){

	}
};

export {
	Component,
	registerComponent,
	getComponentsRegistered,
	getComponent
};


