import Cookies from 'js-cookie';


export default class cookies {

    static setCookies(name, value, options) {
        const Options = options || '';
        Cookies.set(name, value, Options);
    }

    static  removeCookies(name) {
        Cookies.remove(name);
    }

    static getCookies(name) {
        return Cookies.get(name);
    }

    static getAllCookies() {
        return Cookies.get()
    }
}
