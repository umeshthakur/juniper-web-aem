import {Constants} from './juniper';
import {Observer} from "./Observer";

// User Data functions
export default class UserData {
    constructor () {
        this.initialized = false;
        this.userData = {
            'userProfile': {},
            'isLoggedIn': false
        };
    }
    init() {
        if (!this.initialized) {
            this.initialized = true;
            this._makeCall();
        }
    }
    _makeCall() {
        $.get('/bin/juniper/user-info', (dataResponse) => {
            if (dataResponse && dataResponse.userProfile) {
                this.userData.userProfile = JSON.parse(dataResponse.userProfile);
                this.userData.isLoggedIn = dataResponse.isLoggedIn;
            }
        }).always(function () {
            Observer.notifyObservers(Constants.Events.UserDataReady);
        });
    }
    getUserProfile() {
        return this.userData.userProfile;
    }
    getUserFirstName() {
        if (this.userData.userProfile) {
            return this.userData.userProfile.firstName || '';
        }
        return '';
    }
    isLoggedIn() {
        return this.userData.isLoggedIn;
    }
    logout(currentPath) {
        if (currentPath) {
            window.location.href = '/system/sling/logout?resource=' + currentPath.replace("login", "logout");
        } else {
            window.location.href = '/system/sling/logout?resource=/utils/secure/logout.html';
        }
    }
};
