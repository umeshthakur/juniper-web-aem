const Constants  = {
    Events : {
        UserDataReady: 'userDataReady',
    },
    SessionCookies: [
        'utm_campaign',
        'utm_source',
        'utm_medium',
        'utm_content',
        'utm_term',
        'cid',
        'msclkid',
        'gclid'
    ]
};


export {
    Constants
};
