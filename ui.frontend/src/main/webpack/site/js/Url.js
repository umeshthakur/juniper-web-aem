const queryString = require('query-string');
import URL from './Url-params';

export default class Url {

    constructor() {
        this.params = this.getParams(window.location);
        this.filters = false;
    }

    getParams(Url = window.location) {
        const urlParams = Url.search.replace('?', '');
        return queryString.parse(urlParams, {arrayFormat: 'comma'});
    }

    getParamByName(name) {
        const params = this.getParams(window.location);
        return params[name];
    }

    enable(filters = false) {
        URL.enable(
            onChange => {
                //console.log(onChange.queryParams); // object
                //onsole.log(onChange.queryString); // string
            }
        );
        this.filters = filters;
    }

    /** Add
     * Add or update a single parameter value.
     *
     * @param param {String}
     * @param value {String}
     * @returns {*}
     */

    add(param, value) {
        console.log(`url ---  ${param} - ${value}`);

        let Value = value
        if (this.filters) {
            Value = value.replace('/', ':');
            const Param = this.getParam(param);
            if (Param && Param.search(Value) === -1) {
                Value = Param + ',' + Value;
            }
        }
        URL.set(param, Value)
    }

    /** toggle
     *  Add ?param=value if it's not already present. Remove the parameter if it is already present.
     *
     * @param param {String}
     * @param value {String}
     * @returns {*}
     */

    toggle(param, value) {
        URL.toggle(param, value)
    }

    /** Remove
     *  Remove a parameter.
     *
     * @param options {Object}
     * @param options.param {String}
     * @param options.value {String}
     * @param options.removeAll {Boolean}
     * @returns {*}
     */
    remove(options = {param: '', value: '', removeAll: false}) {
        if (this.filters && options.param === 'filters') {
            if (options.removeAll) {
                URL.remove(options.param);
            } else {
                let params = this.parseFilter({params: this.getParams()});
                const Value = options.value.replace('/', ':');
                if (params.filters) {
                    const index = params.filters.indexOf(Value);
                    if (index > -1) {
                        params.filters.splice(index, 1);
                    }
                    params.filters = params.filters.join(',');
                    this.replace({[options.param]: params.filters})
                }
            }
        } else {
            URL.remove(options.param);
        }
    }

    /** ReplaceWith
     *  Replace any current query string with the provided list of parameters.
     *
     * @param Params {Object}
     * @returns {*}
     */
    replaceWith(Params) {
        URL.apply(Params);
    }

    /** ReplaceWith
     *  Replace any current query string with the provided list of parameters.
     *
     * @param Params {Object}
     * @returns {*}
     */
    replace(Params) {
        URL.replace(Params);
    }

    /** Clear
     *  Clear all parameters from the URL.
     *
     */
    clear() {
        URL.clear()
    }

    getAll() {
        return URL.getParams()
    }

    /** getParam
     *  Remove a parameter.
     *
     * @param param {String}
     * @returns {*}
     */
    getParam(param) {
        return URL.get(param)
    }

    parseFilter(options = {params: {}, obj: false, replace: false}) {
        if (options.params.filters) {
            let filters = options.params.filters.split(',');
            if (options.obj) {
                filters = filters.map((value, index) => {
                    return {
                        category: value.split(':')[0],
                        value: value.split(':')[1].replace(':', '/'),
                    }
                });
            }
            if (options.replace) {
                filters = filters.map((value, index) => {
                    return value.replace(':', '/')
                });
            }
            options.params.filters = filters;
        }
        return options.params;
    }
}
