// BreakpointUtils functions
export class BreakpointUtils {
    static isExtraSmall() {
        return this.checkBreakpoint('xs');
    }
    static isSmall() {
        return this.checkBreakpoint('sm');
    }
    static isMedium() {
        return this.checkBreakpoint('md');
    }
    static isLarge() {
        return this.checkBreakpoint('lg');
    }
    static isExtraLarge() {
        return this.checkBreakpoint('xl');
    }
    static checkBreakpoint(target) {
        return $('.breakpoint-check .' + target).css('display') === 'block';
    }
};
