// Observer functions
const observers = {};
export class Observer {
    static addObserver (event, observer) {
        if (observers[event]) {
            observers[event].push(observer);
        } else {
            observers[event] = [observer];
        }
    }
    static removeObserver (event, observer) {
        let observersArray = observers[event];
        const index = observersArray.indexOf(observer);

        if (index !== -1) {
            observersArray = observersArray.splice(index, 1);
            observers[event] = observersArray;
        }
    }
    static notifyObservers (event, args) {
        const observersArray = observers[event];

        if (observersArray) {
            observersArray.forEach(function (observer) {
                observer(args);
            });
        }
    }
};


