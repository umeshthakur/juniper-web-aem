/*eslint no-empty-function: "off"*/
/*eslint @typescript-eslint/no-empty-function: "off"*/
class Subject {
    constructor() {
        this.observers = [];
        this.watchers = [];
    }

    isSubscribed(f) {
        return this.observers.filter(subscriber => subscriber === f).length;
    }

    isSubscribedToWatchers(f) {
        return this.watchers.filter(subscriber => subscriber === f).length;
    }

    subscribe(f) {
        if (this.isSubscribed(f)) {
            return;
        }
        this.observers.push(f);
        const timeout = setInterval(()=> {
            if(this.checkIfFinished()) {
                this.notifyIsSubscriber(f);
                clearInterval(timeout);
            }
        }, 100);

    }

    checkIfFinished(){
        return(this.watchers.length > 0);
    }


    subscribeToWatchObserver(f){
        if (this.isSubscribedToWatchers(f)) {
            return;
        }
        this.watchers.push(f);
    }

    unsubscribe(f) {
        this.observers = this.observers.filter(subscriber => subscriber !== f);
    }

    notifyIsSubscriber(f) {
        this.watchers.forEach(observer => observer.update(f));
    }

    notify(options =
               {
                   type: 'update',
                   data: [],
                   callback: () => {
                   }
               }
    ) {
        this.observers.forEach(observer => observer.update(options.type, options.data, options.callback));
    }
}

export default Subject;
