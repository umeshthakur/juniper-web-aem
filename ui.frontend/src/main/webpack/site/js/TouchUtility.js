// TouchUtility functions
export class TouchUtility {

    constructor(element) {
        this.touchStartX = 0;
        this.touchStartY = 0;
        this.touchEndX = 0;
        this.touchEndY = 0;
        this.leftHandler = {};
        this.rightHandler = {};
        this.upHandler = {};
        this.downHandler = {};
        this.tapHandler = {};
        if (element) {
            this.gestureZone = element;
            this._bindEvents();
        }
        return this;
    }
    setLeftHandler (eventHandler) {
        this.leftHandler = eventHandler;
    }
    setRightHandler (eventHandler) {
        this.rightHandler = eventHandler;
    }

    _bindEvents() {
        this.gestureZone.addEventListener('touchstart', () => {
            this.touchStartX = event.changedTouches[0].screenX;
            this.touchStartY = event.changedTouches[0].screenY;
        }, false);

        this.gestureZone.addEventListener('touchend', (event) => {
            this.touchEndX = event.changedTouches[0].screenX;
            this.touchEndY = event.changedTouches[0].screenY;
            this._handleGesture(event);
        }, false);
    }

    _handleGesture(event) {
        if (this.touchEndX < this.touchStartX && typeof this.leftHandler === 'function') {
            this.leftHandler(event);
        }

        if (this.touchEndX > this.touchStartX && typeof this.rightHandler === 'function') {
            this.rightHandler(event);
        }

        if (this.touchEndY < this.touchStartY && typeof this.upHandler === 'function') {
            this.upHandler(event);
        }

        if (this.touchEndY > this.touchStartY && typeof this.downHandler === 'function') {
            this.downHandler(event);
        }

        if (this.touchEndY === this.touchStartY && typeof this.downHandler === 'function') {
            this.tapHandler(event);
        }
    }
};
