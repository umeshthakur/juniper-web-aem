import * as AEM from "../../../site/js/juniper";
import {CardUtility} from "../../casestudycards/js/cardHeightFile.js";

class CaseStudyCards extends AEM.Component {
	init() {
        
		this.Cards = $('.js-cards-item', this.$el);
		document.addEventListener('DOMContentLoaded', CardUtility._cardHeightSetter);
		window.addEventListener('resize',CardUtility._cardHeightSetter);
        this.filterContainer = document.querySelector(".cmp-filters");
		this.filterContainer.addEventListener('click',CardUtility._tempHeightCalc);
		this._bindEvents();
		AEM.Filter.subscribe(this);
	}
	// eslint-disable-next-line @typescript-eslint/no-unused-vars

	_bindEvents() {
		$(this.Cards).each(function(index) {
			$(this).on("click", function(){
				window.location = $(this).data("card-link");
			});
		});

	}

	update(type, data, callback) {
		switch (type) {
			case 'clean':
				this._showAll();
				break;
			case 'update':
				this._filterCards(data);
				break;
			default:
				this._filterCards(data);
		}
	}

	_filterCards(filters) {
		AEM.Filter.filterCards(filters, this.Cards, this._showAll(),true);
	}

	_showAll() {
		//Show All cards
		$(this.Cards).show();
	}

}

AEM.registerComponent('case-study-cards', CaseStudyCards);