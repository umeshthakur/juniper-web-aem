export class CardUtility {

    static current_cards = []; 
    static resultsCards = document.querySelectorAll('.js-cards-item');

    static _cardHeightSetter() {  
    
		if(window.innerWidth < 1024) {
            return;
        }
    
		setTimeout(()=>  {
			let tags = document.querySelector(".js-tags-container");
			if(tags.children.length > 0) {		
				CardUtility._heightSetter();
            }
    
		},500);
		
	}

    static _heightSetter() {

        if(CardUtility.current_cards.length > 0) {
            for(let i = 0; i < CardUtility.current_cards.length;i++) {
                CardUtility.current_cards[i].style.removeProperty('height');
            }
        }
        CardUtility.current_cards = [];
        let selected_cards_max_height = 0;
        
        for(let i = 0; i < CardUtility.resultsCards.length;i++) {   
            if(getComputedStyle(CardUtility.resultsCards[i]).display == "flex") {
                if(CardUtility.length === 0) {
                    CardUtility.resultsCards[i].setAttribute("style", `height:fit-content`);
                }
                CardUtility.current_cards.push(CardUtility.resultsCards[i]);
                selected_cards_max_height = Math.max(CardUtility.resultsCards[i].clientHeight,selected_cards_max_height);
            }
        }
 
        for(let i = 0; i < CardUtility.current_cards.length;i++) {
            CardUtility.current_cards[i].setAttribute("style", `height:${selected_cards_max_height}px`);
        }
    }

   static _tempHeightCalc(e) {	
		
		if(window.innerWidth < 1024)
			return;	
		
		if(e.target.classList.contains("item-title") || e.target.classList.contains("icon-close")) {
			setTimeout(CardUtility._heightSetter(),100);
		}
			
	}

}