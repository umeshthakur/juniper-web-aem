import * as AEM from "../../../site/js/juniper";
import {BreakpointUtils} from "../../../site/js/BreakpointUtils";
import {Suggester} from '../../global-search/js/Suggester';
import {Observer} from "../../../site/js/Observer";
import 'bootstrap/js/dist/collapse';

class Navigation extends AEM.Component {
    init() {
        this.$itemsDropdown = $('.js-item-dropdown', this.$el);
        this.$rightNavSection = $('.js-right-nav', this.$el);
        this.$leftNavSection = $('.js-left-nav', this.$el);
        this.$topPanelSection = $('.js-top-panel-section', this.$el);
        this.$accountLinks = $('.js-account-links', this.$topPanelSection);
        this.$usernameLabel = $('.js-account-username', this.$accountLinks);
        this.$accountLoginBtn = $('.js-account-login', this.$topPanelSection);
        this.$contactUsBtn = $('.js-contact-us', this.$topPanelSection);
        this.$howToBuyBtn = $('.js-how-to-buy', this.$topPanelSection);
        this.$searchNavSection = $('.js-search-nav', this.$el);
        this.$languageNavSection = $('.js-language-nav', this.$el);
        this.$mobileBackBtn = $('.js-mobile-back', this.$el);
        this.$mobileMenuBtn = $('.js-navbar-toggler', this.$el);
        this.$mobileLanguageBtn = $('.js-language-btn-mobile', this.$el);
        this.$mobileSearchBtn = $('.js-search-btn-mobile', this.$el);
        this.$desktopSearchBtn = $('.js-search-btn', this.$el);
        this.$navContainer = $('.js-navigation-container', this.$el);
        this.$searchInput = $('.js-search-input', this.$el);
        this.$megamenus = $('.js-megamenu', this.$el);
        this.$mainNav = $('#mainNav', this.$el);
        this._initializeVariables();
        AEM.UserService.init();
        this._bindEvents();
        this._positionMegamenus();
        this._positionCloseButton();
        this.suggester = new Suggester(
                this.$searchInput[0], this.elData.publicSearchKey, this.elData.coveoSuggestEndpoint);
    }

    _bindEvents() {
        // Update account information if user is logged in.
        Observer.addObserver(AEM.Constants.Events.UserDataReady, () => {
            if (AEM.UserService.isLoggedIn()) {
                this.$usernameLabel.text(AEM.UserService.getUserFirstName());
                this.$accountLinks.show();
                this.$accountLoginBtn.hide();
            }
        });
        // Due to some edge cases when loading the page we need to calculate the position
        // again once page is loaded in order to fix any miscalculations.
        $(window).load( () => {
            this._positionMegamenus();
        });
        // Reset Navigation status and the Mega-Menu positions when there's a resize event.
        $(window).resize( () => {
            if (this.currentWidth !== this.element.offsetWidth) {
                this.currentWidth = this.element.offsetWidth;
                this._positionMegamenus();
                this._resetNavbarStatus();
            }
        });
        // Prevent dropdown to close when clicking inside the mega-menu.
        this.$megamenus.on("click", function (event) {
            if (!event.target.href) {
                event.stopPropagation();
                event.preventDefault();
            }
        });
        this.$mobileMenuBtn.on('click', (event) => {
            this._mobileMenuBtnHandler(event);
        });
        $(this.$itemsDropdown).each((i, item) => {
            $(item).on('click', (event) => {
                if ($(event.target).hasClass('js-topnav-title')) {
                    this._topLevelItemsHandler(item);
                }
            });
            $('.js-close', item).on('click', () => {
                this._resetToplevelStatus();
                $(item.dataset.target).collapse('hide');
            });
        });
        this.$mobileBackBtn.on('click', () => {
            this._mobileBackBtnHandler();
        });
        this.$desktopSearchBtn.on('click', (event) => {
            this._searchButtonHandler($(event.currentTarget));
        });
        this.$mobileSearchBtn.on('click', () => {
            this._searchButtonMobileHandler();
        });
        this.$mobileLanguageBtn.on('click', () => {
            this._languageButtonMobileHandler();
        });
        this.$accountLinks.on('click', () => {
            this._accountLinksToggleHandler();
        });
        this.$accountLoginBtn.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._mobileTopPanelBtnHandler($(event.currentTarget));
        });
        this.$contactUsBtn.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._mobileTopPanelBtnHandler($(event.currentTarget));
        });
        this.$howToBuyBtn.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._mobileTopPanelBtnHandler($(event.currentTarget));
        });
        $('.js-logout-link', this.$accountLinks).on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._logoutUserHandler();
        });
        $('.js-close', this.$searchNavSection).on('click', () => {
            this._resetToplevelStatus();
            this._resetSearchStatus();
        });
        $('.js-search-icon', this.$searchNavSection).on('click', () => {
            console.log(this.$searchInput.val())
            if (this.$searchInput.val()) {
                this._handleSearchAction();
            } else {
                if (BreakpointUtils.isLarge() || BreakpointUtils.isExtraLarge()) {
                    $('.js-close', this.$searchNavSection).click();
                } else {
                    $('.js-icon-search-mobile', this.$mobileSearchBtn).click();
                }
            }
        });
        this.$searchInput.on('keyup', (event) => {
            const keyCode = (event.keyCode ? event.keyCode : event.which);
            event.preventDefault();
            event.stopPropagation();
            if (keyCode === 13) { // Handle enter key.
                this._handleSearchAction();
            }
        });
        $('.js-megamenu', this.$searchNavSection).on('shown.bs.collapse', () => {
            this.$searchInput.focus();
        });

        $(window).scroll( () => {
            this._scrollHandler();
        });
    }

    // Event handlers
    _scrollHandler () {
        const currentScroll = $(window).scrollTop();
        const isNavFixed = this.$navContainer.hasClass('fixed-nav');
        /* If the current scroll position is greater than 0 (the top) AND the current scroll position
         * is less than the document height minus the window height (the bottom) run the navigation if/else statement.
         */
        if (currentScroll > 0 && currentScroll < $(document).height() - $(window).height()) {
            if (currentScroll > this.stickyNavStart && !isNavFixed) {
                this.$navContainer.addClass('fixed-nav');
                this.$navContainer.removeClass('show');
            } else if (currentScroll < this.stickyNavStart) {
                this.$navContainer.removeClass('show');
            }
            if (currentScroll < (this.stickyNavStart / 2) ) {
                this.$navContainer.removeClass('fixed-nav');
            }
            // Scrolling down.
            if (currentScroll > this.previousScroll) {
                this.$navContainer.removeClass('show');
                this.previousScroll = currentScroll;
            // Scrolling up.
            } else if ( (this.previousScroll - currentScroll) > this.stickyNavShow && isNavFixed) {
                this.$navContainer.addClass('show');
                this.previousScroll = currentScroll;
            }
        }
    }

    _mobileMenuBtnHandler (event) {
        // If search bar is active, then close it and restore nav status.
        if (this.$mobileSearchBtn.hasClass('selected')) {
            this.$mobileSearchBtn.removeClass('selected');
            this.$mainNav.removeClass('show');
            this.$megamenus.removeClass('show');
            $('.js-navbar-container', this.$el).removeAttr( 'style' );
        } else if (this.$mobileLanguageBtn.hasClass('selected')) { // If language bar is active.
            this.$mobileLanguageBtn.removeClass('selected');
            this.$mainNav.removeClass('show');
            this.$megamenus.removeClass('show');
            $('.js-navbar-container', this.$el).removeAttr( 'style' );
        }
        const $button = $(event.currentTarget);
        event.stopPropagation();
        event.preventDefault();
        $('body').toggleClass('suspend-scroll');
        this.$el.toggleClass('mobile-menu-expanded');
        this.$mainNav.collapse('toggle');
        $('.js-icon-menu', $button).toggle();
        $('.js-icon-close', $button).toggle();
        $button.toggleClass('expanded');
    }

    _mobileBackBtnHandler () {
        // If it is mobile and it is on Level 2 -> Hide level 2 and show level 1.
        if ((BreakpointUtils.isSmall() || BreakpointUtils.isExtraSmall()) && this._getCurrentNavLevel() === 2) {
            $('.js-nav-level-2:visible').siblings('.js-nav-level-1').show();
            $('.js-nav-level-2:visible').hide();
            $('.js-recommended-section', this.$el).hide();
        } else {
            this.$mobileBackBtn.hide();
            this.$megamenus.removeClass('show');
            this._showTopLevelItems();
        }
    }

    _topLevelItemsHandler (item) {
        this._resetToplevelStatus();
        this._resetSearchStatus();
        if (BreakpointUtils.isLarge() || BreakpointUtils.isExtraLarge()) {
            if (!$(item.dataset.target).hasClass("show")) {
                $(item).addClass('selected');
            }
            $(item.dataset.target).collapse('toggle');
        } else {
            this._hideTopLevelItems();
            $(item).closest('.js-item-dropdown').show();
            $('.js-topnav-title', item).toggle();
            $('.js-megamenu', item).toggleClass('show');
            this.$mobileBackBtn.toggle();
        }
    }

    _searchButtonHandler ($element) {
        this._resetToplevelStatus();
        $('.js-megamenu.show', this.$el).collapse('hide');
        $element.toggleClass('selected');
        $('.js-megamenu', this.$searchNavSection).collapse('toggle');
    }
    _languageButtonMobileHandler () {
        if (this.$mobileLanguageBtn.hasClass('selected')) { // Hide language bar.
            this.$mobileLanguageBtn.removeClass('selected');
            this.$mainNav.hide();
            this._resetNavbarStatus();
            // Workaround to have a cleaner menu closing process.
            this.$mainNav.on('hidden.bs.collapse', () => {
                this.$mainNav.removeAttr('style');
            });
        } else { // Show language bar.
            $('.js-navbar-container', this.$el).hide();
            $('body').addClass('suspend-scroll');
            this.$el.addClass('mobile-menu-expanded');
            this.$mainNav.collapse('show');
            this.$mobileLanguageBtn.addClass('selected');
            $('.js-megamenu', this.$languageNavSection).collapse('show');
        }
        // If menu is expanded then update it.
        if (this.$mobileMenuBtn.hasClass('expanded')) {
            $('.js-icon-menu', this.$mobileMenuBtn).toggle();
            $('.js-icon-close', this.$mobileMenuBtn).toggle();
            this.$mobileMenuBtn.removeClass('expanded');
        } else if (this.$mobileSearchBtn.hasClass('selected')) {
            this._resetSearchStatus();
        }
    }
    _accountLinksToggleHandler () {
        this.$accountLinks.toggleClass('expanded');
    }
    _logoutUserHandler () {
        AEM.UserService.logout(window.location.pathname);
    }
    _mobileTopPanelBtnHandler ($element) {
        window.location.href = $element.data('target');
    }
    _searchButtonMobileHandler () {
        if (this.$mobileSearchBtn.hasClass('selected')) { // Hide search bar.
            this.$mobileSearchBtn.removeClass('selected');
            this.$mainNav.hide();
            this._resetNavbarStatus();
            // Workaround to have a cleaner menu closing process.
            this.$mainNav.on('hidden.bs.collapse', () => {
                this.$mainNav.removeAttr('style');
            });
        } else { // Show search bar.
            $('.js-navbar-container', this.$el).hide();
            $('body').addClass('suspend-scroll');
            this.$el.addClass('mobile-menu-expanded');
            this.$mainNav.collapse('show');
            this.$mobileSearchBtn.addClass('selected');
            $('.js-megamenu', this.$searchNavSection).collapse('show');
        }
        // If menu is expanded then update it.
        if (this.$mobileMenuBtn.hasClass('expanded')) {
            $('.js-icon-menu', this.$mobileMenuBtn).toggle();
            $('.js-icon-close', this.$mobileMenuBtn).toggle();
            this.$mobileMenuBtn.removeClass('expanded');
        } else if (this.$mobileLanguageBtn.hasClass('selected')) {
            this._resetLanguageStatus();
        }
    }
    _handleSearchAction () {
        const isValid = this.$searchInput[0].validity.valid;
        this.$searchInput.removeClass('invalid');
        if (isValid) {
            this._saveJnprData();
            //window.location.href = this.elData.searchResultsPath
            //    + '?query=' + this.$searchInput.val().trim();
            window.open(this.elData.searchResultsPath + '?query=' + this.$searchInput.val().trim(), '_blank');

        } else {
            this.$searchInput.addClass('invalid');
        }
    }
    _saveJnprData() {
        jnprData = jnprData || {};
        jnprData.search ={ 'keyword': this.$searchInput.val(), 'type': 'Global-Search-Box' };
        _satellite.track('SearchKeyword');
    } 
    // Auxiliar methods
    _initializeVariables() {
        this.currentWidth = this.element.offsetWidth;
        // Amount of pixels after which sticky behaviour is enabled (While scrolling down).
        this.stickyNavStart = 600;
        // Amount of pixels after which nav should be displayed (While scrolling up).
        this.stickyNavShow = 300;
        // Initial scroll position.
        this.previousScroll = 0;
    }

    _resetNavbarStatus () {
        this._resetToplevelStatus();
        this._resetSearchStatus();
        this._resetLanguageStatus();
        this._resetMobileStatus();
        this._showTopLevelItems();
        $('.js-megamenu.show', this.$el).collapse('hide');
        $('.js-navbar-container, .js-nav-level-1, .js-nav-level-2, .js-nav-level-2-item, .js-v2-list-item, .js-recommended-section', this.$el).removeAttr( 'style' );
        $('.js-category-bucket-v2, .js-v2-toggle-button, .js-v2-category-toggle-button', this.$el).removeAttr( 'style' );
        $('.js-nav-level-1-item', this.$el).removeClass( 'active inactive' );
    }
    _resetMobileStatus () {
        this.$mainNav.collapse('hide');
        $('.js-icon-menu', this.$mobileMenuBtn).show();
        $('.js-icon-close', this.$mobileMenuBtn).hide();
        this.$mobileMenuBtn.removeClass('expanded');
        $('body').removeClass('suspend-scroll');
        this.$el.removeClass('mobile-menu-expanded');
        this.$mobileBackBtn.hide();
    }
    _resetToplevelStatus () {
        $('.js-item-dropdown', this.$el).removeClass('selected');
    }
    _resetSearchStatus () {
        this.$desktopSearchBtn.removeClass('selected');
        this.$mobileSearchBtn.removeClass('selected');
        $('.js-megamenu', this.$searchNavSection).collapse('hide');
    }
    _resetLanguageStatus () {
        this.$mobileLanguageBtn.removeClass('selected');
        $('.js-megamenu', this.$languageNavSection).collapse('hide');
    }
    _hideTopLevelItems() {
        $('.js-item-dropdown', this.$el).hide();
        this.$topPanelSection.hide();
    }
    _showTopLevelItems() {
        $('.js-item-dropdown', this.$el).show();
        $('.js-topnav-title', this.$el).show();
        this.$topPanelSection.show();
    }
    _getCurrentNavLevel () {
        if ($('.sub-navigation.v2:visible').length > 0) {
            return 1;
        } else if ($('.js-nav-level-1:visible').length < 1) {
            return 2;
        }
        return 1;
    }
    _positionMegamenus() {
        const leftSectionOffset = this.$leftNavSection.offset().left * -1;
        const rightSectionOffset = this.$rightNavSection.offset().left * -1;
        const searchNavSectionOffset = this.$searchNavSection.offset().left * -1;
        $('.js-megamenu', this.$rightNavSection).css({left: rightSectionOffset + 'px'});
        $('.js-megamenu', this.$leftNavSection).css({left: leftSectionOffset + 'px'});
        $('.js-megamenu', this.$searchNavSection).css({left: searchNavSectionOffset + 'px'});
    }
    // Workaround for Windows OS issue with the position of the Close icon.
    _positionCloseButton() {
        if (AEM.device.windows()) {
            $('.js-close', this.$el).css({right: '42px'});
        }
    }
};

AEM.registerComponent('navigation', Navigation);
