import * as AEM from "../../../site/js/juniper";
import 'bootstrap/js/dist/collapse';

class TextExpandable extends AEM.Component {
    init() {
        this.$itemsExpandable = $('.js-item-expandable', this.$el);
        this.$itemsContainer = $('.js-items', this.$el);
        this.$icons = $('.js-icon-expandable', this.$el);
        this._expandable();
    }
    _expandable() {
        this.$itemsExpandable.map((index, item)=>{
            const target = $('.js-expandable-target',item);
            const trigger = $('.js-icon-expandable', item);
            if(target.length){
                $(trigger).on('click', (event)=>{
                    event.preventDefault();

                    $(this.$icons).map((index, icons)=>{
                        if(!$(icons).is($(event.currentTarget))){
                            $(icons).removeClass('show');
                        }
                    });


                    $(trigger).toggleClass('show');
                    $(target).collapse({
                        parent: this.$itemsContainer
                    });
                    $(target).collapse('toggle');
                });
            }
        });
    }
}

AEM.registerComponent('text-expandable', TextExpandable);

