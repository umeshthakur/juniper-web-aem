import * as AEM from "../../../site/js/juniper";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";
import SimpleBar from 'simplebar';
import 'simplebar/dist/simplebar.css';

class CaseStudy extends AEM.Component {
    init(){
        this.$playVideoButton = $('.js-play-video-button', this.$el);
        this.$closeVideoButton = $('.js-modal-close-button', this.$el);
        this.$modalPlayer = $("#" + this.element.dataset.videoModalId, this.$el);
        this.$videoIframe = $("iframe.vid", this.$modalPlayer);
        this._bindEvents();
        this.$casestudyCarousel = $('.js-caseStudy-carousel', this.$el)[0];
        new SimpleBar(this.$casestudyCarousel);
    }
    _bindEvents() {
        this.$playVideoButton.on('click', (event) => {
            this._playVideoClickHandler(event);
        });
        this.$closeVideoButton.on('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            this._closeVideoClickHandler(event);
        });
    }

    //   Event Handlers
    _playVideoClickHandler (event) {
        GlobalOverlay.show(null, this.element.dataset.videoModalId, true);
        this.$videoIframe[0].src =
          this.$videoIframe[0].src +
          "?autoplay=1&mute=0&enablejsapi=1&controls=1";
    }

    _closeVideoClickHandler (event) {
        GlobalOverlay.close(this.element.dataset.videoModalId);
        this.$videoIframe[0].src = this.$videoIframe[0].src.split("?")[0];
    }

}

AEM.registerComponent('product-case-study', CaseStudy);