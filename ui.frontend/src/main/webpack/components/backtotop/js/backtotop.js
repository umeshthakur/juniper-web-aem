import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

class ScrollToTop extends AEM.Component {
	init() {
		this.$scrollTop = $('.js-scroll-top', this.$el);
		this.bindEvents();
	}
	bindEvents() {
		$(this.$scrollTop).on('click', (event) => {
			event.preventDefault();
			document.body.scrollIntoView({
				top: 0,
				left: 0,
				behavior: "smooth"
			});
		});
	}

}

AEM.registerComponent('back-to-top', ScrollToTop);
