import * as AEM from "../../../site/js/juniper";
import {BreakpointUtils} from "../../../site/js/BreakpointUtils";

class SubNavigation extends AEM.Component {
    init() {
        const items = this.element.querySelectorAll('.v1 .js-nav-level-1 .subnav__list .subnav__list--item');
        this.$itemsDropdown = $('.js-item-dropdown', this.$el);
        this.$recommendedSection = $('.js-recommended-section', this.$el);
        this._addListenerToItems(items);
        this._addListenerToV2TitleToggleButtons();
        this._addListenerToV2CategoryToggleButtons();
    }

    _addListenerToV2TitleToggleButtons() {
        $('.js-v2-toggle-button', this.$el).each((index, item) => {
            $(item).on('click', () => {
                const $subnavCol = $(item).closest('.subnav');
                $('.js-category-bucket-v2', $subnavCol).toggle();
                $(item).toggle();
                $(item).siblings('.js-v2-toggle-button').toggle();
            });
        });
    }
    _addListenerToV2CategoryToggleButtons() {
        $('.js-v2-category-toggle-button', this.$el).each((index, item) => {
            $(item).on('click', () => {
                const $subnavCol = $(item).closest('.subnav__list');
                $('.js-v2-list-item', $subnavCol).toggle();
                $(item).toggle();
                $(item).siblings('.js-v2-category-toggle-button').toggle();
            });
        });
    }

    _addListenerToItems(items) {
        items.forEach((item) => {
            const itemLink = item.querySelector('a');
            itemLink.addEventListener('click', (evt) => {
                this._level1ClickHandler(evt, items, this.resetItems);
            }, true);
        });
    }

    resetItems(evt, items, className = 'active') {
        evt.preventDefault();
        items.forEach((item) => {
            item.classList.remove(className);
        });
    }

    _level1ClickHandler (evt, items, callback) {
        const anchorEl = evt.currentTarget;
        const subNavId = anchorEl.dataset.targetId;
        const $listItemEl = $(anchorEl.parentElement);
        const $subnavWrapperEl = $listItemEl.closest('.js-nav-level-1');
        const $subnavParentEl = $subnavWrapperEl.parent();
        const $subnavParentLevel2El = $('.js-nav-level-2', $subnavParentEl);
        evt.stopPropagation();

        // Reset Level 1 Items status
        callback(evt, items, 'active');
        callback(evt, items, 'inactive');
        if (BreakpointUtils.isSmall() || BreakpointUtils.isExtraSmall()) {
            $listItemEl.closest('.js-nav-level-1').hide();
            $subnavParentLevel2El.show();
        } else {
            // Set active status to selected item;
            $listItemEl.addClass('active');
            // Set inactive status to others Level 1 Items.
            $(".js-nav-level-1-item:not(.active)", $subnavWrapperEl).addClass('inactive');
        }
        // Show Level 2 Container
        $subnavParentLevel2El.removeClass('hidden');

        // Show Level 3 Container (Recommended section)
        $('.js-recommended-content', this.$el).hide();
        const $recommendedContent = $('.js-recommended-content[data-subnav-id="' + subNavId + '"]', $subnavParentEl);
        if ($recommendedContent.length) {
            $recommendedContent.show();
            this.$recommendedSection.show();
        } else {
            this.$recommendedSection.hide();
        }

        // Hide all Level 2 items
        $(".js-nav-level-2-item", $subnavParentLevel2El).hide();
        // Show the target Level 2 item
        const $subnavLevel2 = $('.js-nav-level-2-item[data-subnav-id="' + subNavId + '"]', $subnavParentEl);
        $subnavLevel2.show();
    }
};


AEM.registerComponent('sub-navigation', SubNavigation);
