import * as AEM from "../../../site/js/juniper";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";

const $ = require('jquery');

export class LatestNews extends AEM.Component {
    init() {
        this ._bindEvents();
    }

    _bindEvents() {
        $( 'a.play-icon' , this .$el).on( 'click' , (event) => {
            this ._playVideoClickHandler(event);
        });

        $( '.close-icon' , this .$el).on( 'click' , (event) => {
            event.stopPropagation();
            event.preventDefault();
            this ._closeVideoClickHandler(event);
        });

        const card = $('.card', this.$el);
        $(card).each(function() {
            $(this).on("click", function(){
                window.open($(this).data("card-link"), $(this).data("card-link-location"));
            });
        });

        const featured = $('.featured', this.$el);
        $(featured).each(function() {
            $(this).on("click", function(e){
                if(e.target.className === "image" || e.target.closest("img")) {
                    return;
                }
                if($(this).data("featured-link-location").length) {
                    window.open($(this).data("featured-link"), $(this).data("featured-link-location"));
                }
            });
        });
    }


    _playVideoClickHandler (event) {

        const $playButton = $(event.currentTarget);
        const $videoOverlay = $('#' + $playButton.data( 'videoModalId' )).closest('.video-overlay' );
        const $iframe = $( 'iframe' , $videoOverlay);

        let $autoPlay = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $autoPlay = $iframe.attr( 'src' ).split("?")[0] + "?autoplay=true";
        } else{
            $autoPlay = $iframe.attr( 'src' ) + "?autoplay=true";
        }
        $iframe.attr( 'src' , $autoPlay);
        GlobalOverlay.show( null , $playButton.data( 'videoModalId' ), true );
    }

    _closeVideoClickHandler (event) {
        const $videoOverlay = $(event.currentTarget).closest('.video-overlay' );
        const $iframe = $( 'iframe' , $videoOverlay);

        let $stopUrl = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $stopUrl = $iframe.attr( 'src' ).split("?")[0];
        } else{
            $stopUrl = $iframe.attr( 'src' );
        }
        $iframe.attr( 'src' , $stopUrl);
        GlobalOverlay.close($videoOverlay.attr( 'id' ));
    }

}

AEM.registerComponent('latest-news', LatestNews);
