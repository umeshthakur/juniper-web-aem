import * as AEM from "../../../site/js/juniper";
import { GlobalOverlay } from "../../globaloverlay/js/globaloverlay";
const $ = require('jquery');

export class ClientTestimonial extends AEM.Component {

    init() {
        this._bindEvents();
    }

    _bindEvents() {
        $('a.play-icon', this.$el).on('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            this._playVideoClickHandler(event);
        });

        $('.close-icon', this.$el).on('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            this._closeVideoClickHandler(event);
        });

        const card = $('.card', this.$el);
        $(card).each(function (index) {
            $(this).on("click", function () {
                window.location = $(this).data("card-link");
            });
        });
    }

    _playVideoClickHandler(event) {

        const $playButton = $(event.currentTarget);
        const $videoOverlay = $('#' + $playButton.data('videoModalId')).closest('.video-overlay');
        const $iframe = $('iframe', $videoOverlay);

        let $autoPlay = '';
        if ($iframe.attr('src').indexOf('?') > -1) {
            $autoPlay = $iframe.attr('src').split("?")[0] + "?autoplay=true";
        } else {
            $autoPlay = $iframe.attr('src') + "?autoplay=true";
        }
        $iframe.attr('src', $autoPlay);
        GlobalOverlay.show(null, $playButton.data('videoModalId'), true);
    }

    _closeVideoClickHandler(event) {
        const $videoOverlay = $(event.currentTarget).closest('.video-overlay');
        const $iframe = $('iframe', $videoOverlay);

        let $stopUrl = '';
        if ($iframe.attr('src').indexOf('?') > -1) {
            $stopUrl = $iframe.attr('src').split("?")[0];
        } else {
            $stopUrl = $iframe.attr('src');
        }
        $iframe.attr('src', $stopUrl);
        GlobalOverlay.close($videoOverlay.attr('id'));
    }
}

AEM.registerComponent('clienttestimonial', ClientTestimonial);
