import * as AEM from "../../../site/js/juniper";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";

const $ = require('jquery');

export class SolutionOverview extends AEM.Component {
    init() {

        this.$playButton = $('a.play-icon', this.$el);
        this.$closeButton = $('a.close-icon', this.$el);
        this.$imageElm = $('.img-vid', this.$el);
        this.$videoIframe = $('#yt', this.$el);
        this.$rightDiv = $('.right', this.$el);
        this.$leftDiv = $('.left', this.$el);
        this.$titleElm = $('.title', this.$rightDiv);
        this.$leftContentContainer = $('.title', this.$el).parent();
        this.$videoContainer = $('.video-container', this.$el);
        this.$videoTarget = this.$videoContainer.attr("data-video-target");

        this.$modalPlayer = $('#' + this.element.dataset.videoModalId, this.$el);
        this.$playButton2 = $('a.play-icon', this.$modalPlayer);
        this.$closeButton2 = $('a.close-icon', this.$modalPlayer);
        this.$videoIframe2 = $('iframe.vid', this.$modalPlayer);

        this.$rightText = $('.right-text', this.$rightDiv);
        const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

        this._playVideo();
        this._stopVideo();
        this._setClass(width, this.$el, this.$playButton);
    }

    _setClass(width, elm, plyBtn) {

        if (width < 720) {
            elm.addClass("container-fluid");
        } else {
            elm.removeClass("container-fluid");
        }

        if (plyBtn.length) {
            if (plyBtn.position().top < 0 && navigator.userAgent.toLowerCase().search("firefox") < 0) {
                plyBtn.css("bottom", plyBtn.position().top + "px");
            } else {
                plyBtn.css("display", "flex");
            }

        }

        $(window).resize(function () {
            width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if (width < 720) {
                elm.addClass("container-fluid");
            } else {
                elm.removeClass("container-fluid");
            }

            if (plyBtn.length) {
                if (plyBtn.position().top < 0 && navigator.userAgent.toLowerCase().search("firefox") < 0) {
                    plyBtn.css("bottom", plyBtn.position().top + "px");
                } else {
                    plyBtn.css("display", "flex");
                }
            }

        });

    }

    _playVideo() {

        $(this.$playButton).on('click', (event) => {
            event.preventDefault();

            const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if (this.$videoTarget == 'overlay' && width > 719) {

                GlobalOverlay.show(null, this.element.dataset.videoModalId, true);
                this.$videoIframe2[0].src = this.$videoIframe2[0].src + "?autoplay=1&mute=0&enablejsapi=1&controls=1";

            } else {

                this.$imageElm.hide();
                this.$playButton.hide();
                this.$closeButton.show();
                this.$videoIframe[0].src += "?autoplay=1&mute=0&enablejsapi=1&controls=1";

                this.$leftDiv.attr("class", "inline left");
                this.$rightDiv.attr("class", "inline right");
                this.$videoContainer.show("slow");
                $('html, body').animate({ scrollTop: this.$closeButton.offset().top}, 1);

            }
        });

    }


    _stopVideo() {

        $(this.$closeButton).on('click', (event) => {
            event.preventDefault();
            const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

            if (this.$videoTarget == 'overlay' && width > 719) {

                GlobalOverlay.close(this.element.dataset.videoModalId);
                this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0];

            } else {

                this.$leftDiv.attr("class", "inline left");
                this.$rightDiv.attr("class", "inline right");
                this.$imageElm.show();
                this.$playButton.show();

                this.$closeButton.hide();
                this.$videoIframe[0].src = this.$videoIframe[0].src.split("?")[0];
                this.$videoContainer.hide();
                //this.$rightDiv.css("align-self", "center");
                this.$titleElm.focus();
            }
        });

        $(this.$closeButton2).on('click', (event) => {
            event.preventDefault();

            GlobalOverlay.close(this.element.dataset.videoModalId);
            this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0];
        });

    }

}

AEM.registerComponent('solution-overview-sticky', SolutionOverview);



