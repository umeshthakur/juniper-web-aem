import * as AEM from "../../../site/js/juniper";


const $ = require('jquery');
import 'slick-carousel';

export class Carousel extends AEM.Component {
    init() {
        this.$carousel = $('.js-carousel', this.$el);
        this.$prevArrow = $('.js-prev-arrow', this.$el);
        this.$nextArrow = $('.js-next-arrow', this.$el);
        this.$customDots = $('.js-custom-dots', this.$el);
        this.$expandButton = $('.js-expandable', this.$el);
        this.$closeBtn = $('.js-modal-close-button', this.$el);
        this.$modal = $('#zoom-modal');
        this.$imagesZoom = $('.js-image-zoom', this.$modal);
        this.$carouselZoom = $('.js-carousel-zoom', this.$modal);
        this.$prevArrowZoom = $('.js-prev-arrow-zoom', this.$modal);
        this.$nextArrowZoom = $('.js-next-arrow-zoom', this.$modal);
        this.$customDotsZoom = $('.js-custom-dots-zoom', this.$modal);
        this.createCarousel = this._createCarousel;
        //Move Modal
        $('body').append(this.$modal);
        this._imageLoadZoom();
        this._createCarouselZoom();
        this._createCarousel();
        this._bindEvents();
    }

    _bindEvents() {
        $(window).on('resize orientationchange', this._createCarousel());
        this.$expandButton.on('click', (event) => {
            event.preventDefault();
            this._modal();
        });
    }

    _imageLoadZoom() {
        this.$imagesZoom.each((index, ImageZoom) => {
            const image = new Image();
            image.src = $(ImageZoom).attr("src");
            let className = '';
            if (image.naturalWidth > image.naturalHeight) {
                className = 'max-w-100';
            } else if (image.naturalWidth < image.naturalHeight) {
                className = 'max-vh-90';
            } else {
                className = '';
            }
            $(ImageZoom).addClass(className);
        });

    }

    _createCarousel() {
        this.$carousel.not('.slick-initialized').slick({
            arrows: true,
            useTransform: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 0,
            dots: true,
            appendDots: this.$customDots,
            swipeToSlide: true,
            infinite: false,
            prevArrow: this.$prevArrow,
            nextArrow: this.$nextArrow,
            asNavFor: this.$carouselZoom,
            responsive: [
                {
                    breakpoint: '767',
                    settings: {
                        arrows: true,
                        useTransform: false,
                        prevArrow: this.$prevArrow,
                        nextArrow: this.$nextArrow,
                    }
                }, {
                    breakpoint: '768',
                    settings: {
                        variableWidth: true,
                    }
                }]
        });
    }

    _createCarouselZoom() {
        this.$carouselZoom.not('.slick-initialized').slick({
            arrows: true,
            useTransform: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            initialSlide: 0,
            dots: true,
            appendDots: this.$customDotsZoom,
            swipeToSlide: true,
            infinite: false,
            prevArrow: this.$prevArrowZoom,
            nextArrow: this.$nextArrowZoom,
            asNavFor: this.$carousel,
            responsive: [
                {
                    breakpoint: '767',
                    settings: {
                        arrows: true,
                        useTransform: false,
                        prevArrow: this.$prevArrowZoom,
                        nextArrow: this.$nextArrowZoom,
                    }
                }, {
                    breakpoint: '768',
                    settings: {
                        variableWidth: true,
                    }
                }]
        });
    }

    _modal() {

        this.$modal.modal({
            keyboard: false,
            backdrop: 'static',
        });
        this.$carouselZoom.slick('setPosition');
    }

}

AEM.registerComponent('carousel', Carousel);

