const $ = require( 'jquery');
import {Carousel} from "../carousel.js";

const $element = '<div class="carousel__container" data-mod="carousel"></div>';
let $Component = $($element);
// eslint-disable-next-line @typescript-eslint/no-unused-vars
let CarouselComp = '';
beforeEach(() => {
	$Component = $($element);
});

afterEach(() => {
	jest.restoreAllMocks();
	$Component = '';
	CarouselComp = {};
});


describe('Juniper carousel Component Test', () => {
	test('carousel initialized', () => {
		const $carousel = $('<div class="js-carousel" />');
		$($Component).append($carousel);
		CarouselComp = new Carousel($Component).init();
		expect($($Component).find('.js-carousel').hasClass('slick-initialized')).toBe(true);
	});
});
