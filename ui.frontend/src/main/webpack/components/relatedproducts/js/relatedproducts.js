import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

export class RelatedProducts extends AEM.Component {
    init() {

        if (window.innerWidth > 720) {
            let titleMaxHeight = 0;
            this.$rp_title =  $('.prod-info .title', this.$el);
            $(this.$rp_title ).each(function () {
                
                if ($(this).height() > titleMaxHeight) {
                    titleMaxHeight = $(this).height();
                }
            });
            $(this.$rp_title ).height(titleMaxHeight);
        }

        if (window.innerWidth > 720) {
            let titleNoImageMaxHeight = 0;
            this.$rp_titleNoImage =  $('.prod-info .title-noimage', this.$el);
            $( this.$rp_titleNoImage).each(function () {
                
                if ($(this).height() > titleNoImageMaxHeight) {
                    titleNoImageMaxHeight = $(this).height();
                }
            });
            $(this.$rp_titleNoImage).height(titleNoImageMaxHeight);
        }
        
        if (window.innerWidth > 720) {
            let descriptionMaxHeight = 0;
            this.$rp_description = $('.prod-info .description', this.$el);
            $(this.$rp_description).each(function () {
                if ($(this).height() > descriptionMaxHeight) {
                    descriptionMaxHeight = $(this).height();
                }
            });
            $(this.$rp_description).height(descriptionMaxHeight);
        }

    }
};
AEM.registerComponent('relatedproducts', RelatedProducts);