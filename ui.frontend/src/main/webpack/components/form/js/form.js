import * as AEM from "../../../site/js/juniper";

const $ = require('jquery');
let videoURL;
let srcURL;


class FormVideo extends AEM.Component {

    init() {
        this.$expandButton = $('.js-expandable', this.$el);
        this.$expandableColum = this.$el.parents('[data-expandable]');
        this._bindEvents();
    }

    _bindEvents() {
        if (this.$expandButton.length) {
            $(this.$expandButton).on('click', (event) => {
                event.preventDefault();
                this._expandColumn();
            });
        }
    }
    _expandColumn() {

        if (!this.$expandableColum.hasClass('expanded')) {

            this.$expandableColum.addClass('expanded');
           
            //hiding image & showing video
            $('.overview-video .thumbnail').css("display","none");
            $('.overview-video .video-wrapper').css("display","flex");

            srcURL = $('.youtube-video').attr('src');

            if(srcURL === ''){
                srcURL = videoURL;
            }

            //convert src url into embed URL
            if ( srcURL.indexOf('youku') > -1) {
                videoURL = srcURL;
            } else {
                srcURL = srcURL.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
                var videoId = (srcURL[2] !== undefined) ? srcURL[2].split(/[^0-9a-z_\-]/i)[0] : srcURL[0];
                videoURL = 'https://www.youtube.com/embed/' + videoId + '?version=3&enablejsapi=1&autoplay=1';
            }

            //Set embed URL to src Attr.
            $('.youtube-video').attr('src',videoURL);

        } else {

            this.$expandableColum.removeClass('expanded');

            //hiding video & showing image
            $('.overview-video .thumbnail').css("display","block");
            $('.overview-video .video-wrapper').css("display","none");

            //remove src attr valueon close
            $('.youtube-video').attr('src','');

        }
    }
}

AEM.registerComponent('form-video', FormVideo);



