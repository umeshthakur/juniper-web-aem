import * as AEM from "../../../site/js/juniper";
import {BreakpointUtils} from "../../../site/js/BreakpointUtils";
import 'bootstrap/js/dist/collapse';

const TagTemplate = require("./tag.hbs");

class Filters extends AEM.Component {
    init() {
        this.closeBtn = $('.js-close', this.$el);
        this.clearBtn = $('.js-clear', this.$el);
        this.listItems = $('.js-list-items', this.$el);
        this.applyFiltersBtn = $('.js-apply-filters', this.$el);
        this.showFiltersBtn = $('.js-show-filters', this.$el);
        this.$showFiltersText = $('.js-text', this.showFiltersBtn);
        this.showFiltersText = $(this.$showFiltersText).text();
        this.filterContainer = $('.js-filters-container', this.$el);
        this.itemFilters = $('.js-item-filter', this.$el);
        this.tagsContainer = $('.js-tags-container', this.$el);
        this.tagsContainerMobile = $('.js-tags-container-mobile', this.$el);
        this.titleMobile = $('.js-title-mobile', this.$el);
        this.$itemsExpandable = $('.js-item-expandable', this.$el);
        this.$itemsExpandableContainer = $('.js-items-expandable', this.$el);
        this.bindEvents();
        this.titleMobile.hide();

        AEM.url.enable(true);
        AEM.Filter.subscribeToWatchObserver(this);
    }

    bindEvents() {
        this.closeBtn.on('click', (event) => {
            event.preventDefault();
            this.filterContainer.hide();
            this.applyFilters();
            $('body').removeClass('modal-open');
        });
        this.showFiltersBtn.on('click', (event) => {
            event.preventDefault();
            this.filterContainer.show();
            this.filterContainer.scrollTop(0);
            $('body').addClass('modal-open');
        });
        this.applyFiltersBtn.on('click', (event) => {
            event.preventDefault();
            this.applyFilters();
            $('body').removeClass('modal-open');
        });

        this.clearBtn.on('click', (event) => {
            event.preventDefault();
            let selectedItems = this.itemFilters.filter(function (index, element) {
                if ($(element).attr('data-active') === "true") {
                    return true
                }
            });

            if (selectedItems.length > 0) {
                this.removeAll();
                this.enableOrDisableClearAll();    
                this.changeFilterText([]);
            }
        });
        this.itemFiltersEventHandler();
        this.expandable();
        this.disableApplyFilter();
        this.bindApplyFilter();

    }

    itemFiltersEventHandler() {
        this.itemFilters.each((index, item) => {
            $('.item-title', item).on('click', (event) => {
                event.preventDefault();
                this.activeItem(item);
            });
        });
    }

    activeItem(item) {
        if (!$(item).data('active')) {
            this.addFilter(item);
        }
        $(item).attr('data-active', true);
        this.enableOrDisableClearAll();

        const remove = $(item).find('.js-item-remove');
        $(remove).on('click', (event) => {
            event.preventDefault();
            this.removeFilter(item);
            this.enableOrDisableClearAll();
        });
    }

    enableOrDisableClearAll() {
        let selectedItems = this.itemFilters.filter(function (index, element) {
            if ($(element).attr('data-active') === "true") {
                return true
            }
        });

        if (selectedItems && selectedItems.length > 0) {
            if ($(this.clearBtn).hasClass("disabled")) {
                $(this.clearBtn).removeClass("disabled");
            }

            if($(this.tagsContainer).hasClass("hidden")){
                $(this.tagsContainer).removeClass("hidden");
            }
            if($(this.tagsContainerMobile).hasClass("hidden")){
                $(this.tagsContainerMobile).removeClass("hidden");
            }
        } else {
            if (!$(this.clearBtn).hasClass("disabled")) {
                $(this.clearBtn).addClass("disabled")
            }
            if(!$(this.tagsContainer).hasClass("hidden")){
                $(this.tagsContainer).addClass("hidden");
            }
            if(!$(this.tagsContainerMobile).hasClass("hidden")){
                $(this.tagsContainerMobile).addClass("hidden");
            }
        }
    }

    tagEventHandler(tags, tagMobile) {
        tags.each((index, item) => {
            $(item).on('click', (event) => {
                event.preventDefault();
                this.removeFilter(item, tagMobile);
            });
        });
    }

    applyFilters() {
        //Apply Filters and then close
        const tags = this.tagsContainer.find('.tag');
        if (tags.length > 0) {
            if (this.filterContainer.is(':visible')) {
                this.closeBtn.trigger('click');
            }
            // Send filter to logic filter
            this.notifyTags('update');

            //updating text on show filter btn

            this.changeFilterText(tags);
        }
    }

    bindApplyFilter() {
        this.applyFiltersBtn.on('update', (event) => {
            this.disableApplyFilter();
        });
    }

    disableApplyFilter() {
        const tags = this.tagsContainer.find('.tag');
        this.applyFiltersBtn.find('.juniper-btn').addClass('disabled');
        if (tags.length > 0) {
            this.applyFiltersBtn.find('.juniper-btn').removeClass('disabled');
        }
    }

    changeFilterText(tags) {
        const textShowFilter = this.showFiltersText + ' (' + tags.length + ')';
        if (tags.length > 0) {
            this.$showFiltersText.text(textShowFilter);
            if (BreakpointUtils.isMedium() || BreakpointUtils.isSmall()) {
                this.titleMobile.show();
            }
        } else {
            this.$showFiltersText.text(this.showFiltersText);
            if (BreakpointUtils.isMedium() || BreakpointUtils.isSmall()) {
                this.titleMobile.hide();
            }
        }
    }

    addFilter(item) {
        const dataItem = $(item).data();
        dataItem.tagTheme = $(this.tagsContainer).data('theme') ? $(this.tagsContainer).data('theme') : 'tag--jade';

        if (BreakpointUtils.isMedium() || BreakpointUtils.isSmall()) {
            if ($(this.tagsContainer).parents('[data-theme]').data('theme')) {
                dataItem.tagTheme = $(this.tagsContainer).parents('[data-theme]').data('theme').replace('c-theme-', 'tag--');
            }

        }

        const tagTemplate = TagTemplate(dataItem);
        this.tagsContainer.append(tagTemplate);
        this.tagsContainerMobile.append(tagTemplate);

        // Find Tags and adding event Handlers
        const tags = this.tagsContainer.find('.tag');
        const tagsMobile = this.tagsContainerMobile.find('.tag');
        this.tagEventHandler(tags);
        this.tagEventHandler(tagsMobile, true);

        // Send filter to logic filter if is not mobile
        if ((BreakpointUtils.isLarge() || BreakpointUtils.isExtraLarge())) {
            this.notifyTags('update');
        }
        this.applyFiltersBtn.trigger("update");

        AEM.url.add('filters', dataItem.value);
    }

    removeAll() {
        this.tagsContainer.html('');
        this.tagsContainerMobile.html('');
        this.itemFilters.each((index, item) => {
            $(item).removeAttr('data-active');
        });
        //Send to Filter logic remove all filter
        if ((BreakpointUtils.isLarge() || BreakpointUtils.isExtraLarge())) {
            AEM.Filter.notify({type: 'clean'});
        }
        this.applyFiltersBtn.trigger( "update" );

        AEM.url.remove({param: 'filters', removeAll: true});
    }

    removeFilter(item, tagMobile = false) {

        const data = $(item).data();
        const tags = this.tagsContainer.find('.tag');

        this.itemFilters.map((index, filter) => {
            if ($(filter).data('value') === data.value) {
                $(filter).removeAttr('data-active');
            }
        });
        const newTags = tags.filter((index, filter) => {
            return ($(filter).data('value') !== data.value);
        });

        this.tagsContainer.html(newTags.clone());
        this.tagsContainerMobile.html(newTags.clone());

        const tagsMobile = this.tagsContainerMobile.find('.tag');
        const tagsEvent = this.tagsContainer.find('.tag');

        this.tagEventHandler(tagsEvent);
        this.tagEventHandler(tagsMobile,true);

        if (tagMobile) {
            this.changeFilterText(tagsMobile);
        }

        if ((BreakpointUtils.isLarge() || BreakpointUtils.isExtraLarge()) || tagMobile) {
            this.notifyTags('update');
        }

        this.applyFiltersBtn.trigger("update");

        this.enableOrDisableClearAll();
        AEM.url.remove({param: 'filters', value: data.value});

    }

    notifyTags(type) {
        const tags = this.tagsContainer.find('.tag');
        const filteredTags = [];
        tags.map((index, tag) => {
            const Data = $(tag).data('value').split('/');
            filteredTags.push({
                value: Data[1],
                category: Data[0]
            });
        });
        AEM.Filter.notify({data: filteredTags, type: type});
    }

    expandable() {
        this.$itemsExpandable.map((index, item) => {
            const target = $('.js-expandable-target', item);
            const trigger = $('.js-item-trigger', item);
            if (target.length) {
                $(trigger).on('click', (event) => {
                    event.preventDefault();
                    $(this.$itemsExpandable).map((i, elemnt) => {
                        const trigger = $('.js-item-trigger', elemnt);
                        if (!$(trigger).is($(event.currentTarget))) {
                            $(trigger).removeClass('show');
                        }
                    });

                    $(trigger).toggleClass('show');
                    $(target).collapse({
                        parent: this.$itemsExpandableContainer
                    });
                    $(target).collapse('toggle');
                });
            }
        });
    }

    update(f) {
        this.deepLink();
    }

    deepLink() {
        const filterParams = AEM.url.parseFilter({
            params: AEM.url.getParams(),
            replace: true
        });
        if (filterParams.filters && filterParams.filters.length > 0) {
            $.each(filterParams.filters, (i, filter) => {
                this.itemFilters.each((index, item) => {
                    if ($(item).data('value') === filter) {
                        this.activeItem(item);
                    }
                });
            })
        }
    }
}


AEM.registerComponent('filters', Filters);
