import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

export class SolutionList extends AEM.Component {
    init() {
            var highestTitle = 0;
                $('.solutions-list-card-item .solution-description').each(function(){  
                        if($(this).height() > highestTitle){  
                          highestTitle = $(this).height();  
                }
              });    
          $('.solutions-list-card-item .solution-description').height(highestTitle);
        
    }
}
AEM.registerComponent('solutionlist', SolutionList);