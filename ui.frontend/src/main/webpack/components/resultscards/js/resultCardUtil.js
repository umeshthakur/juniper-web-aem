export class ResultsCardsUtil {

    static _heightEqualizer() {
        if(window.innerWidth < 720)
            return;
        let container = document.querySelectorAll('.tech-details');
        let resultsCards = document.querySelector('.results-cards');      
        let filter = document.querySelector('.filter-section');
        if (filter || window.innerWidth < 1024) {
            for (let i = 1; i < container.length; i += 2) {
                if(!resultsCards.contains(container[i]) || !resultsCards.contains(container[i-1]) ) {
                    continue;
                }    
                let tempMax = Math.max(container[i - 1].clientHeight, container[i].clientHeight);
                container[i - 1].setAttribute("style", `height:${tempMax}px`);
                container[i].setAttribute("style", `height:${tempMax}px`);
            }
        } else {
            for (let i = 0; i < container.length; i += 3) {
                let var1 = (container[i]) ? container[i].clientHeight : 0;
                let var2 = (container[i + 1]) ? container[i + 1].clientHeight : 0;
                let var3 = (container[i + 2]) ? container[i + 2].clientHeight : 0;
                let tempMax = Math.max(var1,var2,var3);
                if(resultsCards.contains(container[i])) {
                    container[i].setAttribute("style", `height:${tempMax}px`);
                }
                if (i + 1 < container.length && resultsCards.contains(container[i+1]))
                    container[i + 1].setAttribute("style", `height:${tempMax}px`);
                    
                if (i + 2 < container.length && resultsCards.contains(container[i+2]))
                    container[i + 2].setAttribute("style", `height:${tempMax}px`);
                    ;
            }
        }
    }

    static _backgroundWhite() {
        let filter = document.querySelector('.filter-section');
        if(filter)
            return;
        let imgCont = document.querySelectorAll('.cmp-results-cards .results-cards .cards-row .product-cards .products-section .product-section .product-item .img-wrapper .img-container');
        imgCont.forEach((el) => el.setAttribute("style", "background:radial-gradient(circle at center center, rgb(255 255 255) 0, rgb(255 255 255) 60%, rgb(255 255 255) 60%, #f0f3f5 0, #f0f3f5 0);"));
    }

}