import * as AEM from "../../../site/js/juniper";
import {ResultsCardsUtil} from "../../resultscards/js/resultCardUtil.js";

class Resultscards extends AEM.Component {
    init() {
        this.Cards = $('.js-product-item', this.$el);
        this.CompareButton = $('.js-compare-button').clone();
        this._bindEvents();
        document.addEventListener('DOMContentLoaded', ResultsCardsUtil._heightEqualizer);
        window.addEventListener('resize', this._equalizerCaller);
        document.addEventListener('DOMContentLoaded', ResultsCardsUtil._backgroundWhite);
        AEM.Filter.subscribe(this);

    }

    _bindEvents() {
        $(window).resize(() => {
            this._compareButton();
        });
        this._compareButton();
        
        
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    update(type, data, callback) {
        switch (type) {
            case 'clean':
                this._showAll();
                break;
            case 'update':
                this._filterCards(data);
                break;
            default:
                this._filterCards(data);
        }
    }

    _filterCards(filters) {
        AEM.Filter.filterCards(filters, this.Cards, this._showAll());
    }

    _showAll() {
        //Show All cards
        $(this.Cards).show();
    }

    _equalizerCaller() {
        let container = document.querySelectorAll('.tech-details');
        for(let i = 0; i < container.length;i++) {
            container[i].setAttribute("style", `height:auto`);
        }
        ResultsCardsUtil._heightEqualizer();
    }

    _compareButton() {
        if (AEM.device.mobile()) {
            if ($('.pfh-container').length && $('.pfh-container .button-link').length) {
                this.CompareButton.addClass('button-link--item');
                $('.pfh-container .button-link').append(this.CompareButton);
            }
        }
    }

}


AEM.registerComponent('resultscards', Resultscards);