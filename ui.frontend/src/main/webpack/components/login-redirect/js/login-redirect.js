import * as AEM from "../../../site/js/juniper";

const $ = require('jquery');

export class LoginRedirect extends AEM.Component {
    init() {    
        
        this.$paramName = $('#query-param', this.$el).data('param');
        this._getUrlVars();
        this ._bindEvents();
    }
    
    _getUrlVars() {
        let vars = [], hash;
        const hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(let i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    _bindEvents() {
        
        if(this._getUrlVars()[this.$paramName]){
            
            const url = unescape(decodeURI(this._getUrlVars()[this.$paramName]));
            console.log("query param value :: " + url);
            window.location = url;
        }
    
        
    }

}

AEM.registerComponent('login-redirect', LoginRedirect);
