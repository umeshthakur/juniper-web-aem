import * as AEM from "../../../site/js/juniper";

const $ = require('jquery');
const Sticky = require('sticky-js');

class Overview extends AEM.Component {

    init() {
        this.$expandButton = $('.js-expandable', this.$el);
        this.$expandableColum = $('[data-expandable]', this.$el);
        this.$closeBtn = $('.js-modal-close-button', this.$el);
        this.$carouselTheme = $('.js-theme', this.$el);
        this.theme = this.$expandableColum.data('theme');
        this.$modal = $('#zoom-modal');

        this.$modal.addClass(this.theme);
        this.$carouselTheme.addClass(this.theme);
        this.$accordionStatus = $('.accordion__item__title', this.$el);
        this._expandCollapseAcc();
    }


    _expandCollapseAcc(){
        $(this.$accordionStatus).on('click', (event) => {
            if($(event.currentTarget).parent().find('.juniper-icon.icon-plus').is(':visible')){
                $('.accordion__item .accordion__item__description').hide();
                $('.accordion__item__title--icons .juniper-icon.icon-minus').css('display','none');
                $('.accordion__item__title--icons .juniper-icon.icon-plus').css('display','block');
                $(event.currentTarget).parent().find('.juniper-icon.icon-plus').hide();
                $(event.currentTarget).parent().find('.juniper-icon.icon-minus').show();
                $(event.currentTarget).parent().parent().find('.accordion__item__description').show();
                $(event.currentTarget).parent().parent().scrollIntoView();

            } else if($(event.currentTarget).parent().find('.juniper-icon.icon-minus').is(':visible')){
                    $(event.currentTarget).parent().find('.juniper-icon.icon-minus').hide();
                    $(event.currentTarget).parent().find('.juniper-icon.icon-plus').show();
                    $(event.currentTarget).parent().parent().find('.accordion__item__description').hide();
                    $(event.currentTarget).parent().parent().scrollIntoView();
                }


        })
    }
}

AEM.registerComponent('product-overview', Overview);



