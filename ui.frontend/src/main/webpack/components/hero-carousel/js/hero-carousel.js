import * as AEM from "../../../site/js/juniper";
import {BreakpointUtils} from "../../../site/js/BreakpointUtils";
import {TouchUtility} from "../../../site/js/TouchUtility";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";

export class HeroCarousel extends AEM.Component {
    init() {
        this.progressBarStatus = ['0%','34%','68%','100%'];
        this.sliderTime = Number(this.elData.sliderTime);
        this.$progressBar = $('.js-progress-bar', this.$el);
        this.$progressBar.css('transition', `width ${this.sliderTime + 0.5}ms ease-out`);
        this.$slidesContainer = $('.js-slides-container', this.$el);
        this.$slideItems = $('.js-item', this.$el);
        this.$itemsTextContent = $('.js-item-content-text', this.$el);
        this.$itemsImageContainer = $('.js-item-content-image', this.$el);
        this.$controlsContainer = $('.js-controls-container', this.$el);
        this.$controlsContainerMobile = $('.js-controls-container-mobile', this.$el);

        this.$carouselControls = $('.js-carousel-control', this.$el);
        this.$carouselMobileControls = $('.js-mobile-carousel-control', this.$el);
        this.$progressBar.width(this.progressBarStatus[1]);
        this.counter = 2;
        this.touchHandler = new TouchUtility(this.$slidesContainer.get(0));
        this.touchHandler.setLeftHandler(this._leftSwipeHandler.bind(this));
        this.touchHandler.setRightHandler(this._rightSwipeHandler.bind(this));

        if (this.elData.autoRotation && !this._isMobileMode()) {
            this._beginAutoRotation();
        } else {
            this._selectSlide(0);
        }
        this._bindEvents();
    }

    _bindEvents() {
        this.$carouselControls.on('click', (event) => {
            this._controlClickHandler(event);
        });
        this.$carouselMobileControls.on('click', (event) => {
            this._mobileControlClickHandler(event);
        });
        $('.js-play-video-btn', this.$el).on('click', (event) => {
            this._playVideoClickHandler(event);
        });
        $('.js-modal-close-button', this.$el).on('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            this._closeVideoClickHandler(event);
        });
        // Due to some edge cases when loading the page we need to calculate the max height of the content
        // so as to make it all slides look consistent.
        $(window).load( () => {
            this._setCarouselHeight();
        });
        // Calculate carousel's height when there's a resize event.
        $(window).resize( () => {
            // Clear elements styles.
            this.$slideItems.removeAttr('style');
            this.$slidesContainer.removeAttr('style');
            this.$itemsTextContent.removeAttr( 'style' );
            this.$itemsImageContainer.removeAttr( 'style' );
            // Re-set carousel's height.
            this._setCarouselHeight();
        });
    }

    _isMobileMode () {
        return BreakpointUtils.isSmall() || BreakpointUtils.isExtraSmall();
    }

    _setCarouselHeight() {
        if (!this._isMobileMode ()) {
            this.$itemsTextContent.css('padding-bottom', this.$controlsContainer.height() + 20);
            const heights = [];
            this.$slideItems.each( (index, element) => {
                heights.push($(element).height());
            });
            const maxHeight = Math.max(...heights);
            this.$slidesContainer.height(maxHeight);
            this.$slideItems.height(maxHeight);
        }else{
            this.$itemsTextContent.css('padding-bottom', this.$controlsContainerMobile.height() + 60);
            const heights = [];
            this.$slideItems.each( (index, element) => {
                heights.push($(element).height());
            });
            const maxHeight = Math.max(...heights);
            this.$slidesContainer.height(maxHeight);
            this.$slideItems.height(maxHeight);
            this.$itemsImageContainer.css({'position':"absolute","bottom":0});
        }
    }

    _beginAutoRotation () {
        this.autoAdvance = setInterval( () => {
            const $activeSlide = this.$slideItems.filter('.active');
            const activeSlideIndex = $activeSlide.data('slideIndex');
            let nextSlideIndex = activeSlideIndex + 1;
            if (nextSlideIndex > 2) {
                nextSlideIndex = 0;
            }
            this._activateSlide(nextSlideIndex);
            if (this.counter <= 3) {
                this.$progressBar.width(this.progressBarStatus[this.counter]);
                this.counter++;
            } else {
                if (this.elData.infiniteLoop) {
                    this.$progressBar.css('transition', 'none');
                    this.$progressBar.width( '0');
                    this.counter = 2;
                    this.$progressBar.width(this.progressBarStatus[1]);
                    this.$progressBar.css('transition', '');
                } else {
                    this._selectSlide(0);
                }
            }
        }, this.sliderTime);
    }

    _selectMobileSlide(targetSlideIndex) {
        this._activateSlide(targetSlideIndex);
    }

    _selectSlide(targetSlideIndex) {
        this._activateSlide(targetSlideIndex);
        clearInterval(this.autoAdvance);

        this.$progressBar.css('transition', 'none');
        if (targetSlideIndex === 2) {
            this.$progressBar.css('width', 'calc(100% - ' + this.progressBarStatus[targetSlideIndex] + ')');
        } else {
            this.$progressBar.css('width', this.progressBarStatus[1]);
        }
        this.$progressBar.css('margin-left', this.progressBarStatus[targetSlideIndex]);
    }

    _activateSlide(targetSlideIndex) {
        const $targetSlide = this.$slideItems.filter(`[data-slide-index="${targetSlideIndex}"]`);
        const $activeSlide = this.$slideItems.filter('.active');
        $activeSlide.removeClass('active');
        this.$carouselControls.removeClass('active');
        this.$carouselMobileControls.removeClass('active');
        $targetSlide.addClass('active');
        this.$carouselControls.filter(`[data-slide-to="${targetSlideIndex}"]`).addClass('active');
        this.$carouselMobileControls.filter(`[data-slide-to="${targetSlideIndex}"]`).addClass('active');
    }

    //   Event Handlers
    _controlClickHandler (event) {
        this._selectSlide($(event.currentTarget).data('slideTo'));
    }

    _mobileControlClickHandler (event) {
        this._selectMobileSlide($(event.currentTarget).data('slideTo'));
    }

    _playVideoClickHandler (event) {
        const $playButton = $(event.currentTarget);
        const currentIndex  = $playButton.parents('.js-item').data('slideIndex');
        
        const $videoOverlay = $('#' + $playButton.data( 'videoModalId' )).closest('.video-overlay' );
        const $iframe = $( 'iframe' , $videoOverlay);
        let $autoPlay = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $autoPlay = $iframe.attr( 'src' ).split("?")[0] + "?enablejsapi=1";
        } else{
            $autoPlay = $iframe.attr( 'src' ) + "?enablejsapi=1";
        }
        $iframe.attr( 'src' , $autoPlay);
        GlobalOverlay.show(null, $playButton.data('videoModalId'), true);
        this._selectSlide(currentIndex);
    }

    _closeVideoClickHandler (event) {
        const $videoOverlay = $(event.currentTarget).closest('.video-overlay');
        const $iframe = $('iframe', $videoOverlay);
        
        let $stopUrl = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $stopUrl = $iframe.attr( 'src' ).split("?")[0] + "?enablejsapi=1";
        } else{
            $stopUrl = $iframe.attr( 'src' ) + "?enablejsapi=1";
        }
        $iframe.attr( 'src' , $stopUrl);
        GlobalOverlay.close($videoOverlay.attr('id'));
    }

    _leftSwipeHandler() {
         const $activeSlide = this.$slideItems.filter('.active');
         const activeSlideIndex = $activeSlide.data('slideIndex');
         if (activeSlideIndex < 2) {
            this._selectSlide(activeSlideIndex + 1);
         }
    }

    _rightSwipeHandler() {
         const $activeSlide = this.$slideItems.filter('.active');
         const activeSlideIndex = $activeSlide.data('slideIndex');
         if (activeSlideIndex > 0) {
            this._selectSlide(activeSlideIndex - 1);
         }
    }
}

AEM.registerComponent('hero-carousel', HeroCarousel);

