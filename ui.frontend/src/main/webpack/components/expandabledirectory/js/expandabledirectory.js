import * as AEM from "../../../site/js/juniper";
import LazyLoad from "vanilla-lazyload";

class ExpandableDirectory extends AEM.Component {
	init(){
        this.$defaultState = $('.exp-dir-cards', this.$el);
        this.$expandState = $('.icon-close', this.$el);
        this.$socialShare = $('.exp-dir-scial-links', this.$el);
        this._bindEvents();
        new LazyLoad({
          });
    }
	_bindEvents() {
		$(this.$defaultState).on('click', (event) => {
            event.preventDefault();
            if($(event.currentTarget).hasClass('expanded')){
                $(event.currentTarget).removeClass('expanded');
                $(event.currentTarget).closest('.exp-dir-cards-details').addClass('expanded');

            }
            else {
                $('.exp-dir-cards').removeClass('expanded').css('opacity', 0.99).css('display','grid');
                $('.exp-dir-cards-details').removeClass('expanded').css('opacity', 0).css('display', 'none');
                $(event.currentTarget).addClass('expanded').fadeTo("10000", 0).css('display', 'none');
                $(event.currentTarget).closest('.exp-dir-cards-container').find('.exp-dir-cards-details').addClass('expanded').fadeTo("10000", .99).css('display','grid');
                $(event.currentTarget).parent().addClass('expanded');
            }
        });
        $(this.$expandState).on('click', (event) => {
            event.stopPropagation();
            $(event.currentTarget).closest('.exp-dir-cards-details').removeClass('expanded').fadeTo("20000", 0).css('display','none');
            $(event.currentTarget).closest('.exp-dir-cards-container').find('.exp-dir-cards').removeClass('expanded').fadeTo("20000", .99).css('display', 'grid');
		});
    }
};


AEM.registerComponent('expandable-directory', ExpandableDirectory);
