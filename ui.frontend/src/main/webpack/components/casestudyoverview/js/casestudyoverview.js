import * as AEM from "../../../site/js/juniper";
import { GlobalOverlay } from "../../globaloverlay/js/globaloverlay";

const $ = require("jquery");
var videoURL;
var srcURL;

export class CasestudyOverview extends AEM.Component {
  init() {
    this.$expandButton = $(".js-expandable", this.$el);
    this.$rightDiv = $(".right", this.$el);
    this.$leftDiv = $(".left", this.$el);
    this.$alignment = $(".alignment", this.$el);
    this.$thumbnail = $(".thumbnail", this.$el);
    this.$youtubeVideo = $(".youtube-video", this.$el);
    this.$videoTarget = this.$rightDiv.attr("data-video-target");
    this.$modalPlayer = $("#" + this.element.dataset.videoModalId, this.$el);
    this.$closeButton2 = $("a.close-icon", this.$el);
    this.$videoIframe2 = $("iframe.vid", this.$modalPlayer);
    this._bindEvents();
  }

  _bindEvents() {
    if (this.$expandButton.length) {
      $(this.$expandButton).on("click", (event) => {
        event.preventDefault();
        this._expandColumn();
      });
      $(this.$closeButton2).on("click", (event) => {
        event.preventDefault();
        this._expandColumn();
      });
    }
  }

  _expandColumn() {
    var browserWidth = $(window).width();
    var imageHeight = this.$thumbnail.height();

    if (this.$videoTarget == "overlay" && browserWidth > 719) {
      if (!this.$leftDiv.hasClass("expanded")) {
        this.$leftDiv.addClass("expanded");
        GlobalOverlay.show(null, this.element.dataset.videoModalId, true);
        this.$videoIframe2[0].src =
          this.$videoIframe2[0].src +
          "?autoplay=1&mute=0&enablejsapi=1&controls=1";
      } else {
        this.$leftDiv.removeClass("expanded");
        GlobalOverlay.close(this.element.dataset.videoModalId);
        this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0];
      }
    } else {
      if (!this.$leftDiv.hasClass("expanded")) {
        this.$leftDiv.addClass("expanded");
        if (this.$alignment.hasClass("inverse")) {
          this.$alignment.addClass("jsExpnaded");
        } else {
          this.$alignment.addClass("jsExpnaded-inverse");
        }
        $(".overview-video .thumbnail",this.$el).css("display", "none");
        $(".overview-video .video-wrapper",this.$el).css("display", "flex");
        srcURL = $(".youtube-video",this.$el).attr("src");
        if (srcURL === "") {
          srcURL = videoURL;
        }
        if (srcURL.indexOf("youku") > -1) {
          videoURL = srcURL;
        } else {
          srcURL = srcURL.split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
          var videoId =
            srcURL[2] !== undefined
              ? srcURL[2].split(/[^0-9a-z_\-]/i)[0]
              : srcURL[0];
          videoURL =
            "https://www.youtube.com/embed/" +
            videoId +
            "?version=3&enablejsapi=1&autoplay=1";
        }
        $(".youtube-video",this.$el).attr("src", videoURL);
      } else {
        this.$leftDiv.removeClass("expanded");
        if (this.$alignment.hasClass("inverse")) {
          this.$alignment.removeClass("jsExpnaded");
        } else {
          this.$alignment.removeClass("jsExpnaded-inverse");
        }
        $(".overview-video .video-wrapper",this.$el).css("display", "none");
        $(".overview-video .thumbnail",this.$el).css("display", "block");
        $(".youtube-video",this.$el).attr("src", "");
      }
    }
  }
}

AEM.registerComponent("case-study-overview", CasestudyOverview);
