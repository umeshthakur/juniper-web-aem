import * as AEM from "../../../site/js/juniper";

const $ = require('jquery');

export class FlexibleCTA extends AEM.Component {
	init() {		
		
		const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);		
		
		this._setClass(width, this.$el);
		
	}
	
	_setClass(width, elm){
		
		if(width < 720){					
			elm.addClass("container-fluid");			
		}else {
			elm.removeClass("container-fluid");
		}
		
		$(window).resize(function() {
			width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
			
			if(width < 720){					
				elm.addClass("container-fluid");			
			}else {
				elm.removeClass("container-fluid");
			}
		});
		
	}
	
	
}

AEM.registerComponent('flexible-cta', FlexibleCTA);



