import * as AEM from "../../../site/js/juniper";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";

export class CaseStudyHomepage extends AEM.Component {
    init() {
        this.$playVideoButton = $('.js-play-video-btn', this.$el);
        this.$closeVideoButton = $('.js-modal-close-button', this.$el);
        this._bindEvents();
    }

    _bindEvents() {
        this.$playVideoButton.on('click', (event) => {
            this._playVideoClickHandler(event);
        });
        this.$closeVideoButton.on('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            this._closeVideoClickHandler(event);
        });
    }

    //   Event Handlers
    _playVideoClickHandler (event) {
        const $playButton = $(event.currentTarget);
        GlobalOverlay.show(null, $playButton.data('videoModalId'), true);
    }

    _closeVideoClickHandler (event) {
        const $videoOverlay = $(event.currentTarget).closest('.video-overlay');
        const $iframe = $('iframe', $videoOverlay);
        // Reset iframe src so as to stop any action happening inside.
        $iframe.attr('src', $iframe.attr('src'));
        GlobalOverlay.close($videoOverlay.attr('id'));
    }

}

AEM.registerComponent('case-study-homepage', CaseStudyHomepage);

