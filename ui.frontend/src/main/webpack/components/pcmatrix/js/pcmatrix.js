import * as AEM from '../../../site/js/juniper';
import {GlobalOverlay} from '../../globaloverlay/js/globaloverlay';

export class ProductCompMatrix extends AEM.Component {
    init() {
        this._bindEvents(this);        
    }
    
    _bindEvents(globalObj){
        const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        
        const row = $('.visiblerow', this.$el);
        const expandableRows = $('.isExpandable', this.$el);
        const headerrow = $('.mobile', this.$el);
        const mobileheader = $('.mobile > td', this.$el);
        const totalcols = $(row).first().children().length;
        const greyheader = $('.head > td', this.$el);        
        
        $(greyheader).each(function() { 
            if(width > 719){
                $(this).attr('colspan',totalcols);
            } else {
                $(this).attr('colspan',totalcols-1); 
            }
        });
        
        $(mobileheader).each(function() { 
            if(width > 719){
                $(this).attr('colspan',totalcols);
            } else {
                $(this).attr('colspan',totalcols-1); 
            }
        });

        $(expandableRows).each(function() {
            const item = $(this);

            $(item).on("click", function() {
                const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                if(width > 719) {
                    $(item).toggleClass("graybg");
                    $(item).toggleClass("add-border");
                    $(item).next(".expanded").toggle();
                    $(this).find("td.clickopen").toggleClass("open");
                }
            });
        });
        
        $(window).resize(function() {
            const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            
            if (width < 720) {
                $(expandableRows).each(function () {
                    $(this).removeClass("graybg");
                    $(this).addClass("add-border");
                    $(this).next(".expanded").hide();
                    $(this).find("td.clickopen").removeClass("open");
                });
            }
            
            $(greyheader).each(function() { 
                if(width > 719){
                    $(this).attr('colspan',totalcols);
                } else {
                    $(this).attr('colspan',totalcols-1); 
                }
            });
            
            $(mobileheader).each(function() { 
                if(width > 719){
                    $(this).attr('colspan',totalcols);
                } else {
                    $(this).attr('colspan',totalcols-1); 
                }
            });
        });
        
        const col = $('.add-rating', this.$el);        
        $(col).each(function() {
            const rating = $(this).data("rating");           
            
            const prev = $(this).prev();
            let styles = "dot dot-black";
            
            // dot color green if first column            
            if(prev.attr("class").indexOf("clickopen") > -1){
                styles = "dot dot-green";
            }
            let html = "", cnt = 0;
            
            while(cnt < rating){
                if(cnt == 0){
                    html += "<span class='" + styles + "'></span>";
                } else {
                    html += "<span class='" + styles + " add-margin'></span>";
                }                
                cnt++;
            }
            
            if(html == ""){
                html = "<span class='dot'></span>";
                cnt++;
            }
            
            if(cnt < 5){
                for(let i=0; i<5-cnt; i++){                    
                    html += "<span class='dot add-margin'></span>"; 
                }
            }
            $(this).append(html);
        });
        
        //video
        $('a.overlay-video' , globalObj.$el).each(function() {                
                $(this).on( 'click' , (event) => {
                    globalObj._playVideoClickHandler(event);
                });
        });
        
        $( '.close-icon' , globalObj.$el).each(function() {
            $(this).on( 'click' , (event) => {
                event.stopPropagation();
                event.preventDefault();
                globalObj._closeVideoClickHandler(event);
            });
        });
        
    }
    
    _playVideoClickHandler (event) {
            
            const $playButton = $(event.currentTarget);
            const $videoOverlay = $('#' + $playButton.data( 'videoModalId' )).closest('.video-overlay' );
            const $iframe = $( 'iframe' , $videoOverlay);
            
            let $autoPlay = '';
            if($iframe.attr( 'src' ).indexOf('?') > -1){
            $autoPlay = $iframe.attr( 'src' ).split('?')[0] + '?autoplay=true';
            } else{
                $autoPlay = $iframe.attr( 'src' ) + '?autoplay=true';
            }
            $iframe.attr( 'src' , $autoPlay);
            GlobalOverlay.show( null , $playButton.data( 'videoModalId' ), true );
        }
        
    _closeVideoClickHandler (event) {
            const $videoOverlay = $(event.currentTarget).closest('.video-overlay' );
            const $iframe = $( 'iframe' , $videoOverlay);       
            
            let $stopUrl = '';
            if($iframe.attr( 'src' ).indexOf('?') > -1){
            $stopUrl = $iframe.attr( 'src' ).split('?')[0];
            } else{
                $stopUrl = $iframe.attr( 'src' );
            }
            $iframe.attr( 'src' , $stopUrl);
            GlobalOverlay.close($videoOverlay.attr( 'id' ));
    }
}

AEM.registerComponent('prod-comp-matrix', ProductCompMatrix);
