import * as AEM from "../../../site/js/juniper";
import {Observer} from "../../../site/js/Observer";


export class NavigationTopPanel extends AEM.Component {
    init() {
        this.currentWidth = this.element.offsetWidth;
        this.$backdrop = $('.js-backdrop', this.$el);
        this.$contactUsBtn = $('.js-contact-us-btn', this.$el);
        this.$loginBtn = $('.js-login-btn', this.$el);
        this.$loginLabel = $('.js-login-label', this.$el);
        this.$accountMenu = $('.js-account-menu', this.$el);
        this.$languageSelector = $('.js-language-selector', this.$el);
        this.$languageMenu = $('.js-language-menu', this.$el);
        this.$usernameLabel = $('.js-username-label', this.$el);
        this.$accountToggleIcon = $('.js-username-toggle-icon', this.$el);
        AEM.UserService.init();
        this._bindEvents();
    }

    _bindEvents() {
        this.$languageSelector.on('click', () => {
            this._languageButtonHandler();
        });
        this.$contactUsBtn.on('click', () => {
            this._contactUsButtonHandler();
        });
        this.$loginBtn.on('click', () => {
            this._loginButtonHandler();
        });
        $('.js-language-close', this.$el).on('click', () => {
            this._closeLanguageMegaMenu();
        });
        this.$backdrop.on('click', () => {
            this._closeLanguageMegaMenu();
            this._closeAccountMegaMenu();
        });
        $('.js-account-close', this.$el).on('click', () => {
            this._closeAccountMegaMenu();
        });
        $('.js-logout-link', this.$el).on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._logoutUserHandler();
        });
        Observer.addObserver(AEM.Constants.Events.UserDataReady, () => {
            if (AEM.UserService.isLoggedIn()) {
                this.$usernameLabel.text(AEM.UserService.getUserFirstName());
                this.$usernameLabel.show();
                this.$accountToggleIcon.show();
                this.$loginLabel.hide();
            }
        });
        $(window).resize( () => {
            if (this.currentWidth !== this.element.offsetWidth) {
                this.currentWidth = this.element.offsetWidth;
                this._closeLanguageMegaMenu();
                this._closeAccountMegaMenu();
            }
        });
    }
    // Handlers
    _contactUsButtonHandler () {
        window.location.href = this.$contactUsBtn.data('link');
    }
    _loginButtonHandler () {
        const currentUrl = window.location.pathname;
        if (AEM.UserService.isLoggedIn()) {
            if (this.$languageSelector.hasClass('selected')) {
                this._closeLanguageMegaMenu();
            }
            if (!this.$loginBtn.hasClass('selected')) {
                this.$backdrop.show();
            }
            this.$accountMenu.collapse('toggle');
            this.$loginBtn.toggleClass('selected');
        } else {
            window.location.href = this.$loginBtn.data('link') + "?saml_request_path=" + currentUrl;
        }
    }
    _languageButtonHandler () {
        if (this.$loginBtn.hasClass('selected')) {
            this._closeAccountMegaMenu();
        }
        if (!this.$languageMenu.hasClass('selected')) {
            this.$backdrop.show();

            // check if any main menu open
            this.$mainNav = $('#mainNav');
            this.$menuselected = $(".js-megamenu",this.$mainNav);
            if(this.$menuselected.hasClass("show")){
                $(".js-close",this.$menuselected).trigger("click");
            }
            // Check if there is search menu open
            this.searchContainer = $(".navigation__container__nav__search",this.$mainNav);
            this.searchContainerMenu = $(".js-megamenu",this.searchContainer);
            if(this.searchContainerMenu.hasClass("show")){
                $(".js-close",this.searchContainerMenu).trigger("click");
            }
 
        }
        this.$languageMenu.collapse('toggle');
        this.$languageSelector.toggleClass('selected');
    }
    _logoutUserHandler () {
        //AEM.UserService.logout(window.location.pathname);
        AEM.UserService.logout(this.$loginBtn.data('link'))
    }
    // Utilities
    _closeLanguageMegaMenu(){
        this.$languageMenu.collapse('hide');
        this.$languageSelector.removeClass('selected');
        this.$backdrop.hide();
    }
    _closeAccountMegaMenu(){
        this.$accountMenu.collapse('hide');
        this.$loginBtn.removeClass('selected');
        this.$backdrop.hide();
    }

};


AEM.registerComponent('top-panel', NavigationTopPanel);
