import * as AEM from "../../../site/js/juniper";
import {GlobalOverlay} from "../../globaloverlay/js/globaloverlay";

import 'bootstrap/js/dist/modal';

const $ = require('jquery');

export class ResourceCenterVideo extends AEM.Component {
    init() {

        this.$videoIframe1 = $('#yt1', this.$el);

        this.$rightDiv = $('.right', this.$el);

        this.$leftDiv = $('.left', this.$el);

        this.$playButton = $('a.play-icon', this.$rightDiv);
        this.$closeButton = $('a.close-icon', this.$rightDiv);
        this.$playIcon = $('.icon-play-btn', this.$rightDiv);
        this.$videoThumb = $('.img-vid', this.$rightDiv);

        this.$videoContainer = $('.video-container', this.$rightDiv);
        this.$modalPlayer = $('#' + this.element.dataset.videoModalId, $(this.$el).parent());
        this.$videoIframe2 = $('iframe.vid', this.$modalPlayer);
        this.$playButton2 = $('a.play-icon', this.$modalPlayer);
        this.$closeButton2 = $('a.close-icon', this.$modalPlayer);

        this.$leftDescCta = $('.desc', this.$leftDiv);
        this.$leftButtonCta = $('.button-link', this.$leftDiv);

        $('.video-desc', this.$rightDiv).append(this.$leftDescCta.clone());
        $('.video-desc', this.$rightDiv).append(this.$leftButtonCta.clone());

        this.$rightDescCta = $('.desc', this.$rightDiv);
        this.$rightButtonCta = $('.button-link', this.$rightDiv);



        this.$cmpvariant = this.$leftDiv.attr("cmp-variant");

        const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        this.$bgColor = this.$leftDiv.attr("data-bg-color");

        let bgColor;

        if(this.$bgColor == 'c-theme-blue') {
            bgColor = '#e5f2f8';
        } else if(this.$bgColor == 'c-theme-gray-light') {
            bgColor = '#888d90';
        } else if(this.$bgColor == 'c-theme-green') {
            bgColor = '#f2f7eb';
        } else if(this.$bgColor == 'c-theme-jade') {
            bgColor = '#e5f5f1';
        } else if(this.$bgColor == 'c-theme-orange') {
            bgColor = '#fcf0e5';
        } else if(this.$bgColor == 'c-theme-purple') {
            bgColor = '#e4e1e8';
        } else if(this.$bgColor == 'c-theme-silver') {
            bgColor = '#f0f3f5';
        } else if( this.$bgColor == 'c-theme-dark-green') {
            bgColor = '#448500';
        }  else if( this.$bgColor == 'c-theme-green-table') {
            bgColor = '#749734';
        }   else if( this.$bgColor == 'c-theme-gray-table') {
            bgColor = '#F1F2F1';
        }  else if(this.$bgColor == 'c-theme-white') {
            bgColor = '#fff';
        } else if(this.$bgColor == 'c-theme-soft-gray') {
            bgColor = '#fafafa';
        }  else if(this.$bgColor == 'c-theme-black') {
            bgColor = '#000';
        }  else {
            bgColor = '#e5f5f1';
        }

        this._playVideo(width);
        this._stopVideo(width);
        this._renderBgCircle(bgColor, width);
        this._setClass(width, this.$el, bgColor, this.$playButton, this.$videoThumb, this.$playIcon, this);

    }

    _setClass(width, elm, bgColor, playBtn, videoThm, playIcn, ref){
            $(window).resize(function() {
                width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                //bg circle
                ref._renderBgCircle(bgColor, width);
            });
        }


    _playVideo(width) {
        if(width == 'undefined' || width == ''){
            width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
        }

        $(this.$playButton).on('click', (event) => {
            event.preventDefault();

            if(width < 720){
                this.$videoIframe1.show();
                this.$videoContainer.addClass("add-video-padding");
                
                if(this.$videoIframe1[0].src.indexOf('?') > -1){
                    this.$videoIframe1[0].src = this.$videoIframe1[0].src.split("?")[0] + "?enablejsapi=1";
                } else{
                    this.$videoIframe1[0].src = this.$videoIframe1[0].src + "?enablejsapi=1";
                }
                //this.$videoIframe1[0].src += "?autoplay=1&mute=0&enablejsapi=1";
                this.$playButton.hide();
                this.$closeButton.show();
                this.$videoThumb.hide();
                this.$videoContainer.height("375px");
            } else {
                GlobalOverlay.show(null, this.element.dataset.videoModalId, true);
                
                if(this.$videoIframe2[0].src.indexOf('?') > -1){
                    this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0] + "?enablejsapi=1";
                } else{
                    this.$videoIframe2[0].src = this.$videoIframe2[0].src + "?enablejsapi=1";
                }
                //this.$videoIframe2[0].src = this.$videoIframe2[0].src + "?autoplay=1&mute=0&enablejsapi=1";
            }

        });

    }


    _stopVideo(width) {
            if(width == 'undefined' || width == ''){
                width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            }
            $(this.$closeButton).on('click', (event) => {
                event.preventDefault();
                if(width < 720){
                    this.$videoIframe1[0].src = this.$videoIframe1[0].src.split("?")[0] + "?enablejsapi=1";
                    this.$playButton.show();
                    this.$closeButton.hide();
                    this.$videoThumb.show();
                    this.$videoIframe1.hide();
                    this.$videoContainer.removeClass("add-video-padding");
                    this.$videoContainer.height("auto");
                } else {
                    GlobalOverlay.close(this.element.dataset.videoModalId);
                    this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0] + "?enablejsapi=1";
                }
            });

            $(this.$closeButton2).on('click', (event) => {
                event.preventDefault();
                GlobalOverlay.close(this.element.dataset.videoModalId);
                this.$videoIframe2[0].src = this.$videoIframe2[0].src.split("?")[0] + "?enablejsapi=1";
            });
        }

    _renderBgCircle(bgColor, width){
        if(width < 720){
            if(this.$cmpvariant !='v3')
            {
            this.$leftDiv.css("background", "radial-gradient(circle at 48% 15%," + bgColor + " 0," +
                    bgColor + " 13%," + bgColor + " 13%, #fff 0, #fff 0)");
            this.$rightDiv.css("background", "none");
            }
        }

        if(width >= 720 && width < 1024){
            this.$rightDiv.css("background", "radial-gradient(circle at 50% 35%," + bgColor + " 0," +
                    bgColor + " 35%," + bgColor + " 35%,#fff 0,#fff 0)");
            this.$leftDiv.css("background", "none");
        }

        if(width >= 1024 && width < 1439){
            this.$rightDiv.css("background", "radial-gradient(circle at 50% 35%," + bgColor + " 0," +
                    bgColor + " 30%," + bgColor + " 30%,#fff 0,#fff 0)");
            this.$leftDiv.css("background", "none");
        }

        if(width > 1439){
            this.$rightDiv.css("background", "radial-gradient(circle at 50%  35%," + bgColor + " 0," +
                    bgColor + " 35%," + bgColor + " 35%,#fff 0,#fff 0)");
            this.$leftDiv.css("background", "none");
        }
    }
}

AEM.registerComponent('resource-center-video', ResourceCenterVideo);