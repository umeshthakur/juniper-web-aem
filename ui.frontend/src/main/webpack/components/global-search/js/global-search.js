import * as AEM from '../../../site/js/juniper';
import {Observer} from "../../../site/js/Observer";
import {GlobalOverlay} from '../../globaloverlay/js/globaloverlay';
import {Suggester} from './Suggester';

const Selectors = {
    checkbox: 'input[type="checkbox"]',
    checkboxChecked: 'input[type="checkbox"]:checked',
    radio: 'input[type="radio"]',
    radioChecked: 'input[type="radio"]:checked',
    filterClearBtn: '.js-filter-clear-btn',
    filtersContainer: '.js-filters-container',
    filterContainer: '.js-filter-container',
    pageBtn: '.js-page-btn'
};
const MessagePlaceholder = '{0}';

const FeaturedResultsHbsTemplate = require('./featured-results.hbs');
const CoveoResultsHbsTemplate = require('./coveo-results.hbs');
const PageIndicatorHbsTemplate = require('./page-indicator.hbs');

export class GlobalSearch extends AEM.Component {
    init() {
        this.$filterToggles = $('.js-filter-toggle', this.$el);
        this.$searchInput = $('.js-search-input', this.$el);
        this.$searchIcon = $('.js-search-icon', this.$el);
        this.$searchErrorMsg = $('.js-error-message', this.$el);
        this.$featuredResults = $('.js-featured-results', this.$el);
        this.$coveoResults = $('.js-coveo-results', this.$el);
        this.$resultsSummary = $('.js-results-summary', this.$el);
        this.$feedbackButton = $('.js-feedback-link', this.$el);
        this.$mobileFiltersToggle = $('.js-mobile-filters-toggle', this.$el);
        this.$searchClearIcon = $('.js-search-clear-icon', this.$el);
        this.$resetFiltersButton = $('.js-filter-reset-all', this.$el);
        this.$expandButtons = $('.js-expand', this.$el);
        this.$collapseButtons = $('.js-collapse', this.$el);
        this.$filtersContainer = $(Selectors.filtersContainer, this.$el);
        this.$clearFilterButtons = $(Selectors.filterClearBtn, this.$el);
        this.$languageFilter = $('.js-filter-language', this.$el);
        this.$resultsContainer = $('.js-results-container', this.$el);
        this.$resultsCount = $('.js-results-count', this.$el);
        this.$noResultsFound = $('.js-no-results', this.$el);
        this.$loader = $('.js-loader', this.$el);
        this.$overlay = $('.js-results-overlay', this.$el);
        this.$pagination = $('.js-pagination', this.$el);
        this.$paginationPrev = $('.js-previous-page', this.$pagination);
        this.$paginationNext = $('.js-next-page', this.$pagination);
        this.$welcomeMsg = $('.js-welcome-msg', this.$el);
        this.$linksPublic = $('.js-links-public', this.$el);
        this.$linksSecure = $('.js-links-secure', this.$el);
        this.currentQuery = '';
        this.isLoading = false;
        this.pageStart = 0;
        this.currentPage = 0;
        this.pageSize = Number(this.elData.pageSize);
        this.pagesToDisplay = 10;
        this.pageTotal = 0;
        this._setInitialStatus();
        this._bindEvents();
        AEM.UserService.init();
        this.suggester = new Suggester(
            this.$searchInput[0], this.elData.publicSearchKey, this.elData.coveoSuggestEndpoint, this.$searchErrorMsg);
    }

    _bindEvents() {
        this.$searchInput.on('keyup', (event) => {
            const keyCode = (event.keyCode ? event.keyCode : event.which);
            event.preventDefault();
            event.stopPropagation();
            if (keyCode === 13) { // Handle enter key.
                this._handleSearchAction();
            } else if (!this.$searchInput.val()) {
                this._handleSearchInputClear();
            }
        });
        this.$searchIcon.on('click', () => {
            this._handleSearchAction();
        });
        this.$searchClearIcon.on('click', () => {
            this._handleSearchInputClear();
        });
        this.$filterToggles.on('click', (event) => {
            const $element = $(event.currentTarget);
            event.preventDefault();
            if ($element.hasClass('js-multiple-selection')) {
                this._filterCheckboxHandler($element);
            } else {
                this._filterRadioHandler($element);
            }
        });
        this.$resetFiltersButton.on('click', (event) => {
            event.preventDefault();
            this._filterResetAllHandler();
        });
        this.$mobileFiltersToggle.on('click', (event) => {
            event.preventDefault();
            this._mobileFiltersToggleHandler();
        });
        this.$expandButtons.on('click', (event) => {
            this._filterToggleHandler(event.currentTarget);
        });
        this.$collapseButtons.on('click', (event) => {
            this._filterToggleHandler(event.currentTarget);
        });
        this.$clearFilterButtons.on('click', (event) => {
            this._filterClearHandler(event.currentTarget);
        });
        $('.js-login-link', this.$el).on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._redirectToLogin();
        });
        $('.js-logout-link', this.$el).on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._logoutUser();
        });
        this.$feedbackButton.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._feedbackButtonHandler(event.currentTarget);
        });
        this.$paginationNext.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._paginationNextHandler();
        });
        this.$paginationPrev.on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            this._paginationPrevHandler();
        });
        // Reset filters view on resize.
        $(window).resize( () => {
            this.$filtersContainer.removeAttr('style');
        });
    }
    _bindPaginationEvents () {
        $(Selectors.pageBtn, this.$pagination).on('click', (event) => {
            event.preventDefault();
            event.stopPropagation();
            if (!$(event.currentTarget).hasClass('active')) {
                this._pageButtonHandler($(event.currentTarget));
            }
        });
    }

    // START - Event Handlers
    // Search
    _handleSearchInputClear () {
        this.$searchInput.val('');
        this.$searchClearIcon.hide();
        this.$searchInput.removeClass('invalid');
        this.suggester.resetSuggest();
    }

    _handleSearchAction () {
        const isValid = this.$searchInput[0].validity.valid;
        this.$searchInput.removeClass('invalid');
        if (isValid) {
            this.$searchClearIcon.show();
            this.suggester.resetSuggest();
            this.currentQuery = this.$searchInput.val().trim();
            this.pageStart = 0;
            this.currentPage = 0;
            this.pageTotal = 0;
            this._performSearch(this.currentQuery, 0);
        } else {
            this.$searchInput.addClass('invalid');
        }
    }

    // Links
    _feedbackButtonHandler (element) {
        if (element.href) {
            const currentUrl = encodeURIComponent(`${window.location.href.split('?')[0]}?query=${this.currentQuery}`);
            window.location.href = `${element.href}?section=sitesearch&url=${currentUrl}`;
        }
    }

    // Filters
    _mobileFiltersToggleHandler () {
        this.$filtersContainer.collapse('toggle');
    }

    _filterToggleHandler (element) {
        const $parentEl = $(element.parentElement);
        this._toggleFilterView($parentEl.data('targetId'));
    }

    _filterRadioHandler ($toggleBtn) {
        const $radioBtn = $('input', $toggleBtn);
        const $filterContainer = $toggleBtn.parents(Selectors.filterContainer);
        $radioBtn.prop('checked', !$radioBtn.prop('checked'));
        $(Selectors.filterClearBtn, $filterContainer).toggle(this._isFilteredSearch($filterContainer));
        this.$resetFiltersButton.toggle(this._isFilteredSearch());
        this._performSearch(this.currentQuery, 0);
    }

    _filterCheckboxHandler ($toggleBtn) {
        const targetId = $toggleBtn.data('targetId');
        this._validateSecureSearch($toggleBtn);
        const $collapseTarget = $('#' + targetId, this.$el);
        // If it has children: display and check them.
        if ($collapseTarget.length > 0) {
            this._toggleCheckboxFilterValue($(Selectors.checkbox, $toggleBtn), $(Selectors.checkbox, $collapseTarget));
            if (!$collapseTarget.is(':visible')) {
                this._toggleFilterView(targetId);
            }
        } else {
            this._toggleCheckboxFilterValue($(Selectors.checkbox, $toggleBtn));
        }
        this._performSearch(this.currentQuery, 0);
    }

    _filterClearHandler (element) {
        const $filterContainer = $(element).parents(Selectors.filterContainer);
        $(Selectors.radio, $filterContainer).prop('checked', false);
        $(Selectors.checkbox, $filterContainer).prop('checked', false);
        $(Selectors.checkbox, $filterContainer).prop('indeterminate', false);
        $(element).hide();
        this.$resetFiltersButton.toggle(this._isFilteredSearch());
        this._performSearch(this.currentQuery, 0);
    }

    _filterResetAllHandler () {
        $(Selectors.checkbox, this.$el).prop('checked', false);
        $(Selectors.checkbox, this.$el).prop('indeterminate', false);
        $(Selectors.radio, this.$el).prop('checked', false);
        this.$clearFilterButtons.hide();
        this.$resetFiltersButton.hide();
        this._performSearch(this.currentQuery, 0);
    }

    // Pagination
    _pageButtonHandler ($pageBtn) {
        const pageIndex = $pageBtn.data('index');
        $(Selectors.pageBtn, this.$pagination).removeClass('active');
        $pageBtn.addClass('active');
        this.currentPage = pageIndex;
        document.body.scrollIntoView();
        this._performSearch(this.currentQuery, pageIndex);
    }

    _paginationNextHandler () {
        const nextPage = this.pageStart + this.pageSize;
        this.pageStart = nextPage;
        this.currentPage = nextPage;
        document.body.scrollIntoView();
        this._performSearch(this.currentQuery, nextPage);
    }

    _paginationPrevHandler () {
        const nextPage = this.pageStart - this.pageSize;
        this.pageStart = nextPage;
        this.currentPage = nextPage;
        document.body.scrollIntoView();
        this._performSearch(this.currentQuery, nextPage);
    }
    // Event Handlers - END

    // START - Search
    _performSearch (queryParam, pageIndex) {
        if (this.isLoading || !queryParam) {
            return;
        }
        this.isLoading = true;
        this.$overlay.addClass('show');
        this.$loader.show();
        $.ajax({
            type: 'GET',
            dataType: 'json',
            data: {
                'query': queryParam,
                'format': this._getFormatFilterValues(),
                'language': this._getLanguageFilterValue(),
                'product': this._getProductFilterValues(),
                'section': this._getSiteSectionFilterValues(),
                'page': pageIndex
            },
            url: this.elData.servletPath
        }).done( (dataResponse) => {
            if (dataResponse) {
                this.$resultsContainer.show();
                if (dataResponse.featuredResults) {
                    this._renderFeaturedResults(dataResponse.featuredResults);
                }
                if (dataResponse.coveoResponse) {
                    this._handleCoveoResponse(dataResponse.coveoResponse);
                }
            } else {
                this.$resultsContainer.hide();
            }
        }).fail(() => {
            GlobalOverlay.show(this.$searchErrorMsg);
        }).always(() => {
            this.isLoading = false;
            this.$resultsSummary.show();
            this.$loader.hide();
            this.$overlay.removeClass('show');
            this.suggester.resetSuggest();
            document.body.scrollIntoView({behavior: 'smooth'});
        });        
        if(window.location.href.indexOf('?') > 0) {
            window.history.pushState({}, document.title, window.location.href.split('?')[0]);
        }
    }
    // Search - END

    // START - Handle search response
    _renderFeaturedResults (resultsList) {
        if (resultsList && resultsList.length > 0) {
            $('.js-featured-results-container', this.$el).html(FeaturedResultsHbsTemplate(resultsList));
            this.$featuredResults.show();
        } else {
            this.$featuredResults.hide();
        }
    }

    _handleCoveoResponse (coveoData) {
        const totalCount = Number(coveoData.totalCount);
        this._renderCoveoResultsCount(totalCount);
        this._renderCoveoResults(coveoData.results);
        if (totalCount > 0) {
            this._renderPagination(Number(coveoData.pagesCount), Number(coveoData.pageNumber), this.pageStart);
        } else {
            this.$pagination.hide();
        }
    }

    _renderCoveoResults (resultsList) {
        if (resultsList && resultsList.length > 0) {
            $('.js-coveo-results-container', this.$el).html(CoveoResultsHbsTemplate(resultsList));
            this._highlightResults();
            this.$coveoResults.show();
        } else {
            this.$coveoResults.hide();
        }
    }

    _renderCoveoResultsCount (totalCount) {
        if (totalCount > 0) {
            this.$noResultsFound.hide();
            this.$resultsCount.show();
            this.$resultsCount.text(this.elData.resultsCountMsg.replace(MessagePlaceholder, totalCount));
        } else {
            this.$noResultsFound.show();
            this.$resultsCount.hide();
        }
    }

    _renderPagination (pagesCount, currentPage, init = 0) {
        if (pagesCount) {
            $(Selectors.pageBtn, this.$pagination).parent().remove();
            if (init > 0) {
                this.$paginationPrev.show();
                if ((pagesCount - init) > this.pageSize) {
                    this.$paginationNext.show();
                } else {
                    this.$paginationNext.hide();
                }
            } else {
                this.$paginationPrev.hide();
                if (pagesCount > this.pageSize) {
                    this.$paginationNext.show();
                } else {
                    this.$paginationNext.hide();
                }
            }
            let index = init + (this.pagesToDisplay -1);
            if (pagesCount <= this.pageSize || index >= pagesCount) {
                index = pagesCount - 1;
            }
            this.pageStart = init;
            this.pageTotal = pagesCount;
            this.currentPage = currentPage;
            let counter = index + 1;
            if ((counter * this.pageSize) >= 1000) {
                this.$paginationNext.hide();
            }
            for (index; index >= init; index--) {
                if (counter * this.pageSize <= 1000) {
                    this.$paginationPrev.after(PageIndicatorHbsTemplate({
                        'active': index == currentPage,
                        'index': index,
                        'number': counter
                    }));
                }
                counter--;
            }
            this._bindPaginationEvents();
            this.$pagination.show();
        } else {
            this.$pagination.hide();
        }
    }
    // Handle search response - END

    // START - Utilities
    _setInitialStatus () {
        if (this.elData.language) {
            $('[data-code="' + this.elData.language + '"]', this.$languageFilter).prop('checked', true);
        }
        const queryParam = AEM.url.getParamByName('query');
        if (queryParam) {
            this.currentQuery = queryParam;
            this.$searchInput.val(queryParam);
            this._performSearch(this.currentQuery, 0);
        }
        Observer.addObserver(AEM.Constants.Events.UserDataReady, () => {
            if (AEM.UserService.isLoggedIn()) {
                this.$linksSecure.show();
                if (this.elData.welcomeMsg) {
                    this.$welcomeMsg.text(this.elData.welcomeMsg.replace(MessagePlaceholder, AEM.UserService.getUserFirstName()));
                }
            } else {
                this.$linksPublic.show();
            }
        });
    }

    _toggleFilterView (targetId) {
        const $target = $('#' + targetId, this.$el);
        const $toggleBox = $('.js-toggle-box[data-target-id="' + targetId + '"]', $target.parent());
        $target.collapse('toggle');
        $('.js-icon', $toggleBox).toggle();
    }

    _toggleCheckboxFilterValue ($checkbox, $children, newState) {
        const state = newState || !$checkbox.prop('checked');
        const $filterContainer = $checkbox.parents(Selectors.filterContainer);
        $checkbox.prop('checked', state);
        $checkbox.prop('indeterminate', false);
        if ($children) {
            if (AEM.UserService.isLoggedIn()) {
                $children.prop('checked', state);
                $children.prop('indeterminate', false);
            } else { // Check for secure filters.
                const $filteredChildren = $children.filter((index, elem) => {
                    return !elem.parentElement.dataset.isSecure;
                });
                const childrenCount = $children.size();
                const filteredChildrenCount = $filteredChildren.size();
                $filteredChildren.prop('checked', state);
                $filteredChildren.prop('indeterminate', false);
                if (childrenCount != filteredChildrenCount && state) {
                    $checkbox.prop('checked', false);
                    $checkbox.prop('indeterminate', true);
                }
            }
        }
        // If there's a checkbox marked, then display the Clear filter buttton
        this._checkParentCheckbox($checkbox);
        $(Selectors.filterClearBtn, $filterContainer).toggle(this._isFilteredSearch($filterContainer));
        this.$resetFiltersButton.toggle(this._isFilteredSearch());
    }

    _validateSecureSearch ($toggleBtn) {
        const isSecure = $toggleBtn.data('isSecure');
        if (isSecure && !AEM.UserService.isLoggedIn()) {
            // Redirect user to login page.
            this._redirectToLogin();
        }
    }

    _checkParentCheckbox ($checkbox) {
        const $parentLevel1 = $checkbox.parents('.js-filter-level-1');
        const $parentLevel2 = $checkbox.parents('.js-filter-level-2');
        const $parentLevel3 = $checkbox.parents('.js-filter-level-3');
        let $parent = null;
        if ($parentLevel3.length) {
            $parent = $parentLevel2;
        } else if ($parentLevel2.length) {
            $parent = $parentLevel1;
        }
        if ($parent != null) {
            const $childrenContainer = $('.js-filter-collapsable', $parent);
            const $parentCheckbox = $(Selectors.checkbox, $parent).first();
            const totalCheckboxes = $(Selectors.checkbox, $childrenContainer).length;
            const checkedCheckboxes = $(Selectors.checkboxChecked, $childrenContainer).length;
            if (checkedCheckboxes === 0) {
                $parentCheckbox.prop('checked', false);
                $parentCheckbox.prop('indeterminate', false);
            } else if (totalCheckboxes === checkedCheckboxes) {
                $parentCheckbox.prop('checked', true);
                $parentCheckbox.prop('indeterminate', false);
            } else {
                $parentCheckbox.prop('checked', false);
                $parentCheckbox.prop('indeterminate', true);
            }
            if ($parentLevel3.length) {
                this._checkParentCheckbox($parentCheckbox);
            }
        }
    }

    _isFilteredSearch ($filterContainer) {
        if ($filterContainer) {
            return $(Selectors.checkboxChecked, $filterContainer).length > 0 ||
                $(Selectors.radioChecked, $filterContainer).length > 0;
        } else {
            return $(Selectors.checkboxChecked, this.$el).length > 0 ||
                $(Selectors.radioChecked, this.$el).length > 0;
        }
    }

    _getFormatFilterValues () {
        return this._getCheckboxValues('.js-filter-format');
    }
    _getProductFilterValues () {
        return this._getCheckboxValues('.js-filter-product');
    }
    _getSiteSectionFilterValues () {
        let sectionParam = AEM.url.getParamByName('section');
        if (sectionParam) {
            sectionParam = decodeURIComponent(sectionParam);            
            $(".js-filter-site-section input[type=checkbox]").each(function() {           
               if($(this).attr('value') == sectionParam && !$(this).is(":checked")){
                    $(this).prop('checked', true);
               }
            });       
        }
        return this._getCheckboxValues('.js-filter-site-section');
    }
    _getCheckboxValues (selector) {
        const values = [];
        $(selector + ' ' + Selectors.checkboxChecked, this.$el).each( (index, item) => {
            values.push(item.value);
        });
        return values.join();
    }
    _getLanguageFilterValue () {
        return $('.js-filter-language ' + Selectors.radioChecked, this.$el).val();
    }
    _highlightResults () {
        const highlightWord = this.currentQuery.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        const regex = new RegExp(highlightWord, 'gi');
        $('.js-result-description,.js-result-header', this.$el).each((index, item) => {
            item.innerHTML = item.innerHTML.replace(regex, `<mark>$&</mark>`);
        });
    }

    _redirectToLogin () {
        if (this.elData.loginLink) {
            let currentUrl = window.location.href.split('?')[0];
            if (this.currentQuery) {
                currentUrl += `?query=${this.currentQuery}`;
            }
            currentUrl = encodeURIComponent(currentUrl);
            window.location.href = `${this.elData.loginLink}?saml_request_path=${currentUrl}`;
        }
    }
    _logoutUser () {
        AEM.UserService.logout(this.elData.loginLink);        
    }
    // Utilities - END
}

AEM.registerComponent('global-search', GlobalSearch);
