const Suggestions = require('suggestions');

export class Suggester {
    constructor (element, publicSearchKey, coveoSuggestEndpoint, $searchErrorMsg) {
        this.inputEl = element;
        this.publicSearchKey = publicSearchKey;
        this.coveoSuggestEndpoint = coveoSuggestEndpoint;
        this.$searchErrorMsg = $searchErrorMsg;
        this.suggest = this._initSuggest();
        this._bindEvents();
        return this;
    }

    performSuggest (queryParam) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            data: {
                'access_token': this.publicSearchKey,
                'q': queryParam
            },
            url: this.coveoSuggestEndpoint
        }).done( (dataResponse) => {
            if (dataResponse && dataResponse.completions) {
                this.suggest.update(dataResponse.completions);
            }
        }).fail(() => {
            if ($searchErrorMsg) {
                GlobalOverlay.show(this.$searchErrorMsg);
            }
        });
    }

    resetSuggest () {
        this.suggest.clear();
        this.suggest.update();
        this.suggest.selected = null;
    }

    _initSuggest () {
        return new Suggestions(this.inputEl, [], {
            minLength: 3, // Number of characters typed into an input to trigger suggestions.
            limit: 5, //  Max number of results to display.
            hideOnBlur: false, // Don't hide results when input loses focus
            getItemValue: function(item) {
                return item.expression;
            }
        });
    }
    _bindEvents () {
        this.inputEl.addEventListener('keyup', (event) => {
            const keyCode = (event.keyCode ? event.keyCode : event.which);
            if (keyCode === 27) { // Handle escape key.
                this.resetSuggest();
            } else if (keyCode > 46 || keyCode === 32) {
                this._handleTypingAction();
            }
        });
    }
    _handleTypingAction () {
        const isValid = this.inputEl.validity.valid;
        if (isValid) {
            this.performSuggest(this.inputEl.value);
        }
    }
};
