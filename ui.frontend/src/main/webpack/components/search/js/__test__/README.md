###### Placeholder folder

#### Structure for "<Component filename\>.test.js"

    require('../<Component filename>.js');
    beforeEach(() => {
    });
    
    afterEach(() => {
    	jest.restoreAllMocks();
    });
    
    
    describe('Juniper <Component filename> Component Test', () => {
    	test('<Component filename>', () => {
    		//Expects here
    	});
    });