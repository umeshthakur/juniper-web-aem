import * as AEM from '../../../site/js/juniper';
import {GlobalOverlay} from '../../globaloverlay/js/globaloverlay';

const $ = require('jquery');

export class WhatWeOffer extends AEM.Component {
    init() {   
        const width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);       
        this._setClass(width, this.$el);
        this._bindEvents(this);
    }
    
    _setClass(width, elm){
       
        $('.step', elm).each(function( index ) {
            $(this).css('bottom', $(this).height());
        });
        
        $(window).resize(function() {
            $('.step', elm).each(function( index ) {
                $(this).css('bottom', $(this).height());
            });
        });
        
    }
    
    _bindEvents(globalObj) {
        $('a.play-icon' , globalObj.$el).each(function() {                
                $(this).on( 'click' , (event) => {
                    globalObj ._playVideoClickHandler(event);
                });
        });
        
        $( '.close-icon' , globalObj.$el).each(function() {
            $(this).on( 'click' , (event) => {
                event.stopPropagation();
                event.preventDefault();
                globalObj._closeVideoClickHandler(event);
            });
        });
        
    }    
    
    _playVideoClickHandler (event) {
        
        const $playButton = $(event.currentTarget);
        const $videoOverlay = $('#' + $playButton.data( 'videoModalId' )).closest('.video-overlay' );
        const $iframe = $( 'iframe' , $videoOverlay);
        
        let $autoPlay = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $autoPlay = $iframe.attr( 'src' ).split('?')[0] + '?autoplay=true';
        } else{
            $autoPlay = $iframe.attr( 'src' ) + '?autoplay=true';
        }
        $iframe.attr( 'src' , $autoPlay);
        GlobalOverlay.show( null , $playButton.data( 'videoModalId' ), true );
    }
    
    _closeVideoClickHandler (event) {
        const $videoOverlay = $(event.currentTarget).closest('.video-overlay' );
        const $iframe = $( 'iframe' , $videoOverlay);       
        
        let $stopUrl = '';
        if($iframe.attr( 'src' ).indexOf('?') > -1){
            $stopUrl = $iframe.attr( 'src' ).split('?')[0];
        } else{
            $stopUrl = $iframe.attr( 'src' );
        }
        $iframe.attr( 'src' , $stopUrl);
        GlobalOverlay.close($videoOverlay.attr( 'id' ));
    }
}

AEM.registerComponent('what-we-offer', WhatWeOffer);
