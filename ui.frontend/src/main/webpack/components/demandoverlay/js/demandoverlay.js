import * as AEM from "../../../site/js/juniper";
import 'bootstrap/js/dist/collapse';

class DemandOverlay extends AEM.Component {
    init() {
        this.$target = $('.js-expandable-target', this.$el);
        this.$trigger = $('.js-icon-expandable', this.$el);
        this.$carousel = $('.js-slick-carousel', this.$el);
        this.$prevArrow = $('.js-prev-arrow', this.$el);
        this.$nextArrow = $('.js-next-arrow', this.$el);
        this.$customDots = $('.js-custom-dots', this.$el);
        this._expandable();
        this._createCarousel();
    }

    _expandable() {
        $(this.$trigger).on('click', (event) => {
            event.preventDefault();
            $(this.$trigger).toggleClass('show');
            $(this.$target).collapse('toggle');
            this.$carousel.slick('setPosition');
        });

    }

    _createCarousel(){
        this.$carousel.not('.slick-initialized').slick({
            arrows: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            initialSlide: 0,
            useTransform: false,
            dots: false,
            appendDots: this.$customDots,
            swipeToSlide: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: '1025',
                    settings: {
                        useTransform: false,
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: '768',
                    settings: {
                        useTransform: false,
                        slidesToShow: 2,
                    }
                },
                {
                    breakpoint: '767',
                    settings: {
                        useTransform: false,
                        slidesToShow: 1,
                        dots: true,
                        arrows: false,
                    }
                }]
        });
    }
}

AEM.registerComponent('demand-overlay', DemandOverlay);

