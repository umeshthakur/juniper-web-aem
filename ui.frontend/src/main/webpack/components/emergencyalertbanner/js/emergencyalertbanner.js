import * as AEM from "../../../site/js/juniper";

class EmergencyAlertBanner extends AEM.Component {
	init(){
		this.$close = $('.js-close', this.$el);
		this._bindEvents();
	}
	_bindEvents() {
		$(this.$close).on('click', (event) => {
			event.preventDefault();
			$(this.$el).hide();
		});
	}
};


AEM.registerComponent('emergency-alert-banner', EmergencyAlertBanner);
