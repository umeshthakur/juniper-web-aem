// Class to control the Global Overlay
export class GlobalOverlay {

    static show (body, modelId = 'globalOverlayModal', darkMode = false, closeCallback = null) {
        const $modal = $('.modal#' + modelId);
        if (body != null) {
            $modal.find('.modal-body').html(body);
        }
        if (typeof closeCallback === 'function') {
            $modal.on('hidden.bs.modal', function () {
                closeCallback();
            });
        } else {
            $modal.off('hidden.bs.modal');
        }
        if (darkMode) {
            $modal.on('shown.bs.modal', function () {
                $('div.modal-backdrop').addClass('dark-mode');
            });
        }
        $modal.modal();
        return $modal;
    }
    static close (modelId = 'globalOverlayModal') {
        const $modal = $('.modal#' + modelId);
        $modal.modal('hide');
    }
};

