import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

export class CardsLevelOne extends AEM.Component {
    init() {
        
        if (window.innerWidth > 720) {
            // let titleMaxHeight = 0;
            // let cl1_title = $('.card-info .prod-title', this.$el);
            // $(cl1_title).each(function () {
            //     if ($(this).height() > titleMaxHeight) {
            //         titleMaxHeight = $(this).height();
            //     }
            // });
            // $(cl1_title).height(titleMaxHeight);

            let descMaxheight = 0;
            let cl1_desc = $('.card-info .description', this.$el);
            $(cl1_desc).each(function () {
                if ($(this).height() > descMaxheight) {
                    descMaxheight = $(this).height();
                }
            });
            $(cl1_desc).height(descMaxheight);
        }
    }
};

AEM.registerComponent('cardslevel1', CardsLevelOne);