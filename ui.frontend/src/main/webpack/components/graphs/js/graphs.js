import * as AEM from "../../../site/js/juniper";

export class Graphs extends AEM.Component {
    init() {
        this.$graphs = $('.js-graph', this.$el);
        this._setGraphs();
    }

    _setGraphs() {
        this.$graphs.each(function (index, item) {
            const $graph = $(item);
            const leftAmount = Number($graph.data('leftSideAmount'));
            const rightAmount = Number($graph.data('rightSideAmount'));
            const total = leftAmount + rightAmount;
            const leftPercentage = leftAmount * 100 / total;
            // Right is always 100%
            $('.js-left-bar', item).width(leftPercentage + '%');
        });
    }
}

AEM.registerComponent('graphs', Graphs);

