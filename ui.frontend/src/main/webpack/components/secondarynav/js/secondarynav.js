import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

	

export class SecondaryNav extends AEM.Component {
	init() {
        this.$secNav = $('.secnav', this.$el);

        this.$morel2 = $(this.$secNav, 'more-l2');

        $('span.toggle-level3', this.$el).on("click",{ref:this},  this._toggleExpandCollapse);
    
        $('.toggle-level2-more', this.$el).on("click", {ref:this}, this._toggleExpandCollapse);

         // for showing current page dot
        this.$currPath = window.location.pathname;
        this.$currPage = $("a[href*='" + this.$currPath + "']",this.$el);
        if(this.$currPage.attr('class') == 'l2'){
            this.$currPage.children('.page').addClass('active');
        }else if(this.$currPage.attr('class') == 'l3'){
            this.$currPage.parent().parent().parent().children('.l2').children('.page').addClass('active');
        }

        this._calculateVarWidth();

        this._onScreenSizeChange(this)
        this._collect();

        $('.toggle-more-mobile', this.$el).on("click",  this._toggleMobileMoreL2);
        $('.level2-extra .toggle-level3-mob', this.$el).on("click",  this._toggleMobileL3);
        $('.back-level2', this.$el).on("click", this._toggleToMobLevel2);

        this.$expandedMenu = null;
   } 
   
   _toggleMobileMoreL2(event){
        if($(this).hasClass('icon-arrow-down')){
            $(this).addClass('icon-arrow-up').removeClass('icon-arrow-down');
        }else{
            $(this).addClass('icon-arrow-down').removeClass('icon-arrow-up');

        }
       $(this).parent().siblings('.level2-extra').toggleClass("show");
   }

   _toggleMobileL3(event){
        $(this).parent().parent().toggleClass("show");
        $(this).parent().parent().siblings('.level3-mob').toggleClass("show");
        
        //clone the object
        let $collectedSet = $(this).siblings('.level3-list');
        $(".level3-list-mob", this.$el).empty().append($collectedSet.clone(true))
   }

   _toggleToMobLevel2(event){
       $(this).parent().toggleClass("show");
       $(this).parent().siblings('.level2-extra').toggleClass("show");

   }

   _toggleExpandCollapse(event){

        if($(this).hasClass('icon-arrow-down')){
            // Check is this the reference of more dropdown for l2
            if(event.data.ref && event.data.ref.$expandedMenu != null){
                if(!$(event.data.ref.$expandedMenu).hasClass('toggle-level2-more') || $(this).parent().parent().hasClass('secnav')){
                    $(event.data.ref.$expandedMenu).addClass('icon-arrow-down').removeClass('icon-arrow-up');
                    $(event.data.ref.$expandedMenu).siblings('.next-level').removeClass("show");    
                }
            }
           // update the reference with current dropdown 
            if(!$(this).parent().parent().hasClass('more-l2')){
                event.data.ref.$expandedMenu = $(this);
            }
            // Expanding the drop down menu
            $(this).addClass('icon-arrow-up').removeClass('icon-arrow-down');
            $(this).siblings('.next-level').toggleClass("show");

        }else{
            $(this).addClass('icon-arrow-down').removeClass('icon-arrow-up');
            $(this).siblings('.next-level').toggleClass("show");
        }
   }

    _calculateVarWidth(){
        this.$varWidth = 0;
        this.$widthLeftOver = 0;
        this.$elWidths=[];
        this.$childrenCount = this.$secNav.children().length;
        var ref = this;
        this.$secNav.children().each(function() {
            ref.$varWidth += parseInt($(this).outerWidth(true),10);
            ref.$elWidths.push(parseInt($(this).outerWidth(true),10));
        });
    }
   
    _collect() { 
        this.$expandedMenu = null;
        this.$secNav.children().removeAttr('style');
        const childFitCount = this._getFitCount();

        if(this.$childrenCount != childFitCount){
            let $collectedSet = this.$secNav.children().slice(childFitCount);
            $(".more-l2", this.$el).empty().append($collectedSet.clone(true)).width(this.$widthLeftOver);
            $collectedSet.css({"display": "none", "width": "0"});

            if($('.more-l2', this.$el).children().length > 0 ){
                $(".more", this.$el).addClass('show');   
            }else{
                $(".more", this.$el).removeClass('show');
            }
        }else{
             $(".more-l2", this.$el).empty();
             $(".more", this.$el).removeClass('show');
        }

    }

    _getFitCount(){
        let count = 0;
        const elemWidth = this.$secNav.width();
        if(this.$varWidth > elemWidth ){

            for (const obj in this.$elWidths){
                count += this.$elWidths[obj];
                if(elemWidth < count + 100){
                    this.$widthLeftOver = elemWidth - (count - this.$elWidths[obj])+60;
                    return obj-1;
                }
            }
        }

        return this.$childrenCount;
    }

    _onScreenSizeChange(ref){
        
        $(window).on("resize", function( ) {
            ref._collect();
        });
        
        $( window ).on("orientationchange", function( ) {
            setTimeout(function(){
                ref._calculateVarWidth();
                ref._collect();
            },300);
        });
 
    }
}

AEM.registerComponent('secondary-nav', SecondaryNav);
