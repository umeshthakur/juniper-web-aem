import * as AEM from "../../../site/js/juniper";
const $ = require('jquery');

export class ProductComparison extends AEM.Component {
    init() {

        let titleMaxHeight = 0;
        this.$pc_title =  $('.prod-info .product-name', this.$el);
        $(this.$pc_title).each(function () {
            if ($(this).height() > titleMaxHeight) {
                titleMaxHeight = $(this).height();
            }
        });
        $(this.$pc_title ).height(titleMaxHeight);

        let descriptionMaxHeight = 0;
        this.$pc_description =  $('.prod-info .description', this.$el);
        $(this.$pc_description).each(function () {
            if ($(this).height() > descriptionMaxHeight) {
                descriptionMaxHeight = $(this).height();
            }
        });
        $(this.$pc_description).height(descriptionMaxHeight);

        let technicalFeaturesMaxHeight = 0;
        this.$pc_techfeatures =  $('.tech-details .technical-features tbody', this.$el);
        $(this.$pc_techfeatures).each(function () {
            if ($(this).height() > technicalFeaturesMaxHeight) {
                technicalFeaturesMaxHeight = $(this).height();
            }
        });
        $(this.$pc_techfeatures).height(technicalFeaturesMaxHeight);

    }
};
AEM.registerComponent('productcomparison', ProductComparison);