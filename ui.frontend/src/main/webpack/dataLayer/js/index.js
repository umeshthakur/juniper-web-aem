function DataLayerManager()  {
    const dataLayer = window.jnprData;

    return {
        getDataLayer: function() {
            return dataLayer;
        },
        push: function(...args) {
            const pushArguments = args;

            Object.keys(pushArguments).forEach(function(key) {
                Object.assign(dataLayer,pushArguments[key]);
            });
        },
        pushMeta: function (name, metaField) {
            const meta = document.getElementsByTagName("meta");
            for (const element in meta) {
                if(element.name === metaField) {
                    this.push(name, element.value);
                }
            }
        },
        fireClickEvent: function(name, type, effect, options) {
            const event = {
                eventName: name,
                eventAction: 'click',
                eventPoints: 200,
                type: type,
                timeStamp: new Date(),
                effect: effect,
                ... options
            };
            // Initialize events if not already there.
            dataLayer.event = dataLayer.event || [];

            dataLayer.event.push(event);
        },
        fireEvent: function(name, action, points, type, effect, options) {
            const event = {
                eventName: name,
                eventAction: action,
                eventPoints: points,
                type: type,
                timeStamp: new Date(),
                effect: effect,
                ... options
            };
            // Initialize events if not already there.
            dataLayer.event = dataLayer.event || [];

            dataLayer.event.push(event);
        },

    };
}

window.jnprData = window.jnprData || {};
window.jnprDataManager = window.jnprDataManager || DataLayerManager();

