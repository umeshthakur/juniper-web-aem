const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TSConfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const WebpackMessages = require('webpack-messages');
const {exec} = require('child_process');

const SOURCE_ROOT = `${__dirname}/src/main/webpack`;

module.exports = {
    resolve: {
        extensions: ['.js', '.ts'],
        plugins: [new TSConfigPathsPlugin({
            configFile: './tsconfig.json',
        })],
    },
    entry: {
        site: `${SOURCE_ROOT}/site/main.js`,
        dependencies: `${SOURCE_ROOT}/vendors/vendors.js`,
        dataLayer: `${SOURCE_ROOT}/dataLayer/main.js`,
    },
    output: {
        filename: (chunkData) => `clientlib-${chunkData.chunk.name}/[name].js`,
        path: path.resolve(__dirname, 'dist'),
    },
    module: {
        rules: [
            {
                test: require.resolve('jquery'),
                loader: 'expose-loader',
                options: {
                    exposes: {
                        globalName: '$',
                        override: true,
                    },
                },
            },
            {
                test: /\.js$/,
                exclude: /node_modules|@babel(?:\/|\\{1,2})runtime|core-js/,
                use: [
                    {
                        loader: 'babel-loader',
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false,
                        },
                    },
                ],
            },
            {
                test: /\.hbs/,
                loader: 'handlebars-loader',
                query: {
                    helperDirs: [
                        `${__dirname}/src/main/webpack/site/hbs/helpers`,
                    ],
                },
            },
            {
                test: /\.scss$|\.css/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            url: false,
                        },
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins() {
                                return [
                                    require('autoprefixer')({grid: 'autoplace'}),
                                ];
                            },
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            url: false,
                        },
                    },
                    {
                        loader: 'webpack-import-glob-loader',
                        options: {
                            url: false,
                        },
                    },
                    {
                        loader: 'kss-loader',
                        options: {
                            config: 'kss-config.json',
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin({
            cleanStaleWebpackAssets: false,
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new MiniCssExtractPlugin({
            filename: 'clientlib-[name]/[name].css',
        }),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, `${SOURCE_ROOT}/resources`),
                to: './clientlib-site/',
                force: true,
                copyUnmodified: true,
            },
        ]),
        new WebpackMessages({
            name: 'client',
            logger: (str) => console.log(`>> ${str}`),
        }),
        {
            apply: (compiler) => {
                compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {
                    exec('clientlib --verbose', (err, stdout, stderr) => {
                        if (stdout) {
                            process.stdout.write(stdout);
                        }
                        if (stderr) {
                            process.stderr.write(stderr);
                        }
                    });
                });
            },
        },
    ],
    stats: {
        assetsSort: 'chunks',
        builtAt: true,
        children: false,
        chunkGroups: true,
        chunkOrigins: true,
        colors: false,
        errors: true,
        errorDetails: true,
        env: true,
        modules: false,
        performance: true,
        providedExports: false,
        source: false,
        warnings: true,
    },
    watchOptions: {
        aggregateTimeout: 600,
        poll: 2000,
    },
};
