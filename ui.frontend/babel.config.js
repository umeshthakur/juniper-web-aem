module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                debug: false,
                useBuiltIns: "usage",
                modules: false,
                corejs: 3,
                targets: {
                    "android": "86",
                    "chrome": "86",
                    "edge": "86",
                    "firefox": "82",
                    "ie": "11",
                    "ios": "12.2",
                    "opera": "71",
                    "safari": "13.1",
                    "samsung": "12"
                }
            },
        ],
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-transform-arrow-functions',
        '@babel/plugin-transform-parameters',
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-transform-modules-commonjs',
        '@babel/plugin-transform-async-to-generator',
        '@babel/plugin-transform-block-scoping',
        '@babel/plugin-transform-eval'
    ],
};
